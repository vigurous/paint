using System;
using System.Collections.Generic;

namespace Бисерок
{
	public static class Timing
	{
		private static List<TimeSpan> ts;

		private static DateTime dt;

		private static int countPeriods;

		static Timing()
		{
			ts = new List<TimeSpan>();
			countPeriods = 13;
			for (int i = 0; i < countPeriods; i++)
			{
				ts.Add(default(TimeSpan));
			}
		}

		public static void PeriodBegin()
		{
			dt = DateTime.Now;
		}

		public static void PeriodEnd(int n)
		{
			List<TimeSpan> list = ts;
			list[n] += DateTime.Now - dt;
		}

		public static string GetResult()
		{
			string text = "";
			for (int i = 0; i < countPeriods; i++)
			{
				text = text + "[" + i.ToString() + "] =\t" + ts[i].TotalSeconds.ToString() + "\r\n";
			}
			return text;
		}
	}
}
