using System;

namespace Бисерок
{
	public class OrderInfo
	{
		public string email;

		public int number;

		public int v;

		public DateTime? dtEnd;

		public bool isOkSendedToServer;

		public bool isActivatingLimit;

		public bool isExpiredPayedOrder;

		public Exception exception;
	}
}
