using System.Collections;
using System.Windows;
using System.Windows.Controls;

namespace Бисерок
{
	public static class Local
	{
		private static int language;

		public static void LocalizeControlAndItsChildren(UIElement control)
		{
			if (language != 0)
			{
				if (control is Panel)
				{
					foreach (object child in (control as Panel).Children)
					{
						LocalizeControlAndItsChildren(child as UIElement);
					}
				}
				else if (control is ComboBox)
				{
					foreach (object item in (IEnumerable)(control as ComboBox).Items)
					{
						LocalizeControlAndItsChildren(item as UIElement);
					}
				}
				else if (control is GroupBox)
				{
					if ((control as GroupBox).Header != null && (control as GroupBox).Header is string)
					{
						(control as GroupBox).Header = Get((control as GroupBox).Header.ToString());
					}
					foreach (object child2 in ((control as GroupBox).Content as Panel).Children)
					{
						LocalizeControlAndItsChildren(child2 as UIElement);
					}
				}
				else if (control is Border)
				{
					LocalizeControlAndItsChildren((control as Border).Child);
				}
				else
				{
					if (control is FrameworkElement && (control as FrameworkElement).ToolTip != null)
					{
						(control as FrameworkElement).ToolTip = Get((control as FrameworkElement).ToolTip.ToString());
					}
					if (control is ContentControl && (control as ContentControl).Content != null && (control as ContentControl).Content is string)
					{
						(control as ContentControl).Content = Get((control as ContentControl).Content.ToString());
					}
					try
					{
						control.SetValue(TextBlock.TextProperty, Get(control.GetValue(TextBlock.TextProperty).ToString()));
					}
					catch
					{
					}
				}
			}
		}

		public static string Get(string src)
		{
			if (language == 0)
			{
				return src;
			}
			switch (src)
			{
			case "Б. Загрузить сохранённую картину (доступно в коммерч.версии)":
				return "B. Load saved painting (available in commercial version)";
			case " ТЕСТОВАЯ ВЕРСИЯ":
				return " TEST VERSION";
			case "ТЕСТОВАЯ версия программы:":
				return "TEST version of the program";
			case "Купить ПОЛНУЮ версию, чтобы активировать эту вкладку":
				return "Buy FULL version to activate this tab";
			case "Б. Загрузить сохранённую схему":
				return "B. Load saved pattern";
			case " ДОМАШНЯЯ ВЕРСИЯ":
				return " HOME VERSION";
			case "ДОМАШНЯЯ версия программы:":
				return "HOME version of the program";
			case "Купить ПРОФЕССИОНАЛЬНУЮ версию программы":
				return "Buy PROFESSIONAL version of the program";
			case " БАЗОВАЯ ВЕРСИЯ":
				return " BASE VERSION";
			case "БАЗОВАЯ версия программы:":
				return "BASE version of the program";
			case " ПРОФЕССИОНАЛЬНАЯ ВЕРСИЯ":
				return " PROFESSIONAL VERSION";
			case " ПРОДВИНУТАЯ ВЕРСИЯ":
				return " ADVANCED VERSION";
			case "ПРОДВИНУТАЯ версия программы:":
				return "ADVANCED version of the program";
			case "Б. Загрузить сохранённую картину":
				return "B. Load save painting";
			case " МАСТЕР-ВЕРСИЯ":
				return "MASTER-VERSION";
			case "Б. Из вкладки \"Доработка\"":
				return "B. From tab \"Rework\"";
			case "Редактор своих цветов бисера":
				return "Editor of own beads colors";
			case " КОММЕРЧЕСКАЯ ВЕРСИЯ":
				return " COMMERCIAL VERSION";
			case "Бисерок 2.0 - создавайте схемы вышивки бисером из Ваших фотографий и картинок!":
				return "Beads 2.0 - let's create beads embroidery patterns from your own photos and pictures!";
			case "Для бисера (выберите):":
				return "For beads (select):";
			case " 3. Выберите бисер нужного цвета":
				return "3. Select beads needed color";
			case "*Цвет и число бисерин":
				return "*Color and number of beads";
			case "Схема будет создана не под конкретного производителя бисера, а просто под обычные цвета (нужный бисер Вам придётся подбирать самостоятельно)":
				return "Pattern will be created not for specific manufacturer, but for simple colors (you should select needed beads yourselves)";
			case "Схема будет создана под выбранные Вами ниже позиции: производители бисера/серии бисера/конкретный бисер":
				return "Pattern will be created for selected yourself clauses below: manufacturers of beads/series of beads/specific beads";
			case "Вы можете выбрать производителей бисера, серии бисера или конкретные номера бисера, из которых будут подбираться цвета в схему (например, для ситуации, когда Вы точно знаете, что не сможете приобрести какую-либо серию бисера, Вы можете исключить её из подбора)":
				return "You can select manufacturers, series or specific beads numbers to create the pattern (for example, for the situation when you know for sure that you can't get any series of beads, you can exclude it from the selection)";
			case "Выбор размера бисера, настройки печати и сохранения схем":
				return "Selecting the size of the beads, printing settings and patterns' saving";
			case "ВЫШИВКЕ":
				return "EMBROIDERY";
			case "Выбрать все":
				return "Select all";
			case "Убрать все":
				return "Remove all";
			case "Ниже представлен лишь вид вышивки. Чтобы получить саму схему вышивки и карту цветов - сохраните созданную вышивку! Кнопка 'Сохранить' находится слева-внизу этого окна.":
				return "Below is just a facade of embroidery. To get the pattern and color map - save the created embroidery! 'Save' button is located at the bottom-left of this window.";
			case "Отмена (Ctrl + Z)":
				return "Cancel (Ctrl + Z)";
			case "в бисеринках:":
				return "in beads";
			case "Крестик 2.0 - создавайте схемы вышивки крестом из Ваших фотографий и картинок!":
				return "Cross 2.0 - let's create cross-stitch embroidery patterns from your own photos and pictures!";
			case "Для мулине (выберите):":
				return "For mouline thread (select)";
			case " 3. Выберите мулине нужного цвета":
				return "Select mouline thread needed color";
			case "*Цвет и число стежков":
				return "Color and number of the stitches";
			case "Схема будет создана не под конкретного производителя мулине, а просто под обычные цвета (нужные мулине Вам придётся подбирать самостоятельно)":
				return "Pattern will be created not for specific manufacturer, but for simple colors (you should select needed mouline thread yourselves)";
			case "Схема будет создана под выбранные Вами ниже позиции: производители мулине/серии мулине/конкретное мулине":
				return "Pattern will be created for selected yourself clauses below: manufacturers of mouline thread/series of mouline thread/specific mouline thread";
			case "Вы можете выбрать производителей мулине, серии мулине или конкретные номера мулине, из которых будут подбираться цвета в схему (например, для ситуации, когда Вы точно знаете, что не сможете приобрести какую-либо серию мулине, Вы можете исключить её из подбора)":
				return "You can select manufacturers, series or specific mouline thread numbers to create the pattern (for example, for the situation when you know for sure that you can't get any series of mouline thread, you can exclude it from the selection)";
			case "Выбор размера канвы, числа сложений нити, настройки печати и сохранения схем":
				return "Selecting canvas size, number of thread additions, settings of printing and patterns saving";
			case "в стежках:":
				return "in stitches:";
			case "Петелька - создавайте схемы вязания из Ваших фотографий и картинок!":
				return "Stitch - let's create knitting patterns from your own photos and pictures!";
			case "Для пряжи (выберите):":
				return "For yarn (select)";
			case " 3. Выберите пряжу нужного цвета":
				return " 3. Select the yarn needed color";
			case "*Цвет и число петель":
				return "*Color and number of stitches";
			case "Схема будет создана не под конкретного производителя пряжи, а просто под обычные цвета (нужную пряжу Вам придётся подбирать самостоятельно)":
				return "Pattern will be created not for specific manufacturer, but for simple colors (you should select needed yarn yourselves)";
			case "Схема будет создана под выбранные Вами ниже позиции: производители пряжи/серии пряжи/конкретная пряжа":
				return "Pattern will be created for selected yourself clauses below: manufacturers of yarn/series of yarn/specific yarn";
			case "Вы можете выбрать производителей пряжи, серии пряжи или конкретные номера пряжи, из которых будут подбираться цвета в схему (например, для ситуации, когда Вы точно знаете, что не сможете приобрести какую-либо серию пряжи, Вы можете исключить её из подбора)":
				return "You can select manufacturers, series or specific yarn numbers to create the pattern (for example, for the situation when you know for sure that you can't get any series of yarn, you can exclude it from the selection)";
			case "Выбор плотности вязания, настройки печати и сохранения схем":
				return "Selecting the closeness of knitting, settings of printing and saving patterns";
			case "ВЯЗАНИЮ":
				return "KNITTING";
			case "Ниже представлен лишь вид вязания. Чтобы получить саму схему вязания и карту цветов - сохраните созданное вязание! Кнопка 'Сохранить' находится слева-посередине этого окна.":
				return "Below is just a facade of knitting. To get the pattern and color map - save the created knitting! 'Save' button is located at the bottom-left of this window.";
			case "в петлях:":
				return "in stitches";
			case "'Раскраска' - создавайте раскраски и картины по номерам из Ваших фотографий и картинок!":
				return "'Painting by numbers' - create colorings and pictures by numbers from your own photos and pictures!";
			case "Купить КОММЕРЧЕСКУЮ версию, чтобы активировать эту вкладку":
				return "Buy COMMERCIAL version to activate this tab";
			case "Коммерческая версия":
				return "Commersial version";
			case "Домашняя/Профессиональная версия":
				return "Home/Professional version";
			case "КОММЕРЧЕСКАЯ версия":
				return "COMMERCIAL version";
			case "ДОМАШНЯЯ/Профессиональная версия":
				return "HOME/Professional version";
			case "ДОМАШНЯЯ версия программы":
				return "HOME version of the program";
			case "Домашняя/ПРОФЕССИОНАЛЬНАЯ версия":
				return "Home/PROFESSIONAL version";
			case "ПРОФЕССИОНАЛЬНАЯ версия программы":
				return "PROFESSIONAL version of the program";
			case "Для красок (выберите):":
				return "For colors (select):";
			case " 3. Выберите краску похожего цвета":
				return "3. Select paint of the similar colour";
			case "Теперь выбранной в пункте 3 краской либо рисуйте на схеме (если в пункте 4 выбрали 'Кисть'), либо заливайте области (если в пункте 4 выбрали 'Заливку').":
				return "Now draw on the painting (if you select 'Brush' in step 4) or fill the areas (if you selected 'Fill' in step 4).";
			case "*Цвет и % краски":
				return "*Color and % paints";
			case "РАСКРАСКЕ":
				return "PAINTING";
			case "Схема будет создана не под конкретного производителя красок, а просто под обычные цвета (нужные краски Вам придётся подбирать самостоятельно)":
				return "Painting will be created not for specific manufacturer, but for simple colors (you should select needed paints yourselves)";
			case "Схема будет создана под выбранные Вами ниже позиции: производители красок, серии и номера красок":
				return "Painting will be created for selected yourself clauses below: manufacturers/series and numbers of the paints";
			case "Вы можете выбрать производителей красок, серии красок или конкретные номера красок, из которых будут подбираться цвета в раскраску (например, для ситуации, когда Вы точно знаете, что не сможете приобрести какую-либо серию красок, Вы можете исключить её из подбора)":
				return "You can select manufacturers, series or specific numbers of paints to create the painting (for example, for the situation when you know for sure that you can't get any series of paints, you can exclude it from the selection)";
			case "Важные настройки":
				return "Important settings";
			case "Редактор своих цветов красок":
				return "The editor of your own colors of paints";
			case "Ниже представлен лишь вид картины. Чтобы получить саму схему раскраски и карту цветов - сохраните созданную картину! Кнопка 'Сохранить' находится слева-внизу этого окна.":
				return "Below is just a facade of painting. To get the pattern and color map - save the created painting! 'Save' button is located at the bottom-left of this window.";
			case "Шаг 1. Создание картины по номерам":
				return "Step 1. Creating a painting by the numbers";
			case "Шаг 2. Редактирование картины":
				return "Step 2. Editing the painting";
			case "Из вкладки 'Шаг 1. Создание картины'":
				return "From the tab 'Step 1. Creating a painting'";
			case "Алмазная мозаика - создавайте схемы из Ваших фотографий и картинок!":
				return "Diamond mosaic - let's create patterns from your own photos and pictures!";
			case "Для страз (выберите):":
				return "For crystals (select):";
			case " 3. Выберите стразы похожего цвета":
				return "Select crystals of the similar colours";
			case "*Цвет и число страз":
				return "*Color and number of crystals";
			case "Схема будет создана не под конкретного производителя страз, а просто под обычные цвета (нужные стразы Вам придётся подбирать самостоятельно)":
				return "Pattern will be created not for specific manufacturer, but for simple colors (you should select needed crystals yourselves)";
			case "Схема будет создана под выбранные Вами ниже позиции: производители страз/серии страз/конкретные стразы":
				return "Pattern will be created for selected yourself clauses below: manufacturers/series of the crystals or specific crystals";
			case "Вы можете выбрать производителей страз, серии страз или конкретные номера страз, из которых будут подбираться цвета в схему (например, для ситуации, когда Вы точно знаете, что не сможете приобрести какую-либо серию страз, Вы можете исключить её из подбора)":
				return "You can select manufacturers, series or specific numbers of crystals to create the pattern (for example, for the situation when you know for sure that you can't get any series of crystals, you can exclude it from the selection)";
			case "Выбор размера страз, настройки печати и сохранения схем":
				return "Selecting the sizes of crystals, settings of printing and saving patterns";
			case "в стразах:":
				return "in crystals:";
			case "До окончания Вашей лицензии: ":
				return "Till the end of your license: ";
			case " дн.":
				return " days";
			case "Ваша лицензия на программу истечёт через ":
				return "Your license for the program ends in ";
			case " дн.\r\n\r\n\r\nСейчас Вы можете продлить программу со скидкой!\r\n\r\nОбратите внимание, что, продляя работу программы ДО истечения лицензии, Вы ничего не теряете, т.к. продление лицензии будет активировано именно со дня окончания текущей лицензии, а не со дня оплаты!":
				return "days \r\n\t\t\t\t\t\t\r\n\r\nNow you can extend the program discounted!\r\n\r\nPlease note that extending the program UNTIL the license expires, you do not lose anything, because the validity period will be completed exactly from the date of termination of the license, not from the date of payment!";
			case "Продлить программу по скидке!":
				return "Extend the program discounted!";
			case "Программа для создания картин по номерам":
				return "Program for creating paintings by numbers";
			case "картина":
				return "painting";
			case "картиной":
				return "by painting";
			case "картину":
				return "painting";
			case "картине":
				return "for painting";
			case "КАРТИНУ":
				return "PAINTING";
			case "Произошла ошибка при загрузке ВАШИХ цветов ":
				return "Error while loading YOUR colors";
			case ". Возможно, что Вы пробовали вручную отредактировать этот файл и допустили ошибку форматирования.\r\n                \r\nЕсли Вам надо сохранить всё, что Вы туда занесли - то перешлите файл, расположенный по адресу 'C:\\Пользователи\\ИМЯ_ПОЛЬЗОВАТЕЛЯ_WINDOWS\\AppData\\Local\\":
				return ". Maybe you tried to manually edit this file and made a formatting error.\r\n\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\r\nIf you need to save everything that you put there - forward the file located at 'C:\\Users\\USER_NAME\\AppData\\Local\\";
			case "нам на почту ":
				return "to our e-mail ";
			case " и поясните, что произошла эта ошибка (обратите внимание, что часть папок по этому пути являются скрытыми, и надо в настройках системы через Панель управления сделать, чтобы скрытые папки показывались). \r\n                \r\nЕсли же Вам не важно сохранение этого файла в его текущем виде - просто удалите его и запустите программу заново - всё должно запуститься в обычном режиме, без ошибок.":
				return " with note of this error (note that some folders on this path are hidden, and you need to make the hidden folders are shown (in the the system settings via Control Panel).\r\n\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\r\nIf you do not need save this file in its current form - just delete it and run the program again. Everything should start in normal mode without errors.";
			case "Пожалуйста, подождите, пока выбранный Вами на схеме цвет удалится из схемы":
				return "Please wait while the color you selected on the pattern is removed from the pattern";
			case "В данной версии программы Вы можете добавить/удалить цвета не более ":
				return "In this version of the program you can add / remove colors no more than ";
			case " раз - просто для демонстрации возможностей программы. В продвинутой и мастер-версии это ограничение конечно снято.":
				return " times - just to demonstrate the capabilities of the program. In the advanced and master versions, there is no such restriction.";
			case "Для начала работы выберите более одного цвета, либо выберите тип подбора 'Простой подбор цветов'":
				return "For beginning select more than one color, or select the type of selection 'Simple selection of colors'";
			case "3. Режим рисования":
				return "3. The painting mode";
			case "4. Что делать дальше?":
				return "4. What to do next?";
			case "Теперь выбранным в пункте 2 цветом либо рисуйте на схеме (если в пункте 3 выбрали 'Кисть'), либо заливайте области (если в пункте 3 выбрали 'Заливку').":
				return "Now draw on the pattern with selected in step 2 color (if you select 'Brush' in step 3) or fill the areas (if you selected 'Fill' in step 3).";
			case "Теперь выбранным в пункте 2 цветом либо рисуйте на картине (если в пункте 3 выбрали 'Кисть'), либо заливайте области (если в пункте 3 выбрали 'Заливку').":
				return "Now draw on the painting with selected in step 2 color (if you select 'Brush' in step 3) or fill the areas (if you selected 'Fill' in step 3).";
			case "4. Режим рисования":
				return "4. The painting mode";
			case "5. Что делать дальше?":
				return "5. What to do next?";
			case "Теперь выбранным в пункте 3 цветом либо рисуйте на схеме (если в пункте 4 выбрали 'Кисть'), либо заливайте области (если в пункте 4 выбрали 'Заливку').":
				return "Now draw on the pattern with selected in step 3 color (if you select 'Brush' in step 4) or fill the areas (if you selected 'Fill' in step 4).";
			case "Теперь выбранным в пункте 3 цветом либо рисуйте на картине (если в пункте 4 выбрали 'Кисть'), либо заливайте области (если в пункте 4 выбрали 'Заливку').":
				return "Now draw on the painting with selected in step 3 color (if you select 'Brush' in step 4) or fill the areas (if you selected 'Fill' in step 4).";
			case "Сперва загрузите изображение":
				return "Firstly upload an image";
			case "Внимание!":
				return "Attention!";
			case "Укажите число цветов в схеме и размер схемы в сантиметрах":
				return "Specify the number of colors in the pattern and the size of the pattern in centimeters";
			case "Вы неверно задали размер или число цветов. В этих полях допустимы только цифры! Пожалуйста, введите эти параметры корректно и создайте схему заново.":
				return "You have incorrectly set the size or number of colors. In these fields only numbers are allowed! Please enter these parameters correctly and create the pattern again.";
			case "Вы указали слишком большой размер схемы. Пожалуйста, укажите размер схемы не более, чем":
				return "You chose too large pattern size. Please choose the size of the pattern no more than";
			case " сантиметров.":
				return " centimeters.";
			case "Пожалуйста, укажите не более 183 цветов в схеме":
				return "Please, choose no more than 183 colors in the pattern";
			case "Пожалуйста, подождите, пока схема полностью сгенерируется. Обычно это занимает от пары секунд до 1 минуты ...":
				return "Please wait the pattern generating. It usually takes from a couple of seconds to 1 minute ...";
			case "Пожалуйста, подождите, пока схема полностью сгенерируется. Обычно это занимает от 5 секунд до 3~10 минут ...":
				return "Please wait the pattern generating. It usually takes from 5 seconds to 3~10 minutes ...";
			case "Пожалуйста, подождите, пока схема полностью сгенерируется. При 'среднем' и 'лучшем' качестве контуров это может занять от 10 секунд до 30 минут и даже более (на слабых компьютерах).":
				return "Please wait the pattern generating. With 'average' and 'best' quality of outline, it takes from 10 seconds to 30 minutes and even more (on slow computers).";
			case " из":
				return " from";
			case "Произошла ошибка. Вероятнее всего, мощности компьютера (оперативной памяти) не хватило для создания раскраски такого большого размера. Попробуйте уменьшить размер раскраски в см, либо воспользуйтесь более мощным компьютером. Извините за неудобства.":
				return "Error. Most likely, the computer's output (RAM's) was not enough to create such a large coloring. Try to reduce the size of the coloring in centimeters, or use a more powerful computer. Sorry about the inconvenience.";
			case "Произошла ошибка. Вероятнее всего, мощности компьютера (оперативной памяти) не хватило для создания раскраски такого большого размера. Попробуйте уменьшить размер раскраски в см и/или указать более плохое качество контуров в 'Важных настройках' (чем выше качество - тем больше оперативой памяти использует программа), либо воспользуйтесь более мощным компьютером. Извините за неудобства.":
				return "Error. Most likely, the computer's output (RAM's) was not enough to create such a large coloring. Try to reduce the size of the coloring in centimeters and/or put the low quality of the outline in the 'Important settings' (the higher the quality - the more RAM memory the program uses), or use a more powerful computer. Sorry about the inconvenience.";
			case "Мощности Вашего компьютера (оперативной памяти) не хватило для создания схемы такого большого размера. Поэтому либо уменьшите размер схемы в см, либо зайдите в пункт 'Прочие настройки' (через соотв. кнопку над кнопкой 'Создать схему') и установите там мЕньшие значения для 'Размер ":
				return "The output of your computer (RAM) was not enough to create such big pattern. Reduce the size of the pattern in centimeters or go to the 'Other settings' (via the corresponding button above the button 'Create a pattern') and set there the lower numbers for'The size ";
			case " на схеме в процессе работы (в пикселях)'. И затем создайте схему заново":
				return " on the pattern during the work (in pixels)'. And then create a new pattern";
			case "Заказ ЭТОЙ вышивки за ":
				return "Order this embroidery for ";
			case "₽":
				return "$";
			case "?":
				return "?";
			case "Выберите папку с сохранённой ранее схемой":
				return "Select folder with a previously saved pattern";
			case "Выберите папку с сохранённой ранее картиной":
				return "Select folder with a previously saved painting";
			case "Пожалуйста, подождите секундочку, пока схема загрузится...":
				return "Please wait a second while the pattern loads...";
			case "ПРОГРАММА НЕ ЗАВИСЛА! Процесс загрузки сохранённой ранее картины по номерам может занимать до 5-10 минут. При этом все элементы программы не активны. Ожидайте, пожалуйста...":
				return "THE PROGRAM DOES NOT FROZE! The process of loading the previously saved painting by numbers can take up to 5-10 minutes. During this process all program elements are not active. Wait, please...";
			case "Вы указали папку, в которой нет файла сохранённой схемы (*.dat)":
				return "You chose a folder without saved pattern file (*.dat)";
			case ". Возможно, Вы пытаетесь загрузить в программу более ранние сохранения, когда программа ещё не сохраняла файлы проекта с расширением *.dat":
				return ". Maybe you are trying to load earlier projects saved into the program when it has not yet saved the project files *.dat";
			case "Произошла какая-то ошибка при загрузке схемы в программу. Пожалуйста, сообщите разработчикам, мы попробуем исправить эту проблему.":
				return "There was some error with loading the pattern. Please tell the developers, we will try to fix this problem.";
			case "Исходное изображение":
				return "Original image";
			case "Выберите изображение, из которого необходимо создать схему":
				return "Select the image for creating the pattern";
			case "Изображения (*.bmp, *.jpg, *.jpeg, *.png)|*.bmp;*.jpg;*.jpeg;*.png)":
				return "Images (*.bmp, *.jpg, *.jpeg, *.png)|*.bmp;*.jpg;*.jpeg;*.png)";
			case "Пожалуйста, подождите секундочку, пока изображение загрузится...":
				return "Please wait a second while the image loads ...";
			case "При загрузке исходного изображения произошла ошибка - возможно, формат изображения не подходит.Пожалуйста, загрузите другое исходное изображение, либо сохраните это же изображение в другом формате и попробуйте загрузить его вновь. Извините за неудобства.":
				return "Error while loading the original image. Maybe the image format may not be suitable. Please upload another image, or save the same image different format and try to download it again. Sorry about the inconvenience.";
			case "Выберите пустую папку для сохранения схемы":
				return "Select an empty folder to save the pattern";
			case "Исходное*":
				return "Original*";
			case "В этой папке уже содержится сохранённая ранее схема. Перезаписать её (все изображения в папке будут удалены)?":
				return "This folder already contains the previously saved pattern. Do you want to overwrite it (all images in the folder will be deleted)?";
			case "Пожалуйста, подождите, пока схема сохранится на компьютер. Обычно это занимает от пары секунд до 2 минут.":
				return "Please wait saving the pattern. Usually it takes from a couple of seconds to 2 minutes.";
			case "Пожалуйста, подождите, пока схема сохранится на компьютер. Обычно это занимает от 5 секунд до 3~10 минут...":
				return "Please wait saving the pattern. Usually it takes from 5 seconds to 3~10 minutes.";
			case "Пожалуйста, подождите, пока происходит печать схемы (обычно это занимает от нескольких секунд до 2 минут)...":
				return "Please wait while the pattern is printed (usually it takes from a few seconds to 2 minutes) ...";
			case "Пожалуйста, подождите, пока выбранный Вами из исходного изображения цвет добавится в схему":
				return "Please wait while the color you selected from the original image is added to the pattern";
			case "Возможно, Вы не сохранили изменения в схеме. Действительно закрыть программу?\r\n\r\n(Да=закрыть, не сохранять; Отмена=не закрывать, чтобы сохранить схему).":
				return "Maybe you did not save the changes in the pattern. Are you sure to close the program?\r\n\r\n(Yes=close, do not save; Cancel=do not close to save the pattern).";
			case "/Инструкция по работе с программой ":
				return "/How to use the program ";
			case "На Вашем компьютере не установлена программа для просмотра файла в формате RTF/docx (в этом формате сохранена инструкция). . Пожалуйста, установите такую программу (например, 'Microsoft Word', 'Open Office Writer', 'WordPad') - и тогда Вы сможете просмотреть инструкцию.":
				return "The program for viewing the file in RTF/docx format is not installed on your computer (the instruction saved with this format).  . Please install such a program (for example, 'Microsoft Word', 'Open Office Writer', 'WordPad') - and then you can view the instruction.";
			case "Пожалуйста, подождите - удаляются одиночные крестики из схемы":
				return "Please wait while single stitches are deleted from the pattern";
			case "мастер-версии":
				return "master-version";
			case "коммерческой версии":
				return "commercial version";
			case "В данной версии программы Вы можете делать для схемы не более ":
				return "This version of the program allows you make no more than ";
			case "В данной версии программы Вы можете делать для картины не более ":
				return "This version of the program allows you make no more than ";
			case " редактирований (заливок, либо нажатий кистью, либо замены цветов) - просто для демонстрации возможностей программы. В ":
				return " editing for pattern (filling, or brushing, or replacing colors) - just to demonstrate the capabilities of the program. At ";
			case " это ограничение конечно же снято.":
				return " this restriction removed.";
			case "не выбран":
				return "not selected";
			case "Спасибо за Ваш вопрос! Мы ответим Вам в течение суток.":
				return "Thank you for your question! We will reply within 24 hours.";
			case "К сожалению, не удалось отправить Ваш вопрос. Проверьте соединение с интернетом, попробуйте немного уменьшить длину вопроса, либо отправьте вопрос самостоятельно на нашу почту ":
				return "Unfortunately, we could not send your question. Check the connection to the Internet, try to reduce the length of the question, or send the question yourself to our mail ";
			case "Введите тему Вашего вопроса":
				return "Enter the subject of your question";
			case "Введите текст Вашего вопроса":
				return "Enter the text of your question";
			case "Введите Ваш адрес эл.почты - на него мы отправим Вам ответ!":
				return "Enter your e-mail address - we will send you an answer!";
			case "ДОМАШНЕЙ":
				return "HOME";
			case "ПРОФЕССИОНАЛЬНОЙ":
				return "PROFESSIONAL";
			case "КОММЕРЧЕСКОЙ":
				return "COMMERCIAL";
			case "БАЗОВОЙ":
				return "BASE";
			case "ПРОДВИНУТОЙ":
				return "ADVANCED";
			case "МАСТЕР":
				return "MASTER";
			case "полной":
				return "full";
			case "Поздравляем с приобретением ":
				return "Congratulations on your purchase ";
			case " версии программы! Надеемся, что программа придётся Вам по душе. Если у Вас появятся любые вопросы - всегда пишите нам через вкладку 'Есть вопрос?' в программе, либо напрямую на эл.почту ":
				return " version of the program! We hope you will like the program. If you have any questions - always write to us through the tab 'Do you have a question?' in the program, or directly on e-mail ";
			case "На указанный Вами e-mail (":
				return "To your e-mail (";
			case ") есть оплата, но для более простой версии программы. Если это неверно - пожалуйста, укажите иной e-mail (в следующем окне), либо свяжитесь с нами по эл.почте ":
				return ") there is a payment, but for a simpler version of the program.If this is incorrect please add another e-mail (in the next window), or contact us by e-mail ";
			case "Нет оплаты на указанный Вами e-mail (":
				return "No payment to your e-mail (";
			case "). Укажите иной e-mail (на который есть оплаченный заказ), либо выполните оплату для этого e-mail'а. Если это ошибка - свяжитесь с нами по эл.почте ":
				return "). Add another e-mail (with paid order), or pay by this e-mail. If this is an error contact us by e-mail ";
			case "Превышено число активаций программы на данный адрес электронной почты (":
				return "The number of program activations to this e-mail address is exceeded (";
			case ").\r\nУкажите иной e-mail (на который есть оплаченный заказ).\r\n\r\nЕсли это ошибка (например, у Вас на компьютере работала оплаченная версия, срок оплаты которой НЕ истёк, и вдруг появилось это окно; или Вы сменили компьютер/переустановили Windows/и т.п.) - ОБЯЗАТЕЛЬНО свяжитесь с нами по эл.почте ":
				return ").\r\nAdd another e-mail (with paid order).\r\n\r\nIf this is an error (for example, you had a paid version on your computer, the payment term of which DID NOT expire, and suddenly this window appeared; or you have changed the computer/reinstalled Windows/etc) - FOR SURE contact us by e-mail";
			case "\r\nМы бесплатно сделаем для Вас новую активацию программы.":
				return "\r\nWe will make a new activation of the program for free.";
			case "Оплаченный период пользования программой для этого адреса электронной почты (":
				return "Paid period of using the program for this email address (";
			case ") истёк. Укажите иной e-mail (на который есть оплаченный заказ), либо продлите оплату для данного e-mail'а. Если это ошибка - свяжитесь с нами по эл.почте ":
				return ") has expired. Add another e-mail (with paid order), or extend the payment for this e-mail. If this is an error contact us by e-mail ";
			case "Возникли помехи связи при проверке оплаты. Пожалуйста, проверьте подключение к интернету и повторите попытку, либо скопируйте информацию об ошибке (см.текст ниже) и пришлите нам на эл.почту ":
				return "There was a communication error when checking the payment. Please check the internet connection and try again, or copy the information about the error (see the text below) and send it to us by e-mail ";
			case "Пожалуйста, укажите Ваш e-mail (который Вы указывали при оплате) в следующем окне для проверки оплаты":
				return "Please add your e-mail (which you add when paying) in the next window for checking payment";
			case "Заменить на ближайший цвет из схемы":
				return "Replace with the similar color from the pattern";
			case "Заменить на похожий цвет:":
				return "Replace with a similar color:";
			case "Заменить на цвет, фильтр по названию: ":
				return "Replace with color, filter by name:";
			case "Подсветить цвет":
				return "Highlight the color";
			case "Нельзя удалить из схемы последний цвет":
				return "You can not remove the last color from the pattern";
			case " редактирований (заливок, нажатий кистью либо замены цветов) - просто для демонстрации возможностей программы. В мастер-версии это ограничение снято.":
				return " editing (fills, brushings or changing colors) - just to demonstrate the features of the program. In the master-version this restriction is removed.";
			case "Спасибо за Ваш заказ! Мы ответим Вам в течение суток. (P.S.: пожалуйста, не забудьте сохранить эту схему на Ваш компьютер через кнопку 'Сохранить')":
				return "Thank you for your order! We will reply within 24 hours. (P.S.: please do not forget to save this pattern to your computer via the 'Save' button)";
			case "К сожалению, не удалось отправить Ваш заказ. Проверьте соединение с интернетом, либо отправьте заказ вручную, для этого сохраните все файлы схемы в архив и пришлите её на нашу почту ":
				return "Unfortunately, we could not send your order. Check the connection to the Internet, or send the order manually. To do this, save all the pattern files to an archive and send it to our e-mail ";
			case "Приносим извинения, но заказ расходных материалов к созданной схеме возможен только при создании схемы под производителей ":
				return "Sorry, but the order of consumables to the designed pattern is availableonly when creating a pattern for manufacturers ";
			case ", не при 'Простом подборе цветов'. Пожалуйста, пересоздайте схему с подбором цветов производителей ":
				return ", not with 'Simple color selection'. Please re-create the pattern with the selection of colors of manufacturers ";
			case " - тогда возможность заказа расходных материалов станет доступна.":
				return " - then the possibility of ordering consumables will be available.";
			case "Недоступно при 'Простом подборе цветов'":
				return "Not available with 'Simple color selection'";
			case "Недоступно при подборе смешения цветов":
				return "Not available when selecting of colors blending";
			case "Приносим извинения, но заказ расходных материалов к созданной картине возможен только при создании картины БЕЗ подбора пропорций смешения цветов. Пожалуйста, выключите функцию смешения в 'Важных настройках' и пересоздайте картину - тогда возможность заказа расходных материалов к картине станет доступна.":
				return "Sorry, but the order of consumables for the created painting is available only when you design a picture WITHOUT selecting the proportions of colors blending. Please turn off the blending function in 'Important settings' and recreate the picture. Then the ability to order consumables to the painting will be available.";
			case " квадратных страз (НЕ круглых!); ":
				return " square crystals (NOT round!);";
			case " страз размером 2.5мм (размер страз Вы изменили в окне 'Прочих настроек'); ":
				return " crystals size 2.5mm (crystals size you changed in the window 'Other settings');";
			case " страз DMC, а у Вас в стразах присутствуют оттенки из 'Моя палитра' (пересоздайте схему, убрав галочки с этой палитры); ":
				return " DMC crystals, and you have shades of 'My Palette' (recreate the pattern, removing the checkmark with this palette)";
			case "Приносим извинения, но заказ расходных материалов к созданной схеме возможен только для ":
				return "Sorry, but the order of consumables to the created pattern is available only for ";
			case ". Пожалуйста, исправьте эти недочёты и пересоздайте схему заново - заказ материалов к схеме станет доступен!":
				return ". Please correct these shortcomings and re-create the pattern again. And materials ordering for the pattern will be available!";
			case "Пересоздайте схему с другими настройками":
				return "Re-create the pattern with other settings";
			case "Приносим извинения, но заказ расходных материалов к созданной схеме возможен только в оплаченной версии программы":
				return "Sorry, but the order of consumables to the created is available only in the paid version of the program";
			case "Недоступно в тестовой версии":
				return "Unavailable in test version";
			case "Ошибка!":
				return "Error!";
			case "Произошла ошибка формирования заказа. Пожалуйста, напишите нам об этом на нашу почту ":
				return "Error while generating the order. Please email us it about ";
			case "Программа 'Бисерок 2.0' создана в помощь рукодельницам. Она позволяет создавать схемы вышивки бисером из любых изображений!":
				return "The program 'Beads 2.0' was designed for needlewomen. It allows you to design embroidery patterns with beads of any images!";
			case "Программа 'Крестик 2.0' создана в помощь рукодельницам. Она позволяет создавать схемы вышивки крестом из любых изображений!":
				return "The program 'Cross 2.0' was designed for needlewomen. It allows you to design cross-stitch patterns of any images!";
			case "Программа 'Петелька' создана в помощь рукодельницам. Она позволяет создавать схемы вязания из любых изображений!":
				return "The program 'Stitch' was designed for needlewomen. It allows you to design kneeting patterns of any images!";
			case "Программа 'Раскраска' создана в помощь рукодельницам. Она позволяет создавать раскраски и картины по номерам из любых изображений!":
				return "The program 'Painting by numbers' was designed for needlewomen. It allows you to design colorings and paintings by numbers of any images!";
			case "Программа 'Алмазная мозаика' создана в помощь рукодельницам. Она позволяет создавать алмазные вышивки из любых изображений!":
				return "The program 'Crystals' was designed for needlewomen. It allows you to design crystals kneetings of any images";
			case "не внесён адрес эл.почты":
				return "no e-mail address";
			case "По какой-то причине программе не удалось открыть ссылку. Пожалуйста, скопируйте вручную ссылку из текстового поля ниже, вставьте в Ваш браузер и перейдите по ней самостоятельно. Спасибо!":
				return "For some reason, the program could not open the link. Please copy manually the link from the text box below, paste it into your browser. Thank you!";
			case "Программа '":
				return "Program '";
			case "' - вопрос из программы":
				return "' - question from program";
			case "Редактор своих цветов мулине":
				return "Editor of your own mouline colors";
			case "В этом окне Вы можете создать и отредактировать имеющиеся в Вашем распоряжении наборы мулине, чтобы в дальнейшем создавать под них схемы вышивки. Для начала создайте серию/серии мулине с помощью кнопки 'Добавить' (слева-внизу), и затем добавляйте к ней/ним конкретные цвета мулине.":
				return "This window you can design and edit the mouline sets you have to design patterns for them. For beginning add series of mouline ('Add' button left-bottom), and then add to it specific colors of the mouline.";
			case "Серии мулине:":
				return "Mouline series";
			case "Мулине в выбранной серии:":
				return "Mouline thread in selecter series";
			case "Редактор своих цветов пряжи":
				return "Editor of your own yarn colors";
			case "В этом окне Вы можете создать и отредактировать имеющиеся в Вашем распоряжении наборы пряжи, чтобы в дальнейшем создавать под них схемы вязания. Для начала создайте серию/серии пряжи с помощью кнопки 'Добавить' (слева-внизу), и затем добавляйте к ней/ним конкретные цвета пряжи.":
				return "This window you can design and edit the yarn sets you have to design patterns for them. For beginning add series of yarn ('Add' button left-bottom), and then add to it specific colors of the yarn.";
			case "Серии пряжи:":
				return "Yarn series:";
			case "Пряжа в выбранной серии:":
				return "Yarn in selected series:";
			case "Редактор своих цветов страз":
				return "Editor of your own crystals colors";
			case "В этом окне Вы можете создать и отредактировать имеющиеся в Вашем распоряжении наборы страз, чтобы в дальнейшем создавать под них схемы вышивки. Для начала создайте серию/серии страз с помощью кнопки 'Добавить' (слева-внизу), и затем добавляйте к ней/ним конкретные цвета страз.":
				return "This window you can design and edit the crystals sets you have to design patterns for them. For beginning add series of crystals ('Add' button left-bottom), and then add to it specific colors of crystals.";
			case "Серии страз:":
				return "Crystals series";
			case "Стразы в выбранной серии:":
				return "Crystals in selected series";
			case "В этом окне Вы можете создать и отредактировать имеющиеся в Вашем распоряжении наборы красок, чтобы в дальнейшем создавать под них  раскраски и картины. Для начала создайте серию/серии цветов с помощью кнопки 'Добавить' (слева-внизу), и затем добавляйте к ней/ним конкретные цвета красок.":
				return "This window you can design and edit the colors sets you have to design patterns for them. For beginning add series of colors ('Add' button left-bottom), and then add to it specific colors.";
			case "Серии красок:":
				return "Colors series:";
			case "Краски в выбранной серии:":
				return "Colors in selected series:";
			case "Текстовый файл (*.txt)|*.txt":
				return "Text file (*.txt)|*.txt";
			case "Произошла ошибка при сохранении файла.":
				return "Error while saving the file.";
			case "Выберите файл с сохранённой ранее палитрой":
				return "Select file with a previously saved palette";
			case "Файл с цветами (*.zcv, *.txt)|*.zcv;*.txt":
				return "File with colors (*.zcv,*.txt)|*.zcv;*.txt";
			case "Профессиональная версия Раскраски (больше цветов)":
				return "Professional version of 'Painting by numbers' (more colors)";
			case "ИНФОРМАЦИЯ ОБ ОШИБКЕ:":
				return "ERROR INFORMATION:";
			case "По какой-то причине программе не удалось открыть ссылку. Пожалуйста, скопируйте вручную ссылку из текстового поля ниже, вставьте в Ваш браузер и перейдите по ней самостоятельно. Спасибо":
				return "For some reason, the program could not open the link. Please copy manually the link from the text box below, paste it into your browser. Thank you!";
			case "2.3. Некоммерческое использование программы\r\nДанная лицензия подразумевает исключительно некоммерческое использование программы. Коммерческое использование программы по данной лицензии запрещено. Для получения лицензии программы в коммерческих целях - прочитайте раздел о Коммерческом использовании в верхней части данного окна.":
				return "2.3. Non-commercial usage of the program\r\nThis license allows exclusively non-commercial usage of the program. Commercial usage of the program under this license is prohibited. To obtain a commercial license for a program, read the Commercial Usage section at the top of this window.";
			case "1. Данная версия программы предназначена для личного некоммерческого использования.\r\n2. Коммерческое использование данной версии программы «":
				return "1. This version of the program is intended for personal non-commercial usage.\r\n2. Commercial usage of this version of the program «";
			case "» (получение любой экономической выгоды) противозаконно.\r\n3. Если у Вас есть желание использовать программу в коммерческих целях (мы только рады этому), то приобретите 'Коммерческую' версию программы, которая, к тому же, имеет широкие возможности редактирования картин по номерам.\r\nПосле приобретения коммерческой версии, помимо активации функций редактирования картин в программе, мы пришлём Вам фирменное заверенное нашей печатью Соглашение, подтверждающее, что Вы можете использовать нашу программу в коммерческих целях для извлечения выгоды.\r\n\r\nВерю в Вашу порядочность! Пожалуйста, помните о том, что мы искренне стараемся для Вас - дорабатываем и развиваем программу, чтобы пользоваться ей было удобно и полезно.":
				return "» (getting any economic benefit) is against the law.\r\n3. If you have a desire to use the program for commercial purposes (we are only happy about this), then buy the 'Commercial' version of the program, which, moreover, has more opportunities to edit piaitings by numbers.\r\nAfter purchasing a commercial version, in addition to activating the editing functions of the pictures in the program, we will send you a branded Agreement affirmed by our seal, confirming that you can use our program for commercial purposes for profit.\r\n\r\nWe believe your decency, please remember that we are sincerely working for you. We are finalizing and developing the program to make it convenient and useful.";
			case "Данная версия программы предназначена для коммерческого использования. Пользуйтесь, зарабатывайте с её помощью деньги, делитесь с нами идеями и пожеланиями - будем стараться всё претворить в следующих версиях программы 'Раскраска'! Удачи Вам!":
				return "This version of the program is for commercial usage. Use, earn money, share your ideas and wishes with us. We will try to use everything in the next versions of the program 'Painting by numbers'! Good luck!";
			case "2.3. Некоммерческое использование программы\r\nДанная лицензия подразумевает возможность полноценного коммерческого использования программы.":
				return "2.3. Non-commercial usage of the program\r\nThis license allows full commercial usage of the program.";
			case "» (получение любой экономической выгоды) противозаконно.\r\n3. Если у Вас есть желание использовать программу в коммерческих целях (мы только рады этому), то сделать это - элементарно:\r\n\tа. Напишите нам по этому адресу ":
				return "» (getting any economic benefit) is against the law.\r\n3. If you have a desire to use the program for commercial purposes (we are only happy about this), then it's elementary:\r\n\ta. Email us ";
			case " (либо на мою личную почту ":
				return " (or to my personal e-mail ";
			case ") письмо с темой 'Коммерческое использование программы «":
				return ") message with the topic 'Commercial use of the program «";
			case "\tб. Я вышлю Вам коммерческие условия использования программы - поверьте, они будут удобны для Вас.\r\n\tв. Составлю, подпишу с печатью и пришлю фирменное соглашение, подтверждающее, что Вы можете использовать нашу программу в коммерческих целях для извлечения выгоды.\r\n\r\nВерю в Вашу порядочность! Пожалуйста, помните о том, что мы искренне стараемся для Вас - дорабатываем и развиваем программу, чтобы пользоваться ей было удобно и полезно.":
				return "\tb. I will send you commercial terms for using the program. Believe me, they are convenient for you.\r\n \tc. I will compose, sign and stamp and send you a brand agreement confirming that you can use our program for commercial purposes for profit.\r\n\r\nWe believe your decency, please remember that we are sincerely work for you - we are finalizing and developing the program for you.";
			case "Лицензионное соглашение на использование программы «":
				return "Usage license «";
			case "»\r\nПожалуйста, прочитайте следующее лицензионное соглашение перед использованием данной компьютерной программы «":
				return "»\r\nPlease read the following license agreement before using this program «";
			case "». Использование программы «":
				return "». Program using «";
			case "» подразумевает, что Вы принимаете условия настоящего лицензионного соглашения.  Если Вы не принимаете условий данного соглашения, то Вы не имеете права использовать данную программу и ее следует незамедлительно вернуть обратно продавцу и получить обратно уплаченные деньги в том случае, если после оплаты программы прошло не более 14 дней.\r\n\r\nДанная программа защищена законами и международными соглашениями об авторских правах, а также другими законами и договорами, регулирующими отношения авторского права. Программа лицензируется, а не продается.\r\n\r\n1. Объём лицензии и права покупателя\r\n\r\n1.1. Использование программы \r\nРазрешается установка программы на одном компьютере (дополнительные установки на дополнительных компьютерах – возможны по запросу к автору Звезинцеву А.И.). Использование программы возможно лишь на срок приобретённой лицензии (как правило, срок лицензии на программу «":
				return "» implies that you accept the terms of this license agreement. If you do not accept the terms of this agreement, you are not allowed to use this program and it should be immediately returned to the seller. You will get your money back if there is no more than 14 days after the payment.\r\n\r\nThis program is protected by laws and international copyright agreements, as well as other laws and treaties governing copyright relations. The program is licensed, not sold.\r\n\r\n1. Scope of the license and buyer rights\r\n\r\n1.1. Program using\r\nIt is allowed to install the program on one computer (additional instaling on additional computers are possible upon request to the author Andrei Zvezintcev). Program using is possible only for the period of the acquired license (as a rule, the term of the license for the program «";
			case "» составляет 1  год с момента покупки). По окончании срока лицензии покупатель теряет право использования программы, поэтому программа будет автоматически заблокирована и переведена в режим тестовой версии. Для дальнейшего использования программы после окончания срока приобретённой лицензии необходимо приобрести новую лицензию на новый срок использования программы.\r\n\r\n1.2. Модификации (версии) программы\r\nПрограмма «":
				return "» is 1 year from the date of purchase). At the end of the license period, the customer loses the right to use the program, so the program will be automatically locked and transferred to the test version mode. To continue using the program after the end of the term of the purchased license, you must purchase a new license for a new period program using.\r\n\r\n1.2. Modifications (versions) of the program\r\nThe program «";
			case "» предоставляется в нескольких модификациях, начиная с тестовой. Тестовая модификация бесплатна и служит лишь для ознакомления с возможностями полных модификаций программы.  Все прочие модификации программы, помимо тестовой, являются платными и различаются по стоимости. В случае оплаты и приобретения «низшей» (менее полной, с меньшим объёмом функционала) модификации, пользователь не имеет права использовать программу в «высших» (с бОльшим объёмом функционала) модификациях. \r\n\r\n2. Oписание прочих прав и ограничений\r\n\r\n2.1. Oграничения на вскрытие технологии, декoмпиляцию и дизассемблирование.\r\nНе разрeшается осуществлять вскрытие технологии, декомпиляцию и дизассемблирование программы, также не разрешаются любые попытки открытия программного кода программы для ознакомления либо копирования. Любые подобные попытки будут наказаны согласно законодательства РФ.\r\n\r\n2.2. Прoкат и распространение программы и результатов её работы\r\nНе разрешается предоставлять программу в прокат или во временное пользование, а также создавать с помощью программы материалы для иных людей (например – запрещается через программу создавать схемы для создания изделий рукоделия для своих знакомых, либо для распространения в сети интернет или иных сетях/местах общего пользования).\r\n\r\n":
				return "» is available in several modifications, starting with the test version. Test modification is for free and serves just to demonstrate the possibilities of full modifications of the program. All other program modifications, excluding test version, are paid and vary in price. In the case of payment and purchase of 'lower' modification (less complete, with less functionality), the user has no right to use the program in the 'higher' modifications (with a larger functional).\r\n\r\n2. Description of other rights and restrictions\r\n\r\n2.1. Limitations on the opening of technology, decompilation and disassembly.\r\nIt is not allowed to perform the opening of the technology, the decompilation and disassembly of the program. Also it is not allowed to open the program code for familiarization or copying. Any such attempts will be punished according to the legislation of the Russian Federation.\r\n\r\n2.2. The distribution and dissemination of the program and the results of its work\r\nIt is not allowed to provide the program for hire or for temporary usage, and also create materials for other people (for example - it is forbidden through the program to design patterns for creating handicraft products for your friends, or for distribution on the Internet or other networks/common areas ).\r\n\r\n";
			case "\r\n\r\n2.4. Услуги по технической поддержке\r\nОбращение к Автору за технической поддержкой осуществляется через данную программу (вкладка «Есть вопрос?»), а также\r\nWeb-сайт: ":
				return "\r\n\r\nTechnical Support Services\r\nContacting the Author for technical support is provided through this program (tab 'Do you have a question?' ), as well as\r\nWebsite: ";
			case "\r\nЛюбые дополнительные программы и материалы, перeданные Вам в рeзультате оказания услуг по технической поддержке, должны рассматриваться как составная часть программы и подпадают, таким образом, под действие ограничений и условий данного соглашения.\r\n\r\n3. Ограничение по гарантийным обязательствам\r\nДанное программное обеспечение и сопутствующие файлы предоставляются такими «как они есть» («as is»), без гарантий по эксплуатации или иных гарантий (например, гарантий качества работы программы), выраженных или подразумеваемых. Все авторские права на программу «":
				return "\r\nAny additional programs and materials given to you as a result of technical support services should be considered as part of the program. Thea are also subject to the terms and conditions of this agreement.\r\n\r\n3. Warranty Limitations\r\nThis software and accompanying files are provided 'as is', without warranty of operation or other guarantees (for example, guarantees of the quality of the program operating) expressed or implied. All copyrights to the program «";
			case "» принадлежат исключительно её автору Звезинцеву А.И.\r\n\r\n4. Авторское право\r\nВсе права собственности и авторские права на программу, изображения и текст, сопровождающие их печатные материалы,  принадлежат Автору – Звезинцеву А.И.. Все права Автора на программу защищены законами и мeждународными соглашениями об авторских правах, а также другими законами и договорами, регулирующими отношения авторского права. Следовательно, с программой необходимо обращаться, как с любым другим объектом авторского права. Копирование или распространение программы и сопровождающих программу материалов запрещено и наказывается законодательством РФ.\r\n":
				return "» belong exclusively to its author Andrei Zvezintcev.\r\n\r\n4. Copyright\r\nAll rights of ownership and copyrights to the program, images and text accompanying their printed materials belong to the Author - Andrei Zvezintcev. All Author rights to the program are protected by laws and international agreements on copyrights, as well as other laws and treaties governing relations copyright. Therefore, the program must be treated as with any other object of copyright. Copying or distribution of the program and materials accompanying the program is prohibited and punished by the legislation.\r\n";
			case "  ДОСТУПНО  В  ПРОФЕСС.  И  КОММЕРЧ.  ВЕРСИЯХ  ":
				return "  AVAILABLE IN PROFESS. AND COMMERC. VERSIONS  ";
			case "Ограничения тестовой версии программы":
				return "Limitations of the test version of the program";
			case "Тестовая версия создана лишь для ознакомления с возможностями программы (поэтому полноценно использовать её не получится):":
				return "The test version was created only to show the program's capabilities (so it's not possible to fully use it):";
			case "Ограничения базовой версии программы":
				return "Limitations of the base version of the program";
			case "В базовой версии программы Вы можете полноценно использовать лишь первую вкладку 'Шаг 1. Создание схемы'. В другие две вкладки внедрены ограничения, чтобы использовать их было нельзя:":
				return "In the Base version of the program you can fully use only the first tab 'Step 1: Pattern design'. The other two tabs are constrained to not be used:";
			case "Ограничения домашней версии программы":
				return "Limitations of the home version of the program";
			case "В домашней версии Вы уже можете полноценно использовать программу, но с ограничениями, которые полностью сняты в Профессиональной и Коммерческой версиях:":
				return "In the Home version you can already fully use the program, but with limitations that are completely removed in the Professional and Commercial versions:";
			case "В продвинутой версии программы Вы можете полноценно использовать только первую и вторую вкладки программы ('Шаг 1. Создание схемы' и 'Шаг 2. Доработка'). В третью вкладку 'Ручное редактирование' внедрены ограничения, чтобы использовать её было нельзя:":
				return "In the Advanced version of the program you can fully use only the first  and the second tabs of the program ('Step 1. Pattern design' and 'Step 2. Finalizing'). The third tab 'Manual Editing' is constrained to not be used:";
			case "Ограничения продвинутой версии программы":
				return "Limitations of the Advanced version of the program";
			case "В профессиональной версии программы Вы можете полноценно использовать все возможности первой вкладки программы 'Шаг 1. Создание картины по номерам'. Во вторую вкладку 'Шаг 2. Редактирование картины' внедрены ограничения, чтобы использовать её было нельзя:":
				return "In the Professional version of the program you can fully use all the features of the first tab of the program 'Step 1. Design painting by numbers'. The second tab 'Step 2. Editing a painting' is constrained to not be used:";
			case "Ограничения профессиональной версии программы":
				return "Limitations of the Professional version of the program";
			case "1. Ограничение по числу цветов своей палитры: до 12 цветов (в профессиональной версии число цветов своей палитры увеличено до 80. Больше - на спец.условиях)":
				return "1. Limit of the number of colors in your palette: up to 12 colors (in the Professional version the number of colors in your palette is increased to 80. More - on special conditions)";
			case "2. В картах ключей НЕ указывается площадь, закрашиваемая каждым цветом, и НЕ указывается необходимый объём каждого цвета в мл (в профессиональной версии всё это будет указано).":
				return "2. The key cards do NOT specify the area filled by each color, and the required volume of each color in ml is NOT indicated (in the Professional version all this is indicated).";
			case "3. Нет возможности ставить минимальные изменения исходного изображения при создании картины.":
				return "3. There is no possibility to put minimal changes to the original image when creating a painting.";
			case "4. Отсутствует возможность создавать схемы с высоким качеством контуров (имеется 'пикселизация' контуров) и настраивать качество и толщину контуров картины.":
				return "4. There is no possibility to design patterns with high quality of contours (there is a 'pixelization' of contours) and adjust the quality and thickness of the outline of the painting.";
			case "5. Нет возможности проводить предобработку исходного изображения.":
				return "5. It is not possible to preprocessing the original image.";
			case "6. Нет возможности создавать картины размером более, чем 120х120см (можно только в ком.версии).":
				return "6. There is no possibility to design a painting larger than 120х120cm (it is possible only in a commercial version).";
			case "1. Ограничение по числу цветов: можно создавать ":
				return "1. Limits in the number of colors: you can design ";
			case " лишь с 25 цветами":
				return " just with 25 colors";
			case "2. Ограничение по размеру: ":
				return "2. Limits on the size: ";
			case " создаются лишь под размер 25см по большей стороне":
				return " are designed only for the size of 25cm on the larger side";
			case "3. Нельзя создавать картины под готовые палитры цветов (производителей красок) - доступен только 'Простой подбор цветов'":
				return "3. You can not design paintings for ready-made color palettes (painters) - only 'Simple color selection' is available:";
			case "4. Нельзя изменять настройки детализации создаваемой картины по номерам":
				return "4. You can not change the detail settings of the designed painting by the numbers";
			case "3. Схемы создаются только под производителей ":
				return "3. Pattern are designed only for manufacturers";
			case ", нельзя создать схемы для самостоятельного подбора цветов или выбрать нужные серии/номера ":
				return ", you can not create patterns for self-selection of colors or choose the desired series/numbers";
			case "4. Нельзя создать схемы для вышивки 'кирпичиком' или 'мозаикой', только простые схемы":
				return "4. You can not design patterns for embroidery 'brick' or 'mosaic', only simple patterns";
			case "4. Нет возможности проводить предобработку исходного изображения.":
				return "4. It is not possible to preprocessing the original image.";
			case "1. Надписи 'тестовая/демо-версия' почти на всех результатах сохранения/печати":
				return "1. The inscription 'test/demo version' on almost all the results of saving/printing";
			case "2. Сохраняется лишь первый лист легенды (карты цветов), остальные - пусты":
				return "2. Only the first sheet of the legend (color map) is preserved, the others are empty";
			case "3. В карте цветов показываются лишь первые 6 цветов":
				return "3. Only the first 6 colors are shown in the color map";
			case "4. Нет возможности создавать схемы с высоким качеством контуров (имеется 'пикселизация' контуров)":
				return "4. There is no possibility to design patterns with high quality of contours (there is 'pixelization' of contours)";
			case "3. На общей схеме показан лишь первый лист схемы, остальные листы искажены чередующимися пустыми квадратами":
				return "3. The general pattern shows only the first sheet of the pattern, other sheets have empty squares";
			case "4. Сохраняется лишь первый лист схемы, остальные - пусты":
				return "4. Only the first sheet of the pattern is saved, other are empty";
			case "5. В легенде не указываются необходимые каталожные номера ":
				return "5. The legend does not specify the required catalog numbers of";
			case "6. В схеме для печати на ткани показывается лишь часть бисеринок, остальные отсутствуют (выглядит белой мозаикой)":
				return "6. Pattern for printing on the fabric has only a part of the beads, another are absent (it looks like white mosaic)";
			case "6. В схеме для печати показывается лишь часть страз, остальные отсутствуют (выглядит белой мозаикой)":
				return "6. Pattern for printing has only a part of the crystals, another are absent (it looks like white mosaic)";
			case "1. Невозможно провести полноценную цветовую коррекцию (можно добавить/удалить не более 3 цветов схемы - только в целях демонстрации такой возможности)":
				return "1. It is impossible to make a full color correction (you can add/remove up to 3 colors of the pattern - just to demonstrate this option)";
			case "2. Недоступно сохранение схемы":
				return "2. Pattern saving is not available";
			case "3. Недоступна печать схемы":
				return "3. Pattern printing is not available";
			case "4. Нет возможности создавать схемы размером более, чем 120х120см (можно только в мастер-версии)":
				return "4. It is not possible to deign patterns larger than 120х120cm (it is possible only in the Master version)";
			case "5. Недоступна возможность убрать одиночные крестики":
				return "5. There is no way to remove single stitches";
			case "6. Нет возможности проводить предобработку исходного изображения.":
				return "6. There is no way to preprocessing the original image.";
			case "1. Невозможно провести редактирование ":
				return "1. There is not allowed editing of";
			case ": доступно не более 3 операций (нажатий кистью, заливок схемы либо замены цвета в схеме) - только в целях демонстрации такой возможности":
				return ": no more than 3 operations are available (brush strokes, pattern fillings or color replacement in the pattern) - just to demonstrate this option";
			case "2. Нельзя перенести ":
				return "2. Can not be moved ";
			case " из вкладки 'Доработка' на вкладку 'Ручное редактирование'":
				return "from the 'Finishing' tab to the 'Manual editing' tab";
			case "3. Недоступно сохранение ":
				return "3. Saving is not available";
			case "4. Недоступна печать ":
				return "4. Printing is not available ";
			case "5. Нет возможности создавать картины размером более, чем 120х120см (можно только в ком.версии).":
				return "5. It is impossible to design patterns larger than 120х120cm (it is possible only in the Commercial version).";
			case "6. Нет возможности подключить постоянную нумерацию своих красок в карте цветов (легенде).":
				return "6. It is not possible to connect the permanent numbering of your colors in the color map (legend).";
			case "7. Вы не имеете права использовать программу в коммерческих целях. [При приобретении коммерческой версии мы направим Вам Соглашение на Ваше имя, заверенное нашей печатью, официально разрешающее коммерческое использование программы 'Раскраска'. Кроме того, при приобретении коммерческой версии мы предоставим Вам 3 дополнительных лицензии программы.]":
				return "7. You are not allowed to use the program for commercial purposes. [When purchasing a Commercial version, we will send you an Agreement, certified by our seal, officially authorizing the commercial usage of the program 'Painting by number'. In addition, when purchasing a Commercial version, we will provide you 3 additional software licenses.]";
			case "5. Нет возможности создавать схемы размером более, чем 120х120см (можно только в мастер-версии).":
				return "5. It is impossible to design patterns larger than 120х120cm (it is possible only in the Master version).";
			case "6. Нет возможности создавать схемы размером более, чем 120х120см (можно только в мастер-версии).":
				return "5. It is impossible to design patterns larger than 120х120cm (it is possible only in the Master version).";
			case "В полной версии программы таких ограничений нет - она позволяет строить полнофункциональные схемы вышивки бисером из любых изображений!":
				return "In the full version of the program there are no such limitations - it allows you to design full-featured beading patterns of any images";
			case "В полной версии программы таких ограничений нет - она позволяет строить полнофункциональные схемы вышивки крестом из любых изображений!":
				return "In the full version of the program there are no such limitations - it allows you to design full-featured cross-stitch patterns of any images!";
			case "В полной версии программы таких ограничений нет - она позволяет строить полнофункциональные схемы вязания из любых изображений!":
				return "In the full version of the program there are no such limitations - it allows you to design full-featured knitting patterns of any images!";
			case "Вкладка 'Шаг 1. Создание картины по номерам'":
				return "Tab 'Step 1. Design painting by numbers'";
			case "Ограничения работы на вкладке 'Шаг 1. Создание картины по номерам':":
				return "Limitations on the tab 'Step 1: Design painting by numbers':";
			case "Вкладка 'Шаг 2. Редактирование картины'":
				return "Tab 'Step 2. Editing the painting'";
			case "Ограничения работы на вкладке 'Шаг 2. Редактирование картины'":
				return "Limitations on the tab 'Step 2: Editing the painting'";
			case "В полной версии программы таких ограничений нет - она позволяет создавать полноценные картины по номерам из любых изображений!":
				return "In the full version of the program there are no such limitations. It allows you to create paintings by numbers of any images!";
			case "В профессиональной версии этих ограничений нет!":
				return "There is no such limitations in Professional version";
			case "Купить ПРОФЕССИОНАЛЬНУЮ версию программы! (через заказ на сайте)":
				return "Buy PROFESSIONAL version of the program! (via order on the site)";
			case "В коммерческой версии этих ограничений нет - Вы сможете пользоваться ВСЕМИ возможностями программы!":
				return "In the Commercial version there is no such limitations - you are able to enjoy ALL of the program's features!";
			case "Купить КОММЕРЧЕСКУЮ версию программы! (через заказ на сайте)":
				return "Buy COMMERCIAL version of the program! (via order on the site)";
			case "В полной версии программы таких ограничений нет - она позволяет строить полнофункциональные схемы алмазной мозаики из любых изображений!":
				return "In the full version of the program there are no such limitations - it allows you to design patterns of crystal mosaicof any images!";
			case "У Вас установлен размер страз, отличный от стандартного (стандартный размер=2.5мм). Сохранить нестандартный размер страз?":
				return "You have a crystal size different from the standard one (standard size=2.5mm). Do you want to save a non-standard crystal size?";
			case "У Вас в настройках установлен размер страз, отличный от стандартного (стандартный размер=2.5мм). Продолжить создание схемы с нестандартным размером страз?":
				return "You have a crystal size different from the standard (standard size=2.5mm). Continue designing a pattern with a non-standard crystal size?";
			case "Размер страз не равен стандартному":
				return "The size of crystals is not equal to the standard";
			case "Плотность вязания:":
				return "Knitting closeness:";
			case "Вид вязания составлен из петелек (если не выбрано, то - из квадратиков)":
				return "The outfit of knitting is made up of stitches (if not selected, of the squares)";
			case "'Диффузия' - с ней вязание реалистичней, но вывязать сложнее":
				return "'Diffusion' makes your knitting more realistic but difficult";
			case "Размер петли на схеме в процессе работы (в пикселях):":
				return "The size of one stitch on the pattern during working (in pixels):";
			case "Размер петли на схеме после сохранения (в пикселях):":
				return "The size of one stitch on the pattern after saving (in pixels):";
			case "Размер одной страницы в петлях:":
				return "The size of one page in the stitches:";
			case "8 петель на 10см, 10 рядов на 10см (40-60м в 100гр. пряжи)":
				return "8 stitches per 10cm, 10 rows per 10cm (40-60m in 100g of yarn)";
			case "12 петель на 10см, 16 рядов на 10см (70-90м в 100гр. пряжи)":
				return "12 stitches per 10cm, 16 rows per 10cm (70-90m in 100g of yarn)";
			case "14 петель на 10см, 18 рядов на 10см (90-120м в 100гр. пряжи)":
				return "14 stitches per 10cm, 18 rows per 10cm (90-120m in 100g of yarn)";
			case "16 петель на 10см, 24 ряда на 10см (120-180м в 100гр. пряжи)":
				return "16 stitches per 10cm, 24 rows per 10cm (120-180m in 100g of yarn)";
			case "18 петель на 10см, 24 ряда на 10см (190-220м в 100гр. пряжи)":
				return "18 stitches per 10cm, 24 rows per 10cm (190-220m in 100g of yarn)";
			case "20 петель на 10см, 30 рядов на 10см (230-300м в 100гр. пряжи)":
				return "20 stitches per 10cm, 30 rows per 10cm (230-300m in 100g of yarn)";
			case "22 петли на 10см, 30 рядов на 10см (280-320м в 100гр. пряжи)":
				return "22 stitches per 10cm, 30 rows per 10cm (280-320m in 100g of yarn)";
			case "24 петли на 10см, 32 ряда на 10см (310-350м в 100гр. пряжи)":
				return "24 stitches per 10cm, 32 rows per 10cm (310-350m in 100g of yarn)";
			case "'Диффузия' в вышивке - с ней вышивка реалистичней, но вышивать сложнее":
				return "'Diffusion' makes embroidery more realistic, but more difficult to needle";
			case "Размер бисера:":
				return "Beads size:";
			case "Вид вышивки составлен из бусинок (если не выбрано, то - из квадратиков)":
				return "The outfit of embroidery is made up of beads (if not selected, of the squares)";
			case "Рисовать мини-легенду на главной схеме и схеме для печати на ткани":
				return "Draw mini-legend on the main pattern and pattern for printing on fabric";
			case "Размер бусинки на схеме в процессе работы (в пикселях):":
				return "The size of the bead on the pattern during working (in pixels):";
			case "Размер бусинки на схеме после сохранения (в пикселях):":
				return "Bead size in the pattern after saving (in pixels):";
			case "Размер одной страницы в бусинках:":
				return "The size of one page in beads:";
			case "Размер канвы:":
				return "Kanwa size:";
			case "Вид вышивки составлен из стежков (если не выбрано, то - из квадратиков)":
				return "The outfit of embroidery is made up of stitches (if not selected, of the squares)";
			case "Рисовать мини-легенду на главной схеме":
				return "Draw mini-legend on the main pattern";
			case "Размер крестика на схеме в процессе работы (в пикселях):":
				return "The size of the cross on the pattern during working (in pixels):";
			case "Размер крестика на схеме после сохранения (в пикселях):":
				return "The size of the cross on the pattern after saving (in pixels):";
			case "Размер одной страницы в стежках:":
				return "The size of one page in stitches:";
			case "Аида 6":
				return "Aida 6";
			case "Аида 7":
				return "Aida 7";
			case "Аида 8":
				return "Aida 8";
			case "Аида 9":
				return "Aida 9";
			case "Аида 10":
				return "Aida 10";
			case "Аида 11":
				return "Aida 11";
			case "Аида 12":
				return "Aida 12";
			case "Аида 13":
				return "Aida 13";
			case "Аида 14":
				return "Aida 14";
			case "Аида 15":
				return "Aida 15";
			case "Аида 16":
				return "Aida 16";
			case "Аида 18":
				return "Aida 18";
			case "Аида 20":
				return "Aida 20";
			case "Аида 22":
				return "Aida 22";
			case "Канва K02 'Gamma'":
				return "K02 'Gamma'";
			case "Страмин Gamma К-19":
				return "Stramin Gamma K-19";
			case "'Диффузия' в вышивке - с ней вышивка реалистичней, но выкладывать посложнее":
				return "'Diffusion' makes embroidery more realistic, but more difficult to make";
			case "Размер стразы в мм:":
				return "Size of crystal in mm:";
			case "Вид мозаики составлен из страз (если не выбрано, то - из квадратиков)":
				return "The form of the mosaic is made up of crystals (if not selected, of the squares)";
			case "Рисовать мини-легенду на главной схеме и схеме для печати":
				return "Draw a mini-legend on the main pattern and pattern for printing";
			case "Размер стразы на схеме в процессе работы (в пикселях):":
				return "The size of one crystal on the pattern during working (in pixels):";
			case "Размер стразы на схеме после сохранения (в пикселях):":
				return "The size of one crystal on the pattern after saving (in pixels):";
			case "Размер одной страницы в стразах:":
				return "Size of one page in crystals:";
			case "В поле размера можно вводить только цифры, в качестве разделителя между целой и дробной частью может использоваться точка или запятая":
				return "In the size field, you can enter only digits, as the separator between the integer and the fractional part you can use dot or a comma";
			case "В поле размера можно вводить только цифры":
				return "In the size field, you can enter only digits";
			case "В это поле можно вводить только цифры":
				return "This field you can enter only digits";
			case "В это поле можно вводить только целые числа":
				return "This field you can enter only integers";
			case "Пожалуйста, введите Ваш адрес электронной почты (e-mail), который Вы указывали при оплате программы:":
				return "Please enter your e-mail, which you added when paying for the program:";
			case "Запуск":
				return "Starting";
			case " Бисерок 2.0":
				return "Beads 2.0";
			case " Крестик 2.0":
				return "Cross 2.0";
			case " Петелька":
				return " Stitch";
			case " Алмазная мозаика":
				return " Crystals";
			case " Раскраска":
				return " Painting by numbers";
			case "Для проверки оплаты Вам необходимо ввести Ваш адрес электронной почты, который Вы указывали при оплате":
				return "To check the payment you need to enter your email address, which you specified when paying";
			case "Введите Ваш e-mail...":
				return "Add your e-mail...";
			case "Нет оплаты на указанный Вами e-mail. Укажите иной e-mail (на который есть оплаченный заказ), либо выполните оплату для этого e-mail'а. Если это ошибка - свяжитесь с нами по эл.почте ":
				return "There is no payment for your e-mail. Add another e-mail (with paid order), or make a payment for this e-mail. If this is an error contact us by e-mail";
			case "Превышено число активаций программы на данный адрес электронной почты.\r\nУкажите иной e-mail (на который есть оплаченный заказ).\r\n\r\nЕсли это ошибка (например, у Вас на компьютере работала оплаченная версия, срок оплаты которой НЕ истёк, и вдруг появилось это окно; или Вы сменили компьютер/переустановили Windows/и т.п.) - ОБЯЗАТЕЛЬНО свяжитесь с нами по эл.почте ":
				return "The number of program activations to this e-mail address is exceeded.\r\nAdd another e-mail (with paid order).\r\n\r\nIf this is an error (for example, you had a paid version on your computer, the payment term of which DID NOT expire, and suddenly this window appeared;or you have changed the computer/reinstalled Windows/etc) - FOR SURE contact us by e-mail";
			case "Оплаченный период пользования программой для этого адреса электронной почты истёк. Укажите иной e-mail (на который есть оплаченный заказ), либо продлите оплату для данного e-mail'а. Если это ошибка - свяжитесь с нами по эл.почте ":
				return "The paid period of the program using for this e-mail address has expired. Add another e-mail (with a paid order) or prolong the payment for this e-mail. If this is an error please contact us by e-mail ";
			case " шт.):":
				return " pcs.)";
			case "цветов":
				return "colors";
			case "цвета":
				return "color";
			case "цвет":
				return "color";
			case "частей":
				return "parts";
			case "часть":
				return "part";
			case "части":
				return "part";
			case "контуров":
				return "outlines";
			case "контур":
				return "outline";
			case "контура":
				return "outline";
			case "Вышивка":
				return "Embroidery";
			case "Вязание":
				return "Kneeting";
			case "Картина":
				return "Painting";
			case "бисера":
				return "beads";
			case "мулине":
				return "mouline";
			case "пряжи":
				return "yarn";
			case "страз":
				return "crystals";
			case "красок":
				return "colors";
			case "картины":
				return "painting";
			case "вязания":
				return "knitting";
			case "вышивки":
				return "embroidery";
			case "схемы":
				return "pattern";
			case "бусинки":
				return "beads";
			case "крестика":
				return "crosses";
			case "петли":
				return "stitches";
			case "стразы":
				return "crystals";
			case "Бисерок":
				return "Beads";
			case "Крестик":
				return "Cross";
			case "Петелька":
				return "";
			case "Раскраска":
				return "Painting by numbers";
			case "Алмазная мозаика":
				return "Crystal mosaic";
			case "Оплаченные для создания картины закончились. Это ошибка или желаете оплатить доп.картины для создания? Обратитесь по адресу ":
				return "There is no more paid for creating pictures. Is it a mistake or do you want to pay for additional pictures to create? Please contact ";
			case "Произошла критическая ошибка. Пожалуйста, напишите об ошибке по адресу ":
				return "There was a critical error, please text about the error at ";
			case " . И пожалуйста, не пытайтесь сейчас создавать картины - они пойдут в 'вычет' из оплаченных Вами. Простите за неудобства!":
				return " . And please do not try to create pictures now - they will go to the 'deduction' from those paid by you. Sorry for the inconvenience!";
			case "Программе не удаётся связаться с сервером. Либо проблемы с интернетом на Вашем компьютере, либо неполадки сервера. Если у Вас с интернетом 100% нет проблем - пожалуйста, напишите об ошибке по адресу ":
				return "The program can not connect with the server, either there are problems with the Internet on your computer, or a server problem. If you sure you have no problems with the Internet  - please text about the error at ";
			case " . Простите за неудобства!":
				return " . Sorry for the inconvenience!";
			case "Схема целиком":
				return "Full pattern";
			case "Схема для печати на ткани":
				return "Pattern for printing on fabric";
			case "Схема для печати":
				return "Pattern for printing";
			case "Произошла ошибка - мощности компьютера не хватило для сохранения схемы для печати на ткани такого большого размера (все остальные элементы схемы были успешно сохранены). Для решения проблемы попробуйте зайти в пункт 'Прочие настройки' (через соотв. кнопку над кнопкой 'Создать схему' на первой вкладке) и установить там мЕньшие значения для параметра 'Разрешение печати в пикс/дюйм'. И затем ещё раз попробуйте сохранить/распечатать схему. Извините за неудобства.":
				return "There was an error - the computer's power was not enough to save the pattern for printing on a fabric of such a large size(all other elements of the pattern have been successfully saved.) To solve the problem, try going to the 'Other settings' (via the corresponding button above the button 'Create a pattern' on the first tab) and set there the lower values for the 'Print resolution in pixels/inch.' And then try again to save/print the pattern. Sorry for the inconvenience.";
			case "Произошла ошибка. Вероятнее всего, мощности компьютера (оперативной памяти) не хватило для сохранения раскраски такого большого размера. Попробуйте либо уменьшить размер раскраски в см (и создать её заново!), либо зайти в пункт 'Важные настройки' (через соотв. кнопку над кнопкой 'Создать картину') и установить там мЕньшие значения для 'Разрешение печати (в пикселях/дюйм)'. И затем ещё раз попробовать сохранить/распечатать картину. Извините за неудобства.":
				return "There was an error, most likely, the computer's power (RAM) was not enough to save the painting by number of such a big size. Try to reduce the size of the coloring in cm (and create it again!), or go to the item 'Important settings' (via the corresponding button above the button 'Create a picture') and set there the lower values for 'Print resolution (in pixels/inch)'. And then try again to save print the picture. Sorry for the inconvenience.";
			case "Произошла ошибка. Вероятнее всего, мощности компьютера (оперативной памяти) не хватило для сохранения раскраски такого большого размера. Попробуйте либо уменьшить размер раскраски в см (и создать её заново!), либо зайти в пункт 'Важные настройки' (через соотв. кнопку над кнопкой 'Создать картину') и установить там мЕньшие значения для 'Разрешение печати (в пикселях/дюйм)' и/или указать более плохое качество контуров (чем выше качество - тем больше оперативой памяти использует программа). И затем ещё раз попробовать сохранить/распечатать картину. Извините за неудобства.":
				return "There was an error, most likely, the computer's power (RAM) was not enough to save the painting by number of such a big size. Try to reduce the size of the coloring in cm (and create it again!), or go to the item 'Important settings' (via the corresponding button above the button 'Create a picture') and set there the lower values for 'Print resolution(in pixels/inch)' and/or indicate poorer quality of the contours (the higher the quality - the more RAM the program uses). And then once again try to save/print the picture. Sorry for the inconvenience.";
			case "Произошла ошибка. Вероятнее всего, мощности компьютера (оперативной памяти) не хватило для обработки схемы такого большого размера. Попробуйте либо уменьшить размер схемы в см (и создать схему заново!), либо зайти в пункт 'Прочие настройки' (через соотв. кнопку над кнопкой 'Создать схему') и установить там мЕньшие значения для 'Размер ":
				return "There was an error. Most likely, the computer's power (RAM) was not enough to work with the pattern of such a large size. Try to reduce the size of the pattern in cm (and create the pattern again!), or go to the 'Other settings' (via the corresponding button above the button 'Create a pattern') and set there the lower values for 'Size ";
			case " на схеме после сохранения (в пикселях)'. И затем ещё раз попробовать сохранить/распечатать схему. Извините за неудобства.":
				return " on the pattern after saving (in pixels)'. And then try again to save/print the pattern. Sorry for the inconvenience.";
			case "Сохранение самой схемы (т.е. всех её листов, легенды, вида вышивки и т.п.) прошло УСПЕШНО. Вы можете открыть сохранённую папку и посмотреть или распечатать схему.\r\n\r\nОднако, возникла ОШИБКА при сохранении файла схемы, который нужен для дальнейшей загрузки схемы в программу с целью доработки. \r\n\r\nЕсли Вы планируете в будущем дорабатывать эту схему - попробуйте выполнить следующие действия:\r\n1. Попробуйте сохранить схему в другую папку (чтобы сохранился и файл схемы для доработки тоже).\r\n\tЕсли не поможет, то:\r\n2. Перенесите схему на другую вкладку (доработка или ручное редактирование) и чуть-чуть измените её (самую малость - просто чтобы какой-то элемент в схеме стал другим). И попробуйте сохранить из той вкладки, куда перенесли.\r\n\r\nИзвините за неудобства.":
				return "Saving the full pattern (all sheets, legend, type of embroidery, etc.) has been SUCCESSFUL. You can open a saved folder and view or print the pattern.\r\n\r\nHowever, an there was an ERROR when saving the pattern file, which is needed to further download the pattern into the program to revise. \r\n\r\nIf you plan to modify this pattern in the future, try the following:\r\n1. Try to save the pattern into another folder (to save full pattern file *.dat).\r\n\t If it does not help, then:\r\n2. Put the pattern to another tab (revision or manual editing) and slightly change it (just for someelement in the pattern has become different.) And try to save from the tab where you moved it. \r\n\r\nSorry for the inconvenience";
			case "Демо-версия":
				return "Demo version";
			case "Картина по номерам":
				return "Painting by numbers";
			case "Вид ":
				return "View of ";
			case "Бисер *** (демо-версия)":
				return "Beads *** (demo version)";
			case "Стразы *** (демо-версия)":
				return "Crystals *** (demo version)";
			case "шт.":
				return "pcs.";
			case "шт., ":
				return "pcs.,";
			case "гр.":
				return "gr.";
			case "- демо-версия":
				return "- demo version";
			case "В Демо-версии половина бисеринок не отображаются -":
				return "In the demo version, half of the beads are not displayed -";
			case "В Демо-версии половина страз не отображаются -":
				return "In the demo version, half of the crystals are not displayed -";
			case " вместо них на этой схеме пустое белое место":
				return " instead of them an empty white space";
			case ", лист №":
				return ", sheet №";
			case "Карта цветов для картины ":
				return "Color map for painting ";
			case "см из ":
				return "cm from ";
			case "Примерные формулы (пропорции) смешения цветов Вашей палитры":
				return "Example formulas (proportions) of mixing colors of your palette";
			case "для получения нужных оттенков:":
				return "to get the desired shades:";
			case "Примерные формулы (":
				return "Example formulas (";
			case "пропорции":
				return "proportions";
			case "и":
				return "and";
			case "очень примерный объём":
				return "very approximate quantity";
			case ") смешения красок":
				return ") of color mixing";
			case "Легенда для схемы размером":
				return "Legend for pattern with size";
			case "см*":
				return "cm*";
			case "петель":
				return "stitches";
			case "стежков":
				return "stitches";
			case ") из ":
				return ") from ";
			case "Значок легенды и номер":
				return "Legend icon and number";
			case "Значок легенды и":
				return "Legend icon and";
			case "бисера по каталогу":
				return "beads by catalog";
			case "цвет бисера":
				return "color of beads";
			case "мулине по каталогу":
				return "mouline by catalog";
			case "цвет мулине":
				return "mouline color";
			case "пряжи по каталогу":
				return "yarn by catalog";
			case "цвет пряжи":
				return "yarn color";
			case "страз по каталогу":
				return "crystals by catalog";
			case "цвет страз":
				return "crystals color";
			case "бисеринок":
				return "beads";
			case "Вес бисера в граммах*":
				return "Weight of beads in grams*";
			case "крестиков":
				return "stitches";
			case "Длина мулине в метрах*":
				return "Length of a mouline in meters*";
			case "Длина пряжи в метрах*":
				return "Yarn length in meters*";
			case "Вес страз в граммах*":
				return "Weight of crystals in grams*";
			case "Число ":
				return "Number ";
			case "Название производителя недоступно в демо-версии!":
				return "The manufacturer's name is not available in the demo version!";
			case "Серия \"":
				return "Series \"";
			case "Серия: Название серии недоступно в демо-версии!":
				return "Series: The title of the series is not available in the demo version!";
			case "Бэкстич:":
				return "Backstitch:";
			case "Французские узелки:":
				return "French beads";
			case " см":
				return " cm";
			case " мл.":
				return " ml.";
			case "Не удалось подобрать смешение красок для данного оттенка":
				return "Could not mix colors for this shade";
			case " гр":
				return " gr";
			case " м, ":
				return " m, ";
			case "мотков":
				return "skeins";
			case "моток":
				return "skein";
			case "мотка":
				return "skein";
			case " м.":
				return " m.";
			case "В тестовой версии доступен":
				return "In the test version there is available";
			case "лишь первый лист легенды":
				return "only the first sheet of the legend";
			case "*Размер схемы в см и вес в граммах для бисера каждого цвета указывается":
				return "*The size of the pattern in cm and the weight in grams for beads of each color is indicated";
			case "ориентировочно - для бисера размера ":
				return "tentatively - for beads of size ";
			case "/0. ПРОСЬБА - для важных участков вышивки":
				return "/0. PLEASE - for important sites of embroidery";
			case "(лица и т.п.) перед вышивкой убедиться в верности подобранных оттенков":
				return "(face, etc.) before embroidering make sure the correctness of the chosen shades";
			case "*Размер схемы в см, а также вес в граммах страз каждого цвета":
				return "*The size of the pattern in cm and the weight in grams of crystals of each color";
			case "    указывается ориентировочно - для страз размера 2,5 х 2,5 мм":
				return "      is indicated approximately - for crystals of 2.5 x 2.5 mm";
			case "*Размер схемы в см, а также длина мулине каждого цвета":
				return "*The size of the pattern in cm and the length of the mouline of each color";
			case "сложений":
				return "numbers";
			case "сложение":
				return "number";
			case "сложения":
				return "number";
			case " нити":
				return " yarn";
			case "    указывается ориентировочно - для канвы Аида ":
				return "   is indicated roughly - for the canvas Aida ";
			case "    (также считается, что в 1 мотке есть 8 метров мулине)":
				return "    (also it is considered that there are 8 meters of a mouline in one skein)";
			case "*Размер схемы в см, а также длина пряжи каждого цвета":
				return "*Pattern size in cm and length of the yarn of each color";
			case "    указывается ориентировочно - для плотности вязания:":
				return "  is indicated roughly - for knitting closeness:";
			case "Смешивать краски можно в любых измерительных величинах, согласно пропорциям":
				return "You can mix colors in any measurement values, according to the proportions";
			case "(выделены жирным). В скобках серым цветом указан мин.объём краски, нужный":
				return "(in bold). In brackets, the min. volume of paint is indicated in gray, required";
			case "для смешения. Пожалуйста, помните, что формула - только ПРИМЕРНАЯ ОСНОВА!":
				return "for mixing. Please remember that the formula is just a SAMPLE BASIS!";
			case "Смешивать краски можно в любых измерительных величинах (мл, мг или другие)":
				return "You can mix colors in any measurement values (ml, mg or other)";
			case "по указанным пропорциям. Но, пожалуйста, помните, что формула - только":
				return "according to the specified proportions, but please remember that the formula is only a";
			case "ПРИМЕРНАЯ ОСНОВА! Есть замечания по подбору пропорций? Пишите нам!":
				return "EXAMPLE BASIS! Do you have any comments on the selection of proportions? Write to us!";
			case "Выше указана площадь, закрашиваемая каждым цветом, и примерный необходимый":
				return "Above is the area covered by each color, and the approximate required";
			case "объём краски (исходя из расхода краски 250мл/1кв.м.)":
				return "paint volume (based on paint consumption 250ml/sq.m.)";
			case "Легенда":
				return "Legend";
			case "Карта цветов":
				return "Map of colors";
			case "Какие краски нужны (":
				return "What colors are needed (";
			case " шт.) для получения из них":
				return " pcs.) to make of them";
			case "оттенков, указанных на прошлых страницах карты цветов:":
				return "shades, shown on the previous pages of the color map:";
			case " мл.)":
				return " ml.)";
			case "Справа от названий цветов в скобках указан примерный необходимый объём":
				return "There is the approximate required volume to the right of the names of the colors in brackets";
			case "краски данного цвета (исходя из расхода краски 250мл/1кв.м.)":
				return "paints of this color (based on the consumption of paint 250ml/sq.m.)";
			case "Схема составлена с использованием":
				return "The pattern is made with";
			case "программы":
				return "program";
			case "\"Бисерок 2.0\"":
				return "\"Beads 2.0\"";
			case "вышивке":
				return "embroidery";
			case "programma-biserok.ru":
				return "programma-biserok.ru";
			case "\"Крестик 2.0\"":
				return "\"Crosses 2.0\"";
			case "программа-крестик.рф":
				return "программа-крестик.рф";
			case "\"Петелька\"":
				return "\"Stitch\"";
			case "вязании":
				return "knitting";
			case "programma-petelka.ru":
				return "programma-petelka.ru";
			case "\"Алмазная мозаика\"":
				return "\"Crystals\"";
			case "рукоделии":
				return "needlework";
			case "almaznaja-mozaika.ru":
				return "almaznaja-mozaika.ru";
			case "Успехов в ":
				return "We wish you success in ";
			case "Сайт: ":
				return "Website: ";
			case " лишь первый лист схемы":
				return " only the first sheet of the pattern";
			case "Лист ":
				return "Sheet ";
			case "Мулине *** (демо-версия)":
				return "Mouline *** (demo version)";
			case "Франц.узелки:":
				return "French beads";
			case " не нужно!":
				return " not necessary!";
			case " бисер не нужен!":
				return " beads are not needed!";
			case " мулине не нужно!":
				return " mouline is not needed!";
			case "эта пряжа не нужна!":
				return "this yarn is not needed!";
			case "этот бисер не нужен!":
				return "this beads are not needed!";
			case "это мулине не нужно!":
				return "this mouline is not needed";
			case "этот бисер/стразы не нужны!":
				return "this beads/crystals are not needed!";
			case "'Лицензия и коммерческое использование'":
				return "'Licension and commercial usage'";
			case "'О программе'":
				return "'About'";
			case "'Справка'":
				return "'Help'";
			case "Шаг 1. Создание схемы":
				return "Step 1. Pattern design";
			case "Ограничения версии":
				return "Limitations of the version";
			case "Купить БАЗОВУЮ версию, чтобы активировать эту вкладку":
				return "Buy BASE version to activate this tab";
			case "Я уже оплатил(-а) (проверка оплаты)":
				return "I've bought (payment checking)";
			case "Изображение пока не загружено":
				return "Source image is not loaded yet";
			case "Схема пока не создана":
				return "Pattern is not created yet";
			case "Картина пока не создана":
				return "Painting is not created yet";
			case "2. Укажите параметры схемы":
				return "2. Add the pattern parameters";
			case "Чем левее установлен ползунок, тем больше деталей будет на картине, но и тем сложнее будет её рисовать. Чем правее ползунок - тем меньше деталей будет на картине, тем проще её рисовать, но она получится менее красивая. Оптимальное значение коэфф. - 2~3.":
				return "The more slider to the left, the more details you can see at the painting, but and more difficult to paint it. The more slider to the right - the less details you can see at the painting (more easy to paint, but less beautifull). Optimal value coefficient is 2~3.";
			case "Число деталей на картине:":
				return "Number of details at painting";
			case "больше":
				return "more";
			case "меньше":
				return "less";
			case "Чем левее ползунок, тем выраженнее детали, картина красивее и её сложнее рисовать. Чем правее ползунок - тем более упрощены детали, картину рисовать легче, но она менее красивая. Оптимальное значение коэфф. - 2~3.":
				return "The more slider to the left, the more significant details (painting is more beautiful, but it is more difficult to paint it). The more slider to the right - the more simplified details (more easy to paint it, but painting is less beautiful). Optimal coefficient is 2~3.";
			case "Плавность деталей:":
				return "Detail's smoothness";
			case "менее плавные":
				return "less smooth";
			case "более плавные":
				return "more smooth";
			case "Чем левее ползунок, тем больше мелких деталей есть в картине (она красивее и её сложнее рисовать). Чем правее ползунок - тем меньше мелких деталей в картине (рисовать легче). Оптимальное значение коэфф. - 2~3.":
				return "The more slider to the left, the more small details are in the painting (painting is more beautiful, but it is more difficult to paint it). The more slider to the right - the less small details are in the painting (more easy to paint). Optimal coefficient is 2~3.";
			case "Мелкие детали:":
				return "Small details:";
			case "оставляем":
				return "save";
			case "убираем":
				return "remove";
			case "(рисовать сложнее)":
				return "(difficult to paint)";
			case "(картина менее красивая)":
				return "(painting is less beautyful)";
			case "(коэфф.=":
				return "(coeff.=";
			case "Не преобразовывать исходное фото":
				return "Don't change source photo";
			case "Желаемое число цветов в будущей схеме":
				return "Desired number colors at future pattern";
			case "Число цветов:":
				return "Number of colors:";
			case "Размер":
				return "Size";
			case "ширина":
				return "width";
			case "Ширина схемы в сантиметрах (если Вы укажете ширину, то высота схемы заполнится АВТОМАТИЧЕСКИ, пропорционально высоте исходного ихображения)":
				return "Width of pattern in centimeters (if you will set a width, then height of pattern will be setted AUTOMATICALLY proportionally to a height of source image)";
			case "Высота схемы в сантиметрах (если Вы укажете высоту, то ширина схемы заполнится АВТОМАТИЧЕСКИ, пропорционально ширине исходного ихображения)":
				return "Height of pattern in centimeters (if you will set a height, then width of pattern will be setted AUTOMATICALLY proportionally to a width of source image)";
			case "высота":
				return "height";
			case "Х":
				return "X";
			case "НЕДОСТУПНО В ТЕСТОВОЙ ВЕРСИИ":
				return "NOT AVAILIABLE IN TEST VERSION";
			case "Обычная":
				return "Normal";
			case "Простая схема, когда каждая бусинка располагается строго под другой бусинкой":
				return "Simple pattern, when each bead is situated strictly under other bead";
			case "Кирпичиками":
				return "By blocks";
			case "Схема кирпичиками - это когда бусинки располагаются, как кирпичная кладка":
				return "Pattern by blocks - it is when beads are allocated alike brickwork";
			case "Тип схемы:":
				return "Pattern type:";
			case "Простой подбор цветов":
				return "Simple color selection";
			case "Подбор цветов:":
				return "Color selection:";
			case "Редактор своих цветов бисера (доступно в мастер-версии)":
				return "Editor of own beads colors (available in master-version)";
			case "Переход к окну редактирования своих цветов, чтобы в дальнейшем из них создавать схемы":
				return "Going to window of editing own colors, to design patterns of them";
			case "Квадратные":
				return "Square";
			case "Схема будет создаваться под обычные квадратные стразы с 4 гранями. Размер страз в мм можно изменить в Настройках ниже":
				return "Pattern will be designed for usual square crystals with 4 verges. Size of crystals in mm you can change at Settings below";
			case "Круглые":
				return "Round";
			case "Схема будет создаваться под круглые стразы. Размер страз в мм можно изменить в Настройках ниже":
				return "Pattern will be designed for round crystals. Size of crystals in mm you can change at Settings below";
			case "Вид страз:":
				return "Kind of crystals:";
			case "в см:":
				return "in cm:";
			case "Не забудьте, что после создания схемы справа отобразится лишь будущий вид выполненной работы. ПОЛНОЦЕННУЮ СХЕМУ же Вы получите ПОСЛЕ её сохранения или печати (см. кнопки ниже)":
				return "Please, don't forget, that after pattern designing at the right side you can see just an appearance of future product. FULL pattern you will get AFTER it's saving or printing (look for buttons below).";
			case "3. Создать схему":
				return "3. Design pattern";
			case "3. Создать картину":
				return "3. Design painting";
			case "Чтобы получить полноценную схему (листы схемы + легенда (ключи схемы) + вид будущей работы), Вам необходимо её сохранить на компьютер или распечатать":
				return "To get full pattern (sheets of pattern + map of colors + appearance of future work), you should save it to computer or print.";
			case "Сохранить":
				return "Save";
			case "Созданная схема автоматически будет распечатана, если принтер подключен к компьютеру":
				return "Designed pattern will be automatically printed if printer is connected to computer";
			case "Печать":
				return "Print";
			case "Масштаб фото:":
				return "Photo scale:";
			case "Масштаб картины:":
				return "Painting scale:";
			case "Исходное изображение:":
				return "Source image:";
			case "Вы можете увеличить или уменьшить исходное изображение - для удобства просмотра":
				return "For your convenience you can increase or decrease source image";
			case "Вышивка:":
				return "Embroidery:";
			case "Вы можете увеличить или уменьшить вид схемы - для удобства просмотра":
				return "For your convenience you can increase or decrease appearance of pattern - just to convenience of viewing";
			case "Масштаб схемы:":
				return "Pattern scale:";
			case "Прочие настройки":
				return "Other settings";
			case " 1. НАЧАЛО РАБОТЫ: ":
				return "1. WORK BEGINNING";
			case "Б. Загрузить сохранённую схему (недоступно в тестовой версии)":
				return "B. Load saved pattern (disable in test version)";
			case "А. Загрузить исход- ное изображение":
				return "A. Load source image";
			case "←Доработать?":
				return "←Finalize?";
			case "Ниже представлен лишь вид вышивки. Чтобы получить саму схему вышивки и карту цветов - сохраните созданную вышивку (кнопка 'Сохранить' слева-внизу)!":
				return "There is only an appearance of embroidery below. To get full pattern and map of colours you should save designed pattern (button 'Save' at bottom-left side)!";
			case "Вы можете выполнить заказ всех необходимых для этой работы материалов прямо из программы!":
				return "You can complete an order of all consumable material for this product right from the program!";
			case "Заказать материалы к схеме":
				return "Order materials for pattern";
			case "Шаг 2. Доработка":
				return "Step 2. Finalization";
			case "Купить ПРОДВИНУТУЮ версию, чтобы активировать эту вкладку":
				return "Buy ADVANCED version to activate this tab";
			case "Схема пока не загружена":
				return "The pattern is not loaded";
			case " 1. Перенести сюда схему: ":
				return " 1. Move the pattern here: ";
			case " 1. Перенести сюда картину: ":
				return " 1. Move the painting here: ";
			case "А. Из вкладки \"Создание схемы\"":
				return "A. From the tab \"Create pattern\"";
			case "Б. Из вкладки \"Ручное редактир-е\"":
				return "B. From the tab \"Manual Edit\"";
			case "Режим частичной зашивки: Чтобы удалить участок из схемы - кликните по нему левой кнопкой мыши. Чтобы вернуть участок схемы обратно в схему - кликните по нему правой кнопкой мыши":
				return "Partial filling mode: To remove a section from the pattern click it with the left button. To return the section back to the pattern - click it with the right button";
			case "Вы можете увеличить или уменьшить исходное изображение - для удобства работы":
				return "For your convenience you can increase or decrease original image";
			case "Режим цветовой коррекции: Для добавления цвета в схему кликните мышкой по исходному изображению (ниже) в месте с нужным цветом":
				return "Color correction mode: To add a color to the pattern, click the original image (below) on the position with the desired color";
			case "Вы можете увеличить или уменьшить вид схемы - для удобства работы":
				return "For your convenience you can increase or decrease the view of the pattern";
			case "Режим цветовой коррекции: Для удаления цвета из схемы кликните мышкой по схеме ниже в месте с ненужным цветом":
				return "Color correction mode: To remove a color from the pattern, click on the pattern below on the position with an unnecessary color";
			case " 2. Доработайте схему ":
				return "2. Modify the pattern";
			case "Режим цветовой коррекции":
				return "Color correction mode";
			case "Режим частичной зашивки":
				return "Partial filling mode";
			case "Размер 'кисти' частичной зашивки":
				return "Size of the 'brush' in partial filling";
			case "размер 'кисти': меньше":
				return "Size of the 'brush': smaller";
			case "Чтобы добавить новый цвет в схему - кликните по ИСХОДНОМУ ИЗОБРАЖЕНИЮ (слева) в месте с нужным цветом. Чтобы удалить ненужный цвет из схемы - кликните по ВЫШИВКЕ (справа) в месте с цветом, который надо удалить":
				return "To add a new color to the pattern - click on the ORIGINAL IMAGE (on the left) in the position with the desired color. To remove the unwanted color from the chart - click on the RESULT (on the right) in the position with the color you want to delete";
			case "Чтобы добавить новый цвет в схему - кликните по ИСХОДНОМУ ИЗОБРАЖЕНИЮ (слева) в месте с нужным цветом. Чтобы удалить ненужный цвет из схемы - кликните по ВЯЗАНИЮ (справа) в месте с цветом, который надо удалить":
				return "To add a new color to the pattern - click on the ORIGINAL IMAGE (on the left) in the position with the desired color. To remove the unwanted color from the chart - click on the RESULT (on the right) in the position with the color you want to delete";
			case "Чтобы добавить новый цвет в схему - кликните по ИСХОДНОМУ ИЗОБРАЖЕНИЮ (слева) в месте с нужным цветом. Чтобы удалить ненужный цвет из схемы - кликните по РАСКРАСКЕ (справа) в месте с цветом, который надо удалить":
				return "To add a new color to the pattern - click on the ORIGINAL IMAGE (on the left) in the position with the desired color. To remove the unwanted color from the chart - click on the RESULT (on the right) in the position with the color you want to delete";
			case "Чтобы удалить участок из схемы - кликните по ВЯЗАНИЮ (справа) ЛЕВОЙ кнопкой мыши. Чтобы вернуть участок на место - кликните ПРАВОЙ кнопкой мыши.":
				return "To remove a section from the pattern - click on the RESULT (on the right) with the LEFT button. To return the section - click the RIGHT button.";
			case "Чтобы удалить участок из схемы - кликните по ВЫШИВКЕ (справа) ЛЕВОЙ кнопкой мыши. Чтобы вернуть участок на место - кликните ПРАВОЙ кнопкой мыши.":
				return "To remove a section from the pattern - click on the RESULT (on the right) with the LEFT button. To return the section - click the RIGHT button.";
			case "Чтобы удалить участок из схемы - кликните по РАСКРАСКЕ (справа) ЛЕВОЙ кнопкой мыши. Чтобы вернуть участок на место - кликните ПРАВОЙ кнопкой мыши.":
				return "To remove a section from the pattern - click on the RESULT (on the right) with the LEFT button. To return the section - click the RIGHT button.";
			case "Убрать одиночные крестики":
				return "Remove single stitches";
			case "Программа автоматически исключит одиночные крестики из схемы":
				return "The program automatically delete single stitches from the pattern";
			case "НЕДОСТУПНО В ТЕКУЩЕЙ ВЕРСИИ":
				return "NOT AVAILABLE IN THE CURRENT VERSION";
			case "Отменить добавление/удаление цвета":
				return "Undo add/remove the color";
			case "Шаг 3. Ручное редактирование":
				return "Step 3. Manual editing";
			case "Купить МАСТЕР-версию, чтобы активировать эту вкладку":
				return "Buy MASTER version to activate this tab";
			case "Схема пока не перенесена на эту вкладку (см.ссылки слева-вверху в п.'1.Перенести сюда схему')":
				return "The pattern has not been transferred yet to this tab (see the links on the left-top in p.'1.Move the pattern here')";
			case "Картина пока не перенесена на эту вкладку (см.ссылки слева-вверху в п.'1.Перенести сюда картину')":
				return "The painting has not been transferred yet to this tab (see the links on the left-top in p.'1.Move the painting here')";
			case "Б. Из вкладки \"Доработка\" (доступно в мастер-версии)":
				return "B. From the tab \"Finalization\" (available in the Master version)";
			case "2. Выберите цвет рисования/заливки":
				return "2. Select the color of drawing/filling";
			case "Если выбран этот режим, то теперь необходимо кликнуть мышкой по схеме в месте с нужным цветом, чтобы этот цвет выбрался":
				return "If you selected this mode, now click on the pattern on the position with the desired color to get this color";
			case "Пипеткой из схемы. Цвет:":
				return "With pipet from the pattern. Color:";
			case "Доступные цвета":
				return "Available colors";
			case "Недавние цвета":
				return "Recent colors";
			case "Если выбран этот режим, то теперь необходимо выбрать цвет из выпадающего спика цветов":
				return "If you selected this mode, now you need to select a color from the drop-down list of the colors";
			case "Из списка:":
				return "From the list:";
			case "Если выбран этот режим, то введите часть наименования":
				return "If you selected this mode, enter the part of the name";
			case "Фильтр по названию:":
				return "Filter by name:";
			case " 3. Выберите бисер похожего цвета":
				return "3. Select the beads of similar color";
			case "Нужно выбрать конкретный цвет для редактирования схемы с его помощью":
				return "You need to select a specific color for editing the pattern";
			case "4. Режим рисования ":
				return "4. Drawing mode ";
			case "В режиме 'Кисть' Вы можете мышкой как кистью рисовать по схеме выбранным выше цветом":
				return "In the 'Brush' mode, you can draw the pattern with the selected color";
			case "Кисть":
				return "Brush";
			case "В режиме 'Заливка' Вы можете заливать выбранным выше цветом отдельные одноцветные области на схеме. Для этого кликните по любой одноцветной области на схеме мышкой":
				return "In the 'Filling' mode, you can fill the single-color areas on the pattern with the selected color. Click on any single-color area on the pattern";
			case "Заливка":
				return "Filling";
			case "Полный стежок":
				return "Full stitch";
			case "1/2 стежка":
				return "1/2 stitch";
			case "1/4 полный":
				return "1/4 full";
			case "1/4 половина":
				return "1/4 half";
			case "3/4 стежка":
				return "3/4 stitch";
			case "Бэкстич":
				return "Backstitch";
			case "Франц.узелок":
				return "French bead";
			case "Отмена (стежки) = Ctrl+Z":
				return "Cancel (stitches) = Ctrl+Z";
			case "* Для замены цвета кликните по нему правой кнопкой мыши и выберите нужный пункт в меню":
				return "* To change the color, right-click on it and select the desired item";
			case "Просчет % красок в картине занимает много времени. Чтобы обновить - нажмите эту кнопку и ожидайте.":
				return "Calculating % of the colors in the painting takes a long time. To update click this button and wait.";
			case "Обновить %":
				return "Update %";
			case "Отмена (бэкстич)":
				return "Cancel (backstitch)";
			case "Отмена (узелки)":
				return "Cancel (fr.beads)";
			case "Программы по рукоделию!":
				return "Needle programs!";
			case "Новости программ":
				return "Programs news";
			case "Новости для Вас загружаются с нашего сайта - пожалуйста, подождите некоторое время...":
				return "News for you are downloaded from our site - please wait a while...";
			case "Есть вопрос?":
				return "Do you have a question?";
			case "Укажите ниже Ваш вопрос и Вы получите ответ на Ваш адрес эл.почты в течение 1 суток. Либо задайте Ваш вопрос на наш адрес эл.почты EMAIL, либо напрямую на мою личную почту AndZvezintsev@gmail.com":
				return "Please enter your question below and we send you reply to your e-mail address within 1 day. Or send your question to our EMAIL address or directly to my personal email AndZvezintsev@gmail.com";
			case "Ваш вопрос (не более 300 символов):":
				return "Your question (300 characters max):";
			case "Ваш адрес электронной почты (e-mail):":
				return "Your e-mail (e-mail):";
			case "Отправить вопрос":
				return "Send question";
			case "Тема вопроса:":
				return "Subject of the question:";
			case "В базовой версии доступна только первая вкладка - создание схемы":
				return "In the Base version, only the first tab is available - patter design";
			case "БАЗОВАЯ ВЕРСИЯ":
				return "BASIC VERSION";
			case "В продвинутой версии доступны первые две вкладки - создание и доработка схемы":
				return "In the Advanced version, the first two tabs are available - design nd finalization of the pattern";
			case "ПРОДВИНУТАЯ ВЕРСИЯ":
				return "ADVANCED VERSION";
			case "В мастер-версии доступны все возможности программы: создание схемы, её доработка и ручное редактирование":
				return "In the Master version, all the features of the program are available: pattern design, finalization and manual editing";
			case "МАСТЕР-ВЕРСИЯ":
				return "MASTER-VERSION";
			case "Замеры времени":
				return "Time measurements";
			case "До окончания Вашей лицензии: 90 дн.":
				return "Before the end of your license: 90 days";
			case "Успейте продлить программу со скидкой!":
				return "Prolong the program at a discount!";
			case "Укажите ниже Ваш вопрос и Вы получите ответ на Ваш адрес эл.почты в течение 1 суток.Либо задайте Ваш вопрос на наш адрес эл.почты EMAIL, либо напрямую на мою личную почту AndZvezintsev@gmail.com":
				return "Set your question below and you will get the answer to your email within 1 day. Or ask your question to our e-mail EMAIL or to my personal e-mail AndZvezintsev@gmail.com";
			case "Автор программы: Звезинцев Андрей, компания HobbyLine":
				return "Author of the program is Zvezintcev Andrei, HobbyLine LLC";
			case "E-mail:":
				return "E-mail:";
			case "AndZvezintsev@gmail.com":
				return "AndZvezintsev@gmail.com";
			case "Программа для:  ":
				return "This program for:";
			case "Продлить на 1 год со скидкой!":
				return "Extend on 1 year at discount!";
			case "О программе":
				return "About";
			case "Сайт программы:":
				return "Site of the program:";
			case "Перед работой с программой, пожалуйста, ознакомьтесь ниже с двумя разделами Лицензионного соглашения - информацией о коммерческом использовании программы и общей информацией (при необходимости Вы всегда сможете ознакомиться с ним повторно из главного окна программы):":
				return "";
			case "Добавить":
				return "Add";
			case "Редакт.":
				return "Edit";
			case "Удал.":
				return "Del.";
			case "Сохранить изменения":
				return "Save changes";
			case "Отмена":
				return "Cancel";
			case "Редактор серии":
				return "Serie's editor";
			case "Название серии:":
				return "Serie's name:";
			case "Редактор цвета":
				return "Color's editor";
			case "Название цвета:":
				return "Color's name:";
			case "Цвет:":
				return "Color:";
			case "Вы можете создать свою палитру с любым числом цветов. Но в домашней версии программы (не для коммерческого применения) использовать можно лишь 12 цветов из Вашей палитры (т.е. только 12 цветов загрузятся в Вашу палитру в главном окне программы). Для полноценного использования всей Вашей палитры (но не более 80 цветов, более - на спец.условиях) приобретите профессиональную версию.":
				return "";
			default:
				return src;
			}
		}
	}
}
