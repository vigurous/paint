using System;
using System.Drawing;

namespace Бисерок
{
	[Serializable]
	public class BackStitch
	{
		public PointF ptBegin;

		public PointF ptEnd;

		public Beads bead;
	}
}
