using System;
using System.Drawing;
using System.Runtime.Serialization;

namespace Бисерок
{
	[Serializable]
	public class Block : Pixel
	{
		public bool? IsItFullStitch = true;

		public Stitch leftTop;

		public Stitch rightTop;

		public Stitch leftBottom;

		public Stitch rightBottom;

		private Beads resBeads;

		public Beads ResBeads;

		public bool IsVisible = true;

		public int X
		{
			get;
			set;
		}

		public int Y
		{
			get;
			set;
		}

		public RectangleF PositionInScheme
		{
			get;
			set;
		}

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			if (!IsItFullStitch.HasValue)
			{
				IsItFullStitch = true;
			}
			if (ResBeads == (Beads)null)
			{
				ResBeads = resBeads;
			}
			resBeads = null;
		}

		public Block GetCopy(bool isItRaskraska = false)
		{
			if (isItRaskraska)
			{
				return new Block
				{
					SrcColor = base.SrcColor,
					ResBeads = ResBeads,
					X = X,
					Y = Y
				};
			}
			Block block = new Block
			{
				PositionInScheme = PositionInScheme,
				SrcColor = base.SrcColor,
				ResBeads = ResBeads,
				X = X,
				Y = Y,
				IsVisible = IsVisible,
				IsItFullStitch = IsItFullStitch
			};
			if (leftTop != null)
			{
				block.leftTop = leftTop.GetCopy();
			}
			else
			{
				block.leftTop = null;
			}
			if (rightTop != null)
			{
				block.rightTop = rightTop.GetCopy();
			}
			else
			{
				block.rightTop = null;
			}
			if (leftBottom != null)
			{
				block.leftBottom = leftBottom.GetCopy();
			}
			else
			{
				block.leftBottom = null;
			}
			if (rightBottom != null)
			{
				block.rightBottom = rightBottom.GetCopy();
			}
			else
			{
				block.rightBottom = null;
			}
			return block;
		}
	}
}
