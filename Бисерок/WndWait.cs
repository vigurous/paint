using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace Бисерок
{
	public partial class WndWait : Window, IComponentConnector
	{
		private const int Feature = 21;

		private const int SetFeatureOnProcess = 2;

		private bool isClosingByProgram;

		private DispatcherTimer tmr = new DispatcherTimer();

		private Random rnd = new Random(DateTime.Now.Millisecond);

		private int lastNumber = -1;

		private string allPhrases = "||==alltime==9==1==\r\nВаша улыбка озаряет комнату :) Делайте это почаще и от души :)\r\n||==alltime==9==1==\r\nВдохновляйте людей - это того стоит :-)\r\n||==alltime==9==1==\r\nНе забывайте говорить близким людям, что любите их :)\r\n||==alltime==9==1==\r\nКаждый человек - автор своей жизни на 100% :) Всё в Ваших руках! Всегда!\r\n||==morning==9==1==\r\nЖелаем Вам в это утро хорошего, доброго и продуктивного дня! Улыбайтесь :)\r\n||==morning==9==1==\r\nНадеемся, что Ваше утро доброе! Желаем прекрасного дня :)\r\n||==lunch==9==1==\r\nСамое время пообедать - не забывайте кормить и любить себя :)\r\n||==lunch==9==1==\r\nОбед - хорошее время, чтобы настроиться на продуктивный день!\r\n||==dinner==9==1==\r\nНадеемся, что этот вечер принесёт Вам хорошие новости!\r\n||==dinner==9==1==\r\nДоброго вечера и верьте в себя :)\r\n||==alltime==9==1==\r\nЛюбите себя! Вы прекрасны и Вам всё подвластно!\r\n||==alltime==9==1==\r\nВесь мир в Ваших руках! Продолжайте нести в него добрые дела :)\r\n||==alltime==9==1==\r\nЖизнь - хорошая штука! Ну, правда!? :)\r\n||==alltime==9==1==\r\nНе переживайте по мелочам :) Всё будет хорошо!\r\n||==alltime==11==1==\r\nДорогой наш пользователь, спасибо Вам, что выбрали нас и нашу команду! Мы правда стараемся ради Вас :)\r\n||==not_for_krestik==25==5==\r\n<a href='http://программа-крестик.рф/?f=wndWaiting'><img src='logoKrestik.ico' width='64px' height='64px' style=' margin: 5px; margin-right: 10px; float: left;' /></a>\r\nА ещё у нас есть программа для создания схем вышивок крестом - из любой фотографии или картинки (или с нуля)! Вот она:\r\n<a href='http://программа-крестик.рф/?f=wndWaiting'>Программа «Крестик 2.0»</a>\r\n<br >Сайт: <a href='http://программа-крестик.рф/?f=wndWaiting'>программа-крестик.рф</a>\r\n||==not_for_biserok==25==5==\r\n<a href='http://programma-biserok.ru/?f=wndWaiting'><img src='logoBiserok.ico' width='64px' height='64px' style=' margin: 5px; margin-right: 10px; float: left;' /></a>\r\nЕсли любите вышивать бисером - у нас есть программа и для этого вида рукоделия! Вот она: <a href='http://programma-biserok.ru/?f=wndWaiting'>Программа «Бисерок 2.0»</a> \r\n(cайт <a href='http://programma-biserok.ru/?f=wndWaiting'>programma-biserok.ru</a>) для создания схем вышивок бисером из фотографий и картинок!\r\n<br /><br />И помните, что Ваша улыбка озаряет комнату ;)\r\n||==not_for_petelka==25==5==\r\n<a href='http://programma-petelka.ru/?f=wndWaiting'><img src='logoPetelka.ico' width='64px' height='64px' style=' margin: 5px; margin-right: 10px; float: left;' /></a>\r\nВяжете? Надоело вязать ромбики и оленей на свитерах? Может, попробуете связать своё фото? На свитере! Это реально с нашей программой \r\n<a href='http://programma-petelka.ru/?f=wndWaiting'>«Петелька»</a>!\r\n<br >Сайт: <a href='http://programma-petelka.ru/?f=wndWaiting'>programma-petelka.ru</a>\r\n||==not_for_mozaika==25==5==\r\n<a href='http://almaznaja-mozaika.ru/?f=wndWaiting'><img src='logoAlmaz.ico' width='64px' height='64px' style=' margin: 5px; margin-right: 10px; float: left;' /></a>\r\nНа самом деле, у нас много программ для увлечений и хобби, и одна из них - это <a href='http://almaznaja-mozaika.ru/?f=wndWaiting'>«Алмазная мозаика»</a> для создания \r\nсхем алмазной вышивки стразами DMC из фото и картинок!\r\n<br >Сайт: <a href='http://almaznaja-mozaika.ru/?f=wndWaiting'>almaznaja-mozaika.ru</a>\r\n||==not_for_raskraska==25==5==\r\n<a href='http://programma-raskraska.ru/?f=wndWaiting'><img src='logoRaskraska.ico' width='64px' height='64px' style='margin: 5px; margin-right: 10px; float: left;' /></a>\r\nСлышали про картины по номерам? Ну, такие, с контурами и обозначениями внутри... А у нас есть программа <a href='http://programma-raskraska.ru/?f=wndWaiting'>«Раскраска»</a>,\r\nкоторая создаёт настоящие полноценные картины по номерам из любых фотографий или изображений!\r\n<br >Сайт: <a href='http://programma-raskraska.ru/?f=wndWaiting'>programma-raskraska.ru</a>\r\n||==not_for_krestik_biserok_petelka_mozaika==25==3==\r\nЕсли Вам нужно добавить в 'Раскраску' Вашу палитру цветов с числом цветов бОльшим, чем 80 - это можно сделать! Напишите на почту \r\n<a href='mailto:AndZvezintsev@gmail.com?subject=В Раскраску больше 80 цветов'>AndZvezintsev@gmail.com</a> или позвоните/напишите на мой личный номер:\r\n+7 (906) 455-49-66 (Звезинцев Андрей)\r\n||==alltime==25==5==\r\nМы разрабатываем не только программы в области хобби и рукоделия. И если у Вас есть потребность в создании особенной компьютерной программы для Вашего бизнеса - \r\nпишите мне на мою почту <a href='mailto:AndZvezintsev@gmail.com?subject=Разработка программ'>AndZvezintsev@gmail.com</a> или звоните/пишите на мой личный номер:\r\n+7 (906) 455-49-66 (Звезинцев Андрей)\r\n||==alltime==25==5==\r\nНужна какая-то индивидуальная доработка программы под Ваши нужды или потребности Вашего бизнеса? Пишите на мою почту \r\n<a href='mailto:AndZvezintsev@gmail.com?subject=Индивидуальная доработка'>AndZvezintsev@gmail.com</a> или звоните/пишите на мой личный номер: +7 (906) 455-49-66 (Звезинцев Андрей)\r\n||==alltime==25==1==\r\nХотите похвалить или поругать нас? Пишите мне на мою почту <a href='mailto:AndZvezintsev@gmail.com?subject=Похвалить или поругать'>AndZvezintsev@gmail.com</a> или \r\nзвоните/пишите на мой личный номер: +7 (906) 455-49-66 (Звезинцев Андрей)<br />Всегда рады обратной связи от наших пользователей! :)\r\n||==alltime==25==2==\r\nУ Вас есть какие-то идеи, пожелания, рекомендации, мысли по работе с программой или по разработке новых программ? Пишите на мою почту \r\n<a href='mailto:AndZvezintsev@gmail.com?subject=Мысли и идеи'>AndZvezintsev@gmail.com</a> или звоните/пишите на мой личный номер: +7 (906) 455-49-66 (Звезинцев Андрей)<br />\r\n<br />\r\nВсегда рады обратной связи от наших пользователей! :)\r\n||==alltime==30==1==\r\nПожалуйста, помните, что для создания качественных работ есть общие правила: <br />\r\n1. Предварительная обработка исходной фотографии играет важную роль: увеличение контраста, яркости, разнообразия цветов, повышение чёткости<br />\r\n2. Главный объект работы должен быть расположен крупно и по центру! Для этого обрежьте исходное фото с краёв в любом фоторедакторе. Для портретов это особенно важно -\r\nлицо должно быть как можно крупнее.\r\n||==alltime==25==1==\r\nДорогой наш пользователь! Если у Вас есть результаты работ по нашим программам для рукоделия (вышивки, связанные изделия, нарисованные картины по номерам и т.п.) -\r\nмы всегда будем рады посмотреть на Ваши результаты! Присылайте фото готовых работ и исходных схем для них на мою личную почту:\r\n<a href='mailto:AndZvezintsev@gmail.com?subject=Похвалить или поругать'>AndZvezintsev@gmail.com</a> Скажу Вам большое спасибо за это! :)\r\n||==alltime==10==1==\r\n<table width='100%' height='100%' ><tr height='100%'><td  height='100%' width='100%' style='text-align: center' > \r\n<img src='labuda\\\\1.JPG'  height='100%' max-height='100%'  />\r\n</td></tr></table>\r\n||==alltime==20==1==\r\n<table width='100%' height='100%' ><tr height='100%'><td  height='100%' width='100%' style='text-align: center' > \r\n<img src='labuda\\\\2.JPG'  height='100%' max-height='100%' />\r\n</td></tr></table>\r\n||==alltime==10==1==\r\n<table width='100%' height='100%' ><tr height='100%'><td  height='100%' width='100%' style='text-align: center' > \r\n<img src='labuda\\\\3.JPG'   height='100%' max-height='100%'  />\r\n</td></tr></table>\r\n||==alltime==10==1==\r\n<table width='100%' height='100%' ><tr height='100%'><td  height='100%' width='100%' style='text-align: center' > \r\n<img src='labuda\\\\4.JPG'  height='100%' max-height='100%'  />\r\n</td></tr></table>\r\n||==alltime==10==1==\r\n<table width='100%' height='100%' ><tr height='100%'><td  height='100%' width='100%' style='text-align: center' > \r\n<img src='labuda\\\\5.JPG'  height='100%' max-height='100%'  />\r\n</td></tr></table>\r\n||==alltime==10==1==\r\n<table width='100%' height='100%' ><tr height='100%'><td  height='100%' width='100%' style='text-align: center' > \r\n<img src='labuda\\\\6.JPG'  height='100%' max-height='100%'  />\r\n</td></tr></table>\r\n||==alltime==10==1==\r\n<table width='100%' height='100%' ><tr height='100%'><td  height='100%' width='100%' style='text-align: center' > \r\n<img src='labuda\\\\7.JPG'  height='100%' max-height='100%'  />\r\n</td></tr></table>\r\n||==alltime==15==1==\r\n<table width='100%' height='100%' ><tr height='100%'><td  height='100%' width='100%' style='text-align: center' > \r\n<img src='labuda\\\\8.JPG'  height='100%' max-height='100%'  />\r\n</td></tr></table>\r\n||==alltime==10==1==\r\n<table width='100%' height='100%' ><tr height='100%'><td  height='100%' width='100%' style='text-align: center' > \r\n<img src='labuda\\\\9.JPG'  height='100%' max-height='100%'  />\r\n</td></tr></table>\r\n||==not_max_version==35==18==\r\nДорогой пользователь!<br />\r\nПредлагаем Вам <b>увеличить Вашу версию программы 'ИМЯ_ПРОГРАММЫ'</b>! Вам не нужно ничего оплачивать заново - надо лишь доплатить разницу стоимости версий на странице Вашего заказа:\r\n<div style='margin: 8px;'><b><a href='ССЫЛКА_НА_ЗАКАЗ'>Ваш заказ (увеличить версию программы!)</a></b></div>\r\nОплата займёт всего пару минут, зато Вы сможете пользоваться <b>ВСЕМИ возможностями программы</b>, использовать её на все 100%, без ограничений! Подумайте, может, оно того стоит?\r\n||==renewalLicense==35==18==\r\nВаша лицензия на программу скоро истечёт.<br />                        \r\nСегодня Вы ещё можете продлить программу со скидкой:\r\n<div style='margin: 8px;'><b><a href='ССЫЛКА_НА_ЗАКАЗ'>Продлить программу 'ИМЯ_ПРОГРАММЫ' со скидкой</a></b></div>\r\nОбратите внимание, что, продляя работу программы ДО истечения лицензии, Вы ничего не теряете, т.к. продление лицензии будет активировано именно со дня окончания текущей лицензии, а не со дня оплаты!";

		[DllImport("urlmon.dll")]
		[return: MarshalAs(UnmanagedType.Error)]
		private static extern int CoInternetSetFeatureEnabled(int featureEntry, [MarshalAs(UnmanagedType.U4)] int dwFlags, bool fEnable);

		public WndWait()
		{
			InitializeComponent();
			//if (!Settings.isItVersionWithoutContactInfo && Settings.v > 0)
			//{
			//	tmr.Tick += tmr_Tick;
			//	tmr.Interval = new TimeSpan(0, 0, 20);
			//	tmr.Start();
			//	tmr_Tick(new object(), new EventArgs());
			//	CoInternetSetFeatureEnabled(21, 2, true);
			//}
			//else
			//{
				grd.RowDefinitions[1].Height = new GridLength(0.0);
				base.Height = 134.0;
			//}
		}

		private void tmr_Tick(object sender, EventArgs e)
		{
			try
			{
				int num = 20;
				string str = "<html><head><meta charset='utf-8'></head><body  scroll='no' style='overflow-x:hidden; font-family: Verdana; Font-size: 9pt; text-align: justify;'> ";
				string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
				string[] array = allPhrases.Split(new string[1]
				{
					"||"
				}, StringSplitOptions.RemoveEmptyEntries);
				List<string> list = new List<string>();
				string[] array2 = array;
				int num2;
				foreach (string text in array2)
				{
					num2 = Convert.ToInt16(text.Split(new string[1]
					{
						"=="
					}, StringSplitOptions.RemoveEmptyEntries)[2]);
					for (int j = 0; j < num2; j++)
					{
						list.Add(text);
					}
				}
				string text2 = "";
				num2 = -1;
				while (text2 == "")
				{
					num2 = rnd.Next(0, list.Count);
					DateTime now;
					if (num2 != lastNumber)
					{
						text2 = list[num2];
						if (text2.Contains("morning"))
						{
							now = DateTime.Now;
							if (now.Hour >= 6)
							{
								now = DateTime.Now;
								if (now.Hour >= 12)
								{
									goto IL_00f9;
								}
								goto IL_0100;
							}
							goto IL_00f9;
						}
						goto IL_0100;
					}
					continue;
					IL_016b:
					text2 = "";
					goto IL_0172;
					IL_0172:
					if (text2.Contains(Settings.ShortProgramNameInEnglishTransliteration))
					{
						text2 = "";
					}
					TimeSpan timeSpan;
					if (text2.Contains("not_max_version"))
					{
						if (Settings.v == 3)
						{
							text2 = "";
						}
						else
						{
							DateTime? nullable = RegistryWork.CheckDateTimeEnd();
							if (nullable.HasValue)
							{
								timeSpan = nullable.Value - DateTime.Now;
								int num3 = Convert.ToInt32(timeSpan.TotalDays);
								if (num3 < 90 && num3 > 0)
								{
									text2 = "";
								}
							}
						}
					}
					if (text2.Contains("renewalLicense"))
					{
						DateTime? nullable2 = RegistryWork.CheckDateTimeEnd();
						if (nullable2.HasValue)
						{
							timeSpan = nullable2.Value - DateTime.Now;
							int num4 = Convert.ToInt32(timeSpan.TotalDays);
							if (num4 > 90 || num4 < 0)
							{
								text2 = "";
							}
						}
						else
						{
							text2 = "";
						}
					}
					continue;
					IL_0132:
					text2 = "";
					goto IL_0139;
					IL_0139:
					if (text2.Contains("dinner"))
					{
						now = DateTime.Now;
						if (now.Hour >= 17)
						{
							now = DateTime.Now;
							if (now.Hour >= 22)
							{
								goto IL_016b;
							}
							goto IL_0172;
						}
						goto IL_016b;
					}
					goto IL_0172;
					IL_00f9:
					text2 = "";
					goto IL_0100;
					IL_0100:
					if (text2.Contains("lunch"))
					{
						now = DateTime.Now;
						if (now.Hour >= 12)
						{
							now = DateTime.Now;
							if (now.Hour >= 15)
							{
								goto IL_0132;
							}
							goto IL_0139;
						}
						goto IL_0132;
					}
					goto IL_0139;
				}
				lastNumber = num2;
				string str2 = System.Windows.Forms.Application.StartupPath + "\\Images\\";
				array = text2.Split(new string[1]
				{
					"=="
				}, StringSplitOptions.RemoveEmptyEntries);
				num = Convert.ToInt16(array[1]);
				text2 = array[3];
				str = ((!text2.Contains("table")) ? (str + "<table height='100%' width='100%' style='font-family: Verdana; Font-size: 9pt;'><tr height='50%'><td></tr></td><tr><td>" + text2.Replace("wndWaiting", "fromWndWaitingIn_" + Settings.ShortProgramNameInEnglishTransliteration).Replace("src='", "src='" + str2) + " </tr></td><tr height='50%'><td></tr></td></table>") : (str + text2.Replace("wndWaiting", "fromWndWaitingIn_" + Settings.ShortProgramNameInEnglishTransliteration).Replace("src='", "src='" + str2)));
				string newValue = Site.GetUrlToBuy() + "&ref=increase_or_renewal_from_waiting_window";
				str = str.Replace("ИМЯ_ПРОГРАММЫ", Settings.ShortProgramName).Replace("ССЫЛКА_НА_ЗАКАЗ", newValue);
				wbr.NavigateToString(str);
				tmr.Interval = new TimeSpan(0, 0, num);
			}
			catch (Exception)
			{
			}
		}

		public void ChangeText(string text)
		{
			TblText.Text = text;
		}

		public void CloseByProgram()
		{
			isClosingByProgram = true;
			base.Close();
		}

		private void WndWait_OnActivated(object sender, EventArgs e)
		{
			base.Visibility = Visibility.Visible;
		}

		private void Window_Closing(object sender, CancelEventArgs e)
		{
			if (!isClosingByProgram)
			{
				e.Cancel = true;
			}
			else
			{
				tmr.IsEnabled = false;
				tmr.Stop();
			}
		}

		private void wbr_Navigating(object sender, NavigatingCancelEventArgs e)
		{
			if (e.Uri != (Uri)null)
			{
				Process.Start(e.Uri.ToString());
				e.Cancel = true;
			}
		}
	}
}
