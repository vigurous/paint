using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;

namespace Бисерок
{
	public partial class wndWrongMixingColorsInColoring : Window, IComponentConnector
	{
		public wndWrongMixingColorsInColoring(List<Beads> wrongColors)
		{
			InitializeComponent();
			foreach (Beads wrongColor in wrongColors)
			{
				TextBlock obj = new TextBlock
				{
					Height = 18.0,
					Width = 300.0,
					Margin = new Thickness(3.0),
					HorizontalAlignment = HorizontalAlignment.Stretch
				};
				System.Drawing.Color clr = wrongColor.clr;
				byte r = clr.R;
				clr = wrongColor.clr;
				byte g = clr.G;
				clr = wrongColor.clr;
				obj.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(r, g, clr.B));
				TextBlock newItem = obj;
				lbxWrongMixColors.Items.Add(newItem);
			}
			TextBlock textBlock = tbx;
			textBlock.Text = textBlock.Text + " (" + wrongColors.Count + Local.Get(" шт.):");
		}

		private void btnOk_Click(object sender, RoutedEventArgs e)
		{
			base.Close();
		}
	}
}
