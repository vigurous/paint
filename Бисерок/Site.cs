using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using Бисерок.ServiceReference;

namespace Бисерок
{
	public static class Site
	{
		public static string SendResponse(string siteWithParams)
		{
			HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(siteWithParams);
			HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
			string str = "";
			using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8))
			{
				return str + streamReader.ReadToEnd();
			}
		}

		public static string GetInfoFromTag(string sourceHtml, string tag, bool isEncrypt)
		{
			if (isEncrypt)
			{
				List<string> list = sourceHtml.Split(new string[1]
				{
					"\r\n"
				}, StringSplitOptions.RemoveEmptyEntries).ToList();
				string text = "";
				foreach (string item in list)
				{
					string cipherText = item.TrimEnd(' ').TrimStart(' ');
					string str = Cryption.DecryptFromSite(cipherText, "JahskW", "UsdIma", "Haj9nPdg7**ja03N", "SHA1", 2, 256);
					text += str;
				}
				if (text.Contains(tag))
				{
					text = text.Split(new string[1]
					{
						tag + ">"
					}, StringSplitOptions.RemoveEmptyEntries)[1];
					return text.Substring(0, text.Length - 2);
				}
				return "";
			}
			string text2 = sourceHtml.Split(new string[1]
			{
				tag + ">"
			}, StringSplitOptions.RemoveEmptyEntries)[1];
			return text2.Substring(0, text2.Length - 2);
		}

		public static string GetDecryptInfo(string encryptedText, string pass, string salt, string initVect)
		{
			List<string> list = encryptedText.Split(new string[1]
			{
				"\r\n"
			}, StringSplitOptions.RemoveEmptyEntries).ToList();
			string text = "";
			foreach (string item in list)
			{
				string cipherText = item.TrimEnd(' ').TrimStart(' ');
				string str = Cryption.DecryptFromSite(cipherText, pass, salt, initVect, "SHA1", 2, 256);
				text += str;
			}
			return text;
		}

		public static bool IsReturnedOrder()
		{
			bool flag = false;
			try
			{
				ServicesClient servicesClient = new ServicesClient(Settings.ShortProgramName);
				string sourceHtml = servicesClient.CheckOrderOnReturn(new Guid(RegistryWork.GetIDComputer()));
				servicesClient.Close();
				return Convert.ToBoolean(GetInfoFromTag(sourceHtml, "ltrIsReturn", true));
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static OrderInfo GetInfoAboutOrder(string email)
		{
			OrderInfo orderInfo = new OrderInfo
			{
				email = email,
				isOkSendedToServer = false
			};
			try
			{
				ServicesClient servicesClient = new ServicesClient(Settings.ShortProgramName);
				string sourceHtml = servicesClient.CheckOrder(email, new Guid(RegistryWork.GetIDComputer()));
				servicesClient.Close();
				orderInfo.isActivatingLimit = Convert.ToBoolean(GetInfoFromTag(sourceHtml, "ltrActivatingLimit", true));
				orderInfo.isExpiredPayedOrder = Convert.ToBoolean(GetInfoFromTag(sourceHtml, "ltrExpiredPayedOrder", true));
				try
				{
					orderInfo.number = Convert.ToInt32(GetInfoFromTag(sourceHtml, "ltrNumber", true));
				}
				catch
				{
				}
				orderInfo.v = Convert.ToInt32(GetInfoFromTag(sourceHtml, "ltrVersion", true));
				if (GetInfoFromTag(sourceHtml, "ltrDateEnd", true).Length > 3)
				{
					orderInfo.dtEnd = Convert.ToDateTime(GetInfoFromTag(sourceHtml, "ltrDateEnd", true));
				}
				else
				{
					orderInfo.dtEnd = null;
				}
				orderInfo.isOkSendedToServer = true;
				return orderInfo;
			}
			catch (Exception ex)
			{
				SendExceptionInfo(ex, false);
				orderInfo.exception = ex;
				orderInfo.isOkSendedToServer = false;
				return orderInfo;
			}
		}

		public static void BuyProgram(int? newVersion)
		{
			string text = GetUrlToBuy();
			if (newVersion.HasValue)
			{
				text = text + "&newVersion=" + newVersion.ToString();
			}
			try
			{
				Process.Start(text);
			}
			catch
			{
				WndInfo wndInfo = new WndInfo("По какой-то причине программе не удалось открыть ссылку. Пожалуйста, скопируйте вручную ссылку из текстового поля ниже, вставьте в Ваш браузер и перейдите по ней самостоятельно. Спасибо!", text);
				wndInfo.ShowDialog();
			}
		}

		public static bool SendExceptionInfo(Exception exc, bool isUnhandled)
		{
			try
			{
				ServicesClient servicesClient = new ServicesClient(Settings.ShortProgramName);
				string message = "";
				string stackTrace = "";
				string innerMessage = "";
				string innerStackTrace = "";
				if (exc.Message != null)
				{
					message = exc.Message;
				}
				if (exc.StackTrace != null)
				{
					stackTrace = exc.StackTrace;
				}
				if (exc.InnerException != null)
				{
					if (exc.InnerException.Message != null)
					{
						innerMessage = exc.InnerException.Message;
					}
					if (exc.InnerException.StackTrace != null)
					{
						innerStackTrace = exc.InnerException.StackTrace;
					}
				}
				bool flag = servicesClient.ProgramException(new Guid(RegistryWork.GetIDComputer()), isUnhandled, message, stackTrace, innerMessage, innerStackTrace);
				servicesClient.Close();
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static void SendActionInfo(string action)
		{
			BackgroundWorker backgroundWorker = new BackgroundWorker();
			backgroundWorker.DoWork += bw_DoWork;
			backgroundWorker.RunWorkerAsync(action);
		}

		private static void bw_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				ServicesClient servicesClient = new ServicesClient(Settings.ShortProgramName);
				Assembly executingAssembly = Assembly.GetExecutingAssembly();
				string assemblyVersion = executingAssembly.GetName().Version.ToString();
				bool flag = servicesClient.ActionWithProgram(new Guid(RegistryWork.GetIDComputer()), Settings.v, assemblyVersion, (string)e.Argument);
				servicesClient.Close();
				e.Result = true;
			}
			catch (Exception)
			{
				e.Result = false;
			}
		}

		public static string GetUrlToBuy()
		{
			string text = RegistryWork.CheckEmail();
			int num = RegistryWork.CheckVersionAndCreateItIfNoExist();
			string iDComputer = RegistryWork.GetIDComputer();
			string text2 = Settings.siteAndEmailSettings.urlSite + "/Service/PayFromProgram.aspx?v=" + num.ToString();
			if (text.Length > 0)
			{
				text2 = text2 + "&email=" + text;
			}
			if (iDComputer != null && iDComputer.Length > 0)
			{
				text2 = text2 + "&idUserComputer=" + iDComputer.ToString();
			}
			return text2;
		}
	}
}
