using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Бисерок
{
	public partial class WndAddConsumblesOrder : Window, IComponentConnector
	{
		public string email;

		public WndAddConsumblesOrder()
		{
			InitializeComponent();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			tbxEmail.Text = RegistryWork.CheckEmail();
		}

		private void BtnDoConsumblesOrder_Click(object sender, RoutedEventArgs e)
		{
			email = tbxEmail.Text;
			base.DialogResult = true;
		}

		private void BtnCancel_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = false;
		}
	}
}
