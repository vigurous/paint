using System;
using System.Collections.Generic;
using System.Drawing;

namespace Бисерок
{
	[Serializable]
	public class ColoringContour
	{
		public List<Point> points = new List<Point>();

		public List<Point> skeletonPoints = new List<Point>();

		public int number;
	}
}
