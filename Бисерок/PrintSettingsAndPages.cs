namespace Бисерок
{
	public class PrintSettingsAndPages
	{
		public bool IsFullScheme
		{
			get;
			set;
		}

		public bool IsSchemePages
		{
			get;
			set;
		}

		public bool IsLegendLists
		{
			get;
			set;
		}

		public bool IsSchemeForPrintingOnTextile
		{
			get;
			set;
		}
	}
}
