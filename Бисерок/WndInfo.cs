using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace Бисерок
{
	public partial class WndInfo : Window, IComponentConnector
	{
		private string urlHref = "";

		private string Info
		{
			get;
			set;
		}

		public WndInfo(string info)
		{
			InitializeComponent();
			Info = info;
			RowDefinition rowDefinition = rowUrlAsTextbox;
			RowDefinition rowDefinition2 = rowUrlLinkAsActiveLink;
			GridLength gridLength3 = rowDefinition.Height = (rowDefinition2.Height = new GridLength(0.0));
		}

		public WndInfo(string info, Exception exc)
		{
			InitializeComponent();
			Info = info;
			string text = "";
			if (exc.Message != null)
			{
				text = text + Local.Get("ИНФОРМАЦИЯ ОБ ОШИБКЕ:") + "\r\n\r\nMESSAGE: " + exc.Message + "\r\n\r\n";
			}
			if (exc.StackTrace != null)
			{
				text = text + "STACKTRACE: " + exc.StackTrace + "\r\n\r\n";
			}
			if (exc.InnerException != null)
			{
				if (exc.InnerException.Message != null)
				{
					text = text + "INNER_MESSAGE: " + exc.InnerException.Message + "\r\n\r\n";
				}
				if (exc.InnerException.StackTrace != null)
				{
					text = text + "INNER_STACKTRACE: " + exc.InnerException.StackTrace + "\r\n\r\n";
				}
			}
			tbxAdditionalInfo.Visibility = Visibility.Visible;
			tbxAdditionalInfo.Height = 250.0;
			tbxAdditionalInfo.TextWrapping = TextWrapping.Wrap;
			tbxAdditionalInfo.Text = text;
			rowUrlLinkAsActiveLink.Height = new GridLength(0.0);
		}

		public WndInfo(string info, string url)
		{
			InitializeComponent();
			Info = info;
			tbxAdditionalInfo.Visibility = Visibility.Visible;
			tbxAdditionalInfo.Text = url;
			rowUrlLinkAsActiveLink.Height = new GridLength(0.0);
		}

		public WndInfo(string info, string _urlHref, string urlText)
		{
			InitializeComponent();
			Info = info;
			urlHref = _urlHref;
			tblActiveUrl.Text = urlText;
			RowDefinition rowDefinition = rowUrlAsTextbox;
			RowDefinition rowDefinition2 = rowOkButton;
			GridLength gridLength3 = rowDefinition.Height = (rowDefinition2.Height = new GridLength(0.0));
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			TblInfo.Text = Info;
		}

		private void btnOk_Click(object sender, RoutedEventArgs e)
		{
			base.Close();
		}

		private void tblActiveUrl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			try
			{
				Process.Start(urlHref);
			}
			catch
			{
				WndInfo wndInfo = new WndInfo(Local.Get("По какой-то причине программе не удалось открыть ссылку. Пожалуйста, скопируйте вручную ссылку из текстового поля ниже, вставьте в Ваш браузер и перейдите по ней самостоятельно. Спасибо!"), urlHref);
				wndInfo.ShowDialog();
			}
		}
	}
}
