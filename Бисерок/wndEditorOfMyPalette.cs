using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace Бисерок
{
	public partial class wndEditorOfMyPalette : Window, IComponentConnector
	{
		private BeadsManufacturer manufacturer;

		public bool isNeedToUpdateColorsInMainWindow;

		public wndEditorOfMyPalette()
		{
			InitializeComponent();
			if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				base.Title = "Редактор своих цветов мулине";
				tblDescription.Text = Local.Get("В этом окне Вы можете создать и отредактировать имеющиеся в Вашем распоряжении наборы мулине, чтобы в дальнейшем создавать под них схемы вышивки. Для начала создайте серию/серии мулине с помощью кнопки 'Добавить' (слева-внизу), и затем добавляйте к ней/ним конкретные цвета мулине.");
				tblSeries.Text = "Серии мулине:";
				tblConcreteColor.Text = "Мулине в выбранной серии:";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
			{
				base.Title = "Редактор своих цветов пряжи";
				tblDescription.Text = Local.Get("В этом окне Вы можете создать и отредактировать имеющиеся в Вашем распоряжении наборы пряжи, чтобы в дальнейшем создавать под них схемы вязания. Для начала создайте серию/серии пряжи с помощью кнопки 'Добавить' (слева-внизу), и затем добавляйте к ней/ним конкретные цвета пряжи.");
				tblSeries.Text = "Серии пряжи:";
				tblConcreteColor.Text = "Пряжа в выбранной серии:";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
			{
				base.Title = "Редактор своих цветов страз";
				tblDescription.Text = Local.Get("В этом окне Вы можете создать и отредактировать имеющиеся в Вашем распоряжении наборы страз, чтобы в дальнейшем создавать под них схемы вышивки. Для начала создайте серию/серии страз с помощью кнопки 'Добавить' (слева-внизу), и затем добавляйте к ней/ним конкретные цвета страз.");
				tblSeries.Text = "Серии страз:";
				tblConcreteColor.Text = "Стразы в выбранной серии:";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				base.Title = "Редактор своих цветов красок";
				tblDescription.Text = Local.Get("В этом окне Вы можете создать и отредактировать имеющиеся в Вашем распоряжении наборы красок, чтобы в дальнейшем создавать под них  раскраски и картины. Для начала создайте серию/серии цветов с помощью кнопки 'Добавить' (слева-внизу), и затем добавляйте к ней/ним конкретные цвета красок.");
				tblSeries.Text = "Серии красок:";
				tblConcreteColor.Text = "Краски в выбранной серии:";
			}
			LoadManufacturersToListFromFile();
			base.Title = Local.Get(base.Title);
			Local.LocalizeControlAndItsChildren(grdMain);
		}

		private void LoadManufacturersToListFromFile()
		{
			List<string> list = FileWorker.ReadFileToEnd(Settings.GetPathToFileInStorage("MyPalette.txt"), true, null).Split(new string[1]
			{
				"\r\n"
			}, StringSplitOptions.RemoveEmptyEntries).ToList();
			manufacturer = new BeadsManufacturer();
			BeadsSeries beadsSeries = new BeadsSeries();
			List<string> list2 = new List<string>();
			foreach (string item in list)
			{
				if (item.Contains("-m-"))
				{
					manufacturer = new BeadsManufacturer
					{
						name = item.Replace("-m-", "")
					};
				}
				else if (item.Contains("-s-"))
				{
					beadsSeries = new BeadsSeries
					{
						name = item.Replace("-s-", ""),
						parentManufacturer = manufacturer
					};
					manufacturer.beadsSeries.Add(beadsSeries);
				}
				else if (item.Replace(" ", "").Length > 0)
				{
					list2 = item.Split(' ').ToList();
					beadsSeries.beads.Add(new Beads
					{
						clr = Color.FromArgb(255, Convert.ToByte(list2[1]), Convert.ToByte(list2[2]), Convert.ToByte(list2[3])),
						name = list2[0].Replace("_", " "),
						parentSeries = beadsSeries
					});
				}
			}
		}

		private void UpdateListboxOfSeries()
		{
			lbxSeries.DataContext = null;
			lbxSeries.DataContext = manufacturer.beadsSeries;
			UpdateListBoxOfConcreteColors();
		}

		private void lbxSeries_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (lbxSeries.SelectedItem != null)
			{
				UpdateListBoxOfConcreteColors();
			}
		}

		private void tbxAddSeries_Click(object sender, RoutedEventArgs e)
		{
			wndEditorSeries wndEditorSeries = new wndEditorSeries();
			if (wndEditorSeries.ShowDialog().Value)
			{
				BeadsSeries beadsSeries = new BeadsSeries
				{
					name = wndEditorSeries.seriesName,
					parentManufacturer = manufacturer
				};
				beadsSeries.beads = new List<Beads>();
				manufacturer.beadsSeries.Add(beadsSeries);
				UpdateListboxOfSeries();
			}
		}

		private void tbxDeleteSeries_Click(object sender, RoutedEventArgs e)
		{
			if (lbxSeries.SelectedItem != null)
			{
				manufacturer.beadsSeries.Remove(lbxSeries.SelectedItem as BeadsSeries);
				UpdateListboxOfSeries();
			}
		}

		private void tbxEditSeries_Click(object sender, RoutedEventArgs e)
		{
			if (lbxSeries.SelectedItem != null)
			{
				BeadsSeries beadsSeries = lbxSeries.SelectedItem as BeadsSeries;
				wndEditorSeries wndEditorSeries = new wndEditorSeries(beadsSeries.name);
				if (wndEditorSeries.ShowDialog().Value)
				{
					beadsSeries.name = wndEditorSeries.seriesName;
					UpdateListboxOfSeries();
				}
			}
		}

		private void tbxAddConcreteColor_Click(object sender, RoutedEventArgs e)
		{
			if (lbxSeries.SelectedItem != null)
			{
				BeadsSeries beadsSeries = lbxSeries.SelectedItem as BeadsSeries;
				wndEditorConcreteColor wndEditorConcreteColor = new wndEditorConcreteColor();
				if (wndEditorConcreteColor.ShowDialog().Value)
				{
					beadsSeries.beads.Add(new Beads
					{
						clr = Color.FromArgb(255, wndEditorConcreteColor.color.R, wndEditorConcreteColor.color.G, wndEditorConcreteColor.color.B),
						name = wndEditorConcreteColor.name,
						parentSeries = beadsSeries
					});
					UpdateListBoxOfConcreteColors();
				}
			}
		}

		private void UpdateListBoxOfConcreteColors()
		{
			lbxConcreteColor.DataContext = null;
			if (lbxSeries.SelectedItem != null)
			{
				lbxConcreteColor.DataContext = (lbxSeries.SelectedItem as BeadsSeries).beads;
			}
			else
			{
				try
				{
					lbxSeries.SelectedIndex = 0;
				}
				catch
				{
				}
			}
		}

		private void tbxDeleteConcreteColor_Click(object sender, RoutedEventArgs e)
		{
			if (lbxConcreteColor.SelectedItem != null && lbxSeries.SelectedItem != null)
			{
				(lbxSeries.SelectedItem as BeadsSeries).beads.Remove(lbxConcreteColor.SelectedItem as Beads);
				UpdateListBoxOfConcreteColors();
			}
		}

		private void tbxEditConcreteColor_Click(object sender, RoutedEventArgs e)
		{
			if (lbxConcreteColor.SelectedItem != null)
			{
				Beads beads = lbxConcreteColor.SelectedItem as Beads;
				wndEditorConcreteColor wndEditorConcreteColor = new wndEditorConcreteColor(beads);
				if (wndEditorConcreteColor.ShowDialog().Value)
				{
					beads.name = wndEditorConcreteColor.name;
					beads.clr = Color.FromArgb(255, wndEditorConcreteColor.color.R, wndEditorConcreteColor.color.G, wndEditorConcreteColor.color.B);
					UpdateListBoxOfConcreteColors();
				}
			}
		}

		private void btnSave_Click(object sender, RoutedEventArgs e)
		{
			SaveAllChangesInPalette();
			isNeedToUpdateColorsInMainWindow = true;
			base.Close();
		}

		private void SaveAllChangesInPalette()
		{
			using (StreamWriter streamWriter = new StreamWriter(Settings.GetPathToFileInStorage("MyPalette.txt")))
			{
				streamWriter.WriteLine("-m-" + manufacturer.name);
				foreach (BeadsSeries item in manufacturer.beadsSeries)
				{
					streamWriter.WriteLine("-s-" + item.name);
					foreach (Beads bead in item.beads)
					{
						StreamWriter streamWriter2 = streamWriter;
						string[] obj = new string[8]
						{
							bead.name.Replace(" ", "_"),
							" ",
							null,
							null,
							null,
							null,
							null,
							null
						};
						Color clr = bead.clr;
						byte b = clr.R;
						obj[2] = b.ToString();
						obj[3] = " ";
						clr = bead.clr;
						b = clr.G;
						obj[4] = b.ToString();
						obj[5] = " ";
						clr = bead.clr;
						b = clr.B;
						obj[6] = b.ToString();
						obj[7] = " ";
						streamWriter2.WriteLine(string.Concat(obj));
					}
				}
			}
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			base.Close();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			if (Settings.hobbyProgramm != HobbyProgramm.Raskraska || Settings.v > 1)
			{
				tblAboutLimitOfOwnColorsInCommonVersion.Visibility = Visibility.Collapsed;
				grdMain.RowDefinitions[1].Height = new GridLength(63.0);
			}
			UpdateListboxOfSeries();
		}

		private void tblSaveMyPaletteToFile_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog
			{
				Filter = "Текстовый файл (*.txt)|*.txt"
			};
			bool? nullable = saveFileDialog.ShowDialog();
			if (nullable.HasValue && nullable.Value)
			{
				try
				{
					SaveAllChangesInPalette();
					isNeedToUpdateColorsInMainWindow = true;
					File.Copy(Settings.GetPathToFileInStorage("MyPalette.txt"), saveFileDialog.FileName, true);
				}
				catch
				{
					MessageBox.Show("Произошла ошибка при сохранении файла.", "Внимание!");
				}
			}
		}

		private void tblLoadMyPaletteFromFile_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog
			{
				Title = "Выберите файл с сохранённой ранее палитрой",
				Filter = "Файл с цветами (*.zcv, *.txt)|*.zcv;*.txt"
			};
			bool? nullable = openFileDialog.ShowDialog();
			if (nullable.HasValue && nullable.Value)
			{
				try
				{
					File.Copy(openFileDialog.FileName, Settings.GetPathToFileInStorage("MyPalette.txt"), true);
					LoadManufacturersToListFromFile();
					UpdateListboxOfSeries();
					isNeedToUpdateColorsInMainWindow = true;
				}
				catch
				{
					MessageBox.Show("Произошла ошибка при сохранении файла.", "Внимание!");
				}
			}
		}

		private void tblWriteEmailToAuthorAboutCommercialUsing_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			Process.Start("mailto:" + Settings.AddressOfAuthorOfProgram + "?subject=" + Local.Get("Профессиональная версия Раскраски (больше цветов)"));
		}

		
	}
}
