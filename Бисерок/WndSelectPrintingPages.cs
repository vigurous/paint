using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Бисерок
{
	public partial class WndSelectPrintingPages : Window, IComponentConnector
	{
		public PrintSettingsAndPages printSettings = new PrintSettingsAndPages();

		public WndSelectPrintingPages()
		{
			InitializeComponent();
			if (Settings.hobbyProgramm != HobbyProgramm.Biserok)
			{
				cbxSchemeForPrintingOnTextile.Visibility = Visibility.Collapsed;
				cbxSchemeForPrintingOnTextile.IsChecked = false;
			}
			if (Settings.hobbyProgramm != HobbyProgramm.Raskraska && !Settings.isItSpecialVersionOfBiserokForMaxim)
			{
				return;
			}
			CbxSchemePages.Visibility = Visibility.Collapsed;
			CbxSchemePages.IsChecked = false;
			CbxFullScheme.Visibility = Visibility.Collapsed;
			CbxFullScheme.IsChecked = false;
		}

		private void btnPrint_Click(object sender, RoutedEventArgs e)
		{
			printSettings.IsFullScheme = (CbxFullScheme.IsChecked.HasValue && CbxFullScheme.IsChecked.Value);
			printSettings.IsSchemePages = (CbxSchemePages.IsChecked.HasValue && CbxSchemePages.IsChecked.Value);
			printSettings.IsLegendLists = (CbxLegendLists.IsChecked.HasValue && CbxLegendLists.IsChecked.Value);
			if (Settings.hobbyProgramm != 0)
			{
				printSettings.IsSchemeForPrintingOnTextile = (cbxSchemeForPrintingOnTextile.IsChecked.HasValue && cbxSchemeForPrintingOnTextile.IsChecked.Value);
			}
			else
			{
				printSettings.IsSchemeForPrintingOnTextile = false;
			}
			base.DialogResult = true;
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = false;
		}
	}
}
