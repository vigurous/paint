using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using Бисерок.Properties;

namespace Бисерок
{
	public partial class wndSettingsForColoring : Window, IComponentConnector
	{
		private Scheme scheme;

		public wndSettingsForColoring(Scheme _scheme)
		{
			InitializeComponent();
			scheme = _scheme;
		}

		private void tbxPrintingResolution_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				int num = Convert.ToInt32((sender as TextBox).Text);
			}
			catch (Exception)
			{
				(sender as TextBox).Text = "300";
				MessageBox.Show("В это поле можно вводить только  целые числа");
			}
		}

		private void btnSave_Click(object sender, RoutedEventArgs e)
		{
			scheme.Param.PrintingResolution = Convert.ToInt32(tbxPrintingResolution.Text);
			if (rbtWayOfBadgesCreating1.IsChecked.Value)
			{
				scheme.Param.wayOfBadgesCreating = 1;
			}
			else if (rbtWayOfBadgesCreating3.IsChecked.Value)
			{
				scheme.Param.wayOfBadgesCreating = 3;
			}
			else
			{
				scheme.Param.wayOfBadgesCreating = 4;
			}
			if (Settings.v >= 3)
			{
				scheme.Param.minFontSizeInPt = (float)Convert.ToInt32(tbxMinFontSizeInPt.Text);
			}
			Бисерок.Properties.Settings.Default.Save();
			base.Close();
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			Бисерок.Properties.Settings.Default.Reload();
			base.Close();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			rbtWayOfBadgesCreating1.IsChecked = (scheme.Param.wayOfBadgesCreating == 1);
			rbtWayOfBadgesCreating3.IsChecked = (scheme.Param.wayOfBadgesCreating == 3);
			rbtWayOfBadgesCreating4.IsChecked = (scheme.Param.wayOfBadgesCreating == 4);
			if (Settings.v > 1)
			{
				grdSettingsOfContoursForProfessionalVersion.IsEnabled = true;
				if (Settings.v >= 3)
				{
					grdSettingsOfContoursForCommercialVersion.IsEnabled = true;
				}
				else
				{
					grdSettingsOfContoursForCommercialVersion.IsEnabled = false;
				}
			}
			else
			{
				grdSettingsOfContoursForProfessionalVersion.IsEnabled = false;
			}
		}

		private void Window_Closing(object sender, CancelEventArgs e)
		{
			Бисерок.Properties.Settings.Default.Reload();
		}

		private void tbxMinFontSizeInPt_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				int num = Convert.ToInt32((sender as TextBox).Text);
			}
			catch (Exception)
			{
				(sender as TextBox).Text = "1";
				MessageBox.Show("В это поле можно вводить только целые числа");
			}
		}
	}
}
