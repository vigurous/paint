using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Media;

namespace Бисерок
{
	public class Node : INotifyPropertyChanged
	{
		private ObservableCollection<Node> children = new ObservableCollection<Node>();

		private ObservableCollection<Node> parent = new ObservableCollection<Node>();

		private string text;

		private string id;

		private Brush brush;

		private bool? isChecked = true;

		private bool isExpanded;

		private int width;

		public object Tag;

		public int Width
		{
			get
			{
				return width;
			}
			set
			{
				width = value;
			}
		}

		public ObservableCollection<Node> Children => children;

		public ObservableCollection<Node> Parent => parent;

		public bool? IsChecked
		{
			get
			{
				return isChecked;
			}
			set
			{
				isChecked = value;
				RaisePropertyChanged("IsChecked");
			}
		}

		public string Text
		{
			get
			{
				return text;
			}
			set
			{
				text = value;
				RaisePropertyChanged("Text");
			}
		}

		public Brush Brush
		{
			get
			{
				return brush;
			}
			set
			{
				brush = value;
				RaisePropertyChanged("Brush");
			}
		}

		public bool IsExpanded
		{
			get
			{
				return isExpanded;
			}
			set
			{
				isExpanded = value;
				RaisePropertyChanged("IsExpanded");
			}
		}

		public string Id
		{
			get
			{
				return id;
			}
			set
			{
				id = value;
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public Node()
		{
			id = Guid.NewGuid().ToString();
		}

		private void RaisePropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
			int countCheck = 0;
			if (propertyName == "IsChecked")
			{
				if (Id == CheckBoxId.checkBoxId && Parent.Count == 0 && Children.Count != 0)
				{
					CheckedTreeParent(Children, IsChecked);
				}
				if (Id == CheckBoxId.checkBoxId && Parent.Count > 0 && Children.Count > 0)
				{
					CheckedTreeChildMiddle(Parent, Children, IsChecked);
				}
				if (Id == CheckBoxId.checkBoxId && Parent.Count > 0 && Children.Count == 0)
				{
					CheckedTreeChild(Parent, countCheck);
				}
			}
		}

		private void CheckedTreeChildMiddle(ObservableCollection<Node> itemsParent, ObservableCollection<Node> itemsChild, bool? isChecked)
		{
			int countCheck = 0;
			CheckedTreeParent(itemsChild, isChecked);
			CheckedTreeChild(itemsParent, countCheck);
		}

		private void CheckedTreeParent(ObservableCollection<Node> items, bool? isChecked)
		{
			foreach (Node item in items)
			{
				item.IsChecked = isChecked;
				if (item.Children.Count != 0)
				{
					CheckedTreeParent(item.Children, isChecked);
				}
			}
		}

		private void CheckedTreeChild(ObservableCollection<Node> items, int countCheck)
		{
			bool flag = false;
			foreach (Node item in items)
			{
				foreach (Node child in item.Children)
				{
					if (child.IsChecked == true || !child.IsChecked.HasValue)
					{
						countCheck++;
						if (!child.IsChecked.HasValue)
						{
							flag = true;
						}
					}
				}
				if (countCheck != item.Children.Count && countCheck != 0)
				{
					item.IsChecked = null;
				}
				else if (countCheck == 0)
				{
					item.IsChecked = false;
				}
				else if (countCheck == item.Children.Count & flag)
				{
					item.IsChecked = null;
				}
				else if (countCheck == item.Children.Count && !flag)
				{
					item.IsChecked = true;
				}
				if (item.Parent.Count != 0)
				{
					CheckedTreeChild(item.Parent, 0);
				}
			}
		}
	}
}
