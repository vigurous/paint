using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Бисерок
{
	[Serializable]
	public class BeadsSeries
	{
		public bool isUsing = true;

		public List<Beads> beads = new List<Beads>();

		[DefaultValue("")]
		public string name
		{
			get;
			set;
		}

		public BeadsManufacturer parentManufacturer
		{
			get;
			set;
		}
	}
}
