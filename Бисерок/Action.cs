namespace Бисерок
{
	public enum Action
	{
		addingColor,
		removingColor,
		drawingFill,
		drawingPen
	}
}
