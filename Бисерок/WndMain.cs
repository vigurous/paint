using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;
using Бисерок.Properties;
using Бисерок.ServiceReference;
using MessageBox = System.Windows.MessageBox;

namespace Бисерок
{
	public partial class WndMain : Window, IComponentConnector, IStyleConnector
	{
		public Scheme scheme = new Scheme();

		public static RoutedCommand CommandCancelRework = new RoutedCommand();

		public static RoutedCommand CommandCancelHandEditing = new RoutedCommand();

		public static RoutedCommand CommandImageBiggerHandEditingScheme = new RoutedCommand();

		public static RoutedCommand CommandImageSmallerHandEditingScheme = new RoutedCommand();

		public static RoutedCommand CommandImageBiggerReworkScheme = new RoutedCommand();

		public static RoutedCommand CommandImageSmallerReworkScheme = new RoutedCommand();

		public static RoutedCommand CommandImageBiggerSrcScheme = new RoutedCommand();

		public static RoutedCommand CommandImageSmallerSrcScheme = new RoutedCommand();

		public static RoutedCommand CommandAboutInVersionWithoutContactInfo = new RoutedCommand();

		private ModeOfManualEditing modeOfManualEditing;

		private System.Windows.Media.Color? lastColorOnManualEditing;

		private bool isEnabledToSelectColorOnManualEditing;

		private bool isChanginglbxReplaceColorSelectedItemProgrammatically;

		private bool isLastEditingModeIsFill;

		private List<StateSchemesPart> prevStateBlocksOnReworking = new List<StateSchemesPart>();

		private List<StateSchemesPart> prevStateBlocksOnHandEditing = new List<StateSchemesPart>();

		private bool isNeedToRefreshListWithBeadsInPaletteOfHandEditingScheme;

		private static int v;

		private int maxEnabledCountColorEditingForSmallVersion;

		private int maxEnabledCountHandEditingForSmallVersion;

		private WriteableBitmap wbmpRework;

		private WriteableBitmap wbmpHandEditing;

		private TypeOfStitch stitchForHandEditing;

		private bool IsInDrawingBackstich;

		private bool isInfoAboutOtherProgramLoaded;

		private static readonly Ellipse elpBrushForMakingInvisibleFake = new Ellipse
		{
			Stroke = System.Windows.Media.Brushes.White,
			StrokeThickness = 1.5,
			VerticalAlignment = VerticalAlignment.Top,
			HorizontalAlignment = System.Windows.HorizontalAlignment.Left
		};

		private static readonly Ellipse elpBrushForMakingInvisible = new Ellipse
		{
			Stroke = System.Windows.Media.Brushes.White,
			StrokeThickness = 1.5,
			VerticalAlignment = VerticalAlignment.Top,
			HorizontalAlignment = System.Windows.HorizontalAlignment.Left
		};

		private static readonly Ellipse elpBrushForHandEditing2 = new Ellipse
		{
			Stroke = System.Windows.Media.Brushes.Black,
			StrokeThickness = 1.0,
			VerticalAlignment = VerticalAlignment.Top,
			HorizontalAlignment = System.Windows.HorizontalAlignment.Left
		};

		private static readonly Ellipse elpBrushForHandEditing1 = new Ellipse
		{
			Stroke = System.Windows.Media.Brushes.White,
			StrokeThickness = 1.0,
			VerticalAlignment = VerticalAlignment.Top,
			HorizontalAlignment = System.Windows.HorizontalAlignment.Left
		};

		private static readonly Ellipse elpBrushForHandEditing3 = new Ellipse
		{
			Stroke = System.Windows.Media.Brushes.White,
			StrokeThickness = 1.0,
			VerticalAlignment = VerticalAlignment.Top,
			HorizontalAlignment = System.Windows.HorizontalAlignment.Left
		};

		private string errorMessage = "";

		private string file = "";

		private string Result = "";

		private bool isOkLoading = true;

		private string nameScheme = "";

		private string articleScheme = "";

		private string selectedPath = "";

		private DateTime dt;

		private string path;

		private Bitmap processingSourceBmp;

		private string txt = "";
		private static double Scale_FOR_PROPORTION_ReworkScheme
		{
			get;
			set;
		}

		private static double Scale_FOR_PROPORTION_HandEditingScheme
		{
			get;
			set;
		}

		private float ScaleImageRework
		{
			get;
			set;
		}

		private float ScaleSchemeRework
		{
			get;
			set;
		}

		private float ScaleSchemeManualEditing
		{
			get;
			set;
		}

		private bool IsChangingSchemesSizeInProgram
		{
			get;
			set;
		}

		public bool IsInProcess
		{
			get;
			set;
		}

		public ObservableCollection<Node> Nodes
		{
			get;
			private set;
		}

		public WndMain()
		{
			InitializeComponent();
			v = Settings.v;
			maxEnabledCountColorEditingForSmallVersion = ((v < 2) ? 3 : 1000000);
			maxEnabledCountHandEditingForSmallVersion = ((v < 3) ? 3 : 1000000);
			SrcPhotoReworkImage.MouseLeftButtonUp += SrcPhotoReworkImage_MouseLeftButtonUp;
			imgReworkScheme.MouseLeftButtonUp += imgReworkScheme_MouseLeftButtonUp;
			imgReworkScheme.MouseMove += imgReworkScheme_MouseMove;
			imgReworkScheme.MouseDown += imgReworkScheme_MouseDown;
			imgReworkScheme.MouseLeave += imgReworkScheme_MouseLeave;
			imgReworkScheme.MouseEnter += imgReworkScheme_MouseEnter;
			RbtCorrectionModeColor.Checked += RbtCorrectionModeColor_Checked;
			RbtCorrectionModeSewing.Checked += RbtCorrectionModeSewing_Checked;
			System.Windows.Controls.Image image = imgForHandEditing;
			System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
			System.Windows.Input.Cursor cursor3 = image.Cursor = (image2.Cursor = new System.Windows.Input.Cursor(AppDomain.CurrentDomain.BaseDirectory + "Images\\pipet.cur"));
			ConfigureWindowAllowToProgramTypeAndVersionAndDateExpired();
			UpdateAllContentTextTooltipsAtCurrentLanguage();
        }

        private void UpdateAllContentTextTooltipsAtCurrentLanguage()
		{
			LocalizeItAndItsChildren(grdFirstTab);
			LocalizeItAndItsChildren(grdMain);
			LocalizeItAndItsChildren(gbxMainSettingScheme_header);
			TabCreateScheme.Header = Local.Get(TabCreateScheme.Header.ToString());
			LocalizeItAndItsChildren(grdSchemeRework);
			LocalizeItAndItsChildren(grd2);
			LocalizeItAndItsChildren(grdReworkingSettings);
			TabReworkScheme.Header = Local.Get(TabReworkScheme.Header.ToString());
			LocalizeItAndItsChildren(grdThirdTab);
			LocalizeItAndItsChildren(grd3);
			tbiHandEditing.Header = Local.Get(tbiHandEditing.Header.ToString());
			cpkColorFromList.AvailableColorsHeader = Local.Get(cpkColorFromList.AvailableColorsHeader);
			cpkColorFromList.RecentColorsHeader = Local.Get(cpkColorFromList.RecentColorsHeader);
		}

		private void LocalizeItAndItsChildren(UIElement control)
		{
			Local.LocalizeControlAndItsChildren(control);
		}

		private void ConfigureWindowAllowToProgramTypeAndVersionAndDateExpired()
		{
			if (Settings.isItVersionWithoutContactInfo)
			{
				TextBlock textBlock = tblRestrictionsHandEditingTab;
				TextBlock textBlock2 = tblRestrictionsMainTab;
				TextBlock textBlock3 = tblRestrictionsReworkTab;
				System.Windows.Controls.Button button = btnBuyHandEditingTab;
				System.Windows.Controls.Button button2 = btnBuyMainTab;
				System.Windows.Controls.Button button3 = btnBuyReworkTab;
				bool flag2 = button3.IsEnabled = false;
				bool flag4 = button2.IsEnabled = flag2;
				bool flag6 = button.IsEnabled = flag4;
				bool flag8 = textBlock3.IsEnabled = flag6;
				bool isEnabled = textBlock2.IsEnabled = flag8;
				textBlock.IsEnabled = isEnabled;
			}
			App.SetLogoAndButtonsStyle();
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && Settings.isItVersionWithAbilityOfTurningOffSourceImageChanging)
			{
				cbxIsNotChangeSourceImageInColoring.Visibility = Visibility.Visible;
			}
			else
			{
				cbxIsNotChangeSourceImageInColoring.Visibility = Visibility.Collapsed;
			}
			if (Settings.isItSpecialVersionOfRaskraskaForNikolayUkraina)
			{
				slrScaleImageForHandEditing.Maximum = 2000.0;
			}
			brdReplaceColorDisabledInDemoVersion.Visibility = Visibility.Collapsed;
			GridLength gridLength5;
			switch (v)
			{
			case 0:
			{
				tbxCountColors.Text = Settings.countColorsInDemo.ToString();
				brdFullSettingsDisabledInDemoVerdion.Visibility = Visibility.Visible;
				gbxMainSettingScheme.IsEnabled = false;
				tblLoadFromSavedScheme.IsEnabled = false;
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					tblLoadFromSavedScheme.Text = Local.Get("Б. Загрузить сохранённую картину (доступно в коммерч.версии)");
					tblLoadFromSavedScheme.Width = 200.0;
				}
				base.Title += Local.Get(" ТЕСТОВАЯ ВЕРСИЯ");
				TextBlock textBlock17 = tblVersionMainTab;
				TextBlock textBlock18 = tblVersionReworkTab;
				TextBlock textBlock19 = tblVersionHandEditingTab;
				string text2 = textBlock19.Text = "ТЕСТОВАЯ версия программы:";
				string text5 = textBlock17.Text = (textBlock18.Text = text2);
				TextBlock textBlock20 = tblReworkSavePrintBtnsDisabled;
				TextBlock textBlock21 = tblHandSavePrintBtnsDisabled;
				Visibility visibility4 = textBlock21.Visibility = Visibility.Visible;
				textBlock20.Visibility = visibility4;
				tblEditorOfMyPalette.IsEnabled = false;
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					RbtSimpleSelectingColors.IsChecked = true;
				}
				brdWarningAboutEmbroideryView.Visibility = Visibility.Visible;
				tblCountOfCountours.Visibility = Visibility.Collapsed;
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					btnBuyMainTab.Content = "Купить ПОЛНУЮ версию, чтобы активировать эту вкладку";
					gbxReplaceColorInHandEditingScheme.IsEnabled = false;
					brdReplaceColorDisabledInDemoVersion.Visibility = Visibility.Visible;
				}
				break;
			}
			case 1:
			{
				brdFullSettingsDisabledInDemoVerdion.Visibility = Visibility.Collapsed;
				if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
				{
					VersionRowMainTab.Height = new GridLength(0.0);
				}
				if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
				{
					tblLoadFromSavedScheme.IsEnabled = true;
					tblLoadFromSavedScheme.Text = "Б. Загрузить сохранённую схему";
				}
				else
				{
					tblLoadFromSavedScheme.IsEnabled = false;
					tblLoadFromSavedScheme.Text = "Б. Загрузить сохранённую картину (доступно в коммерч.версии)";
					tblLoadFromSavedScheme.Width = 200.0;
				}
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					base.Title += " ДОМАШНЯЯ ВЕРСИЯ";
					TextBlock textBlock6 = tblVersionMainTab;
					TextBlock textBlock7 = tblVersionReworkTab;
					TextBlock textBlock8 = tblVersionHandEditingTab;
					string text2 = textBlock8.Text = "ДОМАШНЯЯ версия программы:";
					string text5 = textBlock6.Text = (textBlock7.Text = text2);
					btnBuyMainTab.Content = "Купить ПРОФЕССИОНАЛЬНУЮ версию программы";
				}
				else
				{
					base.Title += " БАЗОВАЯ ВЕРСИЯ";
					TextBlock textBlock9 = tblVersionMainTab;
					TextBlock textBlock10 = tblVersionReworkTab;
					TextBlock textBlock11 = tblVersionHandEditingTab;
					string text2 = textBlock11.Text = "БАЗОВАЯ версия программы:";
					string text5 = textBlock9.Text = (textBlock10.Text = text2);
				}
				TextBlock textBlock12 = tblReworkSavePrintBtnsDisabled;
				TextBlock textBlock13 = tblHandSavePrintBtnsDisabled;
				Visibility visibility4 = textBlock13.Visibility = Visibility.Visible;
				textBlock12.Visibility = visibility4;
				tblEditorOfMyPalette.IsEnabled = false;
				brdWarningAboutEmbroideryView.Visibility = Visibility.Collapsed;
				ScvSrcPhotoCreate.Margin = new Thickness(10.0, 36.0, 4.0, 2.0);
				ScvResSchemeCreate.Margin = new Thickness(4.0, 36.0, 10.0, 2.0);
				break;
			}
			case 2:
			{
				brdFullSettingsDisabledInDemoVerdion.Visibility = Visibility.Collapsed;
				RowDefinition versionRowMainTab2 = VersionRowMainTab;
				RowDefinition versionRowReworkTab2 = VersionRowReworkTab;
				gridLength5 = (versionRowMainTab2.Height = (versionRowReworkTab2.Height = new GridLength(0.0)));
				if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
				{
					tblLoadFromSavedScheme.IsEnabled = true;
					tblLoadFromSavedScheme.Text = "Б. Загрузить сохранённую схему";
				}
				else
				{
					tblLoadFromSavedScheme.IsEnabled = false;
					tblLoadFromSavedScheme.Text = "Б. Загрузить сохранённую картину (доступно в коммерч.версии)";
					tblLoadFromSavedScheme.Width = 200.0;
				}
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					base.Title += " ПРОФЕССИОНАЛЬНАЯ ВЕРСИЯ";
					Slider slider3 = slrMediana;
					Slider slider4 = slrNumberOfMedianaRepeats;
					double num3 = slider3.Minimum = (slider4.Minimum = 1.0);
				}
				else
				{
					base.Title += " ПРОДВИНУТАЯ ВЕРСИЯ";
				}
				tblRemoveAloneBlockDisabled.Visibility = Visibility.Hidden;
				tblReworkSavePrintBtnsDisabled.Visibility = Visibility.Hidden;
				TextBlock textBlock14 = tblVersionMainTab;
				TextBlock textBlock15 = tblVersionReworkTab;
				TextBlock textBlock16 = tblVersionHandEditingTab;
				string text2 = textBlock16.Text = "ПРОДВИНУТАЯ версия программы:";
				string text5 = textBlock14.Text = (textBlock15.Text = text2);
				tblHandSavePrintBtnsDisabled.Visibility = Visibility.Visible;
				tblEditorOfMyPalette.IsEnabled = false;
				brdWarningAboutEmbroideryView.Visibility = Visibility.Collapsed;
				ScvSrcPhotoCreate.Margin = new Thickness(10.0, 36.0, 4.0, 2.0);
				ScvResSchemeCreate.Margin = new Thickness(4.0, 36.0, 10.0, 2.0);
				break;
			}
			case 3:
			{
				brdFullSettingsDisabledInDemoVerdion.Visibility = Visibility.Collapsed;
				tblLoadFromSavedScheme.IsEnabled = true;
				if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
				{
					tblLoadFromSavedScheme.Text = "Б. Загрузить сохранённую схему";
				}
				else
				{
					tblLoadFromSavedScheme.Text = "Б. Загрузить сохранённую картину";
				}
				base.Title += " МАСТЕР-ВЕРСИЯ";
				tblRemoveAloneBlockDisabled.Visibility = Visibility.Hidden;
				TextBlock textBlock4 = tblReworkSavePrintBtnsDisabled;
				TextBlock textBlock5 = tblHandSavePrintBtnsDisabled;
				Visibility visibility4 = textBlock5.Visibility = Visibility.Hidden;
				textBlock4.Visibility = visibility4;
				RowDefinition versionRowMainTab = VersionRowMainTab;
				RowDefinition versionRowReworkTab = VersionRowReworkTab;
				RowDefinition versionRowHandEditingTab = VersionRowHandEditingTab;
				GridLength gridLength2 = versionRowHandEditingTab.Height = new GridLength(0.0);
				gridLength5 = (versionRowMainTab.Height = (versionRowReworkTab.Height = gridLength2));
				tblMoveToHandEditingFromRework.Text = "Б. Из вкладки \"Доработка\"";
				tblEditorOfMyPalette.IsEnabled = true;
				tblEditorOfMyPalette.Text = "Редактор своих цветов бисера";
				brdWarningAboutEmbroideryView.Visibility = Visibility.Collapsed;
				ScvSrcPhotoCreate.Margin = new Thickness(10.0, 36.0, 4.0, 2.0);
				ScvResSchemeCreate.Margin = new Thickness(4.0, 36.0, 10.0, 2.0);
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					base.Title += " КОММЕРЧЕСКАЯ ВЕРСИЯ";
					Slider slider = slrMediana;
					Slider slider2 = slrNumberOfMedianaRepeats;
					double num3 = slider.Minimum = (slider2.Minimum = 1.0);
				}
				break;
			}
			}
			if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
			{
				LoadCatalogueOfManufacturers();
				base.Title = "Бисерок 2.0 - создавайте схемы вышивки бисером из Ваших фотографий и картинок!";
				RbtPreciosaSelectingColors.Content = "Для бисера (выберите):";
				RowDefinition rowDefinition = grdMainSettings.RowDefinitions[3];
				RowDefinition rowDefinition2 = grdMainSettings.RowDefinitions[1];
				gridLength5 = (rowDefinition.Height = (rowDefinition2.Height = new GridLength(0.0)));
				grdReworkingSettings.RowDefinitions[2].Height = new GridLength(0.0);
				RowDefinition rowDefinition3 = grdSchemeRework.RowDefinitions[1];
				gridLength5 = grdSchemeRework.RowDefinitions[1].Height;
				rowDefinition3.Height = new GridLength(gridLength5.Value - 48.0);
				TextBlock tblSchemeSourceAndCountColors = TblSchemeSourceAndCountColors;
				TextBlock tblSchemeReworkAndCountColors = TblSchemeReworkAndCountColors;
				TextBlock textBlock22 = tblCountColorsAfterManualEditing;
				string text2 = textBlock22.Text = Settings.nameOfWork + ": ";
				string text5 = tblSchemeSourceAndCountColors.Text = (tblSchemeReworkAndCountColors.Text = text2);
				gbxSelectNearestColors.Header = " 3. Выберите бисер нужного цвета";
				gbxReplaceColorInHandEditingScheme.Header = "*Цвет и число бисерин";
				RbtSimpleSelectingColors.ToolTip = "Схема будет создана не под конкретного производителя бисера, а просто под обычные цвета (нужный бисер Вам придётся подбирать самостоятельно)";
				RbtPreciosaSelectingColors.ToolTip = "Схема будет создана под выбранные Вами ниже позиции: производители бисера/серии бисера/конкретный бисер";
				trvCatalog.ToolTip = "Вы можете выбрать производителей бисера, серии бисера или конкретные номера бисера, из которых будут подбираться цвета в схему (например, для ситуации, когда Вы точно знаете, что не сможете приобрести какую-либо серию бисера, Вы можете исключить её из подбора)";
				btnShowSettings.ToolTip = "Выбор размера бисера, настройки печати и сохранения схем";
				tblDescriptionOfColorEditingMode.Text = tblDescriptionOfColorEditingMode.Text.Replace("РЕЗУЛЬТАТУ", "ВЫШИВКЕ");
				tblDescriptionOfHidingSewingMode.Text = tblDescriptionOfHidingSewingMode.Text.Replace("РЕЗУЛЬТАТУ", "ВЫШИВКЕ");
				System.Windows.Controls.ContextMenu contextMenu = new System.Windows.Controls.ContextMenu();
				TextBlock textBlock23 = new TextBlock
				{
					Padding = new Thickness(3.0),
					Text = "Выбрать бисер, наиболее часто присутствующий в продаже"
				};
				textBlock23.MouseLeftButtonUp += tblSelectIsInStockBeads_MouseLeftButtonUp;
				TextBlock textBlock24 = new TextBlock
				{
					Padding = new Thickness(3.0),
					Text = "Выбрать все"
				};
				textBlock24.MouseLeftButtonUp += tblSelectAll_MouseLeftButtonUp;
				TextBlock textBlock25 = new TextBlock
				{
					Padding = new Thickness(3.0),
					Text = "Убрать все"
				};
				textBlock25.MouseLeftButtonUp += tblDeselectAll_MouseLeftButtonUp;
				contextMenu.Items.Add(textBlock23);
				contextMenu.Items.Add(textBlock24);
				contextMenu.Items.Add(textBlock25);
				trvCatalog.ContextMenu = contextMenu;
				tblWarningAboutEmbroideryView.Text = "Ниже представлен лишь вид вышивки. Чтобы получить саму схему вышивки и карту цветов - сохраните созданную вышивку! Кнопка 'Сохранить' находится слева-внизу этого окна.";
				tblCountOfCountours.Visibility = Visibility.Collapsed;
				slrBrushSize.Width = 168.0;
				this.textBlock7.Margin = new Thickness(0.0, -2.0, 4.0, 0.0);
				cmbTypeOfStitchForDrawing.Visibility = Visibility.Collapsed;
				System.Windows.Controls.Button button4 = btnUndoBackstitchInHandEditing;
				System.Windows.Controls.Button button5 = btnUndoKnotInHandEditing;
				Visibility visibility4 = button5.Visibility = Visibility.Collapsed;
				button4.Visibility = visibility4;
				btnUndoManualEditingScheme.Content = "Отмена (Ctrl + Z)";
				grdReplaceColorOnHandEditingTab.RowDefinitions[1].Height = new GridLength(0.0);
				System.Windows.Controls.Button button6 = btnOrderConsublesInCreateTab;
				System.Windows.Controls.Button button7 = btnOrderConsublesInReworkTab;
				System.Windows.Controls.Button button8 = btnOrderConsublesInHandEditingTab;
				Visibility visibility2 = button8.Visibility = Visibility.Collapsed;
				visibility4 = (button7.Visibility = visibility2);
				button6.Visibility = visibility4;
				tblSizeInBlocks.Text = "в бисеринках:";
				TextBlock textBlock26 = tblBrushSizeInPxForRaskraska;
				System.Windows.Controls.TextBox textBox = tbxBrushSizeInPxForRaskraska;
				visibility4 = (textBox.Visibility = Visibility.Collapsed);
				textBlock26.Visibility = visibility4;
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				LoadCatalogueOfManufacturers();
				base.Title = "Крестик 2.0 - создавайте схемы вышивки крестом из Ваших фотографий и картинок!";
				RowDefinition rowDefinition4 = grdMainSettings.RowDefinitions[1];
				RowDefinition rowDefinition5 = grdMainSettings.RowDefinitions[2];
				RowDefinition rowDefinition6 = grdMainSettings.RowDefinitions[3];
				GridLength gridLength2 = rowDefinition6.Height = new GridLength(0.0);
				gridLength5 = (rowDefinition4.Height = (rowDefinition5.Height = gridLength2));
				RbtPreciosaSelectingColors.Content = "Для мулине (выберите):";
				TextBlock tblSchemeSourceAndCountColors2 = TblSchemeSourceAndCountColors;
				TextBlock tblSchemeReworkAndCountColors2 = TblSchemeReworkAndCountColors;
				TextBlock textBlock27 = tblCountColorsAfterManualEditing;
				string text2 = textBlock27.Text = Settings.nameOfWork + ": ";
				string text5 = tblSchemeSourceAndCountColors2.Text = (tblSchemeReworkAndCountColors2.Text = text2);
				gbxSelectNearestColors.Header = " 3. Выберите мулине нужного цвета";
				gbxReplaceColorInHandEditingScheme.Header = "*Цвет и число стежков";
				RbtSimpleSelectingColors.ToolTip = "Схема будет создана не под конкретного производителя мулине, а просто под обычные цвета (нужные мулине Вам придётся подбирать самостоятельно)";
				RbtPreciosaSelectingColors.ToolTip = "Схема будет создана под выбранные Вами ниже позиции: производители мулине/серии мулине/конкретное мулине";
				trvCatalog.ToolTip = "Вы можете выбрать производителей мулине, серии мулине или конкретные номера мулине, из которых будут подбираться цвета в схему (например, для ситуации, когда Вы точно знаете, что не сможете приобрести какую-либо серию мулине, Вы можете исключить её из подбора)";
				btnShowSettings.ToolTip = "Выбор размера канвы, числа сложений нити, настройки печати и сохранения схем";
				tblDescriptionOfColorEditingMode.Text = tblDescriptionOfColorEditingMode.Text.Replace("РЕЗУЛЬТАТУ", "ВЫШИВКЕ");
				tblDescriptionOfHidingSewingMode.Text = tblDescriptionOfHidingSewingMode.Text.Replace("РЕЗУЛЬТАТУ", "ВЫШИВКЕ");
				System.Windows.Controls.ContextMenu contextMenu2 = new System.Windows.Controls.ContextMenu();
				TextBlock textBlock28 = new TextBlock
				{
					Padding = new Thickness(3.0),
					Text = "Выбрать все"
				};
				textBlock28.MouseLeftButtonUp += tblSelectAll_MouseLeftButtonUp;
				TextBlock textBlock29 = new TextBlock
				{
					Padding = new Thickness(3.0),
					Text = "Убрать все"
				};
				textBlock29.MouseLeftButtonUp += tblDeselectAll_MouseLeftButtonUp;
				contextMenu2.Items.Add(textBlock28);
				contextMenu2.Items.Add(textBlock29);
				trvCatalog.ContextMenu = contextMenu2;
				tblEditorOfMyPalette.Text = tblEditorOfMyPalette.Text.Replace("бисера", "мулине");
				tblWarningAboutEmbroideryView.Text = "Ниже представлен лишь вид вышивки. Чтобы получить саму схему вышивки и карту цветов - сохраните созданную вышивку! Кнопка 'Сохранить' находится слева-внизу этого окна.";
				tblCountOfCountours.Visibility = Visibility.Collapsed;
				grdReplaceColorOnHandEditingTab.RowDefinitions[1].Height = new GridLength(0.0);
				System.Windows.Controls.Button button9 = btnOrderConsublesInCreateTab;
				System.Windows.Controls.Button button10 = btnOrderConsublesInReworkTab;
				System.Windows.Controls.Button button11 = btnOrderConsublesInHandEditingTab;
				Visibility visibility2 = button11.Visibility = Visibility.Collapsed;
				Visibility visibility4 = button10.Visibility = visibility2;
				button9.Visibility = visibility4;
				tblSizeInBlocks.Text = "в стежках:";
				TextBlock textBlock30 = tblBrushSizeInPxForRaskraska;
				System.Windows.Controls.TextBox textBox2 = tbxBrushSizeInPxForRaskraska;
				visibility4 = (textBox2.Visibility = Visibility.Collapsed);
				textBlock30.Visibility = visibility4;
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
			{
				LoadCatalogueOfManufacturers();
				base.Title = "Петелька - создавайте схемы вязания из Ваших фотографий и картинок!";
				RowDefinition rowDefinition7 = grdMainSettings.RowDefinitions[1];
				RowDefinition rowDefinition8 = grdMainSettings.RowDefinitions[2];
				RowDefinition rowDefinition9 = grdMainSettings.RowDefinitions[3];
				GridLength gridLength2 = rowDefinition9.Height = new GridLength(0.0);
				gridLength5 = (rowDefinition7.Height = (rowDefinition8.Height = gridLength2));
				RbtPreciosaSelectingColors.Content = "Для пряжи (выберите):";
				grdReworkingSettings.RowDefinitions[2].Height = new GridLength(0.0);
				grdReworkingSettings.RowDefinitions[1].Height = new GridLength(0.0);
				grdSchemeRework.RowDefinitions[1].Height = new GridLength(293.0);
				RbtCorrectionModeColor.Visibility = Visibility.Collapsed;
				tblDescriptionOfColorEditingMode.Margin = new Thickness(15.0, 13.0, 0.0, 0.0);
				TextBlock tblSchemeSourceAndCountColors3 = TblSchemeSourceAndCountColors;
				TextBlock tblSchemeReworkAndCountColors3 = TblSchemeReworkAndCountColors;
				TextBlock textBlock31 = tblCountColorsAfterManualEditing;
				string text2 = textBlock31.Text = Settings.nameOfWork + ": ";
				string text5 = tblSchemeSourceAndCountColors3.Text = (tblSchemeReworkAndCountColors3.Text = text2);
				gbxSelectNearestColors.Header = " 3. Выберите пряжу нужного цвета";
				gbxReplaceColorInHandEditingScheme.Header = "*Цвет и число петель";
				RbtSimpleSelectingColors.ToolTip = "Схема будет создана не под конкретного производителя пряжи, а просто под обычные цвета (нужную пряжу Вам придётся подбирать самостоятельно)";
				RbtPreciosaSelectingColors.ToolTip = "Схема будет создана под выбранные Вами ниже позиции: производители пряжи/серии пряжи/конкретная пряжа";
				trvCatalog.ToolTip = "Вы можете выбрать производителей пряжи, серии пряжи или конкретные номера пряжи, из которых будут подбираться цвета в схему (например, для ситуации, когда Вы точно знаете, что не сможете приобрести какую-либо серию пряжи, Вы можете исключить её из подбора)";
				btnShowSettings.ToolTip = "Выбор плотности вязания, настройки печати и сохранения схем";
				tblDescriptionOfColorEditingMode.Text = tblDescriptionOfColorEditingMode.Text.Replace("РЕЗУЛЬТАТУ", "ВЯЗАНИЮ");
				tblDescriptionOfHidingSewingMode.Text = tblDescriptionOfHidingSewingMode.Text.Replace("РЕЗУЛЬТАТУ", "ВЯЗАНИЮ");
				tblEditorOfMyPalette.Text = tblEditorOfMyPalette.Text.Replace("бисера", "пряжи");
				tblWarningAboutEmbroideryView.Text = "Ниже представлен лишь вид вязания. Чтобы получить саму схему вязания и карту цветов - сохраните созданное вязание! Кнопка 'Сохранить' находится слева-посередине этого окна.";
				tblCountOfCountours.Visibility = Visibility.Collapsed;
				slrBrushSize.Width = 168.0;
				this.textBlock7.Margin = new Thickness(0.0, -2.0, 4.0, 0.0);
				cmbTypeOfStitchForDrawing.Visibility = Visibility.Collapsed;
				System.Windows.Controls.Button button12 = btnUndoBackstitchInHandEditing;
				System.Windows.Controls.Button button13 = btnUndoKnotInHandEditing;
				Visibility visibility4 = button13.Visibility = Visibility.Collapsed;
				button12.Visibility = visibility4;
				btnUndoManualEditingScheme.Content = "Отмена (Ctrl + Z)";
				grdReplaceColorOnHandEditingTab.RowDefinitions[1].Height = new GridLength(0.0);
				System.Windows.Controls.Button button14 = btnOrderConsublesInCreateTab;
				System.Windows.Controls.Button button15 = btnOrderConsublesInReworkTab;
				System.Windows.Controls.Button button16 = btnOrderConsublesInHandEditingTab;
				Visibility visibility2 = button16.Visibility = Visibility.Collapsed;
				visibility4 = (button15.Visibility = visibility2);
				button14.Visibility = visibility4;
				tblSizeInBlocks.Text = "в петлях:";
				TextBlock textBlock32 = tblBrushSizeInPxForRaskraska;
				System.Windows.Controls.TextBox textBox3 = tbxBrushSizeInPxForRaskraska;
				visibility4 = (textBox3.Visibility = Visibility.Collapsed);
				textBlock32.Visibility = visibility4;
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				LoadCatalogueOfManufacturers();
                base.Title = "Paint"; // "'Раскраска' - создавайте раскраски и картины по номерам из Ваших фотографий и картинок!";
				TabItem tabReworkScheme = TabReworkScheme;
				System.Windows.Controls.Image image = image18;
				TextBlock textBlock33 = tblProVersionUsing;
				Visibility visibility2 = textBlock33.Visibility = Visibility.Collapsed;
				Visibility visibility4 = image.Visibility = visibility2;
				tabReworkScheme.Visibility = visibility4;
				btnBuyHandEditingTab.Content = "Купить КОММЕРЧЕСКУЮ версию, чтобы активировать эту вкладку";
				TextBlock textBlock34 = tblBaseVersionUsing;
				TextBlock textBlock35 = tblMasterVersionUsing;
				object obj3 = textBlock34.ToolTip = (textBlock35.ToolTip = "");
				tblMasterVersionUsing.Text = "Коммерческая версия";
				if (v == 0 || v == 3)
				{
					tblBaseVersionUsing.Text = "Домашняя/Профессиональная версия";
					if (v == 3)
					{
						tblMasterVersionUsing.Text = "КОММЕРЧЕСКАЯ версия";
					}
				}
				else if (v == 1)
				{
					tblBaseVersionUsing.Text = "ДОМАШНЯЯ/Профессиональная версия";
					tblVersionHandEditingTab.Text = "ДОМАШНЯЯ версия программы";
				}
				else if (v == 2)
				{
					tblBaseVersionUsing.Text = "Домашняя/ПРОФЕССИОНАЛЬНАЯ версия";
					tblVersionHandEditingTab.Text = "ПРОФЕССИОНАЛЬНАЯ версия программы";
				}
				image17.Margin = new Thickness(243.0, 17.0, 0.0, 0.0);
				RowDefinition rowDefinition10 = grdMainSettings.RowDefinitions[1];
				RowDefinition rowDefinition11 = grdMainSettings.RowDefinitions[2];
				gridLength5 = (rowDefinition10.Height = (rowDefinition11.Height = new GridLength(0.0)));
				RbtPreciosaSelectingColors.Content = "Для красок (выберите):";
				ChangeSchemeOnColoringInContentAndToolTip(CreateSchemeButton);
				ChangeSchemeOnColoringInContentAndToolTip(tblLoadFromImage);
				ChangeSchemeOnColoringInContentAndToolTip(tblLoadFromSavedScheme);
				ChangeSchemeOnColoringInContentAndToolTip(tbxCountColors);
				ChangeSchemeOnColoringInContentAndToolTip(tblSizeScheme);
				ChangeSchemeOnColoringInContentAndToolTip(tbxSchemeWidthInSm);
				ChangeSchemeOnColoringInContentAndToolTip(tbxSchemeHeightInSm);
				ChangeSchemeOnColoringInContentAndToolTip(SaveSchemeButton);
				ChangeSchemeOnColoringInContentAndToolTip(PrintSchemeButton);
				ChangeSchemeOnColoringInContentAndToolTip(gbxMainSettingScheme_header);
				ChangeSchemeOnColoringInContentAndToolTip(this.textBlock16);
				ChangeSchemeOnColoringInContentAndToolTip(tblSchemeNotCreatedOnFirstTab);
				ChangeSchemeOnColoringInContentAndToolTip(TblSchemeSourceAndCountColors);
				ChangeSchemeOnColoringInContentAndToolTip(btnSaveSchemeAfterDrawing);
				ChangeSchemeOnColoringInContentAndToolTip(btnPrintSchemeAfterDrawing);
				ChangeSchemeOnColoringInContentAndToolTip(rbtBrush);
				ChangeSchemeOnColoringInContentAndToolTip(rbtFill);
				ChangeSchemeOnColoringInContentAndToolTip(tblIsSchemeToManualEditingEnabled);
				ChangeSchemeOnColoringInContentAndToolTip(this.textBlock21);
				ChangeSchemeOnColoringInContentAndToolTip(grdMoveToHandEditing);
				TextBlock tblSchemeSourceAndCountColors4 = TblSchemeSourceAndCountColors;
				TextBlock tblSchemeReworkAndCountColors4 = TblSchemeReworkAndCountColors;
				TextBlock textBlock36 = tblCountColorsAfterManualEditing;
				string text2 = textBlock36.Text = Settings.nameOfWork + ": ";
				string text5 = tblSchemeSourceAndCountColors4.Text = (tblSchemeReworkAndCountColors4.Text = text2);
				gbxSelectNearestColors.Header = " 3. Выберите краску похожего цвета";
				tblDescriptionOfManualEditing.Text = "Теперь выбранной в пункте 3 краской либо рисуйте на схеме (если в пункте 4 выбрали 'Кисть'), либо заливайте области (если в пункте 4 выбрали 'Заливку').";
				gbxReplaceColorInHandEditingScheme.Header = "*Цвет и % краски";
				tblDescriptionOfColorEditingMode.Text = tblDescriptionOfColorEditingMode.Text.Replace("РЕЗУЛЬТАТУ", "РАСКРАСКЕ");
				tblDescriptionOfHidingSewingMode.Text = tblDescriptionOfHidingSewingMode.Text.Replace("РЕЗУЛЬТАТУ", "РАСКРАСКЕ");
				slrBrushSize.Width = 168.0;
				this.textBlock7.Margin = new Thickness(0.0, -2.0, 4.0, 0.0);
				cmbTypeOfStitchForDrawing.Visibility = Visibility.Collapsed;
				RbtSimpleSelectingColors.ToolTip = "Схема будет создана не под конкретного производителя красок, а просто под обычные цвета (нужные краски Вам придётся подбирать самостоятельно)";
				RbtPreciosaSelectingColors.ToolTip = "Схема будет создана под выбранные Вами ниже позиции: производители красок, серии и номера красок";
				trvCatalog.ToolTip = "Вы можете выбрать производителей красок, серии красок или конкретные номера красок, из которых будут подбираться цвета в раскраску (например, для ситуации, когда Вы точно знаете, что не сможете приобрести какую-либо серию красок, Вы можете исключить её из подбора)";
				scvSecondTab.Visibility = Visibility.Collapsed;
				System.Windows.Controls.ContextMenu contextMenu3 = new System.Windows.Controls.ContextMenu();
				TextBlock textBlock37 = new TextBlock
				{
					Padding = new Thickness(3.0),
					Text = "Выбрать все"
				};
				textBlock37.MouseLeftButtonUp += tblSelectAll_MouseLeftButtonUp;
				TextBlock textBlock38 = new TextBlock
				{
					Padding = new Thickness(3.0),
					Text = "Убрать все"
				};
				textBlock38.MouseLeftButtonUp += tblDeselectAll_MouseLeftButtonUp;
				contextMenu3.Items.Add(textBlock37);
				contextMenu3.Items.Add(textBlock38);
				trvCatalog.ContextMenu = contextMenu3;
				btnShowSettings.Content = "Важные настройки";
				tblEditorOfMyPalette.Text = "Редактор своих цветов красок";
				tblEditorOfMyPalette.IsEnabled = true;
				tblWarningAboutEmbroideryView.Text = "Ниже представлен лишь вид картины. Чтобы получить саму схему раскраски и карту цветов - сохраните созданную картину! Кнопка 'Сохранить' находится слева-внизу этого окна.";
				System.Windows.Controls.Button button17 = btnUndoBackstitchInHandEditing;
				System.Windows.Controls.Button button18 = btnUndoKnotInHandEditing;
				visibility4 = (button18.Visibility = Visibility.Collapsed);
				button17.Visibility = visibility4;
				btnUndoManualEditingScheme.Content = "Отмена (Ctrl + Z)";
				tblMoveToHandEditingFromRework.Visibility = Visibility.Collapsed;
				TabCreateScheme.Header = "Шаг 1. Создание картины по номерам";
				tbiHandEditing.Header = "Шаг 2. Редактирование картины";
				tblMoveToHandEditingFromCreate.Text = "Из вкладки 'Шаг 1. Создание картины'";
				slrBrushSize.Minimum = 2.0;
				grdMoveToHandEditing.Height -= 23.0;
				RowDefinition rowDefinition12 = grdThirdTab.RowDefinitions[1];
				gridLength5 = grdThirdTab.RowDefinitions[1].Height;
				rowDefinition12.Height = new GridLength(gridLength5.Value - 23.0);
				gbxSelectNearestColors.Height += 23.0;
				System.Windows.Controls.Button button19 = btnOrderConsublesInCreateTab;
				System.Windows.Controls.Button button20 = btnOrderConsublesInReworkTab;
				System.Windows.Controls.Button button21 = btnOrderConsublesInHandEditingTab;
				visibility2 = (button21.Visibility = Visibility.Collapsed);
				visibility4 = (button20.Visibility = visibility2);
				button19.Visibility = visibility4;
				tblEditorOfMyPalette.Visibility = Visibility.Visible;
				trvCatalog.Margin = new Thickness(0.0, 47.888, 0.378, 14.867);
				tblSizeInSm.FontWeight = FontWeights.Bold;
				grdMainSettings.RowDefinitions[0].Height = new GridLength(53.0);
				TextBlock textBlock39 = tblSizeInBlocks;
				System.Windows.Controls.TextBox textBox4 = tbxSchemeHeightInBlocks;
				System.Windows.Controls.TextBox textBox5 = tbxSchemeWidthInBlocks;
				TextBlock textBlockX = TextBlockX;
				Visibility visibility25 = textBlockX.Visibility = Visibility.Collapsed;
				visibility2 = (textBox5.Visibility = visibility25);
				visibility4 = (textBox4.Visibility = visibility2);
				textBlock39.Visibility = visibility4;
				TextBlock textBlock40 = tblBrushSizeInPxForRaskraska;
				System.Windows.Controls.TextBox textBox6 = tbxBrushSizeInPxForRaskraska;
				visibility4 = (textBox6.Visibility = Visibility.Visible);
				textBlock40.Visibility = visibility4;
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
			{
				LoadCatalogueOfManufacturers();
				base.Title = "Алмазная мозаика - создавайте схемы из Ваших фотографий и картинок!";
				RowDefinition rowDefinition13 = grdMainSettings.RowDefinitions[2];
				RowDefinition rowDefinition14 = grdMainSettings.RowDefinitions[3];
				gridLength5 = (rowDefinition13.Height = (rowDefinition14.Height = new GridLength(0.0)));
				RbtPreciosaSelectingColors.Content = "Для страз (выберите):";
				grdReworkingSettings.RowDefinitions[2].Height = new GridLength(0.0);
				RowDefinition rowDefinition15 = grdSchemeRework.RowDefinitions[1];
				gridLength5 = grdSchemeRework.RowDefinitions[1].Height;
				rowDefinition15.Height = new GridLength(gridLength5.Value - 48.0);
				TextBlock tblSchemeSourceAndCountColors5 = TblSchemeSourceAndCountColors;
				TextBlock tblSchemeReworkAndCountColors5 = TblSchemeReworkAndCountColors;
				TextBlock textBlock41 = tblCountColorsAfterManualEditing;
				string text2 = textBlock41.Text = Settings.nameOfWork + ": ";
				string text5 = tblSchemeSourceAndCountColors5.Text = (tblSchemeReworkAndCountColors5.Text = text2);
				gbxSelectNearestColors.Header = " 3. Выберите стразы похожего цвета";
				gbxReplaceColorInHandEditingScheme.Header = "*Цвет и число страз";
				RbtSimpleSelectingColors.ToolTip = "Схема будет создана не под конкретного производителя страз, а просто под обычные цвета (нужные стразы Вам придётся подбирать самостоятельно)";
				RbtPreciosaSelectingColors.ToolTip = "Схема будет создана под выбранные Вами ниже позиции: производители страз/серии страз/конкретные стразы";
				trvCatalog.ToolTip = "Вы можете выбрать производителей страз, серии страз или конкретные номера страз, из которых будут подбираться цвета в схему (например, для ситуации, когда Вы точно знаете, что не сможете приобрести какую-либо серию страз, Вы можете исключить её из подбора)";
				btnShowSettings.ToolTip = "Выбор размера страз, настройки печати и сохранения схем";
				tblDescriptionOfColorEditingMode.Text = tblDescriptionOfColorEditingMode.Text.Replace("РЕЗУЛЬТАТУ", "ВЫШИВКЕ");
				tblDescriptionOfHidingSewingMode.Text = tblDescriptionOfHidingSewingMode.Text.Replace("РЕЗУЛЬТАТУ", "ВЫШИВКЕ");
				System.Windows.Controls.ContextMenu contextMenu4 = new System.Windows.Controls.ContextMenu();
				TextBlock textBlock42 = new TextBlock
				{
					Padding = new Thickness(3.0),
					Text = "Выбрать все"
				};
				textBlock42.MouseLeftButtonUp += tblSelectAll_MouseLeftButtonUp;
				TextBlock textBlock43 = new TextBlock
				{
					Padding = new Thickness(3.0),
					Text = "Убрать все"
				};
				textBlock43.MouseLeftButtonUp += tblDeselectAll_MouseLeftButtonUp;
				contextMenu4.Items.Add(textBlock42);
				contextMenu4.Items.Add(textBlock43);
				trvCatalog.ContextMenu = contextMenu4;
				tblEditorOfMyPalette.Text = tblEditorOfMyPalette.Text.Replace("бисера", "страз");
				tblWarningAboutEmbroideryView.Text = "Ниже представлен лишь вид вышивки. Чтобы получить саму схему вышивки и карту цветов - сохраните созданную вышивку! Кнопка 'Сохранить' находится слева-внизу этого окна.";
				tblCountOfCountours.Visibility = Visibility.Collapsed;
				slrBrushSize.Width = 168.0;
				this.textBlock7.Margin = new Thickness(0.0, -2.0, 4.0, 0.0);
				cmbTypeOfStitchForDrawing.Visibility = Visibility.Collapsed;
				System.Windows.Controls.Button button22 = btnUndoBackstitchInHandEditing;
				System.Windows.Controls.Button button23 = btnUndoKnotInHandEditing;
				Visibility visibility4 = button23.Visibility = Visibility.Collapsed;
				button22.Visibility = visibility4;
				btnUndoManualEditingScheme.Content = "Отмена (Ctrl + Z)";
				grdReplaceColorOnHandEditingTab.RowDefinitions[1].Height = new GridLength(0.0);
				System.Windows.Controls.Button button24 = btnOrderConsublesInCreateTab;
				System.Windows.Controls.Button button25 = btnOrderConsublesInReworkTab;
				System.Windows.Controls.Button button26 = btnOrderConsublesInHandEditingTab;
				Visibility visibility2 = button26.Visibility = Visibility.Visible;
				visibility4 = (button25.Visibility = visibility2);
				button24.Visibility = visibility4;
				tblSizeInBlocks.Text = "в стразах:";
				TextBlock textBlock44 = tblBrushSizeInPxForRaskraska;
				System.Windows.Controls.TextBox textBox7 = tbxBrushSizeInPxForRaskraska;
				visibility4 = (textBox7.Visibility = Visibility.Collapsed);
				textBlock44.Visibility = visibility4;
				if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
				{
					tblEditorOfMyPalette.Visibility = Visibility.Visible;
					trvCatalog.Margin = new Thickness(0.0, 47.888, 0.378, 14.867);
				}
			}
			RowDefinition rowDefinition16 = grdFirstTab.RowDefinitions[1];
			gridLength5 = grdFirstTab.RowDefinitions[1].Height;
			rowDefinition16.MaxHeight = gridLength5.Value;
			RowDefinition rowDefinition17 = grdFirstTab.RowDefinitions[2];
			gridLength5 = grdFirstTab.RowDefinitions[2].Height;
			rowDefinition17.MaxHeight = gridLength5.Value;
			RowDefinition rowDefinition18 = grdSchemeRework.RowDefinitions[1];
			gridLength5 = grdSchemeRework.RowDefinitions[1].Height;
			rowDefinition18.MaxHeight = gridLength5.Value;
			RowDefinition rowDefinition19 = grdThirdTab.RowDefinitions[1];
			gridLength5 = grdThirdTab.RowDefinitions[1].Height;
			rowDefinition19.MaxHeight = gridLength5.Value;
			DateTime? nullable = RegistryWork.CheckDateTimeEnd();
			if (nullable.HasValue)
			{
				int num6 = Convert.ToInt32((nullable.Value - DateTime.Now).TotalDays);
				if (num6 > 90 || num6 < 0)
				{
					TextBlock textBlock45 = tblProlongForOneYear;
					TextBlock textBlock46 = tblNumberOfDaysToEndOfLicense;
					Visibility visibility4 = textBlock46.Visibility = Visibility.Collapsed;
					textBlock45.Visibility = visibility4;
				}
				else
				{
					TextBlock textBlock47 = tblProlongForOneYear;
					TextBlock textBlock48 = tblNumberOfDaysToEndOfLicense;
					Visibility visibility4 = textBlock48.Visibility = Visibility.Visible;
					textBlock47.Visibility = visibility4;
					tblNumberOfDaysToEndOfLicense.Text = "До окончания Вашей лицензии: " + num6.ToString() + " дн.";
					DateTime? lastDateReminderAboutApportunityOfDiscountOnRenewal = RegistryWork.GetLastDateReminderAboutApportunityOfDiscountOnRenewal();
					if (!lastDateReminderAboutApportunityOfDiscountOnRenewal.HasValue || lastDateReminderAboutApportunityOfDiscountOnRenewal.Value.AddHours(35.0) < DateTime.Now)
					{
						WndInfo wndInfo = new WndInfo("Ваша лицензия на программу истечёт через " + num6.ToString() + " дн.\r\n                        \r\nСейчас Вы можете продлить программу со скидкой!\r\n\r\nОбратите внимание, что, продляя работу программы ДО истечения лицензии, Вы ничего не теряете, т.к. продление лицензии будет активировано именно со дня окончания текущей лицензии, а не со дня оплаты!", Site.GetUrlToBuy(), "Продлить программу по скидке!");
						wndInfo.ShowDialog();
						RegistryWork.WriteDateReminderAboutApportunityOfDiscountOnRenewal();
					}
				}
			}
			else
			{
				TextBlock textBlock49 = tblProlongForOneYear;
				TextBlock textBlock50 = tblNumberOfDaysToEndOfLicense;
				Visibility visibility4 = textBlock50.Visibility = Visibility.Collapsed;
				textBlock49.Visibility = visibility4;
			}
			Assembly executingAssembly = Assembly.GetExecutingAssembly();
			string str = executingAssembly.GetName().Version.ToString();
			if (Settings.isItVersionWithoutContactInfo)
			{
				base.Title = "Программа для создания картин по номерам";
				TextBlock textBlock51 = tblBaseVersionUsing;
				TextBlock textBlock52 = tblProVersionUsing;
				TextBlock textBlock53 = tblMasterVersionUsing;
				TextBlock textBlock54 = tblProlongForOneYear;
				TextBlock textBlock55 = tblLicenseDescription;
				TextBlock aboutTextBlock = AboutTextBlock;
				TextBlock helpTextBlock = HelpTextBlock;
				Visibility visibility37 = helpTextBlock.Visibility = Visibility.Collapsed;
				Visibility visibility39 = aboutTextBlock.Visibility = visibility37;
				Visibility visibility41 = textBlock55.Visibility = visibility39;
				Visibility visibility25 = textBlock54.Visibility = visibility41;
				Visibility visibility2 = textBlock53.Visibility = visibility25;
				Visibility visibility4 = textBlock52.Visibility = visibility2;
				textBlock51.Visibility = visibility4;
				System.Windows.Controls.Button button27 = btnOrderConsublesInCreateTab;
				System.Windows.Controls.Button button28 = btnOrderConsublesInReworkTab;
				System.Windows.Controls.Button button29 = btnOrderConsublesInHandEditingTab;
				visibility2 = (button29.Visibility = Visibility.Collapsed);
				visibility4 = (button28.Visibility = visibility2);
				button27.Visibility = visibility4;
			}
			//base.Title = Local.Get(base.Title) + ": " + str;
			WaitingWindow.wndOwner = this;
		}

		private void ChangeSchemeOnColoringInContentAndToolTip(FrameworkElement control)
		{
			if (control is ContentControl && (control as ContentControl).Content is string)
			{
				(control as ContentControl).Content = ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt((string)(control as ContentControl).Content);
			}
			if (control is TextBlock)
			{
				(control as TextBlock).Text = ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt((control as TextBlock).Text);
			}
			if (control is System.Windows.Controls.GroupBox)
			{
				(control as System.Windows.Controls.GroupBox).Header = ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt((string)(control as System.Windows.Controls.GroupBox).Header);
			}
			if (control.ToolTip != null && control.ToolTip is string)
			{
				control.ToolTip = ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt((string)control.ToolTip);
			}
		}

		private string ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt(string strSrc)
		{
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				return Local.Get(strSrc.Replace("схема", "картина").Replace("схемы", "картины").Replace("схему", "картину")
					.Replace("схемой", "картиной")
					.Replace("схеме", "картине")
					.Replace("Схема", "Картина")
					.Replace("СХЕМУ", "КАРТИНУ")
					.Replace("Вышивка", "Картина"));
			}
			return Local.Get(strSrc);
		}

		private void tblDeselectAll_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			System.Windows.Input.Cursor cursor = base.Cursor;
			base.Cursor = System.Windows.Input.Cursors.Wait;
			Thread.Yield();
			foreach (object item in (IEnumerable)trvCatalog.Items)
			{
				Node node = (Node)item;
				foreach (Node child in node.Children)
				{
					foreach (Node child2 in child.Children)
					{
						child2.IsChecked = false;
					}
					child.IsChecked = false;
				}
				node.IsChecked = false;
			}
			base.Cursor = cursor;
		}

		private void tblSelectAll_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			foreach (object item in (IEnumerable)trvCatalog.Items)
			{
				Node node = (Node)item;
				CheckBoxId.checkBoxId = node.Id;
				node.IsChecked = true;
			}
		}

		private void tblSelectIsInStockBeads_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
			StreamReader streamReader = File.OpenText(baseDirectory + "PreciosaInSales.txt");
			List<string> list = new List<string>();
			while (!streamReader.EndOfStream)
			{
				string text = streamReader.ReadLine();
				list.Add(text.Replace(" ", ""));
			}
			streamReader.Close();
			foreach (object item in (IEnumerable)trvCatalog.Items)
			{
				Node node = (Node)item;
				bool value = false;
				foreach (Node child in node.Children)
				{
					bool value2 = false;
					foreach (Node child2 in child.Children)
					{
						if (!list.Contains(child2.Text))
						{
							child2.IsChecked = false;
						}
						else
						{
							child2.IsChecked = true;
							value2 = (value = true);
						}
					}
					child.IsChecked = value2;
				}
				node.IsChecked = value;
			}
		}

		private void LoadCatalogueOfManufacturers()
		{
			if (!scheme.LoadBeadsCatalog())
			{
				WndInfo wndInfo = new WndInfo(Local.Get("Произошла ошибка при загрузке ВАШИХ цветов ") + Settings.materials + Local.Get(". Возможно, что Вы пробовали вручную отредактировать этот файл и допустили ошибку форматирования.\r\n                \r\nЕсли Вам надо сохранить всё, что Вы туда занесли - то перешлите файл, расположенный по адресу 'C:\\Пользователи\\ИМЯ_ПОЛЬЗОВАТЕЛЯ_WINDOWS\\AppData\\Local\\") + Settings.FileDirectory + "\\MyPalette.txt' " + Local.Get("нам на почту ") + Settings.siteAndEmailSettings.supportEmail + Local.Get(" и поясните, что произошла эта ошибка (обратите внимание, что часть папок по этому пути являются скрытыми, и надо в настройках системы через Панель управления сделать, чтобы скрытые папки показывались). \r\n                \r\nЕсли же Вам не важно сохранение этого файла в его текущем виде - просто удалите его и запустите программу заново - всё должно запуститься в обычном режиме, без ошибок."));
				wndInfo.Show();
			}
			List<Beads> source = LoadSelectedBeadsOnLastClosingProgram_FromFile();
			Nodes = new ObservableCollection<Node>();
			Nodes.Clear();
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			Node node;
			for (int i = 0; i < scheme.beadsManufacturer.Count(); Nodes.Add(node), i++)
			{
				node = new Node
				{
					Text = scheme.beadsManufacturer[i].name,
					Tag = scheme.beadsManufacturer[i],
					Width = 0
				};
				if (i == scheme.beadsManufacturer.Count() - 2 || scheme.beadsManufacturer.Count() == 1)
				{
					node.IsExpanded = true;
				}
				else
				{
					node.IsExpanded = false;
				}
				num2 = (num3 = 0);
				for (int j = 0; j < scheme.beadsManufacturer[i].beadsSeries.Count(); j++)
				{
					Node node2 = new Node
					{
						Text = scheme.beadsManufacturer[i].beadsSeries[j].name,
						Tag = scheme.beadsManufacturer[i].beadsSeries[j],
						Width = 0
					};
					if (node.IsExpanded && j == 0)
					{
						node2.IsExpanded = true;
					}
					num = 0;
					for (int k = 0; k < scheme.beadsManufacturer[i].beadsSeries[j].beads.Count(); k++)
					{
						Beads _b = scheme.beadsManufacturer[i].beadsSeries[j].beads[k];
						Node node3 = new Node
						{
							Text = _b.name,
							Tag = _b,
							Brush = _b.brushForWPF,
							Width = 22
						};
						if (source.Count() > 0)
						{
							if ((Settings.hobbyProgramm != HobbyProgramm.Raskraska) ? source.Any(delegate(Beads bd)
							{
								if (bd.name == _b.name)
								{
									System.Drawing.Color clr2 = bd.clr;
									byte r2 = clr2.R;
									clr2 = _b.clr;
									if (r2 == clr2.R)
									{
										clr2 = bd.clr;
										byte g2 = clr2.G;
										clr2 = _b.clr;
										if (g2 == clr2.G)
										{
											clr2 = bd.clr;
											byte b2 = clr2.B;
											clr2 = _b.clr;
											return b2 == clr2.B;
										}
									}
								}
								return false;
							}) : source.Any(delegate(Beads bd)
							{
								if (bd.parentSeries.parentManufacturer.name == _b.parentSeries.parentManufacturer.name && bd.parentSeries.name == _b.parentSeries.name && bd.name == _b.name)
								{
									System.Drawing.Color clr = bd.clr;
									byte r = clr.R;
									clr = _b.clr;
									if (r == clr.R)
									{
										clr = bd.clr;
										byte g = clr.G;
										clr = _b.clr;
										if (g == clr.G)
										{
											clr = bd.clr;
											byte b = clr.B;
											clr = _b.clr;
											return b == clr.B;
										}
									}
								}
								return false;
							}))
							{
								node3.IsChecked = true;
								num++;
							}
							else
							{
								node3.IsChecked = false;
							}
						}
						node3.Parent.Add(node2);
						node2.Children.Add(node3);
					}
					if (source.Count() > 0)
					{
						if (num == 0)
						{
							node2.IsChecked = false;
						}
						else if (num < scheme.beadsManufacturer[i].beadsSeries[j].beads.Count())
						{
							node2.IsChecked = null;
							num3++;
						}
						else
						{
							node2.IsChecked = true;
							num2++;
						}
					}
					node2.Parent.Add(node);
					node.Children.Add(node2);
				}
				if (source.Count() > 0)
				{
					if (num3 == 0 && num2 == 0)
					{
						node.IsChecked = false;
					}
					else
					{
						if (num2 > 0 && num2 < scheme.beadsManufacturer[i].beadsSeries.Count())
						{
							goto IL_03d8;
						}
						if (num3 > 0)
						{
							goto IL_03d8;
						}
						node.IsChecked = true;
					}
				}
				continue;
				IL_03d8:
				node.IsChecked = null;
			}
			trvCatalog.ItemsSource = Nodes;
		}

		private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			System.Windows.Controls.CheckBox checkBox = (System.Windows.Controls.CheckBox)sender;
			CheckBoxId.checkBoxId = checkBox.Uid;
		}

		private void RbtCorrectionModeColor_Checked(object sender, RoutedEventArgs e)
		{
			TblSewingSourceInfo.Visibility = Visibility.Hidden;
			TblSewingReworkInfo.Visibility = Visibility.Hidden;
			TblAddColorInfo.Visibility = Visibility.Visible;
			TblDelColorInfo.Visibility = Visibility.Visible;
			ScaleSewingBrush.IsEnabled = false;
			for (int i = 0; i < GrdSchemeRework.Children.Count; i++)
			{
				Ellipse ellipse = GrdSchemeRework.Children[i] as Ellipse;
				if (ellipse != null && ellipse.Equals(elpBrushForMakingInvisible))
				{
					GrdSchemeRework.Children.Remove(ellipse);
					break;
				}
			}
			int num = 0;
			Ellipse ellipse2;
			while (true)
			{
				if (num < GrdSchemeRework.Children.Count)
				{
					ellipse2 = (GrdSchemeRework.Children[num] as Ellipse);
					if (ellipse2 != null && ellipse2.Equals(elpBrushForMakingInvisibleFake))
					{
						break;
					}
					num++;
					continue;
				}
				return;
			}
			GrdSchemeRework.Children.Remove(ellipse2);
		}

		private void RbtCorrectionModeSewing_Checked(object sender, RoutedEventArgs e)
		{
			TblSewingSourceInfo.Visibility = Visibility.Visible;
			TblSewingReworkInfo.Visibility = Visibility.Visible;
			TblAddColorInfo.Visibility = Visibility.Hidden;
			TblDelColorInfo.Visibility = Visibility.Hidden;
			ScaleSewingBrush.IsEnabled = true;
			bool flag = false;
			for (int i = 0; i < GrdSchemeRework.Children.Count; i++)
			{
				Ellipse ellipse = GrdSchemeRework.Children[i] as Ellipse;
				if (ellipse != null && ellipse.Equals(elpBrushForMakingInvisible))
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				GrdSchemeRework.Children.Add(elpBrushForMakingInvisible);
			}
			flag = false;
			for (int j = 0; j < GrdSchemeRework.Children.Count; j++)
			{
				Ellipse ellipse2 = GrdSchemeRework.Children[j] as Ellipse;
				if (ellipse2 != null && ellipse2.Equals(elpBrushForMakingInvisibleFake))
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				GrdSchemeRework.Children.Add(elpBrushForMakingInvisibleFake);
			}
		}

		private void imgReworkScheme_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (!IsInProcess && scheme.IsReworkSchemeLoaded && RbtCorrectionModeColor.IsChecked.HasValue && RbtCorrectionModeColor.IsChecked.Value)
			{
				if (prevStateBlocksOnReworking.Count() < maxEnabledCountColorEditingForSmallVersion)
				{
					System.Drawing.Color? colorOfClickOnSchemeOnReworkTab = GetColorOfClickOnSchemeOnReworkTab(e);
					if (colorOfClickOnSchemeOnReworkTab.HasValue)
					{
						IsInProcess = true;
						WaitingWindow.ChangeTextInWindow(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Пожалуйста, подождите, пока выбранный Вами на схеме цвет удалится из схемы"));
						WaitingWindow.ShowWindow();
						BackgroundWorker backgroundWorker = new BackgroundWorker();
						backgroundWorker.DoWork += bwAddOrRemoveColor_DoWork;
						backgroundWorker.RunWorkerCompleted += bwAddOrRemoveColor_RunWorkerCompleted;
						bool flag = false;
						backgroundWorker.RunWorkerAsync(new object[2]
						{
							flag,
							colorOfClickOnSchemeOnReworkTab.Value
						});
					}
				}
				else
				{
					WndInfo wndInfo = new WndInfo("В данной версии программы Вы можете добавить/удалить цвета не более " + maxEnabledCountColorEditingForSmallVersion.ToString() + " раз - просто для демонстрации возможностей программы. В продвинутой и мастер-версии это ограничение конечно снято.");
					wndInfo.ShowDialog();
				}
			}
		}

		private void ReworkSchemeSewing(System.Windows.Input.MouseEventArgs e, bool newVisibilityState)
		{
			if (!IsInProcess && scheme.IsReworkSchemeLoaded)
			{
				IsInProcess = true;
				System.Windows.Point position = e.GetPosition(imgReworkScheme);
				position.X = position.X / (imgReworkScheme.Width + 0.0) * (double)((float)scheme.Param.WidthInBead + 0f);
				position.Y = position.Y / (imgReworkScheme.Height + 0.0) * (double)((float)scheme.Param.HeightInBead + 0f);
				List<System.Drawing.Point> pointsFromCircle = GetPointsFromCircle(new PointF
				{
					X = (float)position.X,
					Y = (float)position.Y
				}, ScaleSewingBrush.Value);
				scheme.FastHideOrShowBlocksFromEmbroideryView(pointsFromCircle, newVisibilityState);
				DrawEmbroideryReworkOnImage();
				if (v > 1)
				{
					System.Windows.Controls.Button printSchemeReworkButton = PrintSchemeReworkButton;
					System.Windows.Controls.Button saveSchemeReworkButton = SaveSchemeReworkButton;
					bool isEnabled = saveSchemeReworkButton.IsEnabled = true;
					printSchemeReworkButton.IsEnabled = isEnabled;
				}
				UpdateReworkTabControls(true, true);
				IsInProcess = false;
			}
		}

		private void FastDrawBitmapOnWPFImage(Bitmap bmp, System.Windows.Controls.Image img, WriteableBitmap wbmp)
		{
			BitmapData bitmapData = bmp.LockBits(new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);
			try
			{
				wbmp.WritePixels(new Int32Rect(0, 0, bmp.Width, bmp.Height), bitmapData.Scan0, bmp.Height * bitmapData.Stride, bitmapData.Stride);
			}
			catch (Exception exc)
			{
				Site.SendExceptionInfo(exc, false);
			}
			finally
			{
				bmp.UnlockBits(bitmapData);
			}
			img.Source = wbmp;
		}

		private void SetWriteableBitmapBufferToNull_GCCollect()
		{
			wbmpHandEditing = null;
			wbmpRework = null;
			GC.Collect(GC.MaxGeneration);
		}

		private void bwAddOrRemoveColor_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				bool isAddingColor = (bool)(e.Argument as object[])[0];
				System.Drawing.Color clr = (System.Drawing.Color)(e.Argument as object[])[1];
				e.Result = scheme.AddOrRemoveColorFromScheme(isAddingColor, clr);
			}
			catch
			{
				e.Result = new List<Block>();
			}
		}

		private void bwAddOrRemoveColor_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			List<Block> list = e.Result as List<Block>;
			if (list.Count() > 0)
			{
				prevStateBlocksOnReworking.Add(new StateSchemesPart
				{
					prevBlocks = list,
					nextAction = scheme.lastActionWithReworkScheme
				});
				btnUndoReworkingScheme.IsEnabled = true;
			}
			IsInProcess = false;
			DrawEmbroideryReworkOnImage();
			UpdateReworkTabControls(true, true);
			WaitingWindow.HideWindow();
		}

		private void DrawEmbroideryReworkOnImage()
		{
			if (scheme.BmpBeadworkRework != null)
			{
				if (wbmpHandEditing != null)
				{
					wbmpHandEditing = null;
				}
				if (wbmpRework == null)
				{
					wbmpRework = new WriteableBitmap(scheme.BmpBeadworkRework.Width, scheme.BmpBeadworkRework.Height, 96.0, 96.0, PixelFormats.Bgra32, null);
				}
				FastDrawBitmapOnWPFImage(scheme.BmpBeadworkRework, imgReworkScheme, wbmpRework);
				ChangeScheme_InRework_AtScaleSize();
			}
			else
			{
				imgReworkScheme.Source = null;
			}
		}

		private void UpdateReworkTabControls(bool isSchemeLoaded, bool isSchemeChanged)
		{
			if (isSchemeLoaded)
			{
				TblSchemeReworkAndCountColors.Text = string.Format(Settings.nameOfWork + " из {0} {1}:", scheme.CountResColorsRework, AllString.GetCountColorString(scheme.CountResColorsRework));
				tblIsSchemeToReworkEnabled.Visibility = Visibility.Collapsed;
			}
			else
			{
				imgReworkScheme.Source = null;
				TblSchemeReworkAndCountColors.Text = string.Format(Settings.nameOfWork);
				tblIsSchemeToReworkEnabled.Visibility = Visibility.Visible;
			}
			if (!isSchemeChanged)
			{
				prevStateBlocksOnReworking.Clear();
				btnUndoReworkingScheme.IsEnabled = false;
			}
			GrdSchemeRework.Visibility = Visibility.Visible;
			System.Windows.Controls.RadioButton rbtCorrectionModeSewing = RbtCorrectionModeSewing;
			System.Windows.Controls.RadioButton rbtCorrectionModeColor = RbtCorrectionModeColor;
			bool isEnabled = rbtCorrectionModeColor.IsEnabled = isSchemeLoaded;
			rbtCorrectionModeSewing.IsEnabled = isEnabled;
			tblMoveToHandEditingFromRework.IsEnabled = (isSchemeChanged && v == 3);
			gbxReworkingSettings.IsEnabled = isSchemeLoaded;
			if (v > 1)
			{
				System.Windows.Controls.Button printSchemeReworkButton = PrintSchemeReworkButton;
				System.Windows.Controls.Button saveSchemeReworkButton = SaveSchemeReworkButton;
				isEnabled = (saveSchemeReworkButton.IsEnabled = isSchemeChanged);
				printSchemeReworkButton.IsEnabled = isEnabled;
				btnRemovaAloneBlocks.IsEnabled = isSchemeLoaded;
			}
			RemoveChildrenControlsOnImageRework();
		}

		private void UpdateMaximumAndCurrentValueOnSlidersOfSewingBrushAndHandEditingBrush()
		{
			Slider scaleSewingBrush = ScaleSewingBrush;
			Slider slider = slrBrushSize;
			double num3 = scaleSewingBrush.Maximum = (slider.Maximum = Math.Max((double)scheme.Param.WidthInBead / 4.0, (double)scheme.Param.HeightInBead / 4.0));
			Slider scaleSewingBrush2 = ScaleSewingBrush;
			Slider slider2 = slrBrushSize;
			num3 = (scaleSewingBrush2.Value = (slider2.Value = ScaleSewingBrush.Minimum + (ScaleSewingBrush.Maximum - ScaleSewingBrush.Minimum) / 2.0));
		}

		private void RemoveChildrenControlsOnImageRework()
		{
			for (int i = 0; i < GrdSchemeRework.Children.Count; i++)
			{
				if (!(GrdSchemeRework.Children[i] is System.Windows.Controls.Image) && (!(GrdSchemeRework.Children[i] is Ellipse) || !GrdSchemeRework.Children[i].Equals(elpBrushForMakingInvisible)))
				{
					GrdSchemeRework.Children.Remove(GrdSchemeRework.Children[i]);
				}
			}
			System.Windows.Controls.Image element = new System.Windows.Controls.Image();
			int num = 0;
			while (num < GrdImageRework.Children.Count)
			{
				if (!(GrdImageRework.Children[num] is System.Windows.Controls.Image))
				{
					num++;
					continue;
				}
				element = (System.Windows.Controls.Image)GrdImageRework.Children[num];
				break;
			}
			GrdImageRework.Children.Clear();
			GrdImageRework.Children.Add(element);
		}

		private void OnlyDigitInTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			char sep = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
			if ((((System.Windows.Controls.TextBox)sender).Text.Length != 0 || !e.Text.Contains("0")) && !e.Text.Any(delegate(char x)
			{
				if (!char.IsDigit(x))
				{
					return x != sep;
				}
				return false;
			}))
			{
				return;
			}
			e.Handled = true;
		}

		private static void RefreshTextBox(System.Windows.Controls.TextBox textBox)
		{
			string text = textBox.Text;
			textBox.Clear();
			textBox.Text = text;
		}

		private void CreateSchemeButton_Click(object sender, RoutedEventArgs e)
		{
			int num = 0;
			if (RbtPreciosaSelectingColors.IsChecked.Value)
			{
				foreach (Node node in Nodes)
				{
					BeadsManufacturer manInNode = node.Tag as BeadsManufacturer;
					bool isUsing = true;
					if (node.IsChecked.HasValue && node.IsChecked == false)
					{
						isUsing = false;
					}
					BeadsManufacturer beadsManufacturer = scheme.beadsManufacturer.First((BeadsManufacturer m) => m.name == manInNode.name);
					beadsManufacturer.isUsing = isUsing;
					bool flag = false;
					foreach (Node child in node.Children)
					{
						BeadsSeries serInNode = child.Tag as BeadsSeries;
						isUsing = true;
						if (child.IsChecked.HasValue && child.IsChecked == false)
						{
							isUsing = false;
						}
						else
						{
							flag = true;
						}
						BeadsSeries beadsSeries = beadsManufacturer.beadsSeries.First((BeadsSeries s) => s.name == serInNode.name);
						beadsSeries.isUsing = isUsing;
						bool flag2 = false;
						foreach (Node child2 in child.Children)
						{
							Beads beadsInNode = child2.Tag as Beads;
							isUsing = true;
							if (child2.IsChecked.HasValue && child2.IsChecked == false)
							{
								isUsing = false;
							}
							else
							{
								flag2 = true;
							}
							Beads beads = beadsSeries.beads.First((Beads s) => s.name == beadsInNode.name);
							beads.isUsing = isUsing;
							if (isUsing)
							{
								num++;
							}
						}
						if (!beadsSeries.isUsing & flag2)
						{
							beadsSeries.isUsing = true;
						}
					}
					if (!beadsManufacturer.isUsing & flag)
					{
						beadsManufacturer.isUsing = true;
					}
				}
			}
			if (!RbtPreciosaSelectingColors.IsChecked.Value || num > 1)
			{
				prevStateBlocksOnReworking.Clear();
				btnUndoReworkingScheme.IsEnabled = false;
				CreateScheme();
			}
			else
			{
				new WndInfo(Local.Get("Для начала работы выберите более одного цвета, либо выберите тип подбора 'Простой подбор цветов'")).ShowDialog();
			}
		}

		private void UpdateHandEditingTabOnSimpleOrManufacturerSelectingColors()
		{
			if (scheme.Param.BiserType == BiserType.Any || (Settings.hobbyProgramm == HobbyProgramm.Raskraska && scheme.Param.BiserType == BiserType.ForManufacturers && scheme.Param.IsMixingColors))
			{
				rbtColorFromName.Visibility = Visibility.Collapsed;
				grdManualEditingScheme.RowDefinitions[0].Height = new GridLength(83.0);
				System.Windows.Controls.GroupBox groupBox = gbxSelectNearestColors;
				System.Windows.Controls.Image image = imgNarrowGoToSelectManufacturerColors;
				Visibility visibility2 = image.Visibility = Visibility.Collapsed;
				groupBox.Visibility = visibility2;
				gbxModeOfManualEditing.Header = "3. Режим рисования";
				gbxDescriptionOfManualEditing.Header = "4. Что делать дальше?";
				tblDescriptionOfManualEditing.Text = ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Теперь выбранным в пункте 2 цветом либо рисуйте на схеме (если в пункте 3 выбрали 'Кисть'), либо заливайте области (если в пункте 3 выбрали 'Заливку').");
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					lbxReplaceColor.ItemTemplate = (DataTemplate)base.Resources["tmplForListForRaskraskaForAnyColors"];
				}
				else
				{
					lbxReplaceColor.ItemTemplate = (DataTemplate)base.Resources["tmplForListForAnyBeads"];
				}
				gbxReplaceColorInHandEditingScheme.Width = 141.0;
			}
			else
			{
				rbtColorFromName.Visibility = Visibility.Visible;
				grdManualEditingScheme.RowDefinitions[0].Height = new GridLength(106.0);
				System.Windows.Controls.GroupBox groupBox2 = gbxSelectNearestColors;
				System.Windows.Controls.Image image2 = imgNarrowGoToSelectManufacturerColors;
				Visibility visibility2 = image2.Visibility = Visibility.Visible;
				groupBox2.Visibility = visibility2;
				gbxModeOfManualEditing.Header = "4. Режим рисования";
				gbxDescriptionOfManualEditing.Header = "5. Что делать дальше?";
				tblDescriptionOfManualEditing.Text = ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Теперь выбранным в пункте 3 цветом либо рисуйте на схеме (если в пункте 4 выбрали 'Кисть'), либо заливайте области (если в пункте 4 выбрали 'Заливку').");
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					lbxReplaceColor.ItemTemplate = (DataTemplate)base.Resources["tmplForListForRaskraskaForManufacturersColors"];
					gbxReplaceColorInHandEditingScheme.Width = 215.0;
				}
				else
				{
					lbxReplaceColor.ItemTemplate = (DataTemplate)base.Resources["tmplForListForManufacturersBeads"];
					gbxReplaceColorInHandEditingScheme.Width = 235.0;
				}
			}
			LocalizeItAndItsChildren(grdThirdTab);
		}

		private void CreateScheme()
		{
			if (!IsInProcess)
			{
				if (!scheme.IsImageLoaded)
				{
					System.Windows.MessageBox.Show("Сперва загрузите изображение", "Внимание!");
				}
				else if (tbxSchemeWidthInSm.Text.Length == 0 || tbxSchemeHeightInSm.Text.Length == 0 || tbxCountColors.Text.Length == 0)
				{
					System.Windows.MessageBox.Show(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Укажите число цветов в схеме и размер схемы в сантиметрах"), "Внимание!");
				}
				else
				{
					float num = 0f;
					float num2 = 0f;
					int num3 = 24;
					bool flag = true;
					try
					{
						num = Convert.ToSingle(tbxSchemeWidthInSm.Text);
						num2 = Convert.ToSingle(tbxSchemeHeightInSm.Text);
						num3 = Convert.ToInt32(tbxCountColors.Text);
					}
					catch
					{
						flag = false;
						System.Windows.MessageBox.Show("Вы неверно задали размер или число цветов. В этих полях допустимы только цифры! Пожалуйста, введите эти параметры корректно и создайте схему заново.", "Внимание!");
					}
					if (flag)
					{
						int num4 = 120;
						int num5 = 120;
						if (Settings.v >= 3)
						{
							num4 = (num5 = 1000);
						}
						if (num > (float)num4 || num2 > (float)num5)
						{
							System.Windows.MessageBox.Show(string.Format(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt(Local.Get("Вы указали слишком большой размер схемы. Пожалуйста, укажите размер схемы не более, чем") + " {0} x {1}" + Local.Get(" сантиметров.")), num4, num5), "Внимание!");
						}
						else if (!Settings.isItAuthorVersion && num3 > 183)
						{
							System.Windows.MessageBox.Show(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Пожалуйста, укажите не более 183 цветов в схеме"), "Внимание!");
						}
						else
						{
							IsInProcess = true;
							string strSrc = "Пожалуйста, подождите, пока схема полностью сгенерируется. Обычно это занимает от пары секунд до 1 минуты...";
							if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
							{
								strSrc = "Пожалуйста, подождите, пока схема полностью сгенерируется. Обычно это занимает от 5 секунд до 3~10 минут...";
								if (scheme.Param.countOfPixelsInCmAtContoursMapOfColoring > 40)
								{
									strSrc = "Пожалуйста, подождите, пока схема полностью сгенерируется. При 'среднем' и 'лучшем' качестве контуров это может занять от 10 секунд до 30 минут и даже более (на слабых компьютерах).";
								}
							}
							WaitingWindow.ChangeTextInWindow(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt(strSrc));
							WaitingWindow.ShowWindow();
							if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
							{
								ShowWndSettingToAutoSaving();
							}
							scheme.Param.Type = ((RbtBlockTypeScheme.IsChecked.HasValue && RbtBlockTypeScheme.IsChecked.Value) ? SchemeType.Block : SchemeType.Normal);
							scheme.Param.BiserType = ((RbtPreciosaSelectingColors.IsChecked.HasValue && RbtPreciosaSelectingColors.IsChecked.Value) ? BiserType.ForManufacturers : BiserType.Any);
							scheme.Param.CountDesiredColors = num3;
							scheme.Param.isForSepia = false;
							scheme.Param.isSquareStraz = rbtSquareStraz.IsChecked.Value;
							if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && Settings.isItVersionWithAbilityOfTurningOffSourceImageChanging && cbxIsNotChangeSourceImageInColoring.IsChecked.Value)
							{
								scheme.Param.isNotChangeSourceImageInColoring = true;
							}
							else
							{
								scheme.Param.isNotChangeSourceImageInColoring = false;
							}
							if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
							{
								scheme.Param.WidthColoringInSm = num;
								scheme.Param.HeightColoringInSm = num2;
								scheme.ScaleSourceImageInColoring();
								LoadSourceImageToImageControls();
								scheme.Param.WidthInBead = scheme.BmpSourceImage.Width;
								scheme.Param.HeightInBead = scheme.BmpSourceImage.Height;
							}
							else
							{
								scheme.Param.WidthInBead = Convert.ToInt32(tbxSchemeWidthInBlocks.Text);
								scheme.Param.HeightInBead = Convert.ToInt32(tbxSchemeHeightInBlocks.Text);
							}
							System.Windows.Controls.Button saveSchemeButton = SaveSchemeButton;
							System.Windows.Controls.Button printSchemeButton = PrintSchemeButton;
							Grid grid = grdManualEditingScheme;
							bool flag3 = grid.IsEnabled = false;
							bool isEnabled = printSchemeButton.IsEnabled = flag3;
							saveSchemeButton.IsEnabled = isEnabled;
							System.Windows.Controls.Image image = imgForHandEditing;
							System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
							ImageSource imageSource3 = image.Source = (image2.Source = null);
							lbxEnableBeads.DataContext = null;
							System.Windows.Controls.Button saveSchemeReworkButton = SaveSchemeReworkButton;
							System.Windows.Controls.Button printSchemeReworkButton = PrintSchemeReworkButton;
							isEnabled = (printSchemeReworkButton.IsEnabled = false);
							saveSchemeReworkButton.IsEnabled = isEnabled;
							UpdateHandEditingTabOnSimpleOrManufacturerSelectingColors();
							wbmpRework = (wbmpHandEditing = null);
							BackgroundWorker backgroundWorker = new BackgroundWorker();
							backgroundWorker.RunWorkerCompleted += bwGenerateScheme_RunWorkerCompleted;
							backgroundWorker.DoWork += bwGenerateScheme_DoWork;
							backgroundWorker.RunWorkerAsync();
						}
					}
				}
			}
		}

		private void bwGenerateScheme_DoWork(object sender, DoWorkEventArgs e)
		{
			e.Result = scheme.GeneratePrimaryScheme();
		}

		private void bwGenerateScheme_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			IsInProcess = false;
			WaitingWindow.HideWindow();
			SystemSounds.Asterisk.Play();
			if ((bool)e.Result)
			{
				ImageFormat imageFormat = ImageFormat.Png;
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					imageFormat = ImageFormat.Bmp;
				}
				DrawBitmapOnWPFImage(scheme.BmpBeadworkSource, imgCreateScheme, imageFormat);
				ChangeScheme_InPrimary_AtScaleSize();
				System.Windows.Controls.Button saveSchemeButton = SaveSchemeButton;
				System.Windows.Controls.Button printSchemeButton = PrintSchemeButton;
				bool isEnabled = printSchemeButton.IsEnabled = true;
				saveSchemeButton.IsEnabled = isEnabled;
				TextBlock tblSchemeSourceAndCountColors = TblSchemeSourceAndCountColors;
				TextBlock tblSchemeReworkAndCountColors = TblSchemeReworkAndCountColors;
				string text3 = tblSchemeSourceAndCountColors.Text = (tblSchemeReworkAndCountColors.Text = string.Format(Settings.nameOfWork + Local.Get(" из") + " {0} {1}:", scheme.CountResColorsSource, AllString.GetCountColorString(scheme.CountResColorsSource)));
				tblSchemeNotCreatedOnFirstTab.Visibility = Visibility.Collapsed;
				System.Windows.Controls.Image image = image17;
				System.Windows.Controls.Image image2 = image18;
				isEnabled = (image2.IsEnabled = true);
				image.IsEnabled = isEnabled;
				tblMoveToReworkFromCreate.IsEnabled = true;
				tblMoveToHandEditingFromCreate.IsEnabled = true;
				UpdateMaximumAndCurrentValueOnSlidersOfSewingBrushAndHandEditingBrush();
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && scheme.Param.BiserType == BiserType.ForManufacturers && scheme.Param.IsMixingColors)
				{
					List<Beads> list = new List<Beads>();
					List<Beads> list2 = new List<Beads>();
					foreach (Beads item in scheme._resBeadsMain)
					{
						if (item.mixingColors == null)
						{
							list2.Add(item);
						}
						else
						{
							list.Add(item);
						}
					}
					if (list2.Count() > 0)
					{
						wndWrongMixingColorsInColoring wndWrongMixingColorsInColoring = new wndWrongMixingColorsInColoring(list2);
						wndWrongMixingColorsInColoring.ShowDialog();
					}
				}
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					tblCountOfCountours.Text = scheme.contoursMain.Count().ToString() + " " + AllString.GetCountContours(scheme.contoursMain.Count());
				}
			}
			else
			{
				string strSrc = (Settings.hobbyProgramm != HobbyProgramm.Raskraska) ? ("Мощности Вашего компьютера (оперативной памяти) не хватило для создания схемы такого большого размера. Поэтому либо уменьшите размер схемы в см, либо зайдите в пункт 'Прочие настройки' (через соотв. кнопку над кнопкой 'Создать схему') и установите там мЕньшие значения для 'Размер " + Settings.nameOfOneBeads + " на схеме в процессе работы (в пикселях)'. И затем создайте схему заново") : ((v >= 2) ? "Произошла ошибка. Вероятнее всего, мощности компьютера (оперативной памяти) не хватило для создания раскраски такого большого размера. Попробуйте уменьшить размер раскраски в см и/или указать более плохое качество контуров в 'Важных настройках' (чем выше качество - тем больше оперативой памяти использует программа), либо воспользуйтесь более мощным компьютером. Извините за неудобства." : "Произошла ошибка. Вероятнее всего, мощности компьютера (оперативной памяти) не хватило для создания раскраски такого большого размера. Попробуйте уменьшить размер раскраски в см, либо воспользуйтесь более мощным компьютером. Извините за неудобства.");
				WndInfo wndInfo = new WndInfo(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt(strSrc));
				wndInfo.ShowDialog();
			}
			UpdateHandEditingTab(false, false);
			UpdateReworkTabControls(false, false);
		}

		private void SetToOrderConsumablesButtonCostOfOrder()
		{
			int num = scheme.CalculatePriceOfScheme();
			System.Windows.Controls.Button button = btnOrderConsublesInCreateTab;
			System.Windows.Controls.Button button2 = btnOrderConsublesInReworkTab;
			System.Windows.Controls.Button button3 = btnOrderConsublesInHandEditingTab;
			object obj = button3.Content = "Заказ ЭТОЙ вышивки за " + num.ToString() + "₽";
			object obj4 = button.Content = (button2.Content = obj);
		}

		private void ScaleSrcPhotoReworkSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (scheme.IsImageLoaded)
			{
				ChangeSrcImage_InRework_AtScaleSize();
			}
		}

		private void ScaleSrcPhotoCreateSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (scheme.IsImageLoaded)
			{
				ChangeSrcImage_InPrimary_AtScaleSize();
			}
		}

		private void ScaleResSchemeReworkSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (scheme.IsReworkSchemeLoaded)
			{
				ChangeScheme_InRework_AtScaleSize();
			}
		}

		private void ScaleResSchemeCreateSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (scheme.IsBeadworkSourceCreated)
			{
				ChangeScheme_InPrimary_AtScaleSize();
			}
		}

		private void ChangeSrcImage_InPrimary_AtScaleSize()
		{
			try
			{
				float num = (ScaleSrcPhotoCreateSlider.Value > 500.0) ? (1f + 3f * ((float)ScaleSrcPhotoCreateSlider.Value - 500f) / 500f) : (1f - 0.75f * (500f - (float)ScaleSrcPhotoCreateSlider.Value) / 500f);
				float num2 = (float)ScvSrcPhotoCreate.ViewportWidth;
				float num3 = (float)ScvSrcPhotoCreate.ViewportHeight;
				float num4 = Math.Min(num2 / ((float)scheme.WidthSourceImageInPx + 0f), num3 / ((float)scheme.HeightSourceImageInPx + 0f));
				SrcPhotoCreateImage.Height = (double)((float)scheme.HeightSourceImageInPx * num * num4);
				SrcPhotoCreateImage.Width = (double)((float)scheme.WidthSourceImageInPx * num * num4);
			}
			catch (Exception)
			{
			}
		}

		private void ChangeScheme_InPrimary_AtScaleSize()
		{
			try
			{
				int num = Math.Max(scheme.Param.WidthInBead, scheme.Param.HeightInBead);
				float num2 = (num >= 100 && Settings.hobbyProgramm != HobbyProgramm.Raskraska) ? (((float)num + 0f) / 100f) : 1f;
				float num3 = (ScaleResSchemeCreateSlider.Value > 500.0) ? (1f + num2 * 3f * ((float)ScaleResSchemeCreateSlider.Value - 500f) / 500f) : (1f - 0.75f * (500f - (float)ScaleResSchemeCreateSlider.Value) / 500f);
				float num4 = (float)ScvResSchemeCreate.ViewportWidth;
				float num5 = (float)ScvResSchemeCreate.ViewportHeight;
				float num6 = Math.Min(num4 / ((float)scheme.BmpBeadworkSource.Width + 0f), num5 / ((float)scheme.BmpBeadworkSource.Height + 0f));
				float num7 = (float)scheme.BmpBeadworkSource.Width * num3 * num6;
				float num8 = (float)scheme.BmpBeadworkSource.Height * num3 * num6;
				imgCreateScheme.Height = (double)num8;
				imgCreateScheme.Width = (double)num7;
			}
			catch (Exception)
			{
			}
		}

		private void ChangeSrcImage_InRework_AtScaleSize()
		{
			try
			{
				ScaleImageRework = ((ScaleSrcPhotoReworkSlider.Value > 500.0) ? (1f + 3f * ((float)ScaleSrcPhotoReworkSlider.Value - 500f) / 500f) : (1f - 0.75f * (500f - (float)ScaleSrcPhotoReworkSlider.Value) / 500f));
				float num = (float)ScvSrcPhotoCreate.ViewportWidth;
				float num2 = (float)ScvSrcPhotoCreate.ViewportHeight;
				float num3 = Math.Min(num / ((float)scheme.WidthSourceImageInPx + 0f), num2 / ((float)scheme.HeightSourceImageInPx + 0f));
				SrcPhotoReworkImage.Height = (double)((float)scheme.HeightSourceImageInPx * ScaleImageRework * num3);
				SrcPhotoReworkImage.Width = (double)((float)scheme.WidthSourceImageInPx * ScaleImageRework * num3);
			}
			catch (Exception)
			{
			}
		}

		private void ChangeScheme_InRework_AtScaleSize()
		{
			try
			{
				int num = Math.Max(scheme.Param.WidthInBead, scheme.Param.HeightInBead);
				float num2 = (num >= 100) ? (((float)num + 0f) / 100f) : 1f;
				ScaleSchemeRework = ((ScaleResSchemeReworkSlider.Value > 500.0) ? (1f + num2 * 3f * ((float)ScaleResSchemeReworkSlider.Value - 500f) / 500f) : (1f - 0.75f * (500f - (float)ScaleResSchemeReworkSlider.Value) / 500f));
				float num3 = (float)ScvResSchemeCreate.ViewportWidth;
				float num4 = (float)ScvResSchemeCreate.ViewportHeight;
				float num5 = Math.Min(num3 / ((float)scheme.BmpBeadworkRework.Width + 0f), num4 / ((float)scheme.BmpBeadworkRework.Height + 0f));
				float num6 = (float)scheme.BmpBeadworkRework.Height * ScaleSchemeRework * num5;
				float num7 = (float)scheme.BmpBeadworkRework.Width * ScaleSchemeRework * num5;
				imgReworkScheme.Height = (double)num6;
				imgReworkScheme.Width = (double)num7;
				Scale_FOR_PROPORTION_ReworkScheme = (double)num5;
				DrawBrush(null);
			}
			catch (Exception)
			{
			}
		}

		private void ChangeSchemeImage_InManualEditing_AtScaleSize()
		{
			try
			{
				int num = Math.Max(scheme.Param.WidthInBead, scheme.Param.HeightInBead);
				float num2 = (num >= 100 && Settings.hobbyProgramm != HobbyProgramm.Raskraska) ? (((float)num + 0f) / 100f) : 1f;
				ScaleSchemeManualEditing = ((slrScaleImageForHandEditing.Value > 500.0) ? (1f + num2 * 3f * ((float)slrScaleImageForHandEditing.Value - 500f) / 500f) : (1f - 0.75f * (500f - (float)slrScaleImageForHandEditing.Value) / 500f));
				float num3 = (float)scvForHandEditingBackAndKnot.ViewportWidth;
				float num4 = (float)scvForHandEditingBackAndKnot.ViewportHeight;
				float num5 = Math.Min(num3 / ((float)scheme.BmpBeadworkHandEditing.Width + 0f), num4 / ((float)scheme.BmpBeadworkHandEditing.Height + 0f));
				Scale_FOR_PROPORTION_HandEditingScheme = (double)num5;
				float num6 = (float)scheme.BmpBeadworkHandEditing.Height * ScaleSchemeManualEditing * num5;
				float num7 = (float)scheme.BmpBeadworkHandEditing.Width * ScaleSchemeManualEditing * num5;
				imgForHandEditing.Height = (double)num6;
				imgForHandEditing.Width = (double)num7;
				if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
				{
					imgForHandEditingBackAndKnot.Height = (double)num6;
					imgForHandEditingBackAndKnot.Width = (double)num7;
				}
			}
			catch (Exception)
			{
			}
		}

		private void tbxSchemeWidthInSm_OnTextChanged(object sender, TextChangedEventArgs e)
		{
			if (!IsChangingSchemesSizeInProgram && scheme.IsImageLoaded)
			{
				try
				{
					IsChangingSchemesSizeInProgram = true;
					float sizeInSm = Convert.ToSingle(tbxSchemeWidthInSm.Text);
					int schemesWidthInBlockFromWidthInSm = scheme.GetSchemesWidthInBlockFromWidthInSm(sizeInSm);
					int schemesHeightInBlockFromWidthInBlock = scheme.GetSchemesHeightInBlockFromWidthInBlock(schemesWidthInBlockFromWidthInSm);
					tbxSchemeHeightInSm.Text = scheme.GetSchemesHeightInSmFromHeightInBlock(schemesHeightInBlockFromWidthInBlock).ToString();
					tbxSchemeWidthInBlocks.Text = schemesWidthInBlockFromWidthInSm.ToString();
					tbxSchemeHeightInBlocks.Text = schemesHeightInBlockFromWidthInBlock.ToString();
					IsChangingSchemesSizeInProgram = false;
				}
				catch (Exception)
				{
					IsChangingSchemesSizeInProgram = false;
				}
			}
		}

		private void tbxSchemeHeightInSm_OnTextChanged(object sender, TextChangedEventArgs e)
		{
			if (!IsChangingSchemesSizeInProgram && scheme.IsImageLoaded)
			{
				try
				{
					IsChangingSchemesSizeInProgram = true;
					float sizeInSm = Convert.ToSingle(tbxSchemeHeightInSm.Text);
					int schemesHeightInBlockFromHeightInSm = scheme.GetSchemesHeightInBlockFromHeightInSm(sizeInSm);
					int schemesWidthInBlockFromHeightInBlock = scheme.GetSchemesWidthInBlockFromHeightInBlock(schemesHeightInBlockFromHeightInSm);
					tbxSchemeWidthInSm.Text = scheme.GetSchemesWidthInSmFromWidthInBlock(schemesWidthInBlockFromHeightInBlock).ToString();
					tbxSchemeHeightInBlocks.Text = schemesHeightInBlockFromHeightInSm.ToString();
					tbxSchemeWidthInBlocks.Text = schemesWidthInBlockFromHeightInBlock.ToString();
					IsChangingSchemesSizeInProgram = false;
				}
				catch (Exception)
				{
					IsChangingSchemesSizeInProgram = false;
				}
			}
		}

		private void tbxSchemeWidthInBlocks_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!IsChangingSchemesSizeInProgram && scheme.IsImageLoaded)
			{
				try
				{
					IsChangingSchemesSizeInProgram = true;
					int num = Convert.ToInt32(tbxSchemeWidthInBlocks.Text);
					int schemesHeightInBlockFromWidthInBlock = scheme.GetSchemesHeightInBlockFromWidthInBlock(num);
					tbxSchemeHeightInBlocks.Text = schemesHeightInBlockFromWidthInBlock.ToString();
					System.Windows.Controls.TextBox textBox = tbxSchemeHeightInSm;
					float num2 = scheme.GetSchemesHeightInSmFromHeightInBlock(schemesHeightInBlockFromWidthInBlock);
					textBox.Text = num2.ToString();
					System.Windows.Controls.TextBox textBox2 = tbxSchemeWidthInSm;
					num2 = scheme.GetSchemesWidthInSmFromWidthInBlock(num);
					textBox2.Text = num2.ToString();
					IsChangingSchemesSizeInProgram = false;
				}
				catch (Exception)
				{
					IsChangingSchemesSizeInProgram = false;
				}
			}
		}

		private void tbxSchemeHeightInBlocks_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!IsChangingSchemesSizeInProgram && scheme.IsImageLoaded)
			{
				try
				{
					IsChangingSchemesSizeInProgram = true;
					int num = Convert.ToInt32(tbxSchemeHeightInBlocks.Text);
					int schemesWidthInBlockFromHeightInBlock = scheme.GetSchemesWidthInBlockFromHeightInBlock(num);
					tbxSchemeWidthInBlocks.Text = schemesWidthInBlockFromHeightInBlock.ToString();
					System.Windows.Controls.TextBox textBox = tbxSchemeHeightInSm;
					float num2 = scheme.GetSchemesHeightInSmFromHeightInBlock(num);
					textBox.Text = num2.ToString();
					System.Windows.Controls.TextBox textBox2 = tbxSchemeWidthInSm;
					num2 = scheme.GetSchemesWidthInSmFromWidthInBlock(schemesWidthInBlockFromHeightInBlock);
					textBox2.Text = num2.ToString();
					IsChangingSchemesSizeInProgram = false;
				}
				catch (Exception)
				{
					IsChangingSchemesSizeInProgram = false;
				}
			}
		}

		private void ScaleSewingBrush_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			DrawBrush(null);
		}

		private void ScvResSchemeRework_OnMouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			elpBrushForMakingInvisible.Visibility = Visibility.Hidden;
			elpBrushForMakingInvisibleFake.Visibility = Visibility.Hidden;
			DrawBrush(new System.Windows.Point(0.0, 0.0));
		}

		private void imgReworkScheme_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			elpBrushForMakingInvisible.Visibility = Visibility.Hidden;
			elpBrushForMakingInvisibleFake.Visibility = Visibility.Hidden;
		}

		private void imgReworkScheme_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
		{
			elpBrushForMakingInvisible.Visibility = Visibility.Visible;
			elpBrushForMakingInvisibleFake.Visibility = Visibility.Visible;
		}

		private void imgReworkScheme_MouseDown(object sender, MouseButtonEventArgs e)
		{
			MouseMoveClickReworkSewing(e);
		}

		private void imgReworkScheme_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
		{
			DrawBrush(e);
			MouseMoveClickReworkSewing(e);
		}

		private void MouseMoveClickReworkSewing(System.Windows.Input.MouseEventArgs e)
		{
			if (!IsInProcess && scheme.IsReworkSchemeLoaded && RbtCorrectionModeSewing.IsChecked.Value)
			{
				bool? nullable = null;
				if (e.LeftButton == MouseButtonState.Pressed)
				{
					nullable = false;
				}
				else if (e.RightButton == MouseButtonState.Pressed)
				{
					nullable = true;
				}
				if (nullable.HasValue)
				{
					ReworkSchemeSewing(e, nullable.Value);
				}
			}
		}

		private void DrawBrush(System.Windows.Input.MouseEventArgs e = null)
		{
			int squreCellSidePreview_X = scheme.SqureCellSidePreview_X;
			double height = ScaleSewingBrush.Value * (double)squreCellSidePreview_X * Scale_FOR_PROPORTION_ReworkScheme * (double)ScaleSchemeRework;
			double width = ScaleSewingBrush.Value * (double)squreCellSidePreview_X * Scale_FOR_PROPORTION_ReworkScheme * (double)ScaleSchemeRework;
			elpBrushForMakingInvisible.Height = height;
			elpBrushForMakingInvisible.Width = width;
			elpBrushForMakingInvisibleFake.Height = height;
			elpBrushForMakingInvisibleFake.Width = width;
			if (e != null)
			{
				System.Windows.Point position = e.GetPosition(imgReworkScheme);
				position.X -= elpBrushForMakingInvisible.Width / 2.0;
				position.Y -= elpBrushForMakingInvisible.Height / 2.0;
				elpBrushForMakingInvisible.Margin = new Thickness(position.X, position.Y, 0.0, 0.0);
				elpBrushForMakingInvisibleFake.Margin = new Thickness(position.X, position.Y, 0.0, 0.0);
			}
		}

		private void DrawBrush(System.Windows.Point ptCenter)
		{
			int squreCellSidePreview_X = scheme.SqureCellSidePreview_X;
			double height = ScaleSewingBrush.Value * (double)squreCellSidePreview_X * Scale_FOR_PROPORTION_ReworkScheme * (double)ScaleSchemeRework;
			double width = ScaleSewingBrush.Value * (double)squreCellSidePreview_X * Scale_FOR_PROPORTION_ReworkScheme * (double)ScaleSchemeRework;
			elpBrushForMakingInvisible.Height = height;
			elpBrushForMakingInvisible.Width = width;
			elpBrushForMakingInvisibleFake.Height = height;
			elpBrushForMakingInvisibleFake.Width = width;
			double x = ptCenter.X;
			double y = ptCenter.Y;
			x -= elpBrushForMakingInvisible.Width / 2.0;
			y -= elpBrushForMakingInvisible.Height / 2.0;
			elpBrushForMakingInvisible.Margin = new Thickness(x, y, 0.0, 0.0);
			elpBrushForMakingInvisibleFake.Margin = new Thickness(x, y, 0.0, 0.0);
		}

		private void tblLoadFromSavedScheme_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (!IsInProcess)
			{
				string description = "Выберите папку с сохранённой ранее схемой";
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					description = "Выберите папку с сохранённой ранее картиной";
				}
				FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog
				{
					ShowNewFolderButton = false,
					Description = description
				};
				DialogResult dialogResult = folderBrowserDialog.ShowDialog();
				if (dialogResult != System.Windows.Forms.DialogResult.OK && dialogResult != System.Windows.Forms.DialogResult.Yes)
				{
					return;
				}
				IsInProcess = true;
				SetWriteableBitmapBufferToNull_GCCollect();
				if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfRaskraskaForNikolayUkraina || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina || Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
				{
					wndSetCatalogueNameAndArticleOfScheme wndSetCatalogueNameAndArticleOfScheme = new wndSetCatalogueNameAndArticleOfScheme();
					wndSetCatalogueNameAndArticleOfScheme.ShowDialog();
					if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina || Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
					{
						nameScheme = wndSetCatalogueNameAndArticleOfScheme.name;
					}
					if (!Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
					{
						articleScheme = wndSetCatalogueNameAndArticleOfScheme.article;
					}
				}
				string strSrc = "Пожалуйста, подождите секундочку, пока схема загрузится...";
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					strSrc = "ПРОГРАММА НЕ ЗАВИСЛА! Процесс загрузки сохранённой ранее картины по номерам может занимать до 5-10 минут. При этом все элементы программы не активны. Ожидайте, пожалуйста...";
				}
				strSrc = ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt(strSrc);
				WaitingWindow.ChangeTextInWindow(strSrc);
				WaitingWindow.ShowWindow();
				ScaleSrcPhotoCreateSlider.Value = 500.0;
				selectedPath = folderBrowserDialog.SelectedPath;
				try
				{
					file = Directory.GetFiles(selectedPath, "*.dat").First();
				}
				catch (Exception)
				{
					errorMessage = ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Вы указали папку, в которой нет файла сохранённой схемы (*.dat)");
					if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
					{
						errorMessage += ". Возможно, Вы пытаетесь загрузить в программу более ранние сохранения, когда программа ещё не сохраняла файлы проекта с расширением *.dat";
					}
					isOkLoading = false;
				}
				if (isOkLoading)
				{
					scheme.DisposeAllBitmaps();
					dt = DateTime.Now;
					base.Dispatcher.BeginInvoke((System.Action)delegate
					{
						LoadSchemeFromFile();
						AfterLoadingSchemeFromFile();
					}, DispatcherPriority.ApplicationIdle);
				}
				else
				{
					AfterLoadingSchemeFromFile();
				}
			}
		}

		private void LoadSchemeFromFile()
		{
			try
			{
				using (Stream serializationStream = File.OpenRead(file))
				{
					BinaryFormatter binaryFormatter = new BinaryFormatter();
					scheme = (Scheme)binaryFormatter.Deserialize(serializationStream);
				}
			}
			catch (Exception ex)
			{
				System.Windows.MessageBox.Show(ex.InnerException + "\r\n\r\n\r\n" + ex.StackTrace);
				Site.SendExceptionInfo(ex, false);
				errorMessage = ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Произошла какая-то ошибка при загрузке схемы в программу. Пожалуйста, сообщите разработчикам, мы попробуем исправить эту проблему.");
				isOkLoading = false;
			}
		}

		private void AfterLoadingSchemeFromFile()
		{
			if (isOkLoading)
			{
				scheme.IsNeedToSave = false;
				scheme.pathToSrcImage = selectedPath + "\\Исходное изображение" + scheme.ExtSourceImage;
				Result = file.Replace(selectedPath + "\\", "").Replace(".dat", "");
				scheme.nameScheme = nameScheme;
				scheme.articleScheme = articleScheme;
			}
			if (isOkLoading)
			{
				ConfigureWindowAfterOlderSchemeLoading(Result);
			}
			else
			{
				WaitingWindow.HideWindow();
				IsInProcess = false;
				WndInfo wndInfo = new WndInfo(errorMessage);
				wndInfo.ShowDialog();
			}
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				SystemSounds.Exclamation.Play();
			}
		}

		private void ConfigureWindowAfterOlderSchemeLoading(string result)
		{
			int num = 1;
			try
			{
				num = Convert.ToInt16(result);
			}
			catch
			{
				num = 1;
			}
			DrawBitmapOnWPFImage(scheme.BmpSourceImage, SrcPhotoCreateImage, ImageFormat.Jpeg);
			DrawBitmapOnWPFImage(scheme.BmpSourceImage, SrcPhotoReworkImage, ImageFormat.Jpeg);
			ChangeSrcImage_InPrimary_AtScaleSize();
			ChangeSrcImage_InRework_AtScaleSize();
			System.Windows.Controls.Button createSchemeButton = CreateSchemeButton;
			System.Windows.Controls.Button saveSchemeButton = SaveSchemeButton;
			System.Windows.Controls.Button printSchemeButton = PrintSchemeButton;
			bool flag2 = printSchemeButton.IsEnabled = true;
			bool isEnabled = saveSchemeButton.IsEnabled = flag2;
			createSchemeButton.IsEnabled = isEnabled;
			tblIsImageLoaded.Visibility = Visibility.Collapsed;
			if (scheme.IsReworkSchemeLoaded)
			{
				tblIsSchemeToReworkEnabled.Visibility = Visibility.Collapsed;
				if (scheme.IsReworkSchemeEdited)
				{
					UpdateReworkTabControls(true, true);
				}
				else
				{
					UpdateReworkTabControls(true, false);
				}
			}
			else
			{
				UpdateReworkTabControls(false, false);
				tblIsSchemeToReworkEnabled.Visibility = Visibility.Visible;
			}
			if (scheme.IsReworkSchemeEdited)
			{
				tblMoveToHandEditingFromRework.IsEnabled = (v == 3);
			}
			else
			{
				tblMoveToHandEditingFromRework.IsEnabled = false;
			}
			if (scheme.IsHandEditingSchemeLoaded)
			{
				tblIsSchemeToManualEditingEnabled.Visibility = Visibility.Collapsed;
				if (scheme.IsHandEditingSchemeEdited)
				{
					UpdateHandEditingTab(true, true);
				}
				else
				{
					UpdateHandEditingTab(true, false);
				}
				UpdateCountColorsAndListToReplaceColorsInHandEditingTab(null);
			}
			else
			{
				UpdateHandEditingTab(false, false);
				tblIsSchemeToManualEditingEnabled.Visibility = Visibility.Visible;
			}
			if (scheme.IsHandEditingSchemeEdited)
			{
				tblMoveToReworkFromHandEditing.IsEnabled = true;
			}
			else
			{
				tblMoveToReworkFromHandEditing.IsEnabled = false;
			}
			tblIsSourceImageToReworkEnabled.Visibility = Visibility.Collapsed;
			if (v != 0)
			{
				gbxMainSettingScheme.IsEnabled = true;
			}
			btnShowSettings.IsEnabled = true;
			DrawEmbroideryReworkOnImage();
			DrawEmbroideryHandEditingOnImage();
			if (scheme.IsHandEditingSchemeLoaded && Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				DrawBitmapOnWPFImage(scheme.BmpBackAndKnot, imgForHandEditingBackAndKnot, ImageFormat.Png);
			}
			if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				btnUndoBackstitchInHandEditing.IsEnabled = (scheme.backstitchHandEditing.Count() > 0);
				btnUndoKnotInHandEditing.IsEnabled = (scheme.knotHandEditing.Count() > 0);
			}
			ImageFormat imageFormat = ImageFormat.Png;
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				imageFormat = ImageFormat.Bmp;
			}
			int num2;
			if (Settings.hobbyProgramm != HobbyProgramm.Raskraska || !scheme.IsHandEditingSchemeLoaded)
			{
				DrawBitmapOnWPFImage(scheme.BmpBeadworkSource, imgCreateScheme, imageFormat);
				ChangeScheme_InPrimary_AtScaleSize();
				TblSchemeSourceAndCountColors.Text = string.Format(Settings.nameOfWork + Local.Get(" из") + " {0} {1}:", scheme.CountResColorsSource, AllString.GetCountColorString(scheme.CountResColorsSource));
				tblSchemeNotCreatedOnFirstTab.Visibility = Visibility.Collapsed;
				System.Windows.Controls.Image image = image17;
				System.Windows.Controls.Image image2 = image18;
				isEnabled = (image2.IsEnabled = true);
				image.IsEnabled = isEnabled;
				tblMoveToReworkFromCreate.IsEnabled = true;
				tblMoveToHandEditingFromCreate.IsEnabled = true;
				System.Windows.Controls.TextBox textBox = tbxCountColors;
				num2 = scheme.CountResColorsSource;
				textBox.Text = num2.ToString();
			}
			IsChangingSchemesSizeInProgram = true;
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				tbxSchemeWidthInSm.Text = scheme.Param.WidthColoringInSm.ToString();
				tbxSchemeHeightInSm.Text = scheme.Param.HeightColoringInSm.ToString();
			}
			else
			{
				System.Windows.Controls.TextBox textBox2 = tbxSchemeWidthInSm;
				num2 = (int)Math.Round((double)((float)scheme.Param.WidthInBead / scheme.CountBeadesInSm_X));
				textBox2.Text = num2.ToString();
				System.Windows.Controls.TextBox textBox3 = tbxSchemeHeightInSm;
				num2 = (int)Math.Round((double)((float)scheme.Param.HeightInBead / scheme.CountBeadesInSm_Y));
				textBox3.Text = num2.ToString();
			}
			IsChangingSchemesSizeInProgram = false;
			RbtBlockTypeScheme.IsChecked = (scheme.Param.Type == SchemeType.Block);
			RbtNormalTypeScheme.IsChecked = (scheme.Param.Type == SchemeType.Normal);
			rbtSquareStraz.IsChecked = scheme.Param.isSquareStraz;
			rbtCircleStraz.IsChecked = !scheme.Param.isSquareStraz;
			RbtPreciosaSelectingColors.IsChecked = (scheme.Param.BiserType == BiserType.ForManufacturers);
			RbtSimpleSelectingColors.IsChecked = (scheme.Param.BiserType == BiserType.Any);
			UpdateHandEditingTabOnSimpleOrManufacturerSelectingColors();
			UpdateMaximumAndCurrentValueOnSlidersOfSewingBrushAndHandEditingBrush();
			Nodes = new ObservableCollection<Node>();
			bool flag5 = false;
			bool flag6 = false;
			bool flag7 = false;
			bool flag8 = false;
			int num3 = 0;
			for (int i = 0; i < scheme.beadsManufacturer.Count(); i++)
			{
				Node node = new Node
				{
					Text = scheme.beadsManufacturer[i].name,
					Tag = scheme.beadsManufacturer[i]
				};
				if (i == scheme.beadsManufacturer.Count() - 1)
				{
					node.IsExpanded = true;
				}
				flag7 = false;
				flag8 = false;
				for (int j = 0; j < scheme.beadsManufacturer[i].beadsSeries.Count(); j++)
				{
					Node node2 = new Node
					{
						Text = scheme.beadsManufacturer[i].beadsSeries[j].name,
						Tag = scheme.beadsManufacturer[i].beadsSeries[j]
					};
					if (i == scheme.beadsManufacturer.Count() - 1 && j == 0)
					{
						node2.IsExpanded = true;
					}
					node2.Parent.Add(node);
					node.Children.Add(node2);
					flag5 = false;
					flag6 = false;
					for (int k = 0; k < scheme.beadsManufacturer[i].beadsSeries[j].beads.Count(); k++)
					{
						Beads beads = scheme.beadsManufacturer[i].beadsSeries[j].beads[k];
						Node node3 = new Node
						{
							Text = beads.name,
							Tag = beads,
							Brush = beads.brushForWPF,
							IsChecked = new bool?(beads.isUsing),
							Width = 22
						};
						if (beads.isUsing)
						{
							flag5 = true;
						}
						else
						{
							flag6 = true;
						}
						node3.Parent.Add(node2);
						node2.Children.Add(node3);
					}
					if (flag5)
					{
						flag7 = true;
						if (flag6)
						{
							flag8 = true;
							node2.IsChecked = null;
						}
						else
						{
							node2.IsChecked = true;
						}
					}
					else
					{
						flag8 = true;
						node2.IsChecked = false;
					}
				}
				if (flag7)
				{
					if (flag8)
					{
						node.IsChecked = null;
					}
					else
					{
						node.IsChecked = true;
					}
				}
				else
				{
					node.IsChecked = false;
				}
				Nodes.Add(node);
			}
			trvCatalog.ItemsSource = Nodes;
			TbcMain.SelectedIndex = num - 1;
			TbcMain.UpdateLayout();
			WaitingWindow.HideWindow();
			IsInProcess = false;
			switch (num)
			{
			case 2:
				ScaleResSchemeReworkSlider.Value = 400.0;
				ScaleResSchemeReworkSlider.Value = 500.0;
				break;
			case 3:
				slrScaleImageForHandEditing.Value = 400.0;
				slrScaleImageForHandEditing.Value = 500.0;
				break;
			}
			if (scheme.IsHandEditingSchemeLoaded && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				ClearFirstTabInRaskraskaAfterMOvingSchemeOnHandEditingTab();
				tbxCountColors.Text = "";
			}
			rbtGetColorWithPipet.IsChecked = true;
		}

		private void tblLoadFromImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			LoadImage();
		}

		private void LoadImage()
		{
			if (!IsInProcess)
			{
				Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog
				{
					Title = ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Выберите изображение, из которого необходимо создать схему"),
					Filter = "Изображения (*.bmp, *.jpg, *.jpeg, *.png)|*.bmp;*.jpg;*.jpeg;*.png"
				};
				bool? nullable = openFileDialog.ShowDialog();
				if (nullable.HasValue && nullable.Value)
				{
					IsInProcess = true;
					string p = "Пожалуйста, подождите секундочку, пока изображение загрузится...";
					path = openFileDialog.FileName;
					SetTabsStateBeforeSourceImageLoading();
					processingSourceBmp = new Bitmap(path);
					WaitingWindow.ChangeTextInWindow(p);
					WaitingWindow.ShowWindow();
					BackgroundWorker backgroundWorker = new BackgroundWorker();
					backgroundWorker.RunWorkerCompleted += bwLoadImage_RunWorkerCompleted;
					backgroundWorker.DoWork += bwLoadImage_DoWork;
					backgroundWorker.RunWorkerAsync();
				}
			}
		}

		private void SetTabsStateBeforeSourceImageLoading()
		{
			SetWriteableBitmapBufferToNull_GCCollect();
			System.Windows.Controls.Image image = image17;
			System.Windows.Controls.Image image2 = image18;
			bool isEnabled = image2.IsEnabled = false;
			image.IsEnabled = isEnabled;
			CreateNewScheme(null);
			ScaleSrcPhotoCreateSlider.Value = 500.0;
			UpdateReworkTabControls(false, false);
			UpdateHandEditingTab(false, false);
			TextBlock textBlock = tblMoveToHandEditingFromCreate;
			TextBlock textBlock2 = tblMoveToReworkFromCreate;
			isEnabled = (textBlock2.IsEnabled = false);
			textBlock.IsEnabled = isEnabled;
			tblMoveToHandEditingFromRework.IsEnabled = false;
			tblMoveToReworkFromHandEditing.IsEnabled = false;
			tblCountOfCountours.Text = "";
		}

		private void SetTabsStateImmediatelyAfterSourceImageLoading()
		{
			try
			{
				LoadSourceImageToImageControls();
				try
				{
					ChangeSrcImage_InPrimary_AtScaleSize();
					ChangeSrcImage_InRework_AtScaleSize();
					CreateSchemeButton.IsEnabled = true;
					System.Windows.Controls.Button saveSchemeButton = SaveSchemeButton;
					System.Windows.Controls.Button printSchemeButton = PrintSchemeButton;
					bool isEnabled = printSchemeButton.IsEnabled = false;
					saveSchemeButton.IsEnabled = isEnabled;
					tblIsImageLoaded.Visibility = Visibility.Collapsed;
					tblSchemeNotCreatedOnFirstTab.Visibility = Visibility.Visible;
					if (v != 0)
					{
						if (tbxSchemeHeightInSm.Text.Length > 0)
						{
							RefreshTextBox(tbxSchemeHeightInSm);
						}
						else if (tbxSchemeWidthInSm.Text.Length > 0)
						{
							RefreshTextBox(tbxSchemeWidthInSm);
						}
					}
					else
					{
						IsChangingSchemesSizeInProgram = true;
						tbxSchemeWidthInSm.Clear();
						tbxSchemeHeightInSm.Clear();
						IsChangingSchemesSizeInProgram = false;
						if (scheme.BmpSourceImage.Width > scheme.BmpSourceImage.Height)
						{
							tbxSchemeWidthInSm.Text = Settings.sizeByBigSideInDemo.ToString();
						}
						else
						{
							tbxSchemeHeightInSm.Text = Settings.sizeByBigSideInDemo.ToString();
						}
					}
					System.Windows.Controls.Image image = imgForHandEditing;
					System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
					System.Windows.Controls.Image image3 = imgReworkScheme;
					System.Windows.Controls.Image image4 = imgCreateScheme;
					ImageSource imageSource2 = image4.Source = null;
					ImageSource imageSource4 = image3.Source = imageSource2;
					ImageSource imageSource7 = image.Source = (image2.Source = imageSource4);
					tblIsSourceImageToReworkEnabled.Visibility = Visibility.Collapsed;
					TblSchemeSourceAndCountColors.Text = Settings.nameOfWork + ": ";
					if (v != 0)
					{
						gbxMainSettingScheme.IsEnabled = true;
					}
					btnShowSettings.IsEnabled = true;
					btnDoPreprocessingSourcePhoto.IsEnabled = true;
				}
				catch
				{
				}
				if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfRaskraskaForNikolayUkraina || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina || Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
				{
					wndSetCatalogueNameAndArticleOfScheme wndSetCatalogueNameAndArticleOfScheme = new wndSetCatalogueNameAndArticleOfScheme();
					wndSetCatalogueNameAndArticleOfScheme.ShowDialog();
					if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina || Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
					{
						scheme.nameScheme = wndSetCatalogueNameAndArticleOfScheme.name;
					}
					if (!Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
					{
						scheme.articleScheme = wndSetCatalogueNameAndArticleOfScheme.article;
					}
				}
			}
			catch (Exception exc)
			{
				Site.SendExceptionInfo(exc, false);
				WndInfo wndInfo = new WndInfo("При загрузке исходного изображения произошла ошибка - возможно, формат изображения не подходит.Пожалуйста, загрузите другое исходное изображение, либо сохраните это же изображение в другом формате и попробуйте загрузить его вновь. Извините за неудобства.");
				wndInfo.ShowDialog();
			}
		}

		private void CreateNewScheme(string pathToSourceImage = null)
		{
			List<BeadsManufacturer> beadsManufacturer = scheme.beadsManufacturer;
			string pathToSrcImage = scheme.pathToSrcImage;
			if (scheme != null)
			{
				scheme = null;
			}
			scheme = new Scheme();
			scheme.pathToSrcImage = pathToSrcImage;
			processingSourceBmp = null;
			GC.Collect(GC.MaxGeneration);
			scheme.beadsManufacturer = beadsManufacturer;
			beadsManufacturer = null;
		}

		private void bwLoadImage_DoWork(object sender, DoWorkEventArgs e)
		{
			scheme.LoadSourceImage(path, processingSourceBmp);
		}

		private void LoadSourceImageToImageControls()
		{
			System.Windows.Controls.Image srcPhotoCreateImage = SrcPhotoCreateImage;
			System.Windows.Controls.Image srcPhotoReworkImage = SrcPhotoReworkImage;
			ImageSource imageSource3 = srcPhotoCreateImage.Source = (srcPhotoReworkImage.Source = null);
			DrawBitmapOnWPFImage(scheme.BmpSourceImage, SrcPhotoCreateImage, ImageFormat.Jpeg);
			DrawBitmapOnWPFImage(scheme.BmpSourceImage, SrcPhotoReworkImage, ImageFormat.Jpeg);
		}

		private void bwLoadImage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			SetTabsStateImmediatelyAfterSourceImageLoading();
			WaitingWindow.HideWindow();
			IsInProcess = false;
		}

		private void SaveSchemeButton_Click(object sender, RoutedEventArgs e)
		{
			if (!IsInProcess && scheme.IsBeadworkSourceCreated)
			{
				SaveScheme(1);
			}
		}

		private void SaveSchemeReworkButton_Click(object sender, RoutedEventArgs e)
		{
			if (!IsInProcess && scheme.IsReworkSchemeLoaded)
			{
				SaveScheme(2);
			}
		}

		private void btnSaveSchemeAfterDrawing_Click(object sender, RoutedEventArgs e)
		{
			if (!IsInProcess && scheme.IsHandEditingSchemeLoaded)
			{
				SaveScheme(3);
			}
		}

		private void SaveScheme(int numberOfTab)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog
			{
				Description = ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Выберите пустую папку для сохранения схемы"),
				ShowNewFolderButton = true,
				SelectedPath = Settings.SelectedLastPathToSave
			};
			if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				string pathToSave = Settings.SelectedLastPathToSave = folderBrowserDialog.SelectedPath;
				if (!Directory.Exists(pathToSave))
				{
					Directory.CreateDirectory(pathToSave);
				}
				bool flag = true;
				DirectoryInfo directoryInfo = new DirectoryInfo(pathToSave);
				if (directoryInfo.GetFiles(Local.Get("Исходное*")).Count() > 0)
				{
					flag = false;
					wndOkCancel wndOkCancel = new wndOkCancel("Внимание!", ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("В этой папке уже содержится сохранённая ранее схема. Перезаписать её (все изображения в папке будут удалены)?"));
					bool? nullable = wndOkCancel.ShowDialog();
					if (nullable.HasValue && nullable.Value)
					{
						try
						{
							FileInfo[] files = directoryInfo.GetFiles("*.png");
							foreach (FileInfo fileInfo in files)
							{
								fileInfo.Delete();
							}
						}
						catch (Exception exc)
						{
							Site.SendExceptionInfo(exc, false);
						}
						flag = true;
					}
				}
				if (flag)
				{
					IsInProcess = true;
					string p = "Пожалуйста, подождите, пока схема сохранится на компьютер. Обычно это занимает от пары секунд до 2 минут.";
					if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
					{
						p = "Пожалуйста, подождите, пока схема сохранится на компьютер. Обычно это занимает от 5 секунд до 3~10 минут...";
						if (scheme.Param.countOfPixelsInCmAtContoursMapOfColoring > 40)
						{
							p = "Пожалуйста, подождите, пока схема сохранится на компьютер. Обычно это занимает от 5 секунд до 3~10 минут...";
						}
					}
					WaitingWindow.ChangeTextInWindow(p);
					WaitingWindow.ShowWindow();
					BackgroundWorker backgroundWorker = new BackgroundWorker();
					backgroundWorker.RunWorkerCompleted += bwSavingScheme_RunWorkerCompleted;
					backgroundWorker.DoWork += bwSavingScheme_DoWork;
					backgroundWorker.RunWorkerAsync(new SavingOrPrintingSettings
					{
						isSaving = true,
						pathToSave = pathToSave,
						numberOfTab = numberOfTab
					});
				}
			}
		}

		private void bwSavingScheme_DoWork(object sender, DoWorkEventArgs e)
		{
			SetWriteableBitmapBufferToNull_GCCollect();
			SavingOrPrintingSettings savingOrPrintingSettings = e.Argument as SavingOrPrintingSettings;
			ResultOfSavingOrPrinting resultOfSavingOrPrinting = new ResultOfSavingOrPrinting();
			switch (savingOrPrintingSettings.numberOfTab)
			{
			case 1:
				resultOfSavingOrPrinting = scheme.SaveMainSchemeAtDirectory(savingOrPrintingSettings);
				break;
			case 2:
				resultOfSavingOrPrinting = scheme.SaveReworkedSchemeAtDirectory(savingOrPrintingSettings);
				break;
			case 3:
				resultOfSavingOrPrinting = scheme.SaveHandEditingSchemeAtDirectory(savingOrPrintingSettings);
				break;
			}
			if (resultOfSavingOrPrinting.isOk)
			{
				resultOfSavingOrPrinting.folderToOpenIfOk = savingOrPrintingSettings.pathToSave;
			}
			e.Result = resultOfSavingOrPrinting;
		}

		private void bwSavingScheme_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			IsInProcess = false;
			WaitingWindow.HideWindow();
			ResultOfSavingOrPrinting resultOfSavingOrPrinting = e.Result as ResultOfSavingOrPrinting;
			if (!resultOfSavingOrPrinting.isOk)
			{
				WndInfo wndInfo = new WndInfo(resultOfSavingOrPrinting.error);
				wndInfo.ShowDialog();
			}
			else
			{
				Process.Start(resultOfSavingOrPrinting.folderToOpenIfOk);
			}
		}

		private void ShowWndSettingToAutoSaving()
		{
			wndSettings wndSettings = new wndSettings(scheme, true);
			wndSettings.Show();
		}

		private void PrintSchemeButton_Click(object sender, RoutedEventArgs e)
		{
			if (!IsInProcess && scheme.IsBeadworkSourceCreated)
			{
				PrintScheme(1);
			}
		}

		private void PrintSchemeReworkButton_Click(object sender, RoutedEventArgs e)
		{
			if (!IsInProcess && scheme.IsReworkSchemeLoaded)
			{
				PrintScheme(2);
			}
		}

		private void btnPrintSchemeAfterDrawing_Click(object sender, RoutedEventArgs e)
		{
			if (!IsInProcess && scheme.IsHandEditingSchemeLoaded)
			{
				PrintScheme(3);
			}
		}

		private void PrintScheme(int numberOfTab)
		{
			IsInProcess = true;
			WndSelectPrintingPages wndSelectPrintingPages = new WndSelectPrintingPages();
			if (wndSelectPrintingPages.ShowDialog() == true)
			{
				System.Windows.Forms.PrintDialog printDialog = new System.Windows.Forms.PrintDialog();
				if (printDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
				{
					WaitingWindow.ChangeTextInWindow(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Пожалуйста, подождите, пока происходит печать схемы (обычно это занимает от нескольких секунд до 2 минут)..."));
					WaitingWindow.ShowWindow();
					BackgroundWorker backgroundWorker = new BackgroundWorker();
					backgroundWorker.RunWorkerCompleted += bwPrintingScheme_RunWorkerCompleted;
					backgroundWorker.DoWork += bwPrintingScheme_DoWork;
					backgroundWorker.RunWorkerAsync(new SavingOrPrintingSettings
					{
						isPrinting = true,
						printSettings = wndSelectPrintingPages.printSettings,
						printer = printDialog.PrinterSettings,
						numberOfTab = numberOfTab
					});
				}
				else
				{
					IsInProcess = false;
				}
			}
			else
			{
				IsInProcess = false;
			}
		}

		private void bwPrintingScheme_DoWork(object sender, DoWorkEventArgs e)
		{
			SavingOrPrintingSettings savingOrPrintingSettings = e.Argument as SavingOrPrintingSettings;
			ResultOfSavingOrPrinting result = new ResultOfSavingOrPrinting();
			switch (savingOrPrintingSettings.numberOfTab)
			{
			case 1:
				result = scheme.PrintMainScheme(savingOrPrintingSettings);
				break;
			case 2:
				result = scheme.PrintReworkedScheme(savingOrPrintingSettings);
				break;
			case 3:
				result = scheme.PrintHandEditingScheme(savingOrPrintingSettings);
				break;
			}
			e.Result = result;
		}

		private void bwPrintingScheme_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			WaitingWindow.HideWindow();
			IsInProcess = false;
			ResultOfSavingOrPrinting resultOfSavingOrPrinting = e.Result as ResultOfSavingOrPrinting;
			if (!resultOfSavingOrPrinting.isOk)
			{
				WndInfo wndInfo = new WndInfo(resultOfSavingOrPrinting.error);
				wndInfo.ShowDialog();
			}
		}

		private void SrcPhotoReworkImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (!IsInProcess && scheme.IsReworkSchemeLoaded && RbtCorrectionModeColor.IsChecked.HasValue && RbtCorrectionModeColor.IsChecked.Value)
			{
				if (prevStateBlocksOnReworking.Count() < maxEnabledCountColorEditingForSmallVersion)
				{
					System.Drawing.Color? colorOfClickOnSourceImageInReworkTab = GetColorOfClickOnSourceImageInReworkTab(e.GetPosition(SrcPhotoReworkImage));
					if (colorOfClickOnSourceImageInReworkTab.HasValue)
					{
						IsInProcess = true;
						WaitingWindow.ChangeTextInWindow(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Пожалуйста, подождите, пока выбранный Вами из исходного изображения цвет добавится в схему"));
						WaitingWindow.ShowWindow();
						BackgroundWorker backgroundWorker = new BackgroundWorker();
						backgroundWorker.DoWork += bwAddOrRemoveColor_DoWork;
						backgroundWorker.RunWorkerCompleted += bwAddOrRemoveColor_RunWorkerCompleted;
						bool flag = true;
						backgroundWorker.RunWorkerAsync(new object[2]
						{
							flag,
							colorOfClickOnSourceImageInReworkTab.Value
						});
					}
				}
				else
				{
					WndInfo wndInfo = new WndInfo("В данной версии программы Вы можете добавить/удалить цвета не более " + maxEnabledCountColorEditingForSmallVersion.ToString() + " раз - просто для демонстрации возможностей программы. В продвинутой и мастер-версии это ограничение конечно снято.");
					wndInfo.ShowDialog();
				}
			}
		}

		private System.Drawing.Color? GetColorOfClickOnSourceImageInReworkTab(System.Windows.Point p)
		{
			try
			{
				Bitmap bitmap = new Bitmap((int)SrcPhotoReworkImage.Width, (int)SrcPhotoReworkImage.Height);
				Graphics graphics = Graphics.FromImage(bitmap);
				graphics.DrawImage(scheme.BmpSourceImage, new RectangleF(0f, 0f, (float)bitmap.Width, (float)bitmap.Height));
				int num = (int)Math.Round(p.X);
				int num2 = (int)Math.Round(p.Y);
				System.Drawing.Color pixel = bitmap.GetPixel(num, num2);
				int num3 = pixel.R;
				int num4 = pixel.G;
				int num5 = pixel.B;
				int num6 = 1;
				if (num - 1 >= 0)
				{
					pixel = bitmap.GetPixel(num - 1, num2);
					num3 += pixel.R;
					num4 += pixel.G;
					num5 += pixel.B;
					num6++;
				}
				if (num + 1 < bitmap.Width)
				{
					pixel = bitmap.GetPixel(num + 1, num2);
					num3 += pixel.R;
					num4 += pixel.G;
					num5 += pixel.B;
					num6++;
				}
				if (num2 - 1 >= 0)
				{
					pixel = bitmap.GetPixel(num, num2 - 1);
					num3 += pixel.R;
					num4 += pixel.G;
					num5 += pixel.B;
					num6++;
				}
				if (num2 + 1 < bitmap.Height)
				{
					pixel = bitmap.GetPixel(num, num2 + 1);
					num3 += pixel.R;
					num4 += pixel.G;
					num5 += pixel.B;
					num6++;
				}
				num3 = (int)Math.Round((double)(((float)num3 + 0f) / ((float)num6 + 0f)));
				num4 = (int)Math.Round((double)(((float)num4 + 0f) / ((float)num6 + 0f)));
				num5 = (int)Math.Round((double)(((float)num5 + 0f) / ((float)num6 + 0f)));
				graphics.Dispose();
				bitmap.Dispose();
				Ellipse ellipse = new Ellipse
				{
					VerticalAlignment = VerticalAlignment.Top,
					HorizontalAlignment = System.Windows.HorizontalAlignment.Left,
					Fill = System.Windows.Media.Brushes.Red,
					Width = 5.0,
					Height = 5.0
				};
				p.X /= (double)ScaleImageRework;
				p.Y /= (double)ScaleImageRework;
				ellipse.Margin = new Thickness(p.X * (double)ScaleImageRework - 2.5, p.Y * (double)ScaleImageRework - 2.5, 0.0, 0.0);
				GrdImageRework.Children.Add(ellipse);
				ChangeSrcImage_InRework_AtScaleSize();
				return System.Drawing.Color.FromArgb(255, num3, num4, num5);
			}
			catch
			{
				return null;
			}
		}

		private void AboutTextBlock_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			ShowAboutWnd();
		}

		private static void ShowAboutWnd()
		{
			WndAbout wndAbout = new WndAbout();
			wndAbout.ShowDialog();
		}

		private void WndMain_OnClosing(object sender, CancelEventArgs e)
		{
			bool flag;
			if (scheme.IsNeedToSave)
			{
				flag = false;
				wndOkCancel wndOkCancel = new wndOkCancel("Внимание!", ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Возможно, Вы не сохранили изменения в схеме. Действительно закрыть программу?\r\n\r\n(Да=закрыть, не сохранять; Отмена=не закрывать, чтобы сохранить схему)."));
				if (wndOkCancel.ShowDialog() == true)
				{
					flag = true;
				}
			}
			else
			{
				flag = true;
			}
			if (flag)
			{
				SaveSelectedBeads_ToFile();
				Бисерок.Properties.Settings.Default.Save();
				WaitingWindow.CloseByProgram();
			}
			else
			{
				e.Cancel = true;
			}
		}

		private void HelpTextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			OpenInstruction();
		}

		private static void OpenInstruction()
		{
			try
			{
				if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
				{
					Process.Start(AppDomain.CurrentDomain.BaseDirectory + Local.Get("/Инструкция по работе с программой ") + Settings.ShortProgramName + ".rtf");
				}
				else
				{
					Process.Start(AppDomain.CurrentDomain.BaseDirectory + Local.Get("/Инструкция по работе с программой ") + Settings.ShortProgramName + ".docx");
				}
			}
			catch
			{
				WndInfo wndInfo = new WndInfo("На Вашем компьютере не установлена программа для просмотра файла в формате RTF/docx (в этом формате сохранена инструкция). . Пожалуйста, установите такую программу (например, 'Microsoft Word', 'Open Office Writer', 'WordPad') - и тогда Вы сможете просмотреть инструкцию.");
				wndInfo.ShowDialog();
			}
		}

		private List<System.Drawing.Point> GetPointsFromCircle(PointF ptCenter, double diametr)
		{
			double num = (double)ptCenter.X;
			double num2 = (double)ptCenter.Y;
			double num3 = diametr / 2.0;
			List<System.Drawing.Point> list = new List<System.Drawing.Point>();
			int num4 = (int)(num2 - num3);
			int num5 = (int)Math.Round(num2 + num3);
			int num6 = (int)(num - num3);
			int num7 = (int)Math.Round(num + num3);
			float num8 = ((float)scheme.SqureCellSidePreview_Y + 0f) / ((float)scheme.SqureCellSidePreview_X + 0f);
			num2 *= (double)num8;
			for (int i = num4 - 1; i <= num5 + 1; i++)
			{
				for (int j = num6 - 1; j <= num7 + 1; j++)
				{
					float num9 = (scheme.Param.Type == SchemeType.Block && i % 2 != 0) ? 0.5f : 0f;
					float num10 = (float)j + 0.5f + num9;
					float num11 = ((float)i + 0.5f) * num8;
					if (i >= 0 && i < scheme.Param.HeightInBead && j >= 0 && j < scheme.Param.WidthInBead && Math.Sqrt(((double)num10 - num) * ((double)num10 - num) + ((double)num11 - num2) * ((double)num11 - num2)) <= num3)
					{
						list.Add(new System.Drawing.Point(j, i));
					}
				}
			}
			return list;
		}

		private System.Drawing.Color? GetColorOfClickOnSchemeOnReworkTab(System.Windows.Input.MouseEventArgs e)
		{
			System.Windows.Point position = e.GetPosition(imgReworkScheme);
			int num = (int)(position.Y / imgReworkScheme.Height * (double)scheme.Param.HeightInBead);
			int num2;
			if (scheme.Param.Type == SchemeType.Normal)
			{
				num2 = (int)(position.X / imgReworkScheme.Width * (double)scheme.Param.WidthInBead);
			}
			else
			{
				if (num % 2 != 0)
				{
					float num3 = (float)(position.X / imgReworkScheme.Width) * (float)scheme.Param.WidthInBead - 0.5f;
					if (!(num3 < 0f) && (int)num3 != scheme.Param.WidthInBead - 1)
					{
						num2 = (int)num3;
						goto IL_00fd;
					}
					return null;
				}
				num2 = (int)(position.X / imgReworkScheme.Width * (double)scheme.Param.WidthInBead);
			}
			goto IL_00fd;
			IL_00fd:
			Block block = scheme.blocksRework[num2, num];
			if (block.IsVisible && block.IsItFullStitch.Value)
			{
				System.Drawing.Color clr = block.ResBeads.clr;
				Ellipse ellipse = new Ellipse
				{
					VerticalAlignment = VerticalAlignment.Top,
					HorizontalAlignment = System.Windows.HorizontalAlignment.Left,
					Fill = System.Windows.Media.Brushes.Red,
					Width = 5.0,
					Height = 5.0
				};
				position.X /= (double)ScaleSchemeRework;
				position.Y /= (double)ScaleSchemeRework;
				ellipse.Margin = new Thickness(position.X * (double)ScaleSchemeRework - 2.5, position.Y * (double)ScaleSchemeRework - 2.5, 0.0, 0.0);
				GrdSchemeRework.Children.Add(ellipse);
				ChangeScheme_InRework_AtScaleSize();
				return clr;
			}
			return null;
		}

		private void Window_StateChanged(object sender, EventArgs e)
		{
			if (base.WindowState == System.Windows.WindowState.Minimized)
			{
				WaitingWindow.MinimizeWindow();
			}
			if (base.WindowState != 0 && base.WindowState != System.Windows.WindowState.Maximized)
			{
				return;
			}
			WaitingWindow.MaximizeWindow();
		}

		private void RbtPreciosaSelectingColors_Click(object sender, RoutedEventArgs e)
		{
			trvCatalog.IsEnabled = RbtPreciosaSelectingColors.IsChecked.Value;
		}

		private void RbtSimpleSelectingColors_Click(object sender, RoutedEventArgs e)
		{
			trvCatalog.IsEnabled = RbtPreciosaSelectingColors.IsChecked.Value;
		}

		private void btnRemovaAloneBlocks_Click(object sender, RoutedEventArgs e)
		{
			if (!IsInProcess)
			{
				IsInProcess = true;
				WaitingWindow.ChangeTextInWindow("Пожалуйста, подождите - удаляются одиночные крестики из схемы");
				WaitingWindow.ShowWindow();
				BackgroundWorker backgroundWorker = new BackgroundWorker();
				backgroundWorker.DoWork += bwRemoveAloneBlocks_DoWork;
				backgroundWorker.RunWorkerCompleted += bwRemoveAloneBlocks_RunWorkerCompleted;
				backgroundWorker.RunWorkerAsync();
			}
		}

		private void bwRemoveAloneBlocks_DoWork(object sender, DoWorkEventArgs e)
		{
			scheme.RemoveAloneBlocks();
		}

		private void bwRemoveAloneBlocks_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			DrawEmbroideryReworkOnImage();
			UpdateReworkTabControls(true, true);
			WaitingWindow.HideWindow();
			IsInProcess = false;
		}

		private void rbtColorFromList_Checked(object sender, RoutedEventArgs e)
		{
			System.Windows.Controls.Image image = imgForHandEditing;
			System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
			System.Windows.Input.Cursor cursor2 = image.Cursor = (image2.Cursor = System.Windows.Input.Cursors.Arrow);
			SetEnabledOfSelectingColors();
			modeOfManualEditing = ModeOfManualEditing.GetColorFromList;
		}

		private void rbtPipet_Checked(object sender, RoutedEventArgs e)
		{
			System.Windows.Controls.Image image = imgForHandEditing;
			System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
			System.Windows.Input.Cursor cursor3 = image.Cursor = (image2.Cursor = new System.Windows.Input.Cursor(AppDomain.CurrentDomain.BaseDirectory + "Images\\pipet.cur"));
			SetEnabledOfSelectingColors();
			modeOfManualEditing = ModeOfManualEditing.Pipet;
		}

		private void SetEnabledOfSelectingColors()
		{
			try
			{
				bool value = rbtGetColorWithPipet.IsChecked.Value;
				tblPipetSelectedColorsFromScheme.IsEnabled = value;
			}
			catch (Exception)
			{
			}
		}

		private void cpkColorFromList_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<System.Windows.Media.Color> e)
		{
			if (isEnabledToSelectColorOnManualEditing)
			{
				System.Windows.Media.Color selectedColor = cpkColorFromList.SelectedColor;
				SetListOfColorForManualEditing(selectedColor);
			}
		}

		private void SetListOfColorForManualEditing(System.Windows.Media.Color c)
		{
			lastColorOnManualEditing = c;
			System.Windows.Controls.GroupBox groupBox = gbxModeOfManualEditing;
			System.Windows.Controls.GroupBox groupBox2 = gbxDescriptionOfManualEditing;
			bool isEnabled = groupBox2.IsEnabled = false;
			groupBox.IsEnabled = isEnabled;
			if (scheme.Param.BiserType == BiserType.Any || (Settings.hobbyProgramm == HobbyProgramm.Raskraska && scheme.Param.BiserType == BiserType.ForManufacturers && scheme.Param.IsMixingColors))
			{
				gbxModeOfManualEditing.IsEnabled = true;
			}
			else
			{
				gbxSelectNearestColors.IsEnabled = true;
			}
			if (scheme.Param.BiserType == BiserType.Any || (Settings.hobbyProgramm == HobbyProgramm.Raskraska && scheme.Param.BiserType == BiserType.ForManufacturers && scheme.Param.IsMixingColors))
			{
				try
				{
					Beads beads = (lbxReplaceColor.DataContext as IEnumerable<Beads>).First(delegate(Beads bd)
					{
						System.Drawing.Color clr2 = bd.clr;
						if (clr2.R == c.R)
						{
							clr2 = bd.clr;
							if (clr2.G == c.G)
							{
								clr2 = bd.clr;
								return clr2.B == c.B;
							}
						}
						return false;
					});
					isChanginglbxReplaceColorSelectedItemProgrammatically = true;
					lbxReplaceColor.SelectedItem = beads;
					isChanginglbxReplaceColorSelectedItemProgrammatically = false;
					lbxReplaceColor.ScrollIntoView(beads);
				}
				catch
				{
				}
			}
			else
			{
				List<Beads> list = new List<Beads>();
				System.Drawing.Color clr = System.Drawing.Color.FromArgb(255, c.R, c.G, c.B);
				float num = 3.40282347E+38f;
				Beads beads2 = new Beads();
				int num2 = 50;
				if (num2 > scheme.beadsEnableToUse.Count())
				{
					num2 = scheme.beadsEnableToUse.Count();
				}
				while (list.Count() < num2)
				{
					num = 3.40282347E+38f;
					foreach (Beads item in scheme.beadsEnableToUse)
					{
						float distanceBetweenColors = scheme.GetDistanceBetweenColors(item.clr, clr);
						if (distanceBetweenColors < num && !list.Contains(item))
						{
							num = distanceBetweenColors;
							beads2 = item;
							if (v == 0)
							{
								beads2.name = "****" + beads2.name.Last().ToString();
							}
						}
					}
					list.Add(beads2);
				}
				lbxEnableBeads.DataContext = list;
				if (lbxEnableBeads.Items.Count > 0)
				{
					lbxEnableBeads.SelectedIndex = 0;
				}
				if (lbxEnableBeads.SelectedItem != null)
				{
					gbxModeOfManualEditing.IsEnabled = true;
				}
			}
		}

		private void TbcMain_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			foreach (object item in (IEnumerable)TbcMain.Items)
			{
				TabItem tabItem = item as TabItem;
				Thickness margin = tabItem.Margin;
				int num = (int)margin.Left;
				margin = tabItem.Margin;
				int num2 = (int)margin.Right;
				if (tabItem.IsSelected)
				{
					tabItem.Margin = new Thickness((double)num, -2.0, (double)num2, -1.0);
				}
				else
				{
					tabItem.Margin = new Thickness((double)num, 0.0, (double)num2, 0.0);
				}
			}
		}

		private void slrScaleImageForHandEditing_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (scheme.IsHandEditingSchemeLoaded)
			{
				ChangeSchemeImage_InManualEditing_AtScaleSize();
			}
		}

		private void rbtBrush_Checked(object sender, RoutedEventArgs e)
		{
			System.Windows.Controls.Image image = imgForHandEditing;
			System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
			System.Windows.Input.Cursor cursor3 = image.Cursor = (image2.Cursor = new System.Windows.Input.Cursor(AppDomain.CurrentDomain.BaseDirectory + "Images\\brush.cur"));
			modeOfManualEditing = ModeOfManualEditing.Brush;
			if (imgForHandEditing.IsMouseOver || imgForHandEditingBackAndKnot.IsMouseOver)
			{
				DrawEllipseOfHandEditingBrush_InGUI();
				Ellipse ellipse = elpBrushForHandEditing2;
				Ellipse ellipse2 = elpBrushForHandEditing1;
				Ellipse ellipse3 = elpBrushForHandEditing3;
				Visibility visibility2 = ellipse3.Visibility = Visibility.Visible;
				Visibility visibility4 = ellipse2.Visibility = visibility2;
				ellipse.Visibility = visibility4;
			}
			isLastEditingModeIsFill = false;
		}

		private void rbtFill_Checked(object sender, RoutedEventArgs e)
		{
			System.Windows.Controls.Image image = imgForHandEditing;
			System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
			System.Windows.Input.Cursor cursor3 = image.Cursor = (image2.Cursor = new System.Windows.Input.Cursor(AppDomain.CurrentDomain.BaseDirectory + "Images\\fill.cur"));
			modeOfManualEditing = ModeOfManualEditing.Fill;
			Ellipse ellipse = elpBrushForHandEditing2;
			Ellipse ellipse2 = elpBrushForHandEditing1;
			Ellipse ellipse3 = elpBrushForHandEditing3;
			Visibility visibility2 = ellipse3.Visibility = Visibility.Hidden;
			Visibility visibility4 = ellipse2.Visibility = visibility2;
			ellipse.Visibility = visibility4;
			isLastEditingModeIsFill = true;
		}

		private void imgForHandEditingBackAndKnot_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (!IsInProcess && scheme.IsHandEditingSchemeLoaded)
			{
				if (prevStateBlocksOnHandEditing.Count() < maxEnabledCountHandEditingForSmallVersion)
				{
					if (modeOfManualEditing == ModeOfManualEditing.Pipet)
					{
						cpkColorFromList.SelectedColor = Colors.GhostWhite;
						Block blockOfClickInImageForManualEditing = GetBlockOfClickInImageForManualEditing(e);
						if (blockOfClickInImageForManualEditing != null && blockOfClickInImageForManualEditing.IsVisible)
						{
							System.Drawing.Color clr = blockOfClickInImageForManualEditing.ResBeads.clr;
							System.Windows.Media.Color color = System.Windows.Media.Color.FromArgb(byte.MaxValue, clr.R, clr.G, clr.B);
							if (scheme.Param.BiserType == BiserType.Any || (Settings.hobbyProgramm == HobbyProgramm.Raskraska && scheme.Param.BiserType == BiserType.ForManufacturers && scheme.Param.IsMixingColors))
							{
								tblPipetSelectedColorsFromScheme.Text = "";
							}
							else
							{
								tblPipetSelectedColorsFromScheme.Text = blockOfClickInImageForManualEditing.ResBeads.name;
							}
							tblPipetSelectedColorsFromScheme.Background = new SolidColorBrush(color);
							SetListOfColorForManualEditing(color);
							SetBrushModeDrawingCheckedProgrammatically();
						}
					}
					else if (modeOfManualEditing == ModeOfManualEditing.Fill)
					{
						Beads beadsForHandEditing = GetBeadsForHandEditing();
						if (beadsForHandEditing != (Beads)null)
						{
							System.Windows.Point? blockCoordinateOfClickInImageForManualEditing = GetBlockCoordinateOfClickInImageForManualEditing(e);
							System.Windows.Point value = blockCoordinateOfClickInImageForManualEditing.Value;
							int num = (int)Math.Round(value.X);
							value = blockCoordinateOfClickInImageForManualEditing.Value;
							int num2 = (int)Math.Round(value.Y);
							if (blockCoordinateOfClickInImageForManualEditing.HasValue && scheme.blocksHandEditing[num, num2].IsItFullStitch.Value)
							{
								System.Windows.Input.Cursor cursor = imgForHandEditingBackAndKnot.Cursor;
								System.Windows.Controls.Image image = imgForHandEditing;
								System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
								System.Windows.Input.Cursor cursor3 = image.Cursor = (image2.Cursor = System.Windows.Input.Cursors.Wait);
								Dictionary<Block, Block> dictionary = scheme.FillAreaOnManualEditingScheme(beadsForHandEditing, new System.Drawing.Point
								{
									X = num,
									Y = num2
								});
								List<Block> list = new List<Block>();
								foreach (KeyValuePair<Block, Block> item in dictionary)
								{
									list.Add(item.Key);
								}
								List<Block> list2 = new List<Block>();
								foreach (KeyValuePair<Block, Block> item2 in dictionary)
								{
									list2.Add(item2.Value);
								}
								prevStateBlocksOnHandEditing.Add(new StateSchemesPart
								{
									prevBlocks = list2,
									nextAction = Action.drawingFill
								});
								scheme.DrawBlocksWithNewBeadsOnEmbroideryInHandEditing(list);
								DrawEmbroideryHandEditingOnImage();
								SetComponentOnHandEditingTabForChangedScheme();
								isNeedToRefreshListWithBeadsInPaletteOfHandEditingScheme = true;
								System.Windows.Controls.Image image3 = imgForHandEditing;
								System.Windows.Controls.Image image4 = imgForHandEditingBackAndKnot;
								cursor3 = (image3.Cursor = (image4.Cursor = cursor));
							}
						}
					}
					else if (modeOfManualEditing == ModeOfManualEditing.Brush && cmbTypeOfStitchForDrawing.SelectedIndex == 5)
					{
						SetEndBackStitchPoint(e);
					}
					else if (modeOfManualEditing == ModeOfManualEditing.Brush && cmbTypeOfStitchForDrawing.SelectedIndex == 6)
					{
						SetKnotOnHandEditingScheme(e);
					}
				}
				else
				{
					string text = Local.Get("мастер-версии");
					if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
					{
						text = Local.Get("коммерческой версии");
					}
					WndInfo wndInfo = new WndInfo(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("В данной версии программы Вы можете делать для схемы не более ") + maxEnabledCountHandEditingForSmallVersion.ToString() + Local.Get(" редактирований (заливок, либо нажатий кистью, либо замены цветов) - просто для демонстрации возможностей программы. В ") + text + Local.Get(" это ограничение конечно же снято."));
					wndInfo.ShowDialog();
				}
			}
		}

		private void SetComponentOnHandEditingTabForChangedScheme()
		{
			if (v > 2)
			{
				System.Windows.Controls.Button button = btnSaveSchemeAfterDrawing;
				System.Windows.Controls.Button button2 = btnPrintSchemeAfterDrawing;
				bool isEnabled = button2.IsEnabled = true;
				button.IsEnabled = isEnabled;
			}
			tblMoveToReworkFromHandEditing.IsEnabled = true;
			btnUndoManualEditingScheme.IsEnabled = (prevStateBlocksOnHandEditing.Count() > 0);
		}

		private System.Windows.Point? GetBlockCoordinateOfClickInImageForManualEditing(System.Windows.Input.MouseEventArgs e)
		{
			System.Windows.Point point = (Settings.hobbyProgramm != 0) ? e.GetPosition(imgForHandEditing) : e.GetPosition(imgForHandEditingBackAndKnot);
			int num = (int)(point.Y * (double)scheme.Param.HeightInBead / imgForHandEditing.ActualHeight);
			int num2;
			if (scheme.Param.Type == SchemeType.Normal)
			{
				num2 = (int)(point.X * (double)scheme.Param.WidthInBead / imgForHandEditing.ActualWidth);
			}
			else
			{
				if (num % 2 != 0)
				{
					float num3 = (float)(point.X * (double)scheme.Param.WidthInBead / imgForHandEditing.ActualWidth - 0.5);
					if (!(num3 < 0f) && (int)num3 != scheme.Param.WidthInBead - 1)
					{
						num2 = (int)num3;
						goto IL_0113;
					}
					return null;
				}
				num2 = (int)(point.X * (double)scheme.Param.WidthInBead / imgForHandEditing.ActualWidth);
			}
			goto IL_0113;
			IL_0113:
			return new System.Windows.Point((double)num2, (double)num);
		}

		private Block GetBlockOfClickInImageForManualEditing(System.Windows.Input.MouseEventArgs e)
		{
			System.Windows.Point? blockCoordinateOfClickInImageForManualEditing = GetBlockCoordinateOfClickInImageForManualEditing(e);
			Block result = null;
			if (blockCoordinateOfClickInImageForManualEditing.HasValue)
			{
				Block[,] blocksHandEditing = scheme.blocksHandEditing;
				System.Windows.Point value = blockCoordinateOfClickInImageForManualEditing.Value;
				int num = (int)value.X;
				value = blockCoordinateOfClickInImageForManualEditing.Value;
				result = blocksHandEditing[num, (int)value.Y];
			}
			return result;
		}

		private List<PointF> GetFloatCoordinatesOfPointsOfClickInImageForHandEditing(System.Windows.Input.MouseEventArgs e)
		{
			PointF floatBlockCoordinatesOfMouseClickOnHandEditingImage = GetFloatBlockCoordinatesOfMouseClickOnHandEditingImage(e);
			List<System.Drawing.Point> pointsFromCircle = GetPointsFromCircle(floatBlockCoordinatesOfMouseClickOnHandEditingImage, slrBrushSize.Value);
			return pointsFromCircle.Select(delegate(System.Drawing.Point pt)
			{
				PointF result = default(PointF);
				result.X = (float)pt.X;
				result.Y = (float)pt.Y;
				return result;
			}).ToList();
		}

		private List<System.Drawing.Point> GetIntCoordinatesOfPointsOfClickInImageForHandEditing(System.Windows.Input.MouseEventArgs e)
		{
			PointF floatBlockCoordinatesOfMouseClickOnHandEditingImage = GetFloatBlockCoordinatesOfMouseClickOnHandEditingImage(e);
			return GetPointsFromCircle(floatBlockCoordinatesOfMouseClickOnHandEditingImage, slrBrushSize.Value);
		}

		private PointF GetFloatBlockCoordinatesOfMouseClickOnHandEditingImage(System.Windows.Input.MouseEventArgs e)
		{
			System.Windows.Point position;
			if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				position = e.GetPosition(imgForHandEditingBackAndKnot);
				position.X = position.X / (imgForHandEditingBackAndKnot.Width + 0.0) * (double)((float)scheme.Param.WidthInBead + 0f);
				position.Y = position.Y / (imgForHandEditingBackAndKnot.Height + 0.0) * (double)((float)scheme.Param.HeightInBead + 0f);
			}
			else
			{
				position = e.GetPosition(imgForHandEditing);
				position.X = position.X / (imgForHandEditing.Width + 0.0) * (double)((float)scheme.Param.WidthInBead + 0f);
				position.Y = position.Y / (imgForHandEditing.Height + 0.0) * (double)((float)scheme.Param.HeightInBead + 0f);
			}
			PointF result = default(PointF);
			result.X = (float)position.X;
			result.Y = (float)position.Y;
			return result;
		}

		private Beads GetBeadsForHandEditing()
		{
			Beads result = null;
			if (scheme.Param.BiserType == BiserType.Any || (Settings.hobbyProgramm == HobbyProgramm.Raskraska && scheme.Param.BiserType == BiserType.ForManufacturers && scheme.Param.IsMixingColors))
			{
				if (lastColorOnManualEditing.HasValue)
				{
					System.Windows.Media.Color clr = lastColorOnManualEditing.Value;
					IEnumerable<Beads> source = scheme._resBeadsHandEditing.Where(delegate(Beads bd)
					{
						System.Drawing.Color clr2 = bd.clr;
						if (clr2.R == clr.R)
						{
							clr2 = bd.clr;
							if (clr2.G == clr.G)
							{
								clr2 = bd.clr;
								return clr2.B == clr.B;
							}
						}
						return false;
					});
					result = ((!source.Any()) ? new Beads
					{
						clr = System.Drawing.Color.FromArgb(255, clr.R, clr.G, clr.B)
					} : source.First());
				}
			}
			else if (lbxEnableBeads.Items.Count > 0 && lbxEnableBeads.SelectedValue != null)
			{
				result = (Beads)lbxEnableBeads.SelectedValue;
			}
			return result;
		}

		private void tbiHandEditing_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (scheme.IsHandEditingSchemeLoaded)
			{
				ChangeSchemeImage_InManualEditing_AtScaleSize();
			}
		}

		private void rbtGetColorWithPipet_Click(object sender, RoutedEventArgs e)
		{
			modeOfManualEditing = ModeOfManualEditing.Pipet;
			System.Windows.Controls.Image image = imgForHandEditing;
			System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
			System.Windows.Input.Cursor cursor3 = image.Cursor = (image2.Cursor = new System.Windows.Input.Cursor(AppDomain.CurrentDomain.BaseDirectory + "Images\\pipet.cur"));
		}

		private void rbtBrush_Click(object sender, RoutedEventArgs e)
		{
			gbxDescriptionOfManualEditing.IsEnabled = true;
			modeOfManualEditing = ModeOfManualEditing.Brush;
			System.Windows.Controls.Image image = imgForHandEditing;
			System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
			System.Windows.Input.Cursor cursor3 = image.Cursor = (image2.Cursor = new System.Windows.Input.Cursor(AppDomain.CurrentDomain.BaseDirectory + "Images\\brush.cur"));
		}

		private void rbtFill_Click(object sender, RoutedEventArgs e)
		{
			gbxDescriptionOfManualEditing.IsEnabled = true;
			modeOfManualEditing = ModeOfManualEditing.Fill;
			System.Windows.Controls.Image image = imgForHandEditing;
			System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
			System.Windows.Input.Cursor cursor3 = image.Cursor = (image2.Cursor = new System.Windows.Input.Cursor(AppDomain.CurrentDomain.BaseDirectory + "Images\\fill.cur"));
		}

		private void rbtColorFromList_Click(object sender, RoutedEventArgs e)
		{
			modeOfManualEditing = ModeOfManualEditing.GetColorFromList;
			System.Windows.Controls.Image image = imgForHandEditing;
			System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
			System.Windows.Input.Cursor cursor2 = image.Cursor = (image2.Cursor = System.Windows.Input.Cursors.Arrow);
		}

		private void btnShowSettings_Click(object sender, RoutedEventArgs e)
		{
			if (!IsInProcess)
			{
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					wndSettingsForColoring wndSettingsForColoring = new wndSettingsForColoring(scheme);
					wndSettingsForColoring.ShowDialog();
				}
				else
				{
					wndSettings wndSettings = new wndSettings(scheme, false);
					wndSettings.ShowDialog();
				}
			}
		}

		private void grdManualEditingScheme_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
		}

		private void cpkColorFromList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (grdManualEditingScheme.IsEnabled)
			{
				tblPipetSelectedColorsFromScheme.Text = Local.Get("не выбран");
				tblPipetSelectedColorsFromScheme.Background = new SolidColorBrush(Colors.Transparent);
			}
		}

		private void GroupBox1_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				Image1.IsEnabled = (sender as FrameworkElement).IsEnabled;
			}
			catch (Exception)
			{
			}
		}

		private void CreateSchemeButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				Image2.IsEnabled = (sender as FrameworkElement).IsEnabled;
			}
			catch (Exception)
			{
			}
		}

		private void SaveSchemeButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				System.Windows.Controls.Image image = Image3;
				System.Windows.Controls.Button button = btnOrderConsublesInCreateTab;
				bool isEnabled2 = button.IsEnabled = (sender as FrameworkElement).IsEnabled;
				image.IsEnabled = isEnabled2;
			}
			catch (Exception)
			{
			}
		}

		private void PrintSchemeButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				Image4.IsEnabled = (sender as FrameworkElement).IsEnabled;
			}
			catch (Exception)
			{
			}
		}

		private void gbxReworkingScheme_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				Image5.IsEnabled = (sender as FrameworkElement).IsEnabled;
			}
			catch (Exception)
			{
			}
		}

		private void SaveSchemeReworkButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				System.Windows.Controls.Image image = Image9;
				System.Windows.Controls.Button button = btnOrderConsublesInReworkTab;
				bool isEnabled2 = button.IsEnabled = (sender as FrameworkElement).IsEnabled;
				image.IsEnabled = isEnabled2;
			}
			catch (Exception)
			{
			}
		}

		private void PrintSchemeReworkButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				Image10.IsEnabled = (sender as FrameworkElement).IsEnabled;
			}
			catch (Exception)
			{
			}
		}

		private void groupBox2_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				image13.IsEnabled = (sender as FrameworkElement).IsEnabled;
			}
			catch (Exception)
			{
			}
		}

		private void gbxSelectNearestColors_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				imgNarrowGoToSelectManufacturerColors.IsEnabled = (sender as FrameworkElement).IsEnabled;
			}
			catch (Exception)
			{
			}
		}

		private void gbxModeOfManualEditing_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				image12.IsEnabled = (sender as FrameworkElement).IsEnabled;
			}
			catch (Exception)
			{
			}
		}

		private void gbxDescriptionOfManualEditing_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				image14.IsEnabled = (sender as FrameworkElement).IsEnabled;
			}
			catch (Exception)
			{
			}
		}

		private void btnSaveSchemeAfterDrawing_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				System.Windows.Controls.Image image = image8;
				System.Windows.Controls.Button button = btnOrderConsublesInHandEditingTab;
				bool isEnabled2 = button.IsEnabled = (sender as FrameworkElement).IsEnabled;
				image.IsEnabled = isEnabled2;
			}
			catch (Exception)
			{
			}
		}

		private void btnPrintSchemeAfterDrawing_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				image15.IsEnabled = (sender as FrameworkElement).IsEnabled;
			}
			catch (Exception)
			{
			}
		}

		private void btnUndoReworkingScheme_Click(object sender, RoutedEventArgs e)
		{
			if (prevStateBlocksOnReworking.Count() > 0)
			{
				StateSchemesPart stateSchemesPart = prevStateBlocksOnReworking.Last();
				Beads item = new Beads();
				List<Beads> list = new List<Beads>();
				foreach (Block prevBlock in stateSchemesPart.prevBlocks)
				{
					list.Add(prevBlock.ResBeads);
					if (scheme.blocksRework[prevBlock.X, prevBlock.Y].IsItFullStitch.Value)
					{
						item = scheme.blocksRework[prevBlock.X, prevBlock.Y].ResBeads;
					}
					Block[,] blocksRework = scheme.blocksRework;
					int x = prevBlock.X;
					int y = prevBlock.Y;
					Block copy = prevBlock.GetCopy(false);
					blocksRework[x, y] = copy;
				}
				if (stateSchemesPart.nextAction == Action.addingColor)
				{
					scheme._resBeadsRework.Remove(item);
					foreach (Beads item2 in list)
					{
						if (!scheme._resBeadsRework.Contains(item2))
						{
							scheme._resBeadsRework.Add(item2);
						}
					}
				}
				else if (stateSchemesPart.nextAction == Action.removingColor)
				{
					scheme._resBeadsRework.Add(list.First());
				}
				scheme.RedrawReworkEmbroideryView(stateSchemesPart.prevBlocks);
				DrawEmbroideryReworkOnImage();
				UpdateReworkTabControls(true, true);
				prevStateBlocksOnReworking.RemoveAt(prevStateBlocksOnReworking.Count() - 1);
				btnUndoReworkingScheme.IsEnabled = (prevStateBlocksOnReworking.Count() > 0);
			}
		}

		private void btnUndoManualEditingScheme_Click(object sender, RoutedEventArgs e)
		{
			if (prevStateBlocksOnHandEditing.Count() > 0)
			{
				System.Windows.Input.Cursor cursor = imgForHandEditingBackAndKnot.Cursor;
				System.Windows.Controls.Image image = imgForHandEditing;
				System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
				System.Windows.Input.Cursor cursor3 = image.Cursor = (image2.Cursor = System.Windows.Input.Cursors.Wait);
				StateSchemesPart stateSchemesPart = prevStateBlocksOnHandEditing.Last();
				foreach (Block prevBlock in stateSchemesPart.prevBlocks)
				{
					Block[,] blocksHandEditing = scheme.blocksHandEditing;
					int x = prevBlock.X;
					int y = prevBlock.Y;
					Block copy = prevBlock.GetCopy(false);
					blocksHandEditing[x, y] = copy;
				}
				IEnumerable<Beads> enumerable = (from pb in stateSchemesPart.prevBlocks
				where pb.IsVisible
				select pb into t
				select t.ResBeads).Distinct();
				foreach (Beads item in enumerable)
				{
					if (!scheme._resBeadsHandEditing.Contains(item))
					{
						scheme._resBeadsHandEditing.Add(item);
					}
				}
				scheme.DrawBlocksWithNewBeadsOnEmbroideryInHandEditing(stateSchemesPart.prevBlocks);
				DrawEmbroideryHandEditingOnImage();
				prevStateBlocksOnHandEditing.RemoveAt(prevStateBlocksOnHandEditing.Count() - 1);
				btnUndoManualEditingScheme.IsEnabled = (prevStateBlocksOnHandEditing.Count() > 0);
				System.Windows.Controls.Image image3 = imgForHandEditing;
				System.Windows.Controls.Image image4 = imgForHandEditingBackAndKnot;
				cursor3 = (image3.Cursor = (image4.Cursor = cursor));
				isNeedToRefreshListWithBeadsInPaletteOfHandEditingScheme = true;
			}
			else
			{
				btnUndoManualEditingScheme.IsEnabled = false;
			}
		}

		private void imgForHandEditingBackAndKnot_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (!IsInProcess && scheme.IsHandEditingSchemeLoaded)
			{
				if (modeOfManualEditing == ModeOfManualEditing.Brush && cmbTypeOfStitchForDrawing.SelectedIndex == 5 && e.LeftButton == MouseButtonState.Pressed)
				{
					IsInDrawingBackstich = true;
					SetFirstBackStitchPoint(e);
				}
				else
				{
					MouseMoveBrushInManualEditing_DrawInRealEmbroidery(e);
				}
			}
		}

		private void imgForHandEditingBackAndKnot_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			Ellipse ellipse = elpBrushForHandEditing2;
			Ellipse ellipse2 = elpBrushForHandEditing1;
			Ellipse ellipse3 = elpBrushForHandEditing3;
			Visibility visibility2 = ellipse3.Visibility = Visibility.Hidden;
			Visibility visibility4 = ellipse2.Visibility = visibility2;
			ellipse.Visibility = visibility4;
		}

		private void imgForHandEditingBackAndKnot_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
		{
			DrawEllipseOfHandEditingBrush_InGUI();
			MouseMoveBrushInManualEditing_DrawInRealEmbroidery(e);
			if (!IsInProcess && isNeedToRefreshListWithBeadsInPaletteOfHandEditingScheme && e.LeftButton != MouseButtonState.Pressed)
			{
				UpdateCountColorsAndListToReplaceColorsInHandEditingTab(null);
				isNeedToRefreshListWithBeadsInPaletteOfHandEditingScheme = false;
			}
			if (IsInDrawingBackstich)
			{
				Beads beadsForHandEditing = GetBeadsForHandEditing();
				if (beadsForHandEditing != (Beads)null)
				{
					PointF floatBlockCoordinatesOfMouseClickOnHandEditingImage = GetFloatBlockCoordinatesOfMouseClickOnHandEditingImage(e);
					double num = imgForHandEditingBackAndKnot.ActualWidth / (double)scheme.BmpBackAndKnot.Width;
					float num2 = ((float)scheme.SqureCellSidePreview_X + 0f) * ((float)scheme.BmpBackAndKnot.Width + 0f) / ((float)scheme.BmpBeadworkHandEditing.Width + 0f);
					lineBackstitch.StrokeThickness = (double)num2 * num * 0.25;
					Line line = lineBackstitch;
					System.Drawing.Color clr = beadsForHandEditing.clr;
					byte r = clr.R;
					clr = beadsForHandEditing.clr;
					byte g = clr.G;
					clr = beadsForHandEditing.clr;
					line.Stroke = new SolidColorBrush(System.Windows.Media.Color.FromRgb(r, g, clr.B));
					lineBackstitch.X1 = (double)(scheme.backstitchHandEditing.Last().ptBegin.X * num2) * num;
					lineBackstitch.Y1 = (double)(scheme.backstitchHandEditing.Last().ptBegin.Y * num2) * num;
					lineBackstitch.X2 = (double)(floatBlockCoordinatesOfMouseClickOnHandEditingImage.X * num2) * num;
					lineBackstitch.Y2 = (double)(floatBlockCoordinatesOfMouseClickOnHandEditingImage.Y * num2) * num;
				}
			}
		}

		private void DrawEllipseOfHandEditingBrush_InGUI()
		{
			if (modeOfManualEditing == ModeOfManualEditing.Brush && cmbTypeOfStitchForDrawing.SelectedIndex != 6 && cmbTypeOfStitchForDrawing.SelectedIndex != 5)
			{
				int squreCellSidePreview_X = scheme.SqureCellSidePreview_X;
				double num = slrBrushSize.Value * (double)squreCellSidePreview_X * (double)ScaleSchemeManualEditing * Scale_FOR_PROPORTION_HandEditingScheme;
				double num2 = slrBrushSize.Value * (double)squreCellSidePreview_X * (double)ScaleSchemeManualEditing * Scale_FOR_PROPORTION_HandEditingScheme;
				if (num >= 5.0)
				{
					elpBrushForHandEditing3.Height = num + 2.0;
					elpBrushForHandEditing3.Width = num2 + 2.0;
					elpBrushForHandEditing2.Height = num;
					elpBrushForHandEditing2.Width = num2;
					double num3 = num - 2.0;
					if (num3 < 0.1)
					{
						num3 = 0.1;
					}
					double num4 = num2 - 2.0;
					if (num4 < 0.1)
					{
						num4 = 0.1;
					}
					elpBrushForHandEditing1.Height = num3;
					elpBrushForHandEditing1.Width = num4;
					if (!imgForHandEditing.IsMouseOver && !imgForHandEditingBackAndKnot.IsMouseOver)
					{
						return;
					}
					System.Windows.Point point = (Settings.hobbyProgramm != 0) ? Mouse.GetPosition(imgForHandEditing) : Mouse.GetPosition(imgForHandEditingBackAndKnot);
					point.X -= elpBrushForHandEditing2.Width / 2.0;
					point.Y -= elpBrushForHandEditing2.Height / 2.0;
					elpBrushForHandEditing1.Margin = new Thickness(point.X + 1.0, point.Y + 1.0, 0.0, 0.0);
					elpBrushForHandEditing2.Margin = new Thickness(point.X, point.Y, 0.0, 0.0);
					elpBrushForHandEditing3.Margin = new Thickness(point.X - 1.0, point.Y - 1.0, 0.0, 0.0);
				}
				else
				{
					Ellipse ellipse = elpBrushForHandEditing3;
					Ellipse ellipse2 = elpBrushForHandEditing3;
					Ellipse ellipse3 = elpBrushForHandEditing2;
					Ellipse ellipse4 = elpBrushForHandEditing2;
					Ellipse ellipse5 = elpBrushForHandEditing1;
					Ellipse ellipse6 = elpBrushForHandEditing1;
					double num6 = ellipse6.Width = 0.0;
					double num8 = ellipse5.Height = num6;
					double num10 = ellipse4.Width = num8;
					double num12 = ellipse3.Height = num10;
					double num15 = ellipse.Height = (ellipse2.Width = num12);
				}
			}
			else
			{
				Ellipse ellipse7 = elpBrushForHandEditing3;
				Ellipse ellipse8 = elpBrushForHandEditing3;
				Ellipse ellipse9 = elpBrushForHandEditing2;
				Ellipse ellipse10 = elpBrushForHandEditing2;
				Ellipse ellipse11 = elpBrushForHandEditing1;
				Ellipse ellipse12 = elpBrushForHandEditing1;
				double num6 = ellipse12.Width = 0.0;
				double num8 = ellipse11.Height = num6;
				double num10 = ellipse10.Width = num8;
				double num12 = ellipse9.Height = num10;
				double num15 = ellipse7.Height = (ellipse8.Width = num12);
			}
		}

		private void MouseMoveBrushInManualEditing_DrawInRealEmbroidery(System.Windows.Input.MouseEventArgs e)
		{
			if (!IsInProcess && scheme.IsHandEditingSchemeLoaded && prevStateBlocksOnHandEditing.Count() < maxEnabledCountHandEditingForSmallVersion && modeOfManualEditing == ModeOfManualEditing.Brush && cmbTypeOfStitchForDrawing.SelectedIndex < 5 && e.LeftButton == MouseButtonState.Pressed)
			{
				DrawBrushOnManualEditingScheme(e);
			}
		}

		private void DrawBrushOnManualEditingScheme(System.Windows.Input.MouseEventArgs e)
		{
			IsInProcess = true;
			Beads beadsForHandEditing = GetBeadsForHandEditing();
			if (beadsForHandEditing != (Beads)null)
			{
				List<PointF> list = new List<PointF>();
				List<System.Drawing.Point> list2 = new List<System.Drawing.Point>();
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					list2 = GetIntCoordinatesOfPointsOfClickInImageForHandEditing(e);
				}
				else
				{
					list = ((Settings.hobbyProgramm != 0 || slrBrushSize.Value != 1.0 || (stitchForHandEditing != TypeOfStitch.half && stitchForHandEditing != TypeOfStitch.full_quarter && stitchForHandEditing != TypeOfStitch.half_quarter && stitchForHandEditing != TypeOfStitch.three_qarter)) ? GetFloatCoordinatesOfPointsOfClickInImageForHandEditing(e) : new List<PointF>
					{
						GetFloatBlockCoordinatesOfMouseClickOnHandEditingImage(e)
					});
				}
				if (list.Count() > 0 || list2.Count() > 0)
				{
					Dictionary<Block, Block> dictionary = (Settings.hobbyProgramm != HobbyProgramm.Raskraska) ? scheme.ChangeBlocksInDrawingBrushOnHandEditingScheme_ForNOTRaskraska(list, beadsForHandEditing, (TypeOfStitch)cmbTypeOfStitchForDrawing.SelectedIndex) : scheme.ChangeBlocksInDrawingBrushOnHandEditingScheme_ForRaskraska(list2, beadsForHandEditing);
					if (dictionary.Count() > 0)
					{
						List<Block> prevBlocks = (from r in dictionary
						select r.Value).ToList();
						prevStateBlocksOnHandEditing.Add(new StateSchemesPart
						{
							prevBlocks = prevBlocks,
							nextAction = Action.drawingPen
						});
						List<Block> list3 = (from r in dictionary
						select r.Key).ToList();
						scheme.DrawBlocksWithNewBeadsOnEmbroideryInHandEditing(list3);
						list3.Clear();
						list3 = null;
						DrawEmbroideryHandEditingOnImage();
						SetComponentOnHandEditingTabForChangedScheme();
						isNeedToRefreshListWithBeadsInPaletteOfHandEditingScheme = true;
						prevBlocks = null;
					}
					dictionary.Clear();
					dictionary = null;
					list.Clear();
					list = null;
					list2.Clear();
					list2 = null;
				}
			}
			IsInProcess = false;
		}

		private void imgForHandEditingBackAndKnot_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
		{
			if (modeOfManualEditing == ModeOfManualEditing.Brush)
			{
				Ellipse ellipse = elpBrushForHandEditing2;
				Ellipse ellipse2 = elpBrushForHandEditing1;
				Ellipse ellipse3 = elpBrushForHandEditing3;
				Visibility visibility2 = ellipse3.Visibility = Visibility.Visible;
				Visibility visibility4 = ellipse2.Visibility = visibility2;
				ellipse.Visibility = visibility4;
			}
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			try
			{
				GrdImageForHandEditingBackAndKnot.Children.Add(elpBrushForHandEditing3);
				GrdImageForHandEditingBackAndKnot.Children.Add(elpBrushForHandEditing2);
				GrdImageForHandEditingBackAndKnot.Children.Add(elpBrushForHandEditing1);
			}
			catch (Exception)
			{
			}
			ScvSrcPhotoCreate.MaxHeight = ScvSrcPhotoCreate.ViewportHeight;
			ScvResSchemeCreate.MaxHeight = ScvResSchemeCreate.ViewportHeight;
			ScrollViewer scvSrcPhotoRework = ScvSrcPhotoRework;
			ScrollViewer scvResSchemeRework = ScvResSchemeRework;
			double num3 = scvSrcPhotoRework.MaxHeight = (scvResSchemeRework.MaxHeight = ScvSrcPhotoCreate.MaxHeight - 105.0);
			ScrollViewer scrollViewer = scvForHandEditingBackAndKnot;
			System.Windows.Controls.GroupBox groupBox = gbxReplaceColorInHandEditingScheme;
			num3 = (scrollViewer.MaxHeight = (groupBox.MaxHeight = ScvSrcPhotoCreate.MaxHeight - 53.0));
			//if (RegistryWork.CheckVersionAndCreateItIfNoExist() > 0)
			//{
			//	if (!RegistryWork.CheckShowLicense())
			//	{
			//		if (!Settings.isItVersionWithoutContactInfo)
			//		{
			//			wndLicense wndLicense = new wndLicense(RegistryWork.CheckIsItOldUser());
			//			wndLicense.ShowDialog();
			//		}
			//		RegistryWork.WriteShowLicense(true);
			//	}
			//	else if (RegistryWork.GetCurrentProgramAssemblyVersion() != RegistryWork.GetAssemblyVersionFromRegistry())
			//	{
			//		RegistryWork.WriteAssemblyVersionToRegistry();
			//		if (Settings.VersionInfo != "")
			//		{
			//			wndImprovementsOfNewVersion wndImprovementsOfNewVersion = new wndImprovementsOfNewVersion();
			//			wndImprovementsOfNewVersion.ShowDialog();
			//		}
			//		if (Settings.isNeedToSetSettingsToDefaultValues)
			//		{
			//			App.SetDefaultProgramSettings();
			//		}
			//	}
			//}
		}

		private void rbtBrush_Unchecked(object sender, RoutedEventArgs e)
		{
			CancelDrawingBackstitch();
			Ellipse ellipse = elpBrushForHandEditing2;
			Ellipse ellipse2 = elpBrushForHandEditing1;
			Ellipse ellipse3 = elpBrushForHandEditing3;
			Visibility visibility2 = ellipse3.Visibility = Visibility.Collapsed;
			Visibility visibility4 = ellipse2.Visibility = visibility2;
			ellipse.Visibility = visibility4;
		}

		private void slrBrushSize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			rbtBrush.IsChecked = true;
			if (tbxBrushSizeInPxForRaskraska != null && slrBrushSize != null)
			{
				tbxBrushSizeInPxForRaskraska.Text = ((int)Math.Round(slrBrushSize.Value)).ToString();
			}
		}

		private void tbxBrushSizeInPxForRaskraska_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				int num = Convert.ToInt32(tbxBrushSizeInPxForRaskraska.Text);
				if ((double)num <= slrBrushSize.Minimum)
				{
					slrBrushSize.Value = slrBrushSize.Minimum;
				}
				else if ((double)num >= slrBrushSize.Maximum)
				{
					slrBrushSize.Value = slrBrushSize.Maximum;
				}
				else
				{
					slrBrushSize.Value = (double)num;
				}
			}
			catch
			{
			}
		}

		private void tblLicenseDescription_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			wndLicense wndLicense = new wndLicense(RegistryWork.CheckIsItOldUser());
			wndLicense.ShowDialog();
		}

		private void tblRestrictionsMainTab_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			ShowRestrictionsCurrentVersionWnd();
		}

		private void btnBuyMainTab_Click(object sender, RoutedEventArgs e)
		{
			int num = 1;
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && v == 1)
			{
				num = 2;
			}
			BuyProgram(num);
		}

		private void tblIBoughtMain_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				IBoughtProgram(2);
			}
			else
			{
				IBoughtProgram(1);
			}
		}

		private void tblRestrictionsReworkTab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			ShowRestrictionsCurrentVersionWnd();
		}

		private void btnBuyReworkTab_Click(object sender, RoutedEventArgs e)
		{
			BuyProgram(2);
		}

		private void tblIBoughtRework_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			IBoughtProgram(2);
		}

		private void tblRestrictionsHandEditingTab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			ShowRestrictionsCurrentVersionWnd();
		}

		private void btnBuyHandEditingTab_Click(object sender, RoutedEventArgs e)
		{
			BuyProgram(3);
		}

		private void tblIBoughtHandEditing_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			IBoughtProgram(3);
		}

		private void IBoughtProgram(int v)
		{
			string text = RegistryWork.CheckEmail();
			if (text.Length > 0)
			{
				OrderInfo infoAboutOrder = Site.GetInfoAboutOrder(text);
				if (infoAboutOrder.isOkSendedToServer)
				{
					RegistryWork.WriteIsEmailSendedOnServer(true);
					RegistryWork.WriteVersion(infoAboutOrder.v);
					Settings.v = infoAboutOrder.v;
					if (!infoAboutOrder.isActivatingLimit && infoAboutOrder.dtEnd.HasValue)
					{
						RegistryWork.WriteDateTimeExpired(infoAboutOrder.dtEnd);
					}
					if (infoAboutOrder.v > 0)
					{
						RegistryWork.WriteIsUnexpired(false);
						if (infoAboutOrder.v >= v)
						{
							string str = "";
							if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
							{
								switch (infoAboutOrder.v)
								{
								case 1:
									str = "ДОМАШНЕЙ";
									break;
								case 2:
									str = "ПРОФЕССИОНАЛЬНОЙ";
									break;
								case 3:
									str = "КОММЕРЧЕСКОЙ";
									break;
								}
							}
							else
							{
								switch (infoAboutOrder.v)
								{
								case 1:
									str = "БАЗОВОЙ";
									break;
								case 2:
									str = "ПРОДВИНУТОЙ";
									break;
								case 3:
									str = "МАСТЕР";
									break;
								}
							}
							if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
							{
								str = "полной";
							}
							WndInfo wndInfo = new WndInfo("Поздравляем с приобретением " + str + " версии программы! Надеемся, что программа придётся Вам по душе. Если у Вас появятся любые вопросы - всегда пишите нам через вкладку 'Есть вопрос?' в программе, либо напрямую на эл.почту " + Settings.siteAndEmailSettings.supportEmail);
							wndInfo.ShowDialog();
							WndMain wndMain = new WndMain();
							wndMain.Closing += wnd_Closing;
							wndMain.Show();
							base.Hide();
						}
						else
						{
							WndInfo wndInfo2 = new WndInfo("На указанный Вами e-mail (" + text + ") есть оплата, но для более простой версии программы. Если это неверно - пожалуйста, укажите иной e-mail (в следующем окне), либо свяжитесь с нами по эл.почте " + Settings.siteAndEmailSettings.supportEmail);
							wndInfo2.ShowDialog();
							wndStart wndStart = new wndStart(true);
							wndStart.Closing += wnd_Closing;
							wndStart.Show();
							base.Hide();
						}
					}
					else
					{
						string info = "Нет оплаты на указанный Вами e-mail (" + text + "). Укажите иной e-mail (на который есть оплаченный заказ), либо выполните оплату для этого e-mail'а. Если это ошибка - свяжитесь с нами по эл.почте " + Settings.siteAndEmailSettings.supportEmail;
						if (infoAboutOrder.isActivatingLimit)
						{
							info = Local.Get("Превышено число активаций программы на данный адрес электронной почты (") + text + Local.Get(").\r\nУкажите иной e-mail (на который есть оплаченный заказ).\r\n\r\nЕсли это ошибка (например, у Вас на компьютере работала оплаченная версия, срок оплаты которой НЕ истёк, и вдруг появилось это окно; или Вы сменили компьютер/переустановили Windows/и т.п.) - ОБЯЗАТЕЛЬНО свяжитесь с нами по эл.почте ") + Settings.siteAndEmailSettings.supportEmail + Local.Get("\r\nМы бесплатно сделаем для Вас новую активацию программы.");
						}
						else if (infoAboutOrder.isExpiredPayedOrder)
						{
							RegistryWork.WriteIsUnexpired(true);
							info = "Оплаченный период пользования программой для этого адреса электронной почты (" + text + ") истёк. Укажите иной e-mail (на который есть оплаченный заказ), либо продлите оплату для данного e-mail'а. Если это ошибка - свяжитесь с нами по эл.почте " + Settings.siteAndEmailSettings.supportEmail;
						}
						WndInfo wndInfo3 = new WndInfo(info);
						wndInfo3.ShowDialog();
						wndStart wndStart2 = new wndStart(true);
						wndStart2.Closing += wnd_Closing;
						wndStart2.Show();
						base.Hide();
					}
				}
				else
				{
					WndInfo wndInfo4 = new WndInfo("Возникли помехи связи при проверке оплаты. Пожалуйста, проверьте подключение к интернету и повторите попытку, либо скопируйте информацию об ошибке (см.текст ниже) и пришлите нам на эл.почту " + Settings.siteAndEmailSettings.supportEmail, infoAboutOrder.exception);
					wndInfo4.ShowDialog();
				}
			}
			else
			{
				WndInfo wndInfo5 = new WndInfo("Пожалуйста, укажите Ваш e-mail (который Вы указывали при оплате) в следующем окне для проверки оплаты");
				wndInfo5.ShowDialog();
				wndStart wndStart3 = new wndStart(true);
				wndStart3.Closing += wnd_Closing;
				wndStart3.Show();
				base.Hide();
			}
		}

		private void wnd_Closing(object sender, CancelEventArgs e)
		{
			base.Close();
		}

		private void ShowRestrictionsCurrentVersionWnd()
		{
			WndResctrictionsCurrentVersion wndResctrictionsCurrentVersion = new WndResctrictionsCurrentVersion(v);
			wndResctrictionsCurrentVersion.ShowDialog();
		}

		private void BuyProgram(int v)
		{
			Site.BuyProgram(v);
		}

		private void button1_Click(object sender, RoutedEventArgs e)
		{
			RegistryWork.DeleteAll();
		}

		private void Window_Activated(object sender, EventArgs e)
		{
			WaitingWindow.MaximizeWindow();
		}

		private void Window_Deactivated(object sender, EventArgs e)
		{
			WaitingWindow.MinimizeWindow();
		}

		private void CommandCancelRework_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (!IsInProcess)
			{
				btnUndoReworkingScheme_Click(new object(), new RoutedEventArgs());
			}
		}

		private void CommandCancelHandEditing_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (!IsInProcess)
			{
				btnUndoManualEditingScheme_Click(new object(), new RoutedEventArgs());
			}
		}

		private void CommandAboutInVersionWithoutContactInfo_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			bool isItVersionWithoutContactInfo = Settings.isItVersionWithoutContactInfo;
			Settings.isItVersionWithoutContactInfo = false;
			WndAbout wndAbout = new WndAbout();
			wndAbout.ShowDialog();
			Settings.isItVersionWithoutContactInfo = isItVersionWithoutContactInfo;
		}

		private void CommandImageBiggerHandEditingScheme_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			ChangeScaleOfImage(slrScaleImageForHandEditing, 1);
		}

		private void CommandImageSmallerHandEditingScheme_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			ChangeScaleOfImage(slrScaleImageForHandEditing, -1);
		}

		private void CommandImageBiggerReworkScheme_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			ChangeScaleOfImage(ScaleResSchemeReworkSlider, 1);
		}

		private void CommandImageSmallerReworkScheme_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			ChangeScaleOfImage(ScaleResSchemeReworkSlider, -1);
		}

		private void CommandImageBiggerSrcScheme_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			ChangeScaleOfImage(ScaleResSchemeCreateSlider, 1);
		}

		private void CommandImageSmallerSrcScheme_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			ChangeScaleOfImage(ScaleResSchemeCreateSlider, -1);
		}

		private void ChangeScaleByMouseWheel(Slider slr, MouseWheelEventArgs e)
		{
			e.Handled = true;
			ChangeScaleOfImage(slr, e.Delta);
		}

		private void ChangeScaleOfImage(Slider slr, int changeD)
		{
			int num = (int)slr.Maximum / 70;
			if (changeD > 0)
			{
				if (slr.Value + (double)num < slr.Maximum)
				{
					slr.Value += (double)num;
				}
				else
				{
					slr.Value = slr.Maximum;
				}
			}
			else if (slr.Value - (double)num > 0.0)
			{
				slr.Value -= (double)num;
			}
			else
			{
				slr.Value = 0.0;
			}
		}

		private void scvForHandEditingBackAndKnot_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			ChangeScaleByMouseWheel(slrScaleImageForHandEditing, e);
		}

		private void ScvResSchemeRework_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			ChangeScaleByMouseWheel(ScaleResSchemeReworkSlider, e);
		}

		private void ScvSrcPhotoRework_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			ChangeScaleByMouseWheel(ScaleSrcPhotoReworkSlider, e);
		}

		private void ScvResSchemeCreate_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			ChangeScaleByMouseWheel(ScaleResSchemeCreateSlider, e);
		}

		private void ScvSrcPhotoCreate_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			ChangeScaleByMouseWheel(ScaleSrcPhotoCreateSlider, e);
		}

		private void ScaleImage_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			ChangeScaleByMouseWheel(sender as Slider, e);
		}

		private void tblMoveToReworkFromCreate_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if ((sender as TextBlock).IsEnabled)
			{
				imgReworkScheme.Source = null;
				scheme.MoveToReworkSchemeFromCreate();
				DrawEmbroideryReworkOnImage();
				UpdateReworkTabControls(true, false);
				DrawBitmapOnWPFImage(scheme.BmpSourceImage, SrcPhotoReworkImage, ImageFormat.Jpeg);
				ChangeSrcImage_InRework_AtScaleSize();
			}
		}

		private void tblMoveToReworkFromHandEditing_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if ((sender as TextBlock).IsEnabled)
			{
				imgReworkScheme.Source = null;
				scheme.MoveToReworkSchemeFromHandEditing();
				DrawEmbroideryReworkOnImage();
				UpdateReworkTabControls(true, false);
				ChangeSrcImage_InRework_AtScaleSize();
			}
		}

		private void tblMoveToHandEditingFromCreate_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if ((sender as TextBlock).IsEnabled)
			{
				System.Windows.Controls.Image image = imgForHandEditing;
				System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
				ImageSource imageSource3 = image.Source = (image2.Source = null);
				IsInProcess = true;
				WaitingWindow.ChangeTextInWindow(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Пожалуйста, подождите, пока картина перенесётся на эту вкладку"));
				WaitingWindow.ShowWindow();
				BackgroundWorker backgroundWorker = new BackgroundWorker();
				backgroundWorker.DoWork += bwLoadSchemeOnHandEdiitngTabFromCreate_DoWork;
				backgroundWorker.RunWorkerCompleted += bwLoadSchemeOnHandEdiitngTabFromCreate_RunWorkerCompleted;
				backgroundWorker.RunWorkerAsync();
			}
		}

		private void bwLoadSchemeOnHandEdiitngTabFromCreate_DoWork(object sender, DoWorkEventArgs e)
		{
			scheme.MoveToHandEditingSchemeFromCreate();
		}

		private void bwLoadSchemeOnHandEdiitngTabFromCreate_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			UpdateHandEditingTab(true, false);
			if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				DrawBitmapOnWPFImage(scheme.BmpBackAndKnot, imgForHandEditingBackAndKnot, ImageFormat.Png);
			}
			UpdateCountColorsAndListToReplaceColorsInHandEditingTab(true);
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				ClearFirstTabInRaskraskaAfterMOvingSchemeOnHandEditingTab();
			}
			WaitingWindow.HideWindow();
			IsInProcess = false;
		}

		private void ClearFirstTabInRaskraskaAfterMOvingSchemeOnHandEditingTab()
		{
			imgCreateScheme.Source = null;
			tblSchemeNotCreatedOnFirstTab.Visibility = Visibility.Visible;
			System.Windows.Controls.Button saveSchemeButton = SaveSchemeButton;
			System.Windows.Controls.Button printSchemeButton = PrintSchemeButton;
			TextBlock textBlock = tblMoveToHandEditingFromCreate;
			bool flag2 = textBlock.IsEnabled = false;
			bool isEnabled = printSchemeButton.IsEnabled = flag2;
			saveSchemeButton.IsEnabled = isEnabled;
			tblCountOfCountours.Text = "";
			TblSchemeSourceAndCountColors.Text = Settings.nameOfWork + ": ";
		}

		private void tblMoveToHandEditingFromRework_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if ((sender as TextBlock).IsEnabled)
			{
				System.Windows.Controls.Image image = imgForHandEditing;
				System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
				ImageSource imageSource3 = image.Source = (image2.Source = null);
				scheme.MoveToHandEditingSchemeFromRework();
				UpdateHandEditingTab(true, false);
				if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
				{
					DrawBitmapOnWPFImage(scheme.BmpBackAndKnot, imgForHandEditingBackAndKnot, ImageFormat.Png);
				}
				UpdateCountColorsAndListToReplaceColorsInHandEditingTab(null);
			}
		}

		private void DrawBitmapOnWPFImage(Bitmap bmpSrc, System.Windows.Controls.Image img, ImageFormat imageFormat)
		{
			MemoryStream memoryStream = new MemoryStream();
			bmpSrc.Save(memoryStream, imageFormat);
			memoryStream.Position = 0L;
			BitmapImage bitmapImage = new BitmapImage();
			bitmapImage.BeginInit();
			bitmapImage.StreamSource = memoryStream;
			bitmapImage.EndInit();
			img.Source = bitmapImage;
		}

		private void UpdateHandEditingTab(bool isSchemeLoaded, bool isSchemeChanged)
		{
			try
			{
				if (isSchemeLoaded)
				{
					DrawEmbroideryHandEditingOnImage();
					prevStateBlocksOnHandEditing.Clear();
					tblIsSchemeToManualEditingEnabled.Visibility = Visibility.Collapsed;
					UpdateCountColorsAndListToReplaceColorsInHandEditingTab(null);
				}
				else
				{
					System.Windows.Controls.Image image = imgForHandEditing;
					System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
					ImageSource imageSource3 = image.Source = (image2.Source = null);
					tblIsSchemeToManualEditingEnabled.Visibility = Visibility.Visible;
				}
				rbtGetColorWithPipet.IsChecked = true;
				tblPipetSelectedColorsFromScheme.Text = Local.Get("не выбран");
				tblPipetSelectedColorsFromScheme.Background = new SolidColorBrush(Colors.Transparent);
				tbxBeadsFilterOnSelectForHandEditing.Text = "";
				cpkColorFromList.SelectedColor = Colors.GhostWhite;
				System.Windows.Controls.GroupBox groupBox = gbxSelectNearestColors;
				System.Windows.Controls.GroupBox groupBox2 = gbxModeOfManualEditing;
				System.Windows.Controls.GroupBox groupBox3 = gbxDescriptionOfManualEditing;
				bool flag2 = groupBox3.IsEnabled = false;
				bool isEnabled = groupBox2.IsEnabled = flag2;
				groupBox.IsEnabled = isEnabled;
				Grid grid = grdManualEditingScheme;
				System.Windows.Controls.GroupBox groupBox4 = gbxReplaceColorInHandEditingScheme;
				isEnabled = (groupBox4.IsEnabled = isSchemeLoaded);
				grid.IsEnabled = isEnabled;
				if (v == 0 && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					gbxReplaceColorInHandEditingScheme.IsEnabled = false;
				}
				System.Windows.Controls.ListBox listBox = lbxReplaceColor;
				System.Windows.Controls.ListBox listBox2 = lbxEnableBeads;
				object obj3 = listBox.DataContext = (listBox2.DataContext = null);
				System.Windows.Controls.Button button = btnSaveSchemeAfterDrawing;
				System.Windows.Controls.Button button2 = btnPrintSchemeAfterDrawing;
				isEnabled = (button2.IsEnabled = isSchemeChanged);
				button.IsEnabled = isEnabled;
				btnUndoManualEditingScheme.IsEnabled = (prevStateBlocksOnHandEditing.Count() > 0 & isSchemeChanged);
				btnUndoBackstitchInHandEditing.IsEnabled = (scheme.backstitchHandEditing.Count() > 0 & isSchemeChanged);
				btnUndoKnotInHandEditing.IsEnabled = (scheme.knotHandEditing.Count() > 0 & isSchemeChanged);
				isEnabledToSelectColorOnManualEditing = isSchemeLoaded;
				UpdateMaximumAndCurrentValueOnSlidersOfSewingBrushAndHandEditingBrush();
			}
			catch (Exception)
			{
			}
		}

		private void DrawEmbroideryHandEditingOnImage()
		{
			if (scheme.BmpBeadworkHandEditing != null)
			{
				if (wbmpRework != null)
				{
					wbmpRework = null;
				}
				if (wbmpHandEditing == null)
				{
					wbmpHandEditing = new WriteableBitmap(scheme.BmpBeadworkHandEditing.Width, scheme.BmpBeadworkHandEditing.Height, 96.0, 96.0, PixelFormats.Bgr32, null);
				}
				FastDrawBitmapOnWPFImage(scheme.BmpBeadworkHandEditing, imgForHandEditing, wbmpHandEditing);
				ChangeSchemeImage_InManualEditing_AtScaleSize();
			}
			else
			{
				System.Windows.Controls.Image image = imgForHandEditing;
				System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
				ImageSource imageSource3 = image.Source = (image2.Source = null);
			}
		}

		private void tblMoveToHandEditingFromRework_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (tblMoveToHandEditingFromRework.IsEnabled)
			{
				tblMoveToHandEditingFromRework.IsEnabled = (v == 3);
			}
		}

		private void UpdateCountColorsAndListToReplaceColorsInHandEditingTab(bool? _isNeedToRefreshListWithBeadsInPaletteOfHandEditingScheme_withPercentInRaskraska = default(bool?))
		{
			try
			{
				if (v != 0 || Settings.hobbyProgramm != HobbyProgramm.Raskraska)
				{
					lbxReplaceColor.ContextMenu = null;
					System.Windows.Controls.ContextMenu contextMenu = new System.Windows.Controls.ContextMenu();
					System.Windows.Controls.MenuItem menuItem = new System.Windows.Controls.MenuItem
					{
						Header = ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Заменить на ближайший цвет из схемы")
					};
					menuItem.Click += tblChangeColorOnNearestColorInSCheme_HandEditingTab_Click;
					contextMenu.Items.Add(menuItem);
					if (scheme.Param.BiserType != 0 && (Settings.hobbyProgramm != HobbyProgramm.Raskraska || scheme.Param.BiserType != BiserType.ForManufacturers || !scheme.Param.IsMixingColors))
					{
						System.Windows.Controls.MenuItem menuItem2 = new System.Windows.Controls.MenuItem
						{
							Header = "Заменить на похожий цвет:"
						};
						menuItem2.SubmenuOpened += mi_changeColorOnColorFromListOfNearest_handEditingTab_SubmenuOpened;
						System.Windows.Controls.MenuItem newItem = new System.Windows.Controls.MenuItem
						{
							Header = " "
						};
						menuItem2.Items.Add(newItem);
						contextMenu.Items.Add(menuItem2);
						System.Windows.Controls.MenuItem menuItem3 = new System.Windows.Controls.MenuItem();
						StackPanel stackPanel = new StackPanel
						{
							Orientation = System.Windows.Controls.Orientation.Horizontal
						};
						TextBlock element = new TextBlock
						{
							Text = "Заменить на цвет, фильтр по названию: ",
							Margin = new Thickness(0.0, 2.0, 0.0, 0.0)
						};
						stackPanel.Children.Add(element);
						System.Windows.Controls.TextBox textBox = new System.Windows.Controls.TextBox
						{
							Width = 80.0
						};
						textBox.TextChanged += tbxFilterBeadsByNameInContextMenu_TextChanged;
						textBox.Tag = menuItem3;
						stackPanel.Children.Add(textBox);
						menuItem3.Header = stackPanel;
						menuItem3.SubmenuOpened += mi_BeadsFilteredByName;
						System.Windows.Controls.MenuItem newItem2 = new System.Windows.Controls.MenuItem
						{
							Header = " "
						};
						menuItem3.Items.Add(newItem2);
						contextMenu.Items.Add(menuItem3);
					}
					System.Windows.Controls.MenuItem menuItem4 = new System.Windows.Controls.MenuItem
					{
						Header = "Подсветить цвет"
					};
					menuItem4.Click += tblShowColorOnHandEdiitngScheme_Click;
					contextMenu.Items.Add(menuItem4);
					lbxReplaceColor.ContextMenu = contextMenu;
					List<Beads> list = new List<Beads>();
					List<Block> list2;
					if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
					{
						list2 = new List<Block>(scheme.blocksHandEditing.GetLength(0) * scheme.blocksHandEditing.GetLength(1));
						Block[,] blocksHandEditing = scheme.blocksHandEditing;
						foreach (Block item in blocksHandEditing)
						{
							list2.Add(item);
						}
					}
					else
					{
						list2 = (from Block b in scheme.blocksHandEditing
						where b != null
						select b).ToList();
					}
					float num = 100f / ((float)list2.Count() + 0f);
					for (int k = 0; k < scheme._resBeadsHandEditing.Count(); k++)
					{
						Beads beads = new Beads();
						Beads bb = scheme._resBeadsHandEditing[k];
						int? nullable = null;
						if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
						{
							if (_isNeedToRefreshListWithBeadsInPaletteOfHandEditingScheme_withPercentInRaskraska.HasValue && _isNeedToRefreshListWithBeadsInPaletteOfHandEditingScheme_withPercentInRaskraska.Value)
							{
								float num2 = ((float)list2.Count((Block bl) => bl.ResBeads == bb) + 0f) * num;
								nullable = ((!(num2 > 0f) || !(num2 < 1f)) ? new int?((int)Math.Round((double)num2)) : new int?(1));
							}
						}
						else
						{
							nullable = ((Settings.hobbyProgramm != 0) ? new int?(list2.Count(delegate(Block bl)
							{
								if (bl.IsVisible)
								{
									return bl.ResBeads == bb;
								}
								return false;
							})) : new int?(list2.Count(delegate(Block bl)
							{
								if (bl.IsVisible)
								{
									if (bl.IsItFullStitch.Value && bl.ResBeads == bb)
									{
										return true;
									}
									if ((!bl.IsItFullStitch).Value)
									{
										if (bl.leftTop != null && bl.leftTop.typeOfStitch != TypeOfStitch.emptyCanvas && bl.leftTop.ResBeads == bb)
										{
											goto IL_0121;
										}
										if (bl.rightTop != null && bl.rightTop.typeOfStitch != TypeOfStitch.emptyCanvas && bl.rightTop.ResBeads == bb)
										{
											goto IL_0121;
										}
										if (bl.rightBottom != null && bl.rightBottom.typeOfStitch != TypeOfStitch.emptyCanvas && bl.rightBottom.ResBeads == bb)
										{
											goto IL_0121;
										}
										if (bl.leftBottom != null && bl.leftBottom.typeOfStitch != TypeOfStitch.emptyCanvas)
										{
											return bl.leftBottom.ResBeads == bb;
										}
										return false;
									}
									return false;
								}
								return false;
								IL_0121:
								return true;
							})));
						}
						if (nullable.HasValue && nullable == 0)
						{
							scheme._resBeadsHandEditing.RemoveAt(k);
							k--;
						}
						else
						{
							if (nullable.HasValue)
							{
								beads.countBeadsOfThisColor = nullable.Value;
							}
							Beads beads2 = beads;
							System.Drawing.Color clr = bb.clr;
							byte r = clr.R;
							clr = bb.clr;
							byte g = clr.G;
							clr = bb.clr;
							beads2.clr = System.Drawing.Color.FromArgb(255, r, g, clr.B);
							if (scheme.Param.BiserType == BiserType.Any || (Settings.hobbyProgramm == HobbyProgramm.Raskraska && scheme.Param.BiserType == BiserType.ForManufacturers && scheme.Param.IsMixingColors))
							{
								beads.parentSeries = new BeadsSeries
								{
									parentManufacturer = new BeadsManufacturer
									{
										name = ""
									},
									name = ""
								};
								beads.name = "";
							}
							else
							{
								beads.parentSeries = new BeadsSeries
								{
									parentManufacturer = new BeadsManufacturer
									{
										name = bb.parentSeries.parentManufacturer.name
									},
									name = bb.parentSeries.name
								};
								if (v == 0)
								{
									beads.name = "****" + bb.name.Last().ToString();
								}
								else
								{
									beads.name = bb.name;
								}
							}
							list.Add(beads);
						}
					}
					lbxReplaceColor.DataContext = from b in list
					orderby b.countBeadsOfThisColor descending
					select b;
					tblCountColorsAfterManualEditing.Text = string.Format(Settings.nameOfWork + Local.Get(" из") + " {0} {1}:", scheme.CountResColorsHandEditing, AllString.GetCountColorString(scheme.CountResColorsHandEditing));
				}
			}
			catch
			{
			}
		}

		private void tblShowColorOnHandEdiitngScheme_Click(object sender, RoutedEventArgs e)
		{
			if (scheme.IsHandEditingSchemeLoaded && lbxReplaceColor.SelectedItem != null)
			{
				Bitmap bitmap = scheme.HighlightColorInHandEditingScheme(lbxReplaceColor.SelectedItem as Beads);
				if (wbmpRework != null)
				{
					wbmpRework = null;
				}
				if (wbmpHandEditing == null)
				{
					wbmpHandEditing = new WriteableBitmap(bitmap.Width, bitmap.Height, 96.0, 96.0, PixelFormats.Bgr32, null);
				}
				FastDrawBitmapOnWPFImage(bitmap, imgForHandEditing, wbmpHandEditing);
				bitmap.Dispose();
				ChangeSchemeImage_InManualEditing_AtScaleSize();
			}
		}

		private void tbxFilterBeadsByNameInContextMenu_TextChanged(object sender, TextChangedEventArgs e)
		{
			txt = (sender as System.Windows.Controls.TextBox).Text.ToLower();
			mi_BeadsFilteredByName((sender as System.Windows.Controls.TextBox).Tag as System.Windows.Controls.MenuItem, new RoutedEventArgs());
		}

		private void mi_BeadsFilteredByName(object sender, RoutedEventArgs e)
		{
			if (lbxReplaceColor.SelectedItem != null && scheme.Param.BiserType == BiserType.ForManufacturers)
			{
				List<Beads> list = (from bd in scheme.beadsEnableToUse
				where bd.name.ToLower().Contains(txt)
				select bd).ToList();
				System.Windows.Controls.MenuItem menuItem = sender as System.Windows.Controls.MenuItem;
				menuItem.Items.Clear();
				for (int i = 0; i < list.Count(); i++)
				{
					Beads beads = list[i];
					StackPanel stackPanel = new StackPanel
					{
						Orientation = System.Windows.Controls.Orientation.Horizontal
					};
					TextBlock element = new TextBlock
					{
						Text = beads.parentSeries.parentManufacturer.name,
						Padding = new Thickness(3.0),
						Width = 95.0
					};
					TextBlock element2 = new TextBlock
					{
						Text = beads.name,
						Padding = new Thickness(3.0),
						Width = 50.0
					};
					TextBlock element3 = new TextBlock
					{
						Width = 40.0,
						Height = 20.0,
						Background = beads.brushForWPF,
						Padding = new Thickness(3.0)
					};
					stackPanel.Children.Add(element);
					stackPanel.Children.Add(element2);
					stackPanel.Children.Add(element3);
					if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
					{
						TextBlock element4 = new TextBlock
						{
							Text = " = " + beads.countBeadsOfThisColor.ToString(),
							Padding = new Thickness(3.0)
						};
						stackPanel.Children.Add(element4);
					}
					stackPanel.Tag = beads;
					stackPanel.MouseLeftButtonUp += changeColorOnColorFromFilterTbx_HandEditingTab_Menu_MouseLeftButtonUp;
					menuItem.Items.Add(stackPanel);
				}
			}
		}

		private void tblChangeColorOnNearestColorInSCheme_HandEditingTab_Click(object sender, RoutedEventArgs e)
		{
			if (!IsInProcess && scheme.IsHandEditingSchemeLoaded)
			{
				if (prevStateBlocksOnHandEditing.Count() < maxEnabledCountHandEditingForSmallVersion)
				{
					if (lbxReplaceColor.Items.Count > 1)
					{
						if (lbxReplaceColor.SelectedItem != null)
						{
							System.Windows.Input.Cursor cursor = imgForHandEditingBackAndKnot.Cursor;
							System.Windows.Controls.Image image = imgForHandEditing;
							System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
							System.Windows.Input.Cursor cursor3 = image.Cursor = (image2.Cursor = System.Windows.Input.Cursors.Wait);
							Beads beads = lbxReplaceColor.SelectedItem as Beads;
							Beads resBeads = new Beads();
							float num = 3.40282347E+38f;
							for (int i = 0; i < scheme._resBeadsHandEditing.Count(); i++)
							{
								if (beads != scheme._resBeadsHandEditing[i])
								{
									float distanceBetweenColors = scheme.GetDistanceBetweenColors(beads.clr, scheme._resBeadsHandEditing[i].clr);
									if (distanceBetweenColors < num)
									{
										num = distanceBetweenColors;
										resBeads = scheme._resBeadsHandEditing[i];
									}
								}
							}
							List<Block> list = scheme.ReplaceColorOnHandEditingScheme(beads, resBeads);
							if (list.Count() > 0)
							{
								prevStateBlocksOnHandEditing.Add(new StateSchemesPart
								{
									prevBlocks = list,
									nextAction = Action.removingColor
								});
							}
							DrawEmbroideryHandEditingOnImage();
							SetComponentOnHandEditingTabForChangedScheme();
							UpdateCountColorsAndListToReplaceColorsInHandEditingTab(null);
							System.Windows.Controls.Image image3 = imgForHandEditing;
							System.Windows.Controls.Image image4 = imgForHandEditingBackAndKnot;
							cursor3 = (image3.Cursor = (image4.Cursor = cursor));
						}
					}
					else
					{
						System.Windows.MessageBox.Show(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("Нельзя удалить из схемы последний цвет"), "Внимание!");
					}
				}
				else
				{
					WndInfo wndInfo = new WndInfo(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("В данной версии программы Вы можете делать для схемы не более ") + maxEnabledCountHandEditingForSmallVersion.ToString() + ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt(" редактирований (заливок, нажатий кистью либо замены цветов) - просто для демонстрации возможностей программы. В мастер-версии это ограничение снято."));
					wndInfo.ShowDialog();
				}
			}
		}

		private void mi_changeColorOnColorFromListOfNearest_handEditingTab_SubmenuOpened(object sender, RoutedEventArgs e)
		{
			if (lbxReplaceColor.SelectedItem != null && scheme.Param.BiserType == BiserType.ForManufacturers)
			{
				List<Beads> list = new List<Beads>();
				Beads selBEads = lbxReplaceColor.SelectedItem as Beads;
				System.Drawing.Color clr = selBEads.clr;
				float num = 3.40282347E+38f;
				Beads beads = new Beads();
				int num2 = 10;
				if (num2 > scheme.beadsEnableToUse.Count())
				{
					num2 = scheme.beadsEnableToUse.Count();
				}
				System.Windows.Controls.MenuItem menuItem = sender as System.Windows.Controls.MenuItem;
				menuItem.Items.Clear();
				IEnumerable<Beads> enumerable = from bc in scheme.beadsEnableToUse
				where bc != selBEads
				select bc;
				IEnumerable<Block> source = from Block b in scheme.blocksHandEditing
				where b != null
				select b;
				while (menuItem.Items.Count < num2)
				{
					num = 3.40282347E+38f;
					foreach (Beads item in enumerable)
					{
						float distanceBetweenColors = scheme.GetDistanceBetweenColors(item.clr, clr);
						if (distanceBetweenColors < num && !list.Contains(item))
						{
							num = distanceBetweenColors;
							beads = item;
							if (v == 0)
							{
								beads.name = "****" + beads.name.Last().ToString();
							}
							if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
							{
								beads.countBeadsOfThisColor = source.Count(delegate(Block bl)
								{
									if (bl.IsVisible)
									{
										return bl.ResBeads == item;
									}
									return false;
								});
							}
						}
					}
					list.Add(beads);
					StackPanel stackPanel = new StackPanel
					{
						Orientation = System.Windows.Controls.Orientation.Horizontal
					};
					TextBlock element = new TextBlock
					{
						Text = beads.parentSeries.parentManufacturer.name,
						Padding = new Thickness(3.0),
						Width = 95.0
					};
					TextBlock element2 = new TextBlock
					{
						Text = beads.name,
						Padding = new Thickness(3.0),
						Width = 50.0
					};
					TextBlock element3 = new TextBlock
					{
						Width = 40.0,
						Height = 20.0,
						Background = beads.brushForWPF,
						Padding = new Thickness(3.0)
					};
					stackPanel.Children.Add(element);
					stackPanel.Children.Add(element2);
					stackPanel.Children.Add(element3);
					if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
					{
						TextBlock element4 = new TextBlock
						{
							Text = " = " + beads.countBeadsOfThisColor.ToString(),
							Padding = new Thickness(3.0)
						};
						stackPanel.Children.Add(element4);
					}
					stackPanel.Tag = beads;
					stackPanel.MouseLeftButtonUp += changeColorOnColorFromFilterTbx_HandEditingTab_Menu_MouseLeftButtonUp;
					menuItem.Items.Add(stackPanel);
				}
			}
		}

		private void changeColorOnColorFromFilterTbx_HandEditingTab_Menu_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (lbxReplaceColor.SelectedItem != null && !IsInProcess && scheme.IsHandEditingSchemeLoaded)
			{
				if (prevStateBlocksOnHandEditing.Count() < maxEnabledCountHandEditingForSmallVersion)
				{
					System.Windows.Input.Cursor cursor = imgForHandEditingBackAndKnot.Cursor;
					System.Windows.Controls.Image image = imgForHandEditing;
					System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
					System.Windows.Input.Cursor cursor3 = image.Cursor = (image2.Cursor = System.Windows.Input.Cursors.Wait);
					Beads srcBeads = lbxReplaceColor.SelectedItem as Beads;
					Beads resBeads = (sender as StackPanel).Tag as Beads;
					List<Block> list = scheme.ReplaceColorOnHandEditingScheme(srcBeads, resBeads);
					if (list.Count() > 0)
					{
						prevStateBlocksOnHandEditing.Add(new StateSchemesPart
						{
							prevBlocks = list,
							nextAction = Action.removingColor
						});
					}
					DrawEmbroideryHandEditingOnImage();
					SetComponentOnHandEditingTabForChangedScheme();
					UpdateCountColorsAndListToReplaceColorsInHandEditingTab(null);
					System.Windows.Controls.Image image3 = imgForHandEditing;
					System.Windows.Controls.Image image4 = imgForHandEditingBackAndKnot;
					cursor3 = (image3.Cursor = (image4.Cursor = cursor));
				}
				else
				{
					WndInfo wndInfo = new WndInfo(ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt("В данной версии программы Вы можете делать для схемы не более ") + maxEnabledCountHandEditingForSmallVersion.ToString() + ChangeStringSchemeOrEmbroideryOnColoringIfNeeded_AndLocalizeAfterIt(" редактирований (заливок, нажатий кистью либо замены цветов) - просто для демонстрации возможностей программы. В мастер-версии это ограничение снято."));
					wndInfo.ShowDialog();
				}
			}
		}

		private void btnUndoManualEditingScheme_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			if (isNeedToRefreshListWithBeadsInPaletteOfHandEditingScheme)
			{
				UpdateCountColorsAndListToReplaceColorsInHandEditingTab(null);
				isNeedToRefreshListWithBeadsInPaletteOfHandEditingScheme = false;
			}
		}

		private void btnShowTiming_Click(object sender, RoutedEventArgs e)
		{
			System.Windows.MessageBox.Show(Timing.GetResult());
		}

		private void tblProlongForOneYear_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			Site.BuyProgram(null);
		}

		private void RbtSimpleSelectingColors_Checked(object sender, RoutedEventArgs e)
		{
			if (trvCatalog != null)
			{
				trvCatalog.IsEnabled = false;
			}
		}

		private void RbtPreciosaSelectingColors_Checked(object sender, RoutedEventArgs e)
		{
			if (trvCatalog != null)
			{
				trvCatalog.IsEnabled = true;
			}
		}

		private void tblEditorOfMyPalette_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (!IsInProcess)
			{
				wndEditorOfMyPalette wndEditorOfMyPalette = new wndEditorOfMyPalette();
				wndEditorOfMyPalette.ShowDialog();
				if (wndEditorOfMyPalette.isNeedToUpdateColorsInMainWindow)
				{
					LoadCatalogueOfManufacturers();
				}
			}
		}

		private void SaveSelectedBeads_ToFile()
		{
			string text = "";
			foreach (Node node in Nodes)
			{
				foreach (Node child in node.Children)
				{
					foreach (Node child2 in child.Children)
					{
						Beads beads = child2.Tag as Beads;
						if (child2.IsChecked.HasValue && child2.IsChecked == true)
						{
							System.Drawing.Color clr;
							if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
							{
								object[] obj = new object[13]
								{
									text,
									beads.parentSeries.parentManufacturer.name,
									"|",
									beads.parentSeries.name,
									"|",
									beads.name,
									"|",
									null,
									null,
									null,
									null,
									null,
									null
								};
								clr = beads.clr;
								obj[7] = clr.R;
								obj[8] = "|";
								clr = beads.clr;
								obj[9] = clr.G;
								obj[10] = "|";
								clr = beads.clr;
								obj[11] = clr.B;
								obj[12] = "\r\n";
								text = string.Concat(obj);
							}
							else
							{
								object[] obj2 = new object[9]
								{
									text,
									beads.name,
									"|",
									null,
									null,
									null,
									null,
									null,
									null
								};
								clr = beads.clr;
								obj2[3] = clr.R;
								obj2[4] = "|";
								clr = beads.clr;
								obj2[5] = clr.G;
								obj2[6] = "|";
								clr = beads.clr;
								obj2[7] = clr.B;
								obj2[8] = "\r\n";
								text = string.Concat(obj2);
							}
						}
					}
				}
			}
			string value = Cryption.Encrypt(text, "JSjl289;s)", "Skasdf#9", "Aasfml(sdfln5$D{", "SHA1", 2, 256);
			using (StreamWriter streamWriter = new StreamWriter(Settings.GetPathToFileInStorage("CatalogState.txt")))
			{
				streamWriter.Write(value);
			}
		}

		private List<Beads> LoadSelectedBeadsOnLastClosingProgram_FromFile()
		{
			List<Beads> list = new List<Beads>();
			string pathToFileInStorage = Settings.GetPathToFileInStorage("CatalogState.txt");
			try
			{
				if (File.Exists(pathToFileInStorage))
				{
					string cipherText = FileWorker.ReadFileToEnd(pathToFileInStorage, true, null);
					string[] array = Cryption.Decrypt(cipherText, "JSjl289;s)", "Skasdf#9", "Aasfml(sdfln5$D{", "SHA1", 2, 256).Split(new string[1]
					{
						"\r\n"
					}, StringSplitOptions.RemoveEmptyEntries);
					string[] array2 = array;
					foreach (string text in array2)
					{
						string[] array3 = text.Split(new string[1]
						{
							"|"
						}, StringSplitOptions.RemoveEmptyEntries);
						if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
						{
							list.Add(new Beads
							{
								parentSeries = new BeadsSeries
								{
									name = array3[1],
									parentManufacturer = new BeadsManufacturer
									{
										name = array3[0]
									}
								},
								name = array3[2],
								clr = System.Drawing.Color.FromArgb(255, Convert.ToByte(array3[3]), Convert.ToByte(array3[4]), Convert.ToByte(array3[5]))
							});
						}
						else
						{
							list.Add(new Beads
							{
								name = array3[0],
								clr = System.Drawing.Color.FromArgb(255, Convert.ToByte(array3[1]), Convert.ToByte(array3[2]), Convert.ToByte(array3[3]))
							});
						}
					}
					return list;
				}
				return list;
			}
			catch (Exception)
			{
				return list;
			}
		}

		private void tbxBeadsFilterOnSelectForHandEditing_TextChanged(object sender, TextChangedEventArgs e)
		{
			FillLbxWithBeadsForHandEditingAndFilterItIfNeed();
		}

		private void rbtColorFromName_Checked(object sender, RoutedEventArgs e)
		{
			modeOfManualEditing = ModeOfManualEditing.GetColorFromName;
			System.Windows.Controls.Image image = imgForHandEditing;
			System.Windows.Controls.Image image2 = imgForHandEditingBackAndKnot;
			System.Windows.Input.Cursor cursor2 = image.Cursor = (image2.Cursor = System.Windows.Input.Cursors.Arrow);
			tbxBeadsFilterOnSelectForHandEditing.IsEnabled = true;
			FillLbxWithBeadsForHandEditingAndFilterItIfNeed();
			gbxSelectNearestColors.IsEnabled = true;
		}

		private void FillLbxWithBeadsForHandEditingAndFilterItIfNeed()
		{
			if (modeOfManualEditing == ModeOfManualEditing.GetColorFromName)
			{
				List<Beads> list = new List<Beads>();
				if (tbxBeadsFilterOnSelectForHandEditing.Text.Length > 0)
				{
					string name = tbxBeadsFilterOnSelectForHandEditing.Text.ToLower();
					list = (from bd in scheme.beadsEnableToUse
					where bd.name.ToLower().Contains(name)
					select bd).ToList();
				}
				else
				{
					list = scheme.beadsEnableToUse.ToList();
				}
				lbxEnableBeads.DataContext = list;
				if (lbxEnableBeads.Items.Count > 0)
				{
					lbxEnableBeads.SelectedIndex = 0;
					gbxModeOfManualEditing.IsEnabled = true;
				}
			}
		}

		private void rbtColorFromName_Unchecked(object sender, RoutedEventArgs e)
		{
			tbxBeadsFilterOnSelectForHandEditing.IsEnabled = false;
		}

		private void lbxEnableBeads_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (lbxEnableBeads.SelectedItem != null && lbxReplaceColor.Items.Count > 0)
			{
				lbxEnableBeads.ScrollIntoView(lbxEnableBeads.SelectedItem);
				isChanginglbxReplaceColorSelectedItemProgrammatically = true;
				if (lbxReplaceColor.Items.Contains(lbxEnableBeads.SelectedItem as Beads))
				{
					lbxReplaceColor.SelectedItem = (lbxEnableBeads.SelectedItem as Beads);
					lbxReplaceColor.ScrollIntoView(lbxEnableBeads.SelectedItem as Beads);
				}
				else
				{
					lbxReplaceColor.SelectedIndex = -1;
				}
				isChanginglbxReplaceColorSelectedItemProgrammatically = false;
				SetBrushModeDrawingCheckedProgrammatically();
			}
		}

		private void cbxTypeOfStitchForDrawing_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			CancelDrawingBackstitch();
			stitchForHandEditing = (TypeOfStitch)cmbTypeOfStitchForDrawing.SelectedIndex;
			slrBrushSize.IsEnabled = (stitchForHandEditing != TypeOfStitch.back && stitchForHandEditing != TypeOfStitch.junction);
		}

		private void cmbTypeOfStitchForDrawing_DropDownOpened(object sender, EventArgs e)
		{
			rbtBrush.IsChecked = true;
		}

		private void SetFirstBackStitchPoint(MouseButtonEventArgs e)
		{
			Beads beadsForHandEditing = GetBeadsForHandEditing();
			if (beadsForHandEditing != (Beads)null)
			{
				IsInDrawingBackstich = true;
				lineBackstitch.Visibility = Visibility.Visible;
				PointF floatBlockCoordinatesOfMouseClickOnHandEditingImage = GetFloatBlockCoordinatesOfMouseClickOnHandEditingImage(e);
				scheme.AddBeginBackstitchPoint(floatBlockCoordinatesOfMouseClickOnHandEditingImage, beadsForHandEditing);
				DrawBitmapOnWPFImage(scheme.BmpBackAndKnot, imgForHandEditingBackAndKnot, ImageFormat.Png);
			}
		}

		private void SetEndBackStitchPoint(MouseButtonEventArgs e)
		{
			Beads beadsForHandEditing = GetBeadsForHandEditing();
			if (beadsForHandEditing != (Beads)null && scheme.IsItAnyNotEndedBackstitch())
			{
				PointF floatBlockCoordinatesOfMouseClickOnHandEditingImage = GetFloatBlockCoordinatesOfMouseClickOnHandEditingImage(e);
				scheme.AddEndBackstitchPoint(floatBlockCoordinatesOfMouseClickOnHandEditingImage);
				DrawBitmapOnWPFImage(scheme.BmpBackAndKnot, imgForHandEditingBackAndKnot, ImageFormat.Png);
				btnUndoBackstitchInHandEditing.IsEnabled = true;
				SetComponentOnHandEditingTabForChangedScheme();
				CancelDrawingBackstitch();
			}
		}

		private void btnUndoBackstitchInHandEditing_Click(object sender, RoutedEventArgs e)
		{
			scheme.RemoveLastBackstitch();
			DrawBitmapOnWPFImage(scheme.BmpBackAndKnot, imgForHandEditingBackAndKnot, ImageFormat.Png);
			btnUndoBackstitchInHandEditing.IsEnabled = (scheme.backstitchHandEditing.Count() > 0);
		}

		private void SetKnotOnHandEditingScheme(MouseButtonEventArgs e)
		{
			Beads beadsForHandEditing = GetBeadsForHandEditing();
			if (beadsForHandEditing != (Beads)null)
			{
				PointF floatBlockCoordinatesOfMouseClickOnHandEditingImage = GetFloatBlockCoordinatesOfMouseClickOnHandEditingImage(e);
				scheme.AddKnot(floatBlockCoordinatesOfMouseClickOnHandEditingImage, beadsForHandEditing);
				DrawBitmapOnWPFImage(scheme.BmpBackAndKnot, imgForHandEditingBackAndKnot, ImageFormat.Png);
				btnUndoKnotInHandEditing.IsEnabled = true;
				SetComponentOnHandEditingTabForChangedScheme();
			}
		}

		private void btnUndoKnotInHandEditing_Click(object sender, RoutedEventArgs e)
		{
			scheme.RemoveLastKnot();
			DrawBitmapOnWPFImage(scheme.BmpBackAndKnot, imgForHandEditingBackAndKnot, ImageFormat.Png);
			btnUndoKnotInHandEditing.IsEnabled = (scheme.knotHandEditing.Count() > 0);
		}

		private void lineBackstitch_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			imgForHandEditingBackAndKnot_MouseLeftButtonUp(new object(), e);
		}

		private void scvForHandEditingBackAndKnot_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			CancelDrawingBackstitch();
		}

		private void CancelDrawingBackstitch()
		{
			IsInDrawingBackstich = false;
			lineBackstitch.Visibility = Visibility.Collapsed;
			Line line = lineBackstitch;
			Line line2 = lineBackstitch;
			Line line3 = lineBackstitch;
			Line line4 = lineBackstitch;
			double num2 = line4.Y2 = -1.0;
			double num4 = line3.Y1 = num2;
			double num7 = line.X1 = (line2.X2 = num4);
			if (scheme.IsItAnyNotEndedBackstitch())
			{
				scheme.backstitchHandEditing.Remove(scheme.backstitchHandEditing.Last());
			}
		}

		private void wbrOtherProgram_Navigating(object sender, NavigatingCancelEventArgs e)
		{
			if (isInfoAboutOtherProgramLoaded)
			{
				Process.Start(e.Uri.ToString());
				e.Cancel = true;
			}
		}

		private void cbxIsNotChangeSourceImageInColoring_Checked(object sender, RoutedEventArgs e)
		{
			Slider slider = slrMediana;
			Slider slider2 = slrNumberOfMedianaRepeats;
			bool isEnabled = slider2.IsEnabled = false;
			slider.IsEnabled = isEnabled;
		}

		private void cbxIsNotChangeSourceImageInColoring_Unchecked(object sender, RoutedEventArgs e)
		{
			Slider slider = slrMediana;
			Slider slider2 = slrNumberOfMedianaRepeats;
			bool isEnabled = slider2.IsEnabled = true;
			slider.IsEnabled = isEnabled;
		}

		private void lbxReplaceColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (!isChanginglbxReplaceColorSelectedItemProgrammatically && lbxReplaceColor.SelectedItem != null)
			{
				System.Drawing.Color clr = (lbxReplaceColor.SelectedItem as Beads).clr;
				cpkColorFromList.SelectedColor = System.Windows.Media.Color.FromRgb(clr.R, clr.G, clr.B);
				SetBrushModeDrawingCheckedProgrammatically();
			}
		}

		private void SetBrushModeDrawingCheckedProgrammatically()
		{
			if (!rbtColorFromName.IsChecked.Value)
			{
				if (!isLastEditingModeIsFill)
				{
					rbtBrush.IsChecked = true;
					Ellipse ellipse = elpBrushForHandEditing2;
					Ellipse ellipse2 = elpBrushForHandEditing1;
					Ellipse ellipse3 = elpBrushForHandEditing3;
					Visibility visibility2 = ellipse3.Visibility = Visibility.Visible;
					Visibility visibility4 = ellipse2.Visibility = visibility2;
					ellipse.Visibility = visibility4;
				}
				else
				{
					rbtFill.IsChecked = true;
				}
			}
		}

		private void cpkColorFromList_GotFocus(object sender, RoutedEventArgs e)
		{
			rbtColorFromList.IsChecked = true;
		}

		private void btnRefreshPercentageOfUsingColorInRaskraska_Click(object sender, RoutedEventArgs e)
		{
			base.Cursor = System.Windows.Input.Cursors.Wait;
			UpdateCountColorsAndListToReplaceColorsInHandEditingTab(true);
			base.Cursor = System.Windows.Input.Cursors.Arrow;
		}

		private void AddConsublesOrder(string email, string content)
		{
			base.Cursor = System.Windows.Input.Cursors.Wait;
			bool flag = false;
			try
			{
				ServicesClient servicesClient = new ServicesClient(Settings.ShortProgramName);
				flag = servicesClient.AddConsumablesOrder(email, content);
				servicesClient.Close();
			}
			catch (Exception exc)
			{
				Site.SendExceptionInfo(exc, false);
			}
			if (flag)
			{
				WndInfo wndInfo = new WndInfo("Спасибо за Ваш заказ! Мы ответим Вам в течение суток. (P.S.: пожалуйста, не забудьте сохранить эту схему на Ваш компьютер через кнопку 'Сохранить')");
				wndInfo.ShowDialog();
			}
			else
			{
				WndInfo wndInfo2 = new WndInfo("К сожалению, не удалось отправить Ваш заказ. Проверьте соединение с интернетом, либо отправьте заказ вручную, для этого сохраните все файлы схемы в архив и пришлите её на нашу почту " + Settings.siteAndEmailSettings.supportEmail);
				wndInfo2.ShowDialog();
			}
			base.Cursor = System.Windows.Input.Cursors.Arrow;
		}

		private void btnOrderConsublesInCreateTab_Click(object sender, RoutedEventArgs e)
		{
			DoConsumblesOrder(1);
		}

		private void btnOrderConsublesInReworkTab_Click(object sender, RoutedEventArgs e)
		{
			DoConsumblesOrder(2);
		}

		private void btnOrderConsublesInHandEditingTab_Click(object sender, RoutedEventArgs e)
		{
			DoConsumblesOrder(3);
		}

		private void DoConsumblesOrder(int tab)
		{
			try
			{
				bool flag;
				string text;
				if (!IsInProcess)
				{
					if (v > 0)
					{
						if (scheme.Param.BiserType == BiserType.Any)
						{
							System.Windows.MessageBox.Show("Приносим извинения, но заказ расходных материалов к созданной схеме возможен только при создании схемы под производителей " + Settings.materials + ", не при 'Простом подборе цветов'. Пожалуйста, пересоздайте схему с подбором цветов производителей " + Settings.materials + " - тогда возможность заказа расходных материалов станет доступна.", "Недоступно при 'Простом подборе цветов'");
							goto end_IL_0000;
						}
						flag = true;
						if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && scheme.Param.IsMixingColors)
						{
							flag = false;
							System.Windows.MessageBox.Show("Приносим извинения, но заказ расходных материалов к созданной картине возможен только при создании картины БЕЗ подбора пропорций смешения цветов. Пожалуйста, выключите функцию смешения в 'Важных настройках' и пересоздайте картину - тогда возможность заказа расходных материалов к картине станет доступна.", "Недоступно при подборе смешения цветов");
						}
						else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
						{
							text = "";
							if (!scheme.Param.isSquareStraz)
							{
								flag = false;
								text = " квадратных страз (НЕ круглых!); ";
							}
							if (scheme.Param.HorizontalSizeOfBlockAtSchemeInMm != 2.5f || scheme.Param.VerticalSizeOfBlockAtSchemeInMm != 2.5f)
							{
								flag = false;
								text += " страз размером 2.5мм (размер страз Вы изменили в окне 'Прочих настроек'); ";
							}
							if (tab == 1 && scheme._resBeadsMain.Any((Beads stz) => stz.parentSeries.parentManufacturer.name != "DMC"))
							{
								goto IL_01a4;
							}
							if (tab == 2 && scheme._resBeadsRework.Any((Beads stz) => stz.parentSeries.parentManufacturer.name != "DMC"))
							{
								goto IL_01a4;
							}
							if (tab == 3 && scheme._resBeadsHandEditing.Any((Beads stz) => stz.parentSeries.parentManufacturer.name != "DMC"))
							{
								goto IL_01a4;
							}
							goto IL_01b2;
						}
						goto IL_01de;
					}
					System.Windows.MessageBox.Show("Приносим извинения, но заказ расходных материалов к созданной схеме возможен только в оплаченной версии программы", "Недоступно в тестовой версии");
				}
				goto end_IL_0000;
				IL_01a4:
				flag = false;
				text += " страз DMC, а у Вас в стразах присутствуют оттенки из 'Моя палитра' (пересоздайте схему, убрав галочки с этой палитры); ";
				goto IL_01b2;
				IL_01de:
				if (flag)
				{
					WndAddConsumblesOrder wndAddConsumblesOrder = new WndAddConsumblesOrder();
					bool? nullable = wndAddConsumblesOrder.ShowDialog();
					if (nullable.HasValue && nullable.Value)
					{
						if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && tab == 3)
						{
							base.Cursor = System.Windows.Input.Cursors.Wait;
							UpdateCountColorsAndListToReplaceColorsInHandEditingTab(true);
							base.Cursor = System.Windows.Input.Cursors.Arrow;
						}
						string text2 = "E-mail заказа (не для ответа, просто адрес, на который была оформлена программа): " + RegistryWork.CheckEmail() + ";<br />Вкладка в программе, с которой был сделан заказ: " + tab.ToString() + ";<br />Параметры схемы: " + scheme.Param.WidthInBead.ToString() + " x " + scheme.Param.HeightInBead.ToString() + " " + Settings.nameOfOneBeads + ", ";
						if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
						{
							string[] obj = new string[5]
							{
								text2,
								null,
								null,
								null,
								null
							};
							float num = (float)scheme.Param.WidthInBead / 4f;
							obj[1] = num.ToString();
							obj[2] = " x ";
							num = (float)scheme.Param.HeightInBead / 4f;
							obj[3] = num.ToString();
							obj[4] = " см, ";
							text2 = string.Concat(obj);
						}
						int num2;
						if (Settings.hobbyProgramm != HobbyProgramm.Raskraska && Settings.hobbyProgramm != HobbyProgramm.Almaz)
						{
							string str = text2;
							num2 = scheme.Param.beadsOrCanvasSize;
							text2 = str + " размер канвы/бисера/пряжи " + num2.ToString() + ", ";
						}
						if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
						{
							text2 = text2 + scheme.Param.WidthColoringInSm + " x " + scheme.Param.HeightColoringInSm + " см, ";
						}
						switch (tab)
						{
						case 1:
						{
							string str3 = text2;
							num2 = scheme.CountResColorsSource;
							text2 = str3 + num2.ToString() + " цв.<br /><br />Цвета:<br /> ";
							IOrderedEnumerable<Beads> orderedEnumerable2 = from rb in scheme._resBeadsMain
							orderby rb.parentSeries.parentManufacturer.name, rb.parentSeries.name
							select rb;
							foreach (Beads item in orderedEnumerable2)
							{
								text2 = text2 + item.parentSeries.parentManufacturer.name + "_" + item.parentSeries.name + "_" + item.name + " <br /> ";
							}
							text2 += "<br /><br />";
							break;
						}
						case 2:
						{
							string str4 = text2;
							num2 = scheme.CountResColorsRework;
							text2 = str4 + num2.ToString() + " цв.<br /><br />Цвета: <br />";
							IOrderedEnumerable<Beads> orderedEnumerable3 = from rb in scheme._resBeadsRework
							orderby rb.parentSeries.parentManufacturer.name, rb.parentSeries.name
							select rb;
							foreach (Beads item2 in orderedEnumerable3)
							{
								text2 = text2 + item2.parentSeries.parentManufacturer.name + "_" + item2.parentSeries.name + "_" + item2.name + " <br /> ";
							}
							text2 += "<br /><br />";
							break;
						}
						default:
						{
							string str2 = text2;
							num2 = scheme.CountResColorsHandEditing;
							text2 = str2 + num2.ToString() + " цв.<br /><br />Цвета: <br />";
							IOrderedEnumerable<Beads> orderedEnumerable = from rb in scheme._resBeadsHandEditing
							orderby rb.parentSeries.parentManufacturer.name, rb.parentSeries.name
							select rb;
							foreach (Beads item3 in orderedEnumerable)
							{
								text2 = text2 + item3.parentSeries.parentManufacturer.name + "_" + item3.parentSeries.name + "_" + item3.name + " <br /> ";
							}
							text2 += "<br /><br />";
							if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
							{
								text2 += "Нити бэкстича: <br />";
								IEnumerable<Beads> enumerable = (from b in scheme.backstitchHandEditing
								select b.bead).Distinct();
								foreach (Beads item4 in enumerable)
								{
									text2 = text2 + item4.parentSeries.parentManufacturer.name + "_" + item4.parentSeries.name + "_" + item4.name + " <br /> ";
								}
								text2 += "<br /> ";
								text2 += "Нити узелков: <br />";
								enumerable = (from b in scheme.knotHandEditing
								select b.bead).Distinct();
								foreach (Beads item5 in enumerable)
								{
									text2 = text2 + item5.parentSeries.parentManufacturer.name + "_" + item5.parentSeries.name + "_" + item5.name + " <br /> ";
								}
							}
							break;
						}
						}
						AddConsublesOrder(wndAddConsumblesOrder.email, text2);
					}
				}
				goto end_IL_0000;
				IL_01b2:
				if (!flag)
				{
					System.Windows.MessageBox.Show("Приносим извинения, но заказ расходных материалов к созданной схеме возможен только для " + text.Substring(0, text.Length - 2) + ". Пожалуйста, исправьте эти недочёты и пересоздайте схему заново - заказ материалов к схеме станет доступен!", "Пересоздайте схему с другими настройками");
				}
				goto IL_01de;
				end_IL_0000:;
			}
			catch (Exception exc)
			{
				Site.SendExceptionInfo(exc, false);
				System.Windows.MessageBox.Show("Произошла ошибка формирования заказа. Пожалуйста, напишите нам об этом на нашу почту " + Settings.siteAndEmailSettings.supportEmail, "Ошибка!");
			}
		}

		private void btnDoPreprocessingSourcePhoto_Click(object sender, RoutedEventArgs e)
		{
			wndProcessingPhotos wndProcessingPhotos = new wndProcessingPhotos(processingSourceBmp);
			bool? nullable = wndProcessingPhotos.ShowDialog();
			if (nullable.HasValue && nullable.Value)
			{
				SetTabsStateBeforeSourceImageLoading();
				CreateNewScheme(null);
				if (processingSourceBmp != null)
				{
					processingSourceBmp.Dispose();
				}
				processingSourceBmp = new Bitmap(wndProcessingPhotos.bmpSrc);
				scheme.LoadSourceImage(scheme.pathToSrcImage, processingSourceBmp);
				wndProcessingPhotos.bmpSrc.Dispose();
				SetTabsStateImmediatelyAfterSourceImageLoading();
			}
		}

		private void Window_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
		{
			try
			{
				if (e.SystemKey == Key.LeftAlt && v == 3)
				{
					if (rbtGetColorWithPipet.IsChecked.Value)
					{
						if (isLastEditingModeIsFill && rbtFill.IsEnabled)
						{
							rbtFill.IsChecked = true;
						}
						if (!isLastEditingModeIsFill && rbtBrush.IsEnabled)
						{
							rbtBrush.IsChecked = true;
						}
					}
					else if (rbtFill.IsChecked.Value || rbtBrush.IsChecked.Value)
					{
						rbtGetColorWithPipet.IsChecked = true;
					}
				}
			}
			catch
			{
			}
		}
		

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IStyleConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 2)
			{
				((System.Windows.Controls.CheckBox)target).PreviewMouseLeftButtonDown += OnMouseLeftButtonDown;
			}
		}
	}
}
