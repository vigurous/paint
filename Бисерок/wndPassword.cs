using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Media;

namespace Бисерок
{
	public partial class wndPassword : Window, IComponentConnector
	{
		public wndPassword()
		{
			InitializeComponent();
		}

		private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Paint.bin");

            if (!File.Exists(path))
                return;
            
            var expectedPw = Encoding.UTF8.GetString(Convert.FromBase64String(File.ReadAllText(path)));

            if (this.txtPassword.Password == expectedPw)
            {
				base.DialogResult = true;
			}
            else
            {
                this.txtPassword.BorderBrush = Brushes.Red;
                this.txtPassword.Password = "";
            }
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = false;
		}
	}
}
