using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace Бисерок
{
	public static class Cryption
	{
		public static string Encrypt(string plainText, string password, string salt, string initialVector, string hashAlgorithm = "SHA1", int passwordIterations = 2, int keySize = 256)
		{
			if (string.IsNullOrEmpty(plainText))
			{
				return "";
			}
			byte[] bytes = Encoding.ASCII.GetBytes(initialVector);
			byte[] bytes2 = Encoding.ASCII.GetBytes(salt);
			byte[] bytes3 = Encoding.UTF8.GetBytes(plainText);
			PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(password, bytes2, hashAlgorithm, passwordIterations);
			byte[] bytes4 = passwordDeriveBytes.GetBytes(keySize / 8);
			RijndaelManaged rijndaelManaged = new RijndaelManaged();
			rijndaelManaged.Mode = CipherMode.CBC;
			byte[] inArray = null;
			using (ICryptoTransform transform = rijndaelManaged.CreateEncryptor(bytes4, bytes))
			{
				using (MemoryStream memoryStream = new MemoryStream())
				{
					using (CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write))
					{
						cryptoStream.Write(bytes3, 0, bytes3.Length);
						cryptoStream.FlushFinalBlock();
						inArray = memoryStream.ToArray();
						memoryStream.Close();
						cryptoStream.Close();
					}
				}
			}
			rijndaelManaged.Clear();
			return Convert.ToBase64String(inArray);
		}

		public static string Decrypt(string cipherText, string password, string salt, string initialVector, string hashAlgorithm = "SHA1", int passwordIterations = 2, int keySize = 256)
		{
			if (string.IsNullOrEmpty(cipherText))
			{
				return "";
			}
			byte[] bytes = Encoding.ASCII.GetBytes(initialVector);
			byte[] bytes2 = Encoding.ASCII.GetBytes(salt);
			byte[] array = Convert.FromBase64String(cipherText);
			PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(password, bytes2, hashAlgorithm, passwordIterations);
			byte[] bytes3 = passwordDeriveBytes.GetBytes(keySize / 8);
			RijndaelManaged rijndaelManaged = new RijndaelManaged();
			rijndaelManaged.Mode = CipherMode.CBC;
			byte[] array2 = new byte[array.Length];
			int count = 0;
			using (ICryptoTransform transform = rijndaelManaged.CreateDecryptor(bytes3, bytes))
			{
				using (MemoryStream memoryStream = new MemoryStream(array))
				{
					using (CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Read))
					{
						count = cryptoStream.Read(array2, 0, array2.Length);
						memoryStream.Close();
						cryptoStream.Close();
					}
				}
			}
			rijndaelManaged.Clear();
			return Encoding.UTF8.GetString(array2, 0, count);
		}

		public static string DecryptFromSite(string cipherText, string password, string salt, string initialVector, string hashAlgorithm = "SHA1", int passwordIterations = 2, int keySize = 256)
		{
			if (string.IsNullOrEmpty(cipherText))
			{
				return "";
			}
			byte[] bytes = Encoding.ASCII.GetBytes(initialVector);
			byte[] bytes2 = Encoding.ASCII.GetBytes(salt);
			List<string> list = cipherText.Split(new char[1]
			{
				' '
			}, StringSplitOptions.RemoveEmptyEntries).ToList();
			byte[] array = new byte[list.Count()];
			for (int i = 0; i < list.Count(); i++)
			{
				array[i] = Convert.ToByte(list[i]);
			}
			PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(password, bytes2, hashAlgorithm, passwordIterations);
			byte[] bytes3 = passwordDeriveBytes.GetBytes(keySize / 8);
			RijndaelManaged rijndaelManaged = new RijndaelManaged();
			rijndaelManaged.Mode = CipherMode.CBC;
			byte[] array2 = new byte[array.Length];
			int count = 0;
			using (ICryptoTransform transform = rijndaelManaged.CreateDecryptor(bytes3, bytes))
			{
				using (MemoryStream memoryStream = new MemoryStream(array))
				{
					using (CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Read))
					{
						count = cryptoStream.Read(array2, 0, array2.Length);
						memoryStream.Close();
						cryptoStream.Close();
					}
				}
			}
			rijndaelManaged.Clear();
			return Encoding.UTF8.GetString(array2, 0, count);
		}

		public static string GetBitmapHashCode(string pathToBmp)
		{
			Bitmap bitmap = new Bitmap(pathToBmp);
			BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
			int num = (bitmap.PixelFormat == (bitmap.PixelFormat | PixelFormat.Alpha)) ? 4 : 3;
			int num2 = bitmap.Width * bitmap.Height * num;
			byte[] array = new byte[num2];
			IntPtr scan = bitmapData.Scan0;
			Marshal.Copy(scan, array, 0, array.Length);
			MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
			byte[] value = mD5CryptoServiceProvider.ComputeHash(array);
			bitmap.UnlockBits(bitmapData);
			bitmap.Dispose();
			return BitConverter.ToString(value);
		}
	}
}
