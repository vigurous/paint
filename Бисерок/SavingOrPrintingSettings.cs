using System.Drawing.Printing;

namespace Бисерок
{
	public class SavingOrPrintingSettings
	{
		public int numberOfTab;

		public bool isPrinting;

		public PrintSettingsAndPages printSettings;

		public PrinterSettings printer;

		public bool isSaving;

		public string pathToSave = "";
	}
}
