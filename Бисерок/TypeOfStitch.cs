using System;

namespace Бисерок
{
	[Serializable]
	public enum TypeOfStitch
	{
		full,
		half,
		full_quarter,
		half_quarter,
		three_qarter,
		back,
		junction,
		emptyCanvas
	}
}
