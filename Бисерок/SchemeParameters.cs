using System;
using System.Configuration;
using System.Runtime.Serialization;
using Бисерок.Properties;

namespace Бисерок
{
	[Serializable]
	public class SchemeParameters
	{
		public int WidthInBead;

		public int HeightInBead;

		public int CountDesiredColors;

		public SchemeType Type;

		public BiserType BiserType;

		public bool isForSepia;

		public bool isSquareStraz;

		public float WidthColoringInSm;

		public float HeightColoringInSm;

		private int _NumberOfAdditionThread;

		private int _PrintingResolution;

		private int _SqureCellSidePrintPages_Y;

		private int _SqureCellSidePreview_Y;

		private string _KnittingDensityName;

		private float _LengthOfYarnPer1000sm2;

		private int _countOfBeadsIn1000Gramm;

		private float _VerticalSizeOfBlockAtSchemeInMm;

		private float _HorizontalSizeOfBlockAtSchemeInMm;

		private int _beadsOrCanvasSize;

		private bool _IsColorScheme_OrForGrayPrint;

		private bool _IsEmbroideryViewInRealView;

		private bool _IsWithDithering;

		private bool _IsShowNumberOfListOnCommonScheme;

		private float? _WidthOfBorderOfBeadsInPercentToBeadsRadius;

		private float _WidthOfPointBetweenBeadsInPercentToBeadsRadius;

		private bool _IsSeparatingLinesOnScheme;

		private bool? _IsDrawingBadgesOnSchemeForPrinting;

		private bool? _IsDrawSmallLegend;

		private bool? _IsMixingColors;

		private byte? _TransparentColorsOnCanvas;

		private byte? _Mediana;

		private byte? _NumberOfMedianaRepeats;

		private byte? _BrightnessOfContour;

		private byte? _BrightnessOfLabel;

		private byte? _SmallPartsOnColouring;

		private bool? _LightestColorIsCanvas;

		private bool? _DarkestColorIsCanvas;

		private int? _wayOfBadgesCreating;

		private int? _countOfPixelsInCmAtContoursMapOfColoring;

		private int? _thicknessOfAdditionalBorderOfColoringContour;

		private int? _widthPageInBeads;

		private int? _heightPageInBeads;

		private bool? _isNotChangeSourceImageInColoring;

		private float? _minFontSizeInPt;

		private bool? _isConstantNumerationOfColorsInRaskraska;

		public int NumberOfAdditionThread
		{
			get
			{
				_NumberOfAdditionThread = (int)((SettingsBase)Бисерок.Properties.Settings.Default)["NumberOfAdditionThread"];
				return _NumberOfAdditionThread;
			}
			set
			{
				_NumberOfAdditionThread = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["NumberOfAdditionThread"] = value;
			}
		}

		public int PrintingResolution
		{
			get
			{
				_PrintingResolution = (int)((SettingsBase)Бисерок.Properties.Settings.Default)["PrintingResolution"];
				return _PrintingResolution;
			}
			set
			{
				_PrintingResolution = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["PrintingResolution"] = value;
			}
		}

		public int SqureCellSidePrintPages_Y
		{
			get
			{
				_SqureCellSidePrintPages_Y = (int)((SettingsBase)Бисерок.Properties.Settings.Default)["SqureCellSidePrintPages_Y"];
				return _SqureCellSidePrintPages_Y;
			}
			set
			{
				_SqureCellSidePrintPages_Y = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["SqureCellSidePrintPages_Y"] = value;
			}
		}

		public int SqureCellSidePreview_Y
		{
			get
			{
				_SqureCellSidePreview_Y = (int)((SettingsBase)Бисерок.Properties.Settings.Default)["SqureCellSidePreview_Y"];
				return _SqureCellSidePreview_Y;
			}
			set
			{
				_SqureCellSidePreview_Y = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["SqureCellSidePreview_Y"] = value;
			}
		}

		public string KnittingDensityName
		{
			get
			{
				_KnittingDensityName = (string)((SettingsBase)Бисерок.Properties.Settings.Default)["KnittingDensityName"];
				return _KnittingDensityName;
			}
			set
			{
				_KnittingDensityName = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["KnittingDensityName"] = value;
			}
		}

		public float LengthOfYarnPer1000sm2
		{
			get
			{
				_LengthOfYarnPer1000sm2 = (float)((SettingsBase)Бисерок.Properties.Settings.Default)["LengthOfYarnPer1000sm2"];
				return _LengthOfYarnPer1000sm2;
			}
			set
			{
				_LengthOfYarnPer1000sm2 = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["LengthOfYarnPer1000sm2"] = value;
			}
		}

		public int countOfBeadsIn1000Gramm
		{
			get
			{
				if (Settings.hobbyProgramm != HobbyProgramm.Almaz)
				{
					_countOfBeadsIn1000Gramm = (int)((SettingsBase)Бисерок.Properties.Settings.Default)["CountOfBeadsIn1000Gramm"];
					return _countOfBeadsIn1000Gramm;
				}
				return 143000;
			}
			set
			{
				_countOfBeadsIn1000Gramm = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["CountOfBeadsIn1000Gramm"] = value;
			}
		}

		public float VerticalSizeOfBlockAtSchemeInMm
		{
			get
			{
				_VerticalSizeOfBlockAtSchemeInMm = (float)((SettingsBase)Бисерок.Properties.Settings.Default)["VerticalSizeOfBlockAtSchemeInMm"];
				return _VerticalSizeOfBlockAtSchemeInMm;
			}
			set
			{
				_VerticalSizeOfBlockAtSchemeInMm = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["VerticalSizeOfBlockAtSchemeInMm"] = value;
			}
		}

		public float HorizontalSizeOfBlockAtSchemeInMm
		{
			get
			{
				_HorizontalSizeOfBlockAtSchemeInMm = (float)((SettingsBase)Бисерок.Properties.Settings.Default)["HorizontalSizeOfBlockAtSchemeInMm"];
				return _HorizontalSizeOfBlockAtSchemeInMm;
			}
			set
			{
				_HorizontalSizeOfBlockAtSchemeInMm = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["HorizontalSizeOfBlockAtSchemeInMm"] = value;
			}
		}

		public int beadsOrCanvasSize
		{
			get
			{
				_beadsOrCanvasSize = (int)((SettingsBase)Бисерок.Properties.Settings.Default)["KanvasOrBeadsSize"];
				return _beadsOrCanvasSize;
			}
			set
			{
				_beadsOrCanvasSize = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["KanvasOrBeadsSize"] = value;
			}
		}

		public bool IsColorScheme_OrForGrayPrint
		{
			get
			{
				_IsColorScheme_OrForGrayPrint = (bool)((SettingsBase)Бисерок.Properties.Settings.Default)["IsColorScheme_OrForGrayPrint"];
				return _IsColorScheme_OrForGrayPrint;
			}
			set
			{
				_IsColorScheme_OrForGrayPrint = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["IsColorScheme_OrForGrayPrint"] = value;
			}
		}

		public bool IsEmbroideryViewInRealView
		{
			get
			{
				_IsEmbroideryViewInRealView = (bool)((SettingsBase)Бисерок.Properties.Settings.Default)["IsEmbroideryViewInRealView"];
				return _IsEmbroideryViewInRealView;
			}
			set
			{
				_IsEmbroideryViewInRealView = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["IsEmbroideryViewInRealView"] = value;
			}
		}

		public bool IsWithDithering
		{
			get
			{
				_IsWithDithering = (bool)((SettingsBase)Бисерок.Properties.Settings.Default)["IsWithDithering"];
				return _IsWithDithering;
			}
			set
			{
				_IsWithDithering = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["IsWithDithering"] = value;
			}
		}

		public bool IsShowNumberOfListOnCommonScheme
		{
			get
			{
				_IsShowNumberOfListOnCommonScheme = (bool)((SettingsBase)Бисерок.Properties.Settings.Default)["IsShowNumberOfListOnCommonScheme"];
				return _IsShowNumberOfListOnCommonScheme;
			}
			set
			{
				_IsShowNumberOfListOnCommonScheme = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["IsShowNumberOfListOnCommonScheme"] = value;
			}
		}

		public float WidthOfBorderOfBeadsInPercentToBeadsRadius
		{
			get
			{
				_WidthOfBorderOfBeadsInPercentToBeadsRadius = (float)((SettingsBase)Бисерок.Properties.Settings.Default)["WidthOfBorderOfBeadsInPercentToBeadsRadius"];
				return _WidthOfBorderOfBeadsInPercentToBeadsRadius.Value;
			}
			set
			{
				_WidthOfBorderOfBeadsInPercentToBeadsRadius = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["WidthOfBorderOfBeadsInPercentToBeadsRadius"] = value;
			}
		}

		public float WidthOfPointBetweenBeadsInPercentToBeadsRadius
		{
			get
			{
				_WidthOfPointBetweenBeadsInPercentToBeadsRadius = (float)((SettingsBase)Бисерок.Properties.Settings.Default)["WidthOfPointBetweenBeadsInPercentToBeadsRadius"];
				return _WidthOfPointBetweenBeadsInPercentToBeadsRadius;
			}
			set
			{
				_WidthOfPointBetweenBeadsInPercentToBeadsRadius = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["WidthOfPointBetweenBeadsInPercentToBeadsRadius"] = value;
			}
		}

		public bool IsSeparatingLinesOnScheme
		{
			get
			{
				_IsSeparatingLinesOnScheme = (bool)((SettingsBase)Бисерок.Properties.Settings.Default)["IsSeparatingLinesOnScheme"];
				return _IsSeparatingLinesOnScheme;
			}
			set
			{
				_IsSeparatingLinesOnScheme = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["IsSeparatingLinesOnScheme"] = value;
			}
		}

		public bool IsDrawingBadgesOnSchemeForPrinting
		{
			get
			{
				_IsDrawingBadgesOnSchemeForPrinting = (bool)((SettingsBase)Бисерок.Properties.Settings.Default)["IsDrawingBadgesOnSchemeForPrinting"];
				return _IsDrawingBadgesOnSchemeForPrinting.Value;
			}
			set
			{
				_IsDrawingBadgesOnSchemeForPrinting = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["IsDrawingBadgesOnSchemeForPrinting"] = value;
			}
		}

		public bool IsDrawSmallLegend
		{
			get
			{
				_IsDrawSmallLegend = (bool)((SettingsBase)Бисерок.Properties.Settings.Default)["IsDrawSmallLegend"];
				return _IsDrawSmallLegend.Value;
			}
			set
			{
				_IsDrawSmallLegend = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["IsDrawSmallLegend"] = value;
			}
		}

		public bool IsMixingColors
		{
			get
			{
				_IsMixingColors = (bool)((SettingsBase)Бисерок.Properties.Settings.Default)["IsMixingColors"];
				return _IsMixingColors.Value;
			}
			set
			{
				_IsMixingColors = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["IsMixingColors"] = value;
			}
		}

		public byte TransparentColorsOnCanvas
		{
			get
			{
				_TransparentColorsOnCanvas = (byte)((SettingsBase)Бисерок.Properties.Settings.Default)["TransparentColorsOnCanvas"];
				return _TransparentColorsOnCanvas.Value;
			}
			set
			{
				_TransparentColorsOnCanvas = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["TransparentColorsOnCanvas"] = value;
			}
		}

		public byte BrightnessOfContour
		{
			get
			{
				_BrightnessOfContour = (byte)((SettingsBase)Бисерок.Properties.Settings.Default)["BrightnessOfContour"];
				return _BrightnessOfContour.Value;
			}
			set
			{
				_BrightnessOfContour = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["BrightnessOfContour"] = value;
			}
		}

		public byte BrightnessOfLabel
		{
			get
			{
				_BrightnessOfLabel = (byte)((SettingsBase)Бисерок.Properties.Settings.Default)["BrightnessOfLabel"];
				return _BrightnessOfLabel.Value;
			}
			set
			{
				_BrightnessOfLabel = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["BrightnessOfLabel"] = value;
			}
		}

		public int countOfPixelsInCmAtContoursMapOfColoring
		{
			get
			{
				_countOfPixelsInCmAtContoursMapOfColoring = (int)((SettingsBase)Бисерок.Properties.Settings.Default)["countOfPixelsInCmAtContoursMapOfColoring"];
				return _countOfPixelsInCmAtContoursMapOfColoring.Value;
			}
			set
			{
				_countOfPixelsInCmAtContoursMapOfColoring = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["countOfPixelsInCmAtContoursMapOfColoring"] = value;
			}
		}

		public int thicknessOfAdditionalBorderOfColoringContour
		{
			get
			{
				_thicknessOfAdditionalBorderOfColoringContour = (int)((SettingsBase)Бисерок.Properties.Settings.Default)["thicknessOfAdditionalBorderOfColoringContour"];
				return _thicknessOfAdditionalBorderOfColoringContour.Value;
			}
			set
			{
				_thicknessOfAdditionalBorderOfColoringContour = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["thicknessOfAdditionalBorderOfColoringContour"] = value;
			}
		}

		public byte Mediana
		{
			get
			{
				_Mediana = (byte)((SettingsBase)Бисерок.Properties.Settings.Default)["Mediana"];
				return _Mediana.Value;
			}
			set
			{
				_Mediana = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["Mediana"] = value;
			}
		}

		public byte NumberOfMedianaRepeats
		{
			get
			{
				_NumberOfMedianaRepeats = (byte)((SettingsBase)Бисерок.Properties.Settings.Default)["NumberOfMedianaRepeats"];
				return _NumberOfMedianaRepeats.Value;
			}
			set
			{
				_NumberOfMedianaRepeats = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["NumberOfMedianaRepeats"] = value;
			}
		}

		public byte SmallPartsOnColouring
		{
			get
			{
				_SmallPartsOnColouring = (byte)((SettingsBase)Бисерок.Properties.Settings.Default)["SmallPartsOnColouring"];
				return _SmallPartsOnColouring.Value;
			}
			set
			{
				_SmallPartsOnColouring = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["SmallPartsOnColouring"] = value;
			}
		}

		public bool LightestColorIsCanvas
		{
			get
			{
				_LightestColorIsCanvas = (bool)((SettingsBase)Бисерок.Properties.Settings.Default)["LightestColorIsCanvas"];
				return _LightestColorIsCanvas.Value;
			}
			set
			{
				_LightestColorIsCanvas = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["LightestColorIsCanvas"] = value;
			}
		}

		public bool DarkestColorIsCanvas
		{
			get
			{
				_DarkestColorIsCanvas = (bool)((SettingsBase)Бисерок.Properties.Settings.Default)["DarkestColorIsCanvas"];
				return _DarkestColorIsCanvas.Value;
			}
			set
			{
				_DarkestColorIsCanvas = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["DarkestColorIsCanvas"] = value;
			}
		}

		public int wayOfBadgesCreating
		{
			get
			{
				_wayOfBadgesCreating = (int)((SettingsBase)Бисерок.Properties.Settings.Default)["wayOfBadgesCreating"];
				return _wayOfBadgesCreating.Value;
			}
			set
			{
				_wayOfBadgesCreating = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["wayOfBadgesCreating"] = value;
			}
		}

		public int widthPageInBeads
		{
			get
			{
				_widthPageInBeads = (int)((SettingsBase)Бисерок.Properties.Settings.Default)["widthPageInBeads"];
				return _widthPageInBeads.Value;
			}
			set
			{
				_widthPageInBeads = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["widthPageInBeads"] = value;
			}
		}

		public int heightPageInBeads
		{
			get
			{
				_heightPageInBeads = (int)((SettingsBase)Бисерок.Properties.Settings.Default)["heightPageInBeads"];
				return _heightPageInBeads.Value;
			}
			set
			{
				_heightPageInBeads = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["heightPageInBeads"] = value;
			}
		}

		public bool isNotChangeSourceImageInColoring
		{
			get
			{
				_isNotChangeSourceImageInColoring = (bool)((SettingsBase)Бисерок.Properties.Settings.Default)["isNotChangeSourceImageInColoring"];
				return _isNotChangeSourceImageInColoring.Value;
			}
			set
			{
				_isNotChangeSourceImageInColoring = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["isNotChangeSourceImageInColoring"] = value;
			}
		}

		public float minFontSizeInPt
		{
			get
			{
				_minFontSizeInPt = (float)((SettingsBase)Бисерок.Properties.Settings.Default)["minFontSizeInPt"];
				return _minFontSizeInPt.Value;
			}
			set
			{
				_minFontSizeInPt = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["minFontSizeInPt"] = value;
			}
		}

		public bool isConstantNumerationOfColorsInRaskraska
		{
			get
			{
				_isConstantNumerationOfColorsInRaskraska = (bool)((SettingsBase)Бисерок.Properties.Settings.Default)["isConstantNumerationOfColorsInRaskraska"];
				return _isConstantNumerationOfColorsInRaskraska.Value;
			}
			set
			{
				_isConstantNumerationOfColorsInRaskraska = value;
				((SettingsBase)Бисерок.Properties.Settings.Default)["isConstantNumerationOfColorsInRaskraska"] = value;
			}
		}

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			NumberOfAdditionThread = _NumberOfAdditionThread;
			PrintingResolution = _PrintingResolution;
			SqureCellSidePrintPages_Y = _SqureCellSidePrintPages_Y;
			SqureCellSidePreview_Y = _SqureCellSidePreview_Y;
			KnittingDensityName = _KnittingDensityName;
			LengthOfYarnPer1000sm2 = _LengthOfYarnPer1000sm2;
			countOfBeadsIn1000Gramm = _countOfBeadsIn1000Gramm;
			VerticalSizeOfBlockAtSchemeInMm = _VerticalSizeOfBlockAtSchemeInMm;
			HorizontalSizeOfBlockAtSchemeInMm = _HorizontalSizeOfBlockAtSchemeInMm;
			beadsOrCanvasSize = _beadsOrCanvasSize;
			IsColorScheme_OrForGrayPrint = _IsColorScheme_OrForGrayPrint;
			IsEmbroideryViewInRealView = _IsEmbroideryViewInRealView;
			IsWithDithering = _IsWithDithering;
			IsShowNumberOfListOnCommonScheme = _IsShowNumberOfListOnCommonScheme;
			if (!_WidthOfBorderOfBeadsInPercentToBeadsRadius.HasValue)
			{
				WidthOfBorderOfBeadsInPercentToBeadsRadius = 20f;
				WidthOfPointBetweenBeadsInPercentToBeadsRadius = 30f;
				IsSeparatingLinesOnScheme = true;
			}
			else
			{
				WidthOfBorderOfBeadsInPercentToBeadsRadius = _WidthOfBorderOfBeadsInPercentToBeadsRadius.Value;
				WidthOfPointBetweenBeadsInPercentToBeadsRadius = _WidthOfPointBetweenBeadsInPercentToBeadsRadius;
				IsSeparatingLinesOnScheme = _IsSeparatingLinesOnScheme;
			}
			if (_IsDrawingBadgesOnSchemeForPrinting.HasValue)
			{
				IsDrawingBadgesOnSchemeForPrinting = _IsDrawingBadgesOnSchemeForPrinting.Value;
			}
			else
			{
				IsDrawingBadgesOnSchemeForPrinting = false;
			}
			if (_IsDrawSmallLegend.HasValue)
			{
				IsDrawSmallLegend = _IsDrawSmallLegend.Value;
			}
			else
			{
				IsDrawSmallLegend = true;
			}
			if (_IsMixingColors.HasValue)
			{
				IsMixingColors = _IsMixingColors.Value;
			}
			else
			{
				IsMixingColors = false;
			}
			if (_TransparentColorsOnCanvas.HasValue)
			{
				TransparentColorsOnCanvas = _TransparentColorsOnCanvas.Value;
			}
			else
			{
				TransparentColorsOnCanvas = 0;
			}
			if (_BrightnessOfContour.HasValue)
			{
				BrightnessOfContour = _BrightnessOfContour.Value;
			}
			else
			{
				BrightnessOfContour = 140;
			}
			if (_BrightnessOfLabel.HasValue)
			{
				BrightnessOfLabel = _BrightnessOfLabel.Value;
			}
			else
			{
				BrightnessOfLabel = 70;
			}
			if (_countOfPixelsInCmAtContoursMapOfColoring.HasValue)
			{
				countOfPixelsInCmAtContoursMapOfColoring = _countOfPixelsInCmAtContoursMapOfColoring.Value;
			}
			else
			{
				countOfPixelsInCmAtContoursMapOfColoring = 40;
			}
			if (_thicknessOfAdditionalBorderOfColoringContour.HasValue)
			{
				thicknessOfAdditionalBorderOfColoringContour = _thicknessOfAdditionalBorderOfColoringContour.Value;
			}
			else
			{
				thicknessOfAdditionalBorderOfColoringContour = 0;
			}
			if (_widthPageInBeads.HasValue)
			{
				widthPageInBeads = _widthPageInBeads.Value;
			}
			else
			{
				widthPageInBeads = 60;
			}
			if (_heightPageInBeads.HasValue)
			{
				heightPageInBeads = _heightPageInBeads.Value;
			}
			else
			{
				heightPageInBeads = 90;
			}
			if (_Mediana.HasValue)
			{
				Mediana = _Mediana.Value;
			}
			else
			{
				Mediana = 3;
			}
			if (_NumberOfMedianaRepeats.HasValue)
			{
				NumberOfMedianaRepeats = _NumberOfMedianaRepeats.Value;
			}
			else
			{
				NumberOfMedianaRepeats = 3;
			}
			if (_SmallPartsOnColouring.HasValue)
			{
				SmallPartsOnColouring = _SmallPartsOnColouring.Value;
			}
			else
			{
				SmallPartsOnColouring = 1;
			}
			if (_LightestColorIsCanvas.HasValue)
			{
				LightestColorIsCanvas = _LightestColorIsCanvas.Value;
			}
			else
			{
				LightestColorIsCanvas = false;
			}
			if (_DarkestColorIsCanvas.HasValue)
			{
				DarkestColorIsCanvas = _DarkestColorIsCanvas.Value;
			}
			else
			{
				DarkestColorIsCanvas = false;
			}
			if (_wayOfBadgesCreating.HasValue)
			{
				wayOfBadgesCreating = _wayOfBadgesCreating.Value;
			}
			else
			{
				wayOfBadgesCreating = 1;
			}
			if (_isNotChangeSourceImageInColoring.HasValue)
			{
				isNotChangeSourceImageInColoring = _isNotChangeSourceImageInColoring.Value;
			}
			else
			{
				isNotChangeSourceImageInColoring = false;
			}
			if (_minFontSizeInPt.HasValue)
			{
				minFontSizeInPt = (float)(int)_minFontSizeInPt.Value;
			}
			else
			{
				minFontSizeInPt = 1f;
			}
			if (_isConstantNumerationOfColorsInRaskraska.HasValue)
			{
				isConstantNumerationOfColorsInRaskraska = _isConstantNumerationOfColorsInRaskraska.Value;
			}
			else
			{
				isConstantNumerationOfColorsInRaskraska = false;
			}
		}
	}
}
