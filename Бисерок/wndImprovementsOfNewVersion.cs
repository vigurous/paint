using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Бисерок
{
	public partial class wndImprovementsOfNewVersion : Window, IComponentConnector
	{
		public wndImprovementsOfNewVersion()
		{
			InitializeComponent();
		}

		private void btnClose_Click(object sender, RoutedEventArgs e)
		{
			base.Close();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			tbxImprovements.Text = Settings.VersionInfo;
		}
	}
}
