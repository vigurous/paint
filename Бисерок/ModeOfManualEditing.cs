namespace Бисерок
{
	internal enum ModeOfManualEditing
	{
		None,
		GetColorFromList,
		Pipet,
		Brush,
		Fill,
		GetColorFromName
	}
}
