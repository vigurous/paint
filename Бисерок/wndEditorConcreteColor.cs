using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace Бисерок
{
	public partial class wndEditorConcreteColor : Window, IComponentConnector
	{
		public string name;

		public System.Windows.Media.Color color;

		public wndEditorConcreteColor()
		{
			InitializeComponent();
			base.Title = Local.Get(base.Title);
			Local.LocalizeControlAndItsChildren(grdMain);
		}

		public wndEditorConcreteColor(Beads clr)
		{
			InitializeComponent();
			base.Title = Local.Get(base.Title);
			Local.LocalizeControlAndItsChildren(grdMain);
			tbxName.Text = clr.name;
			ColorPicker colorPicker = cpkColors;
			System.Drawing.Color clr2 = clr.clr;
			byte r = clr2.R;
			clr2 = clr.clr;
			byte g = clr2.G;
			clr2 = clr.clr;
			colorPicker.SelectedColor = System.Windows.Media.Color.FromRgb(r, g, clr2.B);
		}

		private void btnSave_Click(object sender, RoutedEventArgs e)
		{
			name = tbxName.Text;
			color = cpkColors.SelectedColor;
			base.DialogResult = true;
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = false;
		}
	}
}
