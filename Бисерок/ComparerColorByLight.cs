using System;
using System.Collections.Generic;
using System.Drawing;

namespace Бисерок
{
	internal class ComparerColorByLight : IComparer<Beads>
	{
		public int Compare(Beads _x, Beads _y)
		{
			Color clr = _x.clr;
			Color clr2 = _y.clr;
			if (clr.R == clr2.R && clr.G == clr2.G && clr.B == clr2.B)
			{
				return 0;
			}
			float num = (float)Math.Round((double)((((float)(int)clr.R + 0f) * 0.3f + ((float)(int)clr.G + 0f) * 0.59f + ((float)(int)clr.B + 0f) * 0.11f) * 100f)) / 100f;
			float num2 = (float)Math.Round((double)((((float)(int)clr2.R + 0f) * 0.3f + ((float)(int)clr2.G + 0f) * 0.59f + ((float)(int)clr2.B + 0f) * 0.11f) * 100f)) / 100f;
			if (num < num2)
			{
				return 1;
			}
			return -1;
		}
	}
}
