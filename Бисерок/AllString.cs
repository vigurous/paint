namespace Бисерок
{
	public static class AllString
	{
		public static string GetCountColorString(int n)
		{
			n -= n / 100 * 100;
			if (n >= 10 && n <= 14)
			{
				return Local.Get("цветов");
			}
			n -= n / 10 * 10;
			switch (n)
			{
			case 0:
				return Local.Get("цветов");
			case 1:
				return Local.Get("цвета");
			case 2:
				return Local.Get("цветов");
			case 3:
				return Local.Get("цветов");
			case 4:
				return Local.Get("цветов");
			case 5:
				return Local.Get("цветов");
			case 6:
				return Local.Get("цветов");
			case 7:
				return Local.Get("цветов");
			case 8:
				return Local.Get("цветов");
			case 9:
				return Local.Get("цветов");
			default:
				return Local.Get("");
			}
		}

		public static string GetCountColorStringInRoditelsiyPadezh(int n)
		{
			n -= n / 100 * 100;
			if (n >= 10 && n <= 14)
			{
				return Local.Get("цветов");
			}
			n -= n / 10 * 10;
			switch (n)
			{
			case 0:
				return Local.Get("цветов");
			case 1:
				return Local.Get("цвет");
			case 2:
				return Local.Get("цвета");
			case 3:
				return Local.Get("цвета");
			case 4:
				return Local.Get("цвета");
			case 5:
				return Local.Get("цветов");
			case 6:
				return Local.Get("цветов");
			case 7:
				return Local.Get("цветов");
			case 8:
				return Local.Get("цветов");
			case 9:
				return Local.Get("цветов");
			default:
				return Local.Get("");
			}
		}

		public static string GetCountPartsString(int n)
		{
			n -= n / 100 * 100;
			if (n >= 10 && n <= 14)
			{
				return Local.Get("частей");
			}
			n -= n / 10 * 10;
			switch (n)
			{
			case 0:
				return Local.Get("частей");
			case 1:
				return Local.Get("часть");
			case 2:
				return Local.Get("части");
			case 3:
				return Local.Get("части");
			case 4:
				return Local.Get("части");
			case 5:
				return Local.Get("частей");
			case 6:
				return Local.Get("частей");
			case 7:
				return Local.Get("частей");
			case 8:
				return Local.Get("частей");
			case 9:
				return Local.Get("частей");
			default:
				return Local.Get("");
			}
		}

		public static string GetCountContours(int n)
		{
			n -= n / 100 * 100;
			if (n >= 10 && n <= 14)
			{
				return Local.Get("контуров");
			}
			n -= n / 10 * 10;
			switch (n)
			{
			case 0:
				return Local.Get("контуров");
			case 1:
				return Local.Get("контур");
			case 2:
				return Local.Get("контура");
			case 3:
				return Local.Get("контура");
			case 4:
				return Local.Get("контура");
			case 5:
				return Local.Get("контуров");
			case 6:
				return Local.Get("контуров");
			case 7:
				return Local.Get("контуров");
			case 8:
				return Local.Get("контуров");
			case 9:
				return Local.Get("контуров");
			default:
				return Local.Get("");
			}
		}
	}
}
