using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Бисерок
{
	[Serializable]
	public class BeadsManufacturer
	{
		public bool isUsing = true;

		public List<BeadsSeries> beadsSeries = new List<BeadsSeries>();

		[DefaultValue("")]
		public string name
		{
			get;
			set;
		}
	}
}
