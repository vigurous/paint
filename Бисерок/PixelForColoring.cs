using System;
using System.Collections.Generic;
using System.Drawing;

namespace Бисерок
{
	[Serializable]
	public class PixelForColoring : Pixel
	{
		public Color clr;

		public Block GetCopy(int x, int y, List<Beads> bds)
		{
			Block block = new Block();
			block.X = x;
			block.Y = y;
			block.SrcColor = base.SrcColor;
			block.ResBeads = bds.Find((Beads bb) => bb.clr == clr);
			return block;
		}
	}
}
