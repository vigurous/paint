using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Бисерок
{
	public partial class wndEditorSeries : Window, IComponentConnector
	{
		public string seriesName = "";

		public wndEditorSeries()
		{
			InitializeComponent();
			base.Title = Local.Get(base.Title);
			Local.LocalizeControlAndItsChildren(grdMain);
		}

		public wndEditorSeries(string _seriesName)
		{
			InitializeComponent();
			base.Title = Local.Get(base.Title);
			Local.LocalizeControlAndItsChildren(grdMain);
			tbxName.Text = _seriesName;
		}

		private void btnSave_Click(object sender, RoutedEventArgs e)
		{
			seriesName = tbxName.Text;
			base.DialogResult = true;
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = false;
		}
	}
}
