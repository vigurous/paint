using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace Бисерок
{
	public partial class WndAbout : Window, IComponentConnector
	{
		private int v = Settings.v;

		public WndAbout()
		{
			InitializeComponent();
			if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
			{
				tblAbout.Text = "Программа 'Бисерок 2.0' создана в помощь рукодельницам. Она позволяет создавать схемы вышивки бисером из любых изображений!";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				tblAbout.Text = "Программа 'Крестик 2.0' создана в помощь рукодельницам. Она позволяет создавать схемы вышивки крестом из любых изображений!";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
			{
				tblAbout.Text = "Программа 'Петелька' создана в помощь рукодельницам. Она позволяет создавать схемы вязания из любых изображений!";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				tblAbout.Text = "Программа 'Раскраска' создана в помощь рукодельницам. Она позволяет создавать раскраски и картины по номерам из любых изображений!";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
			{
				tblAbout.Text = "Программа 'Алмазная мозаика' создана в помощь рукодельницам. Она позволяет создавать алмазные вышивки из любых изображений!";
			}
			TblSite.Text = Settings.siteAndEmailSettings.urlSite;
			DateTime? nullable = RegistryWork.CheckDateTimeEnd();
			if (nullable.HasValue)
			{
				int num = Convert.ToInt32((nullable.Value - DateTime.Now).TotalDays);
				if (num > 90 || num < 0)
				{
					TextBlock textBlock = tblProlongForOneYear;
					TextBlock textBlock2 = tblNumberOfDaysToEndOfLicense;
					Visibility visibility2 = textBlock2.Visibility = Visibility.Collapsed;
					textBlock.Visibility = visibility2;
				}
				else
				{
					TextBlock textBlock3 = tblProlongForOneYear;
					TextBlock textBlock4 = tblNumberOfDaysToEndOfLicense;
					Visibility visibility2 = textBlock4.Visibility = Visibility.Visible;
					textBlock3.Visibility = visibility2;
					tblNumberOfDaysToEndOfLicense.Text = Local.Get("До окончания Вашей лицензии: ") + num.ToString() + Local.Get(" дн.");
				}
			}
			else
			{
				TextBlock textBlock5 = tblProlongForOneYear;
				TextBlock textBlock6 = tblNumberOfDaysToEndOfLicense;
				Visibility visibility2 = textBlock6.Visibility = Visibility.Collapsed;
				textBlock5.Visibility = visibility2;
			}
			string text = RegistryWork.CheckEmail();
			if (text.Length == 0)
			{
				text = Local.Get("не внесён адрес эл.почты");
			}
			Local.LocalizeControlAndItsChildren(grdMain);
			base.Title = Local.Get(base.Title);
			TextBlock textBlock7 = tblLicenseEmail;
			textBlock7.Text += text;
		}

		private void tblSite_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			string urlSite = Settings.siteAndEmailSettings.urlSite;
			urlSite = ((v != 0) ? (urlSite + "?f=about_full_prog") : (urlSite + "/?f=test_prog"));
			try
			{
				Process.Start(urlSite);
			}
			catch
			{
				WndInfo wndInfo = new WndInfo("По какой-то причине программе не удалось открыть ссылку. Пожалуйста, скопируйте вручную ссылку из текстового поля ниже, вставьте в Ваш браузер и перейдите по ней самостоятельно. Спасибо!", urlSite);
				wndInfo.ShowDialog();
			}
		}

		private void tblMail_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			Process.Start("mailto:" + Settings.AddressOfAuthorOfProgram + "?subject=" + Local.Get("Программа '") + Settings.ShortProgramName + Local.Get("' - вопрос из программы"));
		}

		private void tblProlongForOneYear_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			Site.BuyProgram(null);
		}
	}
}
