using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Бисерок
{
	public partial class WndWithRecomendationsForHighQualityResult : Window, IComponentConnector
	{
		public WndWithRecomendationsForHighQualityResult()
		{
			InitializeComponent();
			tbx.Text = "Рекомендации к Фотографиям, из которых Вы создаёте схему:\r\n\r\n    1.    Выбирайте фото с высоким разрешением (файл более 300 кб)\r\n\r\n    2.    Фото должно быть с четким изображением, без размытых лиц.\r\n\r\n    3.    Если на фото есть лица, то они должны быть отображены крупным планом.\r\n\r\n    4.    Лица на фото должны быть хорошо освещены, не выбирайте фото с тенями на лице.\r\n\r\n    5.    Чем больше размер работы, тем картина получается реалистичней, более детализированной.";
		}

		private void btnOk_Click(object sender, RoutedEventArgs e)
		{
			base.Close();
		}
	}
}
