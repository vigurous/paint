using System;
using System.Runtime.Serialization;

namespace Бисерок
{
	[Serializable]
	public class Stitch
	{
		public TypeOfStitch typeOfStitch;

		private Beads resBeads;

		public Beads ResBeads;

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			if (ResBeads == (Beads)null)
			{
				ResBeads = resBeads;
			}
			resBeads = null;
		}

		public Stitch GetCopy()
		{
			Stitch stitch = new Stitch
			{
				typeOfStitch = typeOfStitch
			};
			if (ResBeads != (Beads)null)
			{
				stitch.ResBeads = ResBeads;
			}
			return stitch;
		}
	}
}
