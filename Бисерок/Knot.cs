using System;
using System.Drawing;

namespace Бисерок
{
	[Serializable]
	public class Knot
	{
		public PointF point;

		public Beads bead;
	}
}
