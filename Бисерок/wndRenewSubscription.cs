using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Бисерок
{
	public partial class wndRenewSubscription : Window, IComponentConnector
	{
		public wndRenewSubscription()
		{
			InitializeComponent();
			if (Settings.isItVersionWithoutContactInfo)
			{
				Button button = btnGoTestVersion;
				Button button2 = btnRenewFullVersion;
				bool isEnabled = button2.IsEnabled = false;
				button.IsEnabled = isEnabled;
			}
			tbl.Text = tbl.Text.Replace("EMAIL", Settings.siteAndEmailSettings.supportEmail);
		}

		private void btnRenewFullVersion_Click(object sender, RoutedEventArgs e)
		{
			Site.BuyProgram(null);
		}

		private void btnGoTestVersion_Click(object sender, RoutedEventArgs e)
		{
			WndMain wndMain = new WndMain();
			wndMain.Closing += wnd_Closing;
			wndMain.Show();
			base.Hide();
		}

		private void wnd_Closing(object sender, CancelEventArgs e)
		{
			base.Close();
		}
	}
}
