using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using Бисерок.Properties;

namespace Бисерок
{
	public partial class wndSettings : Window, IComponentConnector
	{
		private Scheme scheme;

		private bool _isShowToAutoSaving;

		

		public wndSettings(Scheme _scheme, bool isShowToAutoSaving = false)
		{
			InitializeComponent();
			scheme = _scheme;
			_isShowToAutoSaving = isShowToAutoSaving;
		}

		private void OnlyDigitInTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			char sep = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
			if ((((TextBox)sender).Text.Length != 0 || !e.Text.Contains("0")) && !e.Text.Any(delegate(char x)
			{
				if (!char.IsDigit(x))
				{
					return x != sep;
				}
				return false;
			}))
			{
				return;
			}
			e.Handled = true;
		}

		private void btnSave_Click(object sender, RoutedEventArgs e)
		{
			float num;
			float num2;
			try
			{
				num = Convert.ToSingle(tbxBlockSizeVertic.Text.Replace(".", ","));
				num2 = Convert.ToSingle(tbxBlockSizeHoriz.Text.Replace(".", ","));
			}
			catch
			{
				if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
				{
					num2 = (num = 2.3f);
				}
				else if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
				{
					num2 = (num = 2f);
				}
				else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
				{
					num2 = (num = 1.81428f);
				}
				else
				{
					num = 3.33333325f;
					num2 = 5f;
				}
			}
			bool flag = true;
			if (Settings.hobbyProgramm == HobbyProgramm.Almaz && (num2 != 2.5f || num != 2.5f))
			{
				string strQuestion = "У Вас установлен размер страз, отличный от стандартного (стандартный размер=2.5мм). Сохранить нестандартный размер страз?";
				if (_isShowToAutoSaving)
				{
					strQuestion = "У Вас в настройках установлен размер страз, отличный от стандартного (стандартный размер=2.5мм). Продолжить создание схемы с нестандартным размером страз?";
				}
				wndOkCancel wndOkCancel = new wndOkCancel("Размер страз не равен стандартному", strQuestion);
				bool? nullable = wndOkCancel.ShowDialog();
				flag = (nullable.HasValue && nullable.Value && true);
			}
			if (flag)
			{
				scheme.Param.VerticalSizeOfBlockAtSchemeInMm = num;
				scheme.Param.HorizontalSizeOfBlockAtSchemeInMm = num2;
				scheme.Param.PrintingResolution = Convert.ToInt32(tbxPrintingResolution.Text);
				scheme.Param.SqureCellSidePreview_Y = Convert.ToInt32(tbxSizeOfBlockInPreview.Text);
				scheme.Param.SqureCellSidePrintPages_Y = Convert.ToInt32(tbxSizeOfBlockInPrinting.Text);
				scheme.Param.widthPageInBeads = Convert.ToInt32(tbxWidthPageInBeads.Text);
				scheme.Param.heightPageInBeads = Convert.ToInt32(tbxHeightPageInBeads.Text);
				scheme.Param.WidthOfBorderOfBeadsInPercentToBeadsRadius = (float)slrWidthOfBorderOfBeadsInPercentToBeadsRadius.Value;
				scheme.Param.WidthOfPointBetweenBeadsInPercentToBeadsRadius = (float)slrWidthOfPointBetweenBeadsInPercentToBeadsRadius.Value;
				if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
				{
					scheme.Param.NumberOfAdditionThread = Convert.ToInt32(tbxNumberOfAdditionThread.Text);
				}
				if (rbtWayOfBadgesCreating1.IsChecked.Value)
				{
					scheme.Param.wayOfBadgesCreating = 1;
				}
				else if (rbtWayOfBadgesCreating2.IsChecked.Value)
				{
					scheme.Param.wayOfBadgesCreating = 2;
				}
				else
				{
					scheme.Param.wayOfBadgesCreating = 3;
				}
				Бисерок.Properties.Settings.Default.Save();
				base.Close();
			}
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			Бисерок.Properties.Settings.Default.Reload();
			base.Close();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			if (_isShowToAutoSaving)
			{
				btnCancel.IsEnabled = false;
			}
			TextBox textBox = tbxBlockSizeVertic;
			float num = scheme.Param.VerticalSizeOfBlockAtSchemeInMm;
			textBox.Text = num.ToString();
			TextBox textBox2 = tbxBlockSizeHoriz;
			num = scheme.Param.HorizontalSizeOfBlockAtSchemeInMm;
			textBox2.Text = num.ToString();
			List<BeadsOrCanvasSize> list = new List<BeadsOrCanvasSize>();
			if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
			{
				Grid grid = grdForKrestik;
				Grid grid2 = grdForBiser;
				Grid grid3 = grdForBiser2;
				Visibility visibility2 = grid3.Visibility = Visibility.Collapsed;
				Visibility visibility4 = grid2.Visibility = visibility2;
				grid.Visibility = visibility4;
				tblSizeOfCanvasOrBeads.Text = "Плотность вязания:";
				cbxViewOfEmbroidery.Content = "Вид вязания составлен из петелек (если не выбрано, то - из квадратиков)";
				cbxIsDrawSmallLegend.Visibility = Visibility.Collapsed;
				cbxDithering.Content = "'Диффузия' - с ней вязание реалистичней, но вывязать сложнее";
				TextBox textBox3 = tbxDensityKnittingIn10SmHoriz;
				int num2 = (int)Math.Round((double)(100f / scheme.Param.HorizontalSizeOfBlockAtSchemeInMm));
				textBox3.Text = num2.ToString();
				TextBox textBox4 = tbxDensityKnittingIn10SmVertic;
				num2 = (int)Math.Round((double)(100f / scheme.Param.VerticalSizeOfBlockAtSchemeInMm));
				textBox4.Text = num2.ToString();
				tblSizeOfBlockInPreview.Text = "Размер петли на схеме в процессе работы (в пикселях):";
				tblSizeOfBlockInPrinting.Text = "Размер петли на схеме после сохранения (в пикселях):";
				tblSizeOfPageInBeads.Text = "Размер одной страницы в петлях:";
				image17.Visibility = Visibility.Collapsed;
				list.Add(new BeadsOrCanvasSize
				{
					name = "8 петель на 10см, 10 рядов на 10см (40-60м в 100гр. пряжи)",
					size = 8,
					verticSizeInMm = 10f,
					horizSizeInMm = 12.5f,
					lengthOfYarnPer1000sm2 = 50.7874f
				});
				list.Add(new BeadsOrCanvasSize
				{
					name = "12 петель на 10см, 16 рядов на 10см (70-90м в 100гр. пряжи)",
					size = 12,
					verticSizeInMm = 6.25f,
					horizSizeInMm = 8.333333f,
					lengthOfYarnPer1000sm2 = 63.5827f
				});
				list.Add(new BeadsOrCanvasSize
				{
					name = "14 петель на 10см, 18 рядов на 10см (90-120м в 100гр. пряжи)",
					size = 14,
					verticSizeInMm = 5.55555534f,
					horizSizeInMm = 7.142857f,
					lengthOfYarnPer1000sm2 = 72.4409f
				});
				list.Add(new BeadsOrCanvasSize
				{
					name = "16 петель на 10см, 24 ряда на 10см (120-180м в 100гр. пряжи)",
					size = 16,
					verticSizeInMm = 4.16666651f,
					horizSizeInMm = 6.25f,
					lengthOfYarnPer1000sm2 = 92.126f
				});
				list.Add(new BeadsOrCanvasSize
				{
					name = "18 петель на 10см, 24 ряда на 10см (190-220м в 100гр. пряжи)",
					size = 18,
					verticSizeInMm = 4.16666651f,
					horizSizeInMm = 5.55555534f,
					lengthOfYarnPer1000sm2 = 99.0157f
				});
				list.Add(new BeadsOrCanvasSize
				{
					name = "20 петель на 10см, 30 рядов на 10см (230-300м в 100гр. пряжи)",
					size = 20,
					verticSizeInMm = 3.33333325f,
					horizSizeInMm = 5f,
					lengthOfYarnPer1000sm2 = 122.8346f
				});
				list.Add(new BeadsOrCanvasSize
				{
					name = "22 петли на 10см, 30 рядов на 10см (280-320м в 100гр. пряжи)",
					size = 22,
					verticSizeInMm = 3.33333325f,
					horizSizeInMm = 4.5454545f,
					lengthOfYarnPer1000sm2 = 141.3386f
				});
				list.Add(new BeadsOrCanvasSize
				{
					name = "24 петли на 10см, 32 ряда на 10см (310-350м в 100гр. пряжи)",
					size = 24,
					verticSizeInMm = 3.125f,
					horizSizeInMm = 4.16666651f,
					lengthOfYarnPer1000sm2 = 174.4094f
				});
				cmbBeadsOrCanvasSize.ItemsSource = list;
				try
				{
					cmbBeadsOrCanvasSize.SelectedItem = list.First((BeadsOrCanvasSize bs) => bs.size == scheme.Param.beadsOrCanvasSize);
				}
				catch (Exception)
				{
					cmbBeadsOrCanvasSize.SelectedIndex = 2;
				}
				TextBlock textBlock = tblInfoAboutColorOfCanvas;
				CheckBox checkBox = cbxDarkestColorIsCanvas;
				CheckBox checkBox2 = cbxLightestColorIsCanvas;
				visibility2 = (checkBox2.Visibility = Visibility.Collapsed);
				visibility4 = (checkBox.Visibility = visibility2);
				textBlock.Visibility = visibility4;
				cbxIsShowNumberOfListOnCommonScheme.Margin = cbxIsDrawSmallLegend.Margin;
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Krestik || Settings.hobbyProgramm == HobbyProgramm.Biserok)
			{
				cbxDithering.Content = "'Диффузия' в вышивке - с ней вышивка реалистичней, но вышивать сложнее";
				if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
				{
					Grid grid4 = grdForBiser;
					Grid grid5 = grdForBiser2;
					Visibility visibility4 = grid5.Visibility = Visibility.Visible;
					grid4.Visibility = visibility4;
					grdForKrestik.Visibility = Visibility.Collapsed;
					tblSizeOfCanvasOrBeads.Text = "Размер бисера:";
					cbxViewOfEmbroidery.Content = "Вид вышивки составлен из бусинок (если не выбрано, то - из квадратиков)";
					cbxIsDrawSmallLegend.Content = "Рисовать мини-легенду на главной схеме и схеме для печати на ткани";
					tblSizeOfBlockInPreview.Text = "Размер бусинки на схеме в процессе работы (в пикселях):";
					tblSizeOfBlockInPrinting.Text = "Размер бусинки на схеме после сохранения (в пикселях):";
					tblSizeOfPageInBeads.Text = "Размер одной страницы в бусинках:";
					list.Add(new BeadsOrCanvasSize
					{
						name = "6/0",
						size = 6,
						horizSizeInMm = 3.57f,
						verticSizeInMm = 3.57f,
						countOfBeadsIn1000Gramm = 13300
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "7/0",
						size = 7,
						horizSizeInMm = 3f,
						verticSizeInMm = 3f,
						countOfBeadsIn1000Gramm = 21100
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "8/0",
						size = 8,
						horizSizeInMm = 2.5f,
						verticSizeInMm = 2.5f,
						countOfBeadsIn1000Gramm = 42000
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "9/0",
						size = 9,
						horizSizeInMm = 2.26f,
						verticSizeInMm = 2.26f,
						countOfBeadsIn1000Gramm = 68000
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "10/0",
						size = 10,
						horizSizeInMm = 2f,
						verticSizeInMm = 2f,
						countOfBeadsIn1000Gramm = 91000
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "11/0",
						size = 11,
						horizSizeInMm = 1.8f,
						verticSizeInMm = 1.8f,
						countOfBeadsIn1000Gramm = 130000
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "12/0",
						size = 12,
						horizSizeInMm = 1.65f,
						verticSizeInMm = 1.65f,
						countOfBeadsIn1000Gramm = 165000
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "13/0",
						size = 13,
						horizSizeInMm = 1.48f,
						verticSizeInMm = 1.48f,
						countOfBeadsIn1000Gramm = 291000
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "14/0",
						size = 14,
						horizSizeInMm = 1.4f,
						verticSizeInMm = 1.4f,
						countOfBeadsIn1000Gramm = 325000
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "15/0",
						size = 15,
						horizSizeInMm = 1.3f,
						verticSizeInMm = 1.3f,
						countOfBeadsIn1000Gramm = 625000
					});
				}
				else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
				{
					Grid grid6 = grdForBiser;
					Grid grid7 = grdForBiser2;
					Visibility visibility4 = grid7.Visibility = Visibility.Collapsed;
					grid6.Visibility = visibility4;
					grdForKrestik.Visibility = Visibility.Visible;
					tblSizeOfCanvasOrBeads.Text = "Размер канвы:";
					cbxViewOfEmbroidery.Content = "Вид вышивки составлен из стежков (если не выбрано, то - из квадратиков)";
					cbxIsDrawSmallLegend.Content = "Рисовать мини-легенду на главной схеме";
					tblSizeOfBlockInPreview.Text = "Размер крестика на схеме в процессе работы (в пикселях):";
					tblSizeOfBlockInPrinting.Text = "Размер крестика на схеме после сохранения (в пикселях):";
					tblSizeOfPageInBeads.Text = "Размер одной страницы в стежках:";
					image17.Visibility = Visibility.Collapsed;
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 6",
						size = 6
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 7",
						size = 7
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 8",
						size = 8
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 9",
						size = 9
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 10",
						size = 10
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 11",
						size = 11
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 12",
						size = 12
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 13",
						size = 13
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 14",
						size = 14
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 15",
						size = 15
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 16",
						size = 16
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 18",
						size = 18
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 20",
						size = 20
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Аида 22",
						size = 22
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Страмин Gamma К-19",
						size = 5
					});
					list.Add(new BeadsOrCanvasSize
					{
						name = "Канва K02 'Gamma'",
						size = 4
					});
					foreach (BeadsOrCanvasSize item in list)
					{
						BeadsOrCanvasSize beadsOrCanvasSize = item;
						BeadsOrCanvasSize beadsOrCanvasSize2 = beadsOrCanvasSize;
						BeadsOrCanvasSize beadsOrCanvasSize3 = beadsOrCanvasSize;
						num = (beadsOrCanvasSize2.horizSizeInMm = (beadsOrCanvasSize3.verticSizeInMm = (float)(int)(2540f / ((float)beadsOrCanvasSize.size + 0f)) / 100f));
						if (beadsOrCanvasSize.name == "Страмин Gamma К-19")
						{
							BeadsOrCanvasSize beadsOrCanvasSize4 = beadsOrCanvasSize;
							BeadsOrCanvasSize beadsOrCanvasSize5 = beadsOrCanvasSize;
							num = (beadsOrCanvasSize4.horizSizeInMm = (beadsOrCanvasSize5.verticSizeInMm = 5.2632f));
						}
						else if (beadsOrCanvasSize.name == "Канва K02 'Gamma'")
						{
							BeadsOrCanvasSize beadsOrCanvasSize6 = beadsOrCanvasSize;
							BeadsOrCanvasSize beadsOrCanvasSize7 = beadsOrCanvasSize;
							num = (beadsOrCanvasSize6.horizSizeInMm = (beadsOrCanvasSize7.verticSizeInMm = 6.666666f));
						}
					}
				}
				cmbBeadsOrCanvasSize.ItemsSource = list;
				try
				{
					cmbBeadsOrCanvasSize.SelectedItem = list.First((BeadsOrCanvasSize bs) => bs.size == scheme.Param.beadsOrCanvasSize);
				}
				catch (Exception)
				{
					cmbBeadsOrCanvasSize.SelectedIndex = 4;
				}
				grdForPetelka.Visibility = Visibility.Collapsed;
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
			{
				cbxDithering.Content = "'Диффузия' в вышивке - с ней вышивка реалистичней, но выкладывать посложнее";
				ColumnDefinition columnDefinition = grdForBiserAndKrestik.ColumnDefinitions[0];
				ColumnDefinition columnDefinition2 = grdForBiserAndKrestik.ColumnDefinitions[1];
				RowDefinition rowDefinition = grdMain.RowDefinitions[2];
				GridLength gridLength2 = rowDefinition.Height = new GridLength(0.0);
				GridLength gridLength5 = columnDefinition.Width = (columnDefinition2.Width = gridLength2);
				image17.Visibility = Visibility.Collapsed;
				tblSizeOfBeadsOrKrestikInMm.Text = "Размер стразы в мм:";
				grdForBiser2.RowDefinitions[1].Height = new GridLength(0.0);
				cbxViewOfEmbroidery.Content = "Вид мозаики составлен из страз (если не выбрано, то - из квадратиков)";
				cbxIsDrawSmallLegend.Content = "Рисовать мини-легенду на главной схеме и схеме для печати";
				tblSizeOfBlockInPreview.Text = "Размер стразы на схеме в процессе работы (в пикселях):";
				tblSizeOfBlockInPrinting.Text = "Размер стразы на схеме после сохранения (в пикселях):";
				tblSizeOfPageInBeads.Text = "Размер одной страницы в стразах:";
				TextBlock textBlock2 = tblInfoAboutColorOfCanvas;
				CheckBox checkBox3 = cbxDarkestColorIsCanvas;
				CheckBox checkBox4 = cbxLightestColorIsCanvas;
				Visibility visibility2 = checkBox4.Visibility = Visibility.Collapsed;
				Visibility visibility4 = checkBox3.Visibility = visibility2;
				textBlock2.Visibility = visibility4;
			}
			rbtWayOfBadgesCreating1.IsChecked = (scheme.Param.wayOfBadgesCreating == 1);
			rbtWayOfBadgesCreating2.IsChecked = (scheme.Param.wayOfBadgesCreating == 2);
			rbtWayOfBadgesCreating3.IsChecked = (scheme.Param.wayOfBadgesCreating == 3);
			if (_isShowToAutoSaving)
			{
				btnSave_Click(new object(), new RoutedEventArgs());
			}
		}

		private void cmbBeadsOrCanvasSize_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (cmbBeadsOrCanvasSize.IsDropDownOpen)
			{
				UpdateBeadsSizeAsSelectedInDDL();
			}
		}

		private void UpdateBeadsSizeAsSelectedInDDL()
		{
			BeadsOrCanvasSize beadsOrCanvasSize = cmbBeadsOrCanvasSize.SelectedItem as BeadsOrCanvasSize;
			scheme.Param.beadsOrCanvasSize = beadsOrCanvasSize.size;
			if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
			{
				scheme.Param.countOfBeadsIn1000Gramm = beadsOrCanvasSize.countOfBeadsIn1000Gramm;
			}
			TextBox textBox = tbxBlockSizeHoriz;
			float num = beadsOrCanvasSize.horizSizeInMm;
			textBox.Text = num.ToString();
			TextBox textBox2 = tbxBlockSizeVertic;
			num = beadsOrCanvasSize.verticSizeInMm;
			textBox2.Text = num.ToString();
			if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				int size = beadsOrCanvasSize.size;
				int num2 = 1;
				if (size <= 8)
				{
					num2 = 6;
				}
				else if (size >= 9 && size <= 10)
				{
					num2 = 4;
				}
				else if (size >= 11 && size <= 13)
				{
					num2 = 3;
				}
				else if (size >= 14 && size <= 16)
				{
					num2 = 2;
				}
				else if (size >= 17)
				{
					num2 = 1;
				}
				tbxNumberOfAdditionThread.Text = num2.ToString();
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
			{
				scheme.Param.LengthOfYarnPer1000sm2 = beadsOrCanvasSize.lengthOfYarnPer1000sm2;
				scheme.Param.KnittingDensityName = beadsOrCanvasSize.name;
			}
		}

		private void tbxBlockSizeHoriz_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				float num = Convert.ToSingle((sender as TextBox).Text.Replace(".", ","));
			}
			catch (Exception)
			{
				(sender as TextBox).Text = "2.3";
				MessageBox.Show("В поле размера можно вводить только цифры, в качестве разделителя между целой и дробной частью может использоваться точка или запятая");
			}
		}

		private void tbxBlockSizeVertic_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				float num = Convert.ToSingle((sender as TextBox).Text.Replace(".", ","));
			}
			catch (Exception)
			{
				(sender as TextBox).Text = "2.3";
				MessageBox.Show("В поле размера можно вводить только цифры, в качестве разделителя между целой и дробной частью может использоваться точка или запятая");
			}
		}

		private void tbxDensityKnittingIn10SmHoriz_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				int num = Convert.ToInt32((sender as TextBox).Text);
				scheme.Param.HorizontalSizeOfBlockAtSchemeInMm = 100f / ((float)num + 0f);
			}
			catch (Exception)
			{
				(sender as TextBox).Text = "20";
				MessageBox.Show("В поле размера можно вводить только цифры");
			}
		}

		private void tbxDensityKnittingIn10SmVertic_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				int num = Convert.ToInt32((sender as TextBox).Text);
				scheme.Param.VerticalSizeOfBlockAtSchemeInMm = 100f / ((float)num + 0f);
			}
			catch (Exception)
			{
				(sender as TextBox).Text = "20";
				MessageBox.Show("В поле размера можно вводить только цифры");
			}
		}

		private void tbxNumberOfAdditionThread_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				int num = Convert.ToInt32((sender as TextBox).Text);
			}
			catch (Exception)
			{
				(sender as TextBox).Text = "2";
				MessageBox.Show("В это поле можно вводить только цифры");
			}
		}

		private void tbxSizeOfBlockInPreview_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				int num = Convert.ToInt32((sender as TextBox).Text);
			}
			catch (Exception)
			{
				(sender as TextBox).Text = "12";
				MessageBox.Show("В это поле можно вводить только цифры");
			}
		}

		private void tbxSizeOfBlockInPrinting_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				int num = Convert.ToInt32((sender as TextBox).Text);
			}
			catch (Exception)
			{
				(sender as TextBox).Text = "20";
				MessageBox.Show("В это поле можно вводить только цифры");
			}
		}

		private void tbxPrintingResolution_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				int num = Convert.ToInt32((sender as TextBox).Text);
			}
			catch (Exception)
			{
				(sender as TextBox).Text = "300";
				MessageBox.Show("В это поле можно вводить только цифры");
			}
		}

		private void Window_Closing(object sender, CancelEventArgs e)
		{
			Бисерок.Properties.Settings.Default.Reload();
		}

		private void cbxLightestColorIsCanvas_Checked(object sender, RoutedEventArgs e)
		{
			try
			{
				cbxDarkestColorIsCanvas.IsChecked = false;
			}
			catch
			{
			}
		}

		private void cbxDarkestColorIsCanvas_Checked(object sender, RoutedEventArgs e)
		{
			try
			{
				cbxLightestColorIsCanvas.IsChecked = false;
			}
			catch
			{
			}
		}

		private void tbxWidthPageInBeads_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				int num = Convert.ToInt32((sender as TextBox).Text);
			}
			catch (Exception)
			{
				(sender as TextBox).Text = "60";
				MessageBox.Show("В это поле можно вводить только цифры");
			}
		}

		private void tbxHeightPageInBeads_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				int num = Convert.ToInt32((sender as TextBox).Text);
			}
			catch (Exception)
			{
				(sender as TextBox).Text = "90";
				MessageBox.Show("В это поле можно вводить только цифры");
			}
		}
	}
}
