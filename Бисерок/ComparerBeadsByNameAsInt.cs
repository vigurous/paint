using System;
using System.Collections.Generic;

namespace Бисерок
{
	internal class ComparerBeadsByNameAsInt : IComparer<Beads>
	{
		public int Compare(Beads x, Beads y)
		{
			bool flag = true;
			bool flag2 = true;
			int num = 0;
			int num2 = 0;
			try
			{
				num = Convert.ToInt32(x.name);
			}
			catch
			{
				flag = false;
			}
			try
			{
				num2 = Convert.ToInt32(y.name);
			}
			catch
			{
				flag2 = false;
			}
			if (flag & flag2)
			{
				if (num == num2)
				{
					return 0;
				}
				if (num > num2)
				{
					return 1;
				}
				return -1;
			}
			if (!flag && !flag2)
			{
				return 0;
			}
			if (!flag)
			{
				return 1;
			}
			return -1;
		}
	}
}
