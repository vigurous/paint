using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Бисерок
{
	public partial class wndSetCatalogueNameAndArticleOfScheme : Window, IComponentConnector
	{
		public string name = "";

		public string article = "";

		public wndSetCatalogueNameAndArticleOfScheme()
		{
			InitializeComponent();
			if (!Settings.isItSpecialVersionOfBiserokForMaxim && !Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina && !Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
			{
				grd.RowDefinitions[0].Height = new GridLength(0.0);
				base.Height = 140.0;
			}
			if (Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
			{
				grd.RowDefinitions[1].Height = new GridLength(0.0);
				base.Height = 140.0;
			}
		}

		private void btnOk_Click(object sender, RoutedEventArgs e)
		{
			if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina || Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
			{
				name = tbxName.Text;
			}
			if (!Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
			{
				article = tbxArticle.Text;
			}
			base.Close();
		}
	}
}
