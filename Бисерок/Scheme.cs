using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Бисерок.ServiceReference;

namespace Бисерок
{
	[Serializable]
	public class Scheme
	{
		[NonSerialized]
		public string pathToSrcImage;

		public List<BeadsManufacturer> beadsManufacturer = new List<BeadsManufacturer>();

		public List<Beads> beadsEnableToUse = new List<Beads>();

		public SchemeParameters Param = new SchemeParameters();

		public Action lastActionWithReworkScheme;

		public Action lastActionWithHandEditingScheme;

		public bool IsImageLoaded;

		public bool IsBeadworkSourceCreated;

		public bool IsReworkSchemeLoaded;

		public bool IsHandEditingSchemeLoaded;

		public bool IsReworkSchemeEdited;

		public bool IsHandEditingSchemeEdited;

		[NonSerialized]
		public bool IsNeedToSave;

		public Bitmap BmpSourceImage;

		public Bitmap bmpSourceImageProcessingButNonScaled;

		public Bitmap BmpBeadworkSource;

		public Bitmap BmpBeadworkRework;

		public Bitmap BmpBeadworkHandEditing;

		public Bitmap BmpBackAndKnot;

		public const int MarginBorderPrintPages = 9;

		public int SqureCellSidePrintPages_Y;

		public int SqureCellSidePrintPages_X;

		public int SqureCellSidePreview_Y;

		public int SqureCellSidePreview_X;

		public float SqureCellSideSchemeForPrinting_Y;

		public float SqureCellSideSchemeForPrinting_X;

		public const int heightOfRowInBadgesInMiniLegend = 2;

		private int sizeOfBadge = 30;

		public List<Color> _srcColors = new List<Color>();

		public List<Beads> _resBeadsMain = new List<Beads>();

		public Pixel[,] blocksMain;

		public List<Beads> _resBeadsRework = new List<Beads>();

		public Block[,] blocksRework;

		public List<Beads> _resBeadsHandEditing = new List<Beads>();

		public Block[,] blocksHandEditing;

		[NonSerialized]
		private Bitmap PrintingBmp;

		[NonSerialized]
		private PrintDocument pd;

		[NonSerialized]
		private ImageFormat format = ImageFormat.Png;

		[NonSerialized]
		private Brush _sbrForDrawBlockInKrestik = new SolidBrush(Color.FromArgb(255, 165, 165, 165));

		[NonSerialized]
		private Bitmap _bmpPatternOfKrestikForDrawBlock = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Images/krest6.png");

		[NonSerialized]
		private Bitmap _bmpPatternOfPetelkaForDrawBlock = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Images/petelka1.png");

		[NonSerialized]
		private Bitmap _bmpPatternOfStrazaForDrawBlock = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Images/straza.png");

		public List<Block> _blocksMain;

		public List<Block> _blocksRework;

		public List<Block> _blocksHandEditing;

		public List<ColoringContour> contoursMain = new List<ColoringContour>();

		private NumberAndColorOfContour[,] numberAndColorOfContourMain;

		public List<ColoringContour> contoursHandEditing = new List<ColoringContour>();

		private NumberAndColorOfContour[,] numberAndColorOfContourHandEditing;

		public string nameScheme = "";

		public string articleScheme = "";

		public List<BackStitch> backstitchHandEditing = new List<BackStitch>();

		public List<Knot> knotHandEditing = new List<Knot>();

		[NonSerialized]
		private static float? _KfForFontSize;

		public string ExtSourceImage
		{
			get;
			set;
		}

		private int v => Settings.v;

		public int widthOfColumnInBadgesInMiniLegend
		{
			get
			{
				if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
				{
					return 13;
				}
				if (Settings.isItSpecialVersionOfBiserokForMaxim)
				{
					return 16;
				}
				return 25;
			}
		}

		public float CountBeadesInSm_X => 10f / Param.HorizontalSizeOfBlockAtSchemeInMm;

		public float CountBeadesInSm_Y => 10f / Param.VerticalSizeOfBlockAtSchemeInMm;

		public int WidthSourceImageInPx
		{
			get;
			set;
		}

		public int HeightSourceImageInPx
		{
			get;
			set;
		}

		public int CountResColorsSource
		{
			get
			{
				return _resBeadsMain.Count();
			}
			set
			{
			}
		}

		public int CountResColorsRework
		{
			get
			{
				return _resBeadsRework.Count();
			}
			set
			{
			}
		}

		public int CountResColorsHandEditing
		{
			get
			{
				return _resBeadsHandEditing.Count();
			}
			set
			{
			}
		}

		public static float KfSystemFontSizes
		{
			get
			{
				if (_KfForFontSize.HasValue)
				{
					return _KfForFontSize.Value;
				}
				Bitmap bitmap = new Bitmap(500, 500);
				Graphics graphics = Graphics.FromImage(bitmap);
				Font font = new Font("Tahoma", 20f);
				float width = graphics.MeasureString("съешь ещё этих мягких французских булок, да выпей чаю", font).Width;
				_KfForFontSize = 750f / width;
				graphics.Dispose();
				bitmap.Dispose();
				return _KfForFontSize.Value;
			}
		}

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			format = ImageFormat.Png;
			_sbrForDrawBlockInKrestik = new SolidBrush(Color.FromArgb(255, 165, 165, 165));
			_bmpPatternOfKrestikForDrawBlock = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Images/krest6.png");
			_bmpPatternOfPetelkaForDrawBlock = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Images/petelka1.png");
			_bmpPatternOfStrazaForDrawBlock = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Images/straza.png");
			if (_blocksMain != null && _blocksMain.Count() > 0)
			{
				blocksMain = new Block[Param.WidthInBead, Param.HeightInBead];
				foreach (Block item in _blocksMain)
				{
					blocksMain[item.X, item.Y] = item;
				}
				_blocksMain.Clear();
			}
			if (_blocksRework != null && _blocksRework.Count() > 0)
			{
				blocksRework = new Block[Param.WidthInBead, Param.HeightInBead];
				foreach (Block item2 in _blocksRework)
				{
					blocksRework[item2.X, item2.Y] = item2;
				}
				_blocksRework.Clear();
			}
			if (_blocksHandEditing != null && _blocksHandEditing.Count() > 0)
			{
				blocksHandEditing = new Block[Param.WidthInBead, Param.HeightInBead];
				foreach (Block item3 in _blocksHandEditing)
				{
					blocksHandEditing[item3.X, item3.Y] = item3;
				}
				_blocksHandEditing.Clear();
			}
			if (IsHandEditingSchemeLoaded && BmpBackAndKnot == null && Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				BmpBackAndKnot = new Bitmap(CalcBmpBackAndKnotWidthInPx(), CalcBmpBackAndKnotHeightInPx());
			}
			sizeOfBadge = 30;
			if (backstitchHandEditing == null)
			{
				backstitchHandEditing = new List<BackStitch>();
			}
			if (knotHandEditing == null)
			{
				knotHandEditing = new List<Knot>();
			}
		}

		public void LoadSourceImage(string path, Bitmap processingSourceBmp = null)
		{
			DisposeAllBitmaps();
			IsBeadworkSourceCreated = false;
			IsNeedToSave = false;
			if (processingSourceBmp == null)
			{
				BmpSourceImage = new Bitmap(path);
			}
			else
			{
				BmpSourceImage = new Bitmap(processingSourceBmp);
			}
			pathToSrcImage = path;
			IsImageLoaded = true;
			ExtSourceImage = Path.GetExtension(path);
			WidthSourceImageInPx = BmpSourceImage.Width;
			HeightSourceImageInPx = BmpSourceImage.Height;
			_srcColors = new List<Color>();
			_resBeadsMain = new List<Beads>();
			blocksMain = null;
			_resBeadsRework = new List<Beads>();
			blocksRework = null;
			_resBeadsHandEditing = new List<Beads>();
			blocksHandEditing = null;
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				bmpSourceImageProcessingButNonScaled = new Bitmap(BmpSourceImage);
			}
		}

		public void LoadProcessedSourceImage(string path, Bitmap processingSourceBmp = null)
		{
			DisposeAllBitmaps();
			IsBeadworkSourceCreated = false;
			IsNeedToSave = false;
			if (processingSourceBmp == null)
			{
				BmpSourceImage = new Bitmap(path);
			}
			else
			{
				BmpSourceImage = new Bitmap(processingSourceBmp);
			}
			pathToSrcImage = path;
			IsImageLoaded = true;
			ExtSourceImage = Path.GetExtension(path);
			WidthSourceImageInPx = BmpSourceImage.Width;
			HeightSourceImageInPx = BmpSourceImage.Height;
			_srcColors = new List<Color>();
			_resBeadsMain = new List<Beads>();
			blocksMain = null;
			_resBeadsRework = new List<Beads>();
			blocksRework = null;
			_resBeadsHandEditing = new List<Beads>();
			blocksHandEditing = null;
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				bmpSourceImageProcessingButNonScaled = new Bitmap(BmpSourceImage);
			}
		}

		public void DisposeAllBitmaps()
		{
			if (BmpSourceImage != null)
			{
				BmpSourceImage.Dispose();
			}
			DisposeBeadworkViewBitmaps();
		}

		private void DisposeBeadworkViewBitmaps()
		{
			if (BmpBeadworkSource != null)
			{
				BmpBeadworkSource.Dispose();
			}
			if (BmpBeadworkRework != null)
			{
				BmpBeadworkRework.Dispose();
			}
		}

		public float GetSchemesWidthInSmFromHeightInSm(float heightInSm)
		{
			float f = ((float)WidthSourceImageInPx + 0f) / ((float)HeightSourceImageInPx + 0f) * (heightInSm + 0f);
			return RoundFloatToTenths(f);
		}

		public float GetSchemesHeightInSmFromWidthInSm(float widthInSm)
		{
			float f = ((float)HeightSourceImageInPx + 0f) / ((float)WidthSourceImageInPx + 0f) * (widthInSm + 0f);
			return RoundFloatToTenths(f);
		}

		public int GetSchemesWidthInBlockFromHeightInBlock(int heightInBlock)
		{
			float num = Param.HorizontalSizeOfBlockAtSchemeInMm / Param.VerticalSizeOfBlockAtSchemeInMm;
			return (int)Math.Round((double)(((float)WidthSourceImageInPx + 0f) / ((float)HeightSourceImageInPx + 0f) * ((float)heightInBlock + 0f) / num));
		}

		public int GetSchemesHeightInBlockFromWidthInBlock(int widthInBlock)
		{
			float num = Param.VerticalSizeOfBlockAtSchemeInMm / Param.HorizontalSizeOfBlockAtSchemeInMm;
			return (int)Math.Round((double)(((float)HeightSourceImageInPx + 0f) / ((float)WidthSourceImageInPx + 0f) * ((float)widthInBlock + 0f) / num));
		}

		public int GetSchemesWidthInBlockFromWidthInSm(float sizeInSm)
		{
			return (int)Math.Round((double)(sizeInSm * 10f / Param.HorizontalSizeOfBlockAtSchemeInMm));
		}

		public int GetSchemesHeightInBlockFromHeightInSm(float sizeInSm)
		{
			return (int)Math.Round((double)(sizeInSm * 10f / Param.VerticalSizeOfBlockAtSchemeInMm));
		}

		public float GetSchemesWidthInSmFromWidthInBlock(int sizeInBlock)
		{
			return RoundFloatToTenths((float)sizeInBlock * Param.HorizontalSizeOfBlockAtSchemeInMm / 10f);
		}

		public float GetSchemesHeightInSmFromHeightInBlock(int sizeInBlock)
		{
			return RoundFloatToTenths((float)sizeInBlock * Param.VerticalSizeOfBlockAtSchemeInMm / 10f);
		}

		private static float RoundFloatToTenths(float f)
		{
			return (float)(int)Math.Round((double)(10f * f)) / 10f;
		}

		public void ScaleSourceImageInColoring()
		{
			float num = (float)Param.countOfPixelsInCmAtContoursMapOfColoring;
			if (v == 1)
			{
				num = 40f;
			}
			float num2 = (float)Math.Round((double)(num * Math.Max(Param.WidthColoringInSm, Param.HeightColoringInSm)));
			float num3 = (float)Math.Max(WidthSourceImageInPx, HeightSourceImageInPx) / num2;
			int width = (int)Math.Round((double)(((float)WidthSourceImageInPx + 0f) / num3));
			int height = (int)Math.Round((double)(((float)HeightSourceImageInPx + 0f) / num3));
			if (Param.isNotChangeSourceImageInColoring)
			{
				Bitmap bitmap = new Bitmap(width, height);
				Graphics graphics = Graphics.FromImage(bitmap);
				graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
				graphics.DrawImage(bmpSourceImageProcessingButNonScaled, new Rectangle(0, 0, width, height), new Rectangle(0, 0, BmpSourceImage.Width, BmpSourceImage.Height), GraphicsUnit.Pixel);
				graphics.Dispose();
				BmpSourceImage = new Bitmap(bitmap);
				bitmap.Dispose();
			}
			else
			{
				BmpSourceImage = new Bitmap(bmpSourceImageProcessingButNonScaled, width, height);
			}
		}

		public bool GeneratePrimaryScheme()
		{
			try
			{
				if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
				{
					SqureCellSidePreview_Y = Param.SqureCellSidePreview_Y;
					SqureCellSidePreview_X = (int)Math.Round((double)(((float)SqureCellSidePreview_Y + 0f) * (Param.HorizontalSizeOfBlockAtSchemeInMm / Param.VerticalSizeOfBlockAtSchemeInMm)));
				}
				else
				{
					SchemeParameters param = Param;
					int squreCellSidePreview_X = param.SqureCellSidePreview_Y = 1;
					SqureCellSidePreview_Y = (SqureCellSidePreview_X = squreCellSidePreview_X);
				}
				DisposeBeadworkViewBitmaps();
				_srcColors = new List<Color>();
				_resBeadsMain = new List<Beads>();
				blocksMain = null;
				SetSourceColors(Param.isNotChangeSourceImageInColoring);
				if (Param.BiserType == BiserType.ForManufacturers)
				{
					FillBeadsEnableToUse();
				}
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && Param.isNotChangeSourceImageInColoring)
				{
					Param.IsMixingColors = false;
					Beads item = new Beads();
					for (int l = 0; l < _srcColors.Count(); l++)
					{
						float num2 = 3.40282347E+38f;
						for (int m = 0; m < beadsEnableToUse.Count(); m++)
						{
							float distanceBetweenColors = GetDistanceBetweenColors(_srcColors[l], beadsEnableToUse[m].clr);
							if (distanceBetweenColors < num2)
							{
								num2 = distanceBetweenColors;
								item = beadsEnableToUse[m];
							}
						}
						if (!_resBeadsMain.Contains(item))
						{
							_resBeadsMain.Add(item);
						}
						if (_resBeadsMain.Count() == Param.CountDesiredColors)
						{
							break;
						}
					}
				}
				else
				{
					GenerateRandomResColors(Param.CountDesiredColors);
					SetResColorsByKmeans();
					if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && Param.BiserType == BiserType.ForManufacturers && Param.IsMixingColors)
					{
						_resBeadsMain = DefineMixingColorsInColoringInMixingColorsMode(_resBeadsMain);
					}
					else if (Param.BiserType == BiserType.ForManufacturers)
					{
						SetResBeadsFromNearestManufacturersBeads();
						Dictionary<Beads, int> dictionary = new Dictionary<Beads, int>();
						for (int n = 0; n < beadsEnableToUse.Count; n++)
						{
							try
							{
								dictionary.Add(beadsEnableToUse[n], 0);
							}
							catch
							{
							}
						}
						int index = -1;
						for (int num3 = 0; num3 < _srcColors.Count; num3++)
						{
							float num2 = 3.40282347E+38f;
							for (int num4 = 0; num4 < beadsEnableToUse.Count; num4++)
							{
								float distanceBetweenColors = GetDistanceBetweenColors(_srcColors[num3], beadsEnableToUse[num4].clr);
								if (distanceBetweenColors < num2)
								{
									num2 = distanceBetweenColors;
									index = num4;
								}
							}
							Dictionary<Beads, int> dictionary2 = dictionary;
							Beads key = beadsEnableToUse[index];
							dictionary2[key]++;
						}
						int count = beadsEnableToUse.Count;
						while (_resBeadsMain.Count() < Param.CountDesiredColors)
						{
							int num5 = 0;
							Beads item2 = new Beads();
							for (int num6 = 0; num6 < count; num6++)
							{
								if (dictionary[beadsEnableToUse[num6]] > num5 && !_resBeadsMain.Contains(beadsEnableToUse[num6]))
								{
									num5 = dictionary[beadsEnableToUse[num6]];
									item2 = beadsEnableToUse[num6];
								}
							}
							if (num5 == 0)
							{
								break;
							}
							_resBeadsMain.Add(item2);
						}
					}
				}
				int num7 = SqureCellSidePreview_Y * Param.HeightInBead;
				int newWidthBmp = SqureCellSidePreview_X * Param.WidthInBead;
				BmpBeadworkSource = new Bitmap(newWidthBmp, num7);
				Graphics graphics = Graphics.FromImage(BmpBeadworkSource);
				if (Param.isNotChangeSourceImageInColoring)
				{
					graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
				}
				graphics.DrawImage(BmpSourceImage, new Rectangle(0, 0, BmpBeadworkSource.Width, BmpBeadworkSource.Height));
				graphics.Dispose();
				BitmapData bitmapData = BmpBeadworkSource.LockBits(new Rectangle(0, 0, BmpBeadworkSource.Width, BmpBeadworkSource.Height), ImageLockMode.ReadWrite, BmpBeadworkSource.PixelFormat);
				try
				{
					int num8 = (BmpBeadworkSource.PixelFormat == (BmpBeadworkSource.PixelFormat | PixelFormat.Alpha)) ? 4 : 3;
					int num9 = BmpBeadworkSource.Width * BmpBeadworkSource.Height * num8;
					byte[] array = new byte[num9];
					IntPtr scan = bitmapData.Scan0;
					Marshal.Copy(scan, array, 0, array.Length);
					int num10 = 0;
					if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
					{
						blocksMain = new PixelForColoring[Param.WidthInBead, Param.HeightInBead];
					}
					else
					{
						blocksMain = new Block[Param.WidthInBead, Param.HeightInBead];
					}
					for (int num11 = 0; num11 < Param.HeightInBead; num11++)
					{
						int num12 = Param.WidthInBead;
						if (Param.Type == SchemeType.Block && num11 % 2 != 0)
						{
							num12--;
						}
						for (int num13 = 0; num13 < num12; num13++)
						{
							int num14;
							int num15;
							int num16 = num15 = (num14 = 0);
							int num17 = 0;
							for (int num18 = 0; num18 < SqureCellSidePreview_Y; num18++)
							{
								for (int num19 = 0; num19 < SqureCellSidePreview_X; num19++)
								{
									try
									{
										int num20 = (num11 * SqureCellSidePreview_Y + num18) * bitmapData.Stride + (num13 * SqureCellSidePreview_X + num19) * num8;
										num14 += array[num20];
										num15 += array[num20 + 1];
										num16 += array[num20 + 2];
										num17++;
									}
									catch (Exception)
									{
									}
								}
							}
							num14 = (int)Math.Round((double)(((float)num14 + 0f) / ((float)num17 + 0f)));
							num15 = (int)Math.Round((double)(((float)num15 + 0f) / ((float)num17 + 0f)));
							num16 = (int)Math.Round((double)(((float)num16 + 0f) / ((float)num17 + 0f)));
							Color srcColor = Color.FromArgb(255, num16, num15, num14);
							int num21 = num13 * SqureCellSidePreview_X;
							if (Param.Type == SchemeType.Block && num11 % 2 != 0)
							{
								num21 += SqureCellSidePreview_X / 2;
							}
							if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
							{
								Pixel[,] array2 = blocksMain;
								int num22 = num13;
								int num23 = num11;
								PixelForColoring obj2 = new PixelForColoring
								{
									SrcColor = srcColor
								};
								array2[num22, num23] = obj2;
							}
							else
							{
								Pixel[,] array3 = blocksMain;
								int num24 = num13;
								int num25 = num11;
								Block obj3 = new Block
								{
									PositionInScheme = (RectangleF)new Rectangle(num21, num11 * SqureCellSidePreview_Y, SqureCellSidePreview_X, SqureCellSidePreview_Y),
									SrcColor = srcColor,
									X = num13,
									Y = num11,
									IsVisible = true
								};
								array3[num24, num25] = obj3;
							}
							num10++;
						}
					}
					Marshal.Copy(array, 0, scan, array.Length);
				}
				finally
				{
					BmpBeadworkSource.UnlockBits(bitmapData);
				}
				List<Beads> _allUsingBeads = new List<Beads>();
				Beads beads = new Beads();
				int k;
				Color key2;
				for (k = 0; k < Param.HeightInBead; k++)
				{
					int num26 = Param.WidthInBead;
					if (Param.Type == SchemeType.Block && k % 2 != 0)
					{
						num26--;
					}
					if (Settings.hobbyProgramm == HobbyProgramm.Raskraska || !Param.IsWithDithering)
					{
						Parallel.For(0, num26, new ParallelOptions
						{
							MaxDegreeOfParallelism = Environment.ProcessorCount
						}, delegate(int j)
						{
							Beads beads2 = new Beads();
							Pixel pixel2 = blocksMain[j, k];
							float num56 = 3.40282347E+38f;
							foreach (Beads item3 in _resBeadsMain)
							{
								float distanceBetweenColors2 = GetDistanceBetweenColors(pixel2.SrcColor, item3.clr);
								if (!(distanceBetweenColors2 >= num56))
								{
									num56 = distanceBetweenColors2;
									beads2 = item3;
								}
							}
							if (pixel2 is Block)
							{
								(pixel2 as Block).ResBeads = beads2;
							}
							else if (pixel2 is PixelForColoring)
							{
								(pixel2 as PixelForColoring).clr = beads2.clr;
							}
							lock (_allUsingBeads)
							{
								_allUsingBeads.Add(beads2);
							}
						});
					}
					else
					{
						for (int num27 = 0; num27 < num26; num27++)
						{
							Pixel pixel = blocksMain[num27, k];
							float num2 = 3.40282347E+38f;
							foreach (Beads item4 in _resBeadsMain)
							{
								float distanceBetweenColors = GetDistanceBetweenColors(pixel.SrcColor, item4.clr);
								if (!(distanceBetweenColors >= num2))
								{
									num2 = distanceBetweenColors;
									beads = item4;
								}
							}
							if (pixel is Block)
							{
								(pixel as Block).ResBeads = beads;
							}
							else if (pixel is PixelForColoring)
							{
								(pixel as PixelForColoring).clr = beads.clr;
							}
							_allUsingBeads.Add(beads);
							if (Settings.hobbyProgramm != HobbyProgramm.Raskraska && Param.IsWithDithering)
							{
								key2 = pixel.SrcColor;
								byte r = key2.R;
								key2 = beads.clr;
								int deltaR = r - key2.R;
								key2 = pixel.SrcColor;
								byte g = key2.G;
								key2 = beads.clr;
								int deltaG = g - key2.G;
								key2 = pixel.SrcColor;
								byte b = key2.B;
								key2 = beads.clr;
								int deltaB = b - key2.B;
								SetDiffColorByDithering(Param.Type, 0.125f, num27 + 1, k, num27, k, ref blocksMain, deltaR, deltaG, deltaB);
								SetDiffColorByDithering(Param.Type, 0.125f, num27 + 2, k, num27, k, ref blocksMain, deltaR, deltaG, deltaB);
								SetDiffColorByDithering(Param.Type, 0.125f, num27 - 1, k + 1, num27, k, ref blocksMain, deltaR, deltaG, deltaB);
								SetDiffColorByDithering(Param.Type, 0.125f, num27, k + 1, num27, k, ref blocksMain, deltaR, deltaG, deltaB);
								SetDiffColorByDithering(Param.Type, 0.125f, num27 + 1, k + 1, num27, k, ref blocksMain, deltaR, deltaG, deltaB);
								SetDiffColorByDithering(Param.Type, 0.125f, num27, k + 2, num27, k, ref blocksMain, deltaR, deltaG, deltaB);
							}
						}
					}
				}
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && !Param.isNotChangeSourceImageInColoring)
				{
					int mediana = Param.Mediana;
					if (v > 1)
					{
						float num28 = (float)Param.countOfPixelsInCmAtContoursMapOfColoring;
						if (num28 > 40f)
						{
							mediana = (int)Math.Round((double)((float)mediana * (num28 / 40f)));
						}
					}
					int numberOfMedianaRepeats = Param.NumberOfMedianaRepeats;
					Color[,] tmpColors = new Color[newWidthBmp, num7];
					int num29 = 2 * newWidthBmp - 2;
					int num30 = 2 * num7 - 2;
					int num31 = newWidthBmp - 1;
					int num32 = num7 - 1;
					for (int num33 = 0; num33 < numberOfMedianaRepeats; num33++)
					{
						int toExclusive = num7 - mediana;
						Parallel.For(mediana, toExclusive, new ParallelOptions
						{
							MaxDegreeOfParallelism = Environment.ProcessorCount
						}, delegate(int i)
						{
							Dictionary<Color, int> dictionary8 = new Dictionary<Color, int>();
							Color[] array4 = new Color[mediana * 2 + 1];
							int num49 = newWidthBmp - mediana;
							int max;
							for (int num50 = mediana; num50 < num49; num50++)
							{
								KeyValuePair<Color, int> keyValuePair2;
								if (num50 == mediana)
								{
									try
									{
										Color clr2;
										for (int num51 = -mediana; num51 <= mediana; num51++)
										{
											int num52 = num51 + mediana;
											for (int num53 = -mediana; num53 <= mediana; num53++)
											{
												int num54 = num50 + num53;
												int num55 = i + num51;
												clr2 = (blocksMain[num54, num55] as PixelForColoring).clr;
												if (num53 == -mediana)
												{
													array4[num52] = clr2;
												}
												if (dictionary8.Keys.Contains(clr2))
												{
													Dictionary<Color, int> dictionary9 = dictionary8;
													Color key3 = clr2;
													dictionary9[key3]++;
												}
												else
												{
													dictionary8.Add(clr2, 1);
												}
											}
										}
										max = dictionary8.Values.Max();
										keyValuePair2 = dictionary8.First((KeyValuePair<Color, int> nn) => nn.Value == max);
										clr2 = keyValuePair2.Key;
										tmpColors[num50, i] = clr2;
									}
									catch (Exception)
									{
									}
								}
								else
								{
									try
									{
										Color clr2;
										for (int num51 = -mediana; num51 <= mediana; num51++)
										{
											int num52 = num51 + mediana;
											int num53 = mediana;
											int num54 = num50 + num53;
											int num55 = i + num51;
											clr2 = (blocksMain[num54, num55] as PixelForColoring).clr;
											Color key3;
											if (dictionary8.Keys.Contains(clr2))
											{
												Dictionary<Color, int> dictionary10 = dictionary8;
												key3 = clr2;
												dictionary10[key3]++;
											}
											else
											{
												dictionary8.Add(clr2, 1);
											}
											Dictionary<Color, int> dictionary11 = dictionary8;
											key3 = array4[num52];
											dictionary11[key3]--;
											num53 = -mediana;
											num54 = num50 + num53;
											array4[num52] = (blocksMain[num54, num55] as PixelForColoring).clr;
										}
										max = dictionary8.Values.Max();
										keyValuePair2 = dictionary8.First((KeyValuePair<Color, int> nn) => nn.Value == max);
										clr2 = keyValuePair2.Key;
										tmpColors[num50, i] = clr2;
									}
									catch (Exception)
									{
									}
								}
							}
							dictionary8.Clear();
						});
						Dictionary<Color, int> dictionary3 = new Dictionary<Color, int>();
						KeyValuePair<Color, int> keyValuePair;
						for (int num34 = 0; num34 < mediana; num34++)
						{
							for (int num35 = 0; num35 < newWidthBmp; num35++)
							{
								dictionary3 = new Dictionary<Color, int>();
								try
								{
									Color clr;
									for (int num36 = -mediana; num36 <= mediana; num36++)
									{
										for (int num37 = -mediana; num37 <= mediana; num37++)
										{
											int num38 = num35 + num37;
											int num39 = num34 + num36;
											if (num38 < 0)
											{
												num38 = -num38;
											}
											else if (num38 > num31)
											{
												num38 = num29 - num38;
											}
											if (num39 < 0)
											{
												num39 = -num39;
											}
											else if (num39 > num32)
											{
												num39 = num30 - num39;
											}
											clr = (blocksMain[num38, num39] as PixelForColoring).clr;
											if (!dictionary3.Keys.Contains(clr))
											{
												dictionary3.Add(clr, 1);
											}
											else
											{
												Dictionary<Color, int> dictionary4 = dictionary3;
												key2 = clr;
												dictionary4[key2]++;
											}
										}
									}
									keyValuePair = (from nn in dictionary3
									orderby nn.Value descending
									select nn).ElementAt(0);
									clr = keyValuePair.Key;
									tmpColors[num35, num34] = clr;
								}
								catch
								{
								}
							}
						}
						for (int num40 = num7 - mediana; num40 < num7; num40++)
						{
							for (int num41 = 0; num41 < newWidthBmp; num41++)
							{
								dictionary3 = new Dictionary<Color, int>();
								try
								{
									Color clr;
									for (int num36 = -mediana; num36 <= mediana; num36++)
									{
										for (int num37 = -mediana; num37 <= mediana; num37++)
										{
											int num38 = num41 + num37;
											int num39 = num40 + num36;
											if (num38 < 0)
											{
												num38 = -num38;
											}
											else if (num38 > num31)
											{
												num38 = num29 - num38;
											}
											if (num39 < 0)
											{
												num39 = -num39;
											}
											else if (num39 > num32)
											{
												num39 = num30 - num39;
											}
											clr = (blocksMain[num38, num39] as PixelForColoring).clr;
											if (!dictionary3.Keys.Contains(clr))
											{
												dictionary3.Add(clr, 1);
											}
											else
											{
												Dictionary<Color, int> dictionary5 = dictionary3;
												key2 = clr;
												dictionary5[key2]++;
											}
										}
									}
									keyValuePair = (from nn in dictionary3
									orderby nn.Value descending
									select nn).ElementAt(0);
									clr = keyValuePair.Key;
									tmpColors[num41, num40] = clr;
								}
								catch
								{
								}
							}
						}
						for (int num42 = 0; num42 < num7; num42++)
						{
							for (int num43 = 0; num43 < mediana; num43++)
							{
								dictionary3 = new Dictionary<Color, int>();
								try
								{
									Color clr;
									for (int num36 = -mediana; num36 <= mediana; num36++)
									{
										for (int num37 = -mediana; num37 <= mediana; num37++)
										{
											int num38 = num43 + num37;
											int num39 = num42 + num36;
											if (num38 < 0)
											{
												num38 = -num38;
											}
											else if (num38 > num31)
											{
												num38 = num29 - num38;
											}
											if (num39 < 0)
											{
												num39 = -num39;
											}
											else if (num39 > num32)
											{
												num39 = num30 - num39;
											}
											clr = (blocksMain[num38, num39] as PixelForColoring).clr;
											if (!dictionary3.Keys.Contains(clr))
											{
												dictionary3.Add(clr, 1);
											}
											else
											{
												Dictionary<Color, int> dictionary6 = dictionary3;
												key2 = clr;
												dictionary6[key2]++;
											}
										}
									}
									keyValuePair = (from nn in dictionary3
									orderby nn.Value descending
									select nn).ElementAt(0);
									clr = keyValuePair.Key;
									tmpColors[num43, num42] = clr;
								}
								catch
								{
								}
							}
						}
						for (int num44 = 0; num44 < num7; num44++)
						{
							for (int num45 = newWidthBmp - mediana; num45 < newWidthBmp; num45++)
							{
								dictionary3 = new Dictionary<Color, int>();
								try
								{
									Color clr;
									for (int num36 = -mediana; num36 <= mediana; num36++)
									{
										for (int num37 = -mediana; num37 <= mediana; num37++)
										{
											int num38 = num45 + num37;
											int num39 = num44 + num36;
											if (num38 < 0)
											{
												num38 = -num38;
											}
											else if (num38 > num31)
											{
												num38 = num29 - num38;
											}
											if (num39 < 0)
											{
												num39 = -num39;
											}
											else if (num39 > num32)
											{
												num39 = num30 - num39;
											}
											clr = (blocksMain[num38, num39] as PixelForColoring).clr;
											if (!dictionary3.Keys.Contains(clr))
											{
												dictionary3.Add(clr, 1);
											}
											else
											{
												Dictionary<Color, int> dictionary7 = dictionary3;
												key2 = clr;
												dictionary7[key2]++;
											}
										}
									}
									keyValuePair = (from nn in dictionary3
									orderby nn.Value descending
									select nn).ElementAt(0);
									clr = keyValuePair.Key;
									tmpColors[num45, num44] = clr;
								}
								catch
								{
								}
							}
						}
						for (int num46 = 0; num46 < num7; num46++)
						{
							for (int num47 = 0; num47 < newWidthBmp; num47++)
							{
								(blocksMain[num47, num46] as PixelForColoring).clr = tmpColors[num47, num46];
							}
						}
					}
					tmpColors = null;
				}
				_allUsingBeads = _allUsingBeads.Distinct().ToList();
				for (int num48 = 0; num48 < _resBeadsMain.Count(); num48++)
				{
					try
					{
						if (!_allUsingBeads.Contains(_resBeadsMain[num48]))
						{
							_resBeadsMain.RemoveAt(num48);
							num48--;
						}
					}
					catch
					{
					}
				}
				DrawPrimaryEmbroideryView();
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					DefineContoursOfColoringAndRedrawingColoringView(BmpBeadworkSource, ref numberAndColorOfContourMain, ref contoursMain);
					RemoveSmallContoursFromColoring(ref BmpBeadworkSource, ref blocksMain, ref numberAndColorOfContourMain, ref contoursMain);
					UpdateColoringColorsCountAndCalcSquareInSm2ForEach(ref _resBeadsMain, BmpBeadworkSource.Width, BmpBeadworkSource.Height, numberAndColorOfContourMain, contoursMain);
					GC.Collect(GC.MaxGeneration);
				}
				IsBeadworkSourceCreated = true;
				IsNeedToSave = true;
				return true;
			}
			catch (Exception exc)
			{
				Site.SendExceptionInfo(exc, false);
				return false;
			}
		}

		private void UpdateColoringColorsCountAndCalcSquareInSm2ForEach(ref List<Beads> _resBeads, int bmpBeadworkWidth, int bmpBeadworkHeight, NumberAndColorOfContour[,] numberAndColorOfContour, List<ColoringContour> contours)
		{
			int num = 0;
			float widthColoringInSm = Param.WidthColoringInSm;
			float num2 = ((float)HeightSourceImageInPx + 0f) / ((float)WidthSourceImageInPx + 0f) * widthColoringInSm;
			byte R;
			byte G;
			byte B;
			for (int i = 0; i < _resBeads.Count(); i++)
			{
				try
				{
					Color clr = _resBeads[i].clr;
					R = clr.R;
					clr = _resBeads[i].clr;
					G = clr.G;
					clr = _resBeads[i].clr;
					B = clr.B;
					if (!contours.Any(delegate(ColoringContour ctr)
					{
						NumberAndColorOfContour[,] array4 = numberAndColorOfContour;
						Point point2 = ctr.points.First();
						int x4 = point2.X;
						point2 = ctr.points.First();
						if (array4[x4, point2.Y].R == R)
						{
							NumberAndColorOfContour[,] array5 = numberAndColorOfContour;
							point2 = ctr.points.First();
							int x5 = point2.X;
							point2 = ctr.points.First();
							if (array5[x5, point2.Y].G == G)
							{
								NumberAndColorOfContour[,] array6 = numberAndColorOfContour;
								point2 = ctr.points.First();
								int x6 = point2.X;
								point2 = ctr.points.First();
								return array6[x6, point2.Y].B == B;
							}
						}
						return false;
					}))
					{
						_resBeads.RemoveAt(i);
						i--;
					}
					else
					{
						int num3 = contours.Where(delegate(ColoringContour ctr)
						{
							NumberAndColorOfContour[,] array = numberAndColorOfContour;
							Point point = ctr.points.First();
							int x = point.X;
							point = ctr.points.First();
							if (array[x, point.Y].R == R)
							{
								NumberAndColorOfContour[,] array2 = numberAndColorOfContour;
								point = ctr.points.First();
								int x2 = point.X;
								point = ctr.points.First();
								if (array2[x2, point.Y].G == G)
								{
									NumberAndColorOfContour[,] array3 = numberAndColorOfContour;
									point = ctr.points.First();
									int x3 = point.X;
									point = ctr.points.First();
									return array3[x3, point.Y].B == B;
								}
							}
							return false;
						}).Sum((ColoringContour _c) => _c.points.Count());
						num += num3;
						float num4 = ((float)num3 + 0f) / ((float)(bmpBeadworkWidth * bmpBeadworkHeight) + 0f) * widthColoringInSm * num2;
						num4 = (float)(int)Math.Round((double)(num4 * 10f)) / 10f;
						_resBeads[i].squareInSm2 = num4;
					}
				}
				catch
				{
				}
			}
		}

		private string GetStringCountOfMlForPaints(float squareInSm2, int numberOfCharatersAfterPoint)
		{
			float num = (float)(int)Math.Round((double)(GetCountOfMlForPaints(squareInSm2) * (float)Math.Pow(10.0, (double)numberOfCharatersAfterPoint))) / (float)Math.Pow(10.0, (double)numberOfCharatersAfterPoint);
			if (num == 0f && numberOfCharatersAfterPoint == 1)
			{
				num = 0.1f;
			}
			return num.ToString();
		}

		private float GetCountOfMlForPaints(float squareInSm2)
		{
			return squareInSm2 / 10000f * 250f;
		}

		private void FillBeadsEnableToUse()
		{
			beadsEnableToUse.Clear();
			foreach (BeadsManufacturer item in (from bm in beadsManufacturer
			where bm.isUsing
			select bm).ToList())
			{
				foreach (BeadsSeries item2 in (from bs in item.beadsSeries
				where bs.isUsing
				select bs).ToList())
				{
					foreach (Beads item3 in (from bb in item2.beads
					where bb.isUsing
					select bb).ToList())
					{
						beadsEnableToUse.Add(item3);
					}
				}
			}
		}

		private void SetResBeadsFromNearestManufacturersBeads()
		{
			Beads value = new Beads();
			for (int i = 0; i < _resBeadsMain.Count(); i++)
			{
				float num = 3.40282347E+38f;
				for (int j = 0; j < beadsEnableToUse.Count(); j++)
				{
					float distanceBetweenColors = GetDistanceBetweenColors(_resBeadsMain[i].clr, beadsEnableToUse[j].clr);
					if (distanceBetweenColors < num)
					{
						num = distanceBetweenColors;
						value = beadsEnableToUse[j];
					}
				}
				_resBeadsMain[i] = value;
			}
			_resBeadsMain = _resBeadsMain.Distinct().ToList();
		}

		private void GenerateRandomResColors(int desiredCountColors)
		{
			Random random = new Random(DateTime.Now.Millisecond);
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && Param.isNotChangeSourceImageInColoring)
			{
				desiredCountColors *= 4;
			}
			for (int i = 0; i < desiredCountColors; i++)
			{
				_resBeadsMain.Add(new Beads
				{
					clr = Color.FromArgb(255, random.Next(0, 256), random.Next(0, 256), random.Next(0, 256))
				});
			}
		}

		private void SetResColorsByKmeans()
		{
			ConcurrentDictionary<int, int> accordanceColor = new ConcurrentDictionary<int, int>();
			for (int k = 0; k < _srcColors.Count(); k++)
			{
				accordanceColor.TryAdd(k, -1);
			}
			bool isEnd = false;
			while (!isEnd)
			{
				isEnd = true;
				int resBeadsCount = _resBeadsMain.Count;
				int count = _srcColors.Count;
				Parallel.For(0, count, new ParallelOptions
				{
					MaxDegreeOfParallelism = Environment.ProcessorCount
				}, delegate(int i)
				{
					float num = 3.40282347E+38f;
					int num2 = -1;
					for (int l = 0; l < resBeadsCount; l++)
					{
						float distanceBetweenColors = GetDistanceBetweenColors(_srcColors[i], _resBeadsMain[l].clr);
						if (!(distanceBetweenColors >= num))
						{
							num = distanceBetweenColors;
							num2 = l;
						}
					}
					if (accordanceColor[i] != num2)
					{
						accordanceColor[i] = num2;
						isEnd = false;
					}
				});
				if (!isEnd)
				{
					Parallel.For(0, resBeadsCount, new ParallelOptions
					{
						MaxDegreeOfParallelism = Environment.ProcessorCount
					}, delegate(int i)
					{
						int red = 0;
						int green = 0;
						int blue = 0;
						Dictionary<int, int> source2 = (from ac in accordanceColor
						where ac.Value == i
						select ac).ToDictionary((KeyValuePair<int, int> ac) => ac.Key, (KeyValuePair<int, int> ac) => ac.Value);
						if (source2.Any())
						{
							red = (int)Math.Round(source2.Average((KeyValuePair<int, int> a) => _srcColors[a.Key].R));
							green = (int)Math.Round(source2.Average((KeyValuePair<int, int> a) => _srcColors[a.Key].G));
							blue = (int)Math.Round(source2.Average((KeyValuePair<int, int> a) => _srcColors[a.Key].B));
						}
						_resBeadsMain[i].clr = Color.FromArgb(255, red, green, blue);
					});
				}
			}
			List<int> list = new List<int>();
			int j;
			for (j = 0; j < _resBeadsMain.Count(); j++)
			{
				Dictionary<int, int> source = (from ac in accordanceColor
				where ac.Value == j
				select ac).ToDictionary((KeyValuePair<int, int> ac) => ac.Key, (KeyValuePair<int, int> ac) => ac.Value);
				if (!source.Any())
				{
					list.Add(j);
				}
			}
			list = (from i in list
			orderby i descending
			select i).ToList();
			foreach (int item in list)
			{
				_resBeadsMain.RemoveAt(item);
			}
		}

		public bool LoadBeadsCatalog()
		{
			this.beadsManufacturer = new List<BeadsManufacturer>();
			string path = AppDomain.CurrentDomain.BaseDirectory + "/" + Settings.ShortProgramName + ".txt";
			BeadsManufacturer beadsManufacturer = new BeadsManufacturer();
			BeadsSeries beadsSeries = new BeadsSeries();
			List<string> list = new List<string>();
			int num = 0;
			if (!Settings.isItSpecialVersionOfBiserokForMaxim && File.Exists(path))
			{
				string text = "";
				string cipherText = FileWorker.ReadFileToEnd(path, true, null);
				List<string> list2 = cipherText.Split(new string[1]
				{
					"\r\n"
				}, StringSplitOptions.RemoveEmptyEntries).ToList();
				foreach (string item in list2)
				{
					if (item.Contains("-m-"))
					{
						beadsManufacturer = new BeadsManufacturer
						{
							name = item.Replace("-m-", "")
						};
						this.beadsManufacturer.Add(beadsManufacturer);
					}
					else if (item.Contains("-s-"))
					{
						beadsSeries = new BeadsSeries
						{
							name = item.Replace("-s-", ""),
							parentManufacturer = beadsManufacturer
						};
						beadsManufacturer.beadsSeries.Add(beadsSeries);
					}
					else if (item.Replace(" ", "").Length > 0)
					{
						try
						{
							list = item.Split(' ').ToList();
							Beads beads = new Beads
							{
								clr = Color.FromArgb(255, Convert.ToByte(list[1]), Convert.ToByte(list[2]), Convert.ToByte(list[3])),
								name = list[0].Replace("_", " "),
								parentSeries = beadsSeries
							};
							text = "";
							if (list.Count() > 4)
							{
								for (int i = 4; i < list.Count(); i++)
								{
									text = text + list[i] + " ";
								}
								beads.description = text.Substring(0, text.Length - 1);
							}
							beadsSeries.beads.Add(beads);
							num++;
						}
						catch
						{
						}
					}
				}
			}

			bool result = true;
   
			try
			{
				var myPallete = FileWorker.ReadFileToEnd(Settings.GetPathToFileInStorage("MyPalette.txt"), true, null).Split(new []{ "\r\n"}, StringSplitOptions.RemoveEmptyEntries).ToList();
				
				foreach (string item2 in myPallete)
				{
					if (item2.Contains("-m-"))
					{
						beadsManufacturer = new BeadsManufacturer
						{
							name = item2.Replace("-m-", "")
						};
						this.beadsManufacturer.Add(beadsManufacturer);
					}
					else if (item2.Contains("-s-"))
					{
						beadsSeries = new BeadsSeries
						{
							name = item2.Replace("-s-", ""),
							parentManufacturer = beadsManufacturer
						};
						beadsManufacturer.beadsSeries.Add(beadsSeries);
					}
					else if (item2.Replace(" ", "").Length > 0)
					{
						list = item2.Split(' ').ToList();
						beadsSeries.beads.Add(new Beads
						{
							clr = Color.FromArgb(255, Convert.ToByte(list[1]), Convert.ToByte(list[2]), Convert.ToByte(list[3])),
							name = list[0].Replace("_", " "),
							parentSeries = beadsSeries
						});
					}
				}

				return result;
			}
			catch
			{
				return false;
			}
		}

		private void DrawEmbroideryView(ref Bitmap BmpBeadwork, Pixel[,] _blocks)
		{
			int height = SqureCellSidePreview_Y * Param.HeightInBead;
			int width = SqureCellSidePreview_X * Param.WidthInBead;
			BmpBeadwork = new Bitmap(width, height);
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				BitmapData bitmapData = BmpBeadwork.LockBits(new Rectangle(0, 0, BmpBeadwork.Width, BmpBeadwork.Height), ImageLockMode.ReadWrite, BmpBeadwork.PixelFormat);
				try
				{
					int num = (BmpBeadwork.PixelFormat == (BmpBeadwork.PixelFormat | PixelFormat.Alpha)) ? 4 : 3;
					int num2 = BmpBeadwork.Width * BmpBeadwork.Height * num;
					byte[] array = new byte[num2];
					IntPtr scan = bitmapData.Scan0;
					Marshal.Copy(scan, array, 0, array.Length);
					bool flag = _blocks[0, 0] is PixelForColoring;
					for (int i = 0; i < Param.HeightInBead; i++)
					{
						for (int j = 0; j < Param.WidthInBead; j++)
						{
							Color color = (!flag) ? (_blocks[j, i] as Block).ResBeads.clr : (_blocks[j, i] as PixelForColoring).clr;
							int num3 = i * bitmapData.Stride + j * num;
							array[num3] = color.B;
							array[num3 + 1] = color.G;
							array[num3 + 2] = color.R;
						}
					}
					Marshal.Copy(array, 0, scan, array.Length);
				}
				finally
				{
					BmpBeadwork.UnlockBits(bitmapData);
				}
			}
			else
			{
				Graphics graphics = Graphics.FromImage(BmpBeadwork);
				if (Param.IsEmbroideryViewInRealView)
				{
					graphics.SmoothingMode = SmoothingMode.AntiAlias;
					int upperBound = _blocks.GetUpperBound(0);
					int upperBound2 = _blocks.GetUpperBound(1);
					for (int k = _blocks.GetLowerBound(0); k <= upperBound; k++)
					{
						for (int l = _blocks.GetLowerBound(1); l <= upperBound2; l++)
						{
							Block bb = (Block)_blocks[k, l];
							DrawRealViewOfBlock(graphics, bb, null, null);
						}
					}
				}
				else
				{
					int upperBound3 = _blocks.GetUpperBound(0);
					int upperBound4 = _blocks.GetUpperBound(1);
					for (int m = _blocks.GetLowerBound(0); m <= upperBound3; m++)
					{
						for (int n = _blocks.GetLowerBound(1); n <= upperBound4; n++)
						{
							Block bb2 = (Block)_blocks[m, n];
							DrawKubicViewOfBlock(graphics, bb2, null, null);
						}
					}
				}
				graphics.Dispose();
			}
		}

		private void SetDiffColorByDithering(SchemeType schemeType, float kf, int j, int i, int current_j, int current_i, ref Pixel[,] arrayBlocks, int deltaR, int deltaG, int deltaB)
		{
			try
			{
				Color srcColor;
				if (schemeType == SchemeType.Normal || (i - current_i) % 2 == 0)
				{
					Pixel pixel = arrayBlocks[j, i];
					srcColor = pixel.SrcColor;
					int num = srcColor.R + (int)Math.Round((double)((float)deltaR * kf));
					if (num < 0)
					{
						num = 0;
					}
					if (num > 255)
					{
						num = 255;
					}
					srcColor = pixel.SrcColor;
					int num2 = srcColor.G + (int)Math.Round((double)((float)deltaG * kf));
					if (num2 < 0)
					{
						num2 = 0;
					}
					if (num2 > 255)
					{
						num2 = 255;
					}
					srcColor = pixel.SrcColor;
					int num3 = srcColor.B + (int)Math.Round((double)((float)deltaB * kf));
					if (num3 < 0)
					{
						num3 = 0;
					}
					if (num3 > 255)
					{
						num3 = 255;
					}
					pixel.SrcColor = Color.FromArgb(255, (byte)num, (byte)num2, (byte)num3);
				}
				else
				{
					int num4 = -1;
					if (current_i % 2 == 1)
					{
						num4 = 1;
					}
					kf *= 0.5f;
					Pixel pixel2 = arrayBlocks[j, i];
					srcColor = pixel2.SrcColor;
					int num5 = srcColor.R + (int)Math.Round((double)((float)deltaR * kf));
					if (num5 < 0)
					{
						num5 = 0;
					}
					if (num5 > 255)
					{
						num5 = 255;
					}
					srcColor = pixel2.SrcColor;
					int num6 = srcColor.G + (int)Math.Round((double)((float)deltaG * kf));
					if (num6 < 0)
					{
						num6 = 0;
					}
					if (num6 > 255)
					{
						num6 = 255;
					}
					srcColor = pixel2.SrcColor;
					int num7 = srcColor.B + (int)Math.Round((double)((float)deltaB * kf));
					if (num7 < 0)
					{
						num7 = 0;
					}
					if (num7 > 255)
					{
						num7 = 255;
					}
					pixel2.SrcColor = Color.FromArgb(255, (byte)num5, (byte)num6, (byte)num7);
					pixel2 = arrayBlocks[j + num4, i];
					srcColor = pixel2.SrcColor;
					num5 = srcColor.R + (int)Math.Round((double)((float)deltaR * kf));
					if (num5 < 0)
					{
						num5 = 0;
					}
					if (num5 > 255)
					{
						num5 = 255;
					}
					srcColor = pixel2.SrcColor;
					num6 = srcColor.G + (int)Math.Round((double)((float)deltaG * kf));
					if (num6 < 0)
					{
						num6 = 0;
					}
					if (num6 > 255)
					{
						num6 = 255;
					}
					srcColor = pixel2.SrcColor;
					num7 = srcColor.B + (int)Math.Round((double)((float)deltaB * kf));
					if (num7 < 0)
					{
						num7 = 0;
					}
					if (num7 > 255)
					{
						num7 = 255;
					}
					pixel2.SrcColor = Color.FromArgb(255, (byte)num5, (byte)num6, (byte)num7);
				}
			}
			catch (Exception)
			{
			}
		}

		private void DrawPrimaryEmbroideryView()
		{
			DrawEmbroideryView(ref BmpBeadworkSource, blocksMain);
		}

		private void DrawReworkEmbroideryView()
		{
			DrawEmbroideryView(ref BmpBeadworkRework, blocksRework);
		}

		private void SetSourceColors(bool IsNeededToCalculateCountPixelsEachColors)
		{
			_srcColors = new List<Color>();
			int num2;
			int num3;
			Bitmap bitmap;
			if (Settings.hobbyProgramm != HobbyProgramm.Raskraska || !Param.isNotChangeSourceImageInColoring)
			{
				float num = ((float)Math.Max(BmpSourceImage.Width, BmpSourceImage.Height) + 0f) / 350f;
				num2 = (int)Math.Round((double)(((float)BmpSourceImage.Width + 0f) / num));
				num3 = (int)Math.Round((double)(((float)BmpSourceImage.Height + 0f) / num));
				bitmap = new Bitmap(num2, num3);
				Graphics graphics = Graphics.FromImage(bitmap);
				graphics.DrawImage(BmpSourceImage, new Rectangle(0, 0, num2, num3));
				graphics.Dispose();
			}
			else
			{
				bitmap = new Bitmap(GetOptimizedByUserSourceImage());
				num2 = bitmap.Width;
				num3 = bitmap.Height;
			}
			if (IsNeededToCalculateCountPixelsEachColors)
			{
				Dictionary<Color, int> dictionary = new Dictionary<Color, int>();
				for (int i = 0; i < num3; i++)
				{
					for (int j = 0; j < num2; j++)
					{
						Color pixel = bitmap.GetPixel(j, i);
						try
						{
							Dictionary<Color, int> dictionary2 = dictionary;
							Color key = pixel;
							dictionary2[key]++;
						}
						catch
						{
							dictionary.Add(pixel, 1);
						}
					}
				}
				bitmap.Dispose();
				dictionary = (from c in dictionary
				orderby c.Value descending
				select c).ToDictionary((KeyValuePair<Color, int> pair) => pair.Key, (KeyValuePair<Color, int> pair) => pair.Value);
				for (int k = 0; k < dictionary.Count(); k++)
				{
					_srcColors.Add(dictionary.ElementAt(k).Key);
				}
			}
			else
			{
				for (int l = 0; l < num3; l++)
				{
					for (int m = 0; m < num2; m++)
					{
						_srcColors.Add(bitmap.GetPixel(m, l));
					}
				}
				bitmap.Dispose();
				_srcColors = _srcColors.Distinct().ToList();
			}
		}

		public float GetDistanceBetweenColors(Color clr1, Color clr2)
		{
			int num = clr1.R - clr2.R;
			int num2 = clr1.G - clr2.G;
			int num3 = clr1.B - clr2.B;
			return (float)Math.Sqrt((double)(num * num + num2 * num2 + num3 * num3));
		}

		private ResultOfSavingOrPrinting GenerateFullSchemeAndPrintingOrSaveToFolder(SavingOrPrintingSettings settings, Bitmap bmpBeadwork, Pixel[,] _blocks, List<Beads> _resBeads, NumberAndColorOfContour[,] numberAndColorOfContour = null, List<ColoringContour> contours = null)
		{
			ResultOfSavingOrPrinting resultOfSavingOrPrinting = new ResultOfSavingOrPrinting
			{
				isOk = true
			};
			bool flag = false;
			if (Settings.isItSpecialVersion_ForManufacturers)
			{
				try
				{
					string bitmapHashCode = Cryption.GetBitmapHashCode(pathToSrcImage);
					string iDComputer = RegistryWork.GetIDComputer();
					ServicesClient servicesClient = new ServicesClient(Settings.ShortProgramName);
					string text = servicesClient.CheckEnableSchemesInSpecialVersion(new Guid(iDComputer), bitmapHashCode, WidthSourceImageInPx, HeightSourceImageInPx, pathToSrcImage, Param.CountDesiredColors, Param.WidthInBead, Param.HeightInBead);
					servicesClient.Close();
					if (text.Length == 0)
					{
						resultOfSavingOrPrinting.isOk = false;
						resultOfSavingOrPrinting.error = Local.Get("Оплаченные для создания картины закончились. Это ошибка или желаете оплатить доп.картины для создания? Обратитесь по адресу ") + Settings.AddressOfAuthorOfProgram;
					}
					else
					{
						string decryptInfo = Site.GetDecryptInfo(text, iDComputer.Substring(0, 6), iDComputer.Substring(9, 6), iDComputer.Substring(20, 16));
						if (decryptInfo != bitmapHashCode)
						{
							resultOfSavingOrPrinting.isOk = false;
							resultOfSavingOrPrinting.error = "Произошла критическая ошибка. Пожалуйста, напишите об ошибке по адресу " + Settings.AddressOfAuthorOfProgram + " . И пожалуйста, не пытайтесь сейчас создавать картины - они пойдут в 'вычет' из оплаченных Вами. Простите за неудобства!";
						}
						else
						{
							flag = true;
						}
					}
				}
				catch (Exception)
				{
					resultOfSavingOrPrinting.isOk = false;
					resultOfSavingOrPrinting.error = "Программе не удаётся связаться с сервером. Либо проблемы с интернетом на Вашем компьютере, либо неполадки сервера. Если у Вас с интернетом 100% нет проблем - пожалуйста, напишите об ошибке по адресу " + Settings.AddressOfAuthorOfProgram + " . Простите за неудобства!";
				}
			}
			else
			{
				flag = true;
			}
			if (flag)
			{
				try
				{
					if (settings.isPrinting)
					{
						pd = new PrintDocument();
						pd.PrinterSettings = settings.printer;
						pd.PrintPage += Print_Page;
					}
					if (settings.isSaving)
					{
						try
						{
							if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
							{
								using (Bitmap bitmap = new Bitmap(bmpSourceImageProcessingButNonScaled))
								{
									bitmap.Save(string.Format("{0}\\" + Local.Get("Исходное изображение") + "{1}", settings.pathToSave, ExtSourceImage));
								}
							}
							else
							{
								using (Bitmap bitmap2 = new Bitmap(GetOptimizedByUserSourceImage()))
								{
									bitmap2.Save(string.Format("{0}\\" + Local.Get("Исходное изображение") + "{1}", settings.pathToSave, ExtSourceImage));
								}
							}
						}
						catch
						{
						}
						SaveBeadworkView(settings, bmpBeadwork);
					}
					if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
					{
						int num = 0;
						if (Param.BiserType == BiserType.ForManufacturers && !Param.IsMixingColors)
						{
							_resBeads.Sort(new ComparerBeadsBySeriesAndManufacturer());
						}
						else
						{
							_resBeads.Sort(new ComparerColorByLight());
						}
						if (Param.BiserType == BiserType.ForManufacturers && !Param.IsMixingColors && Param.isConstantNumerationOfColorsInRaskraska)
						{
							List<Beads> list = new List<Beads>();
							foreach (BeadsManufacturer item in beadsManufacturer.Where(delegate(BeadsManufacturer bm)
							{
								if (bm.name != "Акрил" && bm.name != "Гуашь" && bm.name != "Acrylic")
								{
									return bm.name != "Gouache";
								}
								return false;
							}))
							{
								foreach (BeadsSeries item2 in item.beadsSeries)
								{
									foreach (Beads bead2 in item2.beads)
									{
										list.Add(bead2);
									}
								}
							}
							foreach (Beads _resBead in _resBeads)
							{
								num = list.FindIndex((Beads bd) => bd == _resBead);
								_resBead.symbolForColoring_AndForVoiceWorkInBiserok = GetCharForBadges(num);
							}
						}
						else
						{
							foreach (Beads _resBead2 in _resBeads)
							{
								_resBead2.symbolForColoring_AndForVoiceWorkInBiserok = GetCharForBadges(num);
								num++;
							}
						}
						if (settings.isSaving || settings.printSettings.IsLegendLists)
						{
							GenerateLegendListsAndPrintOrSaveToFolder(_resBeads, settings, null, null, contours.Count(), null);
						}
						GenerateColoringSchemeAndPrintOrSaveToFolder(settings, bmpBeadwork, _resBeads, numberAndColorOfContour, contours);
						if (v >= 3)
						{
							SaveDatFile(settings, ref resultOfSavingOrPrinting);
						}
						if (settings.isSaving && Settings.isItSpecialVersionOfRaskraskaForNikolayUkraina)
						{
							SaveLegendInExcelForNikolayUkraina(_resBeads, articleScheme, settings.pathToSave);
						}
					}
					else
					{
						SqureCellSidePrintPages_Y = Param.SqureCellSidePrintPages_Y;
						SqureCellSidePrintPages_X = (int)Math.Round((double)(((float)SqureCellSidePrintPages_Y + 0f) * (Param.HorizontalSizeOfBlockAtSchemeInMm / Param.VerticalSizeOfBlockAtSchemeInMm)));
						SortBeadsByManufacturerAndGenerateBadges(_resBeads);
						Beads beads = null;
						if (Param.LightestColorIsCanvas || Param.DarkestColorIsCanvas)
						{
							_resBeads.Sort(new ComparerColorByLight());
							if (Param.DarkestColorIsCanvas)
							{
								beads = _resBeads.ElementAt(_resBeads.Count() - 1);
								beads.badge = new Bitmap(1, 1);
								beads.IsCanvas = true;
								_resBeads.Remove(beads);
							}
							if (Param.LightestColorIsCanvas)
							{
								beads = _resBeads.ElementAt(0);
								beads.badge = new Bitmap(1, 1);
								beads.IsCanvas = true;
								_resBeads.Remove(beads);
							}
						}
						int num2 = 0;
						int num3 = 0;
						int countRecordsInMiniLegend = 0;
						int num4 = 0;
						int num5 = 0;
						Point ptBeginMiniLegend = default(Point);
						float num6 = 0f;
						float num7 = 0f;
						float num8 = 0f;
						float num9 = 0f;
						float num10 = 0f;
						if (!Settings.isItSpecialVersionOfBiserokForMaxim)
						{
							_resBeads.Sort(new ComparerBeadsBySeriesAndManufacturer());
						}
						else
						{
							_resBeads.Sort(new ComparerBeadsByNameAsInt());
						}
						IEnumerable<Block> enumerable = from Block b in _blocks
						where b != null
						select b;
						Dictionary<Beads, int> countOfEachBeads = CalculateCountOfEachBeadsInScheme(_resBeads, enumerable);
						if (settings.isSaving || settings.printSettings.IsFullScheme || settings.printSettings.IsSchemePages || settings.printSettings.IsSchemeForPrintingOnTextile)
						{
							int num11 = SqureCellSidePrintPages_X * Param.WidthInBead;
							int num12 = SqureCellSidePrintPages_Y * Param.HeightInBead;
							countRecordsInMiniLegend = 0;
							if (Param.BiserType == BiserType.ForManufacturers && Param.IsDrawSmallLegend)
							{
								CalculateGeometryParamsOfScheme(ref num2, ref num3, ref countRecordsInMiniLegend, ref num4, ref num5, _resBeads, num11, num12, ref num6, ref num7, ref num8, ref num9, ref num10, Settings.hobbyProgramm == HobbyProgramm.Krestik && settings.numberOfTab == 3);
							}
							if (settings.isSaving || settings.printSettings.IsFullScheme || settings.printSettings.IsSchemePages)
							{
								int num13 = SqureCellSidePrintPages_X * Param.WidthInBead + 9;
								float num14 = ((float)num13 + 0f) / 4010f;
								int num15 = (int)Math.Round((double)(145f * num14));
								int num16 = (int)Math.Round((double)(290f * num14));
								int num17 = num15 + num4;
								int num18 = num15 + num5;
								ptBeginMiniLegend = default(Point);
								if (num11 > num12)
								{
									ptBeginMiniLegend.X = 9 + num15;
									ptBeginMiniLegend.Y = num12 + 9 + num16 + (int)Math.Round((double)((float)SqureCellSidePrintPages_Y * 1.5f));
								}
								else
								{
									ptBeginMiniLegend.X = num11 + 9 + num15 + (int)Math.Round((double)((float)SqureCellSidePrintPages_X * 1.5f));
									ptBeginMiniLegend.Y = 9 + num16 + SqureCellSidePrintPages_Y;
								}
								if (!Settings.isItSpecialVersionOfBiserokForMaxim)
								{
									Bitmap bitmap3 = new Bitmap(num11 + 9 + num15 + num18, num12 + 9 + num16 + num17);
									GenerateMainScheme(settings, bitmap3, _blocks as Block[,], _resBeads, num16, num15, num17, num18, num2, num3, countRecordsInMiniLegend, ptBeginMiniLegend, countOfEachBeads);
									if (settings.isSaving || settings.printSettings.IsSchemePages)
									{
										GenerateSchemePagesAndPrintOrSaveToFolder(bitmap3, settings, num16, num15, num17, num18);
									}
									if (settings.isSaving || settings.printSettings.IsFullScheme)
									{
										DrawOnMainSchemeAllStrings(bitmap3, num16, num15, num17, num18, num14);
										if (settings.isPrinting)
										{
											PrintingBmp = bitmap3;
											pd.Print();
										}
										if (settings.isSaving)
										{
											bitmap3.Save(settings.pathToSave + "\\" + Local.Get("Схема целиком") + ".png", format);
										}
									}
									bitmap3.Dispose();
								}
							}
						}
						if (Settings.hobbyProgramm == HobbyProgramm.Biserok || (Settings.hobbyProgramm == HobbyProgramm.Almaz && !Param.isSquareStraz))
						{
							CreateColorsOfBordersForPrintingOnTextileScheme(_resBeads);
						}
						if (settings.isSaving || settings.printSettings.IsLegendLists)
						{
							if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
							{
								GenerateLegendListsAndPrintOrSaveToFolder(_resBeads, settings, countOfEachBeads, enumerable, 0, bmpBeadwork);
							}
							else
							{
								GenerateLegendListsAndPrintOrSaveToFolder(_resBeads, settings, countOfEachBeads, enumerable, 0, null);
							}
						}
						if (beads != (Beads)null)
						{
							beads.IsCanvas = false;
							_resBeads.Add(beads);
						}
						SaveDatFile(settings, ref resultOfSavingOrPrinting);
						if (beads != (Beads)null)
						{
							beads.IsCanvas = true;
							_resBeads.Remove(beads);
						}
						if (settings.isSaving && Settings.isItSpecialVersionOfBiserokForMaxim)
						{
							SaveSourceImageInFrameWithTitles(settings.pathToSave, _resBeads.Count());
						}
						if (settings.isSaving && Settings.isItSpecialVersionOfBiserokForTatyanaKudryavtceva)
						{
							SaveSchemeInTxtFileForVoiceWork(_blocks, settings.pathToSave);
						}
						if ((Settings.hobbyProgramm == HobbyProgramm.Biserok || Settings.hobbyProgramm == HobbyProgramm.Almaz) && (settings.isSaving || settings.printSettings.IsSchemeForPrintingOnTextile))
						{
							try
							{
								float num19 = (float)Param.WidthInBead * Param.HorizontalSizeOfBlockAtSchemeInMm;
								float num20 = (float)Param.HeightInBead * Param.VerticalSizeOfBlockAtSchemeInMm;
								int num21 = (int)Math.Round((double)(num19 * ((float)Param.PrintingResolution / 25.4f)));
								int num22 = (int)Math.Round((double)(num20 * ((float)Param.PrintingResolution / 25.4f)));
								SqureCellSideSchemeForPrinting_X = ((float)num21 + 0f) / ((float)Param.WidthInBead + 0f);
								SqureCellSideSchemeForPrinting_Y = ((float)num22 + 0f) / ((float)Param.HeightInBead + 0f);
								int num11 = (int)Math.Round((double)(SqureCellSideSchemeForPrinting_X * (float)Param.WidthInBead)) + 9;
								int num12 = (int)Math.Round((double)(SqureCellSideSchemeForPrinting_Y * (float)Param.HeightInBead)) + 9;
								num5 = (num4 = 0);
								if (Param.BiserType == BiserType.ForManufacturers && Param.IsDrawSmallLegend)
								{
									if (Param.WidthInBead > Param.HeightInBead)
									{
										num5 = 0;
										num4 = (int)Math.Round((double)((float)num2 * SqureCellSideSchemeForPrinting_Y * 2f)) + 50;
										if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
										{
											num4 += (int)Math.Round((double)(SqureCellSideSchemeForPrinting_Y * 2f));
										}
										ptBeginMiniLegend.X = (int)(15f + SqureCellSideSchemeForPrinting_X * (num9 + num10));
										ptBeginMiniLegend.Y = num12 + 25;
									}
									else
									{
										num5 = (int)Math.Round((double)((float)num3 * SqureCellSideSchemeForPrinting_X * (float)widthOfColumnInBadgesInMiniLegend)) + 50;
										num4 = 0;
										ptBeginMiniLegend.X = num11 + 25;
										ptBeginMiniLegend.Y = (int)(15f + SqureCellSideSchemeForPrinting_X * (num6 + num7 + num8));
									}
								}
								Bitmap bitmap4 = new Bitmap(num11 + num5, num12 + num4);
								if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina || Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
								{
									GenerateSchemeForPrinting(bitmap4, _blocks as Block[,], _resBeads, num11, num12, num2, num3, countRecordsInMiniLegend, ptBeginMiniLegend, countOfEachBeads, enumerable, num6, num7, num8, num9, num10, bmpBeadwork);
								}
								else
								{
									GenerateSchemeForPrinting(bitmap4, _blocks as Block[,], _resBeads, num11, num12, num2, num3, countRecordsInMiniLegend, ptBeginMiniLegend, countOfEachBeads, enumerable, num6, num7, num8, num9, num10, null);
								}
								if (Settings.isItSpecialVersionOfAlmazkaForElenaBartosova && nameScheme.Length > 0)
								{
									int num23 = (int)Math.Round((double)(17f * ((float)Param.PrintingResolution / 25.4f)));
									Bitmap bitmap5 = new Bitmap(bitmap4);
									bitmap4 = new Bitmap(bitmap5.Width, bitmap5.Height + num23);
									Graphics graphics = Graphics.FromImage(bitmap4);
									graphics.Clear(Color.White);
									float emSize = (float)(155 * Param.PrintingResolution) / 300f;
									Font font = new Font("Verdana", emSize, GraphicsUnit.Pixel);
									graphics.DrawString(nameScheme, font, Brushes.Black, new Point(40 * Param.PrintingResolution / 300, 0));
									graphics.DrawImage(bitmap5, new Point(0, num23));
									bitmap5.Dispose();
									graphics.Dispose();
								}
								if (settings.isPrinting)
								{
									PrintingBmp = bitmap4;
									pd.Print();
								}
								if (settings.isSaving)
								{
									if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
									{
										bitmap4.Save(settings.pathToSave + "\\" + Local.Get("Схема для печати на ткани") + ".png", format);
									}
									else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
									{
										bitmap4.Save(settings.pathToSave + "\\" + Local.Get("Схема для печати") + ".png", format);
									}
								}
								bitmap4.Dispose();
							}
							catch (Exception)
							{
								resultOfSavingOrPrinting.isOk = false;
								resultOfSavingOrPrinting.error = "Произошла ошибка - мощности компьютера не хватило для сохранения схемы для печати на ткани такого большого размера (все остальные элементы схемы были успешно сохранены). Для решения проблемы попробуйте зайти в пункт 'Прочие настройки' (через соотв. кнопку над кнопкой 'Создать схему' на первой вкладке) и установить там мЕньшие значения для параметра 'Разрешение печати в пикс/дюйм'. И затем ещё раз попробуйте сохранить/распечатать схему. Извините за неудобства.";
							}
						}
						if (beads != (Beads)null)
						{
							beads.IsCanvas = false;
							_resBeads.Add(beads);
						}
					}
					if (settings.isSaving && resultOfSavingOrPrinting.isOk)
					{
						IsNeedToSave = false;
					}
					SystemSounds.Asterisk.Play();
					Thread.Sleep(350);
					SystemSounds.Asterisk.Play();
				}
				catch (Exception exc)
				{
					Site.SendExceptionInfo(exc, false);
					GC.Collect(GC.MaxGeneration);
					resultOfSavingOrPrinting.isOk = false;
					if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
					{
						if (v < 2)
						{
							resultOfSavingOrPrinting.error = "Произошла ошибка. Вероятнее всего, мощности компьютера (оперативной памяти) не хватило для сохранения раскраски такого большого размера. Попробуйте либо уменьшить размер раскраски в см (и создать её заново!), либо зайти в пункт 'Важные настройки' (через соотв. кнопку над кнопкой 'Создать картину') и установить там мЕньшие значения для 'Разрешение печати (в пикселях/дюйм)'. И затем ещё раз попробовать сохранить/распечатать картину. Извините за неудобства.";
						}
						else
						{
							resultOfSavingOrPrinting.error = "Произошла ошибка. Вероятнее всего, мощности компьютера (оперативной памяти) не хватило для сохранения раскраски такого большого размера. Попробуйте либо уменьшить размер раскраски в см (и создать её заново!), либо зайти в пункт 'Важные настройки' (через соотв. кнопку над кнопкой 'Создать картину') и установить там мЕньшие значения для 'Разрешение печати (в пикселях/дюйм)' и/или указать более плохое качество контуров (чем выше качество - тем больше оперативой памяти использует программа). И затем ещё раз попробовать сохранить/распечатать картину. Извините за неудобства.";
						}
					}
					else
					{
						resultOfSavingOrPrinting.error = Local.Get("Произошла ошибка. Вероятнее всего, мощности компьютера (оперативной памяти) не хватило для обработки схемы такого большого размера. Попробуйте либо уменьшить размер схемы в см (и создать схему заново!), либо зайти в пункт 'Прочие настройки' (через соотв. кнопку над кнопкой 'Создать схему') и установить там мЕньшие значения для 'Размер ") + Settings.nameOfOneBeads + Local.Get(" на схеме после сохранения (в пикселях)'. И затем ещё раз попробовать сохранить/распечатать схему. Извините за неудобства.");
					}
					SystemSounds.Exclamation.Play();
				}
				if (settings.isPrinting && PrintingBmp != null)
				{
					PrintingBmp.Dispose();
				}
			}
			return resultOfSavingOrPrinting;
		}

		private Bitmap GetOptimizedByUserSourceImage()
		{
			return BmpSourceImage;
		}

		private void SaveLegendInExcelForNikolayUkraina(List<Beads> beads, string articleScheme, string pathToSave)
		{
			//string text = pathToSave + "\\" + articleScheme + ".xlsx";
			//if (File.Exists(Path.GetFullPath(text)))
			//{
			//	File.Delete(text);
			//}
			//try
			//{
			//	Microsoft.Office.Interop.Excel.Application application = (Microsoft.Office.Interop.Excel.Application)Activator.CreateInstance(Type.GetTypeFromCLSID(new Guid("00024500-0000-0000-C000-000000000046")));
			//	Workbook workbook = application.Workbooks.Add(Missing.Value);
			//	if (_003C_003Eo__117._003C_003Ep__0 == null)
			//	{
			//		_003C_003Eo__117._003C_003Ep__0 = CallSite<Func<CallSite, object, Worksheet>>.Create(Microsoft.CSharp.RuntimeBinder.Binder.Convert(CSharpBinderFlags.ConvertExplicit, typeof(Worksheet), typeof(Scheme)));
			//	}
			//	Worksheet worksheet = _003C_003Eo__117._003C_003Ep__0.Target(_003C_003Eo__117._003C_003Ep__0, workbook.Sheets[1]);
			//	worksheet.Cells[1, 1] = articleScheme;
			//	int num = 0;
			//	foreach (Beads bead in beads)
			//	{
			//		worksheet.Cells[++num, 2] = bead.name;
			//	}
			//	workbook.SaveAs(text, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
			//	application.Quit();
			//}
			//catch (Exception ex)
			//{
			//	MessageBox.Show("Ошибка при сохранении excel-файла (сделайте принтскрин и перешлите авторам программы):\r\n\r\n" + ex.Message);
			//}
		}

		private void SaveSchemeInTxtFileForVoiceWork(Pixel[,] blocks, string pathToSave)
		{
			using (StreamWriter streamWriter = new StreamWriter(pathToSave + "\\Для говорилки.txt"))
			{
				for (int i = 0; i < Param.HeightInBead; i++)
				{
					try
					{
						int num = i + 1;
						string text = "РЯД №" + num.ToString() + "   ";
						for (int j = 0; j < Param.WidthInBead; j++)
						{
							if (blocks[j, i] != null)
							{
								text = text + (blocks[j, i] as Block).ResBeads.symbolForColoring_AndForVoiceWorkInBiserok + " ";
							}
						}
						string str = text;
						num = i + 1;
						text = str + " КОНЕЦ РЯДА №" + num.ToString();
						streamWriter.WriteLine(text);
					}
					catch (Exception ex)
					{
						MessageBox.Show(ex.Message);
					}
				}
			}
		}

		private void SaveSourceImageInFrameWithTitles(string p, int countOfColors)
		{
			int num = 98;
			int num2 = 99;
			int num3 = 102;
			int num4 = 103;
			float num5 = (float)Math.Min(BmpSourceImage.Width, BmpSourceImage.Height) * 1f / 9f / 100f;
			int num6 = (int)((float)num * num5);
			int num7 = (int)((float)num2 * num5);
			int num8 = (int)((float)num3 * num5);
			int num9 = (int)((float)num4 * num5);
			Bitmap bitmap = new Bitmap(BmpSourceImage);
			if (BmpSourceImage.Width < BmpSourceImage.Height)
			{
				bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
			}
			Bitmap bitmap2 = new Bitmap(bitmap.Width + num7 + num9, bitmap.Height + num6 + num8);
			Graphics graphics = Graphics.FromImage(bitmap2);
			Bitmap bitmap3 = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/frameTopLeft.jpg");
			int num10;
			int width = num10 = (int)((float)bitmap3.Width * num5);
			int num11;
			int height = num11 = (int)((float)bitmap3.Height * num5);
			Bitmap image = new Bitmap(width, height);
			Graphics graphics2 = Graphics.FromImage(image);
			graphics2.DrawImage(bitmap3, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bitmap3.Width, bitmap3.Height), GraphicsUnit.Pixel);
			graphics.DrawImage(image, 0, 0);
			bitmap3 = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/frameTopRight.jpg");
			int num12;
			width = (num12 = (int)((float)bitmap3.Width * num5));
			int num13;
			height = (num13 = (int)((float)bitmap3.Height * num5));
			image = new Bitmap(width, height);
			graphics2 = Graphics.FromImage(image);
			graphics2.DrawImage(bitmap3, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bitmap3.Width, bitmap3.Height), GraphicsUnit.Pixel);
			graphics.DrawImage(image, bitmap2.Width - image.Width, 0);
			bitmap3 = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/frameBottomRight.jpg");
			int num14;
			width = (num14 = (int)((float)bitmap3.Width * num5));
			int num15;
			height = (num15 = (int)((float)bitmap3.Height * num5));
			image = new Bitmap(width, height);
			graphics2 = Graphics.FromImage(image);
			graphics2.DrawImage(bitmap3, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bitmap3.Width, bitmap3.Height), GraphicsUnit.Pixel);
			graphics.DrawImage(image, bitmap2.Width - image.Width, bitmap2.Height - image.Height);
			bitmap3 = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/frameBottomLeft.jpg");
			int num16;
			width = (num16 = (int)((float)bitmap3.Width * num5));
			int num17;
			height = (num17 = (int)((float)bitmap3.Height * num5));
			image = new Bitmap(width, height);
			graphics2 = Graphics.FromImage(image);
			graphics2.DrawImage(bitmap3, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bitmap3.Width, bitmap3.Height), GraphicsUnit.Pixel);
			graphics.DrawImage(image, 0, bitmap2.Height - image.Height);
			bitmap3 = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/frameTopMiddle.jpg");
			width = bitmap2.Width - num10 - num12 + 4;
			height = (int)((float)bitmap3.Height * num5);
			image = new Bitmap(width, height);
			graphics2 = Graphics.FromImage(image);
			graphics2.DrawImage(bitmap3, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bitmap3.Width, bitmap3.Height), GraphicsUnit.Pixel);
			graphics.DrawImage(image, num10 - 2, 0);
			bitmap3 = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/frameMiddleRight.jpg");
			width = (int)((float)bitmap3.Width * num5);
			height = bitmap2.Height - num13 - num15 + 4;
			image = new Bitmap(width, height);
			graphics2 = Graphics.FromImage(image);
			graphics2.DrawImage(bitmap3, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bitmap3.Width, bitmap3.Height), GraphicsUnit.Pixel);
			graphics.DrawImage(image, bitmap2.Width - width, num13 - 2);
			bitmap3 = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/frameBottomMiddle.jpg");
			width = bitmap2.Width - num16 - num14 + 4;
			height = (int)((float)bitmap3.Height * num5);
			image = new Bitmap(width, height);
			graphics2 = Graphics.FromImage(image);
			graphics2.DrawImage(bitmap3, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bitmap3.Width, bitmap3.Height), GraphicsUnit.Pixel);
			graphics.DrawImage(image, num16 - 2, bitmap2.Height - height);
			bitmap3 = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/frameMiddleLeft.jpg");
			width = (int)((float)bitmap3.Width * num5);
			height = bitmap2.Height - num11 - num17 + 4;
			image = new Bitmap(width, height);
			graphics2 = Graphics.FromImage(image);
			graphics2.DrawImage(bitmap3, new Rectangle(0, 0, width, height), new Rectangle(0, 0, bitmap3.Width, bitmap3.Height), GraphicsUnit.Pixel);
			graphics.DrawImage(image, 0, num11 - 2);
			graphics2.Dispose();
			bitmap3.Dispose();
			image.Dispose();
			graphics.DrawImage(bitmap, num9, num6);
			int num18 = (int)Math.Round((double)((float)bitmap.Height / 900f * 4.5f));
			int num19 = (int)((float)bitmap.Height / 900f * 260f);
			height = bitmap2.Height + num19 + 2 * num18;
			Bitmap bitmap4 = new Bitmap(bitmap2.Width + 2 * num18, height);
			graphics = Graphics.FromImage(bitmap4);
			graphics.FillRectangle(Brushes.White, new Rectangle(0, 0, bitmap4.Width, bitmap4.Height));
			graphics.DrawRectangle(new Pen(Brushes.Black, (float)num18), new Rectangle(num18 / 2, num18 / 2, bitmap4.Width - num18, bitmap4.Height - num18));
			graphics.DrawImage(bitmap2, num18, num18);
			float emSize = (float)bitmap.Height / 768f * 52f * KfSystemFontSizes;
			float emSize2 = (float)bitmap.Height / 768f * 34f * KfSystemFontSizes;
			int num20 = (int)((float)bitmap.Height / 900f * 40f);
			int num21 = (int)((float)bitmap.Height / 900f * 37f);
			graphics.DrawString(nameScheme, new Font("Verdana", emSize), Brushes.Black, (float)num21, (float)(num20 + bitmap2.Height));
			SizeF sizeF = graphics.MeasureString(articleScheme, new Font("Verdana", emSize));
			num21 = (int)((float)bitmap2.Width - sizeF.Width - (float)bitmap.Height / 900f * 77f);
			graphics.DrawString(articleScheme, new Font("Verdana", emSize), Brushes.Black, (float)num21, (float)(num20 + bitmap2.Height));
			string text = countOfColors.ToString() + " " + AllString.GetCountColorStringInRoditelsiyPadezh(countOfColors);
			sizeF = graphics.MeasureString(text, new Font("Verdana", emSize2));
			num21 = (int)((float)bitmap2.Width - sizeF.Width - (float)bitmap.Height / 900f * 77f);
			num20 = (int)((float)num19 * 160f / 260f);
			graphics.DrawString(text, new Font("Verdana", emSize2), Brushes.Black, (float)num21, (float)(num20 + bitmap2.Height));
			bitmap2.Dispose();
			float num22 = (float)Math.Min(bitmap4.Width, bitmap4.Height) / 1000f;
			bitmap = new Bitmap((int)(((float)bitmap4.Width + 0f) / num22), (int)(((float)bitmap4.Height + 0f) / num22));
			graphics = Graphics.FromImage(bitmap);
			graphics.DrawImage(bitmap4, new Rectangle(0, 0, bitmap.Width, bitmap.Height));
			bitmap4.Dispose();
			graphics.Dispose();
			bitmap.Save(p + "\\В рамке для ярлыка.png", format);
			bitmap.Dispose();
		}

		private void SaveDatFile(SavingOrPrintingSettings settings, ref ResultOfSavingOrPrinting res)
		{
			if (settings.isSaving)
			{
				string[] files = Directory.GetFiles(settings.pathToSave, "*.dat");
				string[] array = files;
				foreach (string path in array)
				{
					File.Delete(path);
				}
				try
				{
					BinaryFormatter binaryFormatter = new BinaryFormatter();
					using (Stream serializationStream = new FileStream(settings.pathToSave + "\\" + settings.numberOfTab.ToString() + ".dat", FileMode.Create, FileAccess.Write, FileShare.None))
					{
						binaryFormatter.Serialize(serializationStream, this);
					}
				}
				catch (Exception)
				{
					res.isOk = false;
					res.error = "Сохранение самой схемы (т.е. всех её листов, легенды, вида вышивки и т.п.) прошло УСПЕШНО. Вы можете открыть сохранённую папку и посмотреть или распечатать схему.\r\n\r\nОднако, возникла ОШИБКА при сохранении файла схемы, который нужен для дальнейшей загрузки схемы в программу с целью доработки. \r\n\r\nЕсли Вы планируете в будущем дорабатывать эту схему - попробуйте выполнить следующие действия:\r\n1. Попробуйте сохранить схему в другую папку (чтобы сохранился и файл схемы для доработки тоже).\r\n\tЕсли не поможет, то:\r\n2. Перенесите схему на другую вкладку (доработка или ручное редактирование) и чуть-чуть измените её (самую малость - просто чтобы какой-то элемент в схеме стал другим). И попробуйте сохранить из той вкладки, куда перенесли.\r\n\r\nИзвините за неудобства.";
				}
			}
		}

		private void DefineContoursOfColoringAndRedrawingColoringView(Bitmap bmpBeadwork, ref NumberAndColorOfContour[,] numberAndColorOfContour, ref List<ColoringContour> contours)
		{
			BitmapData bitmapData = bmpBeadwork.LockBits(new Rectangle(0, 0, bmpBeadwork.Width, bmpBeadwork.Height), ImageLockMode.ReadWrite, bmpBeadwork.PixelFormat);
			try
			{
				int num = (bmpBeadwork.PixelFormat == (bmpBeadwork.PixelFormat | PixelFormat.Alpha)) ? 4 : 3;
				int num2 = bmpBeadwork.Width * bmpBeadwork.Height * num;
				byte[] array = new byte[num2];
				IntPtr scan = bitmapData.Scan0;
				Marshal.Copy(scan, array, 0, array.Length);
				int num3 = 0;
				int stride = bitmapData.Stride;
				int num4 = 400;
				int num5 = bmpBeadwork.Width / num4;
				if (num5 * num4 != bmpBeadwork.Width)
				{
					num5++;
				}
				int num6 = bmpBeadwork.Height / num4;
				if (num6 * num4 != bmpBeadwork.Height)
				{
					num6++;
				}
				numberAndColorOfContour = null;
				if (contours != null)
				{
					contours.Clear();
				}
				contours = new List<ColoringContour>();
				numberAndColorOfContour = new NumberAndColorOfContour[Param.WidthInBead, Param.HeightInBead];
				for (int i = 0; i < num6; i++)
				{
					int num7 = (i + 1) * num4;
					if (i == num6 - 1)
					{
						num7 = bmpBeadwork.Height;
					}
					for (int j = 0; j < num5; j++)
					{
						int num8 = (j + 1) * num4;
						if (j == num5 - 1)
						{
							num8 = bmpBeadwork.Width;
						}
						for (int k = i * num4; k < num7; k++)
						{
							for (int l = j * num4; l < num8; l++)
							{
								if (numberAndColorOfContour[l, k] == null)
								{
									int num9 = k * stride + l * num;
									byte b = array[num9];
									byte g = array[num9 + 1];
									byte r = array[num9 + 2];
									ColoringContour item = new ColoringContour
									{
										number = num3,
										points = new List<Point>()
									};
									AddLineToContour(l, l, k, r, g, b, i * num4, num7, j * num4, num8, ref item, ref array, stride, num, ref numberAndColorOfContour);
									contours.Add(item);
									num3++;
								}
							}
						}
					}
				}
				for (int m = 1; m < Param.HeightInBead - 1; m++)
				{
					for (int n = 1; n < Param.WidthInBead - 1; n++)
					{
						ComparePointsInContoursAndMergeItIfNeed(n, m, n - 1, m, ref contours, ref numberAndColorOfContour);
						ComparePointsInContoursAndMergeItIfNeed(n, m, n + 1, m, ref contours, ref numberAndColorOfContour);
						ComparePointsInContoursAndMergeItIfNeed(n, m, n, m - 1, ref contours, ref numberAndColorOfContour);
						ComparePointsInContoursAndMergeItIfNeed(n, m, n, m + 1, ref contours, ref numberAndColorOfContour);
					}
				}
				Marshal.Copy(array, 0, scan, array.Length);
			}
			finally
			{
				bmpBeadwork.UnlockBits(bitmapData);
			}
		}

		public List<Beads> DefineMixingColorsInColoringInMixingColorsMode(List<Beads> _r)
		{
			List<Beads> list = ColorModel.CalculateNumberOfMixingColors(_r, beadsEnableToUse);
			for (int i = 0; i < list.Count(); i++)
			{
				for (int j = i + 1; j < list.Count(); j++)
				{
					if (list[i].clr == list[j].clr)
					{
						list.RemoveAt(j);
						j--;
					}
				}
			}
			return list;
		}

		private void RemoveSmallContoursFromColoring(ref Bitmap bmpBeadwork, ref Pixel[,] blocks, ref NumberAndColorOfContour[,] numberAndColorOfContour, ref List<ColoringContour> contours)
		{
			if (Param.SmallPartsOnColouring > 0)
			{
				BitmapData bitmapData = bmpBeadwork.LockBits(new Rectangle(0, 0, bmpBeadwork.Width, bmpBeadwork.Height), ImageLockMode.ReadWrite, bmpBeadwork.PixelFormat);
				try
				{
					int num = (bmpBeadwork.PixelFormat == (bmpBeadwork.PixelFormat | PixelFormat.Alpha)) ? 4 : 3;
					int num2 = bmpBeadwork.Width * bmpBeadwork.Height * num;
					byte[] array = new byte[num2];
					IntPtr scan = bitmapData.Scan0;
					Marshal.Copy(scan, array, 0, array.Length);
					int stride = bitmapData.Stride;
					float num3 = 1f;
					num3 = ((Param.SmallPartsOnColouring != 1) ? ((Param.SmallPartsOnColouring != 2) ? ((Param.SmallPartsOnColouring != 3) ? ((Param.SmallPartsOnColouring != 4) ? ((Param.SmallPartsOnColouring != 5) ? ((Param.SmallPartsOnColouring != 6) ? ((Param.SmallPartsOnColouring != 7) ? ((Param.SmallPartsOnColouring != 8) ? ((Param.SmallPartsOnColouring != 9) ? 8f : 6f) : 4f) : 3f) : 2f) : 1.25f) : 0.75f) : 0.4f) : 0.2f) : 0.09f);
					int areaToRemove = (int)Math.Round((double)((float)(Param.HeightInBead * Param.WidthInBead) * 0.0001f * num3));
					NumberAndColorOfContour _point;
					while (contours.Any((ColoringContour ct) => ct.points.Count() <= areaToRemove))
					{
						ColoringContour coloringContour = contours.First((ColoringContour ct) => ct.points.Count() <= areaToRemove);
						int number = coloringContour.number;
						List<Point> list = new List<Point>();
						foreach (Point point in coloringContour.points)
						{
							list.Add(new Point(point.X, point.Y));
						}
						while (coloringContour.points.Count() > 0)
						{
							Dictionary<KeyValuePair<int, int>, NumberAndColorOfContour> dictionary = new Dictionary<KeyValuePair<int, int>, NumberAndColorOfContour>();
							for (int i = 0; i < coloringContour.points.Count(); i++)
							{
								Point item = coloringContour.points[i];
								int x = item.X;
								int y = item.Y;
								List<Point> list2 = new List<Point>();
								int num4 = x - 1;
								int num5 = y;
								if (num4 >= 0 && num4 < Param.WidthInBead && num5 >= 0 && num5 < Param.HeightInBead)
								{
									list2.Add(new Point(num4, num5));
								}
								num4 = x;
								num5 = y + 1;
								if (num4 >= 0 && num4 < Param.WidthInBead && num5 >= 0 && num5 < Param.HeightInBead)
								{
									list2.Add(new Point(num4, num5));
								}
								num4 = x + 1;
								num5 = y;
								if (num4 >= 0 && num4 < Param.WidthInBead && num5 >= 0 && num5 < Param.HeightInBead)
								{
									list2.Add(new Point(num4, num5));
								}
								num4 = x;
								num5 = y - 1;
								if (num4 >= 0 && num4 < Param.WidthInBead && num5 >= 0 && num5 < Param.HeightInBead)
								{
									list2.Add(new Point(num4, num5));
								}
								foreach (Point item2 in list2)
								{
									_point = numberAndColorOfContour[item2.X, item2.Y];
									if (_point.number != number)
									{
										contours.First((ColoringContour cntr) => cntr.number == _point.number).points.Add(item);
										coloringContour.points.RemoveAt(i);
										dictionary.Add(new KeyValuePair<int, int>(x, y), new NumberAndColorOfContour
										{
											number = _point.number,
											B = _point.B,
											G = _point.G,
											R = _point.R
										});
										int num6 = y * stride + x * num;
										array[num6] = _point.B;
										array[num6 + 1] = _point.G;
										array[num6 + 2] = _point.R;
										(blocks[x, y] as PixelForColoring).clr = Color.FromArgb(255, _point.R, _point.G, _point.B);
										i--;
										break;
									}
								}
							}
							foreach (KeyValuePair<KeyValuePair<int, int>, NumberAndColorOfContour> item3 in dictionary)
							{
								NumberAndColorOfContour[,] obj = numberAndColorOfContour;
								KeyValuePair<int, int> key = item3.Key;
								int key2 = key.Key;
								key = item3.Key;
								NumberAndColorOfContour numberAndColorOfContour2 = obj[key2, key.Value];
								numberAndColorOfContour2.number = item3.Value.number;
								numberAndColorOfContour2.R = item3.Value.R;
								numberAndColorOfContour2.G = item3.Value.G;
								numberAndColorOfContour2.B = item3.Value.B;
							}
						}
						contours.Remove(coloringContour);
					}
					Marshal.Copy(array, 0, scan, array.Length);
				}
				finally
				{
					bmpBeadwork.UnlockBits(bitmapData);
				}
			}
		}

		private void ComparePointsInContoursAndMergeItIfNeed(int j, int i, int X, int Y, ref List<ColoringContour> contours, ref NumberAndColorOfContour[,] numberAndColorOfContour)
		{
			if (numberAndColorOfContour[j, i].number != numberAndColorOfContour[X, Y].number && numberAndColorOfContour[j, i].R == numberAndColorOfContour[X, Y].R && numberAndColorOfContour[j, i].G == numberAndColorOfContour[X, Y].G && numberAndColorOfContour[j, i].B == numberAndColorOfContour[X, Y].B)
			{
				int numberOld = numberAndColorOfContour[X, Y].number;
				int numberNew = numberAndColorOfContour[j, i].number;
				ColoringContour coloringContour = contours.First((ColoringContour c) => c.number == numberOld);
				ColoringContour item = contours.First((ColoringContour c) => c.number == numberNew);
				int index = contours.IndexOf(coloringContour);
				int index2 = contours.IndexOf(item);
				foreach (Point point in coloringContour.points)
				{
					numberAndColorOfContour[point.X, point.Y].number = numberNew;
				}
				foreach (Point point2 in contours[index].points)
				{
					contours[index2].points.Add(new Point(point2.X, point2.Y));
				}
				contours.RemoveAt(index);
			}
		}

		private void GenerateColoringSchemeAndPrintOrSaveToFolder(SavingOrPrintingSettings settings, Bitmap bmpBeadwork, List<Beads> beads, NumberAndColorOfContour[,] numberAndColorOfContour, List<ColoringContour> contours)
		{
			float num = Param.WidthColoringInSm * ((float)Param.PrintingResolution / 2.54f);
			float num2 = ((float)bmpBeadwork.Height + 0f) / ((float)bmpBeadwork.Width + 0f) * num;
			float num3 = num / ((float)bmpBeadwork.Width + 0f);
			float num4 = num2 / ((float)bmpBeadwork.Height + 0f);
			float num5 = (num3 + num4) / 2f;
			Bitmap bitmap = new Bitmap((int)Math.Round((double)((float)bmpBeadwork.Width * num5)), (int)Math.Round((double)((float)bmpBeadwork.Height * num5)));
			bitmap.SetResolution((float)Param.PrintingResolution, (float)Param.PrintingResolution);
			Graphics graphics = Graphics.FromImage(bitmap);
			graphics.FillRectangle(Brushes.White, new Rectangle(0, 0, bitmap.Width, bitmap.Height));
			graphics.Dispose();
			byte brightnessOfContour = Param.BrightnessOfContour;
			BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
			BitmapData bitmapData2 = bmpBeadwork.LockBits(new Rectangle(0, 0, bmpBeadwork.Width, bmpBeadwork.Height), ImageLockMode.ReadWrite, bmpBeadwork.PixelFormat);
			try
			{
				int num6 = (bmpBeadwork.PixelFormat == (bmpBeadwork.PixelFormat | PixelFormat.Alpha)) ? 4 : 3;
				int num7 = bmpBeadwork.Width * bmpBeadwork.Height * num6;
				byte[] array = new byte[num7];
				IntPtr scan = bitmapData2.Scan0;
				Marshal.Copy(scan, array, 0, array.Length);
				bool flag = false;
				int num8 = (bitmap.PixelFormat == (bitmap.PixelFormat | PixelFormat.Alpha)) ? 4 : 3;
				int num9 = bitmap.Width * bitmap.Height * num8;
				byte[] array2 = new byte[num9];
				IntPtr scan2 = bitmapData.Scan0;
				Marshal.Copy(scan2, array2, 0, array2.Length);
				float num10 = ((float)(int)Param.TransparentColorsOnCanvas + 0f) / 255f;
				float num11 = (1f - num10) * 255f;
				int width = bitmap.Width;
				int height = bitmap.Height;
				int width2 = bmpBeadwork.Width;
				int height2 = bmpBeadwork.Height;
				int num12 = Param.thicknessOfAdditionalBorderOfColoringContour;
				if (v == 1)
				{
					num12 = 0;
				}
				int num13 = brightnessOfContour;
				if (num12 > 0)
				{
					num13 = ((num12 != 1) ? brightnessOfContour : (brightnessOfContour + (255 - brightnessOfContour) / 2));
				}
				for (int j = 0; j < height; j++)
				{
					for (int k = 0; k < width; k++)
					{
						try
						{
							flag = false;
							int num14 = (int)Math.Round((double)(((float)k + 0f) / num5));
							int num15 = (int)Math.Round((double)(((float)j + 0f) / num5));
							byte b;
							byte b2;
							byte b3;
							if (num14 < width2 - 1 && num15 < height2 - 1)
							{
								int num16 = num15 * bitmapData2.Stride + num14 * num6;
								int num17 = (num15 + 1) * bitmapData2.Stride + num14 * num6;
								int num18 = num15 * bitmapData2.Stride + (num14 + 1) * num6;
								b = array[num16];
								b2 = array[num16 + 1];
								b3 = array[num16 + 2];
								byte b4 = array[num17];
								byte b5 = array[num17 + 1];
								byte b6 = array[num17 + 2];
								if (b != b4 || b2 != b5 || b3 != b6)
								{
									flag = true;
								}
								else
								{
									byte b7 = array[num18];
									byte b8 = array[num18 + 1];
									byte b9 = array[num18 + 2];
									if (b != b7 || b2 != b8 || b3 != b9)
									{
										flag = true;
									}
								}
							}
							else if (num14 < width2 - 1 && num15 >= height2 - 1)
							{
								num15 = height2 - 1;
								int num16 = num15 * bitmapData2.Stride + num14 * num6;
								int num18 = num15 * bitmapData2.Stride + (num14 + 1) * num6;
								b = array[num16];
								b2 = array[num16 + 1];
								b3 = array[num16 + 2];
								byte b7 = array[num18];
								byte b8 = array[num18 + 1];
								byte b9 = array[num18 + 2];
								if (b != b7 || b2 != b8 || b3 != b9)
								{
									flag = true;
								}
							}
							else if (num14 >= width2 - 1 && num15 < height2 - 1)
							{
								num14 = width2 - 1;
								int num16 = num15 * bitmapData2.Stride + num14 * num6;
								int num17 = (num15 + 1) * bitmapData2.Stride + num14 * num6;
								b = array[num16];
								b2 = array[num16 + 1];
								b3 = array[num16 + 2];
								byte b4 = array[num17];
								byte b5 = array[num17 + 1];
								byte b6 = array[num17 + 2];
								if (b != b4 || b2 != b5 || b3 != b6)
								{
									flag = true;
								}
							}
							else
							{
								num15 = height2 - 1;
								num14 = width2 - 1;
								int num16 = num15 * bitmapData2.Stride + num14 * num6;
								b = array[num16];
								b2 = array[num16 + 1];
								b3 = array[num16 + 2];
							}
							int num19 = j * bitmapData.Stride + k * num8;
							if (flag)
							{
								byte[] array3 = array2;
								int num20 = num19;
								byte[] array4 = array2;
								int num21 = num19 + 1;
								byte b10;
								array2[num19 + 2] = (b10 = brightnessOfContour);
								array4[num21] = (b10 = b10);
								array3[num20] = b10;
								if (num12 > 0)
								{
									if (j + 1 < height)
									{
										num19 = (j + 1) * bitmapData.Stride + k * num8;
										if (array2[num19] != num13 && array2[num19 + 1] != num13 && array2[num19 + 2] != num13 && array2[num19] != brightnessOfContour && array2[num19 + 1] != brightnessOfContour && array2[num19 + 2] != brightnessOfContour)
										{
											byte[] array5 = array2;
											int num22 = num19;
											byte[] array6 = array2;
											int num23 = num19 + 1;
											array2[num19 + 2] = (b10 = (byte)num13);
											array6[num23] = (b10 = b10);
											array5[num22] = b10;
										}
									}
									if (j - 1 >= 0)
									{
										num19 = (j - 1) * bitmapData.Stride + k * num8;
										if (array2[num19] != num13 && array2[num19 + 1] != num13 && array2[num19 + 2] != num13 && array2[num19] != brightnessOfContour && array2[num19 + 1] != brightnessOfContour && array2[num19 + 2] != brightnessOfContour)
										{
											byte[] array7 = array2;
											int num24 = num19;
											byte[] array8 = array2;
											int num25 = num19 + 1;
											array2[num19 + 2] = (b10 = (byte)num13);
											array8[num25] = (b10 = b10);
											array7[num24] = b10;
										}
									}
									if (k + 1 < width)
									{
										num19 = j * bitmapData.Stride + (k + 1) * num8;
										if (array2[num19] != num13 && array2[num19 + 1] != num13 && array2[num19 + 2] != num13 && array2[num19] != brightnessOfContour && array2[num19 + 1] != brightnessOfContour && array2[num19 + 2] != brightnessOfContour)
										{
											byte[] array9 = array2;
											int num26 = num19;
											byte[] array10 = array2;
											int num27 = num19 + 1;
											array2[num19 + 2] = (b10 = (byte)num13);
											array10[num27] = (b10 = b10);
											array9[num26] = b10;
										}
									}
									if (k - 1 >= 0)
									{
										num19 = j * bitmapData.Stride + (k - 1) * num8;
										if (array2[num19] != num13 && array2[num19 + 1] != num13 && array2[num19 + 2] != num13 && array2[num19] != brightnessOfContour && array2[num19 + 1] != brightnessOfContour && array2[num19 + 2] != brightnessOfContour)
										{
											byte[] array11 = array2;
											int num28 = num19;
											byte[] array12 = array2;
											int num29 = num19 + 1;
											array2[num19 + 2] = (b10 = (byte)num13);
											array12[num29] = (b10 = b10);
											array11[num28] = b10;
										}
									}
								}
							}
							else if (num10 > 0.01f && array2[num19] != num13 && array2[num19 + 1] != num13 && array2[num19 + 2] != num13 && array2[num19] != brightnessOfContour && array2[num19 + 1] != brightnessOfContour && array2[num19 + 2] != brightnessOfContour)
							{
								array2[num19] = (byte)Math.Round((double)(num11 + ((float)(int)b + 0f) * num10));
								array2[num19 + 1] = (byte)Math.Round((double)(num11 + ((float)(int)b2 + 0f) * num10));
								array2[num19 + 2] = (byte)Math.Round((double)(num11 + ((float)(int)b3 + 0f) * num10));
							}
						}
						catch (Exception)
						{
							int num30 = 0;
							int num32 = num30--;
							num32++;
						}
					}
				}
				Marshal.Copy(array2, 0, scan2, array2.Length);
				Marshal.Copy(array, 0, scan, array.Length);
			}
			finally
			{
				bitmap.UnlockBits(bitmapData);
				bmpBeadwork.UnlockBits(bitmapData2);
			}
			NumberAndColorOfContour[,] array13 = new NumberAndColorOfContour[numberAndColorOfContour.GetLength(0), numberAndColorOfContour.GetLength(1)];
			for (int l = 0; l < numberAndColorOfContour.GetLength(1); l++)
			{
				for (int m = 0; m < numberAndColorOfContour.GetLength(0); m++)
				{
					NumberAndColorOfContour[,] array14 = array13;
					int num33 = m;
					int num34 = l;
					NumberAndColorOfContour obj = new NumberAndColorOfContour
					{
						R = numberAndColorOfContour[m, l].R,
						G = numberAndColorOfContour[m, l].G,
						B = numberAndColorOfContour[m, l].B,
						number = numberAndColorOfContour[m, l].number
					};
					array14[num33, num34] = obj;
				}
			}
			List<PointForContourSymbol> list = new List<PointForContourSymbol>();
			Point point;
			for (int n = 0; n < contours.Count(); n++)
			{
				contours[n].skeletonPoints = new List<Point>();
				foreach (Point point2 in contours[n].points)
				{
					contours[n].skeletonPoints.Add(new Point(point2.X, point2.Y));
				}
				ColoringContour coloringContour = contours[n];
				int number = coloringContour.number;
				List<Point> list2 = new List<Point>();
				list2.AddRange(contours[n].points);
				while (coloringContour.skeletonPoints.Count() > (int)Math.Round((double)((float)list2.Count() * 0.1f)))
				{
					List<Point> list3 = new List<Point>();
					ColoringContour coloringContour2 = new ColoringContour
					{
						number = number,
						skeletonPoints = new List<Point>()
					};
					for (int num35 = 0; num35 < coloringContour.skeletonPoints.Count(); num35++)
					{
						if (coloringContour.skeletonPoints.Count() == 1)
						{
							break;
						}
						point = coloringContour.skeletonPoints[num35];
						int x2 = point.X;
						point = coloringContour.skeletonPoints[num35];
						int y2 = point.Y;
						bool flag2 = false;
						if (x2 == 0 || y2 == 0 || x2 == Param.WidthInBead - 1 || y2 == Param.HeightInBead - 1 || array13[x2 - 1, y2].number != number || array13[x2 + 1, y2].number != number || array13[x2, y2 - 1].number != number || array13[x2, y2 + 1].number != number)
						{
							flag2 = true;
						}
						if (!flag2)
						{
							coloringContour2.skeletonPoints.Add(coloringContour.skeletonPoints[num35]);
						}
						else
						{
							list3.Add(new Point(x2, y2));
						}
					}
					foreach (Point item in list3)
					{
						array13[item.X, item.Y].number = -1;
					}
					contours[n] = coloringContour;
					coloringContour = coloringContour2;
				}
				PointForContourSymbol pointForContourSymbol = new PointForContourSymbol
				{
					pointsInArea = new List<Point>(),
					parentContour = contours[n],
					number = n + 1
				};
				pointForContourSymbol.pointsInArea.AddRange(contours[n].skeletonPoints);
				pointForContourSymbol.primaryPointsCount = pointForContourSymbol.pointsInArea.Count();
				list.Add(pointForContourSymbol);
				contours[n].skeletonPoints.Clear();
				contours[n].points.Clear();
				contours[n].points.AddRange(list2);
				list2.Clear();
			}
			int num36 = contours.Count() + 1;
			bool flag3 = false;
			while (!flag3)
			{
				flag3 = true;
				for (int num37 = 0; num37 < list.Count(); num37++)
				{
					list[num37].pointsInArea = (from pt in list[num37].pointsInArea
					orderby pt.X
					select pt).ToList();
					int num38 = -1;
					for (int num39 = 0; num39 < list[num37].pointsInArea.Count() - 1; num39++)
					{
						point = list[num37].pointsInArea[num39 + 1];
						int x3 = point.X;
						point = list[num37].pointsInArea[num39];
						if (x3 - point.X > 1)
						{
							flag3 = false;
							num38 = num39;
							break;
						}
					}
					if (num38 > -1)
					{
						PointForContourSymbol pointForContourSymbol2 = new PointForContourSymbol
						{
							parentContour = list[num37].parentContour,
							pointsInArea = new List<Point>(),
							number = num36++
						};
						int num41 = list[num37].pointsInArea.Count() - num38 - 1;
						pointForContourSymbol2.pointsInArea.AddRange(list[num37].pointsInArea.GetRange(num38 + 1, num41));
						pointForContourSymbol2.primaryPointsCount = num41;
						list.Add(pointForContourSymbol2);
						list[num37].pointsInArea.RemoveRange(num38 + 1, list[num37].pointsInArea.Count() - num38 - 1);
						list[num37].primaryPointsCount -= num41;
					}
				}
				for (int num42 = 0; num42 < list.Count(); num42++)
				{
					list[num42].pointsInArea = (from pt in list[num42].pointsInArea
					orderby pt.Y
					select pt).ToList();
					int num43 = -1;
					for (int num44 = 0; num44 < list[num42].pointsInArea.Count() - 1; num44++)
					{
						point = list[num42].pointsInArea[num44 + 1];
						int y3 = point.Y;
						point = list[num42].pointsInArea[num44];
						if (y3 - point.Y > 1)
						{
							flag3 = false;
							num43 = num44;
							break;
						}
					}
					if (num43 > -1)
					{
						PointForContourSymbol pointForContourSymbol2 = new PointForContourSymbol
						{
							parentContour = list[num42].parentContour,
							pointsInArea = new List<Point>(),
							number = num36++
						};
						int num46 = list[num42].pointsInArea.Count() - num43 - 1;
						pointForContourSymbol2.pointsInArea.AddRange(list[num42].pointsInArea.GetRange(num43 + 1, num46));
						pointForContourSymbol2.primaryPointsCount = num46;
						list.Add(pointForContourSymbol2);
						list[num42].pointsInArea.RemoveRange(num43 + 1, list[num42].pointsInArea.Count() - num43 - 1);
						list[num42].primaryPointsCount -= num46;
					}
				}
			}
			int[,] array15 = new int[Param.WidthInBead, Param.HeightInBead];
			for (int num47 = 0; num47 < Param.HeightInBead; num47++)
			{
				for (int num48 = 0; num48 < Param.WidthInBead; num48++)
				{
					array15[num48, num47] = -1;
				}
			}
			for (int num49 = 0; num49 < list.Count(); num49++)
			{
				int number2 = list[num49].number;
				List<Point> pointsInArea = list[num49].pointsInArea;
				for (int num50 = 0; num50 < pointsInArea.Count(); num50++)
				{
					int[,] array16 = array15;
					point = pointsInArea[num50];
					int x4 = point.X;
					point = pointsInArea[num50];
					array16[x4, point.Y] = number2;
				}
			}
			for (int num51 = 0; num51 < list.Count(); num51++)
			{
				int number3 = list[num51].number;
				while (list[num51].pointsInArea.Count() > 1)
				{
					List<Point> list4 = new List<Point>();
					for (int num52 = 0; num52 < list[num51].pointsInArea.Count(); num52++)
					{
						if (list[num51].pointsInArea.Count() == 1)
						{
							break;
						}
						point = list[num51].pointsInArea[num52];
						int x2 = point.X;
						point = list[num51].pointsInArea[num52];
						int y2 = point.Y;
						bool flag4 = false;
						if (x2 == 0 || y2 == 0 || x2 == Param.WidthInBead - 1 || y2 == Param.HeightInBead - 1 || array15[x2 - 1, y2] != number3 || array15[x2 + 1, y2] != number3 || array15[x2, y2 - 1] != number3 || array15[x2, y2 + 1] != number3)
						{
							flag4 = true;
						}
						if (flag4)
						{
							list4.Add(new Point(x2, y2));
							list[num51].pointsInArea.Remove(list[num51].pointsInArea[num52]);
						}
					}
					if (list4.Count() >= list[num51].pointsInArea.Count())
					{
						break;
					}
					foreach (Point item2 in list4)
					{
						array15[item2.X, item2.Y] = -1;
					}
				}
			}
			array15 = null;
			for (int num53 = 0; num53 < list.Count(); num53++)
			{
				if (list[num53].pointsInArea.Count() > 1)
				{
					list[num53].pointsInArea.Sort(delegate(Point x, Point y)
					{
						int num71;
						if (x.X != y.X)
						{
							num71 = x.X;
							return num71.CompareTo(y.X);
						}
						num71 = x.Y;
						return num71.CompareTo(y.Y);
					});
					int index = list[num53].pointsInArea.Count() / 2;
					list[num53].ptForSymbol = list[num53].pointsInArea[index];
				}
				else
				{
					list[num53].ptForSymbol = list[num53].pointsInArea[0];
				}
			}
			float num54 = 2f;
			float num55 = num54 * ((float)Param.PrintingResolution / 2.54f) / num5;
			for (int num56 = 0; num56 < contours.Count; num56++)
			{
				int i = contours[num56].number;
				List<PointForContourSymbol> list5 = (from ct in list
				where ct.parentContour.number == i
				select ct).ToList();
				bool flag5 = false;
				while (!flag5)
				{
					flag5 = true;
					list5 = (from ct in list
					where ct.parentContour.number == i
					orderby ct.primaryPointsCount descending
					select ct).ToList();
					for (int num57 = 0; num57 < list5.Count(); num57++)
					{
						for (int num58 = list5.Count() - 1; num58 > num57; num58--)
						{
							double num59 = Math.Sqrt((double)((list5[num57].ptForSymbol.X - list5[num58].ptForSymbol.X) * (list5[num57].ptForSymbol.X - list5[num58].ptForSymbol.X) + (list5[num57].ptForSymbol.Y - list5[num58].ptForSymbol.Y) * (list5[num57].ptForSymbol.Y - list5[num58].ptForSymbol.Y)));
							if (num59 < (double)num55)
							{
								flag5 = false;
								list.Remove(list5[num58]);
								list5.Remove(list5[num58]);
								num58--;
							}
						}
					}
				}
			}
			foreach (PointForContourSymbol item3 in list)
			{
				int number4 = item3.parentContour.number;
				int x5 = item3.ptForSymbol.X;
				int y4 = item3.ptForSymbol.Y;
				int num60 = 1;
				bool flag6 = false;
				while (!flag6)
				{
					if (x5 - num60 < 0 || y4 - num60 < 0 || x5 + num60 >= numberAndColorOfContour.GetLength(0) || y4 + num60 >= numberAndColorOfContour.GetLength(1) || numberAndColorOfContour[x5 - num60, y4].number != number4 || numberAndColorOfContour[x5 + num60, y4].number != number4 || numberAndColorOfContour[x5, y4 - num60].number != number4 || numberAndColorOfContour[x5, y4 + num60].number != number4)
					{
						flag6 = true;
					}
					num60++;
				}
				item3.sizeOfFont = num60;
			}
			Graphics graphics2 = Graphics.FromImage(bitmap);
			graphics2.DrawRectangle(new Pen(Color.Black, 1f), new Rectangle(0, 0, bitmap.Width - 1, bitmap.Height - 1));
			byte brightnessOfLabel = Param.BrightnessOfLabel;
			for (int num61 = 0; num61 < list.Count(); num61++)
			{
				byte R = array13[list[num61].ptForSymbol.X, list[num61].ptForSymbol.Y].R;
				byte G = array13[list[num61].ptForSymbol.X, list[num61].ptForSymbol.Y].G;
				byte B = array13[list[num61].ptForSymbol.X, list[num61].ptForSymbol.Y].B;
				string text = beads.FirstOrDefault(delegate(Beads bd)
				{
					Color clr = bd.clr;
					if (clr.R == R)
					{
						clr = bd.clr;
						if (clr.G == G)
						{
							clr = bd.clr;
							return clr.B == B;
						}
					}
					return false;
				}).symbolForColoring_AndForVoiceWorkInBiserok.ToString();
				float num62 = (float)Param.PrintingResolution / 10f;
				int x2 = (int)Math.Round((double)((float)list[num61].ptForSymbol.X * num5));
				int y2 = (int)Math.Round((double)((float)list[num61].ptForSymbol.Y * num5));
				float num63 = 8.46f * ((float)Param.countOfPixelsInCmAtContoursMapOfColoring / 40f);
				if ((float)list[num61].sizeOfFont < num63)
				{
					float num64 = (float)list[num61].sizeOfFont / num63;
					num62 *= num64;
				}
				if (Settings.v >= 3 && v >= 3)
				{
					float minFontSizeInPt = Param.minFontSizeInPt;
					float num65 = minFontSizeInPt / 72f * ((float)Param.PrintingResolution + 0f) / 0.935f;
					if (num62 < num65)
					{
						float num66 = num65 / num62;
						num62 *= num66;
					}
				}
				Font font = new Font("Verdana", num62, GraphicsUnit.Pixel);
				SizeF sizeF = graphics2.MeasureString(text, font);
				float num67 = sizeF.Height / 2f;
				float num68 = sizeF.Width / 2f;
				int num69 = (int)Math.Round((double)((float)x2 - num68));
				int num70 = (int)Math.Round((double)((float)y2 - num67));
				if (num69 <= (int)((0f - sizeF.Width) * 0.14f))
				{
					num69 = (int)((0f - sizeF.Width) * 0.14f);
				}
				if (num70 <= (int)((0f - sizeF.Height) * 0.2f))
				{
					num70 = (int)((0f - sizeF.Height) * 0.2f);
				}
				if ((float)num69 + sizeF.Width * 0.86f > (float)(bitmap.Width - 1))
				{
					num69 = (int)((float)(bitmap.Width - 1) - sizeF.Width * 0.86f);
				}
				if ((float)num70 + sizeF.Height * 0.8f > (float)(bitmap.Height - 1))
				{
					num70 = (int)((float)(bitmap.Height - 1) - sizeF.Height * 0.8f);
				}
				graphics2.DrawString(text, font, new SolidBrush(Color.FromArgb(255, brightnessOfLabel, brightnessOfLabel, brightnessOfLabel)), new Point(num69, num70));
			}
			array13 = null;
			if (v == 0)
			{
				float emSize = 90f * KfSystemFontSizes;
				graphics2.DrawString("Демо-версия", new Font("Verdana", emSize), new SolidBrush(Color.Red), new PointF(10f, 10f));
				graphics2.DrawString("Демо-версия", new Font("Verdana", emSize), new SolidBrush(Color.Red), new PointF((float)(bitmap.Width / 3), (float)bitmap.Height * 0.254f));
				graphics2.DrawString("Демо-версия", new Font("Verdana", emSize), new SolidBrush(Color.Red), new PointF(10f, (float)bitmap.Height * 0.494f));
				graphics2.DrawString("Демо-версия", new Font("Verdana", emSize), new SolidBrush(Color.Red), new PointF((float)(bitmap.Width / 3), (float)bitmap.Height * 0.754f));
			}
			graphics2.Dispose();
			if (settings.isPrinting)
			{
				PrintingBmp = bitmap;
				pd.Print();
			}
			if (settings.isSaving)
			{
				bitmap.Save(settings.pathToSave + "\\" + Local.Get("Картина по номерам") + ".png", ImageFormat.Png);
			}
			bitmap.Dispose();
			GC.Collect(GC.MaxGeneration);
		}

		private void AddLineToContour(int x1, int x2, int y, byte R, byte G, byte B, int beginI, int maxI, int beginJ, int maxJ, ref ColoringContour contour, ref byte[] srcBytes, int stride, int srcK, ref NumberAndColorOfContour[,] numberAndColorOfContour)
		{
			if (y >= beginI && maxI > y)
			{
				int num = x1;
				Point item;
				while (num >= beginJ && numberAndColorOfContour[num, y] == null)
				{
					int num2 = y * stride + num * srcK;
					byte b = srcBytes[num2];
					byte b2 = srcBytes[num2 + 1];
					byte b3 = srcBytes[num2 + 2];
					if (b3 != R)
					{
						break;
					}
					if (b2 != G)
					{
						break;
					}
					if (b != B)
					{
						break;
					}
					List<Point> points = contour.points;
					item = new Point
					{
						X = num,
						Y = y
					};
					points.Add(item);
					NumberAndColorOfContour[,] obj = numberAndColorOfContour;
					int num3 = num;
					int num4 = y;
					NumberAndColorOfContour obj2 = new NumberAndColorOfContour
					{
						number = contour.number,
						R = R,
						G = G,
						B = B
					};
					obj[num3, num4] = obj2;
					num--;
				}
				if (num < beginJ)
				{
					num = beginJ;
				}
				if (num < x1 - 2)
				{
					AddLineToContour(num + 1, x1 - 1, y - 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
					AddLineToContour(num + 1, x1 - 1, y + 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
				}
				else if (num < x1 - 1)
				{
					AddLineToContour(num + 1, x1, y - 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
					AddLineToContour(num + 1, x1, y + 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
				}
				else if (num < x1)
				{
					AddLineToContour(num, x1, y - 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
					AddLineToContour(num, x1, y + 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
				}
				int num5 = x2;
				if (x1 == x2 && contour.points.Any(delegate(Point pt)
				{
					if (pt.X == x2)
					{
						return pt.Y == y;
					}
					return false;
				}))
				{
					num5 = x2 + 1;
				}
				int i;
				for (i = num5; i < maxJ; i++)
				{
					try
					{
						if (numberAndColorOfContour[i, y] == null)
						{
							int num6 = y * stride + i * srcK;
							byte b4 = srcBytes[num6];
							byte b5 = srcBytes[num6 + 1];
							byte b6 = srcBytes[num6 + 2];
							if (b6 == R && b5 == G && b4 == B)
							{
								List<Point> points2 = contour.points;
								item = new Point
								{
									X = i,
									Y = y
								};
								points2.Add(item);
								NumberAndColorOfContour[,] obj3 = numberAndColorOfContour;
								int num7 = i;
								int num8 = y;
								NumberAndColorOfContour obj4 = new NumberAndColorOfContour
								{
									number = contour.number,
									R = R,
									G = G,
									B = B
								};
								obj3[num7, num8] = obj4;
								continue;
							}
						}
					}
					catch (Exception)
					{
						continue;
					}
					break;
				}
				try
				{
					if (x2 < i - 2)
					{
						AddLineToContour(x2 + 1, i - 1, y - 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
						AddLineToContour(x2 + 1, i - 1, y + 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
						int num9 = --x2;
					}
					else if (x2 < i - 1)
					{
						AddLineToContour(x2, i - 1, y - 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
						AddLineToContour(x2, i - 1, y + 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
						int num9 = --x2;
					}
					else if (x2 < i)
					{
						AddLineToContour(x2, i, y - 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
						AddLineToContour(x2, i, y + 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
						int num9 = --x2;
					}
				}
				catch (Exception)
				{
				}
				for (i = x1; i <= x2 && i < maxJ; i++)
				{
					try
					{
						int num10 = y * stride + i * srcK;
						byte b7 = srcBytes[num10];
						byte b8 = srcBytes[num10 + 1];
						byte b9 = srcBytes[num10 + 2];
						if (b9 == R && b8 == G && b7 == B && numberAndColorOfContour[i, y] == null)
						{
							List<Point> points3 = contour.points;
							item = new Point
							{
								X = i,
								Y = y
							};
							points3.Add(item);
							NumberAndColorOfContour[,] obj5 = numberAndColorOfContour;
							int num11 = i;
							int num12 = y;
							NumberAndColorOfContour obj6 = new NumberAndColorOfContour
							{
								number = contour.number,
								R = R,
								G = G,
								B = B
							};
							obj5[num11, num12] = obj6;
						}
						else
						{
							if (x1 < i)
							{
								AddLineToContour(x1, i - 1, y - 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
								AddLineToContour(x1, i - 1, y + 1, R, G, B, beginI, maxI, beginJ, maxJ, ref contour, ref srcBytes, stride, srcK, ref numberAndColorOfContour);
								x1 = i;
							}
							while (i <= x2 && i < maxJ)
							{
								if (b9 != R || b8 != G || b7 != B || numberAndColorOfContour[i, y] != null)
								{
									i++;
									continue;
								}
								x1 = i--;
								break;
							}
						}
					}
					catch (Exception)
					{
					}
				}
			}
		}

		private void SaveBeadworkView(SavingOrPrintingSettings settings, Bitmap bmpBeadwork)
		{
			if (v == 0 || settings.numberOfTab != 1)
			{
				Bitmap bitmap = (Bitmap)bmpBeadwork.Clone();
				Graphics graphics = Graphics.FromImage(bitmap);
				if (v == 0)
				{
					float emSize = (float)(bitmap.Width / 20) * KfSystemFontSizes;
					graphics.DrawString("Демо-версия", new Font("Verdana", emSize), new SolidBrush(Color.Red), new PointF((float)(bitmap.Width / 2), (float)(bitmap.Height / 4)));
					graphics.DrawString("Демо-версия", new Font("Verdana", emSize), new SolidBrush(Color.Red), new PointF(10f, (float)(bitmap.Height / 4 * 2)));
					graphics.DrawString("Демо-версия", new Font("Verdana", emSize), new SolidBrush(Color.Red), new PointF((float)(bitmap.Width / 2), (float)bitmap.Height * 3f / 4f));
				}
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					bitmap.Save(settings.pathToSave + "\\" + Local.Get("Вид ") + Settings.mainWord + ".bmp", ImageFormat.Bmp);
				}
				else
				{
					if (Settings.hobbyProgramm == HobbyProgramm.Krestik && settings.numberOfTab == 3)
					{
						graphics.DrawImage(BmpBackAndKnot, new Rectangle(0, 0, bitmap.Width, bitmap.Height));
					}
					bitmap.Save(settings.pathToSave + "\\" + Local.Get("Вид ") + Settings.mainWord + ".png", format);
				}
				graphics.Dispose();
				bitmap.Dispose();
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				bmpBeadwork.Save(settings.pathToSave + "\\" + Local.Get("Вид ") + Settings.mainWord + ".bmp", ImageFormat.Bmp);
			}
			else
			{
				bmpBeadwork.Save(settings.pathToSave + "\\" + Local.Get("Вид ") + Settings.mainWord + ".png", format);
			}
		}

		private void CreateColorsOfBordersForPrintingOnTextileScheme(List<Beads> _resBeads)
		{
			Random random = new Random(DateTime.Now.Millisecond);
			Color color = Color.Transparent;
			bool flag = false;
			List<Color> list = new List<Color>();
			for (int i = 0; i < _resBeads.Count(); i++)
			{
				flag = false;
				while (!flag)
				{
					color = Color.FromArgb(255, random.Next(0, 256), random.Next(0, 256), random.Next(0, 256));
					if (GetDistanceBetweenColors(_resBeads[i].clr, color) <= 100f)
					{
						flag = true;
						for (int j = 0; j < i; j++)
						{
							if (GetDistanceBetweenColors(_resBeads[i].clr, _resBeads[j].clr) <= 30f && GetDistanceBetweenColors(color, list[j]) < 40f)
							{
								flag = false;
							}
							if (!flag)
							{
								break;
							}
						}
					}
				}
				_resBeads[i].clrBorder = color;
				list.Add(color);
			}
		}

		private void Print_Page(object o, PrintPageEventArgs e)
		{
			RectangleF printableArea = e.PageSettings.PrintableArea;
			int num = (int)printableArea.Height;
			printableArea = e.PageSettings.PrintableArea;
			int num2 = (int)printableArea.Width;
			if (((float)PrintingBmp.Height + 0f) / ((float)PrintingBmp.Width + 0f) > ((float)num + 0f) / ((float)num2 + 0f))
			{
				num2 = (int)Math.Round((double)(((float)PrintingBmp.Width + 0f) / ((float)PrintingBmp.Height + 0f) * (float)num));
			}
			else
			{
				num = (int)Math.Round((double)(((float)PrintingBmp.Height + 0f) / ((float)PrintingBmp.Width + 0f) * (float)num2));
			}
			e.Graphics.DrawImage(PrintingBmp, new RectangleF(0f, 0f, (float)num2, (float)num));
		}

		private ResultOfSavingOrPrinting GenerateFullSchemeSource(SavingOrPrintingSettings settings)
		{
			return GenerateFullSchemeAndPrintingOrSaveToFolder(settings, BmpBeadworkSource, blocksMain, _resBeadsMain, numberAndColorOfContourMain, contoursMain);
		}

		private ResultOfSavingOrPrinting GenerateFullSchemeRework(SavingOrPrintingSettings settings)
		{
			return GenerateFullSchemeAndPrintingOrSaveToFolder(settings, BmpBeadworkRework, blocksRework, _resBeadsRework, null, null);
		}

		private ResultOfSavingOrPrinting GenerateFullSchemeHandEditing(SavingOrPrintingSettings settings)
		{
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				if (Param.BiserType == BiserType.ForManufacturers && Param.IsMixingColors)
				{
					_resBeadsHandEditing = DefineMixingColorsInColoringInMixingColorsMode(_resBeadsHandEditing);
					int i;
					for (i = 0; i < Param.HeightInBead; i++)
					{
						int widthInBead = Param.WidthInBead;
						Parallel.For(0, widthInBead, new ParallelOptions
						{
							MaxDegreeOfParallelism = Environment.ProcessorCount
						}, delegate(int j)
						{
							Beads resBeads = new Beads();
							Block block = blocksHandEditing[j, i];
							float num = 3.40282347E+38f;
							foreach (Beads item in _resBeadsHandEditing)
							{
								float distanceBetweenColors = GetDistanceBetweenColors(block.ResBeads.clr, item.clr);
								if (!(distanceBetweenColors >= num))
								{
									num = distanceBetweenColors;
									resBeads = item;
								}
							}
							block.ResBeads = resBeads;
						});
					}
					DrawEmbroideryView(ref BmpBeadworkHandEditing, blocksHandEditing);
				}
				DefineContoursOfColoringAndRedrawingColoringView(BmpBeadworkHandEditing, ref numberAndColorOfContourHandEditing, ref contoursHandEditing);
				UpdateColoringColorsCountAndCalcSquareInSm2ForEach(ref _resBeadsHandEditing, BmpBeadworkHandEditing.Width, BmpBeadworkHandEditing.Height, numberAndColorOfContourHandEditing, contoursHandEditing);
				GC.Collect(GC.MaxGeneration);
			}
			return GenerateFullSchemeAndPrintingOrSaveToFolder(settings, BmpBeadworkHandEditing, blocksHandEditing, _resBeadsHandEditing, numberAndColorOfContourHandEditing, contoursHandEditing);
		}

		private void GenerateSchemeForPrinting(Bitmap schemeForPrinting, Block[,] blocks, List<Beads> resBeads, int widthOnlyScheme, int heightOnlyScheme, int countRowsInMiniLegend, int countColumnsInMiniLegend, int countRecordsInMiniLegend, Point ptBeginMiniLegend, Dictionary<Beads, int> countOfEachBeads, IEnumerable<Block> notNullBlocks, float heightOfLogoForMaximInBead, float heightOfSmallImageForMaximInBead, float heightOfStringOfNameAndArticleForMaximInBead, float widthOfLogoForMaximInBead, float widthOfSmallImageForMaximInBead, Bitmap bmpBeadworkView = null)
		{
			schemeForPrinting.SetResolution((float)Param.PrintingResolution, (float)Param.PrintingResolution);
			Graphics graphics = Graphics.FromImage(schemeForPrinting);
			int num = (int)Math.Round((double)(SqureCellSideSchemeForPrinting_Y * 1.5f));
			int num2 = (int)Math.Round((double)(SqureCellSideSchemeForPrinting_X * 1.5f));
			float num3 = 0.25f * (float)(num + num2) / 2f * (300f / (float)Param.PrintingResolution);
			Font font = new Font("Verdana", num3);
			int num11;
			if ((Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina || Settings.isItSpecialVersionOfAlmazkaForElenaBartosova) && Param.BiserType == BiserType.ForManufacturers && Param.IsDrawSmallLegend)
			{
				graphics.FillRectangle(Brushes.White, new Rectangle(0, 0, schemeForPrinting.Width, schemeForPrinting.Height));
				if (widthOnlyScheme <= heightOnlyScheme)
				{
					if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						heightOfLogoForMaximInBead -= 2f;
					}
					int num4 = 0;
					int num5 = 0;
					int x;
					if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						num4 = (int)(widthOfLogoForMaximInBead * SqureCellSideSchemeForPrinting_X);
						num5 = (int)(heightOfLogoForMaximInBead * SqureCellSideSchemeForPrinting_X);
						x = widthOnlyScheme + (schemeForPrinting.Width - widthOnlyScheme - num4) / 2;
						int num6 = 0;
						Bitmap bitmap;
						if (Settings.isItSpecialVersionOfBiserokForMaxim)
						{
							bitmap = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/LogoMelnica.png");
							num6 = 80;
						}
						else
						{
							bitmap = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/logoArtHobby.png");
						}
						graphics.DrawImage(bitmap, new Rectangle(x, 0, num4, num5), new Rectangle(0, 0, bitmap.Width, bitmap.Height + num6), GraphicsUnit.Pixel);
						bitmap.Dispose();
					}
					int num7 = (int)(widthOfSmallImageForMaximInBead * SqureCellSideSchemeForPrinting_X);
					int num8 = (int)(heightOfSmallImageForMaximInBead * SqureCellSideSchemeForPrinting_X);
					int y = num5;
					x = widthOnlyScheme + (schemeForPrinting.Width - widthOnlyScheme - num7) / 2;
					if (Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
					{
						y = 2 + (int)Math.Round((double)(heightOfStringOfNameAndArticleForMaximInBead * SqureCellSideSchemeForPrinting_Y));
						y /= 2;
					}
					if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina || Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
					{
						graphics.DrawImage(bmpBeadworkView, new Rectangle(x, y, num7, num8), new Rectangle(0, 0, bmpBeadworkView.Width, bmpBeadworkView.Height), GraphicsUnit.Pixel);
					}
					else
					{
						graphics.DrawImage(BmpSourceImage, new Rectangle(x, y, num7, num8), new Rectangle(0, 0, BmpSourceImage.Width, BmpSourceImage.Height), GraphicsUnit.Pixel);
					}
					string text = "";
					string text2 = "";
					string text3 = "";
					string text4 = "";
					if (Settings.isItSpecialVersionOfBiserokForMaxim)
					{
						text = nameScheme;
						text2 = articleScheme;
					}
					else if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						float num9 = ((float)Param.WidthInBead + 0f) / (CountBeadesInSm_X + 0f);
						float num10 = ((float)Param.HeightInBead + 0f) / (CountBeadesInSm_Y + 0f);
						num9 = (float)(int)Math.Round((double)(num9 * 10f)) / 10f;
						num10 = (float)(int)Math.Round((double)(num10 * 10f)) / 10f;
						text = "Наименование: " + nameScheme;
						text2 = "Артикул: " + articleScheme;
						text3 = "Размер: " + num9.ToString() + " x " + num10.ToString();
						num11 = resBeads.Count();
						text4 = "Количество цветов: " + num11.ToString();
					}
					float num12 = num3;
					Font font2 = new Font("Verdana", num12);
					SizeF sizeF = graphics.MeasureString(text, font2);
					SizeF sizeF2 = graphics.MeasureString(text4, font2);
					int num13 = (schemeForPrinting.Width - widthOnlyScheme - num7) / 2;
					float num14 = Math.Max(sizeF.Width, sizeF2.Width);
					int num15 = num4;
					if ((float)(num15 + num13) < num14)
					{
						float emSize = num12 * (((float)(num15 + num13) + 0f) / num14);
						font2 = new Font("Verdana", emSize);
						sizeF = graphics.MeasureString(text, font2);
					}
					SizeF sizeF3 = graphics.MeasureString(text2, font2);
					SizeF sizeF4 = graphics.MeasureString(text3, font2);
					sizeF2 = graphics.MeasureString(text4, font2);
					int num16 = (int)(heightOfStringOfNameAndArticleForMaximInBead * SqureCellSideSchemeForPrinting_X);
					if (Settings.isItSpecialVersionOfBiserokForMaxim)
					{
						y = (int)(((float)num16 - sizeF.Height - sizeF3.Height) / 4f);
						x = widthOnlyScheme + (schemeForPrinting.Width - widthOnlyScheme - (int)sizeF.Width) / 2;
						graphics.DrawString(text, font2, Brushes.Black, (float)x, (float)(y + num5 + num8));
						x = widthOnlyScheme + (schemeForPrinting.Width - widthOnlyScheme - (int)sizeF3.Width) / 2;
						graphics.DrawString(text2, font2, Brushes.Black, (float)x, (float)(y * 2) + sizeF.Height + (float)num5 + (float)num8);
					}
					else if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						y = (int)(((float)num16 - sizeF.Height - sizeF3.Height - sizeF4.Height - sizeF2.Height) / 8f);
						num14 = Math.Max(sizeF.Width, sizeF2.Width);
						x = widthOnlyScheme + (schemeForPrinting.Width - widthOnlyScheme - (int)num14) / 2;
						graphics.DrawString(text, font2, Brushes.Black, (float)x, (float)(y + num5 + num8));
						graphics.DrawString(text2, font2, Brushes.Black, (float)x, (float)(y * 3) + sizeF.Height + (float)num5 + (float)num8);
						graphics.DrawString(text3, font2, Brushes.Black, (float)x, (float)(y * 5) + sizeF.Height + sizeF3.Height + (float)num5 + (float)num8);
						graphics.DrawString(text4, font2, Brushes.Black, (float)x, (float)(y * 7) + sizeF.Height + sizeF3.Height + sizeF4.Height + (float)num5 + (float)num8);
					}
				}
				else
				{
					if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						ptBeginMiniLegend.Y += 2 * sizeOfBadge * 2;
					}
					int x2 = 0;
					int num17 = 0;
					int num18 = 0;
					int y2;
					if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						num17 = (int)((widthOfLogoForMaximInBead - 4f) * SqureCellSideSchemeForPrinting_X);
						num18 = (int)(heightOfLogoForMaximInBead * SqureCellSideSchemeForPrinting_X);
						y2 = heightOnlyScheme + (schemeForPrinting.Height - heightOnlyScheme - num18) / 2;
						Bitmap bitmap2 = (!Settings.isItSpecialVersionOfBiserokForMaxim) ? new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/logoArtHobby.png") : new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/LogoMelnica.png");
						graphics.DrawImage(bitmap2, new Rectangle(x2, y2, num17, num18), new Rectangle(0, 0, bitmap2.Width, bitmap2.Height), GraphicsUnit.Pixel);
						bitmap2.Dispose();
					}
					int num19 = (int)((widthOfSmallImageForMaximInBead - 4f) * SqureCellSideSchemeForPrinting_X);
					int num20 = (int)(heightOfSmallImageForMaximInBead * SqureCellSideSchemeForPrinting_X);
					x2 = (int)(widthOfLogoForMaximInBead * SqureCellSideSchemeForPrinting_X);
					if (Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
					{
						x2 = (ptBeginMiniLegend.X - num19) / 2;
					}
					int num21 = 0;
					if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						num21 = (int)(15f * ((float)Param.PrintingResolution / 300f));
					}
					else if (Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
					{
						num21 = (schemeForPrinting.Height - ptBeginMiniLegend.Y - num20) / 2;
					}
					y2 = heightOnlyScheme + num21;
					if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina || Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
					{
						graphics.DrawImage(bmpBeadworkView, new Rectangle(x2, y2, num19, num20), new Rectangle(0, 0, bmpBeadworkView.Width, bmpBeadworkView.Height), GraphicsUnit.Pixel);
					}
					else
					{
						graphics.DrawImage(BmpSourceImage, new Rectangle(x2, y2, num19, num20), new Rectangle(0, 0, BmpSourceImage.Width, BmpSourceImage.Height), GraphicsUnit.Pixel);
					}
					string text5 = "";
					string text6 = "";
					string text7 = "";
					string text8 = "";
					if (Settings.isItSpecialVersionOfBiserokForMaxim)
					{
						text5 = nameScheme;
						text6 = articleScheme;
					}
					else if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						float num22 = ((float)Param.WidthInBead + 0f) / (CountBeadesInSm_X + 0f);
						float num23 = ((float)Param.HeightInBead + 0f) / (CountBeadesInSm_Y + 0f);
						num22 = (float)(int)Math.Round((double)(num22 * 10f)) / 10f;
						num23 = (float)(int)Math.Round((double)(num23 * 10f)) / 10f;
						text5 = "Наименование: " + nameScheme;
						text6 = "Артикул: " + articleScheme;
						text7 = "Размер: " + num22.ToString() + " x " + num23.ToString();
						num11 = resBeads.Count();
						text8 = "Количество цветов: " + num11.ToString();
					}
					if (Settings.isItSpecialVersionOfBiserokForMaxim)
					{
						SizeF sizeF5 = graphics.MeasureString(text5, font);
						SizeF sizeF6 = graphics.MeasureString(text6, font);
						SizeF sizeF7 = graphics.MeasureString(text7, font);
						SizeF sizeF8 = graphics.MeasureString(text8, font);
						int num24 = (int)(heightOfStringOfNameAndArticleForMaximInBead * SqureCellSideSchemeForPrinting_X);
						int num25 = (int)(((float)num24 - sizeF5.Height - sizeF6.Height) / 4f);
						x2 = (int)(((float)(ptBeginMiniLegend.X - num17) - sizeF5.Width) / 2f + (float)num17);
						graphics.DrawString(nameScheme, font, Brushes.Black, (float)x2, (float)(y2 + num25 + num20));
						x2 = (int)(((float)(ptBeginMiniLegend.X - num17) - sizeF6.Width) / 2f + (float)num17);
						graphics.DrawString(articleScheme, font, Brushes.Black, (float)x2, (float)(y2 + num25 * 2) + sizeF5.Height + (float)num20);
					}
					else if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						Font font3 = new Font("Verdana", num3);
						SizeF sizeF9 = graphics.MeasureString(text5, font3);
						SizeF sizeF10 = graphics.MeasureString(text8, font3);
						float num26 = Math.Max(sizeF9.Width, sizeF10.Width);
						if ((float)(ptBeginMiniLegend.X - num17) < num26)
						{
							float emSize2 = num3 * (((float)(ptBeginMiniLegend.X - num17) + 0f) / num26);
							font3 = new Font("Verdana", emSize2);
							sizeF9 = graphics.MeasureString(text5, font3);
						}
						SizeF sizeF11 = graphics.MeasureString(text6, font3);
						SizeF sizeF12 = graphics.MeasureString(text7, font3);
						sizeF10 = graphics.MeasureString(text8, font3);
						int num27 = schemeForPrinting.Height - heightOnlyScheme - num20 - 2 * num21;
						int num28 = (int)(((float)num27 - sizeF9.Height - sizeF11.Height - sizeF12.Height - sizeF10.Height) / 8f);
						x2 = (int)(((float)(ptBeginMiniLegend.X - num17) - sizeF9.Width) / 2f + (float)num17);
						graphics.DrawString(text5, font3, Brushes.Black, (float)x2, (float)(y2 + num28 + num20));
						graphics.DrawString(text6, font3, Brushes.Black, (float)x2, (float)(y2 + num28 * 3) + sizeF9.Height + (float)num20);
						graphics.DrawString(text7, font3, Brushes.Black, (float)x2, (float)(y2 + num28 * 5) + sizeF9.Height + sizeF11.Height + (float)num20);
						graphics.DrawString(text8, font3, Brushes.Black, (float)x2, (float)(y2 + num28 * 7) + sizeF9.Height + sizeF11.Height + sizeF12.Height + (float)num20);
					}
				}
			}
			graphics.DrawImage(BmpSourceImage, 5, 5, widthOnlyScheme - 9, heightOnlyScheme - 9);
			graphics.Dispose();
			bool isSchemeBlockType = Param.Type == SchemeType.Block;
			float _widthOfBorderOfBeadsEllipse = Param.WidthOfBorderOfBeadsInPercentToBeadsRadius * (SqureCellSideSchemeForPrinting_Y + 0f) / 200f;
			bool isNeedToDrawBadges = Settings.hobbyProgramm == HobbyProgramm.Almaz || Param.IsDrawingBadgesOnSchemeForPrinting;
			float _d = (float)Param.PrintingResolution / 100f;
			int widthOfPen = (int)Math.Round((double)((float)Param.PrintingResolution / 200f));
			int num29 = (int)Math.Round((double)((float)Param.PrintingResolution / 400f));
			ConcurrentBag<RectangleF> rctForWhiteBorderAroundOfSarkSignsInAlmazka = new ConcurrentBag<RectangleF>();
			Parallel.For(0, Param.HeightInBead, new ParallelOptions
			{
				MaxDegreeOfParallelism = Environment.ProcessorCount
			}, delegate(int i)
			{
				int num54 = Param.WidthInBead;
				if (isSchemeBlockType && i % 2 != 0)
				{
					num54--;
				}
				try
				{
					Graphics graphics5 = default(Graphics);
					lock (schemeForPrinting)
					{
						graphics5 = Graphics.FromImage(schemeForPrinting);
					}
					for (int n = 0; n < num54; n++)
					{
						Block block2 = blocks[n, i];
						if (block2.IsVisible && !block2.ResBeads.IsCanvas)
						{
							float num55 = 0f;
							if (isSchemeBlockType && i % 2 != 0)
							{
								num55 = SqureCellSideSchemeForPrinting_Y / 2f;
							}
							if (v != 0 || (v == 0 && (i / 10 + n / 10) % 2 == 0))
							{
								Color white = Color.White;
								SolidBrush brush5 = block2.ResBeads.brush;
								SolidBrush brush6 = new SolidBrush(block2.ResBeads.clrBorder);
								if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
								{
									Color color = Color.White;
									if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
									{
										color = brush5.Color;
									}
									graphics5.FillRectangle(new SolidBrush(color), new RectangleF(5f + (float)n * SqureCellSideSchemeForPrinting_X - 1f + num55, 5f + SqureCellSideSchemeForPrinting_Y * (float)i - 1f, SqureCellSideSchemeForPrinting_X + 2f, SqureCellSideSchemeForPrinting_Y + 2f));
									graphics5.FillEllipse(brush6, new RectangleF(5f + (float)n * SqureCellSideSchemeForPrinting_X + num55, 5f + SqureCellSideSchemeForPrinting_Y * (float)i, SqureCellSideSchemeForPrinting_X - 1f, SqureCellSideSchemeForPrinting_Y - 1f));
									graphics5.FillEllipse(brush5, new RectangleF(5f + (float)n * SqureCellSideSchemeForPrinting_X + num55 + _widthOfBorderOfBeadsEllipse, 5f + SqureCellSideSchemeForPrinting_Y * (float)i + _widthOfBorderOfBeadsEllipse, SqureCellSideSchemeForPrinting_X - 1f - _widthOfBorderOfBeadsEllipse * 2f, SqureCellSideSchemeForPrinting_Y - 1f - _widthOfBorderOfBeadsEllipse * 2f));
								}
								else if (!Param.isSquareStraz)
								{
									Color color2 = brush5.Color;
									graphics5.FillRectangle(new SolidBrush(color2), new RectangleF(4f + (float)n * SqureCellSideSchemeForPrinting_X + num55, 4f + SqureCellSideSchemeForPrinting_Y * (float)i, SqureCellSideSchemeForPrinting_X + 2f, SqureCellSideSchemeForPrinting_Y + 2f));
									graphics5.FillEllipse(brush6, new RectangleF(5f + (float)n * SqureCellSideSchemeForPrinting_X + num55, 5f + SqureCellSideSchemeForPrinting_Y * (float)i, SqureCellSideSchemeForPrinting_X - 1f, SqureCellSideSchemeForPrinting_Y - 1f));
									graphics5.FillEllipse(brush5, new RectangleF(5f + (float)n * SqureCellSideSchemeForPrinting_X + num55 + _widthOfBorderOfBeadsEllipse, 5f + SqureCellSideSchemeForPrinting_Y * (float)i + _widthOfBorderOfBeadsEllipse, SqureCellSideSchemeForPrinting_X - 1f - _widthOfBorderOfBeadsEllipse * 2f, SqureCellSideSchemeForPrinting_Y - 1f - _widthOfBorderOfBeadsEllipse * 2f));
								}
								else
								{
									graphics5.FillRectangle(brush5, new RectangleF(4f + (float)n * SqureCellSideSchemeForPrinting_X, 4f + SqureCellSideSchemeForPrinting_Y * (float)i, SqureCellSideSchemeForPrinting_X, SqureCellSideSchemeForPrinting_Y));
									if (Param.IsSeparatingLinesOnScheme)
									{
										Color clr = block2.ResBeads.clr;
										double num56 = (double)(int)clr.R * 0.299;
										clr = block2.ResBeads.clr;
										double num57 = num56 + (double)(int)clr.G * 0.587;
										clr = block2.ResBeads.clr;
										if (num57 + (double)(int)clr.B * 0.114 <= 50.0)
										{
											rctForWhiteBorderAroundOfSarkSignsInAlmazka.Add(new RectangleF(4f + (float)n * SqureCellSideSchemeForPrinting_X, 4f + SqureCellSideSchemeForPrinting_Y * (float)i, SqureCellSideSchemeForPrinting_X, SqureCellSideSchemeForPrinting_Y));
										}
										Pen pen5 = new Pen(Brushes.Black, (float)widthOfPen);
										graphics5.DrawRectangle(pen5, 4f + (float)n * SqureCellSideSchemeForPrinting_X, 4f + SqureCellSideSchemeForPrinting_Y * (float)i, SqureCellSideSchemeForPrinting_X, SqureCellSideSchemeForPrinting_Y);
									}
								}
								if (isNeedToDrawBadges)
								{
									lock (block2.ResBeads.badge)
									{
										graphics5.DrawImage(block2.ResBeads.badge, new RectangleF(5f + (float)n * SqureCellSideSchemeForPrinting_X + num55 + _widthOfBorderOfBeadsEllipse + _d, 5f + SqureCellSideSchemeForPrinting_Y * (float)i + _widthOfBorderOfBeadsEllipse + _d, SqureCellSideSchemeForPrinting_X - _widthOfBorderOfBeadsEllipse * 2f - _d * 2f, SqureCellSideSchemeForPrinting_Y - _widthOfBorderOfBeadsEllipse * 2f - _d * 2f));
									}
								}
							}
							else
							{
								graphics5.FillRectangle(new SolidBrush(Color.White), new RectangleF(5f + (float)n * SqureCellSideSchemeForPrinting_X + num55 - 1f, 5f + SqureCellSideSchemeForPrinting_Y * (float)i - 1f, SqureCellSideSchemeForPrinting_X + 2f, SqureCellSideSchemeForPrinting_Y + 2f));
							}
						}
					}
					graphics5.Dispose();
				}
				catch (Exception exc2)
				{
					Site.SendExceptionInfo(exc2, false);
				}
			});
			if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
			{
				Graphics graphics2 = Graphics.FromImage(schemeForPrinting);
				Pen pen = new Pen(Color.FromArgb(255, 206, 206, 206), (float)widthOfPen);
				foreach (RectangleF item in rctForWhiteBorderAroundOfSarkSignsInAlmazka)
				{
					graphics2.DrawRectangle(pen, item.X, item.Y, item.Width, item.Height);
				}
				graphics2.Dispose();
			}
			if (Settings.hobbyProgramm == HobbyProgramm.Biserok && !Settings.isItSpecialVersionOfBiserokForMaxim)
			{
				float _widthOfPointBetweenBeads = Param.WidthOfPointBetweenBeadsInPercentToBeadsRadius * (SqureCellSideSchemeForPrinting_Y + 0f) / 100f;
				float d = _widthOfPointBetweenBeads / 2f;
				Parallel.For(0, Param.HeightInBead, new ParallelOptions
				{
					MaxDegreeOfParallelism = Environment.ProcessorCount
				}, delegate(int i)
				{
					int num52 = Param.WidthInBead;
					if (isSchemeBlockType && i % 2 != 0)
					{
						num52--;
					}
					try
					{
						SolidBrush brush4 = new SolidBrush(Color.Black);
						Graphics graphics4 = default(Graphics);
						lock (schemeForPrinting)
						{
							graphics4 = Graphics.FromImage(schemeForPrinting);
							graphics4.SmoothingMode = SmoothingMode.AntiAlias;
						}
						for (int m = 0; m < num52; m++)
						{
							Block block = blocks[m, i];
							if (block.IsVisible && !block.ResBeads.IsCanvas)
							{
								float num53 = 0f;
								if (isSchemeBlockType && i % 2 != 0)
								{
									num53 = SqureCellSideSchemeForPrinting_Y / 2f;
								}
								if (v != 0 || (v == 0 && (i / 10 + m / 10) % 2 == 0))
								{
									SolidBrush solidBrush = new SolidBrush(block.ResBeads.clrBorder);
									if (isSchemeBlockType)
									{
										graphics4.FillEllipse(brush4, new RectangleF(5f + ((float)m + 0.25f) * SqureCellSideSchemeForPrinting_X + num53 - d, 5f + ((float)i + 1f) * SqureCellSideSchemeForPrinting_Y - d, _widthOfPointBetweenBeads, _widthOfPointBetweenBeads));
										graphics4.FillEllipse(brush4, new RectangleF(5f + ((float)m + 0.75f) * SqureCellSideSchemeForPrinting_X + num53 - d, 5f + (float)i * SqureCellSideSchemeForPrinting_Y - d, _widthOfPointBetweenBeads, _widthOfPointBetweenBeads));
									}
									else
									{
										graphics4.FillEllipse(brush4, new RectangleF(5f + (float)m * SqureCellSideSchemeForPrinting_X - d, 5f + (float)i * SqureCellSideSchemeForPrinting_Y - d, _widthOfPointBetweenBeads, _widthOfPointBetweenBeads));
										graphics4.FillEllipse(brush4, new RectangleF(5f + ((float)m + 1f) * SqureCellSideSchemeForPrinting_X - d, 5f + ((float)i + 1f) * SqureCellSideSchemeForPrinting_Y - d, _widthOfPointBetweenBeads, _widthOfPointBetweenBeads));
										graphics4.FillEllipse(brush4, new RectangleF(5f + (float)m * SqureCellSideSchemeForPrinting_X - d, 5f + ((float)i + 1f) * SqureCellSideSchemeForPrinting_Y - d, _widthOfPointBetweenBeads, _widthOfPointBetweenBeads));
										graphics4.FillEllipse(brush4, new RectangleF(5f + ((float)m + 1f) * SqureCellSideSchemeForPrinting_X - d, 5f + (float)i * SqureCellSideSchemeForPrinting_Y - d, _widthOfPointBetweenBeads, _widthOfPointBetweenBeads));
									}
								}
							}
						}
						graphics4.Dispose();
					}
					catch (Exception exc)
					{
						Site.SendExceptionInfo(exc, false);
					}
				});
			}
			graphics = Graphics.FromImage(schemeForPrinting);
			if (Param.BiserType == BiserType.ForManufacturers && Param.IsDrawSmallLegend)
			{
				BeadsManufacturer beadsManufacturer = new BeadsManufacturer();
				BeadsSeries beadsSeries = new BeadsSeries();
				int num30 = 0;
				int num31 = 0;
				SolidBrush brush = new SolidBrush(Color.Black);
				int num32 = (int)Math.Round((double)(2f * SqureCellSideSchemeForPrinting_Y));
				int num33 = (int)Math.Round((double)((float)widthOfColumnInBadgesInMiniLegend * SqureCellSideSchemeForPrinting_X));
				int num34 = (int)Math.Round((double)(SqureCellSideSchemeForPrinting_Y * 0.9f));
				int num35 = (int)Math.Round((double)(SqureCellSideSchemeForPrinting_X * 0.9f));
				float num36 = _widthOfBorderOfBeadsEllipse * 1.5f;
				Font font4 = new Font("Verdana", num3, FontStyle.Italic);
				Font font5 = new Font("Verdana", num3, FontStyle.Bold);
				float num37 = 0.108333334f * SqureCellSideSchemeForPrinting_X;
				if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
				{
					resBeads.Sort(new ComparerBeadsByNameAsInt());
					if (widthOnlyScheme > heightOnlyScheme)
					{
						ptBeginMiniLegend.Y -= num32;
					}
				}
				int num38 = 0;
				for (int j = 0; j < countColumnsInMiniLegend; j++)
				{
					int num39 = (j + 1) * countRowsInMiniLegend;
					if (j >= countRecordsInMiniLegend / countRowsInMiniLegend)
					{
						num39 = countRecordsInMiniLegend;
					}
					if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina && num38 < resBeads.Count())
					{
						Pen pen2 = new Pen(Brushes.Black, (float)(int)Math.Round((double)((float)Param.PrintingResolution / 200f)));
						Font font6 = new Font("Verdana", num3 * 0.65f);
						float num40 = (float)(ptBeginMiniLegend.X + j * num33) + (float)num2 * 0.1f;
						float num41 = (float)(-num32 + ptBeginMiniLegend.Y) - (float)num * 0.1695f;
						float num42 = (float)num * 2.25f;
						float height = (float)num * 1.331522f;
						graphics.DrawRectangle(pen2, num40, num41, num42, height);
						graphics.DrawString("№", font6, Brushes.Black, new PointF(num40 + (float)num2 * 0.6405f, num41 + (float)num * 0.182f));
						num40 += num42;
						num42 = (float)num * 2.33f;
						graphics.DrawRectangle(pen2, num40, num41, num42, height);
						graphics.DrawString("Знак", font6, Brushes.Black, new PointF(num40 + (float)num2 * 0.236f, num41 + (float)num * 0.182f));
						num40 += num42;
						num42 = (float)num * 3.64f;
						graphics.DrawRectangle(pen2, num40, num41, num42, height);
						graphics.DrawString("Номер", font6, Brushes.Black, new PointF(num40 + (float)num2 * 0.59f, num41 + (float)num * 0.182f));
						num40 += num42;
						num42 = (float)num * 4.3f;
						graphics.DrawRectangle(pen2, num40, num41, num42, height);
						graphics.DrawString("Количество", font6, Brushes.Black, new PointF(num40, num41 + (float)num * 0.182f));
						num40 += num42;
						num42 = (float)num * 2.51f;
						graphics.DrawRectangle(pen2, num40, num41, num42, height);
						graphics.DrawString("Вес", font6, Brushes.Black, new PointF(num40 + (float)num2 * 0.517f, num41 + (float)num * 0.182f));
					}
					for (int k = j * countRowsInMiniLegend; k < num39; k++)
					{
						int num43 = k - num30;
						if (beadsManufacturer != resBeads[num43].parentSeries.parentManufacturer && !Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
						{
							num30++;
							beadsManufacturer = resBeads[num43].parentSeries.parentManufacturer;
							string s;
							if (v != 0)
							{
								s = beadsManufacturer.name;
								if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
								{
									s = beadsManufacturer.beadsSeries.First().name;
								}
							}
							else
							{
								s = ((Settings.hobbyProgramm != HobbyProgramm.Biserok) ? "Стразы *** (демо-версия)" : "Бисер *** (демо-версия)");
							}
							graphics.DrawString(s, font5, brush, new PointF((float)(ptBeginMiniLegend.X + j * num33), (float)((k - j * countRowsInMiniLegend) * num32 + ptBeginMiniLegend.Y) - 0.3f * (float)num));
						}
						else
						{
							SolidBrush brush2 = new SolidBrush(resBeads[num43].clr);
							SolidBrush brush3 = new SolidBrush(resBeads[num43].clrBorder);
							if (Settings.hobbyProgramm == HobbyProgramm.Biserok || (Settings.hobbyProgramm == HobbyProgramm.Almaz && !Param.isSquareStraz))
							{
								int num44 = 3;
								if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
								{
									num44 = 1;
								}
								for (int l = 0; l < num44; l++)
								{
									graphics.FillEllipse(brush3, new RectangleF((float)(ptBeginMiniLegend.X + j * num33) + (float)(l * num2) * 1.1f, (float)((k - j * countRowsInMiniLegend) * num32 + ptBeginMiniLegend.Y), (float)num, (float)num));
									graphics.FillEllipse(brush2, new RectangleF((float)(ptBeginMiniLegend.X + j * num33) + num36 + (float)(l * num2) * 1.1f, (float)((k - j * countRowsInMiniLegend) * num32 + ptBeginMiniLegend.Y) + num36, (float)num - num36 * 2f, (float)num - num36 * 2f));
									if (isNeedToDrawBadges)
									{
										graphics.DrawImage(resBeads[num43].badge, new RectangleF((float)(ptBeginMiniLegend.X + j * num33) + num36 + num37 + (float)(l * num2) * 1.1f, (float)((k - j * countRowsInMiniLegend) * num32 + ptBeginMiniLegend.Y) + num36 + num37, (float)num - 2f * num36 - 2f * num37, (float)num - 2f * num36 - 2f * num37));
									}
								}
							}
							else
							{
								float num45 = 0f;
								if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
								{
									Graphics graphics3 = graphics;
									num11 = num38 + 1;
									num38 = num11;
									graphics3.DrawString(num11.ToString(), font, brush, new PointF((float)(ptBeginMiniLegend.X + j * num33) + (float)num2 * 0.3f, (float)((k - j * countRowsInMiniLegend) * num32 + ptBeginMiniLegend.Y) - (float)num * 0.15f));
									num45 = 1.9f;
								}
								graphics.FillRectangle(brush2, new RectangleF((float)(ptBeginMiniLegend.X + j * num33) + (float)num2 * (1.1f + num45), (float)((k - j * countRowsInMiniLegend) * num32 + ptBeginMiniLegend.Y), (float)num, (float)num));
								if (Param.IsSeparatingLinesOnScheme)
								{
									Pen pen3 = new Pen(Brushes.Black, (float)(int)Math.Round((double)((float)Param.PrintingResolution / 200f)));
									graphics.DrawRectangle(pen3, (float)(ptBeginMiniLegend.X + j * num33) + (float)num2 * (1.1f + num45), (float)((k - j * countRowsInMiniLegend) * num32 + ptBeginMiniLegend.Y), (float)num, (float)num);
								}
								graphics.DrawImage(resBeads[num43].badge, new RectangleF((float)(ptBeginMiniLegend.X + j * num33) + num36 + num37 + (float)num2 * (1.1f + num45), (float)((k - j * countRowsInMiniLegend) * num32 + ptBeginMiniLegend.Y) + num36 + num37, (float)num - 2f * num36 - 2f * num37, (float)num - 2f * num36 - 2f * num37));
							}
							string text9 = resBeads[num43].name;
							if (v == 0)
							{
								text9 = ((num43 >= 14) ? "*****" : ("****" + text9.Last().ToString()));
							}
							float num46 = 3.8f;
							if (Settings.isItSpecialVersionOfBiserokForMaxim)
							{
								num46 = 1.25f;
							}
							if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
							{
								num46 = 4.9f;
							}
							graphics.DrawString(text9, font, brush, new PointF((float)(ptBeginMiniLegend.X + j * num33) + (float)num2 * num46, (float)((k - j * countRowsInMiniLegend) * num32 + ptBeginMiniLegend.Y) - (float)num * 0.15f));
							try
							{
								num31 = countOfEachBeads[resBeads[num43]];
							}
							catch
							{
								num31 = 0;
							}
							string text10 = "";
							float num47 = ((float)Param.countOfBeadsIn1000Gramm + 0f) / 1000f;
							int num48 = (int)Math.Round((double)(((float)num31 + 0f) / num47)) + 1;
							if (num31 > 0)
							{
								text10 = ((!Settings.isItSpecialVersionOfBiserokForMaxim) ? ((!Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina) ? ((!Settings.isItSpecialVersionOfAlmazkaForElenaBartosova) ? ("- " + num31.ToString() + Local.Get("шт., ") + num48.ToString() + Local.Get("гр.")) : ("- " + num31.ToString() + "ks., " + num48.ToString() + "g.")) : num31.ToString()) : (num31.ToString() + Local.Get("шт.")));
								if (v == 0 && num43 > 13)
								{
									text10 = Local.Get("- демо-версия");
								}
							}
							else
							{
								text10 = GetStringWithInfoAboutNotNeededInThisBeads(true);
							}
							num46 += 3.7f;
							if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
							{
								num46 += 0.5f;
							}
							if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
							{
								font4 = new Font("Verdana", num3);
							}
							graphics.DrawString(text10, font4, Brushes.Black, new PointF((float)(ptBeginMiniLegend.X + j * num33) + (float)num2 * num46, (float)((k - j * countRowsInMiniLegend) * num32 + ptBeginMiniLegend.Y) - (float)num * 0.15f));
							if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
							{
								text10 = num48.ToString();
								num46 += 3.9f;
								graphics.DrawString(text10, font4, Brushes.Black, new PointF((float)(ptBeginMiniLegend.X + j * num33) + (float)num2 * num46, (float)((k - j * countRowsInMiniLegend) * num32 + ptBeginMiniLegend.Y) - (float)num * 0.15f));
								Pen pen4 = new Pen(Brushes.Black, (float)(int)Math.Round((double)((float)Param.PrintingResolution / 200f)));
								float num49 = (float)(ptBeginMiniLegend.X + j * num33) + (float)num2 * 0.1f;
								float y3 = (float)((k - j * countRowsInMiniLegend) * num32 + ptBeginMiniLegend.Y) - (float)num * 0.1695f;
								float num50 = (float)num * 2.25f;
								float height2 = (float)num * 1.331522f;
								graphics.DrawRectangle(pen4, num49, y3, num50, height2);
								num49 += num50;
								num50 = (float)num * 2.33f;
								graphics.DrawRectangle(pen4, num49, y3, num50, height2);
								num49 += num50;
								num50 = (float)num * 3.64f;
								graphics.DrawRectangle(pen4, num49, y3, num50, height2);
								num49 += num50;
								num50 = (float)num * 4.3f;
								graphics.DrawRectangle(pen4, num49, y3, num50, height2);
								num49 += num50;
								num50 = (float)num * 2.51f;
								graphics.DrawRectangle(pen4, num49, y3, num50, height2);
							}
						}
					}
				}
			}
			if (v == 0)
			{
				string s2 = "";
				if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
				{
					s2 = "В Демо-версии половина бисеринок не отображаются -";
				}
				else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
				{
					s2 = "В Демо-версии половина страз не отображаются -";
				}
				int num51 = widthOnlyScheme / (Param.PrintingResolution / 96);
				float emSize3 = (float)(num51 / 45);
				graphics.DrawString(s2, new Font("Verdana", emSize3, FontStyle.Bold), new SolidBrush(Color.Red), new PointF(10f, (float)heightOnlyScheme / 2.5f));
				graphics.DrawString(" вместо них на этой схеме пустое белое место", new Font("Verdana", emSize3, FontStyle.Bold), new SolidBrush(Color.Red), new PointF(10f, (float)heightOnlyScheme / 1.67f));
			}
			graphics.Dispose();
		}

		private void GenerateLegendListsAndPrintOrSaveToFolder(List<Beads> resBeads, SavingOrPrintingSettings settings, Dictionary<Beads, int> countOfEachBeads, IEnumerable<Block> notNullBlocks, int contours_Count = 0, Bitmap bmpBeadworkView = null)
		{
			int num = 23;
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				num = ((Param.BiserType != BiserType.ForManufacturers || !Param.IsMixingColors) ? 26 : 24);
			}
			int num2 = 1;
			if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
			{
				num2 = 2;
				num = 24;
			}
			int num3 = num2 * num;
			int num4 = 0;
			int num5 = 0;
			int num6 = 0;
			int num7 = 0;
			List<Beads> list = new List<Beads>();
			List<Beads> list2 = new List<Beads>();
			if (Settings.hobbyProgramm == HobbyProgramm.Krestik && settings.numberOfTab == 3)
			{
				if (backstitchHandEditing.Count() > 0)
				{
					list = (from back in backstitchHandEditing
					where !back.ptEnd.IsEmpty
					select back.bead).Distinct().ToList();
					num5 += 1 + list.Count();
				}
				if (knotHandEditing.Count() > 0)
				{
					list2 = (from back in knotHandEditing
					select back.bead).Distinct().ToList();
					num5 += 1 + list2.Count();
				}
			}
			if (Settings.hobbyProgramm != HobbyProgramm.Raskraska && Param.BiserType == BiserType.ForManufacturers)
			{
				goto IL_018e;
			}
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && Param.BiserType == BiserType.ForManufacturers && !Param.IsMixingColors)
			{
				goto IL_018e;
			}
			num5 += resBeads.Count;
			num4 = num5 / num3;
			if (num4 * num3 < num5)
			{
				num4++;
			}
			num7 = resBeads.Count();
			goto IL_0285;
			IL_0285:
			int num8 = 0;
			string text = "";
			string text2 = "";
			BeadsSeries beadsSeries = new BeadsSeries();
			BeadsManufacturer beadsManufacturer = new BeadsManufacturer();
			float num9 = Param.WidthOfBorderOfBeadsInPercentToBeadsRadius * ((float)SqureCellSidePrintPages_Y + 0f) / 200f;
			List<Bitmap> list3 = new List<Bitmap>();
			string text3 = "";
			string s = "";
			if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina && Settings.hobbyProgramm == HobbyProgramm.Almaz)
			{
				resBeads.Sort(new ComparerBeadsByNameAsInt());
			}
			int num10 = 0;
			KeyValuePair<Beads, float> keyValuePair;
			for (int j = 0; j < num4; j++)
			{
				int num11 = (j + 1) * num3;
				if (j == num5 / num3)
				{
					num11 = num5;
				}
				Bitmap bitmap = new Bitmap(2480, 3507);
				Graphics graphics = Graphics.FromImage(bitmap);
				graphics.FillRectangle(Brushes.White, 0, 0, 2480, 3507);
				float emSize = 48f * KfSystemFontSizes;
				float emSize2 = 39f * KfSystemFontSizes;
				float emSize3 = 45f * KfSystemFontSizes;
				float emSize4 = 67f * KfSystemFontSizes;
				float num12 = 50f * KfSystemFontSizes;
				float emSize5 = 44f * KfSystemFontSizes;
				float emSize6 = 40f * KfSystemFontSizes;
				float emSize7 = 35f * KfSystemFontSizes;
				float emSize8 = 40f * KfSystemFontSizes;
				Font font = new Font("Tahoma", emSize, FontStyle.Bold);
				string text4 = "";
				if (num5 > num3)
				{
					text4 = ", лист №" + (j + 1);
				}
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					float widthColoringInSm = Param.WidthColoringInSm;
					float num13 = ((float)HeightSourceImageInPx + 0f) / ((float)WidthSourceImageInPx + 0f) * widthColoringInSm;
					num13 = (float)(int)Math.Round((double)(num13 * 10f)) / 10f;
					graphics.DrawString(string.Format(Local.Get("Карта цветов для картины ") + "{0}x{1}" + Local.Get("см из ") + "{2} {3}, {4} {5}", widthColoringInSm, num13, num7, AllString.GetCountColorString(num7), contours_Count.ToString(), AllString.GetCountContours(contours_Count)), font, Brushes.Black, new PointF(130f, 110f));
					if (Param.BiserType == BiserType.ForManufacturers && Param.IsMixingColors)
					{
						if (Settings.v <= 1)
						{
							graphics.DrawString("Примерные формулы (пропорции) смешения цветов Вашей палитры", new Font("Tahoma", emSize2, FontStyle.Bold), Brushes.Black, new PointF(255f, 212f));
							graphics.DrawString("для получения нужных оттенков:", new Font("Tahoma", emSize2, FontStyle.Bold), Brushes.Black, new PointF(255f, 275f));
						}
						else
						{
							graphics.DrawString("Примерные формулы (", new Font("Tahoma", emSize2), Brushes.Black, new PointF(210f, 212f));
							graphics.DrawString("пропорции", new Font("Tahoma", emSize2, FontStyle.Bold), Brushes.Black, new PointF(756f, 212f));
							graphics.DrawString("и", new Font("Tahoma", emSize2), Brushes.Black, new PointF(1071f, 212f));
							graphics.DrawString("очень примерный объём", new Font("Tahoma", emSize2), Brushes.Gray, new PointF(1112f, 212f));
							graphics.DrawString(") смешения красок", new Font("Tahoma", emSize2), Brushes.Black, new PointF(1698f, 212f));
							graphics.DrawString("для получения нужных оттенков:", new Font("Tahoma", emSize2), Brushes.Black, new PointF(210f, 275f));
						}
					}
				}
				else if (!Settings.isItSpecialVersionOfBiserokForMaxim && !Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
				{
					float num14 = ((float)Param.WidthInBead + 0f) / (CountBeadesInSm_X + 0f);
					float num15 = ((float)Param.HeightInBead + 0f) / (CountBeadesInSm_Y + 0f);
					num14 = (float)(int)Math.Round((double)(num14 * 10f)) / 10f;
					num15 = (float)(int)Math.Round((double)(num15 * 10f)) / 10f;
					graphics.DrawString(string.Format(Local.Get("Легенда для схемы размером") + " {0}x{1}" + Local.Get("см*"), num14, num15), font, Brushes.Black, new PointF(168f, 110f));
					if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
					{
						text3 = "петель";
					}
					else if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
					{
						text3 = "стежков";
					}
					else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
					{
						text3 = "стежков";
					}
					else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
					{
						text3 = "страз";
					}
					graphics.DrawString(string.Format("({0}х{1} " + text3 + Local.Get(") из ") + "{2} {3}{4}:", Param.WidthInBead, Param.HeightInBead, num7, AllString.GetCountColorString(num7), text4), font, Brushes.Black, new PointF(168f, 212f));
					font = new Font("Tahoma", emSize3, FontStyle.Bold);
					if (Param.BiserType == BiserType.ForManufacturers)
					{
						graphics.DrawString("Значок легенды и номер", font, Brushes.Black, new PointF(90f, 377f));
					}
					else if (Param.BiserType == BiserType.Any)
					{
						graphics.DrawString("Значок легенды и", font, Brushes.Black, new PointF(190f, 377f));
					}
					if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
					{
						if (Param.BiserType == BiserType.ForManufacturers)
						{
							graphics.DrawString("бисера по каталогу", font, Brushes.Black, new PointF(190f, 437f));
						}
						else if (Param.BiserType == BiserType.Any)
						{
							graphics.DrawString("цвет бисера", font, Brushes.Black, new PointF(245f, 437f));
						}
					}
					else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
					{
						if (Param.BiserType == BiserType.ForManufacturers)
						{
							graphics.DrawString("мулине по каталогу", font, Brushes.Black, new PointF(190f, 437f));
						}
						else if (Param.BiserType == BiserType.Any)
						{
							graphics.DrawString("цвет мулине", font, Brushes.Black, new PointF(245f, 437f));
						}
					}
					else if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
					{
						if (Param.BiserType == BiserType.ForManufacturers)
						{
							graphics.DrawString("пряжи по каталогу", font, Brushes.Black, new PointF(190f, 437f));
						}
						else if (Param.BiserType == BiserType.Any)
						{
							graphics.DrawString("цвет пряжи", font, Brushes.Black, new PointF(245f, 437f));
						}
					}
					else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
					{
						if (Param.BiserType == BiserType.ForManufacturers)
						{
							graphics.DrawString("страз по каталогу", font, Brushes.Black, new PointF(190f, 437f));
						}
						else if (Param.BiserType == BiserType.Any)
						{
							graphics.DrawString("цвет страз", font, Brushes.Black, new PointF(245f, 437f));
						}
					}
					if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
					{
						text3 = "бисеринок";
						s = "Вес бисера в граммах*";
					}
					else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
					{
						text3 = "крестиков";
						s = "Длина мулине в метрах*";
					}
					else if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
					{
						text3 = "петель";
						s = "Длина пряжи в метрах*";
					}
					else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
					{
						text3 = "страз";
						s = "Вес страз в граммах*";
					}
					graphics.DrawString("Число " + text3, font, Brushes.Black, new PointF(965f, 407f));
					graphics.DrawString(s, font, Brushes.Black, new PointF(1595f, 407f));
				}
				int num16 = 565;
				if (Settings.isItSpecialVersionOfBiserokForMaxim)
				{
					num16 = 651;
					Pen pen = new Pen(Brushes.Black, 5f);
					font = new Font("Tahoma", emSize4, FontStyle.Bold);
					graphics.DrawRectangle(pen, new Rectangle(399, 47, 1605, 181));
					SizeF sizeF = graphics.MeasureString(nameScheme, font);
					graphics.DrawString(nameScheme, font, Brushes.Black, new Point(399 + (int)(1605f - sizeF.Width) / 2, 47 + (int)(181f - sizeF.Height) / 2));
					graphics.DrawRectangle(pen, new Rectangle(839, 279, 741, 165));
					sizeF = graphics.MeasureString(articleScheme, font);
					graphics.DrawString(articleScheme, font, Brushes.Black, new Point(839 + (int)(741f - sizeF.Width) / 2, 279 + (int)(165f - sizeF.Height) / 2));
				}
				if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
				{
					num16 = 680;
				}
				int num26;
				if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina && Settings.hobbyProgramm == HobbyProgramm.Almaz)
				{
					Bitmap bitmap2 = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/logoArtHobby.png");
					float num17 = 707f;
					float num18 = 500f;
					float num19 = 93f;
					float num20 = 35f;
					float num21 = Math.Max(((float)bitmap2.Width + 0f) / num17, ((float)bitmap2.Height + 0f) / num18);
					int num22 = (int)(num17 - (float)bitmap2.Width / num21) / 2;
					int num23 = (int)(num18 - (float)bitmap2.Height / num21) / 2;
					graphics.DrawImage(bitmap2, new RectangleF(num19 + (float)num22, num20 + (float)num23, num17 - (float)(2 * num22), num18 - (float)(2 * num23)), new Rectangle(0, 0, bitmap2.Width, bitmap2.Height), GraphicsUnit.Pixel);
					bitmap2.Dispose();
					bitmap2 = new Bitmap(bmpBeadworkView);
					num19 = 816f;
					num17 = 635f;
					num21 = Math.Max(((float)bitmap2.Width + 0f) / num17, ((float)bitmap2.Height + 0f) / num18);
					num22 = (int)(num17 - (float)bitmap2.Width / num21) / 2;
					num23 = (int)(num18 - (float)bitmap2.Height / num21) / 2;
					graphics.DrawImage(bitmap2, new RectangleF(num19 + (float)num22, num20 + (float)num23, num17 - (float)(2 * num22), num18 - (float)(2 * num23)), new Rectangle(0, 0, bitmap2.Width, bitmap2.Height), GraphicsUnit.Pixel);
					bitmap2.Dispose();
					num19 = 1500f;
					num20 += 20f;
					num17 = 930f;
					num18 -= 40f;
					font = new Font("Tahoma", num12);
					float num24 = ((float)Param.WidthInBead + 0f) / (CountBeadesInSm_X + 0f);
					float num25 = ((float)Param.HeightInBead + 0f) / (CountBeadesInSm_Y + 0f);
					num24 = (float)(int)Math.Round((double)(num24 * 10f)) / 10f;
					num25 = (float)(int)Math.Round((double)(num25 * 10f)) / 10f;
					string text5 = "Наименование: " + nameScheme;
					string text6 = "Артикул: " + articleScheme;
					string text7 = "Размер: " + num24.ToString() + " x " + num25.ToString();
					num26 = resBeads.Count();
					string text8 = "Количество цветов: " + num26.ToString();
					SizeF sizeF2 = graphics.MeasureString(text5, font);
					SizeF sizeF3 = graphics.MeasureString(text8, font);
					float num27 = Math.Max(sizeF2.Width, sizeF3.Width);
					if (num17 < num27)
					{
						float emSize9 = num12 * (num17 / num27);
						font = new Font("Tahoma", emSize9);
						sizeF2 = graphics.MeasureString(text5, font);
						sizeF3 = graphics.MeasureString(text8, font);
					}
					SizeF sizeF4 = graphics.MeasureString(text6, font);
					SizeF sizeF5 = graphics.MeasureString(text7, font);
					num23 = (int)((num18 - sizeF2.Height - sizeF4.Height - sizeF5.Height - sizeF3.Height) / 8f);
					graphics.DrawString(text5, font, Brushes.Black, num19, (float)num23 + num20);
					graphics.DrawString(text6, font, Brushes.Black, num19, (float)(num23 * 3) + sizeF2.Height + num20);
					graphics.DrawString(text7, font, Brushes.Black, num19, (float)(num23 * 5) + sizeF2.Height + sizeF4.Height + num20);
					graphics.DrawString(text8, font, Brushes.Black, num19, (float)(num23 * 7) + sizeF2.Height + sizeF4.Height + sizeF5.Height + num20);
				}
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					num16 = 275;
					if (Param.BiserType == BiserType.ForManufacturers && Param.IsMixingColors)
					{
						num16 = 400;
					}
				}
				if (v > 0 || j == 0)
				{
					int i = j * num3;
					while (i < num11)
					{
						int num28 = 266;
						if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && Param.BiserType == BiserType.ForManufacturers && Param.IsMixingColors)
						{
							num28 = 220;
						}
						if (Settings.isItSpecialVersionOfBiserokForMaxim)
						{
							num28 = 20;
						}
						if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
						{
							num28 = 122;
						}
						int num29 = (int)(((float)(i - j * num3) + 0f) / ((float)num + 0f));
						if (num29 > 0)
						{
							if (Settings.isItSpecialVersionOfBiserokForMaxim)
							{
								num28 = 1315;
							}
							if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
							{
								num28 = 1300;
							}
						}
						if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina && num10 < resBeads.Count())
						{
							Pen pen2 = new Pen(Brushes.Black, (float)(int)Math.Round((double)((float)Param.PrintingResolution / 200f)));
							Font font2 = new Font("Tahoma", num12 * 0.74f);
							float num30 = (float)(num28 - 30);
							float num31 = (float)(num16 - 100);
							float num32 = 158f;
							float height = 117f;
							graphics.DrawRectangle(pen2, num30, num31 - 30f, num32, height);
							graphics.DrawString("№", font2, Brushes.Black, new PointF((float)(num28 + 13), num31));
							num30 += num32;
							num32 = 164f;
							graphics.DrawRectangle(pen2, num30, num31 - 30f, num32, height);
							graphics.DrawString("Знак", font2, Brushes.Black, new PointF((float)(num28 + 152), num31));
							num30 += num32;
							num32 = 254f;
							graphics.DrawRectangle(pen2, num30, num31 - 30f, num32, height);
							graphics.DrawString("Номер", font2, Brushes.Black, new PointF((float)(num28 + 333), num31));
							num30 += num32;
							num32 = 300f;
							graphics.DrawRectangle(pen2, num30, num31 - 30f, num32, height);
							graphics.DrawString("Количество", font2, Brushes.Black, new PointF((float)(num28 + 558), num31));
							num30 += num32;
							num32 = 177f;
							graphics.DrawRectangle(pen2, num30, num31 - 30f, num32, height);
							graphics.DrawString("Вес", font2, Brushes.Black, new PointF((float)(num28 + 888), num31));
						}
						font = new Font("Tahoma", num12);
						Font font3 = new Font("Tahoma", emSize7);
						try
						{
							bool flag = true;
							if (Settings.hobbyProgramm != HobbyProgramm.Raskraska && Param.BiserType == BiserType.ForManufacturers)
							{
								goto IL_12fc;
							}
							if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && Param.BiserType == BiserType.ForManufacturers && !Param.IsMixingColors)
							{
								goto IL_12fc;
							}
							SizeF sizeF6;
							if (Param.BiserType == BiserType.Any || (Settings.hobbyProgramm == HobbyProgramm.Raskraska && Param.BiserType == BiserType.ForManufacturers && Param.IsMixingColors))
							{
								if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
								{
									SolidBrush solidBrush = new SolidBrush(resBeads[i].clr);
									if (Param.BiserType == BiserType.Any)
									{
										if (v == 0 && i > 5)
										{
											graphics.DrawString("Недоступно в тестовой версии", font, Brushes.Red, new PointF((float)(num28 + 209), (float)((i - j * num3 - num29 * num) * 117 + num16 - 15)));
										}
										else
										{
											graphics.FillRectangle(solidBrush, new Rectangle(num28 + 209, (i - j * num3 - num29 * num) * 117 + num16 - 15, 220, 90));
											if (Settings.v > 1)
											{
												graphics.DrawString(resBeads[i].squareInSm2.ToString() + Local.Get(" см"), font, Brushes.Black, new PointF(800f, (float)((i - j * num3) * 117 + num16 - 13)));
												sizeF6 = graphics.MeasureString(resBeads[i].squareInSm2.ToString() + Local.Get(" см"), font);
												float width = sizeF6.Width;
												graphics.DrawString("2", font3, Brushes.Black, new PointF(800f + width - 18f, (float)((i - j * num3) * 117 + num16 - 26)));
												graphics.DrawString("~ " + GetStringCountOfMlForPaints(resBeads[i].squareInSm2, 1) + Local.Get(" мл."), font, Brushes.Black, new PointF(800f + width + 32f, (float)((i - j * num3) * 117 + num16 - 13)));
											}
										}
										graphics.DrawString(resBeads[i].symbolForColoring_AndForVoiceWorkInBiserok, font, Brushes.Black, new PointF((float)(num28 + 39), (float)((i - j * num3 - num29 * num) * 117 + num16 - 13)));
									}
									else if (Param.BiserType == BiserType.ForManufacturers && Param.IsMixingColors)
									{
										Color color;
										float num41;
										if (v <= 1)
										{
											graphics.FillRectangle(solidBrush, new Rectangle(num28 + 54, (i - j * num3 - num29 * num) * 117 + num16 - 15, 220, 90));
											Brush brush = Brushes.Black;
											color = solidBrush.Color;
											float num33 = (float)(int)color.R * 0.299f;
											color = solidBrush.Color;
											float num34 = num33 + (float)(int)color.G * 0.587f;
											color = solidBrush.Color;
											if (num34 + (float)(int)color.B * 0.114f < 60f)
											{
												brush = Brushes.White;
											}
											graphics.DrawString(resBeads[i].symbolForColoring_AndForVoiceWorkInBiserok, font, brush, new PointF((float)(num28 + 124), (float)((i - j * num3 - num29 * num) * 117 + num16 - 13)));
											font = new Font("Tahoma", num12);
											if (resBeads[i].mixingColors != null)
											{
												graphics.DrawString("=", font, Brushes.Black, new PointF((float)(num28 + 300), (float)((i - j * num3 - num29 * num) * 117 + num16 - 13)));
												for (int k = 0; k < resBeads[i].mixingColors.Count(); k++)
												{
													keyValuePair = resBeads[i].mixingColors.ElementAt(k);
													solidBrush = new SolidBrush(keyValuePair.Key.clr);
													int num35 = 320;
													int num36 = num28 + (350 + num35) * k;
													switch (k)
													{
													case 1:
														num36 -= 136;
														break;
													case 2:
														num36 -= 174;
														break;
													}
													int num37 = num36 + 397;
													int right = num37 + num35;
													int num38 = (i - j * num3 - num29 * num) * 117 + num16 - 15;
													int bottom = num38 + 90;
													graphics.FillRectangle(solidBrush, new Rectangle(num37, num38, num35, 90));
													brush = Brushes.Black;
													color = solidBrush.Color;
													float num39 = (float)(int)color.R * 0.299f;
													color = solidBrush.Color;
													float num40 = num39 + (float)(int)color.G * 0.587f;
													color = solidBrush.Color;
													if (num40 + (float)(int)color.B * 0.114f < 60f)
													{
														brush = Brushes.White;
													}
													keyValuePair = resBeads[i].mixingColors.ElementAt(k);
													InscribeStringInRectangle(ref graphics, keyValuePair.Key.name, brush, num37, num38, right, bottom);
													num36 += 100;
													if (resBeads[i].mixingColors.Count > 1)
													{
														font = new Font("Tahoma", num12);
														graphics.DrawString("x", font, Brushes.Black, new PointF((float)(num36 + 632), (float)((i - j * num3 - num29 * num) * 117 + num16 - 13)));
														Graphics graphics2 = graphics;
														keyValuePair = resBeads[i].mixingColors.ElementAt(k);
														num41 = (float)(int)(keyValuePair.Value * 10f) / 10f;
														graphics2.DrawString(num41.ToString(), font, Brushes.Black, new PointF((float)(num36 + 684), (float)((i - j * num3 - num29 * num) * 117 + num16 - 13)));
														if (k < resBeads[i].mixingColors.Count - 1)
														{
															if (k == 0)
															{
																num36 -= 98;
															}
															graphics.DrawString("+", font, Brushes.Black, new PointF((float)(num36 + 841), (float)((i - j * num3 - num29 * num) * 117 + num16 - 13)));
														}
													}
												}
											}
											else
											{
												font = new Font("Tahoma", emSize5);
												graphics.DrawString("Не удалось подобрать смешение красок для данного оттенка", font, Brushes.Black, new PointF((float)(num28 + 400), (float)((i - j * num3 - num29 * num) * 117 + num16 - 7)));
											}
										}
										else
										{
											graphics.FillRectangle(solidBrush, new Rectangle(num28, (i - j * num3 - num29 * num) * 117 + num16 - 15, 128, 90));
											Brush brush2 = Brushes.Black;
											color = solidBrush.Color;
											float num42 = (float)(int)color.R * 0.299f;
											color = solidBrush.Color;
											float num43 = num42 + (float)(int)color.G * 0.587f;
											color = solidBrush.Color;
											if (num43 + (float)(int)color.B * 0.114f < 60f)
											{
												brush2 = Brushes.White;
											}
											graphics.DrawString(resBeads[i].symbolForColoring_AndForVoiceWorkInBiserok, font, brush2, new PointF((float)(num28 + 31), (float)((i - j * num3 - num29 * num) * 117 + num16 - 13)));
											font = new Font("Tahoma", emSize8, FontStyle.Bold);
											if (resBeads[i].mixingColors != null)
											{
												graphics.DrawString("=", font, Brushes.Black, new PointF((float)(num28 + 140), (float)((i - j * num3 - num29 * num) * 117 + num16 - 13)));
												float num44 = 1f;
												if (resBeads[i].mixingColors.Count() > 1)
												{
													float num45 = num44;
													keyValuePair = resBeads[i].mixingColors.ElementAt(1);
													num44 = num45 + keyValuePair.Value;
													if (resBeads[i].mixingColors.Count() > 2)
													{
														float num46 = num44;
														keyValuePair = resBeads[i].mixingColors.ElementAt(2);
														num44 = num46 + keyValuePair.Value;
													}
												}
												for (int l = 0; l < resBeads[i].mixingColors.Count(); l++)
												{
													keyValuePair = resBeads[i].mixingColors.ElementAt(l);
													solidBrush = new SolidBrush(keyValuePair.Key.clr);
													int num47 = 599;
													int num48 = num28 + 23;
													if (l == 2)
													{
														num48 += 78;
													}
													int num49 = num48 + 200 + num47 * l;
													int right2 = num49 + 220;
													int num50 = (i - j * num3 - num29 * num) * 117 + num16 - 15;
													int bottom2 = num50 + 90;
													graphics.FillRectangle(solidBrush, new Rectangle(num49, num50, 220, 90));
													brush2 = Brushes.Black;
													color = solidBrush.Color;
													float num51 = (float)(int)color.R * 0.299f;
													color = solidBrush.Color;
													float num52 = num51 + (float)(int)color.G * 0.587f;
													color = solidBrush.Color;
													if (num52 + (float)(int)color.B * 0.114f < 60f)
													{
														brush2 = Brushes.White;
													}
													keyValuePair = resBeads[i].mixingColors.ElementAt(l);
													InscribeStringInRectangle(ref graphics, keyValuePair.Key.name, brush2, num49, num50, right2, bottom2);
													int num53 = num16 - 8;
													if (resBeads[i].mixingColors.Count > 1)
													{
														graphics.DrawString("x", font, Brushes.Black, new PointF((float)(num48 + 428 + num47 * l), (float)((i - j * num3 - num29 * num) * 117 + num53)));
														Graphics graphics3 = graphics;
														keyValuePair = resBeads[i].mixingColors.ElementAt(l);
														num41 = (float)(int)(keyValuePair.Value * 10f) / 10f;
														graphics3.DrawString(num41.ToString(), font, Brushes.Black, new PointF((float)(num48 + 465 + num47 * l), (float)((i - j * num3 - num29 * num) * 117 + num53)));
														int num54 = 0;
														if (l > 0)
														{
															num54 = 78;
														}
														keyValuePair = resBeads[i].mixingColors.ElementAt(l);
														float squareInSm = keyValuePair.Value / num44 * resBeads[i].squareInSm2;
														float countOfMlForPaints = GetCountOfMlForPaints(squareInSm);
														int numberOfCharatersAfterPoint = 1;
														if (countOfMlForPaints < 1f)
														{
															numberOfCharatersAfterPoint = 2;
														}
														if (countOfMlForPaints < 0.1f)
														{
															numberOfCharatersAfterPoint = 3;
														}
														string s2 = "(" + GetStringCountOfMlForPaints(squareInSm, numberOfCharatersAfterPoint) + "мл)";
														font = new Font("Tahoma", emSize8, FontStyle.Regular);
														graphics.DrawString(s2, font, Brushes.Gray, new PointF((float)(num48 + 504 + num47 * l + num54), (float)((i - j * num3 - num29 * num) * 117 + num53)));
														font = new Font("Tahoma", emSize8, FontStyle.Bold);
														if (l < resBeads[i].mixingColors.Count - 1)
														{
															int num55 = num48 + 738 + num47 * l + num54;
															graphics.DrawString("+", font, Brushes.Black, new PointF((float)num55, (float)((i - j * num3 - num29 * num) * 117 + num53)));
														}
													}
													else
													{
														float squareInSm2 = resBeads[i].squareInSm2;
														float countOfMlForPaints2 = GetCountOfMlForPaints(squareInSm2);
														int numberOfCharatersAfterPoint2 = 1;
														if (countOfMlForPaints2 < 1f)
														{
															numberOfCharatersAfterPoint2 = 2;
														}
														if (countOfMlForPaints2 < 0.1f)
														{
															numberOfCharatersAfterPoint2 = 3;
														}
														string s3 = "(" + GetStringCountOfMlForPaints(squareInSm2, numberOfCharatersAfterPoint2) + "мл)";
														font = new Font("Tahoma", emSize8, FontStyle.Regular);
														graphics.DrawString(s3, font, Brushes.Gray, new PointF((float)(num48 + 465 + num47 * l), (float)((i - j * num3 - num29 * num) * 117 + num53)));
													}
												}
											}
											else
											{
												font = new Font("Tahoma", emSize5);
												graphics.DrawString("Не удалось подобрать смешение красок для данного оттенка", font, Brushes.Black, new PointF((float)(num28 + 160), (float)((i - j * num3 - num29 * num) * 117 + num16 - 7)));
											}
										}
									}
								}
								else if (i < resBeads.Count())
								{
									SolidBrush solidBrush = new SolidBrush(resBeads[i].clr);
									graphics.FillRectangle(solidBrush, new Rectangle(num28 + 219, (i - j * num3 - num29 * num) * 117 + num16 - 15, 220, 90));
									SolidBrush brush3 = Param.IsColorScheme_OrForGrayPrint ? new SolidBrush(resBeads[i].clr) : new SolidBrush(Color.White);
									graphics.FillRectangle(brush3, new Rectangle(num28 + 50, (i - j * num3 - num29 * num) * 117 + num16, 60, 60));
									graphics.DrawImage(resBeads[i].badge, new RectangleF((float)(num28 + 62), (float)((i - j * num3 - num29 * num) * 117 + num16 + 12), 36f, 36f));
									if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
									{
										SolidBrush brush4 = new SolidBrush(resBeads[i].clrBorder);
										float num56 = num9 * (float)(60 / SqureCellSidePrintPages_X);
										graphics.FillEllipse(brush4, new Rectangle(num28 + 130, (i - j * num3 - num29 * num) * 117 + num16, 60, 60));
										graphics.FillEllipse(solidBrush, new RectangleF((float)(num28 + 130) + num56, (float)((i - j * num3 - num29 * num) * 117 + num16) + num56, 60f - 2f * num56, 60f - 2f * num56));
										if (Param.IsDrawingBadgesOnSchemeForPrinting)
										{
											graphics.DrawImage(resBeads[i].badge, new RectangleF((float)(num28 + 130) + num56 + 6.5f, (float)((i - j * num3 - num29 * num) * 117 + num16) + num56 + 6.5f, 60f - 2f * num56 - 13f, 60f - 2f * num56 - 13f));
										}
									}
									num6 = notNullBlocks.Count(delegate(Block b)
									{
										if (b.IsVisible)
										{
											return b.ResBeads == resBeads[i];
										}
										return false;
									});
								}
								else
								{
									if (i == resBeads.Count())
									{
										if (list.Count() > 0)
										{
											graphics.DrawString("Бэкстич:", font, Brushes.Black, new PointF((float)(num28 - 131), (float)((i - j * num3 - num29 * num) * 117 + num16)));
										}
										else if (list2.Count() > 0)
										{
											graphics.DrawString("Французские узелки:", font, Brushes.Black, new PointF((float)(num28 - 131), (float)((i - j * num3 - num29 * num) * 117 + num16)));
										}
									}
									else if (list.Count() > 0)
									{
										if (i < resBeads.Count() + 1 + list.Count())
										{
											int index = resBeads.Count() + 1 + list.Count() - i - 1;
											SolidBrush solidBrush = new SolidBrush(list[index].clr);
											graphics.FillRectangle(solidBrush, new Rectangle(num28, (i - j * num3 - num29 * num) * 117 + num16 - 15, 320, 90));
										}
										else if (i == resBeads.Count() + 1 + list.Count())
										{
											graphics.DrawString("Французские узелки:", font, Brushes.Black, new PointF((float)(num28 - 131), (float)((i - j * num3 - num29 * num) * 117 + num16)));
										}
										else
										{
											int index2 = resBeads.Count() + 1 + list.Count() + 1 + list2.Count() - i - 1;
											SolidBrush solidBrush = new SolidBrush(list2[index2].clr);
											graphics.FillRectangle(solidBrush, new Rectangle(num28, (i - j * num3 - num29 * num) * 117 + num16 - 15, 320, 90));
										}
									}
									else if (list2.Count() > 0)
									{
										int index3 = resBeads.Count() + list2.Count() - i;
										SolidBrush solidBrush = new SolidBrush(list2[index3].clr);
										graphics.FillRectangle(solidBrush, new Rectangle(num28, (i - j * num3 - num29 * num) * 117 + num16 - 15, 320, 90));
									}
									flag = false;
								}
							}
							goto IL_34a6;
							IL_34a6:
							if ((Settings.hobbyProgramm != HobbyProgramm.Raskraska & flag) && !Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
							{
								if (num6 > 0)
								{
									string s4 = "";
									if (Settings.hobbyProgramm == HobbyProgramm.Biserok || Settings.hobbyProgramm == HobbyProgramm.Almaz)
									{
										float num57 = ((float)Param.countOfBeadsIn1000Gramm + 0f) / 1000f;
										s4 = ((int)Math.Round((double)(((float)num6 + 0f) / num57)) + 1).ToString() + Local.Get(" гр");
									}
									else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
									{
										int num58 = (int)(27.935f / (float)Param.beadsOrCanvasSize * (float)num6 * (float)Param.NumberOfAdditionThread);
										float num59 = 10f;
										if (num58 <= 1000)
										{
											num59 = 100f;
										}
										float num60 = (float)(int)Math.Round((double)((float)num58 / 1000f * num59)) / num59;
										int num61 = (int)(num60 / 8f);
										if ((float)num61 * 8f < num60)
										{
											num61++;
										}
										s4 = num60.ToString() + Local.Get(" м, ") + num61.ToString();
										string src = "";
										int num62 = num61;
										num62 -= num62 / 100 * 100;
										if (num62 >= 10 && num62 <= 14)
										{
											src = "мотков";
										}
										else
										{
											switch (num62 - num62 / 10 * 10)
											{
											case 0:
												src = "мотков";
												break;
											case 1:
												src = "моток";
												break;
											case 2:
												src = "мотка";
												break;
											case 3:
												src = "мотка";
												break;
											case 4:
												src = "мотка";
												break;
											case 5:
												src = "мотков";
												break;
											case 6:
												src = "мотков";
												break;
											case 7:
												src = "мотков";
												break;
											case 8:
												src = "мотков";
												break;
											case 9:
												src = "мотков";
												break;
											}
										}
										s4 = s4 + " " + Local.Get(src);
									}
									else if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
									{
										float num63 = (float)num6 * Param.VerticalSizeOfBlockAtSchemeInMm * Param.HorizontalSizeOfBlockAtSchemeInMm / 100f;
										s4 = ((float)(int)Math.Round((double)(num63 * 100f / 1000f * Param.LengthOfYarnPer1000sm2)) / 100f).ToString() + Local.Get(" м.");
									}
									int num64 = num28 + 879;
									if (Settings.isItSpecialVersionOfBiserokForMaxim)
									{
										num64 = num28 + 800;
									}
									graphics.DrawString(num6.ToString(), font, Brushes.Black, new PointF((float)num64, (float)((i - j * num3 - num29 * num) * 117 + num16 + 1)));
									if (!Settings.isItSpecialVersionOfBiserokForMaxim)
									{
										graphics.DrawString(s4, font, Brushes.Black, new PointF((float)(num28 + 1519), (float)((i - j * num3 - num29 * num) * 117 + num16 + 1)));
									}
								}
								else if (!Settings.isItSpecialVersionOfBiserokForMaxim)
								{
									graphics.DrawString(GetStringWithInfoAboutNotNeededInThisBeads(false), font, Brushes.Black, new PointF((float)(num28 + 879), (float)((i - j * num3 - num29 * num) * 117 + num16 + 1)));
								}
							}
							goto end_IL_12b7;
							IL_12fc:
							int num65 = i - num8;
							if (num65 < resBeads.Count())
							{
								if (beadsManufacturer != resBeads[num65].parentSeries.parentManufacturer && !Settings.isItSpecialVersionOfBiserokForMaxim && !Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
								{
									num8++;
									beadsManufacturer = resBeads[num65].parentSeries.parentManufacturer;
									SolidBrush brush5 = new SolidBrush(Color.Black);
									text2 = ((v == 0) ? "Название производителя недоступно в демо-версии!" : beadsManufacturer.name);
									graphics.DrawString(text2, font, brush5, new PointF((float)(num28 - 131), (float)((i - j * num3 - num29 * num) * 117 + num16)));
									flag = false;
								}
								else if (IsNeedToWriteSeriesName(true) && beadsSeries != resBeads[num65].parentSeries && !Settings.isItSpecialVersionOfBiserokForMaxim && !Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
								{
									beadsSeries = resBeads[num65].parentSeries;
									num8++;
									text = ((v == 0) ? "Серия: Название серии недоступно в демо-версии!" : ("Серия \"" + beadsSeries.name + "\""));
									graphics.DrawString(text, font, Brushes.Black, new PointF((float)(num28 - 131), (float)((i - j * num3 - num29 * num) * 117 + num16)));
									flag = false;
								}
								else if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina && Settings.hobbyProgramm == HobbyProgramm.Almaz)
								{
									Graphics graphics4 = graphics;
									num26 = num10 + 1;
									num10 = num26;
									graphics4.DrawString(num26.ToString(), font, Brushes.Black, new PointF((float)num28, (float)((i - j * num3 - num29 * num) * 117 + num16)));
									SolidBrush brush6 = new SolidBrush(resBeads[num65].clr);
									graphics.FillRectangle(brush6, new RectangleF((float)(num28 + 165), (float)((i - j * num3 - num29 * num) * 117 + num16), 90f, 90f));
									Pen pen3 = new Pen(Brushes.Black, (float)(int)Math.Round((double)((float)Param.PrintingResolution / 200f)));
									graphics.DrawRectangle(pen3, new Rectangle(num28 + 165, (i - j * num3 - num29 * num) * 117 + num16, 90, 90));
									graphics.DrawImage(resBeads[num65].badge, new RectangleF((float)(num28 + 165 + 18), (float)((i - j * num3 - num29 * num) * 117 + num16 + 18), 54f, 54f));
									string name = resBeads[num65].name;
									graphics.DrawString(name, font, Brushes.Black, new PointF((float)(num28 + 324), (float)((i - j * num3 - num29 * num) * 117 + num16)));
									try
									{
										num6 = countOfEachBeads[resBeads[num65]];
									}
									catch
									{
										num6 = 0;
									}
									text3 = "-";
									if (num6 > 0)
									{
										text3 = num6.ToString();
									}
									graphics.DrawString(text3, font, Brushes.Black, new PointF((float)(num28 + 617), (float)((i - j * num3 - num29 * num) * 117 + num16)));
									float num66 = ((float)Param.countOfBeadsIn1000Gramm + 0f) / 1000f;
									int num67 = (int)Math.Round((double)(((float)num6 + 0f) / num66)) + 1;
									graphics.DrawString(num67.ToString(), font, Brushes.Black, new PointF((float)(num28 + 892), (float)((i - j * num3 - num29 * num) * 117 + num16)));
									pen3 = new Pen(Brushes.Black, (float)(int)Math.Round((double)((float)Param.PrintingResolution / 200f)));
									float num68 = (float)(num28 - 30);
									float y = (float)((i - j * num3 - num29 * num) * 117 + num16 - 13);
									float num69 = 158f;
									float height2 = 117f;
									graphics.DrawRectangle(pen3, num68, y, num69, height2);
									num68 += num69;
									num69 = 164f;
									graphics.DrawRectangle(pen3, num68, y, num69, height2);
									num68 += num69;
									num69 = 254f;
									graphics.DrawRectangle(pen3, num68, y, num69, height2);
									num68 += num69;
									num69 = 300f;
									graphics.DrawRectangle(pen3, num68, y, num69, height2);
									num68 += num69;
									num69 = 177f;
									graphics.DrawRectangle(pen3, num68, y, num69, height2);
								}
								else
								{
									SolidBrush brush6 = new SolidBrush(resBeads[num65].clr);
									graphics.FillRectangle(brush6, new Rectangle(num28 + 154, (i - j * num3 - num29 * num) * 117 + num16 - 15, 220, 90));
									if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
									{
										graphics.DrawString(resBeads[num65].symbolForColoring_AndForVoiceWorkInBiserok, font, Brushes.Black, new PointF((float)(num28 - 20), (float)((i - j * num3 - num29 * num) * 117 + num16 - 13)));
										graphics.DrawString(resBeads[num65].name, font, Brushes.Black, new PointF(750f, (float)((i - j * num3) * 117 + num16 - 13)));
										if (Settings.v > 1)
										{
											graphics.DrawString(resBeads[num65].squareInSm2.ToString() + " см", font, Brushes.Black, new PointF(1600f, (float)((i - j * num3) * 117 + num16 - 13)));
											sizeF6 = graphics.MeasureString(resBeads[num65].squareInSm2.ToString() + " см", font);
											float width2 = sizeF6.Width;
											graphics.DrawString("2", font3, Brushes.Black, new PointF(1600f + width2 - 18f, (float)((i - j * num3) * 117 + num16 - 26)));
											graphics.DrawString("~ " + GetStringCountOfMlForPaints(resBeads[num65].squareInSm2, 1) + " мл.", font, Brushes.Black, new PointF(1600f + width2 + 32f, (float)((i - j * num3) * 117 + num16 - 13)));
										}
									}
									else
									{
										SolidBrush brush7 = Param.IsColorScheme_OrForGrayPrint ? new SolidBrush(resBeads[num65].clr) : new SolidBrush(Color.White);
										if (!Settings.isItSpecialVersionOfBiserokForMaxim && !Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
										{
											Rectangle rect = new Rectangle(num28, (i - j * num3 - num29 * num) * 117 + num16, 60, 60);
											graphics.DrawRectangle(new Pen(new SolidBrush(Color.Black)), rect.X - 1, rect.Y - 1, rect.Width + 1, rect.Height + 1);
											graphics.FillRectangle(brush7, rect);
											graphics.DrawImage(resBeads[num65].badge, new RectangleF((float)(num28 + 12), (float)((i - j * num3 - num29 * num) * 117 + num16 + 12), 36f, 36f));
										}
										if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
										{
											SolidBrush brush8 = new SolidBrush(resBeads[num65].clrBorder);
											float num70 = num9 * (float)(60 / SqureCellSidePrintPages_X);
											graphics.FillEllipse(brush8, new RectangleF((float)(num28 + 80), (float)((i - j * num3 - num29 * num) * 117 + num16), 60f, 60f));
											graphics.FillEllipse(brush6, new RectangleF((float)(num28 + 80) + num70, (float)((i - j * num3 - num29 * num) * 117 + num16) + num70, 60f - num70 * 2f, 60f - num70 * 2f));
											if (Param.IsDrawingBadgesOnSchemeForPrinting)
											{
												graphics.DrawImage(resBeads[num65].badge, new RectangleF((float)(num28 + 80) + num70 + 6.5f, (float)((i - j * num3 - num29 * num) * 117 + num16) + num70 + 6.5f, 60f - 2f * num70 - 13f, 60f - 2f * num70 - 13f));
											}
										}
										string text9 = resBeads[num65].name;
										if (v == 0)
										{
											text9 = "****" + text9.Last().ToString();
										}
										int num71 = num28 + 394;
										graphics.DrawString(" - " + text9, font, Brushes.Black, new PointF((float)num71, (float)((i - j * num3 - num29 * num) * 117 + num16 + 1)));
										try
										{
											num6 = countOfEachBeads[resBeads[num65]];
										}
										catch
										{
											num6 = 0;
										}
									}
								}
							}
							else
							{
								if (num65 == resBeads.Count())
								{
									if (list.Count() > 0)
									{
										graphics.DrawString("Бэкстич:", font, Brushes.Black, new PointF((float)(num28 - 131), (float)((i - j * num3 - num29 * num) * 117 + num16)));
									}
									else if (list2.Count() > 0)
									{
										graphics.DrawString("Французские узелки:", font, Brushes.Black, new PointF((float)(num28 - 131), (float)((i - j * num3 - num29 * num) * 117 + num16)));
									}
								}
								else if (list.Count() > 0)
								{
									if (num65 < resBeads.Count() + 1 + list.Count())
									{
										int index4 = resBeads.Count() + 1 + list.Count() - num65 - 1;
										SolidBrush brush6 = new SolidBrush(list[index4].clr);
										graphics.FillRectangle(brush6, new Rectangle(num28, (i - j * num3 - num29 * num) * 117 + num16 - 15, 320, 90));
										graphics.DrawString(" - " + list[index4].parentSeries.parentManufacturer.name + " " + list[index4].name, font, Brushes.Black, new PointF((float)(num28 + 320), (float)((i - j * num3 - num29 * num) * 117 + num16 + 1)));
									}
									else if (num65 == resBeads.Count() + 1 + list.Count())
									{
										graphics.DrawString("Французские узелки:", font, Brushes.Black, new PointF((float)(num28 - 131), (float)((i - j * num3 - num29 * num) * 117 + num16)));
									}
									else
									{
										int index5 = resBeads.Count() + 1 + list.Count() + 1 + list2.Count() - num65 - 1;
										SolidBrush brush6 = new SolidBrush(list2[index5].clr);
										graphics.FillRectangle(brush6, new Rectangle(num28, (i - j * num3 - num29 * num) * 117 + num16 - 15, 320, 90));
										graphics.DrawString(" - " + list2[index5].parentSeries.parentManufacturer.name + " " + list2[index5].name, font, Brushes.Black, new PointF((float)(num28 + 320), (float)((i - j * num3 - num29 * num) * 117 + num16 + 1)));
									}
								}
								else if (list2.Count() > 0)
								{
									int index6 = resBeads.Count() + list2.Count() - num65;
									SolidBrush brush6 = new SolidBrush(list2[index6].clr);
									graphics.FillRectangle(brush6, new Rectangle(num28, (i - j * num3 - num29 * num) * 117 + num16 - 15, 320, 90));
									graphics.DrawString(" - " + list2[index6].parentSeries.parentManufacturer.name + " " + list2[index6].name, font, Brushes.Black, new PointF((float)(num28 + 320), (float)((i - j * num3 - num29 * num) * 117 + num16 + 1)));
								}
								num65++;
								flag = false;
							}
							goto IL_34a6;
							end_IL_12b7:;
						}
						catch (Exception)
						{
						}
						num26 = ++i;
					}
				}
				else
				{
					float emSize10 = 90f * KfSystemFontSizes;
					Font font4 = new Font("Tahoma", emSize10, FontStyle.Bold);
					graphics.DrawString("В тестовой версии доступен", font4, new SolidBrush(Color.Red), new PointF(135f, 799f));
					graphics.DrawString("лишь первый лист легенды", font4, new SolidBrush(Color.Red), new PointF(135f, 1033f));
				}
				font = new Font("Tahoma", emSize6);
				text3 = "";
				s = "";
				string s5 = "";
				int num72 = 200;
				if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
				{
					text3 = "*Размер схемы в см и вес в граммах для бисера каждого цвета указывается";
					num26 = Param.beadsOrCanvasSize;
					s = "ориентировочно - для бисера размера " + num26.ToString() + "/0. ПРОСЬБА - для важных участков вышивки";
					s5 = "(лица и т.п.) перед вышивкой убедиться в верности подобранных оттенков";
				}
				else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
				{
					text3 = "*Размер схемы в см, а также вес в граммах страз каждого цвета";
					s = "    указывается ориентировочно - для страз размера 2,5 х 2,5 мм";
				}
				else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
				{
					text3 = "*Размер схемы в см, а также длина мулине каждого цвета";
					string src2 = "";
					int numberOfAdditionThread = Param.NumberOfAdditionThread;
					numberOfAdditionThread -= numberOfAdditionThread / 100 * 100;
					if (numberOfAdditionThread >= 10 && numberOfAdditionThread <= 14)
					{
						src2 = "сложений";
					}
					else
					{
						switch (numberOfAdditionThread - numberOfAdditionThread / 10 * 10)
						{
						case 0:
							src2 = "сложений";
							break;
						case 1:
							src2 = "сложение";
							break;
						case 2:
							src2 = "сложения";
							break;
						case 3:
							src2 = "сложения";
							break;
						case 4:
							src2 = "сложения";
							break;
						case 5:
							src2 = "сложений";
							break;
						case 6:
							src2 = "сложений";
							break;
						case 7:
							src2 = "сложений";
							break;
						case 8:
							src2 = "сложений";
							break;
						case 9:
							src2 = "сложений";
							break;
						}
					}
					string[] obj3 = new string[7]
					{
						"    указывается ориентировочно - для канвы Аида ",
						null,
						null,
						null,
						null,
						null,
						null
					};
					num26 = Param.beadsOrCanvasSize;
					obj3[1] = num26.ToString();
					obj3[2] = ", в ";
					num26 = Param.NumberOfAdditionThread;
					obj3[3] = num26.ToString();
					obj3[4] = " ";
					obj3[5] = Local.Get(src2);
					obj3[6] = Local.Get(" нити");
					s = string.Concat(obj3);
					s5 = "    (также считается, что в 1 мотке есть 8 метров мулине)";
				}
				else if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
				{
					text3 = "*Размер схемы в см, а также длина пряжи каждого цвета";
					s = "    указывается ориентировочно - для плотности вязания:";
					s5 = "    " + Param.KnittingDensityName;
				}
				else if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					if (Param.BiserType == BiserType.ForManufacturers)
					{
						if (Param.IsMixingColors)
						{
							if (Settings.v > 1)
							{
								text3 = "Смешивать краски можно в любых измерительных величинах, согласно пропорциям";
								s = "(выделены жирным). В скобках серым цветом указан мин.объём краски, нужный";
								s5 = "для смешения. Пожалуйста, помните, что формула - только ПРИМЕРНАЯ ОСНОВА!";
								num72 = 215;
							}
							else
							{
								text3 = "Смешивать краски можно в любых измерительных величинах (мл, мг или другие)";
								s = "по указанным пропорциям. Но, пожалуйста, помните, что формула - только";
								s5 = "ПРИМЕРНАЯ ОСНОВА! Есть замечания по подбору пропорций? Пишите нам!";
								num72 = 215;
							}
						}
						else if (Settings.v > 1)
						{
							s = "Выше указана площадь, закрашиваемая каждым цветом, и примерный необходимый";
							s5 = "объём краски (исходя из расхода краски 250мл/1кв.м.)";
							num72 = 215;
						}
					}
					else if (Settings.v > 1)
					{
						s = "Выше указана площадь, закрашиваемая каждым цветом, и примерный необходимый";
						s5 = "объём краски (исходя из расхода краски 250мл/1кв.м.)";
						num72 = 215;
					}
				}
				if (!Settings.isItSpecialVersionOfBiserokForMaxim && !Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
				{
					graphics.DrawString(text3, font, Brushes.Black, new PointF((float)num72, 3255f));
					graphics.DrawString(s, font, Brushes.Black, new PointF((float)num72, 3325f));
					graphics.DrawString(s5, font, Brushes.Black, new PointF((float)num72, 3395f));
				}
				list3.Add(bitmap);
			}
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && Param.BiserType == BiserType.ForManufacturers && Param.IsMixingColors)
			{
				List<Beads> list4 = new List<Beads>();
				foreach (Beads resBead in resBeads)
				{
					float num73 = 1f;
					if (resBead.mixingColors != null)
					{
						if (resBead.mixingColors.Count() > 1)
						{
							float num74 = num73;
							keyValuePair = resBead.mixingColors.ElementAt(1);
							num73 = num74 + keyValuePair.Value;
							if (resBead.mixingColors.Count() > 2)
							{
								float num75 = num73;
								keyValuePair = resBead.mixingColors.ElementAt(2);
								num73 = num75 + keyValuePair.Value;
							}
						}
						foreach (KeyValuePair<Beads, float> mixingColor in resBead.mixingColors)
						{
							Beads beads = mixingColor.Key;
							float num76 = mixingColor.Value / num73 * resBead.squareInSm2;
							if (!list4.Any((Beads b) => b.id == beads.id))
							{
								list4.Add(new Beads
								{
									id = beads.id,
									name = beads.name,
									parentSeries = beads.parentSeries,
									clr = beads.clr,
									squareInSm2 = num76
								});
							}
							else
							{
								list4.First((Beads b) => b.id == beads.id).squareInSm2 += num76;
							}
						}
					}
				}
				list4 = (from b in list4
				orderby b.parentSeries.name
				select b).ToList();
				CreateAdditionalListWithInfoAboutNeededColorsOfManufacturers(ref list3, list4);
			}
			if (settings.isPrinting)
			{
				foreach (Bitmap item in list3)
				{
					Bitmap bitmap3 = PrintingBmp = item;
					pd.Print();
				}
			}
			if (settings.isSaving)
			{
				string text10 = "Легенда";
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					text10 = "Карта цветов";
				}
				if (list3.Count() == 1)
				{
					list3[0].Save(settings.pathToSave + "\\" + text10 + ".png", format);
				}
				else
				{
					for (int m = 0; m < list3.Count(); m++)
					{
						list3[m].Save(string.Format("{0}\\" + text10 + ", лист {1}.png", settings.pathToSave, m + 1), format);
					}
				}
			}
			for (int n = 0; n < list3.Count(); n++)
			{
				list3[n].Dispose();
			}
			list3.Clear();
			return;
			IL_018e:
			int num77 = (from r in resBeads
			select r.parentSeries.parentManufacturer.name).Distinct().Count();
			int num78 = 0;
			if (IsNeedToWriteSeriesName(true))
			{
				num78 = (from r in resBeads
				select r.parentSeries).Distinct().Count();
			}
			if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
			{
				num78 = (num77 = 0);
			}
			num5 += resBeads.Count + num78 + num77;
			num4 = (int)Math.Ceiling((double)(((float)num5 + 0f) / (float)num3));
			num7 = resBeads.Count();
			goto IL_0285;
		}

		private void CreateAdditionalListWithInfoAboutNeededColorsOfManufacturers(ref List<Bitmap> bmpLegendLists, List<Beads> colorsOfManufacturers)
		{
			int num = 26;
			if (v > 1)
			{
				num = 25;
			}
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			int num5 = (from r in colorsOfManufacturers
			select r.parentSeries.parentManufacturer.name).Distinct().Count();
			int num6 = 0;
			if (IsNeedToWriteSeriesName(true))
			{
				num6 = (from r in colorsOfManufacturers
				select r.parentSeries).Distinct().Count();
			}
			num3 += colorsOfManufacturers.Count + num6 + num5;
			num2 = (int)Math.Ceiling((double)(((float)num3 + 0f) / (float)num));
			num4 = colorsOfManufacturers.Count();
			int num7 = 0;
			BeadsSeries beadsSeries = new BeadsSeries();
			BeadsManufacturer beadsManufacturer = new BeadsManufacturer();
			colorsOfManufacturers.Sort(new ComparerBeadsBySeriesAndManufacturer());
			for (int i = 0; i < num2; i++)
			{
				int num8 = (i + 1) * num;
				if (i == num3 / num)
				{
					num8 = num3;
				}
				Bitmap bitmap = new Bitmap(2480, 3507);
				Graphics graphics = Graphics.FromImage(bitmap);
				graphics.FillRectangle(Brushes.White, 0, 0, 2480, 3507);
				float emSize = 48f * KfSystemFontSizes;
				float emSize2 = 50f * KfSystemFontSizes;
				float emSize3 = 40f * KfSystemFontSizes;
				float emSize4 = 35f * KfSystemFontSizes;
				Font font = new Font("Tahoma", emSize, FontStyle.Bold);
				string text = "";
				if (num3 > num)
				{
					text = ", лист №" + (i + 1);
				}
				float widthColoringInSm = Param.WidthColoringInSm;
				float num9 = ((float)HeightSourceImageInPx + 0f) / ((float)WidthSourceImageInPx + 0f) * widthColoringInSm;
				num9 = (float)(int)Math.Round((double)(num9 * 10f)) / 10f;
				graphics.DrawString(string.Format(Local.Get("Какие краски нужны (") + "{0}" + Local.Get(" шт.) для получения из них"), num4), font, Brushes.Black, new PointF(130f, 110f));
				graphics.DrawString("оттенков, указанных на прошлых страницах карты цветов:", font, Brushes.Black, new PointF(130f, 205f));
				int num10 = 340;
				for (int j = i * num; j < num8; j++)
				{
					int num11 = 220;
					font = new Font("Tahoma", emSize2);
					Font font2 = new Font("Tahoma", emSize4);
					try
					{
						int index = j - num7;
						if (beadsManufacturer != colorsOfManufacturers[index].parentSeries.parentManufacturer)
						{
							num7++;
							beadsManufacturer = colorsOfManufacturers[index].parentSeries.parentManufacturer;
							font = new Font("Tahoma", emSize2, FontStyle.Bold);
							graphics.DrawString(beadsManufacturer.name, font, Brushes.Black, new PointF((float)(num11 - 10), (float)((j - i * num) * 117 + num10)));
						}
						else if (IsNeedToWriteSeriesName(true) && beadsSeries != colorsOfManufacturers[index].parentSeries)
						{
							beadsSeries = colorsOfManufacturers[index].parentSeries;
							num7++;
							graphics.DrawString("Серия \"" + beadsSeries.name + "\"", font, Brushes.Black, new PointF((float)(num11 - 10), (float)((j - i * num) * 117 + num10)));
						}
						else
						{
							SolidBrush brush = new SolidBrush(colorsOfManufacturers[index].clr);
							graphics.FillRectangle(brush, new Rectangle(num11, (j - i * num) * 117 + num10 - 15, 220, 90));
							string text2 = colorsOfManufacturers[index].name;
							if (Settings.v > 1)
							{
								text2 = text2 + "  (~ " + GetStringCountOfMlForPaints(colorsOfManufacturers[index].squareInSm2, 1) + " мл.)";
							}
							graphics.DrawString(text2, font, Brushes.Black, new PointF(480f, (float)((j - i * num) * 117 + num10 - 13)));
						}
					}
					catch (Exception)
					{
					}
				}
				if (Settings.v > 1)
				{
					font = new Font("Tahoma", emSize3);
					int num12 = 215;
					graphics.DrawString("Справа от названий цветов в скобках указан примерный необходимый объём", font, Brushes.Black, new PointF((float)num12, 3285f));
					graphics.DrawString("краски данного цвета (исходя из расхода краски 250мл/1кв.м.)", font, Brushes.Black, new PointF((float)num12, 3355f));
				}
				bmpLegendLists.Add(bitmap);
			}
		}

		private void InscribeStringInRectangle(ref Graphics g, string name, Brush br, int left, int top, int right, int bottom)
		{
			float num = 28f;
			Font font;
			SizeF sizeF;
			do
			{
				font = new Font("Tahoma", num, FontStyle.Regular);
				sizeF = g.MeasureString(name, font);
				num -= 1f;
			}
			while (sizeF.Width >= (float)(right - left) || sizeF.Height >= (float)(bottom - top));
			int num2 = top + (int)Math.Round((double)(((float)(bottom - top) - sizeF.Height) / 2f));
			int num3 = left + (int)Math.Round((double)(((float)(right - left) - sizeF.Width) / 2f));
			g.DrawString(name, font, br, new PointF((float)num3, (float)num2));
		}

		private bool IsNeedToWriteSeriesName(bool isFullLegendLists)
		{
			if (isFullLegendLists)
			{
				return Settings.hobbyProgramm != HobbyProgramm.Krestik;
			}
			return false;
		}

		private void DrawOnMainSchemeAllStrings(Bitmap scheme, int dTop, int dLeft, int dBottom, int dRight, float kf)
		{
			SolidBrush brush = new SolidBrush(Color.Black);
			float emSize = 8f * (float)SqureCellSidePrintPages_Y * KfSystemFontSizes;
			Graphics graphics = Graphics.FromImage(scheme);
			int heightPageInBeads = Param.heightPageInBeads;
			int widthPageInBeads = Param.widthPageInBeads;
			Font font;
			if (Param.IsShowNumberOfListOnCommonScheme)
			{
				int num = 0;
				for (int i = 0; i <= Param.HeightInBead / heightPageInBeads; i++)
				{
					if (Param.HeightInBead != i * heightPageInBeads)
					{
						for (int j = 0; j <= Param.WidthInBead / widthPageInBeads; j++)
						{
							if (Param.WidthInBead != j * widthPageInBeads)
							{
								int num2 = 1 + j * widthPageInBeads * SqureCellSidePrintPages_X + dLeft;
								int num3 = 5 + SqureCellSidePrintPages_X * widthPageInBeads + 5 - 1 - 4;
								if (j == Param.WidthInBead / widthPageInBeads)
								{
									num3 = scheme.Width - dLeft - dRight - (5 + j * widthPageInBeads * SqureCellSidePrintPages_X - 1 - 3);
								}
								int num4 = 1 + i * heightPageInBeads * SqureCellSidePrintPages_Y + dTop;
								int num5 = 5 + SqureCellSidePrintPages_Y * heightPageInBeads + 5 - 1 - 4;
								if (i == Param.HeightInBead / heightPageInBeads)
								{
									num5 = scheme.Height - dTop - dBottom - (5 + i * heightPageInBeads * SqureCellSidePrintPages_Y - 1 - 3);
								}
								font = new Font("Tahoma", emSize, FontStyle.Bold);
								Graphics graphics2 = graphics;
								int num6 = num + 1;
								num = num6;
								graphics2.DrawString(num6.ToString(), font, brush, new PointF((float)num2 + 0.5f * (float)num3 - 110f, (float)num4 + 0.5f * (float)num5 - 135f));
							}
						}
					}
				}
			}
			float emSize2 = 70f * kf * KfSystemFontSizes;
			float emSize3 = 130f * kf * KfSystemFontSizes;
			float emSize4 = 90f * kf * KfSystemFontSizes;
			float emSize5 = 55f * kf * KfSystemFontSizes;
			font = new Font("Tahoma", emSize2);
			int num7 = scheme.Width - dRight;
			graphics.DrawString("Схема составлена с использованием", font, Brushes.Black, new PointF((float)num7 - 2955f * kf, 5f * kf));
			graphics.DrawString("программы", font, Brushes.Black, new PointF((float)num7 - 1855f * kf, 85f * kf));
			font = new Font("Tahoma", emSize3, FontStyle.Bold);
			string str = "";
			string str2 = "";
			if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
			{
				graphics.DrawString("\"Бисерок 2.0\"", font, Brushes.Black, new PointF((float)num7 - 1290f * kf, 0f));
				str = "вышивке";
				str2 = "programma-biserok.ru";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				graphics.DrawString("\"Крестик 2.0\"", font, Brushes.Black, new PointF((float)num7 - 1290f * kf, 0f));
				str = "вышивке";
				str2 = "программа-крестик.рф";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
			{
				graphics.DrawString("\"Петелька\"", font, Brushes.Black, new PointF((float)num7 - 1290f * kf, 0f));
				str = "вязании";
				str2 = "programma-petelka.ru";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
			{
				font = new Font("Tahoma", emSize4, FontStyle.Bold);
				graphics.DrawString("\"Алмазная мозаика\"", font, Brushes.Black, new PointF((float)num7 - 1290f * kf, 30f * kf));
				str = "рукоделии";
				str2 = "almaznaja-mozaika.ru";
			}
			font = new Font("Tahoma", emSize5);
			graphics.DrawString("Успехов в " + str + "!", font, Brushes.Black, new PointF((float)num7 - 1170f * kf, 186f * kf));
			font = new Font("Tahoma", emSize5);
			graphics.DrawString("Сайт: " + str2, font, Brushes.Black, new PointF((float)num7 - 2942f * kf, 166f * kf));
			graphics.Dispose();
		}

		private void GenerateSchemePagesAndPrintOrSaveToFolder(Bitmap scheme, SavingOrPrintingSettings settings, int dTop, int dLeft, int dBottom, int dRight)
		{
			List<Bitmap> list = new List<Bitmap>();
			int heightPageInBeads = Param.heightPageInBeads;
			int widthPageInBeads = Param.widthPageInBeads;
			int num = 0;
			for (int i = 0; i <= Param.HeightInBead / heightPageInBeads; i++)
			{
				if (Param.HeightInBead != i * heightPageInBeads)
				{
					for (int j = 0; j <= Param.WidthInBead / widthPageInBeads; j++)
					{
						if (Param.WidthInBead != j * widthPageInBeads)
						{
							int num2 = 1 + j * widthPageInBeads * SqureCellSidePrintPages_X + dLeft;
							int num3 = 5 + SqureCellSidePrintPages_X * widthPageInBeads;
							int num4 = 1 + i * heightPageInBeads * SqureCellSidePrintPages_Y + dTop;
							int num5 = 5 + SqureCellSidePrintPages_Y * heightPageInBeads;
							Bitmap bitmap = new Bitmap(num3 + 40 + 120, num5 + 40 + 85);
							if (j == Param.WidthInBead / widthPageInBeads)
							{
								num3 = scheme.Width - dLeft - dRight - (j * widthPageInBeads * SqureCellSidePrintPages_X + 1);
							}
							if (i == Param.HeightInBead / heightPageInBeads)
							{
								num5 = scheme.Height - dBottom - dTop - (i * heightPageInBeads * SqureCellSidePrintPages_Y + 1);
							}
							Graphics graphics = Graphics.FromImage(bitmap);
							graphics.FillRectangle(Brushes.White, new Rectangle(0, 0, bitmap.Width, bitmap.Height));
							num++;
							graphics.DrawImage(scheme, new RectangleF(40f, 40f, (float)num3, (float)num5), new RectangleF((float)num2, (float)num4, (float)num3, (float)num5), GraphicsUnit.Pixel);
							float emSize = 3f * (float)SqureCellSidePrintPages_Y * KfSystemFontSizes;
							float emSize2 = 2.3f * (float)SqureCellSidePrintPages_Y * KfSystemFontSizes;
							float emSize3 = 0.9f * (float)SqureCellSidePrintPages_Y * KfSystemFontSizes;
							Font font;
							if (v == 0 && (i > 0 || j > 0))
							{
								graphics.FillRectangle(Brushes.White, new RectangleF(43f, 43f, (float)(num3 - 6), (float)(num5 - 6)));
								font = new Font("Tahoma", emSize, FontStyle.Bold);
								graphics.DrawString("В тестовой версии доступен", font, Brushes.Red, new PointF(40f, 800f * ((float)heightPageInBeads + 0f) / 90f));
								graphics.DrawString(" лишь первый лист схемы", font, Brushes.Red, new PointF(40f, 1000f * ((float)heightPageInBeads + 0f) / 90f));
							}
							font = new Font("Tahoma", emSize2, FontStyle.Bold);
							graphics.DrawString(num.ToString(), font, Brushes.Black, new PointF((float)(40 + num3 + 35), (float)(40 + SqureCellSidePrintPages_Y)));
							list.Add(bitmap);
							int num6 = (j + 1) * widthPageInBeads;
							int num7 = (i + 1) * heightPageInBeads;
							if (j == Param.WidthInBead / widthPageInBeads)
							{
								num6 = Param.WidthInBead;
							}
							if (i == Param.HeightInBead / heightPageInBeads)
							{
								num7 = Param.HeightInBead;
							}
							font = new Font("Tahoma", emSize3);
							for (int k = j * widthPageInBeads; k <= num6; k += 10)
							{
								graphics.DrawString(k.ToString(), font, Brushes.Black, new PointF((float)(40 + k * SqureCellSidePrintPages_X - 10 - j * SqureCellSidePrintPages_X * widthPageInBeads), (float)(40 + num5 + 5)));
							}
							for (int l = i * heightPageInBeads; l <= num7; l += 10)
							{
								graphics.DrawString(l.ToString(), font, Brushes.Black, new PointF((float)(40 + num3 + 15), (float)(40 + l * SqureCellSidePrintPages_Y - 10 - i * SqureCellSidePrintPages_Y * heightPageInBeads)));
							}
						}
					}
				}
			}
			if (settings.isPrinting)
			{
				foreach (Bitmap item in list)
				{
					Bitmap bitmap2 = PrintingBmp = item;
					pd.Print();
				}
			}
			if (settings.isSaving)
			{
				for (int m = 0; m < list.Count(); m++)
				{
					list[m].Save(string.Format("{0}\\" + Local.Get("Лист ") + "{1}.png", settings.pathToSave, m + 1), format);
				}
			}
			for (int n = 0; n < list.Count(); n++)
			{
				list[n].Dispose();
			}
			list.Clear();
		}

		private void CalculateGeometryParamsOfScheme(ref int countRowsInMiniLegend, ref int countColumnsInMiniLegend, ref int countRecordsInMiniLegend, ref int additionalHeightWithMiniLegend, ref int additionalWidthWithMiniLegend, List<Beads> resBeads, int onlySchemeWidth, int onlySchemeHeight, ref float heightOfLogoInBead, ref float heightOfSmallImageInBead, ref float heightOfStringOfNameAndArticleInBead, ref float widthOfLogoInBead, ref float widthOfSmallImageInBead, bool isAddingBackAndKnots)
		{
			int num = (from r in resBeads
			select r.parentSeries.parentManufacturer.name).Distinct().Count();
			int num2 = 0;
			if (IsNeedToWriteSeriesName(false))
			{
				num2 = (from r in resBeads
				select r.parentSeries).Distinct().Count();
			}
			countRecordsInMiniLegend = resBeads.Count + num2 + num;
			if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
			{
				countRecordsInMiniLegend = resBeads.Count();
			}
			List<Beads> list = new List<Beads>();
			List<Beads> list2 = new List<Beads>();
			if (isAddingBackAndKnots)
			{
				if (backstitchHandEditing.Count() > 0)
				{
					list = (from back in backstitchHandEditing
					where !back.ptEnd.IsEmpty
					select back.bead).Distinct().ToList();
					countRecordsInMiniLegend += 1 + list.Count();
				}
				if (knotHandEditing.Count() > 0)
				{
					list2 = (from back in knotHandEditing
					select back.bead).Distinct().ToList();
					countRecordsInMiniLegend += 1 + list2.Count();
				}
			}
			int num3 = (int)Math.Round((double)(((float)(SqureCellSidePrintPages_Y + SqureCellSidePrintPages_X) + 0f) / 2f));
			heightOfLogoInBead = 0f;
			heightOfSmallImageInBead = 0f;
			heightOfStringOfNameAndArticleInBead = 0f;
			widthOfLogoInBead = 0f;
			widthOfSmallImageInBead = 0f;
			if (onlySchemeWidth > onlySchemeHeight)
			{
				if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina || Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
				{
					widthOfLogoInBead = 0f;
					if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						widthOfLogoInBead = (float)widthOfColumnInBadgesInMiniLegend;
						Bitmap bitmap = (!Settings.isItSpecialVersionOfBiserokForMaxim) ? new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/logoArtHobby.png") : new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/LogoMelnica.png");
						heightOfLogoInBead = ((float)bitmap.Height + 0f) / ((float)bitmap.Width + 0f) * widthOfLogoInBead;
						bitmap.Dispose();
						widthOfLogoInBead += 4f;
					}
					widthOfSmallImageInBead = (float)widthOfColumnInBadgesInMiniLegend * 0.8f;
					heightOfSmallImageInBead = ((float)BmpSourceImage.Height + 0f) / ((float)BmpSourceImage.Width + 0f) * widthOfSmallImageInBead;
					widthOfSmallImageInBead += 4f;
				}
				countColumnsInMiniLegend = (int)(((float)Param.WidthInBead - widthOfLogoInBead - widthOfSmallImageInBead) / (float)widthOfColumnInBadgesInMiniLegend);
				if (countColumnsInMiniLegend == 0)
				{
					countColumnsInMiniLegend = 1;
				}
				countRowsInMiniLegend = (int)Math.Ceiling((double)(((float)countRecordsInMiniLegend + 0f) / ((float)countColumnsInMiniLegend + 0f)));
				if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina && countRowsInMiniLegend < 8)
				{
					countRowsInMiniLegend = 8;
				}
				else if (Settings.isItSpecialVersionOfAlmazkaForElenaBartosova && countRowsInMiniLegend < 6)
				{
					countRowsInMiniLegend = 5;
				}
				int num4 = countRowsInMiniLegend * num3 * 2;
				if ((float)(2 * countRowsInMiniLegend) < heightOfLogoInBead)
				{
					countRowsInMiniLegend = (int)(heightOfLogoInBead / 2f);
					countColumnsInMiniLegend = (int)Math.Ceiling((double)(((float)countRecordsInMiniLegend + 0f) / ((float)countRowsInMiniLegend + 0f)));
				}
				additionalHeightWithMiniLegend = num4;
				additionalWidthWithMiniLegend = 0;
				if (Settings.isItSpecialVersionOfBiserokForMaxim)
				{
					heightOfStringOfNameAndArticleInBead = heightOfLogoInBead - heightOfSmallImageInBead;
				}
				else if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
				{
					heightOfStringOfNameAndArticleInBead = (float)(countRowsInMiniLegend * 2) - heightOfSmallImageInBead;
					if (heightOfSmallImageInBead / 2f > 5f)
					{
						heightOfSmallImageInBead = 10f;
						widthOfSmallImageInBead = ((float)BmpSourceImage.Width + 0f) / ((float)BmpSourceImage.Height + 0f) * heightOfSmallImageInBead;
						widthOfSmallImageInBead += 4f;
					}
				}
				else if (heightOfSmallImageInBead / 2f > 5f)
				{
					heightOfSmallImageInBead = 10f;
					widthOfSmallImageInBead = ((float)BmpSourceImage.Width + 0f) / ((float)BmpSourceImage.Height + 0f) * heightOfSmallImageInBead;
					widthOfSmallImageInBead += 4f;
				}
			}
			else
			{
				if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina || Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
				{
					if (Settings.isItSpecialVersionOfBiserokForMaxim || Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						widthOfLogoInBead = (float)widthOfColumnInBadgesInMiniLegend;
						Bitmap bitmap2 = (!Settings.isItSpecialVersionOfBiserokForMaxim) ? new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/logoArtHobby.png") : new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "/Additional/LogoMelnica.png");
						if (Settings.isItSpecialVersionOfBiserokForMaxim)
						{
							heightOfLogoInBead = ((float)bitmap2.Height + 80f) / ((float)bitmap2.Width + 0f) * widthOfLogoInBead;
						}
						else
						{
							heightOfLogoInBead = (float)bitmap2.Height / ((float)bitmap2.Width + 0f) * widthOfLogoInBead;
							heightOfLogoInBead += 2f;
						}
						bitmap2.Dispose();
					}
					widthOfSmallImageInBead = (float)widthOfColumnInBadgesInMiniLegend;
					heightOfSmallImageInBead = ((float)BmpSourceImage.Height + 0f) / ((float)BmpSourceImage.Width + 0f) * widthOfSmallImageInBead;
					heightOfStringOfNameAndArticleInBead = 13.6f;
					if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						heightOfStringOfNameAndArticleInBead += 2f;
					}
					if (Settings.isItSpecialVersionOfAlmazkaForElenaBartosova)
					{
						heightOfStringOfNameAndArticleInBead = 6f;
					}
				}
				countRowsInMiniLegend = (int)(((float)Param.HeightInBead - heightOfLogoInBead - heightOfSmallImageInBead - heightOfStringOfNameAndArticleInBead) / 2f);
				if (countRowsInMiniLegend == 0)
				{
					countRowsInMiniLegend = 1;
				}
				if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina && countRowsInMiniLegend < 6)
				{
					heightOfSmallImageInBead = ((float)Param.HeightInBead - heightOfLogoInBead - heightOfStringOfNameAndArticleInBead) / 2f - 6f;
					widthOfSmallImageInBead = ((float)BmpSourceImage.Width + 0f) / ((float)BmpSourceImage.Height + 0f) * heightOfSmallImageInBead;
					countRowsInMiniLegend = (int)(((float)Param.HeightInBead - heightOfLogoInBead - heightOfSmallImageInBead - heightOfStringOfNameAndArticleInBead) / 2f);
				}
				countColumnsInMiniLegend = (int)Math.Ceiling((double)(((float)countRecordsInMiniLegend + 0f) / ((float)countRowsInMiniLegend + 0f)));
				int num5 = countColumnsInMiniLegend * num3 * widthOfColumnInBadgesInMiniLegend;
				additionalHeightWithMiniLegend = 0;
				additionalWidthWithMiniLegend = num5;
			}
		}

		private Dictionary<Beads, int> CalculateCountOfEachBeadsInScheme(List<Beads> resBeads, IEnumerable<Block> bbb)
		{
			return (from bead in resBeads
			let count = bbb.Count(delegate(Block x)
			{
				if (x.IsVisible)
				{
					if (x.IsItFullStitch.Value && x.ResBeads == bead)
					{
						return true;
					}
					if ((!x.IsItFullStitch).Value)
					{
						if (x.leftTop != null && x.leftTop.typeOfStitch != TypeOfStitch.emptyCanvas && x.leftTop.ResBeads == bead)
						{
							goto IL_0121;
						}
						if (x.rightTop != null && x.rightTop.typeOfStitch != TypeOfStitch.emptyCanvas && x.rightTop.ResBeads == bead)
						{
							goto IL_0121;
						}
						if (x.rightBottom != null && x.rightBottom.typeOfStitch != TypeOfStitch.emptyCanvas && x.rightBottom.ResBeads == bead)
						{
							goto IL_0121;
						}
						if (x.leftBottom != null && x.leftBottom.typeOfStitch != TypeOfStitch.emptyCanvas)
						{
							return x.leftBottom.ResBeads == bead;
						}
						return false;
					}
					return false;
				}
				return false;
				IL_0121:
				return true;
			})
			where count > 0
			select new KeyValuePair<Beads, int>(bead, count)).ToDictionary((KeyValuePair<Beads, int> x) => x.Key, (KeyValuePair<Beads, int> y) => y.Value);
		}

		private void GenerateMainScheme(SavingOrPrintingSettings settings, Bitmap scheme, Block[,] blocks, List<Beads> resBeads, int dTop, int dLeft, int dBottom, int dRight, int countRowsInMiniLegend, int countColumnsInMiniLegend, int countRecordsInMiniLegend, Point ptBeginMiniLegend, Dictionary<Beads, int> countOfEachBeads)
		{
			SolidBrush brush = new SolidBrush(Color.Black);
			Graphics graphics = Graphics.FromImage(scheme);
			graphics.FillRectangle(Brushes.White, 0, 0, scheme.Width, scheme.Height);
			if (Param.IsSeparatingLinesOnScheme)
			{
				graphics.FillRectangle(brush, dLeft, dTop, scheme.Width - dLeft - dRight, scheme.Height - dTop - dBottom);
			}
			int num = 5;
			graphics.DrawImage(BmpSourceImage, new Rectangle(num + dLeft, num + dTop, scheme.Width - 2 * num - dLeft - dRight, scheme.Height - 2 * num - dTop - dBottom), new Rectangle(0, 0, BmpSourceImage.Width, BmpSourceImage.Height), GraphicsUnit.Pixel);
			graphics.Dispose();
			bool isSchemeBlockType = Param.Type == SchemeType.Block;
			int _SqureCellSidePrintPages_X = SqureCellSidePrintPages_X;
			Parallel.For(0, Param.HeightInBead, new ParallelOptions
			{
				MaxDegreeOfParallelism = Environment.ProcessorCount
			}, delegate(int i)
			{
				int num25 = Param.WidthInBead;
				if (isSchemeBlockType && i % 2 != 0)
				{
					num25--;
				}
				try
				{
					Graphics graphics2 = default(Graphics);
					lock (scheme)
					{
						graphics2 = Graphics.FromImage(scheme);
					}
					for (int num26 = 0; num26 < num25; num26++)
					{
						Block block = blocks[num26, i];
						Color white = Color.White;
						SolidBrush brush5 = Param.IsColorScheme_OrForGrayPrint ? block.ResBeads.brush : new SolidBrush(white);
						int num27 = 1;
						if (!Param.IsSeparatingLinesOnScheme)
						{
							num27 = 0;
						}
						if (isSchemeBlockType && i % 2 != 0)
						{
							if (block.IsVisible)
							{
								if (Param.IsSeparatingLinesOnScheme)
								{
									graphics2.DrawRectangle(new Pen(Color.Black, 1f), new Rectangle(4 + num26 * _SqureCellSidePrintPages_X + _SqureCellSidePrintPages_X / 2 + dLeft, 4 + SqureCellSidePrintPages_Y * i + dTop, _SqureCellSidePrintPages_X - 1 + 1, SqureCellSidePrintPages_Y - 1 + 1));
								}
								bool flag = true;
								if (v > 0 || (i < 90 && num26 < 60))
								{
									flag = true;
								}
								else if ((i / 10 + num26 / 10) % 2 == 0)
								{
									flag = false;
								}
								if (flag)
								{
									graphics2.FillRectangle(brush5, new Rectangle(5 + num26 * _SqureCellSidePrintPages_X + _SqureCellSidePrintPages_X / 2 + dLeft, 5 + SqureCellSidePrintPages_Y * i + dTop, _SqureCellSidePrintPages_X - num27, SqureCellSidePrintPages_Y - num27));
									float num28 = 4f * (float)SqureCellSidePrintPages_Y / 20f;
									float num29 = ((float)(_SqureCellSidePrintPages_X - SqureCellSidePrintPages_Y) + 0f) / 2f;
									float x = 4f + num28 + num29 + (float)(num26 * _SqureCellSidePrintPages_X) + (float)(_SqureCellSidePrintPages_X / 2) + (float)dLeft;
									float y = 4f + num28 + (float)(SqureCellSidePrintPages_Y * i) + (float)dTop;
									float num30 = (float)SqureCellSidePrintPages_Y - 2f * num28;
									float width2 = num30;
									lock (block.ResBeads.badge)
									{
										graphics2.DrawImage(block.ResBeads.badge, new RectangleF(x, y, width2, num30));
									}
								}
								else
								{
									graphics2.FillRectangle(new SolidBrush(Color.White), new Rectangle(5 + num26 * _SqureCellSidePrintPages_X + _SqureCellSidePrintPages_X / 2 + dLeft, 5 + SqureCellSidePrintPages_Y * i + dTop, _SqureCellSidePrintPages_X - 1, SqureCellSidePrintPages_Y - 1));
								}
							}
						}
						else if (block.IsVisible)
						{
							if (Param.IsSeparatingLinesOnScheme)
							{
								graphics2.DrawRectangle(new Pen(Color.Black, 1f), new Rectangle(4 + num26 * _SqureCellSidePrintPages_X + dLeft, 4 + SqureCellSidePrintPages_Y * i + dTop, _SqureCellSidePrintPages_X, SqureCellSidePrintPages_Y));
							}
							bool flag2 = true;
							if (v > 0 || (i < 90 && num26 < 60))
							{
								flag2 = true;
							}
							else if ((i / 10 + num26 / 10) % 2 == 0)
							{
								flag2 = false;
							}
							if (flag2)
							{
								Rectangle rect3 = new Rectangle(5 + num26 * _SqureCellSidePrintPages_X + dLeft, 5 + SqureCellSidePrintPages_Y * i + dTop, _SqureCellSidePrintPages_X - num27, SqureCellSidePrintPages_Y - num27);
								if (block.IsItFullStitch.Value)
								{
									graphics2.FillRectangle(brush5, rect3);
									float num31 = 4f * (float)SqureCellSidePrintPages_Y / 20f;
									float num32 = ((float)(_SqureCellSidePrintPages_X - SqureCellSidePrintPages_Y) + 0f) / 2f;
									float x2 = 4f + num31 + num32 + (float)(num26 * _SqureCellSidePrintPages_X) + (float)dLeft;
									float y2 = 4f + num31 + (float)(SqureCellSidePrintPages_Y * i) + (float)dTop;
									float num33 = (float)SqureCellSidePrintPages_Y - 2f * num31;
									float width3 = num33;
									if (block.ResBeads.badge != null)
									{
										lock (block.ResBeads.badge)
										{
											graphics2.DrawImage(block.ResBeads.badge, new RectangleF(x2, y2, width3, num33));
										}
									}
								}
								else
								{
									graphics2.FillRectangle(Brushes.White, rect3);
									Pen pen = new Pen(Brushes.Black, 1f);
									rect3 = new Rectangle(4 + num26 * _SqureCellSidePrintPages_X + dLeft, 4 + SqureCellSidePrintPages_Y * i + dTop, _SqureCellSidePrintPages_X - num27, SqureCellSidePrintPages_Y - num27);
									if (block.leftTop != null && block.leftTop.typeOfStitch != TypeOfStitch.emptyCanvas)
									{
										if (block.leftTop.typeOfStitch == TypeOfStitch.half)
										{
											graphics2.FillRectangle(block.leftTop.ResBeads.brush, new Rectangle(rect3.X, rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.DrawRectangle(pen, new Rectangle(rect3.X, rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.FillRectangle(block.leftTop.ResBeads.brush, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.DrawRectangle(pen, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											if (block.leftTop.ResBeads.badge != null)
											{
												lock (block.leftTop.ResBeads.badge)
												{
													graphics2.DrawImage(block.leftTop.ResBeads.badge, new Rectangle(rect3.X, rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
													graphics2.DrawImage(block.leftTop.ResBeads.badge, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
												}
											}
										}
										else if (block.leftTop.typeOfStitch == TypeOfStitch.full_quarter || block.leftTop.typeOfStitch == TypeOfStitch.half_quarter)
										{
											graphics2.FillRectangle(block.leftTop.ResBeads.brush, new Rectangle(rect3.X, rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.DrawRectangle(pen, new Rectangle(rect3.X, rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											if (block.leftTop.ResBeads.badge != null)
											{
												lock (block.leftTop.ResBeads.badge)
												{
													graphics2.DrawImage(block.leftTop.ResBeads.badge, new Rectangle(rect3.X, rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
												}
											}
										}
										else if (block.leftTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											List<PointF> list3 = new List<PointF>
											{
												new PointF((float)rect3.X, (float)rect3.Y),
												new PointF((float)(rect3.X + rect3.Width), (float)rect3.Y),
												new PointF((float)rect3.X, (float)(rect3.Y + rect3.Height))
											};
											graphics2.FillPolygon(block.leftTop.ResBeads.brush, list3.ToArray());
											graphics2.DrawPolygon(pen, list3.ToArray());
											if (block.leftTop.ResBeads.badge != null)
											{
												lock (block.leftTop.ResBeads.badge)
												{
													graphics2.DrawImage(block.leftTop.ResBeads.badge, new Rectangle(rect3.X, rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
												}
											}
										}
									}
									if (block.rightTop != null && block.rightTop.typeOfStitch != TypeOfStitch.emptyCanvas)
									{
										if (block.rightTop.typeOfStitch == TypeOfStitch.half)
										{
											graphics2.FillRectangle(block.rightTop.ResBeads.brush, new Rectangle(rect3.X, rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.DrawRectangle(pen, new Rectangle(rect3.X, rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.FillRectangle(block.rightTop.ResBeads.brush, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.DrawRectangle(pen, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											if (block.rightTop.ResBeads.badge != null)
											{
												lock (block.rightTop.ResBeads.badge)
												{
													graphics2.DrawImage(block.rightTop.ResBeads.badge, new Rectangle(rect3.X, rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
													graphics2.DrawImage(block.rightTop.ResBeads.badge, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
												}
											}
										}
										else if (block.rightTop.typeOfStitch == TypeOfStitch.full_quarter || block.rightTop.typeOfStitch == TypeOfStitch.half_quarter)
										{
											graphics2.FillRectangle(block.rightTop.ResBeads.brush, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.DrawRectangle(pen, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											if (block.rightTop.ResBeads.badge != null)
											{
												lock (block.rightTop.ResBeads.badge)
												{
													graphics2.DrawImage(block.rightTop.ResBeads.badge, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
												}
											}
										}
										else if (block.rightTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											List<PointF> list4 = new List<PointF>
											{
												new PointF((float)rect3.X, (float)rect3.Y),
												new PointF((float)(rect3.X + rect3.Width + 1), (float)rect3.Y),
												new PointF((float)(rect3.X + rect3.Width + 1), (float)(rect3.Y + rect3.Height))
											};
											graphics2.FillPolygon(block.rightTop.ResBeads.brush, list4.ToArray());
											graphics2.DrawPolygon(pen, list4.ToArray());
											if (block.rightTop.ResBeads.badge != null)
											{
												lock (block.rightTop.ResBeads.badge)
												{
													graphics2.DrawImage(block.rightTop.ResBeads.badge, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
												}
											}
										}
									}
									if (block.rightBottom != null && block.rightBottom.typeOfStitch != TypeOfStitch.emptyCanvas)
									{
										if (block.rightBottom.typeOfStitch == TypeOfStitch.half)
										{
											graphics2.FillRectangle(block.rightBottom.ResBeads.brush, new Rectangle(rect3.X, rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.DrawRectangle(pen, new Rectangle(rect3.X, rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.FillRectangle(block.rightBottom.ResBeads.brush, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.DrawRectangle(pen, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											if (block.rightBottom.ResBeads.badge != null)
											{
												lock (block.rightBottom.ResBeads.badge)
												{
													graphics2.DrawImage(block.rightBottom.ResBeads.badge, new Rectangle(rect3.X, rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
													graphics2.DrawImage(block.rightBottom.ResBeads.badge, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
												}
											}
										}
										else if (block.rightBottom.typeOfStitch == TypeOfStitch.full_quarter || block.rightBottom.typeOfStitch == TypeOfStitch.half_quarter)
										{
											graphics2.FillRectangle(block.rightBottom.ResBeads.brush, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.DrawRectangle(pen, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											if (block.rightBottom.ResBeads.badge != null)
											{
												lock (block.rightBottom.ResBeads.badge)
												{
													graphics2.DrawImage(block.rightBottom.ResBeads.badge, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
												}
											}
										}
										else if (block.rightBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											List<PointF> list5 = new List<PointF>
											{
												new PointF((float)(rect3.X + rect3.Width + 1), (float)rect3.Y),
												new PointF((float)(rect3.X + rect3.Width + 1), (float)(rect3.Y + rect3.Height + 1)),
												new PointF((float)rect3.X, (float)(rect3.Y + rect3.Height + 1))
											};
											graphics2.FillPolygon(block.rightBottom.ResBeads.brush, list5.ToArray());
											graphics2.DrawPolygon(pen, list5.ToArray());
											if (block.rightBottom.ResBeads.badge != null)
											{
												lock (block.rightBottom.ResBeads.badge)
												{
													graphics2.DrawImage(block.rightBottom.ResBeads.badge, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
												}
											}
										}
									}
									if (block.leftBottom != null && block.leftBottom.typeOfStitch != TypeOfStitch.emptyCanvas)
									{
										if (block.leftBottom.typeOfStitch == TypeOfStitch.half)
										{
											graphics2.FillRectangle(block.leftBottom.ResBeads.brush, new Rectangle(rect3.X, rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.DrawRectangle(pen, new Rectangle(rect3.X, rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.FillRectangle(block.leftBottom.ResBeads.brush, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.DrawRectangle(pen, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											if (block.leftBottom.ResBeads.badge != null)
											{
												lock (block.leftBottom.ResBeads.badge)
												{
													graphics2.DrawImage(block.leftBottom.ResBeads.badge, new Rectangle(rect3.X, rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
													graphics2.DrawImage(block.leftBottom.ResBeads.badge, new Rectangle(rect3.X + (int)Math.Round((double)((float)rect3.Width / 2f)), rect3.Y, (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
												}
											}
										}
										else if (block.leftBottom.typeOfStitch == TypeOfStitch.full_quarter || block.leftBottom.typeOfStitch == TypeOfStitch.half_quarter)
										{
											graphics2.FillRectangle(block.leftBottom.ResBeads.brush, new Rectangle(rect3.X, rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											graphics2.DrawRectangle(pen, new Rectangle(rect3.X, rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
											if (block.leftBottom.ResBeads.badge != null)
											{
												lock (block.leftBottom.ResBeads.badge)
												{
													graphics2.DrawImage(block.leftBottom.ResBeads.badge, new Rectangle(rect3.X, rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
												}
											}
										}
										else if (block.leftBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											List<PointF> list6 = new List<PointF>
											{
												new PointF((float)rect3.X, (float)rect3.Y),
												new PointF((float)(rect3.X + rect3.Width + 1), (float)(rect3.Y + rect3.Height + 1)),
												new PointF((float)rect3.X, (float)(rect3.Y + rect3.Height + 1))
											};
											graphics2.FillPolygon(block.leftBottom.ResBeads.brush, list6.ToArray());
											graphics2.DrawPolygon(pen, list6.ToArray());
											if (block.leftBottom.ResBeads.badge != null)
											{
												lock (block.leftBottom.ResBeads.badge)
												{
													graphics2.DrawImage(block.leftBottom.ResBeads.badge, new Rectangle(rect3.X, rect3.Y + (int)Math.Round((double)((float)rect3.Height / 2f)), (int)Math.Round((double)((float)rect3.Width / 2f)), (int)Math.Round((double)((float)rect3.Height / 2f))));
												}
											}
										}
									}
								}
							}
							else
							{
								graphics2.FillRectangle(new SolidBrush(Color.White), new Rectangle(5 + num26 * _SqureCellSidePrintPages_X + dLeft, 5 + SqureCellSidePrintPages_Y * i + dTop, _SqureCellSidePrintPages_X - 1, SqureCellSidePrintPages_Y - 1));
							}
						}
					}
					graphics2.Dispose();
				}
				catch (Exception exc)
				{
					Site.SendExceptionInfo(exc, false);
				}
			});
			graphics = Graphics.FromImage(scheme);
			if (Param.IsSeparatingLinesOnScheme)
			{
				for (int j = 0; j <= Param.HeightInBead / 10; j++)
				{
					graphics.DrawLine(new Pen(brush, 3f), new PointF((float)dLeft, (float)(3 + j * SqureCellSidePrintPages_Y * 10 + dTop)), new PointF((float)(scheme.Width - dRight), (float)(3 + j * SqureCellSidePrintPages_Y * 10 + dTop)));
				}
				if (Param.Type == SchemeType.Normal)
				{
					for (int k = 0; k <= Param.WidthInBead / 10; k++)
					{
						graphics.DrawLine(new Pen(brush, 3f), new PointF((float)(3 + k * SqureCellSidePrintPages_X * 10 + dLeft), (float)dTop), new PointF((float)(3 + k * SqureCellSidePrintPages_X * 10 + dLeft), (float)(scheme.Height - dBottom)));
					}
				}
				else
				{
					for (int l = 0; l <= Param.WidthInBead / 10; l++)
					{
						for (int m = 0; m < Param.HeightInBead; m++)
						{
							int num2 = 2 + l * SqureCellSidePrintPages_X * 10;
							if (m % 2 != 0)
							{
								num2 += SqureCellSidePrintPages_X / 2;
							}
							graphics.DrawLine(new Pen(brush, 3f), new PointF((float)(num2 + dLeft), (float)(4 + m * SqureCellSidePrintPages_Y + dTop)), new PointF((float)(num2 + dLeft), (float)(4 + (m + 1) * SqureCellSidePrintPages_Y + dTop)));
						}
					}
				}
			}
			if (Settings.hobbyProgramm != HobbyProgramm.Almaz)
			{
				int heightPageInBeads = Param.heightPageInBeads;
				int widthPageInBeads = Param.widthPageInBeads;
				if (Param.IsSeparatingLinesOnScheme)
				{
					for (int n = 0; n <= Param.HeightInBead / heightPageInBeads; n++)
					{
						graphics.DrawLine(new Pen(brush, 5f), new PointF((float)dLeft, (float)(3 + n * heightPageInBeads * SqureCellSidePrintPages_Y + dTop)), new PointF((float)(scheme.Width - dRight), (float)(3 + n * heightPageInBeads * SqureCellSidePrintPages_Y + dTop)));
					}
					for (int num3 = 0; num3 <= Param.WidthInBead / widthPageInBeads; num3++)
					{
						if (Param.Type == SchemeType.Normal)
						{
							graphics.DrawLine(new Pen(brush, 5f), new PointF((float)(3 + num3 * widthPageInBeads * SqureCellSidePrintPages_X + dLeft), (float)dTop), new PointF((float)(3 + num3 * widthPageInBeads * SqureCellSidePrintPages_X + dLeft), (float)(scheme.Height - dBottom)));
						}
						else
						{
							for (int num4 = 0; num4 < Param.HeightInBead; num4++)
							{
								int num2 = 4 + num3 * widthPageInBeads * SqureCellSidePrintPages_X;
								if (num4 % 2 != 0)
								{
									num2 += SqureCellSidePrintPages_X / 2;
								}
								graphics.DrawLine(new Pen(brush, 5f), new PointF((float)(num2 + dLeft), (float)(4 + num4 * SqureCellSidePrintPages_Y + dTop)), new PointF((float)(num2 + dLeft), (float)(4 + (num4 + 1) * SqureCellSidePrintPages_Y + dTop)));
							}
						}
					}
				}
			}
			if (Settings.hobbyProgramm == HobbyProgramm.Krestik && settings.numberOfTab == 3)
			{
				graphics.SmoothingMode = SmoothingMode.AntiAlias;
				int num5 = (int)Math.Round((double)((float)SqureCellSidePrintPages_X * 0.85f));
				float width = (float)SqureCellSidePrintPages_X * 0.28f;
				foreach (BackStitch item in backstitchHandEditing)
				{
					if (!item.ptEnd.IsEmpty)
					{
						graphics.DrawLine(new Pen(new SolidBrush(item.bead.clr), width), (float)(5 + dLeft) + item.ptBegin.X * (float)SqureCellSidePrintPages_X, (float)(5 + dTop) + item.ptBegin.Y * (float)SqureCellSidePrintPages_X, (float)(5 + dLeft) + item.ptEnd.X * (float)SqureCellSidePrintPages_X, (float)(5 + dTop) + item.ptEnd.Y * (float)SqureCellSidePrintPages_X);
					}
				}
				foreach (Knot item2 in knotHandEditing)
				{
					Rectangle rect = new Rectangle(5 + dLeft + (int)(item2.point.X * (float)SqureCellSidePrintPages_X - ((float)num5 + 0f) / 2f), 5 + dTop + (int)(item2.point.Y * (float)SqureCellSidePrintPages_X - ((float)num5 + 0f) / 2f), num5, num5);
					graphics.DrawEllipse(Pens.Black, (float)rect.X - 0.5f, (float)rect.Y - 0.5f, (float)rect.Width + 1f, (float)rect.Height + 1f);
					graphics.FillEllipse(new SolidBrush(item2.bead.clr), rect);
				}
				graphics.SmoothingMode = SmoothingMode.Default;
			}
			if (Param.BiserType == BiserType.ForManufacturers && Param.IsDrawSmallLegend)
			{
				float num6 = Param.WidthOfBorderOfBeadsInPercentToBeadsRadius * ((float)SqureCellSidePrintPages_Y + 0f) / 200f;
				BeadsManufacturer beadsManufacturer = new BeadsManufacturer();
				BeadsSeries beadsSeries = new BeadsSeries();
				int num7 = 0;
				int num8 = 0;
				SolidBrush brush2 = new SolidBrush(Color.Black);
				int num9 = 2 * SqureCellSidePrintPages_Y;
				int num10 = widthOfColumnInBadgesInMiniLegend * SqureCellSidePrintPages_X;
				int num11 = (int)Math.Round((double)((float)SqureCellSidePrintPages_Y * 1.5f));
				int num12 = (int)Math.Round((double)((float)SqureCellSidePrintPages_X * 1.5f));
				int num13 = (int)Math.Round((double)((float)SqureCellSidePrintPages_Y * 0.9f));
				int num14 = (int)Math.Round((double)((float)SqureCellSidePrintPages_X * 0.9f));
				int num15 = (int)Math.Round((double)((float)(num12 - num14) / 2f));
				float emSize = 0.75f * (float)(num11 + num12) / 2f * KfSystemFontSizes;
				Font font = new Font("Verdana", emSize);
				Font font2 = new Font("Verdana", emSize, FontStyle.Italic);
				Font font3 = new Font("Verdana", emSize, FontStyle.Bold);
				List<Beads> list = new List<Beads>();
				List<Beads> list2 = new List<Beads>();
				if (Settings.hobbyProgramm == HobbyProgramm.Krestik && settings.numberOfTab == 3)
				{
					if (backstitchHandEditing.Count() > 0)
					{
						list = (from back in backstitchHandEditing
						where !back.ptEnd.IsEmpty
						select back.bead).Distinct().ToList();
					}
					if (knotHandEditing.Count() > 0)
					{
						list2 = (from back in knotHandEditing
						select back.bead).Distinct().ToList();
					}
				}
				for (int num16 = 0; num16 < countColumnsInMiniLegend; num16++)
				{
					int num17 = (num16 + 1) * countRowsInMiniLegend;
					if (num16 >= countRecordsInMiniLegend / countRowsInMiniLegend)
					{
						num17 = countRecordsInMiniLegend;
					}
					for (int num18 = num16 * countRowsInMiniLegend; num18 < num17; num18++)
					{
						int num19 = num18 - num7;
						if (num19 < resBeads.Count())
						{
							if (beadsManufacturer != resBeads[num19].parentSeries.parentManufacturer)
							{
								num7++;
								beadsManufacturer = resBeads[num19].parentSeries.parentManufacturer;
								string s = (v == 0) ? ((Settings.hobbyProgramm != 0) ? "Бисер *** (демо-версия)" : "Мулине *** (демо-версия)") : beadsManufacturer.name;
								graphics.DrawString(s, font3, brush2, new PointF((float)(ptBeginMiniLegend.X + num16 * num10 - num12), (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y) - 0.3f * (float)num11));
							}
							else
							{
								SolidBrush brush3 = Param.IsColorScheme_OrForGrayPrint ? new SolidBrush(resBeads[num19].clr) : new SolidBrush(Color.White);
								Rectangle rect2 = new Rectangle(ptBeginMiniLegend.X + num16 * num10, (num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y, num12, num11);
								graphics.DrawRectangle(new Pen(new SolidBrush(Color.Black)), rect2.X - 1, rect2.Y - 1, rect2.Width + 1, rect2.Height + 1);
								graphics.FillRectangle(brush3, rect2);
								graphics.DrawImage(resBeads[num19].badge, new RectangleF((float)(ptBeginMiniLegend.X + num16 * num10 + num15), (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y + num15), (float)num14, (float)num13));
								SolidBrush brush4 = new SolidBrush(resBeads[num19].clr);
								float num20 = 1.5f;
								if (Param.IsColorScheme_OrForGrayPrint)
								{
									graphics.FillRectangle(brush4, new RectangleF((float)(ptBeginMiniLegend.X + num16 * num10) + (float)num12 * 1.2f, (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y), (float)num12 * 2.5f, (float)num11));
									num20 += 2.3f;
								}
								string text = resBeads[num19].name;
								if (v == 0)
								{
									text = ((num19 >= 14) ? "*****" : ("****" + text.Last().ToString()));
								}
								graphics.DrawString(text, font, brush2, new PointF((float)(ptBeginMiniLegend.X + num16 * num10) + (float)num12 * num20, (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y) - (float)num11 * 0.15f));
								if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
								{
									try
									{
										num8 = countOfEachBeads[resBeads[num19]];
									}
									catch
									{
										num8 = 0;
									}
									if (num8 > 0)
									{
										float num21 = ((float)Param.countOfBeadsIn1000Gramm + 0f) / 1000f;
										int num22 = (int)Math.Round((double)(((float)num8 + 0f) / num21)) + 1;
										string s2 = "- " + num8.ToString() + "шт., " + num22.ToString() + "гр.";
										if (v == 0 && num19 > 13)
										{
											s2 = "- демо-версия";
										}
										graphics.DrawString(s2, font2, Brushes.Black, new PointF((float)(ptBeginMiniLegend.X + num16 * num10) + (float)num12 * (num20 + 3.7f), (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y) - (float)num11 * 0.15f));
									}
									else
									{
										string stringWithInfoAboutNotNeededInThisBeads = GetStringWithInfoAboutNotNeededInThisBeads(true);
										graphics.DrawString(stringWithInfoAboutNotNeededInThisBeads, font2, Brushes.Black, new PointF((float)(ptBeginMiniLegend.X + num16 * num10) + (float)num12 * (num20 + 3.7f), (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y) - (float)num11 * 0.15f));
									}
								}
							}
						}
						else
						{
							float num23 = 1.5f;
							if (Param.IsColorScheme_OrForGrayPrint)
							{
								num23 += 2.3f;
							}
							if (num19 == resBeads.Count())
							{
								if (list.Count() > 0)
								{
									graphics.DrawString("Бэкстич:", font3, brush2, new PointF((float)(ptBeginMiniLegend.X + num16 * num10 - num12), (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y) - (float)num11 * 0.15f));
								}
								else if (list2.Count() > 0)
								{
									graphics.DrawString("Франц.узелки:", font3, brush2, new PointF((float)(ptBeginMiniLegend.X + num16 * num10 - num12), (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y) - (float)num11 * 0.15f));
								}
							}
							else if (list.Count() > 0)
							{
								if (num19 < resBeads.Count() + 1 + list.Count())
								{
									int index = resBeads.Count() + 1 + list.Count() - num19 - 1;
									SolidBrush brush4 = new SolidBrush(list[index].clr);
									if (Param.IsColorScheme_OrForGrayPrint)
									{
										graphics.FillRectangle(brush4, new RectangleF((float)(ptBeginMiniLegend.X + num16 * num10) + (float)num12 * 1.2f - 40f, (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y), (float)num12 * 2.5f, (float)num11));
									}
									string text2 = list[index].parentSeries.parentManufacturer.name;
									if (text2.Contains("ПНК"))
									{
										text2 = "ПНК";
									}
									text2 = text2 + " " + list[index].name;
									graphics.DrawString(text2, font, brush2, new PointF((float)(ptBeginMiniLegend.X + num16 * num10) + (float)num12 * num23 - 40f, (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y) - (float)num11 * 0.15f));
								}
								else if (num19 == resBeads.Count() + 1 + list.Count())
								{
									graphics.DrawString("Франц.узелки:", font3, brush2, new PointF((float)(ptBeginMiniLegend.X + num16 * num10 - num12), (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y) - (float)num11 * 0.15f));
								}
								else
								{
									int index2 = resBeads.Count() + 1 + list.Count() + 1 + list2.Count() - num19 - 1;
									SolidBrush brush4 = new SolidBrush(list2[index2].clr);
									if (Param.IsColorScheme_OrForGrayPrint)
									{
										graphics.FillRectangle(brush4, new RectangleF((float)(ptBeginMiniLegend.X + num16 * num10) + (float)num12 * 1.2f - 40f, (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y), (float)num12 * 2.5f, (float)num11));
									}
									string text3 = list2[index2].parentSeries.parentManufacturer.name;
									if (text3.Contains("ПНК"))
									{
										text3 = "ПНК";
									}
									text3 = text3 + " " + list2[index2].name;
									graphics.DrawString(text3, font, brush2, new PointF((float)(ptBeginMiniLegend.X + num16 * num10) + (float)num12 * num23 - 40f, (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y) - (float)num11 * 0.15f));
								}
							}
							else if (list2.Count() > 0)
							{
								int index3 = resBeads.Count() + list2.Count() - num19;
								SolidBrush brush4 = new SolidBrush(list2[index3].clr);
								if (Param.IsColorScheme_OrForGrayPrint)
								{
									graphics.FillRectangle(brush4, new RectangleF((float)(ptBeginMiniLegend.X + num16 * num10) + (float)num12 * 1.2f - 40f, (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y), (float)num12 * 2.5f, (float)num11));
								}
								string text4 = list2[index3].parentSeries.parentManufacturer.name;
								if (text4.Contains("ПНК"))
								{
									text4 = "ПНК";
								}
								text4 = text4 + " " + list2[index3].name;
								graphics.DrawString(text4, font, brush2, new PointF((float)(ptBeginMiniLegend.X + num16 * num10) + (float)num12 * num23 - 40f, (float)((num18 - num16 * countRowsInMiniLegend) * num9 + ptBeginMiniLegend.Y) - (float)num11 * 0.15f));
							}
							num19++;
						}
					}
				}
			}
			if (v == 0)
			{
				int num24 = 15;
				float emSize2 = (float)(scheme.Width / 15) * KfSystemFontSizes;
				graphics.DrawString("Демо-версия", new Font("Verdana", emSize2), new SolidBrush(Color.Red), new PointF((float)(10 + dLeft), (float)(num24 + dTop)));
				graphics.DrawString("Демо-версия", new Font("Verdana", emSize2), new SolidBrush(Color.Red), new PointF((float)scheme.Width / 3.3f + (float)dLeft, (float)(scheme.Height / 4 + num24 + dTop)));
				graphics.DrawString("Демо-версия", new Font("Verdana", emSize2), new SolidBrush(Color.Red), new PointF((float)(10 + dLeft), (float)(scheme.Height / 4 * 2 + num24 + dTop)));
				graphics.DrawString("Демо-версия", new Font("Verdana", emSize2), new SolidBrush(Color.Red), new PointF((float)scheme.Width / 3.3f + (float)dLeft, (float)scheme.Height * 3f / 4f + (float)num24 + (float)dTop));
			}
			graphics.Dispose();
		}

		private string GetStringWithInfoAboutNotNeededInThisBeads(bool isItMiniLegend)
		{
			string result = "";
			if (isItMiniLegend)
			{
				if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
				{
					result = " не нужно!";
				}
				else if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
				{
					result = " бисер не нужен!";
				}
				else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
				{
					result = " мулине не нужно!";
				}
				else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
				{
					result = " не нужно!";
					if (Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina)
					{
						result = "-";
					}
				}
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
			{
				result = "эта пряжа не нужна!";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
			{
				result = "этот бисер не нужен!";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				result = "это мулине не нужно!";
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
			{
				result = "этот бисер/стразы не нужны!";
			}
			return result;
		}

		private Bitmap DrawBadges(int badgeNumber)
		{
			int wayOfBadgesCreating = Param.wayOfBadgesCreating;
			float emSize = 36f;
			Bitmap bitmap;
			int num;
			switch (wayOfBadgesCreating)
			{
			case 1:
				if (badgeNumber < 60)
				{
					bitmap = new Bitmap(sizeOfBadge, sizeOfBadge);
					Graphics graphics2 = Graphics.FromImage(bitmap);
					graphics2.DrawString(GetCharForBadges(badgeNumber), new Font("Verdana", emSize, GraphicsUnit.Pixel), Brushes.Black, new RectangleF(0f, 0f, (float)sizeOfBadge, (float)sizeOfBadge), new StringFormat
					{
						Alignment = StringAlignment.Center,
						LineAlignment = StringAlignment.Center
					});
				}
				else
				{
					string baseDirectory2 = AppDomain.CurrentDomain.BaseDirectory;
					num = badgeNumber - 59;
					bitmap = new Bitmap($"{baseDirectory2}Badges\\{num.ToString()}.png");
				}
				break;
			case 2:
				if (badgeNumber < 123)
				{
					string baseDirectory3 = AppDomain.CurrentDomain.BaseDirectory;
					num = badgeNumber + 1;
					bitmap = new Bitmap($"{baseDirectory3}Badges\\{num.ToString()}.png");
				}
				else
				{
					bitmap = new Bitmap(sizeOfBadge, sizeOfBadge);
				}
				break;
			case 3:
				if (badgeNumber < 41)
				{
					bitmap = new Bitmap(sizeOfBadge, sizeOfBadge);
					Graphics graphics = Graphics.FromImage(bitmap);
					graphics.DrawString(GetCharForBadges(badgeNumber), new Font("Verdana", emSize, GraphicsUnit.Pixel), Brushes.Black, new RectangleF(0f, 0f, (float)sizeOfBadge, (float)sizeOfBadge), new StringFormat
					{
						Alignment = StringAlignment.Center,
						LineAlignment = StringAlignment.Center
					});
				}
				else
				{
					string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
					num = badgeNumber - 40;
					bitmap = new Bitmap($"{baseDirectory}Badges\\{num.ToString()}.png");
				}
				break;
			default:
				bitmap = new Bitmap(sizeOfBadge, sizeOfBadge);
				break;
			}
			return bitmap;
		}

		private string GetCharForBadges(int badgeNumber)
		{
			int num;
			switch (Param.wayOfBadgesCreating)
			{
			case 1:
				if (badgeNumber < 9)
				{
					num = badgeNumber + 1;
					return num.ToString();
				}
				badgeNumber++;
				switch (badgeNumber)
				{
				case 10:
					return "А";
				case 11:
					return "Б";
				case 12:
					return "В";
				case 13:
					return "Г";
				case 14:
					return "Д";
				case 15:
					return "Е";
				case 16:
					return "Ж";
				case 17:
					return "И";
				case 18:
					return "К";
				case 19:
					return "Л";
				case 20:
					return "М";
				case 21:
					return "Н";
				case 22:
					return "О";
				case 23:
					return "П";
				case 24:
					return "Р";
				case 25:
					return "С";
				case 26:
					return "Т";
				case 27:
					return "У";
				case 28:
					return "Ф";
				case 29:
					return "Х";
				case 30:
					return "Ц";
				case 31:
					return "Ч";
				case 32:
					return "Ш";
				case 33:
					return "Ы";
				case 34:
					return "Э";
				case 35:
					return "Ю";
				case 36:
					return "Я";
				case 37:
					return "R";
				case 38:
					return "W";
				case 39:
					return "U";
				case 40:
					return "I";
				case 41:
					return "S";
				case 42:
					return "F";
				case 43:
					return "G";
				case 44:
					return "J";
				case 45:
					return "L";
				case 46:
					return "Z";
				case 47:
					return "V";
				case 48:
					return "+";
				case 49:
					return "-";
				case 50:
					return "/";
				case 51:
					return "*";
				case 52:
					return "&";
				case 53:
					return "~";
				case 54:
					return "@";
				case 55:
					return "#";
				case 56:
					return "%";
				case 57:
					return "^";
				case 58:
					return "=";
				case 59:
					return "?";
				case 60:
					return "<";
				}
				break;
			case 2:
				return "";
			case 3:
				if (badgeNumber < 9)
				{
					num = badgeNumber + 1;
					return num.ToString();
				}
				badgeNumber++;
				switch (badgeNumber)
				{
				case 10:
					return "A";
				case 11:
					return "C";
				case 12:
					return "E";
				case 13:
					return "H";
				case 14:
					return "J";
				case 15:
					return "K";
				case 16:
					return "L";
				case 17:
					return "M";
				case 18:
					return "N";
				case 19:
					return "O";
				case 20:
					return "P";
				case 21:
					return "R";
				case 22:
					return "S";
				case 23:
					return "T";
				case 24:
					return "U";
				case 25:
					return "W";
				case 26:
					return "X";
				case 27:
					return "Y";
				case 28:
					return "Z";
				case 29:
					return "+";
				case 30:
					return "-";
				case 31:
					return "/";
				case 32:
					return "*";
				case 33:
					return "&";
				case 34:
					return "~";
				case 35:
					return "@";
				case 36:
					return "#";
				case 37:
					return "%";
				case 38:
					return "^";
				case 39:
					return "=";
				case 40:
					return "?";
				case 41:
					return "<";
				}
				break;
			case 4:
				num = badgeNumber + 1;
				return num.ToString();
			}
			return "";
		}

		private void SortBeadsByManufacturerAndGenerateBadges(List<Beads> resBeads)
		{
			ImageAttributes imageAttributes = new ImageAttributes();
			ColorMap[] array = new ColorMap[1]
			{
				new ColorMap()
			};
			array[0].OldColor = Color.Black;
			resBeads.Sort(new ComparerBeadsBySeriesAndManufacturer());
			for (int i = 0; i < resBeads.Count(); i++)
			{
				Bitmap bitmap = DrawBadges(i);
				if (Param.IsColorScheme_OrForGrayPrint)
				{
					bitmap.MakeTransparent(Color.White);
					Color clr = resBeads[i].clr;
					double num = (double)(int)clr.R * 0.299;
					clr = resBeads[i].clr;
					double num2 = num + (double)(int)clr.G * 0.587;
					clr = resBeads[i].clr;
					int num3 = (int)Math.Round(num2 + (double)(int)clr.B * 0.114);
					if (num3 <= 100)
					{
						int num4 = 255;
						array[0].NewColor = Color.FromArgb(255, num4, num4, num4);
						imageAttributes.SetRemapTable(array);
						using (Graphics graphics = Graphics.FromImage(bitmap))
						{
							graphics.DrawImage(bitmap, new Rectangle(0, 0, bitmap.Width, bitmap.Height), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel, imageAttributes);
						}
					}
				}
				resBeads[i].badge = bitmap;
				if (Settings.hobbyProgramm == HobbyProgramm.Biserok && Settings.isItSpecialVersionOfBiserokForTatyanaKudryavtceva)
				{
					resBeads[i].symbolForColoring_AndForVoiceWorkInBiserok = GetCharForBadges(i);
				}
			}
		}

        public List<Block> AddOrRemoveColorFromScheme(bool _isAddingColor, Color _clr)
        {
            List<Block> blockList = new List<Block>();
            List<Block> changedBlocks = new List<Block>();
            bool flag1 = !_isAddingColor;
            bool flag2 = true;
            if (this.Param.BiserType == BiserType.Any)
                flag2 = false;
            bool flag3 = false;
            bool flag4 = false;
            if (_isAddingColor)
                flag3 = this.TryAddColorToReworkSchemeIfItIsNotInSchemeYet(_clr);
            Beads beads1 = new Beads();
            if (flag1 && this._resBeadsRework.Count<Beads>() > 1)
            {
                flag4 = true;
                this.lastActionWithReworkScheme = Action.removingColor;
                beads1 = this._resBeadsRework.First<Beads>((Func<Beads, bool>)(bb => bb.clr == _clr));
                this._resBeadsRework.Remove(beads1);
            }
            if (flag4 | flag3)
            {
                List<Beads> source1 = new List<Beads>();
                List<Beads> source2 = new List<Beads>();
                for (int index1 = 0; index1 < this.Param.HeightInBead; ++index1)
                {
                    int widthInBead = this.Param.WidthInBead;
                    if (this.Param.Type == SchemeType.Block && index1 % 2 != 0)
                        --widthInBead;
                    for (int index2 = 0; index2 < widthInBead; ++index2)
                    {
                        Block block = this.blocksRework[index2, index1];
                        if (block.IsVisible)
                        {
                            bool? nullable1;
                            if (!flag3)
                            {
                                if (flag4)
                                {
                                    if (!block.IsItFullStitch.Value || !(block.ResBeads == beads1))
                                    {
                                        nullable1 = block.IsItFullStitch;
                                        bool? nullable2;
                                        bool? nullable3;
                                        if (!nullable1.HasValue)
                                        {
                                            nullable2 = new bool?();
                                            nullable3 = nullable2;
                                        }
                                        else
                                            nullable3 = new bool?(!nullable1.GetValueOrDefault());
                                        nullable2 = nullable3;
                                        if (nullable2.Value && (block.leftTop != null && block.leftTop.typeOfStitch != TypeOfStitch.emptyCanvas && block.leftTop.ResBeads == beads1 || block.rightTop != null && block.rightTop.typeOfStitch != TypeOfStitch.emptyCanvas && block.rightTop.ResBeads == beads1 || (block.rightBottom != null && block.rightBottom.typeOfStitch != TypeOfStitch.emptyCanvas && block.rightBottom.ResBeads == beads1 || block.leftBottom != null && block.leftBottom.typeOfStitch != TypeOfStitch.emptyCanvas && block.leftBottom.ResBeads == beads1)))
                                            goto label_19;
                                    }
                                    else
                                        goto label_19;
                                }
                                source1.Add(block.ResBeads);
                                continue;
                            }
                            label_19:
                            Beads resBeads = new Beads();
                            if (flag1)
                            {
                                Block copy = block.GetCopy(false);
                                float num = float.MaxValue;
                                foreach (Beads beads2 in this._resBeadsRework)
                                {
                                    float distanceBetweenColors = this.GetDistanceBetweenColors(block.SrcColor, beads2.clr);
                                    if ((double)distanceBetweenColors < (double)num)
                                    {
                                        num = distanceBetweenColors;
                                        resBeads = beads2;
                                    }
                                }
                                if (block.IsItFullStitch.Value && block.ResBeads == beads1)
                                {
                                    block.ResBeads = resBeads;
                                    source1.Add(resBeads);
                                    blockList.Add(copy);
                                    changedBlocks.Add(block);
                                }
                                else
                                {
                                    bool? isItFullStitch = block.IsItFullStitch;
                                    nullable1 = isItFullStitch.HasValue ? new bool?(!isItFullStitch.GetValueOrDefault()) : new bool?();
                                    if (nullable1.Value)
                                    {
                                        if (block.leftTop != null && block.leftTop.typeOfStitch != TypeOfStitch.emptyCanvas && block.leftTop.ResBeads == beads1)
                                            block.leftTop.ResBeads = resBeads;
                                        if (block.rightTop != null && block.rightTop.typeOfStitch != TypeOfStitch.emptyCanvas && block.rightTop.ResBeads == beads1)
                                            block.rightTop.ResBeads = resBeads;
                                        if (block.rightBottom != null && block.rightBottom.typeOfStitch != TypeOfStitch.emptyCanvas && block.rightBottom.ResBeads == beads1)
                                            block.rightBottom.ResBeads = resBeads;
                                        if (block.leftBottom != null && block.leftBottom.typeOfStitch != TypeOfStitch.emptyCanvas && block.leftBottom.ResBeads == beads1)
                                            block.leftBottom.ResBeads = resBeads;
                                        source1.Add(resBeads);
                                        blockList.Add(copy);
                                        changedBlocks.Add(block);
                                    }
                                }
                            }
                            else if (_isAddingColor)
                            {
                                source2.Clear();
                                if (block.IsItFullStitch.Value)
                                {
                                    source2.Add(block.ResBeads);
                                }
                                else
                                {
                                    if (block.leftTop != null && block.leftTop.ResBeads != (Beads)null)
                                        source2.Add(block.leftTop.ResBeads);
                                    if (block.rightTop != null && block.rightTop.ResBeads != (Beads)null)
                                        source2.Add(block.rightTop.ResBeads);
                                    if (block.rightBottom != null && block.rightBottom.ResBeads != (Beads)null)
                                        source2.Add(block.rightBottom.ResBeads);
                                    if (block.leftBottom != null && block.leftBottom.ResBeads != (Beads)null)
                                        source2.Add(block.leftBottom.ResBeads);
                                }
                                Block copy = block.GetCopy(false);
                                float num = float.MaxValue;
                                foreach (Beads beads2 in this._resBeadsRework)
                                {
                                    float distanceBetweenColors = this.GetDistanceBetweenColors(block.SrcColor, beads2.clr);
                                    if ((double)distanceBetweenColors < (double)num)
                                    {
                                        num = distanceBetweenColors;
                                        resBeads = beads2;
                                    }
                                }
                                if (flag2 && source2.Any<Beads>((Func<Beads, bool>)(pb => pb.id != resBeads.id)) || !flag2 && source2.Any<Beads>((Func<Beads, bool>)(pb => pb.clr != resBeads.clr)))
                                {
                                    if (block.IsItFullStitch.Value)
                                    {
                                        block.ResBeads = resBeads;
                                    }
                                    else
                                    {
                                        if (block.leftTop != null && block.leftTop.ResBeads != (Beads)null)
                                            block.leftTop.ResBeads = resBeads;
                                        if (block.rightTop != null && block.rightTop.ResBeads != (Beads)null)
                                            block.rightTop.ResBeads = resBeads;
                                        if (block.rightBottom != null && block.rightBottom.ResBeads != (Beads)null)
                                            block.rightBottom.ResBeads = resBeads;
                                        if (block.leftBottom != null && block.leftBottom.ResBeads != (Beads)null)
                                            block.leftBottom.ResBeads = resBeads;
                                    }
                                    blockList.Add(copy);
                                    changedBlocks.Add(block);
                                    source1.Add(resBeads);
                                }
                                else
                                    source1.AddRange((IEnumerable<Beads>)source2);
                            }
                        }
                    }
                }
                List<Beads> list = source1.Distinct<Beads>().ToList<Beads>();
                for (int index = 0; index < this._resBeadsRework.Count<Beads>(); ++index)
                {
                    if (!list.Contains(this._resBeadsRework[index]))
                    {
                        this._resBeadsRework.RemoveAt(index);
                        --index;
                    }
                }
                this.RedrawReworkEmbroideryView(changedBlocks);
                this.IsReworkSchemeEdited = true;
                this.IsNeedToSave = true;
            }
            return blockList;
        }

        /*
		public List<Block> AddOrRemoveColorFromScheme(bool _isAddingColor, Color _clr)
		{
		   List<Block> list = new List<Block>();
			List<Block> list2 = new List<Block>();
			bool flag = !_isAddingColor;
			bool flag2 = true;
			if (Param.BiserType == BiserType.Any)
			{
				flag2 = false;
			}
			bool flag3 = false;
			bool flag4 = false;
			if (_isAddingColor)
			{
				flag3 = TryAddColorToReworkSchemeIfItIsNotInSchemeYet(_clr);
			}
			Beads beads = new Beads();
			if (flag && _resBeadsRework.Count() > 1)
			{
				flag4 = true;
				lastActionWithReworkScheme = Action.removingColor;
				beads = _resBeadsRework.First((Beads bb) => bb.clr == _clr);
				_resBeadsRework.Remove(beads);
			}
			if (flag4 | flag3)
			{
				List<Beads> list3 = new List<Beads>();
				List<Beads> list4 = new List<Beads>();
				for (int i = 0; i < Param.HeightInBead; i++)
				{
					int num = Param.WidthInBead;
					if (Param.Type == SchemeType.Block && i % 2 != 0)
					{
						num--;
					}
					for (int j = 0; j < num; j++)
					{
						Block block = blocksRework[j, i];
						if (block.IsVisible)
						{
							if (flag3)
							{
								goto IL_0227;
							}
							if (flag4)
							{
								if (block.IsItFullStitch.Value && block.ResBeads == beads)
								{
									goto IL_0227;
								}
								if ((!block.IsItFullStitch).Value)
								{
									if (block.leftTop != null && block.leftTop.typeOfStitch != TypeOfStitch.emptyCanvas && block.leftTop.ResBeads == beads)
									{
										goto IL_0227;
									}
									if (block.rightTop != null && block.rightTop.typeOfStitch != TypeOfStitch.emptyCanvas && block.rightTop.ResBeads == beads)
									{
										goto IL_0227;
									}
									if (block.rightBottom != null && block.rightBottom.typeOfStitch != TypeOfStitch.emptyCanvas && block.rightBottom.ResBeads == beads)
									{
										goto IL_0227;
									}
									if (block.leftBottom != null && block.leftBottom.typeOfStitch != TypeOfStitch.emptyCanvas && block.leftBottom.ResBeads == beads)
									{
										goto IL_0227;
									}
								}
							}
							list3.Add(block.ResBeads);
						}
						continue;
						IL_05eb:
						_003C_003Ec__DisplayClass147_1 _003C_003Ec__DisplayClass147_;
						if (block.IsItFullStitch.Value)
						{
							block.ResBeads = _003C_003Ec__DisplayClass147_.resBeads;
						}
						else
						{
							if (block.leftTop != null && block.leftTop.ResBeads != (Beads)null)
							{
								block.leftTop.ResBeads = _003C_003Ec__DisplayClass147_.resBeads;
							}
							if (block.rightTop != null && block.rightTop.ResBeads != (Beads)null)
							{
								block.rightTop.ResBeads = _003C_003Ec__DisplayClass147_.resBeads;
							}
							if (block.rightBottom != null && block.rightBottom.ResBeads != (Beads)null)
							{
								block.rightBottom.ResBeads = _003C_003Ec__DisplayClass147_.resBeads;
							}
							if (block.leftBottom != null && block.leftBottom.ResBeads != (Beads)null)
							{
								block.leftBottom.ResBeads = _003C_003Ec__DisplayClass147_.resBeads;
							}
						}
						Block copy;
						list.Add(copy);
						list2.Add(block);
						list3.Add(_003C_003Ec__DisplayClass147_.resBeads);
						continue;
						IL_0227:
						Beads resBeads = new Beads();
						if (flag)
						{
							copy = block.GetCopy(false);
							float num2 = 3.40282347E+38f;
							foreach (Beads item in _resBeadsRework)
							{
								float distanceBetweenColors = GetDistanceBetweenColors(block.SrcColor, item.clr);
								if (!(distanceBetweenColors >= num2))
								{
									num2 = distanceBetweenColors;
									resBeads = item;
								}
							}
							if (block.IsItFullStitch.Value && block.ResBeads == beads)
							{
								block.ResBeads = resBeads;
								list3.Add(resBeads);
								list.Add(copy);
								list2.Add(block);
							}
							else
							{
								bool? nullable = !block.IsItFullStitch;
								if (nullable.Value)
								{
									if (block.leftTop != null && block.leftTop.typeOfStitch != TypeOfStitch.emptyCanvas && block.leftTop.ResBeads == beads)
									{
										block.leftTop.ResBeads = resBeads;
									}
									if (block.rightTop != null && block.rightTop.typeOfStitch != TypeOfStitch.emptyCanvas && block.rightTop.ResBeads == beads)
									{
										block.rightTop.ResBeads = resBeads;
									}
									if (block.rightBottom != null && block.rightBottom.typeOfStitch != TypeOfStitch.emptyCanvas && block.rightBottom.ResBeads == beads)
									{
										block.rightBottom.ResBeads = resBeads;
									}
									if (block.leftBottom != null && block.leftBottom.typeOfStitch != TypeOfStitch.emptyCanvas && block.leftBottom.ResBeads == beads)
									{
										block.leftBottom.ResBeads = resBeads;
									}
									list3.Add(resBeads);
									list.Add(copy);
									list2.Add(block);
								}
							}
						}
						else if (_isAddingColor)
						{
							list4.Clear();
							if (block.IsItFullStitch.Value)
							{
								list4.Add(block.ResBeads);
							}
							else
							{
								if (block.leftTop != null && block.leftTop.ResBeads != (Beads)null)
								{
									list4.Add(block.leftTop.ResBeads);
								}
								if (block.rightTop != null && block.rightTop.ResBeads != (Beads)null)
								{
									list4.Add(block.rightTop.ResBeads);
								}
								if (block.rightBottom != null && block.rightBottom.ResBeads != (Beads)null)
								{
									list4.Add(block.rightBottom.ResBeads);
								}
								if (block.leftBottom != null && block.leftBottom.ResBeads != (Beads)null)
								{
									list4.Add(block.leftBottom.ResBeads);
								}
							}
							copy = block.GetCopy(false);
							float num3 = 3.40282347E+38f;
							foreach (Beads item2 in _resBeadsRework)
							{
								float distanceBetweenColors2 = GetDistanceBetweenColors(block.SrcColor, item2.clr);
								if (!(distanceBetweenColors2 >= num3))
								{
									num3 = distanceBetweenColors2;
									resBeads = item2;
								}
							}
							if (flag2 && list4.Any((Beads pb) => pb.id != resBeads.id))
							{
								goto IL_05eb;
							}
							if (!flag2 && list4.Any((Beads pb) => pb.clr != resBeads.clr))
							{
								goto IL_05eb;
							}
							list3.AddRange(list4);
						}
					}
				}
				list3 = list3.Distinct().ToList();
				for (int k = 0; k < _resBeadsRework.Count(); k++)
				{
					if (!list3.Contains(_resBeadsRework[k]))
					{
						_resBeadsRework.RemoveAt(k);
						k--;
					}
				}
				RedrawReworkEmbroideryView(list2);
				IsReworkSchemeEdited = true;
				IsNeedToSave = true;
			}
			return list;
        }*/

        private bool TryAddColorToReworkSchemeIfItIsNotInSchemeYet(Color _clr)
		{
			bool result = false;
			if (Param.BiserType == BiserType.Any)
			{
				bool flag = true;
				foreach (Beads item in _resBeadsRework)
				{
					Color clr = item.clr;
					double num = Math.Sqrt((double)((_clr.G - clr.G) * (_clr.G - clr.G)) + (double)((_clr.R - clr.R) * (_clr.R - clr.R)) / 4.0 + (double)((_clr.B - clr.B) * (_clr.B - clr.B)) / 16.0);
					if (num < 5.0)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					result = true;
					lastActionWithReworkScheme = Action.addingColor;
					_resBeadsRework.Add(new Beads
					{
						clr = _clr
					});
				}
			}
			else if (Param.BiserType == BiserType.ForManufacturers)
			{
				Beads beads = new Beads();
				float num2 = 3.40282347E+38f;
				foreach (Beads item2 in beadsEnableToUse)
				{
					float distanceBetweenColors = GetDistanceBetweenColors(_clr, item2.clr);
					if (!(distanceBetweenColors >= num2))
					{
						num2 = distanceBetweenColors;
						beads = item2;
					}
				}
				if (!_resBeadsRework.Any((Beads resB) => resB == beads))
				{
					result = true;
					lastActionWithReworkScheme = Action.addingColor;
					_resBeadsRework.Add(beads);
				}
			}
			return result;
		}

		private void RedrawEmbroideryView(Bitmap BmpBeadwork, List<Block> changedBlocks)
		{
			Graphics graphics = Graphics.FromImage(BmpBeadwork);
			if (Param.IsEmbroideryViewInRealView)
			{
				graphics.SmoothingMode = SmoothingMode.AntiAlias;
				foreach (Block changedBlock in changedBlocks)
				{
					DrawRealViewOfBlock(graphics, changedBlock, null, null);
				}
			}
			else
			{
				foreach (Block changedBlock2 in changedBlocks)
				{
					DrawKubicViewOfBlock(graphics, changedBlock2, null, null);
				}
			}
			graphics.Dispose();
		}

		public void RedrawReworkEmbroideryView(List<Block> changedBlocks)
		{
			RedrawEmbroideryView(BmpBeadworkRework, changedBlocks);
		}

		public void FastHideOrShowBlocksFromEmbroideryView(List<Point> points, bool newVisibilityState)
		{
			List<Block> list = new List<Block>();
			try
			{
				Graphics graphics = Graphics.FromImage(BmpBeadworkRework);
				graphics.SmoothingMode = SmoothingMode.AntiAlias;
				List<Block> bbb = new List<Block>();
				for (int i = 0; i < Param.HeightInBead; i++)
				{
					for (int j = 0; j < Param.WidthInBead; j++)
					{
						if (blocksRework[j, i] != null)
						{
							bbb.Add(blocksRework[j, i]);
						}
					}
				}
				int widthSum = Param.WidthInBead + Param.WidthInBead - ((Param.Type == SchemeType.Block) ? 1 : 0);
				foreach (int item in from p in points
				select p.Y / 2 * widthSum + ((p.Y % 2 != 0) ? Param.WidthInBead : 0) + p.X into num
				where bbb[num].IsVisible != newVisibilityState
				select num)
				{
					try
					{
						Block block = bbb[item];
						list.Add(block);
						if (block.IsVisible != newVisibilityState)
						{
							block.IsVisible = newVisibilityState;
							if (newVisibilityState)
							{
								if (Param.IsEmbroideryViewInRealView)
								{
									DrawRealViewOfBlock(graphics, block, null, null);
								}
								else
								{
									DrawKubicViewOfBlock(graphics, block, null, null);
								}
							}
							else
							{
								DrawSourceImageOnPlaceOfRemovedBlock(graphics, block);
							}
							IsReworkSchemeEdited = true;
							IsNeedToSave = true;
						}
					}
					catch
					{
					}
				}
				graphics.Dispose();
			}
			catch
			{
			}
			IEnumerable<Beads> enumerable = (from bl in list
			select bl.ResBeads).Distinct();
			IEnumerable<Block> source = from Block b in blocksRework
			where b != null
			select b;
			foreach (Beads item2 in enumerable)
			{
				if (!newVisibilityState)
				{
					if (!source.Any(delegate(Block rb)
					{
						if (rb.IsVisible)
						{
							return rb.ResBeads == item2;
						}
						return false;
					}))
					{
						_resBeadsRework.Remove(item2);
					}
				}
				else if (!_resBeadsRework.Contains(item2))
				{
					_resBeadsRework.Add(item2);
				}
			}
		}

		private void DrawSourceImageOnPlaceOfRemovedBlock(Graphics g, Block bb)
		{
			int width;
			int height;
			if (BmpBeadworkSource != null)
			{
				width = BmpBeadworkSource.Width;
				height = BmpBeadworkSource.Height;
			}
			else if (BmpBeadworkRework != null)
			{
				width = BmpBeadworkRework.Width;
				height = BmpBeadworkRework.Height;
			}
			else
			{
				width = BmpBeadworkHandEditing.Width;
				height = BmpBeadworkHandEditing.Height;
			}
			float num = ((float)BmpSourceImage.Width + 0f) / ((float)width + 0f);
			float num2 = ((float)BmpSourceImage.Height + 0f) / ((float)height + 0f);
			RectangleF positionInScheme = bb.PositionInScheme;
			float left = positionInScheme.Left;
			positionInScheme = bb.PositionInScheme;
			float top = positionInScheme.Top;
			positionInScheme = bb.PositionInScheme;
			float width2 = positionInScheme.Width + 1f;
			positionInScheme = bb.PositionInScheme;
			RectangleF destRect = new RectangleF(left, top, width2, positionInScheme.Height + 1f);
			RectangleF srcRect = new RectangleF(destRect.Left * num, destRect.Top * num2, destRect.Width * num, destRect.Height * num2);
			g.DrawImage(BmpSourceImage, destRect, srcRect, GraphicsUnit.Pixel);
		}

		private void DrawKubicViewOfBlock(Graphics g, Block bb, Brush highliteColor = null, Beads highlitedBeads = null)
		{
			if (bb.IsVisible)
			{
				RectangleF positionInScheme = bb.PositionInScheme;
				if (bb.IsItFullStitch.Value)
				{
					SolidBrush brush = (highliteColor != null) ? ((SolidBrush)highliteColor) : bb.ResBeads.brush;
					g.FillRectangle(brush, positionInScheme);
				}
				else
				{
					g.FillRectangle(Brushes.White, positionInScheme);
					if (bb.leftTop != null && bb.leftTop.typeOfStitch != TypeOfStitch.emptyCanvas)
					{
						if (bb.leftTop.typeOfStitch == TypeOfStitch.half)
						{
							g.FillRectangle(bb.leftTop.ResBeads.brush, new RectangleF(positionInScheme.X, positionInScheme.Y, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
							g.FillRectangle(bb.leftTop.ResBeads.brush, new RectangleF(positionInScheme.X + positionInScheme.Width / 2f, positionInScheme.Y + positionInScheme.Height / 2f, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
						}
						else if (bb.leftTop.typeOfStitch == TypeOfStitch.full_quarter)
						{
							g.FillRectangle(bb.leftTop.ResBeads.brush, new RectangleF(positionInScheme.X, positionInScheme.Y, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
						}
						else if (bb.leftTop.typeOfStitch == TypeOfStitch.half_quarter)
						{
							g.FillRectangle(bb.leftTop.ResBeads.brush, new RectangleF(positionInScheme.X, positionInScheme.Y, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
						}
						else if (bb.leftTop.typeOfStitch == TypeOfStitch.three_qarter)
						{
							List<PointF> list = new List<PointF>();
							list.Add(new PointF(positionInScheme.X, positionInScheme.Y));
							list.Add(new PointF(positionInScheme.X + positionInScheme.Width, positionInScheme.Y));
							list.Add(new PointF(positionInScheme.X, positionInScheme.Y + positionInScheme.Height));
							g.FillPolygon(bb.leftTop.ResBeads.brush, list.ToArray());
						}
					}
					if (bb.rightTop != null && bb.rightTop.typeOfStitch != TypeOfStitch.emptyCanvas)
					{
						if (bb.rightTop.typeOfStitch == TypeOfStitch.half)
						{
							g.FillRectangle(bb.rightTop.ResBeads.brush, new RectangleF(positionInScheme.X + positionInScheme.Width / 2f, positionInScheme.Y, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
							g.FillRectangle(bb.rightTop.ResBeads.brush, new RectangleF(positionInScheme.X, positionInScheme.Y + positionInScheme.Height / 2f, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
						}
						else if (bb.rightTop.typeOfStitch == TypeOfStitch.full_quarter)
						{
							g.FillRectangle(bb.rightTop.ResBeads.brush, new RectangleF(positionInScheme.X + positionInScheme.Width / 2f, positionInScheme.Y, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
						}
						else if (bb.rightTop.typeOfStitch == TypeOfStitch.half_quarter)
						{
							g.FillRectangle(bb.rightTop.ResBeads.brush, new RectangleF(positionInScheme.X + positionInScheme.Width / 2f, positionInScheme.Y, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
						}
						else if (bb.rightTop.typeOfStitch == TypeOfStitch.three_qarter)
						{
							List<PointF> list2 = new List<PointF>();
							list2.Add(new PointF(positionInScheme.X, positionInScheme.Y));
							list2.Add(new PointF(positionInScheme.X + positionInScheme.Width, positionInScheme.Y));
							list2.Add(new PointF(positionInScheme.X + positionInScheme.Width, positionInScheme.Y + positionInScheme.Height));
							g.FillPolygon(bb.rightTop.ResBeads.brush, list2.ToArray());
						}
					}
					if (bb.rightBottom != null && bb.rightBottom.typeOfStitch != TypeOfStitch.emptyCanvas)
					{
						if (bb.rightBottom.typeOfStitch == TypeOfStitch.half)
						{
							g.FillRectangle(bb.rightBottom.ResBeads.brush, new RectangleF(positionInScheme.X, positionInScheme.Y, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
							g.FillRectangle(bb.rightBottom.ResBeads.brush, new RectangleF(positionInScheme.X + positionInScheme.Width / 2f, positionInScheme.Y + positionInScheme.Height / 2f, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
						}
						else if (bb.rightBottom.typeOfStitch == TypeOfStitch.full_quarter)
						{
							g.FillRectangle(bb.rightBottom.ResBeads.brush, new RectangleF(positionInScheme.X + positionInScheme.Width / 2f, positionInScheme.Y + positionInScheme.Height / 2f, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
						}
						else if (bb.rightBottom.typeOfStitch == TypeOfStitch.half_quarter)
						{
							g.FillRectangle(bb.rightBottom.ResBeads.brush, new RectangleF(positionInScheme.X + positionInScheme.Width / 2f, positionInScheme.Y + positionInScheme.Height / 2f, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
						}
						else if (bb.rightBottom.typeOfStitch == TypeOfStitch.three_qarter)
						{
							List<PointF> list3 = new List<PointF>();
							list3.Add(new PointF(positionInScheme.X, positionInScheme.Y + positionInScheme.Height));
							list3.Add(new PointF(positionInScheme.X + positionInScheme.Width, positionInScheme.Y));
							list3.Add(new PointF(positionInScheme.X + positionInScheme.Width, positionInScheme.Y + positionInScheme.Height));
							g.FillPolygon(bb.rightBottom.ResBeads.brush, list3.ToArray());
						}
					}
					if (bb.leftBottom != null && bb.leftBottom.typeOfStitch != TypeOfStitch.emptyCanvas)
					{
						if (bb.leftBottom.typeOfStitch == TypeOfStitch.half)
						{
							g.FillRectangle(bb.leftBottom.ResBeads.brush, new RectangleF(positionInScheme.X + positionInScheme.Width / 2f, positionInScheme.Y, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
							g.FillRectangle(bb.leftBottom.ResBeads.brush, new RectangleF(positionInScheme.X, positionInScheme.Y + positionInScheme.Height / 2f, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
						}
						else if (bb.leftBottom.typeOfStitch == TypeOfStitch.full_quarter)
						{
							g.FillRectangle(bb.leftBottom.ResBeads.brush, new RectangleF(positionInScheme.X, positionInScheme.Y + positionInScheme.Height / 2f, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
						}
						else if (bb.leftBottom.typeOfStitch == TypeOfStitch.half_quarter)
						{
							g.FillRectangle(bb.leftBottom.ResBeads.brush, new RectangleF(positionInScheme.X, positionInScheme.Y + positionInScheme.Height / 2f, positionInScheme.Width / 2f, positionInScheme.Height / 2f));
						}
						else if (bb.leftBottom.typeOfStitch == TypeOfStitch.three_qarter)
						{
							List<PointF> list4 = new List<PointF>();
							list4.Add(new PointF(positionInScheme.X, positionInScheme.Y));
							list4.Add(new PointF(positionInScheme.X + positionInScheme.Width, positionInScheme.Y + positionInScheme.Height));
							list4.Add(new PointF(positionInScheme.X, positionInScheme.Y + positionInScheme.Height));
							g.FillPolygon(bb.leftBottom.ResBeads.brush, list4.ToArray());
						}
					}
				}
			}
			else
			{
				DrawSourceImageOnPlaceOfRemovedBlock(g, bb);
			}
		}

		private void DrawRealViewOfBlock(Graphics g, Block bb, Brush highliteColor = null, Beads highlitedBeads = null)
		{
			if (bb != null)
			{
				if (bb.IsVisible)
				{
					RectangleF positionInScheme = bb.PositionInScheme;
					float x = positionInScheme.X;
					positionInScheme = bb.PositionInScheme;
					float y = positionInScheme.Y;
					positionInScheme = bb.PositionInScheme;
					float width = positionInScheme.Width;
					positionInScheme = bb.PositionInScheme;
					RectangleF rect = new RectangleF(x, y, width, positionInScheme.Height);
					Color color2;
					if (Settings.hobbyProgramm == HobbyProgramm.Biserok || (Settings.hobbyProgramm == HobbyProgramm.Almaz && !Param.isSquareStraz))
					{
						SolidBrush solidBrush = (highliteColor != null) ? ((SolidBrush)highliteColor) : bb.ResBeads.brush;
						positionInScheme = bb.PositionInScheme;
						float x2 = positionInScheme.X - 0.5f;
						positionInScheme = bb.PositionInScheme;
						float y2 = positionInScheme.Y - 0.5f;
						positionInScheme = bb.PositionInScheme;
						float width2 = positionInScheme.Width + 1f;
						positionInScheme = bb.PositionInScheme;
						RectangleF rect2 = new RectangleF(x2, y2, width2, positionInScheme.Height + 1f);
						g.FillRectangle(new SolidBrush(Color.LightGray), rect2);
						PointF pointF = new PointF(0.4f, 0.15f);
						float num = 0.4f;
						float num2 = 0.75f;
						positionInScheme = bb.PositionInScheme;
						float x3 = positionInScheme.X - num2;
						positionInScheme = bb.PositionInScheme;
						float y3 = positionInScheme.Y - num2;
						positionInScheme = bb.PositionInScheme;
						float width3 = positionInScheme.Width + num2 * 2f;
						positionInScheme = bb.PositionInScheme;
						rect = new RectangleF(x3, y3, width3, positionInScheme.Height + num2 * 2f);
						g.FillEllipse(solidBrush, rect);
						float num3 = 0f;
						Color color = Color.FromArgb(240, solidBrush.Color);
						positionInScheme = bb.PositionInScheme;
						float x4 = positionInScheme.X;
						positionInScheme = bb.PositionInScheme;
						float x5 = x4 + positionInScheme.Width * (pointF.X + num3);
						positionInScheme = bb.PositionInScheme;
						float y4 = positionInScheme.Y;
						positionInScheme = bb.PositionInScheme;
						float y5 = y4 + positionInScheme.Height * (pointF.Y + num3);
						positionInScheme = bb.PositionInScheme;
						float width4 = positionInScheme.Width * (num - 2f * num3);
						positionInScheme = bb.PositionInScheme;
						rect = new RectangleF(x5, y5, width4, positionInScheme.Height * (num - 2f * num3));
						g.FillEllipse(new SolidBrush(Color.White), rect);
						g.FillEllipse(new SolidBrush(color), rect);
						num3 = 0.06666f;
						color = Color.FromArgb(189, solidBrush.Color);
						positionInScheme = bb.PositionInScheme;
						float x6 = positionInScheme.X;
						positionInScheme = bb.PositionInScheme;
						float x7 = x6 + positionInScheme.Width * (pointF.X + num3);
						positionInScheme = bb.PositionInScheme;
						float y6 = positionInScheme.Y;
						positionInScheme = bb.PositionInScheme;
						float y7 = y6 + positionInScheme.Height * (pointF.Y + num3);
						positionInScheme = bb.PositionInScheme;
						float width5 = positionInScheme.Width * (num - 2f * num3);
						positionInScheme = bb.PositionInScheme;
						rect = new RectangleF(x7, y7, width5, positionInScheme.Height * (num - 2f * num3));
						g.FillEllipse(new SolidBrush(Color.White), rect);
						g.FillEllipse(new SolidBrush(color), rect);
						num3 = 0.13333f;
						color = Color.FromArgb(138, solidBrush.Color);
						positionInScheme = bb.PositionInScheme;
						float x8 = positionInScheme.X;
						positionInScheme = bb.PositionInScheme;
						float x9 = x8 + positionInScheme.Width * (pointF.X + num3);
						positionInScheme = bb.PositionInScheme;
						float y8 = positionInScheme.Y;
						positionInScheme = bb.PositionInScheme;
						float y9 = y8 + positionInScheme.Height * (pointF.Y + num3);
						positionInScheme = bb.PositionInScheme;
						float width6 = positionInScheme.Width * (num - 2f * num3);
						positionInScheme = bb.PositionInScheme;
						rect = new RectangleF(x9, y9, width6, positionInScheme.Height * (num - 2f * num3));
						g.FillEllipse(new SolidBrush(Color.White), rect);
						g.FillEllipse(new SolidBrush(color), rect);
					}
					else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
					{
						positionInScheme = bb.PositionInScheme;
						float x10 = positionInScheme.X - 0.5f;
						positionInScheme = bb.PositionInScheme;
						float y10 = positionInScheme.Y - 0.5f;
						positionInScheme = bb.PositionInScheme;
						float width7 = positionInScheme.Width + 1f;
						positionInScheme = bb.PositionInScheme;
						RectangleF rect3 = new RectangleF(x10, y10, width7, positionInScheme.Height + 1f);
						g.FillRectangle(_sbrForDrawBlockInKrestik, rect3);
						g.FillRectangle(Brushes.White, rect);
						float num4 = 1.073f;
						SolidBrush solidBrush2 = (highliteColor != null) ? ((SolidBrush)highliteColor) : bb.ResBeads.brush;
						float num5 = num4;
						color2 = solidBrush2.Color;
						int num6 = (int)Math.Round((double)(num5 * (float)(int)color2.R));
						float num7 = num4;
						color2 = solidBrush2.Color;
						int num8 = (int)Math.Round((double)(num7 * (float)(int)color2.G));
						float num9 = num4;
						color2 = solidBrush2.Color;
						int num10 = (int)Math.Round((double)(num9 * (float)(int)color2.B));
						int num11 = Math.Max(Math.Max(num6, num8), num10);
						if (num11 > 255)
						{
							num4 = (float)num11 / 255f;
							num6 = (int)Math.Round((double)(((float)num6 + 0f) / num4));
							num8 = (int)Math.Round((double)(((float)num8 + 0f) / num4));
							num10 = (int)Math.Round((double)(((float)num10 + 0f) / num4));
						}
						Color color3 = Color.FromArgb(num6, num8, num10);
						if (bb.IsItFullStitch.Value)
						{
							g.FillRectangle(new SolidBrush(color3), rect);
						}
						else
						{
							if (bb.leftTop != null && bb.leftTop.typeOfStitch != TypeOfStitch.emptyCanvas)
							{
								if (bb.leftTop.typeOfStitch == TypeOfStitch.half)
								{
									List<PointF> list = new List<PointF>();
									list.Add(new PointF(rect.X, rect.Y));
									list.Add(new PointF(rect.X + rect.Width * 4f / 11f, rect.Y));
									list.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height * 8f / 11f));
									list.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height));
									list.Add(new PointF(rect.X + rect.Width * 8f / 11f, rect.Y + rect.Height));
									list.Add(new PointF(rect.X, rect.Y + rect.Height * 4f / 11f));
									if (highlitedBeads == (Beads)null)
									{
										g.FillPolygon(bb.leftTop.ResBeads.brush, list.ToArray());
									}
									else if (highlitedBeads == bb.leftTop.ResBeads)
									{
										g.FillPolygon(highliteColor, list.ToArray());
									}
									else
									{
										g.FillPolygon(bb.leftTop.ResBeads.brush, list.ToArray());
									}
								}
								else if (bb.leftTop.typeOfStitch == TypeOfStitch.full_quarter)
								{
									if (highlitedBeads == (Beads)null)
									{
										g.FillRectangle(bb.leftTop.ResBeads.brush, new RectangleF(rect.X, rect.Y, rect.Width / 2f, rect.Height / 2f));
									}
									else if (highlitedBeads == bb.leftTop.ResBeads)
									{
										g.FillRectangle(highliteColor, new RectangleF(rect.X, rect.Y, rect.Width / 2f, rect.Height / 2f));
									}
									else
									{
										g.FillRectangle(bb.leftTop.ResBeads.brush, new RectangleF(rect.X, rect.Y, rect.Width / 2f, rect.Height / 2f));
									}
								}
								else if (bb.leftTop.typeOfStitch == TypeOfStitch.half_quarter)
								{
									List<PointF> list2 = new List<PointF>();
									list2.Add(new PointF(rect.X, rect.Y));
									list2.Add(new PointF(rect.X + rect.Width * 3f / 11f, rect.Y));
									list2.Add(new PointF(rect.X + rect.Width * 6f / 11f, rect.Y + rect.Height * 4f / 11f));
									list2.Add(new PointF(rect.X + rect.Width * 4f / 11f, rect.Y + rect.Height * 6f / 11f));
									list2.Add(new PointF(rect.X, rect.Y + rect.Height * 3f / 11f));
									if (highlitedBeads == (Beads)null)
									{
										g.FillPolygon(bb.leftTop.ResBeads.brush, list2.ToArray());
									}
									else if (highlitedBeads == bb.leftTop.ResBeads)
									{
										g.FillPolygon(highliteColor, list2.ToArray());
									}
									else
									{
										g.FillPolygon(bb.leftTop.ResBeads.brush, list2.ToArray());
									}
								}
								else if (bb.leftTop.typeOfStitch == TypeOfStitch.three_qarter)
								{
									List<PointF> list3 = new List<PointF>();
									list3.Add(new PointF(rect.X, rect.Y));
									list3.Add(new PointF(rect.X + rect.Width, rect.Y));
									list3.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height * 3f / 11f));
									list3.Add(new PointF(rect.X + rect.Width * 3f / 11f, rect.Y + rect.Height));
									list3.Add(new PointF(rect.X, rect.Y + rect.Height));
									if (highlitedBeads == (Beads)null)
									{
										g.FillPolygon(bb.leftTop.ResBeads.brush, list3.ToArray());
									}
									else if (highlitedBeads == bb.leftTop.ResBeads)
									{
										g.FillPolygon(highliteColor, list3.ToArray());
									}
									else
									{
										g.FillPolygon(bb.leftTop.ResBeads.brush, list3.ToArray());
									}
								}
							}
							if (bb.rightTop != null && bb.rightTop.typeOfStitch != TypeOfStitch.emptyCanvas)
							{
								if (bb.rightTop.typeOfStitch == TypeOfStitch.half)
								{
									List<PointF> list4 = new List<PointF>();
									list4.Add(new PointF(rect.X + rect.Width * 8f / 11f, rect.Y));
									list4.Add(new PointF(rect.X + rect.Width, rect.Y));
									list4.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height * 4f / 11f));
									list4.Add(new PointF(rect.X + rect.Width * 4f / 11f, rect.Y + rect.Height));
									list4.Add(new PointF(rect.X, rect.Y + rect.Height));
									list4.Add(new PointF(rect.X, rect.Y + rect.Height * 8f / 11f));
									if (highlitedBeads == (Beads)null)
									{
										g.FillPolygon(bb.rightTop.ResBeads.brush, list4.ToArray());
									}
									else if (highlitedBeads == bb.rightTop.ResBeads)
									{
										g.FillPolygon(highliteColor, list4.ToArray());
									}
									else
									{
										g.FillPolygon(bb.rightTop.ResBeads.brush, list4.ToArray());
									}
								}
								else if (bb.rightTop.typeOfStitch == TypeOfStitch.full_quarter)
								{
									if (highlitedBeads == (Beads)null)
									{
										g.FillRectangle(bb.rightTop.ResBeads.brush, new RectangleF(rect.X + rect.Width / 2f, rect.Y, rect.Width / 2f, rect.Height / 2f));
									}
									else if (highlitedBeads == bb.rightTop.ResBeads)
									{
										g.FillRectangle(highliteColor, new RectangleF(rect.X + rect.Width / 2f, rect.Y, rect.Width / 2f, rect.Height / 2f));
									}
									else
									{
										g.FillRectangle(bb.rightTop.ResBeads.brush, new RectangleF(rect.X + rect.Width / 2f, rect.Y, rect.Width / 2f, rect.Height / 2f));
									}
								}
								else if (bb.rightTop.typeOfStitch == TypeOfStitch.half_quarter)
								{
									List<PointF> list5 = new List<PointF>();
									list5.Add(new PointF(rect.X + rect.Width * 8f / 11f, rect.Y));
									list5.Add(new PointF(rect.X + rect.Width, rect.Y));
									list5.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height * 3f / 11f));
									list5.Add(new PointF(rect.X + rect.Width * 6f / 11f, rect.Y + rect.Height * 6f / 11f));
									list5.Add(new PointF(rect.X + rect.Width * 4f / 11f, rect.Y + rect.Height * 4f / 11f));
									if (highlitedBeads == (Beads)null)
									{
										g.FillPolygon(bb.rightTop.ResBeads.brush, list5.ToArray());
									}
									else if (highlitedBeads == bb.rightTop.ResBeads)
									{
										g.FillPolygon(highliteColor, list5.ToArray());
									}
									else
									{
										g.FillPolygon(bb.rightTop.ResBeads.brush, list5.ToArray());
									}
								}
								else if (bb.rightTop.typeOfStitch == TypeOfStitch.three_qarter)
								{
									List<PointF> list6 = new List<PointF>();
									list6.Add(new PointF(rect.X, rect.Y + rect.Height * 4f / 11f));
									list6.Add(new PointF(rect.X, rect.Y));
									list6.Add(new PointF(rect.X + rect.Width, rect.Y));
									list6.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height));
									list6.Add(new PointF(rect.X + rect.Width * 8f / 11f, rect.Y + rect.Height));
									if (highlitedBeads == (Beads)null)
									{
										g.FillPolygon(bb.rightTop.ResBeads.brush, list6.ToArray());
									}
									else if (highlitedBeads == bb.rightTop.ResBeads)
									{
										g.FillPolygon(highliteColor, list6.ToArray());
									}
									else
									{
										g.FillPolygon(bb.rightTop.ResBeads.brush, list6.ToArray());
									}
								}
							}
							if (bb.rightBottom != null && bb.rightBottom.typeOfStitch != TypeOfStitch.emptyCanvas)
							{
								if (bb.rightBottom.typeOfStitch == TypeOfStitch.half)
								{
									List<PointF> list7 = new List<PointF>();
									list7.Add(new PointF(rect.X, rect.Y));
									list7.Add(new PointF(rect.X + rect.Width * 4f / 11f, rect.Y));
									list7.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height * 8f / 11f));
									list7.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height));
									list7.Add(new PointF(rect.X + rect.Width * 8f / 11f, rect.Y + rect.Height));
									list7.Add(new PointF(rect.X, rect.Y + rect.Height * 4f / 11f));
									if (highlitedBeads == (Beads)null)
									{
										g.FillPolygon(bb.rightBottom.ResBeads.brush, list7.ToArray());
									}
									else if (highlitedBeads == bb.rightBottom.ResBeads)
									{
										g.FillPolygon(highliteColor, list7.ToArray());
									}
									else
									{
										g.FillPolygon(bb.rightBottom.ResBeads.brush, list7.ToArray());
									}
								}
								else if (bb.rightBottom.typeOfStitch == TypeOfStitch.full_quarter)
								{
									if (highlitedBeads == (Beads)null)
									{
										g.FillRectangle(bb.rightBottom.ResBeads.brush, new RectangleF(rect.X + rect.Width / 2f, rect.Y + rect.Height / 2f, rect.Width / 2f, rect.Height / 2f));
									}
									else if (highlitedBeads == bb.rightBottom.ResBeads)
									{
										g.FillRectangle(highliteColor, new RectangleF(rect.X + rect.Width / 2f, rect.Y + rect.Height / 2f, rect.Width / 2f, rect.Height / 2f));
									}
									else
									{
										g.FillRectangle(bb.rightBottom.ResBeads.brush, new RectangleF(rect.X + rect.Width / 2f, rect.Y + rect.Height / 2f, rect.Width / 2f, rect.Height / 2f));
									}
								}
								else if (bb.rightBottom.typeOfStitch == TypeOfStitch.half_quarter)
								{
									List<PointF> list8 = new List<PointF>();
									list8.Add(new PointF(rect.X + rect.Width * 6f / 11f, rect.Y + rect.Height * 4f / 11f));
									list8.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height * 9f / 11f));
									list8.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height));
									list8.Add(new PointF(rect.X + rect.Width * 9f / 11f, rect.Y + rect.Height));
									list8.Add(new PointF(rect.X + rect.Width * 4f / 11f, rect.Y + rect.Height * 6f / 11f));
									if (highlitedBeads == (Beads)null)
									{
										g.FillPolygon(bb.rightBottom.ResBeads.brush, list8.ToArray());
									}
									else if (highlitedBeads == bb.rightBottom.ResBeads)
									{
										g.FillPolygon(highliteColor, list8.ToArray());
									}
									else
									{
										g.FillPolygon(bb.rightBottom.ResBeads.brush, list8.ToArray());
									}
								}
								else if (bb.rightBottom.typeOfStitch == TypeOfStitch.three_qarter)
								{
									List<PointF> list9 = new List<PointF>();
									list9.Add(new PointF(rect.X + rect.Width * 8f / 11f, rect.Y));
									list9.Add(new PointF(rect.X + rect.Width, rect.Y));
									list9.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height));
									list9.Add(new PointF(rect.X, rect.Y + rect.Height));
									list9.Add(new PointF(rect.X, rect.Y + rect.Height * 8f / 11f));
									if (highlitedBeads == (Beads)null)
									{
										g.FillPolygon(bb.rightBottom.ResBeads.brush, list9.ToArray());
									}
									else if (highlitedBeads == bb.rightBottom.ResBeads)
									{
										g.FillPolygon(highliteColor, list9.ToArray());
									}
									else
									{
										g.FillPolygon(bb.rightBottom.ResBeads.brush, list9.ToArray());
									}
								}
							}
							if (bb.leftBottom != null && bb.leftBottom.typeOfStitch != TypeOfStitch.emptyCanvas)
							{
								if (bb.leftBottom.typeOfStitch == TypeOfStitch.half)
								{
									List<PointF> list10 = new List<PointF>();
									list10.Add(new PointF(rect.X + rect.Width * 8f / 11f, rect.Y));
									list10.Add(new PointF(rect.X + rect.Width, rect.Y));
									list10.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height * 4f / 11f));
									list10.Add(new PointF(rect.X + rect.Width * 4f / 11f, rect.Y + rect.Height));
									list10.Add(new PointF(rect.X, rect.Y + rect.Height));
									list10.Add(new PointF(rect.X, rect.Y + rect.Height * 8f / 11f));
									if (highlitedBeads == (Beads)null)
									{
										g.FillPolygon(bb.leftBottom.ResBeads.brush, list10.ToArray());
									}
									else if (highlitedBeads == bb.leftBottom.ResBeads)
									{
										g.FillPolygon(highliteColor, list10.ToArray());
									}
									else
									{
										g.FillPolygon(bb.leftBottom.ResBeads.brush, list10.ToArray());
									}
								}
								else if (bb.leftBottom.typeOfStitch == TypeOfStitch.full_quarter)
								{
									if (highlitedBeads == (Beads)null)
									{
										g.FillRectangle(bb.leftBottom.ResBeads.brush, new RectangleF(rect.X, rect.Y + rect.Height / 2f, rect.Width / 2f, rect.Height / 2f));
									}
									else if (highlitedBeads == bb.leftBottom.ResBeads)
									{
										g.FillRectangle(highliteColor, new RectangleF(rect.X, rect.Y + rect.Height / 2f, rect.Width / 2f, rect.Height / 2f));
									}
									else
									{
										g.FillRectangle(bb.leftBottom.ResBeads.brush, new RectangleF(rect.X, rect.Y + rect.Height / 2f, rect.Width / 2f, rect.Height / 2f));
									}
								}
								else if (bb.leftBottom.typeOfStitch == TypeOfStitch.half_quarter)
								{
									List<PointF> list11 = new List<PointF>();
									list11.Add(new PointF(rect.X, rect.Y + rect.Height * 9f / 11f));
									list11.Add(new PointF(rect.X + rect.Width * 4f / 11f, rect.Y + rect.Height * 4f / 11f));
									list11.Add(new PointF(rect.X + rect.Width * 6f / 11f, rect.Y + rect.Height * 6f / 11f));
									list11.Add(new PointF(rect.X + rect.Width * 3f / 11f, rect.Y + rect.Height));
									list11.Add(new PointF(rect.X, rect.Y + rect.Height));
									if (highlitedBeads == (Beads)null)
									{
										g.FillPolygon(bb.leftBottom.ResBeads.brush, list11.ToArray());
									}
									else if (highlitedBeads == bb.leftBottom.ResBeads)
									{
										g.FillPolygon(highliteColor, list11.ToArray());
									}
									else
									{
										g.FillPolygon(bb.leftBottom.ResBeads.brush, list11.ToArray());
									}
								}
								else if (bb.leftBottom.typeOfStitch == TypeOfStitch.three_qarter)
								{
									List<PointF> list12 = new List<PointF>();
									list12.Add(new PointF(rect.X, rect.Y));
									list12.Add(new PointF(rect.X + rect.Width * 3f / 11f, rect.Y));
									list12.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height * 9f / 11f));
									list12.Add(new PointF(rect.X + rect.Width, rect.Y + rect.Height));
									list12.Add(new PointF(rect.X, rect.Y + rect.Height));
									if (highlitedBeads == (Beads)null)
									{
										g.FillPolygon(bb.leftBottom.ResBeads.brush, list12.ToArray());
									}
									else if (highlitedBeads == bb.leftBottom.ResBeads)
									{
										g.FillPolygon(highliteColor, list12.ToArray());
									}
									else
									{
										g.FillPolygon(bb.leftBottom.ResBeads.brush, list12.ToArray());
									}
								}
							}
						}
						g.DrawImage(_bmpPatternOfKrestikForDrawBlock, rect);
					}
					else if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
					{
						try
						{
							float num12 = 1.06478f;
							SolidBrush solidBrush3 = (highliteColor != null) ? ((SolidBrush)highliteColor) : bb.ResBeads.brush;
							float num13 = num12;
							color2 = solidBrush3.Color;
							int num14 = (int)Math.Round((double)(num13 * (float)(int)color2.R));
							float num15 = num12;
							color2 = solidBrush3.Color;
							int num16 = (int)Math.Round((double)(num15 * (float)(int)color2.G));
							float num17 = num12;
							color2 = solidBrush3.Color;
							int num18 = (int)Math.Round((double)(num17 * (float)(int)color2.B));
							int num19 = Math.Max(Math.Max(num14, num16), num18);
							if (num19 > 255)
							{
								num12 = (float)num19 / 255f;
								num14 = (int)Math.Round((double)(((float)num14 + 0f) / num12));
								num16 = (int)Math.Round((double)(((float)num16 + 0f) / num12));
								num18 = (int)Math.Round((double)(((float)num18 + 0f) / num12));
							}
							Color color4 = Color.FromArgb(num14, num16, num18);
							float num20 = 0.5f;
							g.FillPolygon(new SolidBrush(color4), new PointF[6]
							{
								new PointF(rect.Left - num20, rect.Top - num20),
								new PointF(rect.Left + rect.Width / 2f, rect.Top + rect.Height / 2f - num20),
								new PointF(rect.Right + num20, rect.Top - num20),
								new PointF(rect.Right + num20, rect.Bottom + num20),
								new PointF(rect.Left + rect.Width / 2f, rect.Bottom + rect.Height / 2f + num20),
								new PointF(rect.Left - num20, rect.Bottom + num20)
							});
							g.DrawImage(_bmpPatternOfPetelkaForDrawBlock, new RectangleF(rect.Left, rect.Top, rect.Width, rect.Height * 1.5f));
						}
						catch (Exception)
						{
						}
					}
					else if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
					{
						float num21 = 1.09f;
						SolidBrush solidBrush4 = (highliteColor != null) ? ((SolidBrush)highliteColor) : bb.ResBeads.brush;
						float num22 = num21;
						color2 = solidBrush4.Color;
						int num23 = (int)Math.Round((double)(num22 * (float)(int)color2.R));
						float num24 = num21;
						color2 = solidBrush4.Color;
						int num25 = (int)Math.Round((double)(num24 * (float)(int)color2.G));
						float num26 = num21;
						color2 = solidBrush4.Color;
						int num27 = (int)Math.Round((double)(num26 * (float)(int)color2.B));
						int num28 = Math.Max(Math.Max(num23, num25), num27);
						if (num28 > 255)
						{
							num21 = (float)num28 / 255f;
							num23 = (int)Math.Round((double)(((float)num23 + 0f) / num21));
							num25 = (int)Math.Round((double)(((float)num25 + 0f) / num21));
							num27 = (int)Math.Round((double)(((float)num27 + 0f) / num21));
						}
						Color color5 = Color.FromArgb(num23, num25, num27);
						g.FillRectangle(new SolidBrush(color5), rect);
						g.DrawImage(_bmpPatternOfStrazaForDrawBlock, rect);
					}
				}
				else
				{
					DrawSourceImageOnPlaceOfRemovedBlock(g, bb);
				}
			}
		}

		public ResultOfSavingOrPrinting PrintMainScheme(SavingOrPrintingSettings settings)
		{
			return GenerateFullSchemeSource(settings);
		}

		public ResultOfSavingOrPrinting PrintReworkedScheme(SavingOrPrintingSettings settings)
		{
			return GenerateFullSchemeRework(settings);
		}

		public ResultOfSavingOrPrinting PrintHandEditingScheme(SavingOrPrintingSettings settings)
		{
			return GenerateFullSchemeHandEditing(settings);
		}

		public ResultOfSavingOrPrinting SaveMainSchemeAtDirectory(SavingOrPrintingSettings settings)
		{
			return GenerateFullSchemeSource(settings);
		}

		public ResultOfSavingOrPrinting SaveReworkedSchemeAtDirectory(SavingOrPrintingSettings settings)
		{
			return GenerateFullSchemeRework(settings);
		}

		public ResultOfSavingOrPrinting SaveHandEditingSchemeAtDirectory(SavingOrPrintingSettings settings)
		{
			return GenerateFullSchemeHandEditing(settings);
		}

		public void RemoveAloneBlocks()
		{
			float num = 3.40282347E+38f;
			Beads beads = null;
			List<Beads> list = new List<Beads>();
			int num2 = -1;
			for (int i = 0; i < Param.HeightInBead; i++)
			{
				for (int j = 0; j < Param.WidthInBead; j++)
				{
					if (blocksRework[j, i].IsVisible)
					{
						try
						{
							Beads resBeads = blocksRework[j, i].ResBeads;
							num = 3.40282347E+38f;
							beads = null;
							num2++;
							list.Add(resBeads);
							try
							{
								if (blocksRework[j - 1, i - 1].IsVisible)
								{
									if (blocksRework[j - 1, i - 1].ResBeads == resBeads)
									{
										continue;
									}
									float distanceBetweenColors = GetDistanceBetweenColors(blocksRework[j - 1, i - 1].ResBeads.clr, resBeads.clr);
									if (distanceBetweenColors < num)
									{
										num = distanceBetweenColors;
										beads = blocksRework[j - 1, i - 1].ResBeads;
									}
								}
							}
							catch (Exception)
							{
							}
							try
							{
								if (blocksRework[j, i - 1].IsVisible)
								{
									if (blocksRework[j, i - 1].ResBeads == resBeads)
									{
										continue;
									}
									float distanceBetweenColors2 = GetDistanceBetweenColors(blocksRework[j, i - 1].ResBeads.clr, resBeads.clr);
									if (distanceBetweenColors2 < num)
									{
										num = distanceBetweenColors2;
										beads = blocksRework[j, i - 1].ResBeads;
									}
								}
							}
							catch (Exception)
							{
							}
							try
							{
								if (blocksRework[j + 1, i - 1].IsVisible)
								{
									if (blocksRework[j + 1, i - 1].ResBeads == resBeads)
									{
										continue;
									}
									float distanceBetweenColors3 = GetDistanceBetweenColors(blocksRework[j + 1, i - 1].ResBeads.clr, resBeads.clr);
									if (distanceBetweenColors3 < num)
									{
										num = distanceBetweenColors3;
										beads = blocksRework[j + 1, i - 1].ResBeads;
									}
								}
							}
							catch (Exception)
							{
							}
							try
							{
								if (blocksRework[j - 1, i].IsVisible)
								{
									if (blocksRework[j - 1, i].ResBeads == resBeads)
									{
										continue;
									}
									float distanceBetweenColors4 = GetDistanceBetweenColors(blocksRework[j - 1, i].ResBeads.clr, resBeads.clr);
									if (distanceBetweenColors4 < num)
									{
										num = distanceBetweenColors4;
										beads = blocksRework[j - 1, i].ResBeads;
									}
								}
							}
							catch (Exception)
							{
							}
							try
							{
								if (blocksRework[j + 1, i].IsVisible)
								{
									if (blocksRework[j + 1, i].ResBeads == resBeads)
									{
										continue;
									}
									float distanceBetweenColors5 = GetDistanceBetweenColors(blocksRework[j + 1, i].ResBeads.clr, resBeads.clr);
									if (distanceBetweenColors5 < num)
									{
										num = distanceBetweenColors5;
										beads = blocksRework[j + 1, i].ResBeads;
									}
								}
							}
							catch (Exception)
							{
							}
							try
							{
								if (blocksRework[j - 1, i + 1].IsVisible)
								{
									if (blocksRework[j - 1, i + 1].ResBeads == resBeads)
									{
										continue;
									}
									float distanceBetweenColors6 = GetDistanceBetweenColors(blocksRework[j - 1, i + 1].ResBeads.clr, resBeads.clr);
									if (distanceBetweenColors6 < num)
									{
										num = distanceBetweenColors6;
										beads = blocksRework[j - 1, i + 1].ResBeads;
									}
								}
							}
							catch (Exception)
							{
							}
							try
							{
								if (blocksRework[j, i + 1].IsVisible)
								{
									if (blocksRework[j, i + 1].ResBeads == resBeads)
									{
										continue;
									}
									float distanceBetweenColors7 = GetDistanceBetweenColors(blocksRework[j, i + 1].ResBeads.clr, resBeads.clr);
									if (distanceBetweenColors7 < num)
									{
										num = distanceBetweenColors7;
										beads = blocksRework[j, i + 1].ResBeads;
									}
								}
							}
							catch (Exception)
							{
							}
							try
							{
								if (blocksRework[j + 1, i + 1].IsVisible)
								{
									if (blocksRework[j + 1, i + 1].ResBeads == resBeads)
									{
										continue;
									}
									float distanceBetweenColors8 = GetDistanceBetweenColors(blocksRework[j + 1, i + 1].ResBeads.clr, resBeads.clr);
									if (distanceBetweenColors8 < num)
									{
										num = distanceBetweenColors8;
										beads = blocksRework[j + 1, i + 1].ResBeads;
									}
								}
							}
							catch (Exception)
							{
							}
							if (beads != (Beads)null)
							{
								blocksRework[j, i].ResBeads = beads;
								list[num2] = beads;
							}
						}
						catch (Exception)
						{
						}
					}
				}
			}
			list = list.Distinct().ToList();
			for (int k = 0; k < _resBeadsRework.Count(); k++)
			{
				if (!list.Contains(_resBeadsRework[k]))
				{
					_resBeadsRework.RemoveAt(k);
					k--;
				}
			}
			DrawReworkEmbroideryView();
			IsReworkSchemeEdited = true;
		}

		public Dictionary<Block, Block> FillAreaOnManualEditingScheme(Beads newBeads, Point blockCoord)
		{
			Block block = blocksHandEditing[blockCoord.X, blockCoord.Y];
			bool isVisible = block.IsVisible;
			Beads resBeads = block.ResBeads;
			List<Block> list = new List<Block>();
			SeedFill_3(blockCoord.X, blockCoord.Y, newBeads, ref blocksHandEditing, ref list);
			Dictionary<Block, Block> dictionary = new Dictionary<Block, Block>();
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				foreach (Block item in list)
				{
					Block block2 = blocksHandEditing[item.X, item.Y];
					Block value = new Block
					{
						X = block2.X,
						Y = block2.Y,
						ResBeads = resBeads
					};
					dictionary.Add(block2, value);
				}
			}
			else
			{
				foreach (Block item2 in list)
				{
					Block block3 = blocksHandEditing[item2.X, item2.Y];
					Block value2 = new Block
					{
						X = block3.X,
						Y = block3.Y,
						ResBeads = resBeads,
						IsVisible = isVisible,
						PositionInScheme = block3.PositionInScheme
					};
					dictionary.Add(block3, value2);
				}
			}
			if (!_resBeadsHandEditing.Any((Beads b) => b == newBeads))
			{
				_resBeadsHandEditing.Add(newBeads);
			}
			return dictionary;
		}

		private void LineFill_3(int x1, int x2, int y, Beads newBeads, Beads oldBeads, bool isFillInvisible, ref Block[,] arrayBlocks, ref List<Block> blocksToChanging)
		{
			if (y >= 0 && y < Param.HeightInBead)
			{
				int num = x1;
				while (num >= 0 && arrayBlocks[num, y].IsItFullStitch.Value)
				{
					if (!isFillInvisible || arrayBlocks[num, y].IsVisible)
					{
						if (isFillInvisible)
						{
							break;
						}
						if (!arrayBlocks[num, y].IsVisible)
						{
							break;
						}
						if (!(arrayBlocks[num, y].ResBeads == oldBeads))
						{
							break;
						}
					}
					ChangeColor(num, y, newBeads, ref arrayBlocks, ref blocksToChanging);
					num--;
				}
				if (num < 0)
				{
					num = 0;
				}
				if (num < x1 - 2)
				{
					LineFill_3(num + 1, x1 - 1, y - 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
					LineFill_3(num + 1, x1 - 1, y + 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
				}
				else if (num < x1 - 1)
				{
					LineFill_3(num + 1, x1, y - 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
					LineFill_3(num + 1, x1, y + 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
				}
				else if (num < x1)
				{
					LineFill_3(num, x1, y - 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
					LineFill_3(num, x1, y + 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
				}
				int num2 = x2;
				if (x1 == x2 && arrayBlocks[x2, y].ResBeads == newBeads)
				{
					num2 = x2 + 1;
				}
				int i;
				for (i = num2; i < Param.WidthInBead; i++)
				{
					try
					{
						if (arrayBlocks[i, y].IsItFullStitch.Value)
						{
							if (isFillInvisible && !arrayBlocks[i, y].IsVisible)
							{
								goto IL_01aa;
							}
							if (!isFillInvisible && arrayBlocks[i, y].IsVisible && arrayBlocks[i, y].ResBeads == oldBeads)
							{
								goto IL_01aa;
							}
						}
						goto end_IL_0150;
						IL_01aa:
						ChangeColor(i, y, newBeads, ref arrayBlocks, ref blocksToChanging);
						continue;
						end_IL_0150:;
					}
					catch (Exception)
					{
						continue;
					}
					break;
				}
				try
				{
					if (x2 < i - 2)
					{
						LineFill_3(x2 + 1, i - 1, y - 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
						LineFill_3(x2 + 1, i - 1, y + 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
						x2--;
					}
					else if (x2 < i - 1)
					{
						LineFill_3(x2, i - 1, y - 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
						LineFill_3(x2, i - 1, y + 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
						x2--;
					}
					else if (x2 < i)
					{
						LineFill_3(x2, i, y - 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
						LineFill_3(x2, i, y + 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
						x2--;
					}
				}
				catch (Exception)
				{
				}
				for (i = x1; i <= x2 && i < Param.WidthInBead; i++)
				{
					try
					{
						if (arrayBlocks[i, y].IsItFullStitch.Value)
						{
							if (isFillInvisible && !arrayBlocks[i, y].IsVisible)
							{
								goto IL_02e3;
							}
							if (!isFillInvisible && arrayBlocks[i, y].IsVisible && arrayBlocks[i, y].ResBeads == oldBeads)
							{
								goto IL_02e3;
							}
						}
						if (x1 < i)
						{
							LineFill_3(x1, i - 1, y - 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
							LineFill_3(x1, i - 1, y + 1, newBeads, oldBeads, isFillInvisible, ref arrayBlocks, ref blocksToChanging);
							x1 = i;
						}
						for (; i <= x2 && i <= Param.WidthInBead - 1; i++)
						{
							if (arrayBlocks[i, y].IsItFullStitch.Value)
							{
								if (isFillInvisible && !arrayBlocks[i, y].IsVisible)
								{
									goto IL_0388;
								}
								if (!isFillInvisible && arrayBlocks[i, y].IsVisible && arrayBlocks[i, y].ResBeads == oldBeads)
								{
									goto IL_0388;
								}
							}
							continue;
							IL_0388:
							x1 = i--;
							break;
						}
						goto end_IL_028b;
						IL_02e3:
						ChangeColor(i, y, newBeads, ref arrayBlocks, ref blocksToChanging);
						end_IL_028b:;
					}
					catch (Exception)
					{
					}
				}
			}
		}

		private void ChangeColor(int x, int y, Beads newBeads, ref Block[,] arrayBlocks, ref List<Block> blocksToChanging)
		{
			try
			{
				arrayBlocks[x, y].ResBeads = newBeads;
				arrayBlocks[x, y].IsVisible = true;
				blocksToChanging.Add(arrayBlocks[x, y]);
			}
			catch (Exception)
			{
			}
		}

		private void SeedFill_3(int x, int y, Beads newbeads, ref Block[,] arrayBlocks, ref List<Block> blocksToChanging)
		{
			bool flag = !arrayBlocks[x, y].IsVisible;
			Beads resBeads = arrayBlocks[x, y].ResBeads;
			if (!flag && !(newbeads != resBeads))
			{
				return;
			}
			LineFill_3(x, x, y, newbeads, resBeads, flag, ref arrayBlocks, ref blocksToChanging);
		}

		public void DrawBlocksWithNewBeadsOnEmbroideryInHandEditing(List<Block> newBlocks)
		{
			IsHandEditingSchemeEdited = true;
			IsNeedToSave = true;
			if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
			{
				Graphics graphics = Graphics.FromImage(BmpBeadworkHandEditing);
				if (Param.IsEmbroideryViewInRealView)
				{
					graphics.SmoothingMode = SmoothingMode.AntiAlias;
					foreach (Block newBlock in newBlocks)
					{
						DrawRealViewOfBlock(graphics, newBlock, null, null);
					}
				}
				else
				{
					foreach (Block newBlock2 in newBlocks)
					{
						DrawKubicViewOfBlock(graphics, newBlock2, null, null);
					}
				}
				graphics.Dispose();
			}
			else if (newBlocks.Any())
			{
				IEnumerable<int> source = from bl in newBlocks
				select bl.Y;
				int num = source.Min();
				int num2 = source.Max();
				source = null;
				int num3 = num2 - num + 1;
				BitmapData bitmapData = BmpBeadworkHandEditing.LockBits(new Rectangle(0, 0, BmpBeadworkHandEditing.Width, num3), ImageLockMode.WriteOnly, BmpBeadworkHandEditing.PixelFormat);
				int num4 = (BmpBeadworkHandEditing.PixelFormat == (BmpBeadworkHandEditing.PixelFormat | PixelFormat.Alpha)) ? 4 : 3;
				int num5 = BmpBeadworkHandEditing.Width * num3 * num4;
				byte[] array = new byte[num5];
				IntPtr intPtr = bitmapData.Scan0 + num * bitmapData.Stride;
				Marshal.Copy(intPtr, array, 0, array.Length);
				foreach (Block newBlock3 in newBlocks)
				{
					Color clr = newBlock3.ResBeads.clr;
					int num6 = (newBlock3.Y - num) * bitmapData.Stride + newBlock3.X * num4;
					array[num6] = clr.B;
					array[num6 + 1] = clr.G;
					array[num6 + 2] = clr.R;
				}
				Marshal.Copy(array, 0, intPtr, array.Length);
				BmpBeadworkHandEditing.UnlockBits(bitmapData);
				bitmapData = null;
				array = null;
			}
		}

		public void MoveToReworkSchemeFromCreate()
		{
			MoveScheme(_resBeadsMain, ref _resBeadsRework, blocksMain, ref blocksRework, BmpBeadworkSource, ref BmpBeadworkRework);
			IsReworkSchemeLoaded = true;
			IsReworkSchemeEdited = false;
		}

		public void MoveToReworkSchemeFromHandEditing()
		{
			MoveScheme(_resBeadsHandEditing, ref _resBeadsRework, blocksHandEditing, ref blocksRework, BmpBeadworkHandEditing, ref BmpBeadworkRework);
			IsReworkSchemeLoaded = true;
			IsReworkSchemeEdited = false;
		}

		public void MoveToHandEditingSchemeFromCreate()
		{
			MoveScheme(_resBeadsMain, ref _resBeadsHandEditing, blocksMain, ref blocksHandEditing, BmpBeadworkSource, ref BmpBeadworkHandEditing);
			if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				CreateBmpBackAndKnot();
			}
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				CleanComputerSourceInTabCreateAndGCCollect();
			}
			IsHandEditingSchemeLoaded = true;
			IsHandEditingSchemeEdited = false;
		}

		private void CleanComputerSourceInTabCreateAndGCCollect()
		{
			_resBeadsMain.Clear();
			_resBeadsMain = null;
			blocksMain = new Pixel[1, 1];
			blocksMain = null;
			BmpBeadworkSource.Dispose();
			BmpBeadworkSource = null;
			GC.Collect(GC.MaxGeneration);
		}

		private void CreateBmpBackAndKnot()
		{
			knotHandEditing.Clear();
			backstitchHandEditing.Clear();
			if (BmpBackAndKnot != null)
			{
				BmpBackAndKnot.Dispose();
			}
			BmpBackAndKnot = new Bitmap(CalcBmpBackAndKnotWidthInPx(), CalcBmpBackAndKnotHeightInPx());
		}

		private int CalcBmpBackAndKnotHeightInPx()
		{
			if (BmpBeadworkHandEditing.Height * BmpBeadworkHandEditing.Width < 4500000)
			{
				return BmpBeadworkHandEditing.Height;
			}
			return (int)Math.Round((double)((float)BmpBeadworkHandEditing.Height / 2.5f));
		}

		private int CalcBmpBackAndKnotWidthInPx()
		{
			if (BmpBeadworkHandEditing.Height * BmpBeadworkHandEditing.Width < 4500000)
			{
				return BmpBeadworkHandEditing.Width;
			}
			return (int)Math.Round((double)((float)BmpBeadworkHandEditing.Width / 2.5f));
		}

		private void MoveScheme(List<Beads> srcBeads, ref List<Beads> resBeads, Pixel[,] srcBlock, ref Block[,] resBlock, Bitmap srcBmp, ref Bitmap resBmp)
		{
			resBeads = new List<Beads>();
			foreach (Beads srcBead in srcBeads)
			{
				resBeads.Add(srcBead);
			}
			resBlock = new Block[Param.WidthInBead, Param.HeightInBead];
			if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
			{
				for (int i = 0; i < Param.HeightInBead; i++)
				{
					for (int j = 0; j < Param.WidthInBead - 1; j++)
					{
						Block[,] obj = resBlock;
						int num = j;
						int num2 = i;
						Block copy = (srcBlock[j, i] as Block).GetCopy(false);
						obj[num, num2] = copy;
					}
				}
				if (Param.Type == SchemeType.Normal)
				{
					for (int k = 0; k < Param.HeightInBead; k++)
					{
						Block[,] obj2 = resBlock;
						int num3 = Param.WidthInBead - 1;
						int num4 = k;
						Block copy2 = (srcBlock[Param.WidthInBead - 1, k] as Block).GetCopy(false);
						obj2[num3, num4] = copy2;
					}
				}
				else
				{
					for (int l = 0; l < Param.HeightInBead; l += 2)
					{
						Block[,] obj3 = resBlock;
						int num5 = Param.WidthInBead - 1;
						int num6 = l;
						Block copy3 = (srcBlock[Param.WidthInBead - 1, l] as Block).GetCopy(false);
						obj3[num5, num6] = copy3;
					}
				}
			}
			else
			{
				for (int m = 0; m < Param.HeightInBead; m++)
				{
					for (int n = 0; n < Param.WidthInBead - 1; n++)
					{
						Block[,] obj4 = resBlock;
						int num7 = n;
						int num8 = m;
						Block copy4 = (srcBlock[n, m] as PixelForColoring).GetCopy(n, m, resBeads);
						obj4[num7, num8] = copy4;
					}
				}
				if (Param.Type == SchemeType.Normal)
				{
					for (int num9 = 0; num9 < Param.HeightInBead; num9++)
					{
						Block[,] obj5 = resBlock;
						int num10 = Param.WidthInBead - 1;
						int num11 = num9;
						Block copy5 = (srcBlock[Param.WidthInBead - 1, num9] as PixelForColoring).GetCopy(Param.WidthInBead - 1, num9, resBeads);
						obj5[num10, num11] = copy5;
					}
				}
			}
			resBmp = srcBmp.Clone(new Rectangle(0, 0, srcBmp.Width, srcBmp.Height), srcBmp.PixelFormat);
		}

		public void MoveToHandEditingSchemeFromRework()
		{
			MoveScheme(_resBeadsRework, ref _resBeadsHandEditing, blocksRework, ref blocksHandEditing, BmpBeadworkRework, ref BmpBeadworkHandEditing);
			if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				CreateBmpBackAndKnot();
			}
			IsHandEditingSchemeLoaded = true;
			IsHandEditingSchemeEdited = false;
		}

		internal List<Block> ReplaceColorOnHandEditingScheme(Beads srcBeads, Beads resBeads)
		{
			lastActionWithHandEditingScheme = Action.removingColor;
			if (Param.BiserType == BiserType.ForManufacturers)
			{
				_resBeadsHandEditing.Remove(srcBeads);
			}
			else
			{
				Beads item = _resBeadsHandEditing.First((Beads bbb) => bbb == srcBeads);
				_resBeadsHandEditing.Remove(item);
			}
			List<Beads> list = new List<Beads>();
			List<Beads> list2 = new List<Beads>();
			List<Block> list3 = new List<Block>();
			List<Block> list4 = new List<Block>();
			for (int i = 0; i < Param.HeightInBead; i++)
			{
				int num = Param.WidthInBead;
				if (Param.Type == SchemeType.Block && i % 2 != 0)
				{
					num--;
				}
				for (int j = 0; j < num; j++)
				{
					Block block = blocksHandEditing[j, i];
					if (block.IsVisible)
					{
						if (block.IsItFullStitch.Value && srcBeads == block.ResBeads)
						{
							goto IL_01fa;
						}
						if ((!block.IsItFullStitch).Value)
						{
							if (block.leftTop != null && block.leftTop.typeOfStitch != TypeOfStitch.emptyCanvas && srcBeads == block.leftTop.ResBeads)
							{
								goto IL_01fa;
							}
							if (block.rightTop != null && block.rightTop.typeOfStitch != TypeOfStitch.emptyCanvas && srcBeads == block.rightTop.ResBeads)
							{
								goto IL_01fa;
							}
							if (block.rightBottom != null && block.rightBottom.typeOfStitch != TypeOfStitch.emptyCanvas && srcBeads == block.rightBottom.ResBeads)
							{
								goto IL_01fa;
							}
							if (block.leftBottom != null && block.leftBottom.typeOfStitch != TypeOfStitch.emptyCanvas && srcBeads == block.leftBottom.ResBeads)
							{
								goto IL_01fa;
							}
						}
						list.Add(block.ResBeads);
					}
					continue;
					IL_01fa:
					list2.Clear();
					if (block.IsItFullStitch.Value)
					{
						list2.Add(block.ResBeads);
					}
					else
					{
						if (block.leftTop != null && block.leftTop.ResBeads != (Beads)null)
						{
							list2.Add(block.leftTop.ResBeads);
						}
						if (block.rightTop != null && block.rightTop.ResBeads != (Beads)null)
						{
							list2.Add(block.rightTop.ResBeads);
						}
						if (block.rightBottom != null && block.rightBottom.ResBeads != (Beads)null)
						{
							list2.Add(block.rightBottom.ResBeads);
						}
						if (block.leftBottom != null && block.leftBottom.ResBeads != (Beads)null)
						{
							list2.Add(block.leftBottom.ResBeads);
						}
					}
					Block copy = block.GetCopy(false);
					if (list2.Any((Beads pb) => pb == srcBeads))
					{
						block.ResBeads = resBeads;
						if (!block.IsItFullStitch.Value)
						{
							if (block.leftTop != null && block.leftTop.ResBeads == srcBeads)
							{
								block.leftTop.ResBeads = resBeads;
							}
							if (block.rightTop != null && block.rightTop.ResBeads == srcBeads)
							{
								block.rightTop.ResBeads = resBeads;
							}
							if (block.rightBottom != null && block.rightBottom.ResBeads == srcBeads)
							{
								block.rightBottom.ResBeads = resBeads;
							}
							if (block.leftBottom != null && block.leftBottom.ResBeads == srcBeads)
							{
								block.leftBottom.ResBeads = resBeads;
							}
						}
						list3.Add(copy);
						list4.Add(block);
						list.Add(resBeads);
					}
					else
					{
						list.AddRange(list2);
					}
				}
			}
			_resBeadsHandEditing = list.Distinct().ToList();
			DrawBlocksWithNewBeadsOnEmbroideryInHandEditing(list4);
			IsHandEditingSchemeEdited = true;
			IsNeedToSave = true;
			return list3;
		}

		public Dictionary<Block, Block> ChangeBlocksInDrawingBrushOnHandEditingScheme_ForNOTRaskraska(List<PointF> coordsOfBlocks, Beads newBeads, TypeOfStitch typeOfStitch)
		{
			Dictionary<Block, Block> dictionary = new Dictionary<Block, Block>();
			foreach (PointF coordsOfBlock in coordsOfBlocks)
			{
				Block block = blocksHandEditing[(int)coordsOfBlock.X, (int)coordsOfBlock.Y];
				if (block != null && (!block.IsVisible || (block.IsItFullStitch.Value && block.ResBeads.clr != newBeads.clr) || (!block.IsItFullStitch.Value && typeOfStitch == TypeOfStitch.full) || typeOfStitch != 0))
				{
					bool flag = true;
					if (typeOfStitch != 0 && block.leftTop != null && block.rightTop != null && block.rightBottom != null && block.leftBottom != null && (typeOfStitch == TypeOfStitch.half || typeOfStitch == TypeOfStitch.three_qarter || typeOfStitch == TypeOfStitch.full_quarter || typeOfStitch == TypeOfStitch.half_quarter))
					{
						if (coordsOfBlock.X - (float)(int)coordsOfBlock.X + (coordsOfBlock.Y - (float)(int)coordsOfBlock.Y) == 0f)
						{
							if (block.rightTop.typeOfStitch == TypeOfStitch.emptyCanvas && block.leftBottom.typeOfStitch == TypeOfStitch.emptyCanvas && block.rightBottom.typeOfStitch == TypeOfStitch.emptyCanvas && block.leftTop.typeOfStitch == typeOfStitch && block.leftTop.ResBeads != (Beads)null && block.leftTop.ResBeads == newBeads)
							{
								flag = false;
							}
						}
						else
						{
							bool flag2 = coordsOfBlock.Y - (float)(int)coordsOfBlock.Y <= 0.5f;
							bool flag3 = coordsOfBlock.X - (float)(int)coordsOfBlock.X <= 0.5f;
							if ((flag3 & flag2) && block.leftTop.typeOfStitch == typeOfStitch && block.leftTop.ResBeads != (Beads)null && block.leftTop.ResBeads == newBeads)
							{
								flag = false;
							}
							else if ((!flag3 & flag2) && block.rightTop.typeOfStitch == typeOfStitch && block.rightTop.ResBeads != (Beads)null && block.rightTop.ResBeads == newBeads)
							{
								flag = false;
							}
							else if (flag3 && !flag2 && block.leftBottom.typeOfStitch == typeOfStitch && block.leftBottom.ResBeads != (Beads)null && block.leftBottom.ResBeads == newBeads)
							{
								flag = false;
							}
							else if (!flag3 && !flag2 && block.rightBottom.typeOfStitch == typeOfStitch && block.rightBottom.ResBeads != (Beads)null && block.rightBottom.ResBeads == newBeads)
							{
								flag = false;
							}
						}
					}
					if (flag)
					{
						Block copy = block.GetCopy(false);
						block.IsVisible = true;
						switch (typeOfStitch)
						{
						case TypeOfStitch.full:
							block.ResBeads = newBeads;
							block.IsItFullStitch = true;
							block.leftTop = (block.rightTop = (block.leftBottom = (block.rightBottom = null)));
							break;
						case TypeOfStitch.half:
						case TypeOfStitch.full_quarter:
						case TypeOfStitch.half_quarter:
						case TypeOfStitch.three_qarter:
							block.IsItFullStitch = false;
							block.ResBeads = newBeads;
							if (coordsOfBlock.X - (float)(int)coordsOfBlock.X + (coordsOfBlock.Y - (float)(int)coordsOfBlock.Y) == 0f)
							{
								Block block2 = block;
								Block block3 = block;
								Block block4 = block;
								Stitch obj = new Stitch
								{
									typeOfStitch = TypeOfStitch.emptyCanvas
								};
								Stitch leftBottom = obj;
								block4.rightBottom = obj;
								block2.rightTop = (block3.leftBottom = leftBottom);
								block.leftTop = new Stitch
								{
									typeOfStitch = typeOfStitch,
									ResBeads = newBeads
								};
							}
							else
							{
								bool flag4 = coordsOfBlock.Y - (float)(int)coordsOfBlock.Y <= 0.5f;
								bool flag5 = coordsOfBlock.X - (float)(int)coordsOfBlock.X <= 0.5f;
								Stitch stitch = new Stitch
								{
									typeOfStitch = typeOfStitch,
									ResBeads = newBeads
								};
								if (flag5 & flag4)
								{
									block.leftTop = stitch;
								}
								else if (!flag5 & flag4)
								{
									block.rightTop = stitch;
								}
								else if (flag5 && !flag4)
								{
									block.leftBottom = stitch;
								}
								else if (!flag5 && !flag4)
								{
									block.rightBottom = stitch;
								}
								if (block.leftTop == null)
								{
									block.leftTop = new Stitch
									{
										typeOfStitch = TypeOfStitch.emptyCanvas
									};
								}
								if (block.rightTop == null)
								{
									block.rightTop = new Stitch
									{
										typeOfStitch = TypeOfStitch.emptyCanvas
									};
								}
								if (block.leftBottom == null)
								{
									block.leftBottom = new Stitch
									{
										typeOfStitch = TypeOfStitch.emptyCanvas
									};
								}
								if (block.rightBottom == null)
								{
									block.rightBottom = new Stitch
									{
										typeOfStitch = TypeOfStitch.emptyCanvas
									};
								}
								if (flag5 & flag4)
								{
									switch (typeOfStitch)
									{
									case TypeOfStitch.half:
										if (block.rightTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightBottom.typeOfStitch == TypeOfStitch.full_quarter || block.rightBottom.typeOfStitch == TypeOfStitch.half_quarter)
										{
											block.rightBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightBottom.typeOfStitch == TypeOfStitch.half)
										{
											block.rightBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										break;
									case TypeOfStitch.full_quarter:
									case TypeOfStitch.half_quarter:
										if (block.rightTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightBottom.typeOfStitch == TypeOfStitch.half)
										{
											block.rightBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										break;
									case TypeOfStitch.three_qarter:
										if (block.leftBottom.typeOfStitch == TypeOfStitch.full_quarter || block.leftBottom.typeOfStitch == TypeOfStitch.half_quarter || block.leftBottom.typeOfStitch == TypeOfStitch.half || block.leftBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightTop.typeOfStitch == TypeOfStitch.full_quarter || block.rightTop.typeOfStitch == TypeOfStitch.half_quarter || block.rightTop.typeOfStitch == TypeOfStitch.half || block.rightTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightBottom.typeOfStitch != TypeOfStitch.half && block.rightBottom.typeOfStitch != TypeOfStitch.three_qarter)
										{
											break;
										}
										block.rightBottom = new Stitch
										{
											typeOfStitch = TypeOfStitch.emptyCanvas
										};
										break;
									}
								}
								else if (!flag5 & flag4)
								{
									switch (typeOfStitch)
									{
									case TypeOfStitch.half:
										if (block.leftTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftBottom.typeOfStitch == TypeOfStitch.full_quarter || block.leftBottom.typeOfStitch == TypeOfStitch.half_quarter)
										{
											block.leftBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftBottom.typeOfStitch == TypeOfStitch.half)
										{
											block.leftBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										break;
									case TypeOfStitch.full_quarter:
									case TypeOfStitch.half_quarter:
										if (block.leftTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftBottom.typeOfStitch == TypeOfStitch.half)
										{
											block.leftBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										break;
									case TypeOfStitch.three_qarter:
										if (block.leftTop.typeOfStitch == TypeOfStitch.full_quarter || block.leftTop.typeOfStitch == TypeOfStitch.half_quarter || block.leftTop.typeOfStitch == TypeOfStitch.half || block.leftTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightBottom.typeOfStitch == TypeOfStitch.full_quarter || block.rightBottom.typeOfStitch == TypeOfStitch.half_quarter || block.rightBottom.typeOfStitch == TypeOfStitch.half || block.rightBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftBottom.typeOfStitch != TypeOfStitch.half && block.leftBottom.typeOfStitch != TypeOfStitch.three_qarter)
										{
											break;
										}
										block.leftBottom = new Stitch
										{
											typeOfStitch = TypeOfStitch.emptyCanvas
										};
										break;
									}
								}
								else if (flag5 && !flag4)
								{
									switch (typeOfStitch)
									{
									case TypeOfStitch.half:
										if (block.leftTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightTop.typeOfStitch == TypeOfStitch.full_quarter || block.rightTop.typeOfStitch == TypeOfStitch.half_quarter)
										{
											block.rightTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightTop.typeOfStitch == TypeOfStitch.half)
										{
											block.rightTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										break;
									case TypeOfStitch.full_quarter:
									case TypeOfStitch.half_quarter:
										if (block.leftTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightTop.typeOfStitch == TypeOfStitch.half)
										{
											block.rightTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										break;
									case TypeOfStitch.three_qarter:
										if (block.leftTop.typeOfStitch == TypeOfStitch.full_quarter || block.leftTop.typeOfStitch == TypeOfStitch.half_quarter || block.leftTop.typeOfStitch == TypeOfStitch.half || block.leftTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightBottom.typeOfStitch == TypeOfStitch.full_quarter || block.rightBottom.typeOfStitch == TypeOfStitch.half_quarter || block.rightBottom.typeOfStitch == TypeOfStitch.half || block.rightBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightTop.typeOfStitch != TypeOfStitch.half && block.rightTop.typeOfStitch != TypeOfStitch.three_qarter)
										{
											break;
										}
										block.rightTop = new Stitch
										{
											typeOfStitch = TypeOfStitch.emptyCanvas
										};
										break;
									}
								}
								else if (!flag5 && !flag4)
								{
									switch (typeOfStitch)
									{
									case TypeOfStitch.half:
										if (block.rightTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftTop.typeOfStitch == TypeOfStitch.full_quarter || block.leftTop.typeOfStitch == TypeOfStitch.half_quarter)
										{
											block.leftTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftTop.typeOfStitch == TypeOfStitch.half)
										{
											block.leftTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										break;
									case TypeOfStitch.full_quarter:
									case TypeOfStitch.half_quarter:
										if (block.rightTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftTop.typeOfStitch == TypeOfStitch.half)
										{
											block.leftTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										break;
									case TypeOfStitch.three_qarter:
										if (block.leftBottom.typeOfStitch == TypeOfStitch.full_quarter || block.leftBottom.typeOfStitch == TypeOfStitch.half_quarter || block.leftBottom.typeOfStitch == TypeOfStitch.half || block.leftBottom.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.leftBottom = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.rightTop.typeOfStitch == TypeOfStitch.full_quarter || block.rightTop.typeOfStitch == TypeOfStitch.half_quarter || block.rightTop.typeOfStitch == TypeOfStitch.half || block.rightTop.typeOfStitch == TypeOfStitch.three_qarter)
										{
											block.rightTop = new Stitch
											{
												typeOfStitch = TypeOfStitch.emptyCanvas
											};
										}
										if (block.leftTop.typeOfStitch != TypeOfStitch.half && block.leftTop.typeOfStitch != TypeOfStitch.three_qarter)
										{
											break;
										}
										block.leftTop = new Stitch
										{
											typeOfStitch = TypeOfStitch.emptyCanvas
										};
										break;
									}
								}
							}
							break;
						}
						dictionary.Add(block, copy);
					}
				}
			}
			if (!_resBeadsHandEditing.Any((Beads b) => b == newBeads))
			{
				_resBeadsHandEditing.Add(newBeads);
			}
			return dictionary;
		}

		public Dictionary<Block, Block> ChangeBlocksInDrawingBrushOnHandEditingScheme_ForRaskraska(List<Point> coordsOfBlocks, Beads newBeads)
		{
			Dictionary<Block, Block> dictionary = new Dictionary<Block, Block>();
			foreach (Point coordsOfBlock in coordsOfBlocks)
			{
				Block block = blocksHandEditing[coordsOfBlock.X, coordsOfBlock.Y];
				if (block.ResBeads.clr != newBeads.clr)
				{
					Block copy = block.GetCopy(true);
					block.ResBeads = newBeads;
					dictionary.Add(block, copy);
				}
			}
			if (!_resBeadsHandEditing.Any((Beads b) => b == newBeads))
			{
				_resBeadsHandEditing.Add(newBeads);
			}
			return dictionary;
		}

		internal void AddKnot(PointF point, Beads beads)
		{
			IsHandEditingSchemeEdited = true;
			knotHandEditing.Add(new Knot
			{
				bead = beads,
				point = point
			});
			RedrawBackstitchesAndKnots();
		}

		private void RedrawBackstitchesAndKnots()
		{
			Graphics graphics = Graphics.FromImage(BmpBackAndKnot);
			graphics.SmoothingMode = SmoothingMode.AntiAlias;
			graphics.Clear(Color.Transparent);
			float num = ((float)SqureCellSidePreview_X + 0f) * ((float)BmpBackAndKnot.Width + 0f) / ((float)BmpBeadworkHandEditing.Width + 0f);
			float width = num * 0.28f;
			foreach (BackStitch item in backstitchHandEditing)
			{
				graphics.DrawLine(new Pen(new SolidBrush(item.bead.clr), width), item.ptBegin.X * num, item.ptBegin.Y * num, item.ptEnd.X * num, item.ptEnd.Y * num);
			}
			int num2 = (int)Math.Round((double)(num * 0.85f));
			foreach (Knot item2 in knotHandEditing)
			{
				graphics.FillEllipse(new SolidBrush(item2.bead.clr), new Rectangle((int)(item2.point.X * num - ((float)num2 + 0f) / 2f), (int)(item2.point.Y * num - ((float)num2 + 0f) / 2f), num2, num2));
			}
			graphics.Dispose();
		}

		internal void RemoveLastKnot()
		{
			try
			{
				knotHandEditing.RemoveAt(knotHandEditing.Count() - 1);
				RedrawBackstitchesAndKnots();
			}
			catch
			{
			}
		}

		internal void AddBeginBackstitchPoint(PointF ptBegin, Beads beads)
		{
			IsHandEditingSchemeEdited = true;
			if (backstitchHandEditing.Count() == 0 || !backstitchHandEditing[backstitchHandEditing.Count() - 1].ptEnd.IsEmpty)
			{
				backstitchHandEditing.Add(new BackStitch
				{
					ptBegin = ptBegin,
					bead = beads
				});
			}
			else if (backstitchHandEditing.Count() > 0)
			{
				backstitchHandEditing[backstitchHandEditing.Count() - 1] = new BackStitch
				{
					ptBegin = ptBegin,
					bead = beads
				};
			}
		}

		internal void AddEndBackstitchPoint(PointF ptEnd)
		{
			if (backstitchHandEditing[backstitchHandEditing.Count() - 1].ptEnd.IsEmpty)
			{
				backstitchHandEditing[backstitchHandEditing.Count() - 1].ptEnd = ptEnd;
				RedrawBackstitchesAndKnots();
			}
		}

		internal void RemoveLastBackstitch()
		{
			try
			{
				backstitchHandEditing.RemoveAt(backstitchHandEditing.Count() - 1);
				RedrawBackstitchesAndKnots();
			}
			catch
			{
			}
		}

		internal bool IsItAnyNotEndedBackstitch()
		{
			if (backstitchHandEditing.Count() > 0)
			{
				return backstitchHandEditing.Last().ptEnd.IsEmpty;
			}
			return false;
		}

		internal Bitmap HighlightColorInHandEditingScheme(Beads beads)
		{
			Bitmap bitmap = (Bitmap)BmpBeadworkHandEditing.Clone();
			Graphics graphics = Graphics.FromImage(bitmap);
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				List<Block> list = (from Block b in blocksHandEditing
				where b.ResBeads == beads
				select b).ToList();
				Pen pen = new Pen(Brushes.Cyan);
				foreach (Block item in list)
				{
					graphics.DrawRectangle(pen, new Rectangle(item.X, item.Y, 1, 1));
				}
			}
			else
			{
				List<Block> list = (Settings.hobbyProgramm != 0) ? blocksHandEditing.Cast<Block>().Where(delegate(Block b)
				{
					if (b != null && b.IsVisible)
					{
						return b.ResBeads == beads;
					}
					return false;
				}).ToList() : blocksHandEditing.Cast<Block>().Where(delegate(Block b)
				{
					if (b != null && b.IsVisible)
					{
						if (b.IsItFullStitch.Value && b.ResBeads == beads)
						{
							return true;
						}
						if ((!b.IsItFullStitch).Value)
						{
							if (b.leftTop != null && b.leftTop.typeOfStitch != TypeOfStitch.emptyCanvas && beads == b.leftTop.ResBeads)
							{
								goto IL_0127;
							}
							if (b.rightTop != null && b.rightTop.typeOfStitch != TypeOfStitch.emptyCanvas && beads == b.rightTop.ResBeads)
							{
								goto IL_0127;
							}
							if (b.rightBottom != null && b.rightBottom.typeOfStitch != TypeOfStitch.emptyCanvas && beads == b.rightBottom.ResBeads)
							{
								goto IL_0127;
							}
							if (b.leftBottom != null && b.leftBottom.typeOfStitch != TypeOfStitch.emptyCanvas)
							{
								return beads == b.leftBottom.ResBeads;
							}
							return false;
						}
						return false;
					}
					return false;
					IL_0127:
					return true;
				}).ToList();
				if (Param.IsEmbroideryViewInRealView)
				{
					graphics.SmoothingMode = SmoothingMode.AntiAlias;
					foreach (Block item2 in list)
					{
						DrawRealViewOfBlock(graphics, item2, Brushes.Cyan, beads);
					}
				}
				else
				{
					foreach (Block item3 in list)
					{
						DrawKubicViewOfBlock(graphics, item3, Brushes.Cyan, beads);
					}
				}
			}
			graphics.Dispose();
			return bitmap;
		}

		internal void UpdateSourceImage()
		{
		}

		internal int CalculatePriceOfScheme()
		{
			int result = 3000;
			if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
			{
				float num = ((float)Param.WidthInBead + 0f) * ((float)Param.HeightInBead + 0f) * 6.25f / 10000f;
				result = (int)Math.Round((double)(0.0017f * num * num * num - 0.4793f * num * num) + 62.681 * (double)num + 1047.5);
				result = (int)Math.Round((double)(((float)result + 0f) / 10f)) * 10;
			}
			return result;
		}
	}
}
