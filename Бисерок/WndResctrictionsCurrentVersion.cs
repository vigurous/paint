using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace Бисерок
{
	public partial class WndResctrictionsCurrentVersion : Window, IComponentConnector
	{
		private int v;

		public WndResctrictionsCurrentVersion(int ve)
		{
			InitializeComponent();
			v = ve;
			switch (v)
			{
			case 0:
				base.Title = "Ограничения тестовой версии программы";
				tbiMainTab.IsSelected = true;
				tblCommonDescriptionVersion.Text = "Тестовая версия создана лишь для ознакомления с возможностями программы (поэтому полноценно использовать её не получится):";
				break;
			case 1:
				if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
				{
					base.Title = "Ограничения базовой версии программы";
					tbiMainTab.Visibility = Visibility.Collapsed;
					tbiReworkTab.IsSelected = true;
					tblCommonDescriptionVersion.Text = "В базовой версии программы Вы можете полноценно использовать лишь первую вкладку 'Шаг 1. Создание схемы'. В другие две вкладки внедрены ограничения, чтобы использовать их было нельзя:";
				}
				else
				{
					base.Title = "Ограничения домашней версии программы";
					tblCommonDescriptionVersion.Text = "В домашней версии Вы уже можете полноценно использовать программу, но с ограничениями, которые полностью сняты в Профессиональной и Коммерческой версиях:";
				}
				break;
			case 2:
			{
				if (Settings.hobbyProgramm != HobbyProgramm.Raskraska)
				{
					tblCommonDescriptionVersion.Text = "В продвинутой версии программы Вы можете полноценно использовать только первую и вторую вкладки программы ('Шаг 1. Создание схемы' и 'Шаг 2. Доработка'). В третью вкладку 'Ручное редактирование' внедрены ограничения, чтобы использовать её было нельзя:";
					base.Title = "Ограничения продвинутой версии программы";
				}
				else
				{
					tblCommonDescriptionVersion.Text = "В профессиональной версии программы Вы можете полноценно использовать все возможности первой вкладки программы 'Шаг 1. Создание картины по номерам'. Во вторую вкладку 'Шаг 2. Редактирование картины' внедрены ограничения, чтобы использовать её было нельзя:";
					base.Title = "Ограничения профессиональной версии программы";
				}
				TabItem tabItem = tbiMainTab;
				TabItem tabItem2 = tbiReworkTab;
				Visibility visibility2 = tabItem2.Visibility = Visibility.Collapsed;
				tabItem.Visibility = visibility2;
				tbiHandEditingTab.IsSelected = true;
				break;
			}
			}
			List<string> list = new List<string>();
			List<string> list2 = new List<string>();
			List<string> list3 = new List<string>();
			List<string> list4 = new List<string>();
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska && Settings.v == 1)
			{
				list.Add("1. Ограничение по числу цветов своей палитры: до 12 цветов (в профессиональной версии число цветов своей палитры увеличено до 80. Больше - на спец.условиях)");
				list.Add("2. В картах ключей НЕ указывается площадь, закрашиваемая каждым цветом, и НЕ указывается необходимый объём каждого цвета в мл (в профессиональной версии всё это будет указано).");
				list.Add("3. Нет возможности ставить минимальные изменения исходного изображения при создании картины.");
				list.Add("4. Отсутствует возможность создавать схемы с высоким качеством контуров (имеется 'пикселизация' контуров) и настраивать качество и толщину контуров картины.");
				list.Add("5. Нет возможности проводить предобработку исходного изображения.");
				list.Add("6. Нет возможности создавать картины размером более, чем 120х120см (можно только в ком.версии).");
				this.textBlock7.Visibility = Visibility.Collapsed;
			}
			else
			{
				list.Add("1. Ограничение по числу цветов: можно создавать " + Settings.canvas + " лишь с 25 цветами");
				list.Add("2. Ограничение по размеру: " + Settings.canvas + " создаются лишь под размер 25см по большей стороне");
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					list.Add("3. Нельзя создавать картины под готовые палитры цветов (производителей красок) - доступен только 'Простой подбор цветов'");
					list.Add("4. Нельзя изменять настройки детализации создаваемой картины по номерам");
					list.Add("5. Нет возможности проводить предобработку исходного изображения.");
				}
				else
				{
					list.Add("3. Схемы создаются только под производителей " + Settings.materials + " , нельзя создать схемы для самостоятельного подбора цветов или выбрать нужные серии/номера " + Settings.materials);
					if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
					{
						list.Add("4. Нельзя создать схемы для вышивки 'кирпичиком' или 'мозаикой', только простые схемы");
						list.Add("5. Нет возможности проводить предобработку исходного изображения.");
					}
					else
					{
						list.Add("4. Нет возможности проводить предобработку исходного изображения.");
					}
				}
				list2.Add("1. Надписи 'тестовая/демо-версия' почти на всех результатах сохранения/печати");
				list2.Add("2. Сохраняется лишь первый лист легенды (карты цветов), остальные - пусты");
				if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
				{
					list2.Add("3. В карте цветов показываются лишь первые 6 цветов");
					list2.Add("4. Нет возможности создавать схемы с высоким качеством контуров (имеется 'пикселизация' контуров)");
				}
				else
				{
					list2.Add("3. На общей схеме показан лишь первый лист схемы, остальные листы искажены чередующимися пустыми квадратами");
					list2.Add("4. Сохраняется лишь первый лист схемы, остальные - пусты");
					list2.Add("5. В легенде не указываются необходимые каталожные номера " + Settings.materials);
					if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
					{
						list2.Add("6. В схеме для печати на ткани показывается лишь часть бисеринок, остальные отсутствуют (выглядит белой мозаикой)");
					}
					if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
					{
						list2.Add("6. В схеме для печати показывается лишь часть страз, остальные отсутствуют (выглядит белой мозаикой)");
					}
				}
			}
			list3.Add("1. Невозможно провести полноценную цветовую коррекцию (можно добавить/удалить не более 3 цветов схемы - только в целях демонстрации такой возможности)");
			list3.Add("2. Недоступно сохранение схемы");
			list3.Add("3. Недоступна печать схемы");
			list3.Add("4. Нет возможности создавать схемы размером более, чем 120х120см (можно только в мастер-версии)");
			if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				list3.Add("5. Недоступна возможность убрать одиночные крестики");
				if (Settings.v == 1)
				{
					list3.Add("6. Нет возможности проводить предобработку исходного изображения.");
				}
			}
			else if (Settings.v == 1)
			{
				list3.Add("5. Нет возможности проводить предобработку исходного изображения.");
			}
			list4.Add("1. Невозможно провести редактирование " + Settings.canvas + ": доступно не более 3 операций (нажатий кистью, заливок схемы либо замены цвета в схеме) - только в целях демонстрации такой возможности");
			list4.Add("2. Нельзя перенести " + Settings.canvas + " из вкладки 'Доработка' на вкладку 'Ручное редактирование'");
			list4.Add("3. Недоступно сохранение " + Settings.canvas);
			list4.Add("4. Недоступна печать " + Settings.canvas);
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				list4.Add("5. Нет возможности создавать картины размером более, чем 120х120см (можно только в ком.версии).");
				list4.Add("6. Нет возможности подключить постоянную нумерацию своих красок в карте цветов (легенде).");
				list4.Add("7. Вы не имеете права использовать программу в коммерческих целях. [При приобретении коммерческой версии мы направим Вам Соглашение на Ваше имя, заверенное нашей печатью, официально разрешающее коммерческое использование программы 'Раскраска'. Кроме того, при приобретении коммерческой версии мы предоставим Вам 3 дополнительных лицензии программы.]");
			}
			else if (Settings.v == 1)
			{
				list4.Add("5. Нет возможности создавать схемы размером более, чем 120х120см (можно только в мастер-версии).");
			}
			else if (Settings.v == 2)
			{
				list4.Add("5. Нет возможности проводить предобработку исходного изображения.");
				list4.Add("6. Нет возможности создавать схемы размером более, чем 120х120см (можно только в мастер-версии).");
			}
			switch (Settings.hobbyProgramm)
			{
			case HobbyProgramm.Biserok:
			{
				TextBlock textBlock4 = tblAboutFullVersion1;
				TextBlock textBlock5 = tblAboutFullVersion2;
				TextBlock textBlock6 = tblAboutFullVersion3;
				string text2 = textBlock6.Text = "В полной версии программы таких ограничений нет - она позволяет строить полнофункциональные схемы вышивки бисером из любых изображений!";
				string text5 = textBlock4.Text = (textBlock5.Text = text2);
				break;
			}
			case HobbyProgramm.Krestik:
			{
				base.Height = 600.0;
				TextBlock textBlock10 = tblAboutFullVersion1;
				TextBlock textBlock11 = tblAboutFullVersion2;
				TextBlock textBlock12 = tblAboutFullVersion3;
				string text2 = textBlock12.Text = "В полной версии программы таких ограничений нет - она позволяет строить полнофункциональные схемы вышивки крестом из любых изображений!";
				string text5 = textBlock10.Text = (textBlock11.Text = text2);
				break;
			}
			case HobbyProgramm.Petelka:
			{
				base.Height = 600.0;
				TextBlock textBlock7 = tblAboutFullVersion1;
				TextBlock textBlock8 = tblAboutFullVersion2;
				TextBlock textBlock9 = tblAboutFullVersion3;
				string text2 = textBlock9.Text = "В полной версии программы таких ограничений нет - она позволяет строить полнофункциональные схемы вязания из любых изображений!";
				string text5 = textBlock7.Text = (textBlock8.Text = text2);
				break;
			}
			case HobbyProgramm.Raskraska:
				base.Height = 600.0;
				tbiReworkTab.Visibility = Visibility.Collapsed;
				tbiMainTab.Header = "Вкладка 'Шаг 1. Создание картины по номерам'";
				this.textBlock2.Text = "Ограничения работы на вкладке 'Шаг 1. Создание картины по номерам':";
				tbiHandEditingTab.Header = "Вкладка 'Шаг 2. Редактирование картины'";
				textBlock17.Text = "Ограничения работы на вкладке 'Шаг 2. Редактирование картины'";
				if (v == 0)
				{
					tblAboutFullVersion1.Text = "В полной версии программы таких ограничений нет - она позволяет создавать полноценные картины по номерам из любых изображений!";
				}
				else if (v == 1)
				{
					tblAboutFullVersion1.Text = "В профессиональной версии этих ограничений нет!";
					tblBuyMainTab.Text = "Купить ПРОФЕССИОНАЛЬНУЮ версию программы! (через заказ на сайте)";
					tblAboutFullVersion3.Text = "В коммерческой версии этих ограничений нет - Вы сможете пользоваться ВСЕМИ возможностями программы!";
					tblBuyHandEditingTab.Text = "Купить КОММЕРЧЕСКУЮ версию программы! (через заказ на сайте)";
				}
				else if (v == 2)
				{
					tblAboutFullVersion3.Text = "В коммерческой версии этих ограничений нет - Вы сможете пользоваться ВСЕМИ возможностями программы!";
					tblBuyHandEditingTab.Text = "Купить КОММЕРЧЕСКУЮ версию программы! (через заказ на сайте)";
				}
				break;
			case HobbyProgramm.Almaz:
			{
				base.Height = 650.0;
				TextBlock textBlock = tblAboutFullVersion1;
				TextBlock textBlock2 = tblAboutFullVersion2;
				TextBlock textBlock3 = tblAboutFullVersion3;
				string text2 = textBlock3.Text = "В полной версии программы таких ограничений нет - она позволяет строить полнофункциональные схемы алмазной мозаики из любых изображений!";
				string text5 = textBlock.Text = (textBlock2.Text = text2);
				break;
			}
			}
			foreach (string item in list)
			{
				stpRestrictionsOnCreatingSchemeTab.Children.Add(GetStandrardTextBlockForListOfRestrictions(item));
			}
			foreach (string item2 in list2)
			{
				stpRestrictionsOfSavingOnCreatingSchemeTab.Children.Add(GetStandrardTextBlockForListOfRestrictions(item2));
			}
			foreach (string item3 in list3)
			{
				stpRestrictionsOnReworkingSchemeTab.Children.Add(GetStandrardTextBlockForListOfRestrictions(item3));
			}
			foreach (string item4 in list4)
			{
				stpRestrictionsOnHandEditingSchemeTab.Children.Add(GetStandrardTextBlockForListOfRestrictions(item4));
			}
		}

		private TextBlock GetStandrardTextBlockForListOfRestrictions(string text)
		{
			return new TextBlock
			{
				Text = text,
				FontSize = 15.0,
				Margin = new Thickness(10.0, 2.0, 10.0, 2.0),
				TextWrapping = TextWrapping.Wrap
			};
		}

		private void tblBuyHandEditingTab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			BuyProgram(3);
		}

		private void tblBuyReworkTab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			BuyProgram(2);
		}

		private void tblBuyMainTab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			BuyProgram(1);
		}

		private void BuyProgram(int vv)
		{
			Site.BuyProgram(vv);
		}
	}
}
