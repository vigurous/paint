using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace Бисерок
{
	[Serializable]
	public class Beads
	{
		public float squareInSm2;

		public string symbolForColoring_AndForVoiceWorkInBiserok;

		[NonSerialized]
		private SolidColorBrush _brushForWPF;

		private byte R;

		private byte G;

		private byte B;

		private System.Drawing.Color _clr;

		public System.Drawing.Color clrBorder;

		[NonSerialized]
		public Bitmap badge;

		public bool isUsing = true;

		public Dictionary<Beads, float> mixingColors;

		[NonSerialized]
		public bool IsCanvas;

		public Guid id
		{
			get;
			set;
		}

		public BeadsSeries parentSeries
		{
			get;
			set;
		}

		public SolidColorBrush brushForWPF
		{
			get
			{
				return _brushForWPF;
			}
			set
			{
				_brushForWPF = value;
				System.Windows.Media.Color color = _brushForWPF.Color;
				R = color.R;
				color = _brushForWPF.Color;
				G = color.G;
				color = _brushForWPF.Color;
				B = color.B;
			}
		}

		public System.Drawing.Color clr
		{
			get
			{
				return _clr;
			}
			set
			{
				_clr = value;
				System.Drawing.Color clr = this.clr;
				byte r = clr.R;
				clr = this.clr;
				byte g = clr.G;
				clr = this.clr;
				brushForWPF = new SolidColorBrush(System.Windows.Media.Color.FromArgb(byte.MaxValue, r, g, clr.B));
			}
		}

		public SolidBrush brush => new SolidBrush(clr);

		public int? countBeadsOfThisColor
		{
			get;
			set;
		}

		[DefaultValue("")]
		public string name
		{
			get;
			set;
		}

		[DefaultValue("")]
		public string description
		{
			get;
			set;
		}

		public Beads()
		{
			id = Guid.NewGuid();
		}

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			brushForWPF = new SolidColorBrush(System.Windows.Media.Color.FromRgb(R, G, B));
		}

		public static bool operator ==(Beads a, Beads b)
		{
			if ((object)a == b)
			{
				return true;
			}
			if ((object)a != null && (object)b != null)
			{
				if (a.name == null)
				{
					a.name = "";
				}
				if (b.name == null)
				{
					b.name = "";
				}
				bool flag = a.clr == b.clr && a.name == b.name;
				try
				{
					if (a.parentSeries != null)
					{
						if (a.parentSeries.name != null)
						{
							if (a.parentSeries.name != "")
							{
								if (b.parentSeries != null)
								{
									bool flag2 = a.parentSeries.parentManufacturer.name == b.parentSeries.parentManufacturer.name;
									flag &= flag2;
									return flag;
								}
								return flag;
							}
							return flag;
						}
						return flag;
					}
					return flag;
				}
				catch (Exception)
				{
					return flag;
				}
			}
			return false;
		}

		public static bool operator !=(Beads a, Beads b)
		{
			return !(a == b);
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			Beads beads = obj as Beads;
			if ((object)beads == null)
			{
				return false;
			}
			if (name == null)
			{
				name = "";
			}
			if (beads.name == null)
			{
				beads.name = "";
			}
			bool flag = clr == beads.clr && name == beads.name;
			try
			{
				if (parentSeries != null)
				{
					if (parentSeries.name != null)
					{
						if (parentSeries.name != "")
						{
							bool flag2 = parentSeries.parentManufacturer.name == beads.parentSeries.parentManufacturer.name;
							flag &= flag2;
							return flag;
						}
						return flag;
					}
					return flag;
				}
				return flag;
			}
			catch (Exception)
			{
				return flag;
			}
		}

		public bool Equals(Beads p)
		{
			if ((object)p == null)
			{
				return false;
			}
			if (name == null)
			{
				name = "";
			}
			if (p.name == null)
			{
				p.name = "";
			}
			bool flag = clr == p.clr && name == p.name;
			try
			{
				if (parentSeries != null)
				{
					if (parentSeries.name != null)
					{
						if (parentSeries.name != "")
						{
							bool flag2 = parentSeries.parentManufacturer.name == p.parentSeries.parentManufacturer.name;
							flag &= flag2;
							return flag;
						}
						return flag;
					}
					return flag;
				}
				return flag;
			}
			catch (Exception)
			{
				return flag;
			}
		}

		public override int GetHashCode()
		{
			return clr.GetHashCode();
		}
	}
}
