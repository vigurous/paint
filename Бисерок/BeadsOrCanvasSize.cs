namespace Бисерок
{
	public class BeadsOrCanvasSize
	{
		public string name
		{
			get;
			set;
		}

		public int size
		{
			get;
			set;
		}

		public float verticSizeInMm
		{
			get;
			set;
		}

		public float horizSizeInMm
		{
			get;
			set;
		}

		public int countOfBeadsIn1000Gramm
		{
			get;
			set;
		}

		public float lengthOfYarnPer1000sm2
		{
			get;
			set;
		}
	}
}
