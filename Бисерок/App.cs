using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Threading;
using Бисерок.Properties;

namespace Бисерок
{
	public partial class App : System.Windows.Application
	{
		public static App _thisApp;

		protected override void OnStartup(StartupEventArgs e)
		{
			Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
			Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = ",";
			System.Windows.Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
			AppDomain currentDomain = AppDomain.CurrentDomain;
			currentDomain.UnhandledException += MyGlobalErrorHandler;
			bool flag = RegistryWork.IsIDComputerMemorized_AndGenerateAndWriteItIfNo();
			DefineSpecialVersions();
			if (!flag)
			{
				SetDefaultProgramSettings();
			}
			_thisApp = this;
			SetLogoAndButtonsStyle();
			string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
			string text = Path.Combine(folderPath, Settings.FileDirectory);
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			string sourceFileName = Path.Combine(System.Windows.Forms.Application.StartupPath, "MyPalette.txt");
			string text2 = Path.Combine(text, "MyPalette.txt");
			if (!File.Exists(text2))
			{
				File.Copy(sourceFileName, text2);
			}
			sourceFileName = Path.Combine(System.Windows.Forms.Application.StartupPath, "CatalogState.txt");
			text2 = Path.Combine(text, "CatalogState.txt");
			if (!File.Exists(text2))
			{
				File.Copy(sourceFileName, text2);
			}
		}

		public static void SetDefaultProgramSettings()
		{
			SchemeParameters schemeParameters = new SchemeParameters();
			if (Settings.hobbyProgramm == HobbyProgramm.Almaz)
			{
				SchemeParameters schemeParameters2 = schemeParameters;
				SchemeParameters schemeParameters3 = schemeParameters;
				float num3 = schemeParameters2.VerticalSizeOfBlockAtSchemeInMm = (schemeParameters3.HorizontalSizeOfBlockAtSchemeInMm = 2.5f);
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Biserok)
			{
				schemeParameters.beadsOrCanvasSize = 10;
				SchemeParameters schemeParameters4 = schemeParameters;
				SchemeParameters schemeParameters5 = schemeParameters;
				float num3 = schemeParameters4.VerticalSizeOfBlockAtSchemeInMm = (schemeParameters5.HorizontalSizeOfBlockAtSchemeInMm = 2f);
				schemeParameters.countOfBeadsIn1000Gramm = 91000;
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Krestik)
			{
				schemeParameters.beadsOrCanvasSize = 14;
				SchemeParameters schemeParameters6 = schemeParameters;
				SchemeParameters schemeParameters7 = schemeParameters;
				float num3 = schemeParameters6.VerticalSizeOfBlockAtSchemeInMm = (schemeParameters7.HorizontalSizeOfBlockAtSchemeInMm = 1.81428576f);
				schemeParameters.NumberOfAdditionThread = 2;
			}
			else if (Settings.hobbyProgramm == HobbyProgramm.Petelka)
			{
				schemeParameters.beadsOrCanvasSize = 20;
				schemeParameters.VerticalSizeOfBlockAtSchemeInMm = 3.33333325f;
				schemeParameters.HorizontalSizeOfBlockAtSchemeInMm = 5f;
				schemeParameters.LengthOfYarnPer1000sm2 = 122.8346f;
			}
			Бисерок.Properties.Settings.Default.Save();
		}

		private void DefineSpecialVersions()
		{
			/*if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs() == "Maxim" && Settings.hobbyProgramm == HobbyProgramm.Biserok)
			{
				Settings.isItSpecialVersionOfBiserokForMaxim = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs() == "Denis" && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForDenis = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("EvgeniyHoludintcev") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForEvgeniyHoludintcev = true;
				Settings.isItVersionWithAbilityOfTurningOffSourceImageChanging = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("Sergey_gotoshopping") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForSergey = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("Evgeniy_Fedorov") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForEvgeniyFedorov = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("ViktoriyaErmolenko") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForViktoriyaErmolenko = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("NikolayUkraina") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForNikolayUkraina = true;
				Settings.isItVersionWithAbilityOfTurningOffSourceImageChanging = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("_ForManufacturers"))
			{
				Settings.isItSpecialVersion_ForManufacturers = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("withoutContact"))
			{
				Settings.isItVersionWithoutContactInfo = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("raskrasim_ru") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				RegistryWork.DeleteAll();
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("AlexandrTimchenko") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForAlexandrTimchenko = true;
				Settings.isItVersionWithAbilityOfTurningOffSourceImageChanging = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("Evgeniy_K_ae") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForEvgeniy_K = true;
				Settings.isItVersionWithAbilityOfTurningOffSourceImageChanging = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("TatyanaKudryavtceva") && Settings.hobbyProgramm == HobbyProgramm.Biserok)
			{
				Settings.isItSpecialVersionOfBiserokForTatyanaKudryavtceva = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("JuliaKonyahina") && Settings.hobbyProgramm == HobbyProgramm.Almaz)
			{
				Settings.isItSpecialVersionOfAlmazkaForJuliaKonyahina = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("JuliaTuktamysheva") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForJuliaTuktamysheva = true;
				Settings.isItVersionWithAbilityOfTurningOffSourceImageChanging = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("AlexandrGalanzovsky") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForAlexandrGalanzovsky = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("AlexandrZorin") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForAlexandrZorin = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("Vitaliy_visim2008") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForVitaliy_visim2008 = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("VersionWithAbilityOfTurningOffSourceImageChanging") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItVersionWithAbilityOfTurningOffSourceImageChanging = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("ElenaBartosova") && Settings.hobbyProgramm == HobbyProgramm.Almaz)
			{
				Settings.isItSpecialVersionOfAlmazkaForElenaBartosova = true;
			}
			if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("AntoniShmerling") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				Settings.isItSpecialVersionOfRaskraskaForAntoniShmerling = true;
			}*/

            //Settings.isItSpecialVersionOfRaskraskaForAntoniShmerling = true;
        }

		private void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			bool flag = Site.SendExceptionInfo(e.Exception, true);
		}

		private static void MyGlobalErrorHandler(object sender, UnhandledExceptionEventArgs args)
		{
			bool flag = Site.SendExceptionInfo((Exception)args.ExceptionObject, true);
		}

		public static void SetLogoAndButtonsStyle()
		{
            _thisApp.Resources["btnBackground"] = new SolidColorBrush(Color.FromArgb(byte.MaxValue, byte.MaxValue, 188, 117));
            _thisApp.Resources["btnBorderBrush"] = new SolidColorBrush(Color.FromArgb(byte.MaxValue, byte.MaxValue, 154, 71));
            _thisApp.Resources["btnBorderBackgroundIsMouseOver"] = new SolidColorBrush(Color.FromArgb(byte.MaxValue, byte.MaxValue, 210, 163));

			ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
			string text = string.Format("{0}\\{1}", AppDomain.CurrentDomain.BaseDirectory, "Images/logo.ico");
			ImageSource value = (ImageSource)imageSourceConverter.ConvertFromString(text);
			_thisApp.Resources["logo"] = value;
		}
	}
}
