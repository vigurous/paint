using System.Collections.Generic;
using System.Drawing;

namespace Бисерок
{
	internal class ComparerBeadsBySeriesAndManufacturer : IComparer<Beads>
	{
		public int Compare(Beads x, Beads y)
		{
			if (x.parentSeries != null)
			{
				if (x.parentSeries.parentManufacturer.name != y.parentSeries.parentManufacturer.name)
				{
					return x.parentSeries.parentManufacturer.name.CompareTo(y.parentSeries.parentManufacturer.name);
				}
				if (x.parentSeries.name != y.parentSeries.name)
				{
					return x.parentSeries.name.CompareTo(y.parentSeries.name);
				}
				return x.name.CompareTo(y.name);
			}
			if (x.clr == y.clr)
			{
				return 0;
			}
			Color clr = x.clr;
			byte r = clr.R;
			clr = y.clr;
			if (r == clr.R)
			{
				clr = x.clr;
				byte g = clr.G;
				clr = y.clr;
				if (g == clr.G)
				{
					clr = x.clr;
					byte b = clr.B;
					clr = y.clr;
					if (b == clr.B)
					{
						return 0;
					}
					clr = x.clr;
					byte b2 = clr.B;
					clr = y.clr;
					if (b2 > clr.B)
					{
						return 1;
					}
					return -1;
				}
				clr = x.clr;
				byte g2 = clr.G;
				clr = y.clr;
				if (g2 > clr.G)
				{
					return 1;
				}
				return -1;
			}
			clr = x.clr;
			byte r2 = clr.R;
			clr = y.clr;
			if (r2 > clr.R)
			{
				return 1;
			}
			return -1;
		}
	}
}
