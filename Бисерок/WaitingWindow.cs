using System;
using System.Windows.Threading;

namespace Бисерок
{
	public class WaitingWindow
	{
		public static WndWait _wnd;

		private static bool isShow;

		public static WndMain wndOwner;

		private static DispatcherTimer tmr;

		static WaitingWindow()
		{
			_wnd = new WndWait();
			isShow = false;
			tmr = new DispatcherTimer();
			tmr.Tick += Tmr_Tick;
			tmr.Interval = new TimeSpan(0, 0, 0, 0, 300);
			tmr.Start();
		}

		private static void Tmr_Tick(object sender, EventArgs e)
		{
			if (wndOwner != null && !wndOwner.IsInProcess)
			{
				HideWindow();
			}
		}

		internal static void ShowWindow()
		{
			isShow = true;
			_wnd.Show();
		}

		internal static void HideWindow()
		{
			isShow = false;
			_wnd.Hide();
		}

		internal static void ChangeTextInWindow(string p)
		{
			_wnd.ChangeText(p);
		}

		internal static void CloseByProgram()
		{
			isShow = false;
			_wnd.CloseByProgram();
		}

		public static void MinimizeWindow()
		{
			if (isShow)
			{
				_wnd.Hide();
			}
		}

		public static void MaximizeWindow()
		{
			if (isShow)
			{
				_wnd.Show();
			}
		}
	}
}
