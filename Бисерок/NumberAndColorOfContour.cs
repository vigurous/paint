using System;

namespace Бисерок
{
	[Serializable]
	public class NumberAndColorOfContour
	{
		public byte R;

		public byte G;

		public byte B;

		public int number = -1;
	}
}
