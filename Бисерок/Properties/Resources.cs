﻿// Decompiled with JetBrains decompiler
// Type: Бисерок.Properties.Resources
// Assembly: Раскраска, Version=2.1.1.4, Culture=neutral, PublicKeyToken=54a239f4f0353298
// MVID: 38285A8D-209D-4E23-BF7B-8FF762BD4139
// Assembly location: D:\Temp\Okmjanski\App\Раскраска.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace Бисерок.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  public class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static ResourceManager ResourceManager
    {
      get
      {
        if (Бисерок.Properties.Resources.resourceMan == null)
          Бисерок.Properties.Resources.resourceMan = new ResourceManager("Бисерок.Properties.Resources", typeof (Бисерок.Properties.Resources).Assembly);
        return Бисерок.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static CultureInfo Culture
    {
      get
      {
        return Бисерок.Properties.Resources.resourceCulture;
      }
      set
      {
        Бисерок.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
