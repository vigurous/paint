﻿// Decompiled with JetBrains decompiler
// Type: Бисерок.Properties.Resources_KR
// Assembly: Раскраска, Version=2.1.1.4, Culture=neutral, PublicKeyToken=54a239f4f0353298
// MVID: 38285A8D-209D-4E23-BF7B-8FF762BD4139
// Assembly location: D:\Temp\Okmjanski\App\Раскраска.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace Бисерок.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  public class Resources_KR
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources_KR()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static ResourceManager ResourceManager
    {
      get
      {
        if (Resources_KR.resourceMan == null)
          Resources_KR.resourceMan = new ResourceManager("Бисерок.Properties.Resources.KR", typeof (Resources_KR).Assembly);
        return Resources_KR.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static CultureInfo Culture
    {
      get
      {
        return Resources_KR.resourceCulture;
      }
      set
      {
        Resources_KR.resourceCulture = value;
      }
    }

    public static string MainTitle
    {
      get
      {
        return Resources_KR.ResourceManager.GetString(nameof (MainTitle), Resources_KR.resourceCulture);
      }
    }
  }
}
