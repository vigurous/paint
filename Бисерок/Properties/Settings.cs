﻿// Decompiled with JetBrains decompiler
// Type: Бисерок.Properties.Settings
// Assembly: Раскраска, Version=2.1.1.4, Culture=neutral, PublicKeyToken=54a239f4f0353298
// MVID: 38285A8D-209D-4E23-BF7B-8FF762BD4139
// Assembly location: D:\Temp\Okmjanski\App\Раскраска.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace Бисерок.Properties
{
  [CompilerGenerated]
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.3.0.0")]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    private void SettingChangingEventHandler(object sender, SettingChangingEventArgs e)
    {
    }

    private void SettingsSavingEventHandler(object sender, CancelEventArgs e)
    {
    }

    public static Settings Default
    {
      get
      {
        return Settings.defaultInstance;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("10")]
    public int KanvasOrBeadsSize
    {
      get
      {
        return (int) this[nameof (KanvasOrBeadsSize)];
      }
      set
      {
        this[nameof (KanvasOrBeadsSize)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("3")]
    public int ThiknessLineBetweenGroupOf10Block
    {
      get
      {
        return (int) this[nameof (ThiknessLineBetweenGroupOf10Block)];
      }
      set
      {
        this[nameof (ThiknessLineBetweenGroupOf10Block)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("Black")]
    public Color ColorOfLinesOnScheme
    {
      get
      {
        return (Color) this[nameof (ColorOfLinesOnScheme)];
      }
      set
      {
        this[nameof (ColorOfLinesOnScheme)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool IsShowNumberOfListOnCommonScheme
    {
      get
      {
        return (bool) this[nameof (IsShowNumberOfListOnCommonScheme)];
      }
      set
      {
        this[nameof (IsShowNumberOfListOnCommonScheme)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool IsWithDithering
    {
      get
      {
        return (bool) this[nameof (IsWithDithering)];
      }
      set
      {
        this[nameof (IsWithDithering)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool IsColorScheme_OrForGrayPrint
    {
      get
      {
        return (bool) this[nameof (IsColorScheme_OrForGrayPrint)];
      }
      set
      {
        this[nameof (IsColorScheme_OrForGrayPrint)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool IsEmbroideryViewInRealView
    {
      get
      {
        return (bool) this[nameof (IsEmbroideryViewInRealView)];
      }
      set
      {
        this[nameof (IsEmbroideryViewInRealView)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("2.3")]
    public float HorizontalSizeOfBlockAtSchemeInMm
    {
      get
      {
        return (float) this[nameof (HorizontalSizeOfBlockAtSchemeInMm)];
      }
      set
      {
        this[nameof (HorizontalSizeOfBlockAtSchemeInMm)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("2.3")]
    public float VerticalSizeOfBlockAtSchemeInMm
    {
      get
      {
        return (float) this[nameof (VerticalSizeOfBlockAtSchemeInMm)];
      }
      set
      {
        this[nameof (VerticalSizeOfBlockAtSchemeInMm)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("91000")]
    public int CountOfBeadsIn1000Gramm
    {
      get
      {
        return (int) this[nameof (CountOfBeadsIn1000Gramm)];
      }
      set
      {
        this[nameof (CountOfBeadsIn1000Gramm)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("2")]
    public int NumberOfAdditionThread
    {
      get
      {
        return (int) this[nameof (NumberOfAdditionThread)];
      }
      set
      {
        this[nameof (NumberOfAdditionThread)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("63.5827")]
    public float LengthOfYarnPer1000sm2
    {
      get
      {
        return (float) this[nameof (LengthOfYarnPer1000sm2)];
      }
      set
      {
        this[nameof (LengthOfYarnPer1000sm2)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("12 петель на 10см, 16 рядов на 10см (70-90м  в 100гр. пряжи)")]
    public string KnittingDensityName
    {
      get
      {
        return (string) this[nameof (KnittingDensityName)];
      }
      set
      {
        this[nameof (KnittingDensityName)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("20")]
    public int SqureCellSidePrintPages_Y
    {
      get
      {
        return (int) this[nameof (SqureCellSidePrintPages_Y)];
      }
      set
      {
        this[nameof (SqureCellSidePrintPages_Y)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("12")]
    public int SqureCellSidePreview_Y
    {
      get
      {
        return (int) this[nameof (SqureCellSidePreview_Y)];
      }
      set
      {
        this[nameof (SqureCellSidePreview_Y)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("300")]
    public int PrintingResolution
    {
      get
      {
        return (int) this[nameof (PrintingResolution)];
      }
      set
      {
        this[nameof (PrintingResolution)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("20")]
    public float WidthOfBorderOfBeadsInPercentToBeadsRadius
    {
      get
      {
        return (float) this[nameof (WidthOfBorderOfBeadsInPercentToBeadsRadius)];
      }
      set
      {
        this[nameof (WidthOfBorderOfBeadsInPercentToBeadsRadius)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("30")]
    public float WidthOfPointBetweenBeadsInPercentToBeadsRadius
    {
      get
      {
        return (float) this[nameof (WidthOfPointBetweenBeadsInPercentToBeadsRadius)];
      }
      set
      {
        this[nameof (WidthOfPointBetweenBeadsInPercentToBeadsRadius)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool IsSeparatingLinesOnScheme
    {
      get
      {
        return (bool) this[nameof (IsSeparatingLinesOnScheme)];
      }
      set
      {
        this[nameof (IsSeparatingLinesOnScheme)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool IsDrawingBadgesOnSchemeForPrinting
    {
      get
      {
        return (bool) this[nameof (IsDrawingBadgesOnSchemeForPrinting)];
      }
      set
      {
        this[nameof (IsDrawingBadgesOnSchemeForPrinting)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool IsDrawSmallLegend
    {
      get
      {
        return (bool) this[nameof (IsDrawSmallLegend)];
      }
      set
      {
        this[nameof (IsDrawSmallLegend)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool IsMixingColors
    {
      get
      {
        return (bool) this[nameof (IsMixingColors)];
      }
      set
      {
        this[nameof (IsMixingColors)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("30")]
    public byte TransparentColorsOnCanvas
    {
      get
      {
        return (byte) this[nameof (TransparentColorsOnCanvas)];
      }
      set
      {
        this[nameof (TransparentColorsOnCanvas)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("2")]
    public byte Mediana
    {
      get
      {
        return (byte) this[nameof (Mediana)];
      }
      set
      {
        this[nameof (Mediana)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("2")]
    public byte NumberOfMedianaRepeats
    {
      get
      {
        return (byte) this[nameof (NumberOfMedianaRepeats)];
      }
      set
      {
        this[nameof (NumberOfMedianaRepeats)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("140")]
    public byte BrightnessOfContour
    {
      get
      {
        return (byte) this[nameof (BrightnessOfContour)];
      }
      set
      {
        this[nameof (BrightnessOfContour)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("1")]
    public byte SmallPartsOnColouring
    {
      get
      {
        return (byte) this[nameof (SmallPartsOnColouring)];
      }
      set
      {
        this[nameof (SmallPartsOnColouring)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool LightestColorIsCanvas
    {
      get
      {
        return (bool) this[nameof (LightestColorIsCanvas)];
      }
      set
      {
        this[nameof (LightestColorIsCanvas)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool DarkestColorIsCanvas
    {
      get
      {
        return (bool) this[nameof (DarkestColorIsCanvas)];
      }
      set
      {
        this[nameof (DarkestColorIsCanvas)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("1")]
    public int wayOfBadgesCreating
    {
      get
      {
        return (int) this[nameof (wayOfBadgesCreating)];
      }
      set
      {
        this[nameof (wayOfBadgesCreating)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool IsChangeSourceImage
    {
      get
      {
        return (bool) this[nameof (IsChangeSourceImage)];
      }
      set
      {
        this[nameof (IsChangeSourceImage)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool IsSquareStraz
    {
      get
      {
        return (bool) this[nameof (IsSquareStraz)];
      }
      set
      {
        this[nameof (IsSquareStraz)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("40")]
    public int countOfPixelsInCmAtContoursMapOfColoring
    {
      get
      {
        return (int) this[nameof (countOfPixelsInCmAtContoursMapOfColoring)];
      }
      set
      {
        this[nameof (countOfPixelsInCmAtContoursMapOfColoring)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("0")]
    public int thicknessOfAdditionalBorderOfColoringContour
    {
      get
      {
        return (int) this[nameof (thicknessOfAdditionalBorderOfColoringContour)];
      }
      set
      {
        this[nameof (thicknessOfAdditionalBorderOfColoringContour)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("70")]
    public byte BrightnessOfLabel
    {
      get
      {
        return (byte) this[nameof (BrightnessOfLabel)];
      }
      set
      {
        this[nameof (BrightnessOfLabel)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("60")]
    public int widthPageInBeads
    {
      get
      {
        return (int) this[nameof (widthPageInBeads)];
      }
      set
      {
        this[nameof (widthPageInBeads)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("90")]
    public int heightPageInBeads
    {
      get
      {
        return (int) this[nameof (heightPageInBeads)];
      }
      set
      {
        this[nameof (heightPageInBeads)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("1")]
    public float minFontSizeInPt
    {
      get
      {
        return (float) this[nameof (minFontSizeInPt)];
      }
      set
      {
        this[nameof (minFontSizeInPt)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool isNotChangeSourceImageInColoring
    {
      get
      {
        return (bool) this[nameof (isNotChangeSourceImageInColoring)];
      }
      set
      {
        this[nameof (isNotChangeSourceImageInColoring)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool isShowPreprocessingWindow
    {
      get
      {
        return (bool) this[nameof (isShowPreprocessingWindow)];
      }
      set
      {
        this[nameof (isShowPreprocessingWindow)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("")]
    public string selectedLastPathToSave
    {
      get
      {
        return (string) this[nameof (selectedLastPathToSave)];
      }
      set
      {
        this[nameof (selectedLastPathToSave)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool isConstantNumerationOfColorsInRaskraska
    {
      get
      {
        return (bool) this[nameof (isConstantNumerationOfColorsInRaskraska)];
      }
      set
      {
        this[nameof (isConstantNumerationOfColorsInRaskraska)] = (object) value;
      }
    }
  }
}
