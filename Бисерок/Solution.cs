using System.Drawing;

namespace Бисерок
{
	public class Solution
	{
		public bool isItialized;

		public int countColors;

		public float weight1;

		public Color clr1;

		public float weight2;

		public Color clr2;

		public float weight3;

		public Color clr3;

		public Color resultClr;

		public float deltaR;

		public float deltaY;

		public float deltaB;

		public float delta_in_RYB = 3.40282347E+38f;
	}
}
