using Microsoft.Win32;
using System;
using System.Reflection;

namespace Бисерок
{
	public static class RegistryWork
	{
		private static string salt = "SjoaJJ";

		private static string pass = "pDhwns";

		private static string vect = "39dk*jq*2mp0d82H";

		public static int CheckVersionAndCreateItIfNoExist()
		{
		    return 3;
		}
		public static DateTime? CheckDateTimeEnd()
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
				RegistryKey registryKey2 = registryKey.CreateSubKey(Settings.RegistryDir);
				string decryptString = GetDecryptString(registryKey2.GetValue(GetEncryptString("dtExpired")));
				if (decryptString != null && decryptString.Length > 0)
				{
					return Convert.ToDateTime(decryptString);
				}
				return null;
			}
			catch (Exception)
			{
				return null;
			}
		}

		public static string CheckEmail()
		{
		    return "test@test.com";
		}

		public static bool IsUnexpired()
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE");
				RegistryKey registryKey2 = registryKey.OpenSubKey(Settings.RegistryDir);
				string decryptString = GetDecryptString(registryKey2.GetValue(GetEncryptString("isUnexpired")));
				if (decryptString != null && decryptString.Length > 0)
				{
					return Convert.ToBoolean(decryptString);
				}
				return false;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static bool CheckEmailSendedOnServer()
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE");
				RegistryKey registryKey2 = registryKey.OpenSubKey(Settings.RegistryDir);
				string decryptString = GetDecryptString(registryKey2.GetValue(GetEncryptString("email_sended")));
				if (decryptString != null && decryptString.Length > 0)
				{
					return Convert.ToBoolean(decryptString);
				}
				return false;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static bool WriteEmail(string email)
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
				RegistryKey registryKey2 = registryKey.CreateSubKey(Settings.RegistryDir);
				registryKey2.SetValue(GetEncryptString("email"), GetEncryptString(email));
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static bool WriteIsEmailSendedOnServer(bool? IsEmailSendedOnServer)
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
				RegistryKey registryKey2 = registryKey.CreateSubKey(Settings.RegistryDir);
				if (IsEmailSendedOnServer.HasValue)
				{
					registryKey2.SetValue(GetEncryptString("email_sended"), GetEncryptString(IsEmailSendedOnServer.ToString()));
				}
				else
				{
					registryKey2.DeleteValue(GetEncryptString("email_sended"));
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static bool WriteVersion(int? v)
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
				RegistryKey registryKey2 = registryKey.CreateSubKey(Settings.RegistryDir);
				if (v.HasValue)
				{
					registryKey2.SetValue(GetEncryptString("version"), GetEncryptString(v.ToString()));
				}
				else
				{
					registryKey2.DeleteValue(GetEncryptString("version"));
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static bool WriteDateTimeExpired(DateTime? dt)
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
				RegistryKey registryKey2 = registryKey.CreateSubKey(Settings.RegistryDir);
				if (dt.HasValue)
				{
					registryKey2.SetValue(GetEncryptString("dtExpired"), GetEncryptString(dt.ToString()));
				}
				else
				{
					registryKey2.DeleteValue(GetEncryptString("dtExpired"));
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static bool WriteIsUnexpired(bool? isExpired)
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
				RegistryKey registryKey2 = registryKey.CreateSubKey(Settings.RegistryDir);
				if (isExpired.HasValue)
				{
					registryKey2.SetValue(GetEncryptString("isUnexpired"), GetEncryptString(isExpired.ToString()));
				}
				else
				{
					registryKey2.DeleteValue(GetEncryptString("isUnexpired"));
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static bool RewriteToUnexpiredVersion()
		{
			if (WriteVersion(0) && WriteDateTimeExpired(null))
			{
				return WriteIsUnexpired(true);
			}
			return false;
		}

		public static bool RewriteToReturn()
		{
			if (WriteVersion(0))
			{
				return WriteDateTimeExpired(null);
			}
			return false;
		}

		public static void DeleteAll()
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
				registryKey.DeleteSubKeyTree(Settings.RegistryDir);
			}
			catch (Exception)
			{
			}
		}

		public static bool IsIDComputerMemorized_AndGenerateAndWriteItIfNo()
		{
			RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
			RegistryKey registryKey2 = registryKey.CreateSubKey(Settings.RegistryDir);
			string decryptString = GetDecryptString(registryKey2.GetValue(GetEncryptString("idComputer")));
			bool result = true;
			if (decryptString == null || decryptString.Length == 0)
			{
				result = false;
				registryKey2.SetValue(GetEncryptString("idComputer"), GetEncryptString(Guid.NewGuid().ToString()));
			}
			return result;
		}

		public static string GetIDComputer()
		{
			RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
			RegistryKey registryKey2 = registryKey.CreateSubKey(Settings.RegistryDir);
			return GetDecryptString(registryKey2.GetValue(GetEncryptString("idComputer"))).ToString();
		}

		public static bool CheckShowLicense()
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE");
				RegistryKey registryKey2 = registryKey.OpenSubKey(Settings.RegistryDir);
				string decryptString = GetDecryptString(registryKey2.GetValue(GetEncryptString("isShownLicenseAndCommercialUsingInfo")));
				if (decryptString != null && decryptString.Length > 0)
				{
					return Convert.ToBoolean(decryptString);
				}
				return false;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static bool WriteShowLicense(bool ok)
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
				RegistryKey registryKey2 = registryKey.CreateSubKey(Settings.RegistryDir);
				registryKey2.SetValue(GetEncryptString("isShownLicenseAndCommercialUsingInfo"), GetEncryptString(ok.ToString()));
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		private static string GetEncryptString(string src)
		{
			return Cryption.Encrypt(src, pass, salt, vect, "SHA1", 2, 256);
		}

		private static string GetDecryptString(object encryptObj)
		{
			return Cryption.Decrypt(Convert.ToString(encryptObj), pass, salt, vect, "SHA1", 2, 256);
		}

		internal static bool CheckIsItOldUser()
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE");
				RegistryKey registryKey2 = registryKey.OpenSubKey(Settings.RegistryDir);
				string decryptString = GetDecryptString(registryKey2.GetValue(GetEncryptString("isShownLicense")));
				if (decryptString != null && decryptString.Length != 0)
				{
					return true;
				}
				return false;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static string CheckIsItSpecialVersionAndGetKeyIfItIs()
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE");
				RegistryKey registryKey2 = registryKey.OpenSubKey(Settings.RegistryDir);
				return Convert.ToString(GetDecryptString(registryKey2.GetValue(GetEncryptString("specialVersion"))));
			}
			catch (Exception)
			{
				return "";
			}
		}

		public static string GetCurrentProgramAssemblyVersion()
		{
			return Assembly.GetExecutingAssembly().GetName().Version.ToString();
		}

		public static string GetAssemblyVersionFromRegistry()
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE");
				RegistryKey registryKey2 = registryKey.OpenSubKey(Settings.RegistryDir);
				string decryptString = GetDecryptString(registryKey2.GetValue(GetEncryptString("ProgramVersion")));
				if (decryptString != null && decryptString.Length != 0)
				{
					return decryptString;
				}
				return "";
			}
			catch (Exception)
			{
				return "";
			}
		}

		public static bool WriteAssemblyVersionToRegistry()
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
				RegistryKey registryKey2 = registryKey.CreateSubKey(Settings.RegistryDir);
				registryKey2.SetValue(GetEncryptString("ProgramVersion"), GetEncryptString(GetCurrentProgramAssemblyVersion()));
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static DateTime? GetLastDateReminderAboutApportunityOfDiscountOnRenewal()
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE");
				RegistryKey registryKey2 = registryKey.OpenSubKey(Settings.RegistryDir);
				string decryptString = GetDecryptString(registryKey2.GetValue(GetEncryptString("LastDateReminderAboutApportunityOfDiscountOnRenewal")));
				if (decryptString != null && decryptString.Length != 0)
				{
					return Convert.ToDateTime(decryptString);
				}
				return null;
			}
			catch (Exception)
			{
				return null;
			}
		}

		public static void WriteDateReminderAboutApportunityOfDiscountOnRenewal()
		{
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
				RegistryKey registryKey2 = registryKey.CreateSubKey(Settings.RegistryDir);
				registryKey2.SetValue(GetEncryptString("LastDateReminderAboutApportunityOfDiscountOnRenewal"), GetEncryptString(DateTime.Now.ToString()));
			}
			catch (Exception)
			{
			}
		}
	}
}
