﻿// Decompiled with JetBrains decompiler
// Type: Бисерок.ServiceReference.IServices
// Assembly: Раскраска, Version=2.1.1.4, Culture=neutral, PublicKeyToken=54a239f4f0353298
// MVID: 38285A8D-209D-4E23-BF7B-8FF762BD4139
// Assembly location: D:\Temp\Okmjanski\App\Раскраска.exe

using System;
using System.CodeDom.Compiler;
using System.ServiceModel;

namespace Бисерок.ServiceReference
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "ServiceReference.IServices")]
  public interface IServices
  {
    [OperationContract(Action = "http://tempuri.org/IServices/ProgramException", ReplyAction = "http://tempuri.org/IServices/ProgramExceptionResponse")]
    bool ProgramException(Guid idComputer, bool isUnhandled, string message, string stackTrace, string innerMessage, string innerStackTrace);

    [OperationContract(Action = "http://tempuri.org/IServices/AddUserQuestion", ReplyAction = "http://tempuri.org/IServices/AddUserQuestionResponse")]
    bool AddUserQuestion(string subject, string text, string emailFrom);

    [OperationContract(Action = "http://tempuri.org/IServices/CheckOrder", ReplyAction = "http://tempuri.org/IServices/CheckOrderResponse")]
    string CheckOrder(string email, Guid idComputer);

    [OperationContract(Action = "http://tempuri.org/IServices/CheckOrderOnReturn", ReplyAction = "http://tempuri.org/IServices/CheckOrderOnReturnResponse")]
    string CheckOrderOnReturn(Guid idComputer);

    [OperationContract(Action = "http://tempuri.org/IServices/GetDate", ReplyAction = "http://tempuri.org/IServices/GetDateResponse")]
    string GetDate();

    [OperationContract(Action = "http://tempuri.org/IServices/GetNews", ReplyAction = "http://tempuri.org/IServices/GetNewsResponse")]
    string GetNews(Guid idComputer);

    [OperationContract(Action = "http://tempuri.org/IServices/ActionWithProgram", ReplyAction = "http://tempuri.org/IServices/ActionWithProgramResponse")]
    bool ActionWithProgram(Guid idComputer, int orderVersion, string assemblyVersion, string action);

    [OperationContract(Action = "http://tempuri.org/IServices/CheckEnableSchemesInSpecialVersion", ReplyAction = "http://tempuri.org/IServices/CheckEnableSchemesInSpecialVersionResponse")]
    string CheckEnableSchemesInSpecialVersion(Guid idComputer, string bmpHashcode, int widthInPx, int heightInPx, string fileName, int colorCount, int widthInBlocks, int heightInBlocks);

    [OperationContract(Action = "http://tempuri.org/IServices/AddConsumablesOrder", ReplyAction = "http://tempuri.org/IServices/AddConsumablesOrderResponse")]
    bool AddConsumablesOrder(string email, string content);
  }
}
