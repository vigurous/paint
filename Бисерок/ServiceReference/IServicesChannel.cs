﻿// Decompiled with JetBrains decompiler
// Type: Бисерок.ServiceReference.IServicesChannel
// Assembly: Раскраска, Version=2.1.1.4, Culture=neutral, PublicKeyToken=54a239f4f0353298
// MVID: 38285A8D-209D-4E23-BF7B-8FF762BD4139
// Assembly location: D:\Temp\Okmjanski\App\Раскраска.exe

using System;
using System.CodeDom.Compiler;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Бисерок.ServiceReference
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public interface IServicesChannel : IServices, IClientChannel, IContextChannel, IChannel, ICommunicationObject, IExtensibleObject<IContextChannel>, IDisposable
  {
  }
}
