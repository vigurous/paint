﻿// Decompiled with JetBrains decompiler
// Type: Бисерок.ServiceReference.ServicesClient
// Assembly: Раскраска, Version=2.1.1.4, Culture=neutral, PublicKeyToken=54a239f4f0353298
// MVID: 38285A8D-209D-4E23-BF7B-8FF762BD4139
// Assembly location: D:\Temp\Okmjanski\App\Раскраска.exe

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Бисерок.ServiceReference
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class ServicesClient : ClientBase<IServices>, IServices
  {
    public ServicesClient()
    {
    }

    public ServicesClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public ServicesClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public ServicesClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public ServicesClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public bool ProgramException(Guid idComputer, bool isUnhandled, string message, string stackTrace, string innerMessage, string innerStackTrace)
    {
      return this.Channel.ProgramException(idComputer, isUnhandled, message, stackTrace, innerMessage, innerStackTrace);
    }

    public bool AddUserQuestion(string subject, string text, string emailFrom)
    {
      return this.Channel.AddUserQuestion(subject, text, emailFrom);
    }

    public string CheckOrder(string email, Guid idComputer)
    {
      return this.Channel.CheckOrder(email, idComputer);
    }

    public string CheckOrderOnReturn(Guid idComputer)
    {
      return this.Channel.CheckOrderOnReturn(idComputer);
    }

    public string GetDate()
    {
      return this.Channel.GetDate();
    }

    public string GetNews(Guid idComputer)
    {
      return this.Channel.GetNews(idComputer);
    }

    public bool ActionWithProgram(Guid idComputer, int orderVersion, string assemblyVersion, string action)
    {
      return this.Channel.ActionWithProgram(idComputer, orderVersion, assemblyVersion, action);
    }

    public string CheckEnableSchemesInSpecialVersion(Guid idComputer, string bmpHashcode, int widthInPx, int heightInPx, string fileName, int colorCount, int widthInBlocks, int heightInBlocks)
    {
      return this.Channel.CheckEnableSchemesInSpecialVersion(idComputer, bmpHashcode, widthInPx, heightInPx, fileName, colorCount, widthInBlocks, heightInBlocks);
    }

    public bool AddConsumablesOrder(string email, string content)
    {
      return this.Channel.AddConsumablesOrder(email, content);
    }
  }
}
