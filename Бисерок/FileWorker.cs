using System;
using System.IO;
using System.Text;

namespace Бисерок
{
	public static class FileWorker
	{
		public static string ReadFileToEnd(string path, bool isItAbsoletePath, Encoding encoding = null)
		{
			string path2 = (!isItAbsoletePath) ? (AppDomain.CurrentDomain.BaseDirectory + "/" + path) : path;
			StreamReader streamReader = (encoding != null) ? new StreamReader(path2, encoding) : new StreamReader(path2);
			string result = streamReader.ReadToEnd();
			streamReader.Close();
			return result;
		}
	}
}
