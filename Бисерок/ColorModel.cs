using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace Бисерок
{
	internal static class ColorModel
	{
		private static RYB MixColors(RYB ryb1, float weight1, RYB ryb2, float weight2)
		{
			RYB rYB = new RYB();
			rYB.R = (ryb1.R * weight1 + ryb2.R * weight2) / (weight1 + weight2);
			rYB.Y = (ryb1.Y * weight1 + ryb2.Y * weight2) / (weight1 + weight2);
			rYB.B = (ryb1.B * weight1 + ryb2.B * weight2) / (weight1 + weight2);
			float num = 2.35f;
			float num2 = (rYB.R + rYB.Y - 3f * rYB.B) / num + 1f;
			if (num2 < 1f)
			{
				num2 = 1f;
			}
			rYB.R *= num2;
			if (rYB.R > 1f)
			{
				rYB.R = 1f;
			}
			rYB.Y *= num2;
			if (rYB.Y > 1f)
			{
				rYB.Y = 1f;
			}
			return rYB;
		}

		private static RYB MixColors(RYB ryb1, float weight1, RYB ryb2, float weight2, float _kf)
		{
			RYB rYB = new RYB();
			rYB.R = (ryb1.R * weight1 + ryb2.R * weight2) / (weight1 + weight2);
			rYB.Y = (ryb1.Y * weight1 + ryb2.Y * weight2) / (weight1 + weight2);
			rYB.B = (ryb1.B * weight1 + ryb2.B * weight2) / (weight1 + weight2);
			float num = (rYB.R + rYB.Y - 3f * rYB.B) / _kf + 1f;
			if (num < 1f)
			{
				num = 1f;
			}
			rYB.R *= num;
			if (rYB.R > 1f)
			{
				rYB.R = 1f;
			}
			rYB.Y *= num;
			if (rYB.Y > 1f)
			{
				rYB.Y = 1f;
			}
			return rYB;
		}

		private static Color MixColors(Color clr1, float weight1, Color clr2, float weight2)
		{
			RYB ryb = rgb2ryb(clr1);
			RYB ryb2 = rgb2ryb(clr2);
			RYB ryb3 = MixColors(ryb, weight1, ryb2, weight2);
			return ryb2rgb(ryb3);
		}

		private static Color MixColors(Color clr1, float weight1, Color clr2, float weight2, float _kf)
		{
			RYB ryb = rgb2ryb(clr1);
			RYB ryb2 = rgb2ryb(clr2);
			RYB ryb3 = MixColors(ryb, weight1, ryb2, weight2, _kf);
			return ryb2rgb(ryb3);
		}

		private static Color ryb2rgb_1(RYB ryb)
		{
			float r = ryb.R;
			float y = ryb.Y;
			float b = ryb.B;
			float num = Math.Min(r, Math.Min(y, b));
			r -= num;
			y -= num;
			b -= num;
			float num2 = Math.Max(r, Math.Max(y, b));
			float num3 = Math.Min(y, b);
			y -= num3;
			b -= num3;
			if (b != 0f && num3 != 0f)
			{
				b *= 2f;
				num3 *= 2f;
			}
			r += y;
			num3 += y;
			float num4 = Math.Max(r, Math.Max(num3, b));
			if (num4 != 0f)
			{
				float num5 = num2 / num4;
				r *= num5;
				num3 *= num5;
				b *= num5;
			}
			r += num;
			num3 += num;
			b += num;
			int num6 = (int)Math.Round((double)(r * 255f));
			if (num6 > 255)
			{
				num6 = 255;
			}
			int num7 = (int)Math.Round((double)(num3 * 255f));
			if (num7 > 255)
			{
				num7 = 255;
			}
			int num8 = (int)Math.Round((double)(b * 255f));
			if (num8 > 255)
			{
				num8 = 255;
			}
			return Color.FromArgb(255, (byte)num6, (byte)num7, (byte)num8);
		}

		private static Color ryb2rgb(RYB ryb)
		{
			return ryb2rgb_1(ryb);
		}

		private static RYB rgb2ryb(Color clr)
		{
			float num = ((float)(int)clr.R + 0f) / 255f;
			float num2 = ((float)(int)clr.G + 0f) / 255f;
			float num3 = ((float)(int)clr.B + 0f) / 255f;
			float num4 = Math.Min(num, Math.Min(num2, num3));
			num -= num4;
			num2 -= num4;
			num3 -= num4;
			float num5 = Math.Max(num, Math.Max(num2, num3));
			float num6 = Math.Min(num, num2);
			num -= num6;
			num2 -= num6;
			if (num3 != 0f && num2 != 0f)
			{
				num3 /= 2f;
				num2 /= 2f;
			}
			num6 += num2;
			num3 += num2;
			float num7 = Math.Max(num, Math.Max(num6, num3));
			if (num7 != 0f)
			{
				float num8 = num5 / num7;
				num *= num8;
				num6 *= num8;
				num3 *= num8;
			}
			num += num4;
			num6 += num4;
			num3 += num4;
			return new RYB
			{
				R = num,
				Y = num6,
				B = num3
			};
		}

		private static Solution FindWeights(RYB srcRYB1, RYB srcRYB2, RYB neededRYB)
		{
			Solution solution = new Solution();
			float r = srcRYB1.R;
			float y = srcRYB1.Y;
			float b = srcRYB1.B;
			float r2 = srcRYB2.R;
			float y2 = srcRYB2.Y;
			float b2 = srcRYB2.B;
			float r3 = neededRYB.R;
			float y3 = neededRYB.Y;
			float b3 = neededRYB.B;
			float num5;
			float num4;
			float num3;
			float num2;
			float num;
			float num6 = num5 = (num4 = (num3 = (num2 = (num = float.NaN))));
			bool flag = true;
			bool flag2 = true;
			bool flag3 = true;
			if (y2 != 0f)
			{
				num6 = (r3 - y3 * r2 / y2) / (r - y * r2 / y2);
			}
			else if (y != 0f)
			{
				num6 = y3 / y;
			}
			else
			{
				flag = false;
			}
			if (flag)
			{
				if (r2 != 0f)
				{
					num5 = (r3 - r * num6) / r2;
				}
				else if (y2 != 0f)
				{
					num5 = (y3 - y * num6) / y2;
				}
				else
				{
					flag = false;
				}
			}
			if (b2 != 0f)
			{
				num4 = (r3 - b3 * r2 / b2) / (r - b * r2 / b2);
			}
			else if (b != 0f)
			{
				num4 = b3 / b;
			}
			else
			{
				flag2 = false;
			}
			if (r2 != 0f)
			{
				num3 = (r3 - r * num4) / r2;
			}
			else if (b2 != 0f)
			{
				num3 = (b3 - b * num4) / b2;
			}
			else
			{
				flag2 = false;
			}
			if (b2 != 0f)
			{
				num2 = (y3 - b3 * y2 / b2) / (y - b * y2 / b2);
			}
			else if (b != 0f)
			{
				num2 = b3 / b;
			}
			else
			{
				flag3 = false;
			}
			if (y2 != 0f)
			{
				num = (y3 - y * num2) / y2;
			}
			else if (b2 != 0f)
			{
				num = (b3 - b * num2) / b2;
			}
			else
			{
				flag3 = false;
			}
			float num7 = 0f;
			float num8 = 0f;
			int num9 = 0;
			if (flag && !float.IsNaN(num6) && !float.IsNaN(num5))
			{
				num7 += num6;
				num8 += num5;
				num9++;
			}
			if (flag2 && !float.IsNaN(num4) && !float.IsNaN(num3))
			{
				num7 += num4;
				num8 += num3;
				num9++;
			}
			if (flag3 && !float.IsNaN(num2) && !float.IsNaN(num))
			{
				num7 += num2;
				num8 += num;
				num9++;
			}
			num7 /= (float)num9;
			num8 /= (float)num9;
			RYB rYB = MixColors(srcRYB1, num7, srcRYB2, num8);
			solution.weight1 = num7;
			solution.clr1 = ryb2rgb(srcRYB1);
			solution.weight2 = num8;
			solution.clr2 = ryb2rgb(srcRYB2);
			solution.deltaR = Math.Abs(neededRYB.R - rYB.R);
			solution.deltaY = Math.Abs(neededRYB.Y - rYB.Y);
			solution.deltaB = Math.Abs(neededRYB.B - rYB.B);
			solution.delta_in_RYB = (float)Math.Sqrt((double)(solution.deltaR * solution.deltaR + solution.deltaY * solution.deltaY + solution.deltaB * solution.deltaB));
			solution.countColors = 2;
			solution.resultClr = ryb2rgb(rYB);
			return solution;
		}

		private static Solution FindWeights(RYB srcRYB1, RYB srcRYB2, RYB srcRYB3, RYB neededRYB)
		{
			Solution solution = new Solution();
			float r = srcRYB1.R;
			float y = srcRYB1.Y;
			float b = srcRYB1.B;
			float r2 = srcRYB2.R;
			float y2 = srcRYB2.Y;
			float b2 = srcRYB2.B;
			float r3 = srcRYB3.R;
			float y3 = srcRYB3.Y;
			float b3 = srcRYB3.B;
			float r4 = neededRYB.R;
			float y4 = neededRYB.Y;
			float b4 = neededRYB.B;
			float num = r - b * r2 / b2;
			float num2 = r3 - b3 * r2 / b2;
			float num3 = r4 - b4 * r2 / b2;
			float num4 = r - y * r2 / y2;
			float num5 = r3 - y3 * r2 / y2;
			float num6 = r4 - y4 * r2 / y2;
			float num7 = (num6 - num5 * num3 / num2) / (num4 - num * num5 / num2);
			float num8 = (num6 - num7 * num4) / num5;
			float num9 = (r4 - num7 * r - num8 * r3) / r2;
			RYB rYB = MixColors(MixColors(srcRYB1, num7, srcRYB2, num9), num7 + num9, srcRYB3, num8);
			RYB rYB2 = MixColors(MixColors(srcRYB1, num7, srcRYB3, num8), num7 + num8, srcRYB2, num9);
			RYB rYB3 = MixColors(MixColors(srcRYB3, num8, srcRYB2, num9), num8 + num9, srcRYB1, num7);
			RYB rYB4 = new RYB
			{
				R = (rYB.R + rYB2.R + rYB3.R) / 3f,
				Y = (rYB.Y + rYB2.Y + rYB3.Y) / 3f,
				B = (rYB.B + rYB2.B + rYB3.B) / 3f
			};
			solution.weight1 = num7;
			solution.clr1 = ryb2rgb(srcRYB1);
			solution.weight2 = num9;
			solution.clr2 = ryb2rgb(srcRYB2);
			solution.weight3 = num8;
			solution.clr3 = ryb2rgb(srcRYB3);
			solution.deltaR = Math.Abs(neededRYB.R - rYB4.R);
			solution.deltaY = Math.Abs(neededRYB.Y - rYB4.Y);
			solution.deltaB = Math.Abs(neededRYB.B - rYB4.B);
			solution.delta_in_RYB = (float)Math.Sqrt((double)(solution.deltaR * solution.deltaR + solution.deltaY * solution.deltaY + solution.deltaB * solution.deltaB));
			solution.countColors = 3;
			solution.resultClr = ryb2rgb(rYB4);
			return solution;
		}

		public static List<Beads> CalculateNumberOfMixingColors(List<Beads> _neededColors, List<Beads> srcColors)
		{
			List<Beads> neededColors = new List<Beads>();
			foreach (Beads _neededColor in _neededColors)
			{
				List<Beads> list = neededColors;
				Beads beads = new Beads();
				Color clr = _neededColor.clr;
				byte r = clr.R;
				clr = _neededColor.clr;
				byte g = clr.G;
				clr = _neededColor.clr;
				beads.clr = Color.FromArgb(255, r, g, clr.B);
				beads.id = _neededColor.id;
				list.Add(beads);
			}
			Parallel.For(0, neededColors.Count(), new ParallelOptions
			{
				MaxDegreeOfParallelism = Environment.ProcessorCount
			}, delegate(int n)
			{
				Solution bestWeights = new Solution();
				Color clr2 = neededColors[n].clr;
				RYB rYB = rgb2ryb(Color.FromArgb(255, clr2.R, clr2.G, clr2.B));
				Color clr3;
				for (int i = 0; i < srcColors.Count(); i++)
				{
					clr3 = srcColors[i].clr;
					RYB rYB2 = rgb2ryb(Color.FromArgb(255, clr3.R, clr3.G, clr3.B));
					float num = Math.Abs(rYB.R - rYB2.R);
					float num2 = Math.Abs(rYB.Y - rYB2.Y);
					float num3 = Math.Abs(rYB.B - rYB2.B);
					float num4 = (float)Math.Sqrt((double)(num * num + num2 * num2 + num3 * num3));
					if (num4 < 0.2f && num4 < bestWeights.delta_in_RYB)
					{
						bestWeights.countColors = 1;
						bestWeights.clr1 = (bestWeights.resultClr = clr3);
						bestWeights.weight1 = 1f;
						bestWeights.deltaR = num;
						bestWeights.deltaY = num2;
						bestWeights.deltaB = num3;
						bestWeights.delta_in_RYB = num4;
						bestWeights.isItialized = true;
					}
				}
				for (int j = 0; j < srcColors.Count(); j++)
				{
					clr3 = srcColors[j].clr;
					RYB rYB2 = rgb2ryb(Color.FromArgb(255, clr3.R, clr3.G, clr3.B));
					for (int k = j + 1; k < srcColors.Count(); k++)
					{
						clr3 = srcColors[k].clr;
						RYB srcRYB = rgb2ryb(Color.FromArgb(255, clr3.R, clr3.G, clr3.B));
						Solution solution = FindWeights(rYB2, srcRYB, rYB);
						if (solution.weight1 >= 0f && solution.weight2 >= 0f && solution.delta_in_RYB < bestWeights.delta_in_RYB)
						{
							bestWeights = solution;
							bestWeights.isItialized = true;
						}
					}
				}
				for (int l = 0; l < srcColors.Count(); l++)
				{
					clr3 = srcColors[l].clr;
					RYB rYB2 = rgb2ryb(Color.FromArgb(255, clr3.R, clr3.G, clr3.B));
					for (int m = l + 1; m < srcColors.Count(); m++)
					{
						clr3 = srcColors[m].clr;
						RYB srcRYB = rgb2ryb(Color.FromArgb(255, clr3.R, clr3.G, clr3.B));
						for (int num5 = m + 1; num5 < srcColors.Count(); num5++)
						{
							clr3 = srcColors[num5].clr;
							RYB srcRYB2 = rgb2ryb(Color.FromArgb(255, clr3.R, clr3.G, clr3.B));
							Solution solution2 = FindWeights(rYB2, srcRYB, srcRYB2, rYB);
							if (solution2.weight1 >= 0f && solution2.weight2 >= 0f && solution2.weight3 >= 0f && solution2.delta_in_RYB < bestWeights.delta_in_RYB)
							{
								bestWeights = solution2;
								bestWeights.isItialized = true;
							}
						}
					}
				}
				if (bestWeights.isItialized && bestWeights.delta_in_RYB < 0.2f)
				{
					float num6 = 1f;
					num6 = ((bestWeights.countColors != 1) ? ((bestWeights.countColors != 2) ? Math.Min(bestWeights.weight1, Math.Min(bestWeights.weight2, bestWeights.weight3)) : Math.Min(bestWeights.weight1, bestWeights.weight2)) : bestWeights.weight1);
					neededColors[n].mixingColors = new Dictionary<Beads, float>();
					neededColors[n].clr = bestWeights.resultClr;
					Beads key = srcColors.First((Beads bd) => bd.clr == bestWeights.clr1);
					neededColors[n].mixingColors.Add(key, bestWeights.weight1 / num6);
					if (bestWeights.countColors > 1)
					{
						key = srcColors.First((Beads bd) => bd.clr == bestWeights.clr2);
						neededColors[n].mixingColors.Add(key, bestWeights.weight2 / num6);
						if (bestWeights.countColors == 3)
						{
							key = srcColors.First((Beads bd) => bd.clr == bestWeights.clr3);
							neededColors[n].mixingColors.Add(key, bestWeights.weight3 / num6);
						}
					}
					neededColors[n].mixingColors = (from pair in neededColors[n].mixingColors
					orderby pair.Value
					select pair).ToDictionary((KeyValuePair<Beads, float> pair) => pair.Key, (KeyValuePair<Beads, float> pair) => pair.Value);
				}
				else
				{
					neededColors[n].mixingColors = null;
				}
			});
			return neededColors;
		}
	}
}
