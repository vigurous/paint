using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Бисерок
{
	public partial class wndProcessingPhotos : Window, IComponentConnector
	{
		public Bitmap bmpSrc;

		private WriteableBitmap wbmp;

		public Bitmap tmpBmp;

		private bool isSrcImageLoaded;

		private bool isNotProgrammaticallyChangesLightnessOrContrast = true;

		private bool inProcessOfChangingLightness;

		private bool inProcessOfChangingContrast;

		public RectangleF rctCropping_coordinates;

		private float lastWidth;

		private float lastHeight;

		private bool isInProcessIfCropping;

		private bool topBorderTitghtening;

		private bool bottomBorderTitghtening;

		private bool rightBorderTitghtening;

		private bool leftBorderTitghtening;

		private Bitmap nonChangedSourceBmp;

		

		public wndProcessingPhotos(Bitmap _bmp)
		{
			InitializeComponent();
			nonChangedSourceBmp = new Bitmap(_bmp);
			LoadRawSourceImage();
			AdjustWindowAccordingToVersion();
		}

		private void AdjustWindowAccordingToVersion()
		{
			if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
			{
				if (Settings.v < 2)
				{
					tblEnableInMoreVersions.Text = "  ДОСТУПНО  В  ПРОФЕСС.  И  КОММЕРЧ.  ВЕРСИЯХ  ";
					btnConvertToGrayScale_Click(new object(), new RoutedEventArgs());
					rctCropping.Stroke = System.Windows.Media.Brushes.Gray;
					btnApplyChanges.IsEnabled = false;
					gbx.IsEnabled = false;
				}
				else
				{
					btnApplyChanges.IsEnabled = true;
					brdDisabledInThisVersion.Visibility = Visibility.Collapsed;
				}
			}
			else if (Settings.v < 3)
			{
				btnConvertToGrayScale_Click(new object(), new RoutedEventArgs());
				rctCropping.Stroke = System.Windows.Media.Brushes.Gray;
				btnApplyChanges.IsEnabled = false;
				gbx.IsEnabled = false;
			}
			else
			{
				btnApplyChanges.IsEnabled = true;
				brdDisabledInThisVersion.Visibility = Visibility.Collapsed;
			}
		}

		private void LoadRawSourceImage()
		{
			if (bmpSrc != null)
			{
				bmpSrc.Dispose();
				bmpSrc = null;
			}
			bmpSrc = new Bitmap(nonChangedSourceBmp);
			UpdateWriteableBitmapAccordingToBitmapSizes();
			FastDrawBitmapOnWPFImage();
			SetRectCroppingToFullImageShow();
			isSrcImageLoaded = true;
		}

		private void UpdateWriteableBitmapAccordingToBitmapSizes()
		{
			if (wbmp != null)
			{
				wbmp = null;
			}
			wbmp = new WriteableBitmap(bmpSrc.Width, bmpSrc.Height, (double)bmpSrc.HorizontalResolution, (double)bmpSrc.VerticalResolution, ConvertPixelFormat(bmpSrc.PixelFormat), null);
		}

		private System.Windows.Media.PixelFormat ConvertPixelFormat(System.Drawing.Imaging.PixelFormat sourceFormat)
		{
			switch (sourceFormat)
			{
			case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
				return PixelFormats.Bgr24;
			case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
				return PixelFormats.Bgra32;
			case System.Drawing.Imaging.PixelFormat.Format32bppRgb:
				return PixelFormats.Bgr32;
			case System.Drawing.Imaging.PixelFormat.Canonical:
				return PixelFormats.Bgra32;
			case System.Drawing.Imaging.PixelFormat.Format16bppArgb1555:
				return PixelFormats.Bgr555;
			case System.Drawing.Imaging.PixelFormat.Format16bppGrayScale:
				return PixelFormats.Gray16;
			case System.Drawing.Imaging.PixelFormat.Format16bppRgb555:
				return PixelFormats.Bgr555;
			case System.Drawing.Imaging.PixelFormat.Format16bppRgb565:
				return PixelFormats.Bgr565;
			case System.Drawing.Imaging.PixelFormat.Format1bppIndexed:
				return PixelFormats.Indexed1;
			case System.Drawing.Imaging.PixelFormat.Format32bppPArgb:
				return PixelFormats.Pbgra32;
			case System.Drawing.Imaging.PixelFormat.Format48bppRgb:
				return PixelFormats.Rgb48;
			case System.Drawing.Imaging.PixelFormat.Format4bppIndexed:
				return PixelFormats.Indexed4;
			case System.Drawing.Imaging.PixelFormat.Format64bppArgb:
				return PixelFormats.Rgba64;
			case System.Drawing.Imaging.PixelFormat.Format64bppPArgb:
				return PixelFormats.Prgba64;
			case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
				return PixelFormats.Indexed8;
			default:
				return PixelFormats.Bgr24;
			}
		}

		private void FastDrawBitmapOnWPFImage()
		{
			BitmapData bitmapData = bmpSrc.LockBits(new System.Drawing.Rectangle(0, 0, bmpSrc.Width, bmpSrc.Height), ImageLockMode.ReadOnly, bmpSrc.PixelFormat);
			try
			{
				wbmp.WritePixels(new Int32Rect(0, 0, bmpSrc.Width, bmpSrc.Height), bitmapData.Scan0, bmpSrc.Height * bitmapData.Stride, bitmapData.Stride);
			}
			catch (Exception exc)
			{
				Site.SendExceptionInfo(exc, false);
			}
			finally
			{
				bmpSrc.UnlockBits(bitmapData);
			}
			img.Source = wbmp;
		}

		private void btnApplyChanges_Click(object sender, RoutedEventArgs e)
		{
			DiscardContrastAndLightnessChangesIfTheyAre();
			if (rctCropping_coordinates.X > 0f || rctCropping_coordinates.Y > 0f || rctCropping_coordinates.Width < 1f || rctCropping_coordinates.Height < 1f)
			{
				int x = (int)Math.Round((double)(rctCropping_coordinates.X * (float)bmpSrc.Width));
				int width = (int)Math.Round((double)(rctCropping_coordinates.Width * (float)bmpSrc.Width));
				int y = (int)Math.Round((double)(rctCropping_coordinates.Y * (float)bmpSrc.Height));
				int height = (int)Math.Round((double)(rctCropping_coordinates.Height * (float)bmpSrc.Height));
				using (Bitmap bitmap = new Bitmap(width, height))
				{
					Graphics graphics = Graphics.FromImage(bitmap);
					graphics.DrawImage(bmpSrc, new System.Drawing.Rectangle(0, 0, width, height), new System.Drawing.Rectangle(x, y, width, height), GraphicsUnit.Pixel);
					graphics.Dispose();
					bmpSrc = new Bitmap(bitmap);
				}
			}
			base.DialogResult = true;
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = false;
		}

		private void btnDiscardChanges_Click(object sender, RoutedEventArgs e)
		{
			LoadRawSourceImage();
			img.UpdateLayout();
			SetRectCroppingToFullImageShow();
			inProcessOfChangingContrast = (inProcessOfChangingLightness = false);
			if (tmpBmp != null)
			{
				tmpBmp.Dispose();
				tmpBmp = null;
			}
			isNotProgrammaticallyChangesLightnessOrContrast = false;
			Slider slider = slrLightness;
			Slider slider2 = sltContrast;
			double num3 = slider.Value = (slider2.Value = 0.0);
			isNotProgrammaticallyChangesLightnessOrContrast = true;
		}

		public void ChangeLightness(int delta)
		{
			BitmapData destData = bmpSrc.LockBits(new System.Drawing.Rectangle(0, 0, bmpSrc.Width, bmpSrc.Height), ImageLockMode.ReadWrite, bmpSrc.PixelFormat);
			int destK = destData.Stride / bmpSrc.Width;
			int num = bmpSrc.Width * bmpSrc.Height * destK;
			byte[] destBytes = new byte[num];
			IntPtr scan = destData.Scan0;
			Marshal.Copy(scan, destBytes, 0, destBytes.Length);
			BitmapData srcData = tmpBmp.LockBits(new System.Drawing.Rectangle(0, 0, tmpBmp.Width, tmpBmp.Height), ImageLockMode.ReadWrite, tmpBmp.PixelFormat);
			int srcK = srcData.Stride / tmpBmp.Width;
			int num2 = tmpBmp.Width * tmpBmp.Height * srcK;
			byte[] srcBytes = new byte[num2];
			IntPtr scan2 = srcData.Scan0;
			Marshal.Copy(scan2, srcBytes, 0, srcBytes.Length);
			int width = bmpSrc.Width;
			int height = bmpSrc.Height;
			int i;
			for (i = 0; i < height; i++)
			{
				Parallel.For(0, width, new ParallelOptions
				{
					MaxDegreeOfParallelism = Environment.ProcessorCount
				}, delegate(int j)
				{
					try
					{
						int num3 = i * srcData.Stride + j * srcK;
						int num4 = srcBytes[num3] + delta;
						int num5 = srcBytes[num3 + 1] + delta;
						int num6 = srcBytes[num3 + 2] + delta;
						if (num6 < 0)
						{
							num6 = 0;
						}
						else if (num6 > 255)
						{
							num6 = 255;
						}
						if (num5 < 0)
						{
							num5 = 0;
						}
						else if (num5 > 255)
						{
							num5 = 255;
						}
						if (num4 < 0)
						{
							num4 = 0;
						}
						else if (num4 > 255)
						{
							num4 = 255;
						}
						int num7 = i * destData.Stride + j * destK;
						destBytes[num7] = (byte)num4;
						destBytes[num7 + 1] = (byte)num5;
						destBytes[num7 + 2] = (byte)num6;
					}
					catch (Exception)
					{
					}
				});
			}
			Marshal.Copy(srcBytes, 0, scan2, srcBytes.Length);
			tmpBmp.UnlockBits(srcData);
			Marshal.Copy(destBytes, 0, scan, destBytes.Length);
			bmpSrc.UnlockBits(destData);
		}

		private void slrLightness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (isSrcImageLoaded && isNotProgrammaticallyChangesLightnessOrContrast)
			{
				btnDiscardContrast_Click(new object(), new RoutedEventArgs());
				if (!inProcessOfChangingLightness)
				{
					inProcessOfChangingLightness = true;
					if (tmpBmp != null)
					{
						tmpBmp.Dispose();
						tmpBmp = null;
					}
					tmpBmp = new Bitmap(bmpSrc);
				}
				ChangeLightness((int)slrLightness.Value);
				FastDrawBitmapOnWPFImage();
			}
		}

		public void ChangeContrast(int delta)
		{
			int width = bmpSrc.Width;
			int height = bmpSrc.Height;
			BitmapData destData = bmpSrc.LockBits(new System.Drawing.Rectangle(0, 0, bmpSrc.Width, bmpSrc.Height), ImageLockMode.ReadWrite, bmpSrc.PixelFormat);
			int destK = destData.Stride / bmpSrc.Width;
			int num = bmpSrc.Width * bmpSrc.Height * destK;
			byte[] destBytes = new byte[num];
			IntPtr scan = destData.Scan0;
			Marshal.Copy(scan, destBytes, 0, destBytes.Length);
			BitmapData srcData = tmpBmp.LockBits(new System.Drawing.Rectangle(0, 0, tmpBmp.Width, tmpBmp.Height), ImageLockMode.ReadWrite, tmpBmp.PixelFormat);
			int srcK = srcData.Stride / tmpBmp.Width;
			int num2 = tmpBmp.Width * tmpBmp.Height * srcK;
			byte[] srcBytes = new byte[num2];
			IntPtr scan2 = srcData.Scan0;
			Marshal.Copy(scan2, srcBytes, 0, srcBytes.Length);
			int i;
			for (i = 0; i < height; i++)
			{
				Parallel.For(0, width, new ParallelOptions
				{
					MaxDegreeOfParallelism = Environment.ProcessorCount
				}, delegate(int j)
				{
					try
					{
						int num3 = i * srcData.Stride + j * srcK;
						float num4 = (float)(int)srcBytes[num3];
						float num5 = (float)(int)srcBytes[num3 + 1];
						float num6 = (float)(int)srcBytes[num3 + 2];
						float num7 = Math.Abs(127.5f - (float)(int)srcBytes[num3 + 2]) * (float)delta / 255f;
						float num8 = Math.Abs(127.5f - (float)(int)srcBytes[num3 + 1]) * (float)delta / 255f;
						float num9 = Math.Abs(127.5f - (float)(int)srcBytes[num3]) * (float)delta / 255f;
						num6 = ((!(num6 > 127f)) ? (num6 - num7) : (num6 + num7));
						num5 = ((!(num5 > 127f)) ? (num5 - num8) : (num5 + num8));
						num4 = ((!(num4 > 127f)) ? (num4 - num9) : (num4 + num9));
						if (num6 < 0f)
						{
							num6 = 0f;
						}
						else if (num6 > 255f)
						{
							num6 = 255f;
						}
						if (num5 < 0f)
						{
							num5 = 0f;
						}
						else if (num5 > 255f)
						{
							num5 = 255f;
						}
						if (num4 < 0f)
						{
							num4 = 0f;
						}
						else if (num4 > 255f)
						{
							num4 = 255f;
						}
						int num10 = i * destData.Stride + j * destK;
						destBytes[num10] = (byte)num4;
						destBytes[num10 + 1] = (byte)num5;
						destBytes[num10 + 2] = (byte)num6;
					}
					catch (Exception)
					{
					}
				});
			}
			Marshal.Copy(srcBytes, 0, scan2, srcBytes.Length);
			tmpBmp.UnlockBits(srcData);
			Marshal.Copy(destBytes, 0, scan, destBytes.Length);
			bmpSrc.UnlockBits(destData);
		}

		private void sltContrast_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (isSrcImageLoaded && isNotProgrammaticallyChangesLightnessOrContrast)
			{
				btnDiscardLightness_Click(new object(), new RoutedEventArgs());
				if (!inProcessOfChangingContrast)
				{
					inProcessOfChangingContrast = true;
					if (tmpBmp != null)
					{
						tmpBmp.Dispose();
						tmpBmp = null;
					}
					tmpBmp = new Bitmap(bmpSrc);
				}
				ChangeContrast((int)sltContrast.Value);
				FastDrawBitmapOnWPFImage();
			}
		}

		private void btnApplyLightness_Click(object sender, RoutedEventArgs e)
		{
			if (inProcessOfChangingLightness)
			{
				inProcessOfChangingLightness = false;
				tmpBmp.Dispose();
				tmpBmp = null;
				isNotProgrammaticallyChangesLightnessOrContrast = false;
				slrLightness.Value = 0.0;
				isNotProgrammaticallyChangesLightnessOrContrast = true;
			}
		}

		private void btnDiscardLightness_Click(object sender, RoutedEventArgs e)
		{
			if (inProcessOfChangingLightness)
			{
				inProcessOfChangingLightness = false;
				bmpSrc = new Bitmap(tmpBmp);
				UpdateWriteableBitmapAccordingToBitmapSizes();
				tmpBmp.Dispose();
				tmpBmp = null;
				isNotProgrammaticallyChangesLightnessOrContrast = false;
				slrLightness.Value = 0.0;
				isNotProgrammaticallyChangesLightnessOrContrast = true;
				FastDrawBitmapOnWPFImage();
			}
		}

		private void btnApplyContrast_Click(object sender, RoutedEventArgs e)
		{
			if (inProcessOfChangingContrast)
			{
				inProcessOfChangingContrast = false;
				tmpBmp.Dispose();
				tmpBmp = null;
				isNotProgrammaticallyChangesLightnessOrContrast = false;
				sltContrast.Value = 0.0;
				isNotProgrammaticallyChangesLightnessOrContrast = true;
			}
		}

		private void btnDiscardContrast_Click(object sender, RoutedEventArgs e)
		{
			if (inProcessOfChangingContrast)
			{
				inProcessOfChangingContrast = false;
				bmpSrc = new Bitmap(tmpBmp);
				UpdateWriteableBitmapAccordingToBitmapSizes();
				tmpBmp.Dispose();
				tmpBmp = null;
				isNotProgrammaticallyChangesLightnessOrContrast = false;
				sltContrast.Value = 0.0;
				isNotProgrammaticallyChangesLightnessOrContrast = true;
				FastDrawBitmapOnWPFImage();
			}
		}

		private void DiscardContrastAndLightnessChangesIfTheyAre()
		{
			btnDiscardContrast_Click(new object(), new RoutedEventArgs());
			btnDiscardLightness_Click(new object(), new RoutedEventArgs());
		}

		private void btnRotateCounterClockwise_Click(object sender, RoutedEventArgs e)
		{
			DiscardContrastAndLightnessChangesIfTheyAre();
			bmpSrc.RotateFlip(RotateFlipType.Rotate270FlipNone);
			UpdateWriteableBitmapAccordingToBitmapSizes();
			FastDrawBitmapOnWPFImage();
			img.UpdateLayout();
			float x = rctCropping_coordinates.X;
			float y = rctCropping_coordinates.Y;
			float width = rctCropping_coordinates.Width;
			float height = rctCropping_coordinates.Height;
			rctCropping_coordinates.Width = height;
			rctCropping_coordinates.Height = width;
			rctCropping_coordinates.X = y;
			rctCropping_coordinates.Y = 1f - (x + width);
			RedrawRectCropping();
		}

		private void btnRotateClockwise_Click(object sender, RoutedEventArgs e)
		{
			DiscardContrastAndLightnessChangesIfTheyAre();
			bmpSrc.RotateFlip(RotateFlipType.Rotate90FlipNone);
			UpdateWriteableBitmapAccordingToBitmapSizes();
			FastDrawBitmapOnWPFImage();
			img.UpdateLayout();
			float x = rctCropping_coordinates.X;
			float y = rctCropping_coordinates.Y;
			float width = rctCropping_coordinates.Width;
			float height = rctCropping_coordinates.Height;
			rctCropping_coordinates.Width = height;
			rctCropping_coordinates.Height = width;
			rctCropping_coordinates.Y = x;
			rctCropping_coordinates.X = 1f - (y + height);
			RedrawRectCropping();
		}

		private void btnFlipHorizontally_Click(object sender, RoutedEventArgs e)
		{
			DiscardContrastAndLightnessChangesIfTheyAre();
			bmpSrc.RotateFlip(RotateFlipType.RotateNoneFlipX);
			FastDrawBitmapOnWPFImage();
			rctCropping_coordinates.X = 1f - (rctCropping_coordinates.X + rctCropping_coordinates.Width);
			RedrawRectCropping();
		}

		private void btnFlipVertically_Click(object sender, RoutedEventArgs e)
		{
			DiscardContrastAndLightnessChangesIfTheyAre();
			bmpSrc.RotateFlip(RotateFlipType.Rotate180FlipX);
			FastDrawBitmapOnWPFImage();
			rctCropping_coordinates.Y = 1f - (rctCropping_coordinates.Y + rctCropping_coordinates.Height);
			RedrawRectCropping();
		}

		private void SetRectCroppingToFullImageShow()
		{
			//ref RectangleF val = ref rctCropping_coordinates;
			//ref RectangleF val2 = ref rctCropping_coordinates;
			//float num3 = val.X = (val2.Y = 0f);
			//ref RectangleF val3 = ref rctCropping_coordinates;
			//ref RectangleF val4 = ref rctCropping_coordinates;
			//float num4 = lastWidth = (lastHeight = 1f);
			//num3 = (val3.Width = (val4.Height = num4));
			//RedrawRectCropping();
		}

		private void SetAllBordersTighteningToFalse()
		{
			topBorderTitghtening = (bottomBorderTitghtening = (rightBorderTitghtening = (leftBorderTitghtening = false)));
		}

		private void RedrawRectCropping()
		{
			if (img.ActualWidth > 0.0)
			{
				float num = 0f;
				float num2 = rctCropping_coordinates.Width * (float)img.ActualWidth;
				float num3 = rctCropping_coordinates.Height * (float)img.ActualHeight;
				float num4 = ((float)grdImg.ActualWidth - (float)img.ActualWidth) / 2f + rctCropping_coordinates.X * (float)img.ActualWidth;
				float num5 = ((float)grdImg.ActualHeight - (float)img.ActualHeight) / 2f + rctCropping_coordinates.Y * (float)img.ActualHeight;
				rctCropping.Margin = new Thickness((double)(num4 - num), (double)(num5 - num), 0.0, 0.0);
				rctCropping.Width = (double)(num2 + 2f * num);
				rctCropping.Height = (double)(num3 + 2f * num);
			}
		}

		private void rctCropping_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			DiscardContrastAndLightnessChangesIfTheyAre();
			isInProcessIfCropping = true;
			SetAllBordersTighteningToFalse();
			System.Windows.Point position = e.GetPosition(img);
			double num = position.X / img.ActualWidth;
			double num2 = position.Y / img.ActualHeight;
			double num3 = (rctCropping.StrokeThickness + 0.5) / img.ActualWidth;
			if ((double)rctCropping_coordinates.X - 2.0 * num3 <= num && (double)rctCropping_coordinates.X + num3 * 4.0 >= num)
			{
				leftBorderTitghtening = true;
			}
			else if ((double)(rctCropping_coordinates.X + rctCropping_coordinates.Width) - 2.0 * num3 <= num && (double)(rctCropping_coordinates.X + rctCropping_coordinates.Width) + num3 * 4.0 >= num)
			{
				rightBorderTitghtening = true;
			}
			else if ((double)rctCropping_coordinates.Y - 2.0 * num3 <= num2 && (double)rctCropping_coordinates.Y + num3 * 4.0 >= num2)
			{
				topBorderTitghtening = true;
			}
			else if ((double)(rctCropping_coordinates.Y + rctCropping_coordinates.Height) - 2.0 * num3 <= num2 && (double)(rctCropping_coordinates.Y + rctCropping_coordinates.Height) + num3 * 4.0 >= num2)
			{
				bottomBorderTitghtening = true;
			}
			else
			{
				isInProcessIfCropping = false;
			}
		}

		private void rctCropping_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			isInProcessIfCropping = false;
		}

		private void grdImg_MouseLeave(object sender, MouseEventArgs e)
		{
			isInProcessIfCropping = false;
		}

		private void img_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			isInProcessIfCropping = false;
		}

		private void grdImg_MouseMove(object sender, MouseEventArgs e)
		{
			if (isInProcessIfCropping)
			{
				System.Windows.Point position = e.GetPosition(img);
				float num = (float)position.X / (float)img.ActualWidth;
				float num2 = (float)position.Y / (float)img.ActualHeight;
				if (leftBorderTitghtening)
				{
					rctCropping_coordinates.Width = lastWidth - (num - rctCropping_coordinates.X);
					lastWidth = rctCropping_coordinates.Width;
					rctCropping_coordinates.X = num;
				}
				else if (rightBorderTitghtening)
				{
					rctCropping_coordinates.Width = (lastWidth = num - rctCropping_coordinates.X);
				}
				else if (topBorderTitghtening)
				{
					rctCropping_coordinates.Height = lastHeight - (num2 - rctCropping_coordinates.Y);
					lastHeight = rctCropping_coordinates.Height;
					rctCropping_coordinates.Y = num2;
				}
				else
				{
					rctCropping_coordinates.Height = (lastHeight = num2 - rctCropping_coordinates.Y);
				}
				RedrawRectCropping();
			}
		}

		private void btnConvertToSepia_Click(object sender, RoutedEventArgs e)
		{
			DiscardContrastAndLightnessChangesIfTheyAre();
			int width = bmpSrc.Width;
			int height = bmpSrc.Height;
			BitmapData srcData = bmpSrc.LockBits(new System.Drawing.Rectangle(0, 0, bmpSrc.Width, bmpSrc.Height), ImageLockMode.ReadWrite, bmpSrc.PixelFormat);
			int srcK = srcData.Stride / bmpSrc.Width;
			int num = bmpSrc.Width * bmpSrc.Height * srcK;
			byte[] srcBytes = new byte[num];
			IntPtr scan = srcData.Scan0;
			Marshal.Copy(scan, srcBytes, 0, srcBytes.Length);
			int i;
			for (i = 0; i < height; i++)
			{
				Parallel.For(0, width, new ParallelOptions
				{
					MaxDegreeOfParallelism = Environment.ProcessorCount
				}, delegate(int j)
				{
					try
					{
						int num2 = i * srcData.Stride + j * srcK;
						byte b = srcBytes[num2];
						byte b2 = srcBytes[num2 + 1];
						byte b3 = srcBytes[num2 + 2];
						float num3 = (float)(int)b3 * 0.393f + (float)(int)b2 * 0.769f + (float)(int)b * 0.189f;
						float num4 = (float)(int)b3 * 0.349f + (float)(int)b2 * 0.686f + (float)(int)b * 0.168f;
						float num5 = (float)(int)b3 * 0.272f + (float)(int)b2 * 0.534f + (float)(int)b * 0.131f;
						if (num3 > 255f)
						{
							num3 = 255f;
						}
						if (num4 > 255f)
						{
							num4 = 255f;
						}
						if (num5 > 255f)
						{
							num5 = 255f;
						}
						srcBytes[num2] = (byte)num5;
						srcBytes[num2 + 1] = (byte)num4;
						srcBytes[num2 + 2] = (byte)num3;
					}
					catch (Exception)
					{
					}
				});
			}
			Marshal.Copy(srcBytes, 0, scan, srcBytes.Length);
			bmpSrc.UnlockBits(srcData);
			FastDrawBitmapOnWPFImage();
		}

		private void Window_Closing(object sender, CancelEventArgs e)
		{
			DiscardContrastAndLightnessChangesIfTheyAre();
			if (tmpBmp != null)
			{
				tmpBmp.Dispose();
				tmpBmp = null;
			}
			if (wbmp != null)
			{
				wbmp = null;
			}
			if (nonChangedSourceBmp != null)
			{
				nonChangedSourceBmp.Dispose();
				nonChangedSourceBmp = null;
			}
		}

		private void btnConvertToGrayScale_Click(object sender, RoutedEventArgs e)
		{
			DiscardContrastAndLightnessChangesIfTheyAre();
			int width = bmpSrc.Width;
			int height = bmpSrc.Height;
			BitmapData srcData = bmpSrc.LockBits(new System.Drawing.Rectangle(0, 0, bmpSrc.Width, bmpSrc.Height), ImageLockMode.ReadWrite, bmpSrc.PixelFormat);
			int srcK = srcData.Stride / bmpSrc.Width;
			int num = bmpSrc.Width * bmpSrc.Height * srcK;
			byte[] srcBytes = new byte[num];
			IntPtr scan = srcData.Scan0;
			Marshal.Copy(scan, srcBytes, 0, srcBytes.Length);
			int i;
			for (i = 0; i < height; i++)
			{
				Parallel.For(0, width, new ParallelOptions
				{
					MaxDegreeOfParallelism = Environment.ProcessorCount
				}, delegate(int j)
				{
					try
					{
						int num2 = i * srcData.Stride + j * srcK;
						byte[] array = srcBytes;
						int num3 = num2;
						byte[] array2 = srcBytes;
						int num4 = num2 + 1;
						byte b;
						srcBytes[num2 + 2] = (b = (byte)Math.Round((double)((float)(int)srcBytes[num2 + 2] * 0.299f + (float)(int)srcBytes[num2 + 1] * 0.587f + (float)(int)srcBytes[num2] * 0.114f)));
						array2[num4] = (b = b);
						array[num3] = b;
					}
					catch (Exception)
					{
					}
				});
			}
			Marshal.Copy(srcBytes, 0, scan, srcBytes.Length);
			bmpSrc.UnlockBits(srcData);
			FastDrawBitmapOnWPFImage();
		}

		private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			RedrawRectCropping();
		}
	}
}
