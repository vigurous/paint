using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Бисерок
{
	public partial class wndOkCancel : Window, IComponentConnector
	{
		public wndOkCancel(string strTitle, string strQuestion)
		{
			InitializeComponent();
			base.Title = strTitle;
			tblQuestion.Text = strQuestion;
		}

		private void btnOk_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = true;
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = false;
		}
	}
}
