using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using Бисерок.ServiceReference;

namespace Бисерок
{
	public partial class wndStart : Window, IComponentConnector
	{
		private bool isNeededRebootProgram;

		private bool isClosingByProgram;

		private WndMain wndMain;

		private bool isMainWindowOpenedAsBackground;

		public wndStart(bool _isNeededRebootProgram)
		{
			InitializeComponent();
			isNeededRebootProgram = _isNeededRebootProgram;
			tbl.Text = "Пожалуйста, введите Ваш адрес электронной почты (e-mail), который Вы указывали при оплате программы:";
			CheckAllOnStartup();
		}

		public wndStart()
		{
			InitializeComponent();

            if (AskPassword())
            {
                CheckAllOnStartup();
			}
            else
            {
                this.isClosingByProgram = true;
                Application.Current.Shutdown();
			}
		}


        private bool AskPassword()
        {
            var dialog = new wndPassword();
            return dialog.ShowDialog() == true;
        }

		private void CheckAllOnStartup()
		{
			if (!Settings.isItVersionWithoutContactInfo)
			{
				TextBlock textBlock = tblMail;
				textBlock.Text += Settings.siteAndEmailSettings.supportEmail;
				switch (Settings.hobbyProgramm)
				{
				case HobbyProgramm.Biserok:
					base.Title += " Бисерок 2.0";
					break;
				case HobbyProgramm.Krestik:
					base.Title += " Крестик 2.0";
					break;
				case HobbyProgramm.Petelka:
					base.Title += " Петелька";
					break;
				case HobbyProgramm.Almaz:
					base.Title += " Алмазная мозаика";
					break;
				case HobbyProgramm.Raskraska:
					base.Title += " Раскраска";
					break;
				}
			}
			else
			{
				Button button = btnBuyFullVersion;
				Button button2 = btnGoTestVersion;
				bool isEnabled = button2.IsEnabled = false;
				button.IsEnabled = isEnabled;
			}
			string text = RegistryWork.CheckEmail();
			if (text.Length > 0)
			{
				tbxEmail.Text = text;
			}
			if (!isNeededRebootProgram)
			{
				Settings.v = RegistryWork.CheckVersionAndCreateItIfNoExist();
				if (Settings.v > 0)
				{
                    RunProgramUnderCurrentVersion();

                    //               DateTime? nullable = RegistryWork.CheckDateTimeEnd();
                    //               if (nullable.HasValue)
                    //               {
                    //                   DateTime value = DateTime.Now;
                    //                   DateTime date = value.Date;
                    //                   if (date <= nullable.Value)
                    //                   {
                    //                       DateTime? nullable2 = null;
                    //                       try
                    //                       {
                    //                           ServicesClient servicesClient = new ServicesClient(Settings.ShortProgramName);
                    //                           string date2 = servicesClient.GetDate();
                    //                           servicesClient.Close();
                    //                           value = Convert.ToDateTime(Site.GetInfoFromTag(date2, "ltrDate", true));
                    //                           nullable2 = value.Date;
                    //                       }
                    //                       catch (Exception)
                    //                       {
                    //                       }
                    //                       if (nullable2.HasValue)
                    //                       {
                    //                           DateTime? t;
                    //                           if (RegistryWork.CheckIsItSpecialVersionAndGetKeyIfItIs().Contains("AntoniShmerling") && Settings.hobbyProgramm == HobbyProgramm.Raskraska)
                    //                           {
                    //                               t = nullable2;
                    //                               value = new DateTime(2018, 5, 6);
                    //                               if (t < (DateTime?)value)
                    //                               {
                    //                                   Settings.isItVersionWithAbilityOfTurningOffSourceImageChanging = true;
                    //                               }
                    //                           }
                    //                           t = nullable2;
                    //                           value = nullable.Value;
                    //                           if (t <= (DateTime?)value)
                    //                           {
                    //                               if (Site.IsReturnedOrder())
                    //                               {
                    //                                   ReturnPayment();
                    //                               }
                    //                               RunProgramUnderCurrentVersion();
                    //                           }
                    //                           else
                    //                           {
                    //                               EndOfSubscription();
                    //                           }
                    //                       }
                    //                       else
                    //                       {
                    //                           if (Site.IsReturnedOrder())
                    //                           {
                    //                               ReturnPayment();
                    //                           }
                    //                           RunProgramUnderCurrentVersion();
                    //                       }
                    //                   }
                    //	else
                    //	{
                    //		EndOfSubscription();
                    //	}
                    //}
                }
				else if (text.Length > 0)
				{
					if (RegistryWork.CheckEmailSendedOnServer())
					{
						RunProgramUnderCurrentVersion();
					}
					else
					{
						OrderInfo infoAboutOrder = Site.GetInfoAboutOrder(text);
						if (infoAboutOrder.isOkSendedToServer)
						{
							RegistryWork.WriteVersion(infoAboutOrder.v);
							Settings.v = infoAboutOrder.v;
							if (!infoAboutOrder.isActivatingLimit && infoAboutOrder.dtEnd.HasValue)
							{
								RegistryWork.WriteDateTimeExpired(infoAboutOrder.dtEnd);
							}
							RunProgramUnderCurrentVersion();
						}
						else
						{
							RunProgramUnderCurrentVersion();
						}
					}
				}
				else
				{
					OpenMainWindowOnBackground();
				}
				if (RegistryWork.IsUnexpired())
				{
					wndRenewSubscription wndRenewSubscription = new wndRenewSubscription();
					wndRenewSubscription.Closed += parent_wnd_Closed;
					wndRenewSubscription.Show();
					HideMainWindowAsBackground();
					base.Hide();
				}
			}
		}

		private void OpenMainWindowOnBackground()
		{
			wndMain = new WndMain();
			wndMain.IsEnabled = false;
			wndMain.ShowInTaskbar = false;
			wndMain.Show();
			isMainWindowOpenedAsBackground = true;
		}

		private void HideMainWindowAsBackground()
		{
			if (isMainWindowOpenedAsBackground)
			{
				try
				{
					wndMain.Hide();
				}
				catch
				{
				}
			}
		}

		private void CloseMainWindowAsBackground()
		{
			if (isMainWindowOpenedAsBackground)
			{
				try
				{
					wndMain.Close();
				}
				catch
				{
				}
			}
		}

		private void RunProgramUnderCurrentVersion()
		{
			HideMainWindowAsBackground();
			if (isNeededRebootProgram)
			{
				string fileName = AppDomain.CurrentDomain.BaseDirectory + AppDomain.CurrentDomain.SetupInformation.ApplicationName;
				Process process = new Process();
				process.StartInfo.FileName = fileName;
				process.Start();
				Application.Current.Shutdown();
			}
			else
			{
                WndMain wndMain = new WndMain();
                wndMain.Closed += parent_wnd_Closed;
                wndMain.Show();
                base.Hide();
            }
		}



		private void parent_wnd_Closed(object sender, EventArgs e)
		{
			isClosingByProgram = true;
			CloseMainWindowAsBackground();
			base.Close();
		}

		private void ReturnPayment()
		{
			RegistryWork.RewriteToReturn();
			Settings.v = 0;
			Site.SendActionInfo("Деактивация по возврату");
		}

		private void EndOfSubscription()
		{
			RegistryWork.RewriteToUnexpiredVersion();
			Settings.v = 0;
		}

		private void tbxEmail_GotFocus(object sender, RoutedEventArgs e)
		{
			if (tbxEmail.Text == "Введите Ваш e-mail...")
			{
				tbxEmail.Text = "";
			}
		}

		private void tbxEmail_LostFocus(object sender, RoutedEventArgs e)
		{
			if (tbxEmail.Text == "")
			{
				tbxEmail.Text = "Введите Ваш e-mail...";
			}
		}

		private void btnGoTestVersion_Click(object sender, RoutedEventArgs e)
		{
			if (IsCorrectEmail(tbxEmail.Text))
			{
				CheckExistingOrderAndWriteItAndGoUnderItVersion(tbxEmail.Text, false);
			}
			else
			{
				RunProgramUnderCurrentVersion();
			}
		}

		private void btnBuyFullVersion_Click(object sender, RoutedEventArgs e)
		{
			if (IsCorrectEmail(tbxEmail.Text))
			{
				string a = RegistryWork.CheckEmail();
				if (a != tbxEmail.Text)
				{
					RegistryWork.WriteIsEmailSendedOnServer(false);
				}
				RegistryWork.WriteEmail(tbxEmail.Text);
			}
			OrderInfo infoAboutOrder = Site.GetInfoAboutOrder(tbxEmail.Text);
			if (infoAboutOrder.isOkSendedToServer)
			{
				RegistryWork.WriteIsEmailSendedOnServer(true);
				RegistryWork.WriteVersion(infoAboutOrder.v);
				Settings.v = infoAboutOrder.v;
				if (!infoAboutOrder.isActivatingLimit && infoAboutOrder.dtEnd.HasValue)
				{
					RegistryWork.WriteDateTimeExpired(infoAboutOrder.dtEnd);
				}
			}
			Site.BuyProgram(null);
		}

		private void btnIBought_Click(object sender, RoutedEventArgs e)
		{
			if (IsCorrectEmail(tbxEmail.Text))
			{
				CheckExistingOrderAndWriteItAndGoUnderItVersion(tbxEmail.Text, true);
			}
			else
			{
				WndInfo wndInfo = new WndInfo("Для проверки оплаты Вам необходимо ввести Ваш адрес электронной почты, который Вы указывали при оплате");
				wndInfo.ShowDialog();
			}
		}

		private void CheckExistingOrderAndWriteItAndGoUnderItVersion(string email, bool isBoughtAndCheckingIt)
		{
			string a = RegistryWork.CheckEmail();
			if (a != email)
			{
				RegistryWork.WriteIsEmailSendedOnServer(false);
			}
			OrderInfo infoAboutOrder = Site.GetInfoAboutOrder(email);
			RegistryWork.WriteEmail(email);
			if (infoAboutOrder.isOkSendedToServer)
			{
				RegistryWork.WriteIsEmailSendedOnServer(true);
				RegistryWork.WriteVersion(infoAboutOrder.v);
				Settings.v = infoAboutOrder.v;
				if (!infoAboutOrder.isActivatingLimit && infoAboutOrder.dtEnd.HasValue)
				{
					RegistryWork.WriteDateTimeExpired(infoAboutOrder.dtEnd);
				}
				if (!isBoughtAndCheckingIt)
				{
					RunProgramUnderCurrentVersion();
				}
				else if (infoAboutOrder.v > 0)
				{
					RegistryWork.WriteIsUnexpired(false);
					string str = "";
					if (Settings.hobbyProgramm == HobbyProgramm.Raskraska)
					{
						switch (infoAboutOrder.v)
						{
						case 1:
							str = "ДОМАШНЕЙ";
							break;
						case 2:
							str = "ПРОФЕССИОНАЛЬНОЙ";
							break;
						case 3:
							str = "КОММЕРЧЕСКОЙ";
							break;
						}
					}
					else
					{
						switch (infoAboutOrder.v)
						{
						case 1:
							str = "БАЗОВОЙ";
							break;
						case 2:
							str = "ПРОДВИНУТОЙ";
							break;
						case 3:
							str = "МАСТЕР";
							break;
						}
					}
					WndInfo wndInfo = new WndInfo(Local.Get("Поздравляем с приобретением ") + str + Local.Get(" версии программы! Надеемся, что программа придётся Вам по душе. Если у Вас появятся любые вопросы - всегда пишите нам через вкладку 'Есть вопрос?' в программе, либо напрямую на эл.почту ") + Settings.siteAndEmailSettings.supportEmail);
					wndInfo.ShowDialog();
					RunProgramUnderCurrentVersion();
				}
				else
				{
					string info = Local.Get("Нет оплаты на указанный Вами e-mail. Укажите иной e-mail (на который есть оплаченный заказ), либо выполните оплату для этого e-mail'а. Если это ошибка - свяжитесь с нами по эл.почте ") + Settings.siteAndEmailSettings.supportEmail;
					if (infoAboutOrder.isActivatingLimit)
					{
						info = Local.Get("Превышено число активаций программы на данный адрес электронной почты.\r\nУкажите иной e-mail (на который есть оплаченный заказ).\r\n\r\nЕсли это ошибка (например, у Вас на компьютере работала оплаченная версия, срок оплаты которой НЕ истёк, и вдруг появилось это окно; или Вы сменили компьютер/переустановили Windows/и т.п.) - ОБЯЗАТЕЛЬНО свяжитесь с нами по эл.почте ") + Settings.siteAndEmailSettings.supportEmail + Local.Get("\r\nМы бесплатно сделаем для Вас новую активацию программы.");
					}
					else if (infoAboutOrder.isExpiredPayedOrder)
					{
						RegistryWork.WriteIsUnexpired(true);
						info = "Оплаченный период пользования программой для этого адреса электронной почты истёк. Укажите иной e-mail (на который есть оплаченный заказ), либо продлите оплату для данного e-mail'а. Если это ошибка - свяжитесь с нами по эл.почте " + Settings.siteAndEmailSettings.supportEmail;
					}
					WndInfo wndInfo2 = new WndInfo(info);
					wndInfo2.ShowDialog();
				}
			}
			else if (!isBoughtAndCheckingIt)
			{
				RunProgramUnderCurrentVersion();
			}
			else
			{
				WndInfo wndInfo3 = new WndInfo("Возникли помехи связи при проверке оплаты. Пожалуйста, проверьте подключение к интернету и повторите попытку, либо скопируйте информацию об ошибке (см.текст ниже) и пришлите нам на эл.почту " + Settings.siteAndEmailSettings.supportEmail, infoAboutOrder.exception);
				wndInfo3.ShowDialog();
			}
		}

		private bool IsCorrectEmail(string email)
		{
			if (email.Length > 0)
			{
				return tbxEmail.Text != "Введите Ваш e-mail...";
			}
			return false;
		}

		private void Window_Closing(object sender, CancelEventArgs e)
		{
			if (!isClosingByProgram)
			{
				RunProgramUnderCurrentVersion();
				e.Cancel = true;
			}
			else
			{
				CloseMainWindowAsBackground();
			}
		}
	}
}
