using System;
using System.Collections.Generic;
using System.Drawing;

namespace Бисерок
{
	[Serializable]
	public class PointForContourSymbol
	{
		public int number;

		public ColoringContour parentContour;

		public Point ptForSymbol;

		public int primaryPointsCount;

		public List<Point> pointsInArea = new List<Point>();

		public int sizeOfFont;
	}
}
