﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Resources;

[assembly: AssemblyTitle("Paint")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("2.1.1.4")]
[assembly: AssemblyFileVersion("2.1.1.4")]
[assembly: ComVisible(false)]
[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]
[assembly: NeutralResourcesLanguage("ru")]
[assembly: Guid("4338dafd-b201-4132-81e4-5e74e80dc2f8")]
[assembly: AssemblyAssociatedContentFile("additional/framebottomleft.jpg")]
[assembly: AssemblyAssociatedContentFile("additional/framebottommiddle.jpg")]
[assembly: AssemblyAssociatedContentFile("additional/framebottomright.jpg")]
[assembly: AssemblyAssociatedContentFile("additional/framemiddleleft.jpg")]
[assembly: AssemblyAssociatedContentFile("additional/framemiddleright.jpg")]
[assembly: AssemblyAssociatedContentFile("additional/frametopleft.jpg")]
[assembly: AssemblyAssociatedContentFile("additional/frametopmiddle.jpg")]
[assembly: AssemblyAssociatedContentFile("additional/frametopright.jpg")]
[assembly: AssemblyAssociatedContentFile("additional/logoarthobby.png")]
[assembly: AssemblyAssociatedContentFile("additional/logomelnica.png")]
[assembly: AssemblyAssociatedContentFile("badges/1.png")]
[assembly: AssemblyAssociatedContentFile("badges/10.png")]
[assembly: AssemblyAssociatedContentFile("badges/100.png")]
[assembly: AssemblyAssociatedContentFile("badges/101.png")]
[assembly: AssemblyAssociatedContentFile("badges/102.png")]
[assembly: AssemblyAssociatedContentFile("badges/103.png")]
[assembly: AssemblyAssociatedContentFile("badges/104.png")]
[assembly: AssemblyAssociatedContentFile("badges/105.png")]
[assembly: AssemblyAssociatedContentFile("badges/106.png")]
[assembly: AssemblyAssociatedContentFile("badges/107.png")]
[assembly: AssemblyAssociatedContentFile("badges/108.png")]
[assembly: AssemblyAssociatedContentFile("badges/109.png")]
[assembly: AssemblyAssociatedContentFile("badges/11.png")]
[assembly: AssemblyAssociatedContentFile("badges/110.png")]
[assembly: AssemblyAssociatedContentFile("badges/111.png")]
[assembly: AssemblyAssociatedContentFile("badges/112.png")]
[assembly: AssemblyAssociatedContentFile("badges/113.png")]
[assembly: AssemblyAssociatedContentFile("badges/114.png")]
[assembly: AssemblyAssociatedContentFile("badges/115.png")]
[assembly: AssemblyAssociatedContentFile("badges/116.png")]
[assembly: AssemblyAssociatedContentFile("badges/117.png")]
[assembly: AssemblyAssociatedContentFile("badges/118.png")]
[assembly: AssemblyAssociatedContentFile("badges/119.png")]
[assembly: AssemblyAssociatedContentFile("badges/12.png")]
[assembly: AssemblyAssociatedContentFile("badges/120.png")]
[assembly: AssemblyAssociatedContentFile("badges/121.png")]
[assembly: AssemblyAssociatedContentFile("badges/122.png")]
[assembly: AssemblyAssociatedContentFile("badges/123.png")]
[assembly: AssemblyAssociatedContentFile("badges/13.png")]
[assembly: AssemblyAssociatedContentFile("badges/14.png")]
[assembly: AssemblyAssociatedContentFile("badges/15.png")]
[assembly: AssemblyAssociatedContentFile("badges/16.png")]
[assembly: AssemblyAssociatedContentFile("badges/17.png")]
[assembly: AssemblyAssociatedContentFile("badges/18.png")]
[assembly: AssemblyAssociatedContentFile("badges/19.png")]
[assembly: AssemblyAssociatedContentFile("badges/2.png")]
[assembly: AssemblyAssociatedContentFile("badges/20.png")]
[assembly: AssemblyAssociatedContentFile("badges/21.png")]
[assembly: AssemblyAssociatedContentFile("badges/22.png")]
[assembly: AssemblyAssociatedContentFile("badges/23.png")]
[assembly: AssemblyAssociatedContentFile("badges/24.png")]
[assembly: AssemblyAssociatedContentFile("badges/25.png")]
[assembly: AssemblyAssociatedContentFile("badges/26.png")]
[assembly: AssemblyAssociatedContentFile("badges/27.png")]
[assembly: AssemblyAssociatedContentFile("badges/28.png")]
[assembly: AssemblyAssociatedContentFile("badges/29.png")]
[assembly: AssemblyAssociatedContentFile("badges/3.png")]
[assembly: AssemblyAssociatedContentFile("badges/30.png")]
[assembly: AssemblyAssociatedContentFile("badges/31.png")]
[assembly: AssemblyAssociatedContentFile("badges/32.png")]
[assembly: AssemblyAssociatedContentFile("badges/33.png")]
[assembly: AssemblyAssociatedContentFile("badges/34.png")]
[assembly: AssemblyAssociatedContentFile("badges/35.png")]
[assembly: AssemblyAssociatedContentFile("badges/36.png")]
[assembly: AssemblyAssociatedContentFile("badges/37.png")]
[assembly: AssemblyAssociatedContentFile("badges/38.png")]
[assembly: AssemblyAssociatedContentFile("badges/39.png")]
[assembly: AssemblyAssociatedContentFile("badges/4.png")]
[assembly: AssemblyAssociatedContentFile("badges/40.png")]
[assembly: AssemblyAssociatedContentFile("badges/41.png")]
[assembly: AssemblyAssociatedContentFile("badges/42.png")]
[assembly: AssemblyAssociatedContentFile("badges/43.png")]
[assembly: AssemblyAssociatedContentFile("badges/44.png")]
[assembly: AssemblyAssociatedContentFile("badges/45.png")]
[assembly: AssemblyAssociatedContentFile("badges/46.png")]
[assembly: AssemblyAssociatedContentFile("badges/47.png")]
[assembly: AssemblyAssociatedContentFile("badges/48.png")]
[assembly: AssemblyAssociatedContentFile("badges/49.png")]
[assembly: AssemblyAssociatedContentFile("badges/5.png")]
[assembly: AssemblyAssociatedContentFile("badges/50.png")]
[assembly: AssemblyAssociatedContentFile("badges/51.png")]
[assembly: AssemblyAssociatedContentFile("badges/52.png")]
[assembly: AssemblyAssociatedContentFile("badges/53.png")]
[assembly: AssemblyAssociatedContentFile("badges/54.png")]
[assembly: AssemblyAssociatedContentFile("badges/55.png")]
[assembly: AssemblyAssociatedContentFile("badges/56.png")]
[assembly: AssemblyAssociatedContentFile("badges/57.png")]
[assembly: AssemblyAssociatedContentFile("badges/58.png")]
[assembly: AssemblyAssociatedContentFile("badges/59.png")]
[assembly: AssemblyAssociatedContentFile("badges/6.png")]
[assembly: AssemblyAssociatedContentFile("badges/60.png")]
[assembly: AssemblyAssociatedContentFile("badges/61.png")]
[assembly: AssemblyAssociatedContentFile("badges/62.png")]
[assembly: AssemblyAssociatedContentFile("badges/63.png")]
[assembly: AssemblyAssociatedContentFile("badges/64.png")]
[assembly: AssemblyAssociatedContentFile("badges/65.png")]
[assembly: AssemblyAssociatedContentFile("badges/66.png")]
[assembly: AssemblyAssociatedContentFile("badges/67.png")]
[assembly: AssemblyAssociatedContentFile("badges/68.png")]
[assembly: AssemblyAssociatedContentFile("badges/69.png")]
[assembly: AssemblyAssociatedContentFile("badges/7.png")]
[assembly: AssemblyAssociatedContentFile("badges/70.png")]
[assembly: AssemblyAssociatedContentFile("badges/71.png")]
[assembly: AssemblyAssociatedContentFile("badges/72.png")]
[assembly: AssemblyAssociatedContentFile("badges/73.png")]
[assembly: AssemblyAssociatedContentFile("badges/74.png")]
[assembly: AssemblyAssociatedContentFile("badges/75.png")]
[assembly: AssemblyAssociatedContentFile("badges/76.png")]
[assembly: AssemblyAssociatedContentFile("badges/77.png")]
[assembly: AssemblyAssociatedContentFile("badges/78.png")]
[assembly: AssemblyAssociatedContentFile("badges/79.png")]
[assembly: AssemblyAssociatedContentFile("badges/8.png")]
[assembly: AssemblyAssociatedContentFile("badges/80.png")]
[assembly: AssemblyAssociatedContentFile("badges/81.png")]
[assembly: AssemblyAssociatedContentFile("badges/82.png")]
[assembly: AssemblyAssociatedContentFile("badges/83.png")]
[assembly: AssemblyAssociatedContentFile("badges/84.png")]
[assembly: AssemblyAssociatedContentFile("badges/85.png")]
[assembly: AssemblyAssociatedContentFile("badges/86.png")]
[assembly: AssemblyAssociatedContentFile("badges/87.png")]
[assembly: AssemblyAssociatedContentFile("badges/88.png")]
[assembly: AssemblyAssociatedContentFile("badges/89.png")]
[assembly: AssemblyAssociatedContentFile("badges/9.png")]
[assembly: AssemblyAssociatedContentFile("badges/90.png")]
[assembly: AssemblyAssociatedContentFile("badges/91.png")]
[assembly: AssemblyAssociatedContentFile("badges/92.png")]
[assembly: AssemblyAssociatedContentFile("badges/93.png")]
[assembly: AssemblyAssociatedContentFile("badges/94.png")]
[assembly: AssemblyAssociatedContentFile("badges/95.png")]
[assembly: AssemblyAssociatedContentFile("badges/96.png")]
[assembly: AssemblyAssociatedContentFile("badges/97.png")]
[assembly: AssemblyAssociatedContentFile("badges/98.png")]
[assembly: AssemblyAssociatedContentFile("badges/99.png")]
[assembly: AssemblyAssociatedContentFile("catalogstate.txt")]
[assembly: AssemblyAssociatedContentFile("images/labuda/1.jpg")]
[assembly: AssemblyAssociatedContentFile("images/labuda/2.jpg")]
[assembly: AssemblyAssociatedContentFile("images/labuda/3.jpg")]
[assembly: AssemblyAssociatedContentFile("images/labuda/4.jpg")]
[assembly: AssemblyAssociatedContentFile("images/labuda/5.jpg")]
[assembly: AssemblyAssociatedContentFile("images/labuda/6.jpg")]
[assembly: AssemblyAssociatedContentFile("images/labuda/7.jpg")]
[assembly: AssemblyAssociatedContentFile("images/labuda/8.jpg")]
[assembly: AssemblyAssociatedContentFile("images/labuda/9.jpg")]
[assembly: AssemblyAssociatedContentFile("images/logoalmaz.ico")]
[assembly: AssemblyAssociatedContentFile("images/logoraskraska.ico")]
[assembly: AssemblyAssociatedContentFile("images/logowithoutcontactinfo.ico")]
[assembly: AssemblyAssociatedContentFile("images/straza.png")]
[assembly: AssemblyAssociatedContentFile("images/strazcircle.png")]
[assembly: AssemblyAssociatedContentFile("images/strazsquare.png")]
[assembly: AssemblyAssociatedContentFile("mypalette.txt")]
[assembly: AssemblyAssociatedContentFile("otherprograms/%d0%b0%d0%bb%d0%bc%d0%b0%d0%b7%d0%bd%d0%b0%d1%8f%20%d0%bc%d0%be%d0%b7%d0%b0%d0%b8%d0%ba%d0%b0.html")]
[assembly: AssemblyAssociatedContentFile("otherprograms/%d0%b0%d0%bb%d0%bc%d0%b0%d0%b7%d0%bd%d0%b0%d1%8f%20%d0%bc%d0%be%d0%b7%d0%b0%d0%b8%d0%ba%d0%b0.png")]
[assembly: AssemblyAssociatedContentFile("otherprograms/%d0%b1%d0%b8%d1%81%d0%b5%d1%80%d0%be%d0%ba.html")]
[assembly: AssemblyAssociatedContentFile("otherprograms/%d0%b1%d0%b8%d1%81%d0%b5%d1%80%d0%be%d0%ba.png")]
[assembly: AssemblyAssociatedContentFile("otherprograms/%d0%ba%d1%80%d0%b5%d1%81%d1%82%d0%b8%d0%ba.html")]
[assembly: AssemblyAssociatedContentFile("otherprograms/%d0%ba%d1%80%d0%b5%d1%81%d1%82%d0%b8%d0%ba.png")]
[assembly: AssemblyAssociatedContentFile("otherprograms/%d0%bf%d0%b5%d1%82%d0%b5%d0%bb%d1%8c%d0%ba%d0%b0.html")]
[assembly: AssemblyAssociatedContentFile("otherprograms/%d0%bf%d0%b5%d1%82%d0%b5%d0%bb%d1%8c%d0%ba%d0%b0.png")]
[assembly: AssemblyAssociatedContentFile("otherprograms/%d1%80%d0%b0%d1%81%d0%ba%d1%80%d0%b0%d1%81%d0%ba%d0%b0.html")]
[assembly: AssemblyAssociatedContentFile("otherprograms/%d1%80%d0%b0%d1%81%d0%ba%d1%80%d0%b0%d1%81%d0%ba%d0%b0.png")]
[assembly: AssemblyAssociatedContentFile("preciosainsales.txt")]
[assembly: AssemblyAssociatedContentFile("%d0%90%d0%bb%d0%bc%d0%b0%d0%b7%d0%bd%d0%b0%d1%8f%20%d0%bc%d0%be%d0%b7%d0%b0%d0%b8%d0%ba%d0%b0.txt")]
[assembly: AssemblyAssociatedContentFile("%d0%9a%d1%80%d0%b5%d1%81%d1%82%d0%b8%d0%ba.txt")]
[assembly: AssemblyAssociatedContentFile("%d0%9f%d0%b5%d1%82%d0%b5%d0%bb%d1%8c%d0%ba%d0%b0.txt")]
[assembly: AssemblyAssociatedContentFile("%d0%a0%d0%b0%d1%81%d0%ba%d1%80%d0%b0%d1%81%d0%ba%d0%b0.txt")]
[assembly: AssemblyAssociatedContentFile("%d0%91%d0%b8%d1%81%d0%b5%d1%80%d0%be%d0%ba.txt")]
[assembly: AssemblyAssociatedContentFile("%d0%98%d0%bd%d1%81%d1%82%d1%80%d1%83%d0%ba%d1%86%d0%b8%d1%8f%20%d0%bf%d0%be%20%d1%80%d0%b0%d0%b1%d0%be%d1%82%d0%b5%20%d1%81%20%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%be%d0%b9%20%d0%9a%d1%80%d0%b5%d1%81%d1%82%d0%b8%d0%ba.rtf")]
[assembly: AssemblyAssociatedContentFile("%d0%98%d0%bd%d1%81%d1%82%d1%80%d1%83%d0%ba%d1%86%d0%b8%d1%8f%20%d0%bf%d0%be%20%d1%80%d0%b0%d0%b1%d0%be%d1%82%d0%b5%20%d1%81%20%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%be%d0%b9%20%d0%91%d0%b8%d1%81%d0%b5%d1%80%d0%be%d0%ba.rtf")]
[assembly: AssemblyAssociatedContentFile("%d0%98%d0%bd%d1%81%d1%82%d1%80%d1%83%d0%ba%d1%86%d0%b8%d1%8f%20%d0%bf%d0%be%20%d1%80%d0%b0%d0%b1%d0%be%d1%82%d0%b5%20%d1%81%20%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%be%d0%b9%20%d0%9f%d0%b5%d1%82%d0%b5%d0%bb%d1%8c%d0%ba%d0%b0.rtf")]
[assembly: AssemblyAssociatedContentFile("images/1.png")]
[assembly: AssemblyAssociatedContentFile("images/2.png")]
[assembly: AssemblyAssociatedContentFile("images/2fill.png")]
[assembly: AssemblyAssociatedContentFile("images/arrow_lr.png")]
[assembly: AssemblyAssociatedContentFile("images/bottomnarrow.png")]
[assembly: AssemblyAssociatedContentFile("images/bottomnarrowdisabled.png")]
[assembly: AssemblyAssociatedContentFile("images/brush.cur")]
[assembly: AssemblyAssociatedContentFile("images/brush.png")]
[assembly: AssemblyAssociatedContentFile("images/fill.cur")]
[assembly: AssemblyAssociatedContentFile("images/fill.png")]
[assembly: AssemblyAssociatedContentFile("images/krest4.png")]
[assembly: AssemblyAssociatedContentFile("images/krest5.png")]
[assembly: AssemblyAssociatedContentFile("images/krest6.png")]
[assembly: AssemblyAssociatedContentFile("images/krest7.png")]
[assembly: AssemblyAssociatedContentFile("images/logobiserok.ico")]
[assembly: AssemblyAssociatedContentFile("images/logokrestik.ico")]
[assembly: AssemblyAssociatedContentFile("images/logopetelka.ico")]
[assembly: AssemblyAssociatedContentFile("images/narrowright.png")]
[assembly: AssemblyAssociatedContentFile("images/narrowrightdisabled.png")]
[assembly: AssemblyAssociatedContentFile("images/petelka1.png")]
[assembly: AssemblyAssociatedContentFile("images/pipet.cur")]
[assembly: AssemblyAssociatedContentFile("images/pipet.png")]
[assembly: AssemblyAssociatedContentFile("%d0%98%d0%bd%d1%81%d1%82%d1%80%d1%83%d0%ba%d1%86%d0%b8%d1%8f%20%d0%bf%d0%be%20%d1%80%d0%b0%d0%b1%d0%be%d1%82%d0%b5%20%d1%81%20%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%be%d0%b9%20%d0%90%d0%bb%d0%bc%d0%b0%d0%b7%d0%bd%d0%b0%d1%8f%20%d0%bc%d0%be%d0%b7%d0%b0%d0%b8%d0%ba%d0%b0.rtf")]
[assembly: AssemblyAssociatedContentFile("%d0%98%d0%bd%d1%81%d1%82%d1%80%d1%83%d0%ba%d1%86%d0%b8%d1%8f%20%d0%bf%d0%be%20%d1%80%d0%b0%d0%b1%d0%be%d1%82%d0%b5%20%d1%81%20%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%be%d0%b9%20%d0%a0%d0%b0%d1%81%d0%ba%d1%80%d0%b0%d1%81%d0%ba%d0%b0.docx")]